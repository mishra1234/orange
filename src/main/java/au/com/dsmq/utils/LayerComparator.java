package au.com.dsmq.utils;

import au.com.dmsq.model.Layer;
import java.util.*;

public class LayerComparator  implements Comparator<Layer> {

    public int compare(Layer obj1, Layer obj2) {
        return obj1.getName().compareTo(obj2.getName());
    }

}