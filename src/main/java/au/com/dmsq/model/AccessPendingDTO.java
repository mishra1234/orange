package au.com.dmsq.model;

public class AccessPendingDTO 
{
	Account account;
	String denyCode;
	String grantCode;
	
	public AccessPendingDTO(Account acct, String denycode, String grntCode)
	{
		this.account=acct;
		this.denyCode = denycode;
		this.grantCode = grntCode;
		
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public String getDenyCode() {
		return denyCode;
	}
	public void setDenyCode(String denyCode) {
		this.denyCode = denyCode;
	}
	public String getGrantCode() {
		return grantCode;
	}
	public void setGrantCode(String grantCode) {
		this.grantCode = grantCode;
	}
	

}
