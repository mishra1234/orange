package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "auth")
public class Auth implements java.io.Serializable {
	
	private static final long serialVersionUID = -3896174669911771810L;

	private Integer id;
	private Account account;
	private String token;
	private Long expiry;
	private String device;

	public Auth() {
	}

	public Auth(Account account) {
		this.account = account;
	}

	public Auth(Account account, String token, Long expiry, String device) {
		this.account = account;
		this.token = token;
		this.expiry = expiry;
		this.device = device;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account_id", nullable = false)
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Column(name = "token", length = 64)
	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Column(name = "expiry")
	public Long getExpiry() {
		return this.expiry;
	}

	public void setExpiry(Long expiry) {
		this.expiry = expiry;
	}

	@Column(name = "device", length = 4)
	public String getDevice() {
		return this.device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

}
