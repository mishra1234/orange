package au.com.dmsq.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherForecast {
	
	private String weatherData="";
	private String maxTemperature="0";
	private String minTemperature="0";
	private Date weatherDate;
	private String imgUrl="";
	
	public WeatherForecast()
	{
		weatherData="";
		maxTemperature="0";
		minTemperature="0";
		weatherDate=new Date();
		imgUrl="";
	}
	
	
	/**
	 * @return the maxTemperature
	 */
	public String getMaxTemperature() {
		return maxTemperature;
	}


	/**
	 * @param maxTemperature the maxTemperature to set
	 */
	public void setMaxTemperature(String maxTemperature) {
		this.maxTemperature = maxTemperature.replaceAll("\\D+","");
	}


	/**
	 * @return the minTemperature
	 */
	public String getMinTemperature() {
		return minTemperature;
	}


	/**
	 * @param minTemperature the minTemperature to set
	 */
	public void setMinTemperature(String minTemperature) {
		this.minTemperature = minTemperature.replaceAll("\\D+","");
	}


	/**
	 * @param weatherData
	 * @param maxTemperature
	 * @param minTemperature
	 * @param weatherDate
	 * @param imgUrl
	 */
	public WeatherForecast(String weatherData, String maxTemperature,
			String minTemperature, Date weatherDate, String imgUrl) {
		super();
		this.weatherData = weatherData;
		this.maxTemperature = maxTemperature;
		this.minTemperature = minTemperature;
		this.weatherDate = weatherDate;
		this.imgUrl = imgUrl;
	}
	/**
	 * @return the weatherData
	 */
	public String getWeatherData() {
		return weatherData;
	}
	/**
	 * @param weatherData the weatherData to set
	 */
	public void setWeatherData(String weatherData) {
		this.weatherData = weatherData;
		if(weatherData.contains(" "))
		{
			switch(weatherData.toLowerCase())
			{
				case  "possible shower":
					weatherData="light-showers";
					break;
				
				case  "partly cloudy":
					weatherData="partly-cloudy";
					break;
					
				default:
					weatherData="sunny";
					break;
			}

		}
		this.setImgUrl("http://www.bom.gov.au/images/symbols/large/"+weatherData.toLowerCase()+".png");
	}
	
	/**
	 * @return the weatherDate
	 */
	public Date getWeatherDate() {
		return weatherDate;
	}
	/**
	 * @param weatherDate the weatherDate to set
	 */
	public void setWeatherDate(Date weatherDate) {
		this.weatherDate = weatherDate;
	}
	/**
	 * @return the imgUrl
	 */
	public String getImgUrl() {
		return imgUrl;
	}
	/**
	 * @param imgUrl the imgUrl to set
	 */
	private void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WeatherForecast [weatherData=" + weatherData
				+ ", MaxTemperature=" + maxTemperature + ", MinTemperature="
				+ minTemperature + ", weatherDate=" + weatherDate + ", imgUrl="
				+ imgUrl + "]";
	}

	
	public static Date StringtoDate(String stringDate)
	{
		Date temp=new Date();

		
	    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	    try {
			temp =  df.parse(stringDate);
			System.out.println("new data is"+ temp.toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	    
	    
		return temp;
	}

}
