package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class RecommendedActivitiesId implements java.io.Serializable {

	private static final long serialVersionUID = 5364621524171715583L;

	private int accountId;
	private int activityId;
	private int recommendedActivityId;

	public RecommendedActivitiesId() {
	}

	public RecommendedActivitiesId(int accountId, int activityId,
			int recommendedActivityId) {
		this.accountId = accountId;
		this.activityId = activityId;
		this.recommendedActivityId = recommendedActivityId;
	}

	@Column(name = "account_id", nullable = false)
	public int getAccountId() {
		return this.accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	@Column(name = "activity_id", nullable = false)
	public int getActivityId() {
		return this.activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	@Column(name = "recommended_activity_id", nullable = false)
	public int getRecommendedActivityId() {
		return this.recommendedActivityId;
	}

	public void setRecommendedActivityId(int recommendedActivityId) {
		this.recommendedActivityId = recommendedActivityId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof RecommendedActivitiesId))
			return false;
		RecommendedActivitiesId castOther = (RecommendedActivitiesId) other;

		return (this.getAccountId() == castOther.getAccountId())
				&& (this.getActivityId() == castOther.getActivityId())
				&& (this.getRecommendedActivityId() == castOther
						.getRecommendedActivityId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getAccountId();
		result = 37 * result + this.getActivityId();
		result = 37 * result + this.getRecommendedActivityId();
		return result;
	}

}
