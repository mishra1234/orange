package au.com.dmsq.model;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;


@Entity
@Table(name = "activity")
public class Activity implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Account account; //Activity Owner
	private String contentTitle;
	private String contentBody;
	private String activityType;
	private String actionUrl;
	private String actionTitle;
	private Boolean recommended;
	private String iconUrl;
	private Boolean deleted;
	private SurveyActivity surveyActivity; //new winter2
	private Set<Booking> bookings = new HashSet<Booking>(0);
	private Event event;
	private Date createdDatetime;
	private Integer createdAccountId;
	private Integer ownerCommunity;
	private Set<File> files = new HashSet<File>(0);
	private Set<RecommendedActivities> recommendedActivitiesesForActivityId = new HashSet<RecommendedActivities>(0);
	private Set<RecommendedActivities> recommendedActivitiesesForRecommendedActivityId = new HashSet<RecommendedActivities>(0);
	private Set<Banner> banners = new HashSet<Banner>(0);
	private Set<Activity> activitiesForChildActivityId = new HashSet<Activity>(0);
	private Set<Layer> layers = new HashSet<Layer>(0);
	private Set<Message> messages = new HashSet<Message>(0);
	private Set<ActivityCalendar> activityCalendars = new HashSet<ActivityCalendar>(0);
	private Set<Account> accounts = new HashSet<Account>(0);
	private Set<Activity> activitiesForActivityId = new HashSet<Activity>(0);
	private Set<Layer> layers_1 = new HashSet<Layer>(0);
	
	// DTOS
	@Transient private transient String startDate = null;
	@Transient private transient String endDate = null;
	@Transient private transient String startTime = null;
	@Transient private transient String endTime = null;
	@Transient private transient String rangeTime = null;
	@Transient private transient String startDT = null;
	@Transient private transient String endDT = null;
	@Transient private transient String calData = null;
	@Transient private transient String month = "Jan";
	@Transient private transient String day = "01";


	public Activity() {
	}

	public Activity(Account account, boolean recommended, boolean deleted) {
		this.account = account;
		this.recommended = recommended;
		this.deleted = deleted;
	}

	public Activity(Account account, String contentTitle, String contentBody, String activityType,
			String actionUrl, String actionTitle, boolean recommended, String iconUrl, boolean deleted,
			SurveyActivity surveyActivity, Set<Booking> bookings, Event event, Date createdDatetime,
			Integer createdAccountId, Set<File> files, Set<RecommendedActivities> recommendedActivitiesesForActivityId,
			Set<RecommendedActivities> recommendedActivitiesesForRecommendedActivityId, Set<Banner> banners, 
			Set<Activity> activitiesForChildActivityId, Set<Layer> layers, Set<Message> messages, 
			Set<ActivityCalendar> activityCalendars, Set<Account> accounts, Set<Activity> activitiesForActivityId, 
			Set<Layer> layers_1)
	{
		this.account = account;
		this.contentTitle = contentTitle;
		this.contentBody = contentBody;
		this.activityType = activityType;
		this.actionUrl = actionUrl;
		this.actionTitle = actionTitle;
		this.recommended = recommended;
		this.iconUrl = iconUrl;
		this.deleted = deleted;
		this.surveyActivity=surveyActivity;
		this.bookings = bookings;
		this.event = event;
		this.createdDatetime = createdDatetime;
		this.createdAccountId = createdAccountId;
		this.files = files;
		this.recommendedActivitiesesForActivityId= recommendedActivitiesesForActivityId;
		this.recommendedActivitiesesForRecommendedActivityId=recommendedActivitiesesForRecommendedActivityId;
		this.banners = banners;
		this.activitiesForChildActivityId = activitiesForChildActivityId;
		this.layers = layers;
		this.messages = messages;
		this.activityCalendars = activityCalendars;
		this.accounts = accounts;
		this.activitiesForActivityId = activitiesForActivityId;
		this.layers_1 = layers_1;
	}
	
	public Activity(String contentTitle, String contentBody, String activityType,String iconUrl,	
			Account account, Set<File> files, Set<Banner> banners,Set<Layer> layers
	){
		this.account = account;
		this.contentTitle = contentTitle;
		this.contentBody = contentBody;
		this.activityType = activityType;
		this.iconUrl = iconUrl;
		this.files = files;
		this.banners = banners;
		this.layers = layers;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id", nullable = false)
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	//@NotEmpty(message = "Must not be blank.")
	//@Size(min=2, max=64, message="Must be between 8 and 64 characters!")
	@Column(name = "content_title", length = 255)
	public String getContentTitle() {
		return this.contentTitle;
	}

	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}

	//@NotEmpty(message = "Must not be blank.")
	@Size(min=0, max=65535, message="Must be minimum 8 characters!")
	@Column(name = "content_body", length = 65535, columnDefinition="text")
	public String getContentBody() {
		return this.contentBody;
	}

	public void setContentBody(String contentBody) {
		this.contentBody = contentBody;
	}

	//@NotEmpty(message = "Must not be blank.")
	//@Size(min=2, max=6, message="Must be between 4 and 6 characters!")
	@Column(name = "activity_type", length = 6)
	public String getActivityType() {
		return this.activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}


	@Column(name = "action_url")	// This var will be NULL as it is not required
	public String getActionUrl() {
		return this.actionUrl;
	}

	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}


	@Column(name = "action_title") // This var will be NULL as it is not required
	public String getActionTitle() {
		return this.actionTitle;
	}

	public void setActionTitle(String actionTitle) {
		this.actionTitle = actionTitle;
	}

	//@NotNull(message = "Must not be blank.")
	@Column(name = "recommended")
	public Boolean getRecommended() {
		return this.recommended;
	}

	public void setRecommended(Boolean recommended) {
		this.recommended = recommended;
	}


	@Column(name = "icon_url")
	public String getIconUrl() {
		return this.iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	@Column(name = "deleted")
	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "activity")
	public Set<Booking> getBookings() {
		return this.bookings;
	}

	public void setBookings(Set<Booking> bookings) {
		this.bookings = bookings;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "activity")
	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "activity_file_links", joinColumns = { 
	@JoinColumn(name = "activity_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
	@JoinColumn(name = "file_id", nullable = false, updatable = false) })
	public Set<File> getFiles() {
		return this.files;
	}

	public void setFiles(Set<File> files) {
		this.files = files;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "activityByActivityId")
	public Set<RecommendedActivities> getRecommendedActivitiesesForActivityId() {
		return this.recommendedActivitiesesForActivityId;
	}

	public void setRecommendedActivitiesesForActivityId(
			Set<RecommendedActivities> recommendedActivitiesesForActivityId) {
		this.recommendedActivitiesesForActivityId = recommendedActivitiesesForActivityId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "activityByRecommendedActivityId")
	public Set<RecommendedActivities> getRecommendedActivitiesesForRecommendedActivityId() {
		return this.recommendedActivitiesesForRecommendedActivityId;
	}

	public void setRecommendedActivitiesesForRecommendedActivityId(
			Set<RecommendedActivities> recommendedActivitiesesForRecommendedActivityId) {
		this.recommendedActivitiesesForRecommendedActivityId = recommendedActivitiesesForRecommendedActivityId;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "activity_banners", joinColumns = { @JoinColumn(name = "activity_id", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "banner_id", nullable = false, updatable = false) })
	public Set<Banner> getBanners() {
		return this.banners;
	}

	public void setBanners(Set<Banner> banners) {
		this.banners = banners;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "activity_activity_links", joinColumns = { @JoinColumn(name = "activity_id", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "child_activity_id", nullable = false, updatable = false) })
	public Set<Activity> getActivitiesForChildActivityId() {
		return this.activitiesForChildActivityId;
	}

	public void setActivitiesForChildActivityId(
			Set<Activity> activitiesForChildActivityId) {
		this.activitiesForChildActivityId = activitiesForChildActivityId;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "layer_activities", joinColumns = { 
			@JoinColumn(name = "activity_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
			@JoinColumn(name = "layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayers() {
		return this.layers;
	}

	public void setLayers(Set<Layer> layers) {
		this.layers = layers;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "message_activity_links", joinColumns = { 
			@JoinColumn(name = "activity_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
			@JoinColumn(name = "message_id", nullable = false, updatable = false) })
	public Set<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "activity")
	public Set<ActivityCalendar> getActivityCalendars() {
		return this.activityCalendars;
	}

	public void setActivityCalendars(Set<ActivityCalendar> activityCalendars) {
		this.activityCalendars = activityCalendars;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "activity_managers", joinColumns = { 
			@JoinColumn(name = "activity_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
			@JoinColumn(name = "account_id", nullable = false, updatable = false) })
	public Set<Account> getAccounts() {
		return this.accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "activity_activity_links", joinColumns = { 
			@JoinColumn(name = "child_activity_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
			@JoinColumn(name = "activity_id", nullable = false, updatable = false) })
	public Set<Activity> getActivitiesForActivityId() {
		return this.activitiesForActivityId;
	}

	public void setActivitiesForActivityId(Set<Activity> activitiesForActivityId) {
		this.activitiesForActivityId = activitiesForActivityId;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "activity_layer_links", joinColumns = { 
			@JoinColumn(name = "activity_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
			@JoinColumn(name = "layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayers_1() {
		return this.layers_1;
	}

	public void setLayers_1(Set<Layer> layers_1) {
		this.layers_1 = layers_1;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "activity")
	public SurveyActivity getSurveyActivity() {
		return surveyActivity;
	}

	public void setSurveyActivity(SurveyActivity surveyActivity) {
		this.surveyActivity = surveyActivity;
	}

	@Column(name = "created_datetime")
	public java.util.Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(java.util.Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Column(name = "created_account_id")
	public Integer getCreatedAccountId() {
		return createdAccountId;
	}

	public void setCreatedAccountId(Integer createdAccountId) {
		this.createdAccountId = createdAccountId;
	}

	@Transient
	public String getStartDate() {
		return startDate;
	}

	@Transient
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@Transient
	public String getEndDate() {
		return endDate;
	}

	@Transient
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Transient
	public String getRangeTime() {
		return rangeTime;
	}

	@Transient
	public void setRangeTime(String rangeTime) {
		this.rangeTime = rangeTime;
	}
	
	@Transient
	public String getStartDT() {
		return startDT;
	}

	@Transient
	public void setStartDT(String startDT) {
		this.startDT = startDT;
	}

	@Transient
	public String getEndDT() {
		return endDT;
	}

	@Transient
	public void setEndDT(String endDT) {
		this.endDT = endDT;
	}
	
	@Transient
	public String getCalData() {
		return calData;
	}

	@Transient
	public void setCalData(String calData) {
		this.calData = calData;
	}

	@Transient
	public String getStartTime() {
		return startTime;
	}

	@Transient
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Transient
	public String getEndTime() {
		return endTime;
	}

	@Transient
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	@Column(name = "owner_community")
	public Integer getOwnerCommunity() {
		return ownerCommunity;
	}

	public void setOwnerCommunity(Integer ownerCommunity) {
		this.ownerCommunity = ownerCommunity;
	}

	public static Comparator<Activity> ActivityComparator = new Comparator<Activity>() {

		public int compare(Activity act1, Activity act2) {

			if(act2.getId().compareTo(act1.getId())==0){
        		return 0;
        	}
        	return act2.getId().compareTo(act1.getId());
			
			//ascending order
			//return startDate2.compareTo(startDate1);
		}
	};

	
	public static Comparator<Activity> ActivityComparatorD = new Comparator<Activity>() {

		public int compare(Activity act1, Activity act2) {
			return act1.getStartDate().toString().compareTo(act2.getStartDate().toString());
        	//return act2.getStartDate().toString().compareTo(act1.getStartDate().toString());
		}
	};

	@Transient
	public String getMonth() {
		String temp = getStartDate(); 
		String temp2 [] = temp.split("-"); 
		if (temp2 != null && temp2.length == 3){
			return getMonthString(temp2[1]);
		}else{
			return "";
		}
		
	}
	
	@Transient
	private String getMonthString(String monthNumber){
		
		String month = "Jan"; 
		switch(monthNumber) {
		case "02": 
			month = "Feb";
			break; 
		case "03": 
			month = "Mar";
			break; 
		case "04": 
			month = "Apr";
			break; 
		case "05": 
			month = "May";
			break; 
		case "06": 
			month = "Jun";
			break; 
		case "07": 
			month = "Jul";
			break; 
		case "08": 
			month = "Ago";
			break; 
		case "09": 
			month = "Sep";
			break; 
		case "10": 
			month = "Oct";
			break; 
		case "11": 
			month = "Nov";
			break; 
		case "12": 
			month = "Dec";
			break; 
		}
		
		return month;
	}

	@Transient
	public void setMonth(String month) {
		this.month = month;
	}

	@Transient
	public String getDay() {
		String temp = getStartDate(); 
		String temp2 [] = temp.split("-"); 
		if (temp2 != null && temp2.length == 3){
			return temp2[0];
		}else {
			
			return "";
		}
		
	}

	@Transient
	public void setDay(String day) {
		this.day = day;
	}


}