package au.com.dmsq.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "date_range")
public class DateRange implements java.io.Serializable {

	private static final long serialVersionUID = 1563898723412214880L;
	
	private Integer id;
	private Long startDatetime;
	private Long endDatetime;
	private String displayDatetime;
	private Set<SurveyActivity> surveyActivities = new HashSet<SurveyActivity>(0);

	
	public DateRange() {
	}

	public DateRange(Long startDatetime, Long endDatetime,
			String displayDatetime, Set<SurveyActivity> surveyActivities) {
		this.startDatetime = startDatetime;
		this.endDatetime = endDatetime;
		this.displayDatetime = displayDatetime;
		this.surveyActivities = surveyActivities;
	}

	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "start_datetime")
	public Long getStartDatetime() {
		return this.startDatetime;
	}

	public void setStartDatetime(Long startDatetime) {
		this.startDatetime = startDatetime;
	}

	@Column(name = "end_datetime")
	public Long getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Long endDatetime) {
		this.endDatetime = endDatetime;
	}

	@Column(name = "display_datetime")
	public String getDisplayDatetime() {
		return this.displayDatetime;
	}

	public void setDisplayDatetime(String displayDatetime) {
		this.displayDatetime = displayDatetime;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "dateRange")
	public Set<SurveyActivity> getSurveyActivities() {
		return this.surveyActivities;
	}

	public void setSurveyActivities(Set<SurveyActivity> surveyActivities) {
		this.surveyActivities = surveyActivities;
	}

}