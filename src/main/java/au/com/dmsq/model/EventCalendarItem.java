package au.com.dmsq.model;

import java.util.Comparator;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "event_calendar_item")
public class EventCalendarItem implements java.io.Serializable {

	private static final long serialVersionUID = 7374270854136935910L;
	
	private EventCalendarItemId id;
	private Event event;
	private String calEtag;
	private String calData;
	private long dtend;
	
	// DTOS
	@Transient private transient String startDate = null;
	@Transient private transient String endDate = null;
	@Transient private transient String startTime = null;
	@Transient private transient String endTime = null;
	@Transient private transient String duration = null;

	public EventCalendarItem() {
	}

	public EventCalendarItem(EventCalendarItemId id, Event event) {
		this.id = id;
		this.event = event;
	}

	public EventCalendarItem(EventCalendarItemId id, Event event,
			String calEtag, String calData) {
		this.id = id;
		this.event = event;
		this.calEtag = calEtag;
		this.calData = calData;
		//this.setDtend(dtend);
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "activityId", column = @Column(name = "activity_id", nullable = false)),
			@AttributeOverride(name = "calHref", column = @Column(name = "cal_href", nullable = false)) })
	public EventCalendarItemId getId() {
		return this.id;
	}

	public void setId(EventCalendarItemId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "activity_id", nullable = false, insertable = false, updatable = false)
	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@Column(name = "cal_etag", length = 45)
	public String getCalEtag() {
		return this.calEtag;
	}

	public void setCalEtag(String calEtag) {
		this.calEtag = calEtag;
	}

	@Column(name = "cal_data", length = 65535, columnDefinition="text")
	public String getCalData() {
		return this.calData;
	}

	public void setCalData(String calData) {
		this.calData = calData;
	}

	@Column(name="dtend", length = 11)
	public long getDtend() {
		return dtend;
	}

	public void setDtend(long l) {
		this.dtend = l;
	}

	@Transient
	public String getStartDate() {
		return startDate;
	}

	@Transient
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@Transient
	public String getEndDate() {
		return endDate;
	}

	@Transient
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Transient
	public String getStartTime() {
		return startTime;
	}

	@Transient
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Transient
	public String getDuration() {
		return duration;
	}

	@Transient
	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Transient
	public String getEndTime() {
		return endTime;
	}

	@Transient
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public static Comparator<EventCalendarItem> ECIComparator = new Comparator<EventCalendarItem>() {

		public int compare(EventCalendarItem eci1, EventCalendarItem eci2) {

			if(eci1.getStartDate().compareTo(eci2.getStartDate())==0){
        		return 0;
        	}
        	return eci1.getStartDate().compareTo(eci2.getStartDate()); //Ascending Order
		}
	};

}

