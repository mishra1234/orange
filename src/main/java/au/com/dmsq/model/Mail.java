package au.com.dmsq.model;

import static javax.persistence.GenerationType.IDENTITY;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "mail")
public class Mail implements Serializable{

	private static final long serialVersionUID = -9128138935791521434L;

	private Integer id;
	private String from;
	private String to;
	private String subject;
	private String message;
	
	public Mail() {
	}

	public Mail(String from, String to, String subject, String message){
		this.setFrom(from);
		this.setTo(to);
		this.setSubject(subject);
		this.setMessage(message);
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "from", length = 45)
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@Column(name = "to", length = 45)
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@Column(name = "subject", length = 128)
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "message", length = 65535, columnDefinition="text")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
