package au.com.dmsq.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "category")
public class Category implements java.io.Serializable {

	private static final long serialVersionUID = -1293333422436930010L;

	private Integer id;
	private String name;
	private String iconUrl;
	private Boolean deleted;
	private String alias;
	private Set<Category> categoriesForChildCategoryId = new HashSet<Category>(0);
	private Set<Category> categoriesForParentCategoryId = new HashSet<Category>(0);
	private Set<Banner> banners = new HashSet<Banner>(0);
	public Set<Layer> layers = new HashSet<Layer>(0);
	
	// DTOS
	@Transient
	private transient int numberLayers = 0;
	@Transient
	private transient int numberEvents = 0; 
	@Transient
	private transient int connections = 0; 
	
	public Category() {
	}

	public Category(String name, String iconUrl, Boolean deleted, String alias,
			Set<Category> categoriesForChildCategoryId,
			Set<Category> categoriesForParentCategoryId, Set<Banner> banners,
			Set<Layer> layers) {
		this.name = name;
		this.iconUrl = iconUrl;
		this.deleted = deleted;
		this.alias = alias;
		this.categoriesForChildCategoryId = categoriesForChildCategoryId;
		this.categoriesForParentCategoryId = categoriesForParentCategoryId;
		this.banners = banners;
		this.layers = layers;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "icon_url")
	public String getIconUrl() {
		return this.iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	@Column(name = "deleted")
	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	@Column(name = "alias", length = 45)
	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "subcategories", joinColumns = { 
		@JoinColumn(name = "parent_category_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "child_category_id", nullable = false, updatable = false) })
	public Set<Category> getCategoriesForChildCategoryId() {
		return this.categoriesForChildCategoryId;
	}

	public void setCategoriesForChildCategoryId(
			Set<Category> categoriesForChildCategoryId) {
		this.categoriesForChildCategoryId = categoriesForChildCategoryId;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "subcategories", joinColumns = { 
		@JoinColumn(name = "child_category_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "parent_category_id", nullable = false, updatable = false) })
	public Set<Category> getCategoriesForParentCategoryId() {
		return this.categoriesForParentCategoryId;
	}

	public void setCategoriesForParentCategoryId(
			Set<Category> categoriesForParentCategoryId) {
		this.categoriesForParentCategoryId = categoriesForParentCategoryId;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "category_banner", joinColumns = { 
		@JoinColumn(name = "category_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "banner_id", nullable = false, updatable = false) })
	public Set<Banner> getBanners() {
		return this.banners;
	}

	public void setBanners(Set<Banner> banners) {
		this.banners = banners;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "layer_categories", joinColumns = {
		@JoinColumn(name = "category_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayers() {
		return this.layers;
	}

	@Transient
	public int getNumberLayers() {
		return numberLayers;
	}
	@Transient
	public void setNumberLayers(int numberLayers) {
		this.numberLayers = numberLayers;
	}

	public void setLayers(Set<Layer> layers) {
		this.layers = layers;
	}
	@Transient
	public int getNumberEvents() {
		return numberEvents;
	}
	@Transient
	public void setNumberEvents(int numberEvents) {
		this.numberEvents = numberEvents;
	}

	@Transient
	public int getConnections() {
		return connections;
	}

	@Transient
	public void setConnections(int connections) {
		this.connections = connections;
	}

}
