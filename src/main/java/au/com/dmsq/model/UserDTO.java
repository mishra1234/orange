package au.com.dmsq.model;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class UserDTO {
	
	@NotNull
    @NotEmpty
    private String firstName;
     
    @NotNull
    @NotEmpty
    private String lastName;
     
    @NotNull
    @NotEmpty
    private String username;
    
    
    public UserDTO() {
	}

    
    public UserDTO(String firstName,String lastName,String username) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username; //email
    }
    
    
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}
