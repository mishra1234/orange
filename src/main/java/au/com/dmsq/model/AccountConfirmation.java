package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account_confirmation")
public class AccountConfirmation implements java.io.Serializable {

	private static final long serialVersionUID = 7797255074300092422L;

	
	private Integer id;
	private Account account;
	private String confirmationCode;
	private Boolean confirmed;
	private Long requestDate;
	private Long confirmedDate;

	public AccountConfirmation() {
	}

	public AccountConfirmation(Account account) {
		this.account = account;
	}

	public AccountConfirmation(Account account, String confirmationCode,
			Boolean confirmed, Long requestDate, Long confirmedDate) {
		this.account = account;
		this.confirmationCode = confirmationCode;
		this.confirmed = confirmed;
		this.requestDate = requestDate;
		this.confirmedDate = confirmedDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account_id", nullable = false)
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Column(name = "confirmation_code", length = 40)
	public String getConfirmationCode() {
		return this.confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}

	@Column(name = "confirmed")
	public Boolean getConfirmed() {
		return this.confirmed;
	}

	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}

	@Column(name = "request_date")
	public Long getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Long requestDate) {
		this.requestDate = requestDate;
	}

	@Column(name = "confirmed_date")
	public Long getConfirmedDate() {
		return this.confirmedDate;
	}

	public void setConfirmedDate(Long confirmedDate) {
		this.confirmedDate = confirmedDate;
	}

}

