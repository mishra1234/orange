package au.com.dmsq.model;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;



@Entity
@Table(name = "invoice")
public class Invoice implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	private Integer 	id;
	private String 		reference;
	private Date 		inicialDate;
	private Date 		endDate;
	private Date 		creationDate;
	private Integer		amount;
	private Boolean		isPaid;
	private Integer 	discount;
	private Integer		promocode;
	private String  	client;
	private String 		clientAddress;
	private String 		clientPhone;
	private String  	clientEmail;
	private String  	clientAbn;
	private Set<Layer> 	layers = new HashSet<Layer>(0);


	public Invoice() {
	}

	public Invoice(Integer id, String reference) {
		this.id = id;
		this.reference=reference;
	}

	public Invoice(Integer id, String reference, Date inicialDate, Date endDate, Date creationDate,
				   Integer amount, Boolean isPaid, Integer discount, Integer	promocode, String client,
				   String clientAddress, String clientPhone, String clientEmail, String clientAbn,
				   Set<Layer> layers)
	{
		this.id = id;
		this.reference=reference;
		this.inicialDate=inicialDate;
		this.endDate=endDate;
		this.creationDate=creationDate;
		this.amount=amount;
		this.isPaid=isPaid;
		this.discount=discount;
		this.promocode=promocode;
		this.client=client;
		this.clientAddress=clientAddress;
		this.clientPhone=clientPhone;
		this.clientEmail=clientEmail;
		this.clientAbn=clientAbn;
		this.layers = layers;
	}
	

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "reference")
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Column(name = "inicial_date")
	public Date getInicialDate() {
		return inicialDate;
	}

	public void setInicialDate(Date inicialDate) {
		this.inicialDate = inicialDate;
	}

	@Column(name = "end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "creation_date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "amount")
	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	@Column(name = "is_paid")
	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

	@Column(name = "discount")
	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	@Column(name = "promocode")
	public Integer getPromocode() {
		return promocode;
	}

	public void setPromocode(Integer promocode) {
		this.promocode = promocode;
	}

	@Column(name = "client")
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	@Column(name = "client_address")
	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	@Column(name = "client_phone")
	public String getClientPhone() {
		return clientPhone;
	}

	public void setClientPhone(String clientPhone) {
		this.clientPhone = clientPhone;
	}

	@Column(name = "client_email")
	public String getClientEmail() {
		return clientEmail;
	}

	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}

	@Column(name = "client_abn")
	public String getClientAbn() {
		return clientAbn;
	}

	public void setClientAbn(String clientAbn) {
		this.clientAbn = clientAbn;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "invoice_layers", joinColumns = { 
			@JoinColumn(name = "invoice_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
			@JoinColumn(name = "layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayers() {
		return this.layers;
	}

	public void setLayers(Set<Layer> layers) {
		this.layers = layers;
	}

	


}