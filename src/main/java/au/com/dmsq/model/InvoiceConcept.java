package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;


@Entity
@Table(name = "invoice_concept")
public class InvoiceConcept implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	private Integer invoiceId;
	private Invoice invoice;
	private String 	concept;
	private Integer amount;
	private Integer unitCost;
	private Integer total;


	public InvoiceConcept() {
	}

	
	public InvoiceConcept(Invoice invoice) {
		this.invoice=invoice;
	}

	
	public InvoiceConcept(Invoice invoice, String concept, Integer amount, Integer unitCost,Integer total)
	{
		this.invoice=invoice;
		this.concept=concept;
		this.amount=amount;
		this.unitCost=unitCost;
		this.total=total;
	}
	

	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name= "property", value = "invoice"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "invoice_id", unique = true, nullable = false)
	public Integer getInvoiceId() {
		return this.invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	@Column(name = "concept")
	public String getConcept() {
		return concept;
	}

	public void setConcept(String concept) {
		this.concept = concept;
	}

	@Column(name = "amount")
	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	@Column(name = "unit_cost")
	public Integer getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Integer unitCost) {
		this.unitCost = unitCost;
	}

	@Column(name = "total")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}