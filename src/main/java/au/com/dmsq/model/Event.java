package au.com.dmsq.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "event")
public class Event implements java.io.Serializable {
	
	private static final long serialVersionUID = 6922373977181505983L;
	
	private Integer activityId;
	private Activity activity;
	private String address;
	private BigDecimal addressLat;
	private BigDecimal addressLng;
	private String contactEmail;
	private String contactName;
	private String contactPhone;
	private String organisation;
	private String cost;
	private String displayDatetime;
	private Boolean bookable;
	private Set<EventCalendarItem> eventCalendarItems = new HashSet<EventCalendarItem>(0);
	
	@Transient
	private String href;
	
	
	public Event() {
	}
	
	public Event(Activity activity) {
	}
	
	public Event(Activity activity, BigDecimal addressLat,
			BigDecimal addressLng, String contactPhone, boolean bookable) {
		this.activity = activity;
		this.addressLat = addressLat;
		this.addressLng = addressLng;
		this.contactPhone = contactPhone;
		this.bookable = bookable;
	}
	
	public Event(Activity activity, String address, BigDecimal addressLat,
			BigDecimal addressLng, String contactEmail, String contactName,
			String contactPhone, String organisation, String cost,
			String displayDatetime, boolean bookable,
			Set<EventCalendarItem> eventCalendarItems) {
		this.activity = activity;
		this.address = address;
		this.addressLat = addressLat;
		this.addressLng = addressLng;
		this.contactEmail = contactEmail;
		this.contactName = contactName;
		this.contactPhone = contactPhone;
		this.organisation = organisation;
		this.cost = cost;
		this.displayDatetime = displayDatetime;
		this.bookable = bookable;
		this.eventCalendarItems = eventCalendarItems;
	}
	
	public Event(String address, String contactEmail, String contactName,
			String contactPhone, String organisation, String cost
			){
		this.address = address;
		this.contactEmail = contactEmail;
		this.contactName = contactName;
		this.contactPhone = contactPhone;
		this.organisation = organisation;
		this.cost = cost;
	}
	
	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name= "property", value = "activity"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "activity_id", unique = true, nullable = false)
	public Integer getActivityId() {
		return this.activityId;
	}
	
	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}
	
	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	public Activity getActivity() {
		return this.activity;
	}
	
	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	
	//@NotEmpty(message = "Must not be blank.")
	//@Size(min=3, max=128, message="Must be between 8 and 128 characters!")
	@Column(name = "address")
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "address_lat", nullable = false, precision = 10, scale = 6)
	public BigDecimal getAddressLat() {
		return addressLat;
	}

	public void setAddressLat(BigDecimal addressLat) {
		this.addressLat = addressLat;
	}

	@Column(name = "address_lng", nullable = false, precision = 10, scale = 6)
	public BigDecimal getAddressLng() {
		return addressLng;
	}

	public void setAddressLng(BigDecimal addressLng) {
		this.addressLng = addressLng;
	}
	
	@Column(name = "contact_email")
	public String getContactEmail() {
		return this.contactEmail;
	}
	
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	
	@Column(name = "contact_name", length = 64)
	public String getContactName() {
		return this.contactName;
	}
	
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	
	@Column(name = "organisation")
	public String getOrganisation() {
		return this.organisation;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
	
	//@NotEmpty(message = "Must not be blank.")
	//@Size(min=1, max=64, message="Must be between 1 and 64 characters!")
	@Column(name = "cost", length = 64)
	public String getCost() {
		return this.cost;
	}
	
	public void setCost(String cost) {
		this.cost = cost;
	}
	
	//@NotEmpty(message = "Must not be blank.")
	//@Size(min=8, max=64, message="Must be between 8 and 64 characters!")
	@Column(name = "display_datetime")
	public String getDisplayDatetime() {
		return this.displayDatetime;
	}
	
	public void setDisplayDatetime(String displayDatetime) {
		this.displayDatetime = displayDatetime;
	}
	
	@Column(name = "contact_phone", length = 48)
	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	//@NotNull(message = "Must not be blank.")
	@Column(name = "bookable")
	public Boolean getBookable() {
		return bookable;
	}

	public void setBookable(Boolean bookable) {
		this.bookable = bookable;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "event")
	public Set<EventCalendarItem> getEventCalendarItems() {
		return this.eventCalendarItems;
	}

	public void setEventCalendarItems(Set<EventCalendarItem> eventCalendarItems) {
		this.eventCalendarItems = eventCalendarItems;
	}

	@Transient
	public String getHref() {
		return href;
	}

	@Transient
	public void setHref(String href) {
		this.href = href;
	}
	
}
