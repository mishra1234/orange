package au.com.dmsq.model;

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "account_layer_access")
public class AccountLayerAccess implements java.io.Serializable{

	private static final long serialVersionUID = 1337856084195652214L;
	
	private AccountLayerAccessId id;
	private Account account;
	private Layer layer;
	private String status;
	private Date lastStatusUpdate;
	private String grantAccessCode;
	private String denyAccessCode;

	public AccountLayerAccess() {
	}

	public AccountLayerAccess(AccountLayerAccessId id, Account account, Layer layer) {
		this.id = id;
		this.account = account;
		this.layer = layer;
	}

	public AccountLayerAccess(AccountLayerAccessId id, Account account,
			Layer layer, String status, Date lastStatusUpdate,
			String grantAccessCode, String denyAccessCode) {
		this.id = id;
		this.account = account;
		this.layer = layer;
		this.status = status;
		this.lastStatusUpdate = lastStatusUpdate;
		this.grantAccessCode = grantAccessCode;
		this.denyAccessCode = denyAccessCode;
	}

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "accountId", column = @Column(name = "account_id", nullable = false)),
		@AttributeOverride(name = "layerId", column = @Column(name = "layer_id", nullable = false)) })
	public AccountLayerAccessId getId() {
		return this.id;
	}

	public void setId(AccountLayerAccessId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account_id", nullable = false, insertable = false, updatable = false)
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "layer_id", nullable = false, insertable = false, updatable = false)
	public Layer getLayer() {
		return this.layer;
	}

	public void setLayer(Layer layer) {
		this.layer = layer;
	}

	@Column(name = "status", length = 9)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_status_update", length = 19)
	public Date getLastStatusUpdate() {
		return this.lastStatusUpdate;
	}

	public void setLastStatusUpdate(Date lastStatusUpdate) {
		this.lastStatusUpdate = lastStatusUpdate;
	}

	@Column(name = "grant_access_code", length = 45)
	public String getGrantAccessCode() {
		return this.grantAccessCode;
	}

	public void setGrantAccessCode(String grantAccessCode) {
		this.grantAccessCode = grantAccessCode;
	}

	@Column(name = "deny_access_code", length = 45)
	public String getDenyAccessCode() {
		return this.denyAccessCode;
	}

	public void setDenyAccessCode(String denyAccessCode) {
		this.denyAccessCode = denyAccessCode;
	}

}

