package au.com.dmsq.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "activity_payment")
public class ActivityPayment implements java.io.Serializable {

	private static final long serialVersionUID = -4422206932344665557L;
	private Integer id;
	private Activity activity;
	private Account account;
	private Date date;
	private Integer amount;
	private String response;
	
	public ActivityPayment() {
	}

	public ActivityPayment(Activity activity, Account account) {
		this.activity = activity;
		this.account = account;
	}

	public ActivityPayment(Activity activity, Account account, Date date,
			Integer amount, String response) 
	{
		this.activity = activity;
		this.account = account;
		this.date = date;
		this.amount = amount;
		this.response = response;
	}
	

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "activity_id", nullable = false)
	public Activity getActivity() {
		return this.activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account_id", nullable = false)
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Column(name = "date")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "amount")
	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	@Column(name = "response", length = 65535, columnDefinition="text")
	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	

}

