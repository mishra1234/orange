package au.com.dmsq.model;

import static javax.persistence.GenerationType.IDENTITY;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "catalogue")
public class Catalogue {
	private static final long serialVersionUID = -4884952787236687640L;
	
	private Integer id;
	private String itemName;
	private Double price;
	private int perAmount;
	private Date dateCreated;
	
	 public Catalogue() {
		
	}

	public Catalogue(String itemName, Double price, int perAmount, Date dateCreated) {
		super();
		this.itemName = itemName;
		this.price = price;
		this.perAmount = perAmount;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "item_name")
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@Column(name = "price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	@Column(name = "per_amount")
	public int getPerAmount() {
		return perAmount;
	}

	public void setPerAmount(int perAmount) {
		this.perAmount = perAmount;
	}
	@Column(name = "date_created")
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
}
