package au.com.dmsq.model;

import java.util.Date;

public class TimeData{
	 
	 private double value;
	 private Date timeDate;
	 
	 TimeData()
	 {
		 
	 }

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}

	/**
	 * @return the timeDate
	 */
	public Date getTimeDate() {
		return timeDate;
	}

	/**
	 * @param timeDate the timeDate to set
	 */
	public void setTimeDate(Date timeDate) {
		this.timeDate = timeDate;
	}	
}

