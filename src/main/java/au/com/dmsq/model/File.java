package au.com.dmsq.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "file")
public class File implements java.io.Serializable {
	
	private static final long serialVersionUID = -4884952787236687640L;
	
	private Integer id;
	private String title;
	private String description;
	private String url;
	private String iconUrl;
	private String type;
	private Set<Activity> activities = new HashSet<Activity>(0);

	public File() {
	}

	public File(String title, String description, String url, String iconUrl,
			Set<Activity> activities) {
		this.title = title;
		this.description = description;
		this.url = url;
		this.iconUrl = iconUrl;
		this.activities = activities;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "title", length = 64)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "description", length = 65535, columnDefinition="text")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "url")
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "icon_url")
	public String getIconUrl() {
		return this.iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "activity_file_links", joinColumns = { 
		@JoinColumn(name = "file_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "activity_id", nullable = false, updatable = false) })
	public Set<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(Set<Activity> activities) {
		this.activities = activities;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

