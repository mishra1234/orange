package au.com.dmsq.model;

public class EventDTO {
	
	private String title;
	private String start;
	private String end;
	private boolean allday; 
	
	
	public boolean isAllday() {
		return allday;
	}
	public void isAllDay(boolean allday) {
		this.allday = allday;
	}
	private Integer id;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	

}
