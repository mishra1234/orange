package au.com.dmsq.model;

public class AccessDTO {

	public AccessDTO(Account account2, String denyAccessCode) {
		
		this.account = account2;
		this.code = denyAccessCode;
		
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	private Account account; 
	private String code; 
	
	
	
}
