package au.com.dmsq.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


@Entity
@Table(name = "child_layers")
public class ChildLayers implements Serializable 
{
	private static final long serialVersionUID = -1972160438641330324L;

	@Id @ManyToOne( cascade = { CascadeType.ALL })
	@NotFound(action = NotFoundAction.IGNORE)
	@PrimaryKeyJoinColumn(name="parentLayerId")
	private Layer parent;
	
	@Id @OneToMany (fetch = FetchType.LAZY, mappedBy = "childLayerId")
	@NotFound(action = NotFoundAction.IGNORE)
	private List<Layer> childs;

	public Layer getParent() {
		return parent;
	}

	public void setParent(Layer parent) {
		this.parent = parent;
	}

	public List<Layer> getChilds() {
		return childs;
	}

	public void setChilds(List<Layer> childs) {
		this.childs = childs;
	}
	
	
}

	
	