package au.com.dmsq.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "message")
public class Message implements java.io.Serializable {

	private static final long serialVersionUID = -509889644994074270L;

	private Integer id;
	private Account accountByFromId;
	private Account accountByToId;
	private String subject;
	private String message;
	private Long dateSent;
	private Boolean isRead;
	private Boolean deleted;
	private Integer layerId;  //target
	private Integer activityId;  // target
	private Set<Banner> banners = new HashSet<Banner>(0);
	private Set<Layer> layers = new HashSet<Layer>(0);
	private Set<Activity> activities = new HashSet<Activity>(0);
	
	@Transient
	private String formattedDate;

	
	public Message() {
	}

	public Message(Account accountByFromId, Account accountByToId) {
		this.accountByFromId = accountByFromId;
		this.accountByToId = accountByToId;
	}

	public Message(Account accountByFromId, Account accountByToId,
			String subject, String message, Long dateSent, Boolean isRead,
			Boolean deleted, Integer layerId, Integer activityId,
			Set<Banner> banners, Set<Layer> layers, Set<Activity> activities) {
		this.accountByFromId = accountByFromId;
		this.accountByToId = accountByToId;
		this.subject = subject;
		this.message = message;
		this.dateSent = dateSent;
		this.isRead = isRead;
		this.deleted = deleted;
		this.layerId = layerId;
		this.activityId = activityId;
		this.banners = banners;
		this.layers = layers;
		this.activities = activities;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "from_id", nullable = false)
	public Account getAccountByFromId() {
		return this.accountByFromId;
	}

	public void setAccountByFromId(Account accountByFromId) {
		this.accountByFromId = accountByFromId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "to_id", nullable = false)
	public Account getAccountByToId() {
		return this.accountByToId;
	}

	public void setAccountByToId(Account accountByToId) {
		this.accountByToId = accountByToId;
	}

	//@NotEmpty
	//@Size(min=8, max=255, message="Lenght must be minimum 8 characters!")
	@Column(name = "subject", length = 255)
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	//@NotEmpty
	//@Size(min=8, max=65535, message="Lenght must be minimum 8 characters!")
	@Column(name = "message", length = 65535, columnDefinition="text")
	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "date_sent")
	public Long getDateSent() {
		return this.dateSent;
	}

	public void setDateSent(Long dateSent) {
		this.dateSent = dateSent;
	}

	@Column(name = "is_read")
	public Boolean getIsRead() {
		return this.isRead;
	}

	public void setIsRead(Boolean read) {
		this.isRead = read;
	}

	@Column(name = "deleted")
	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
	public Integer getLayerId() {
		return layerId;
	}

	public void setLayerId(Integer layerId) {
		this.layerId = layerId;
	}

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "message_banner", 
		joinColumns = { @JoinColumn(name = "message_id", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "banner_id", nullable = false, updatable = false) })
	public Set<Banner> getBanners() {
		return this.banners;
	}

	public void setBanners(Set<Banner> banners) {
		this.banners = banners;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "message_layer_links", 
		joinColumns = { @JoinColumn(name = "message_id", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayers() {
		return this.layers;
	}

	public void setLayers(Set<Layer> layers) {
		this.layers = layers;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "message_activity_links", 
		joinColumns = { @JoinColumn(name = "message_id", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "activity_id", nullable = false, updatable = false) })
	public Set<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(Set<Activity> activities) {
		this.activities = activities;
	}

	/**
	 * @return the formattedDate
	 */
	@Transient
	public String getFormattedDate() {
		return formattedDate;
	}

	/**
	 * @param formattedDate the formattedDate to set
	 */
	@Transient
	public void setFormattedDate(String formattedDate) {
		this.formattedDate = formattedDate;
	}
	
}
