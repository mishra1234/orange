package au.com.dmsq.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "subcategories")
public class Subcategory implements Serializable {

	private static final long serialVersionUID = -1293333422436930010L;

	@Id @OneToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "parent_category_id")
	private Category parentCategory; 
	
	@Id @OneToOne
	@JoinColumn(name = "child_category_id")
	private Category childCategory;

	
	public Category getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(Category parentCategory) {
		this.parentCategory = parentCategory;
	}

	public Category getChildCategory() {
		return childCategory;
	}

	public void setChildCategory(Category childCategory) {
		this.childCategory = childCategory;
	}
	
	

	
}
