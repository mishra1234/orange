package au.com.dmsq.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "recommended_activities")
public class RecommendedActivities implements java.io.Serializable {

	private static final long serialVersionUID = 4953534847095000688L;

	private RecommendedActivitiesId id;
	private Activity activityByActivityId;
	private Account account;
	private Activity activityByRecommendedActivityId;

	public RecommendedActivities() {
	}

	public RecommendedActivities(RecommendedActivitiesId id,
			Activity activityByActivityId, Account account,
			Activity activityByRecommendedActivityId) {
		this.id = id;
		this.activityByActivityId = activityByActivityId;
		this.account = account;
		this.activityByRecommendedActivityId = activityByRecommendedActivityId;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "accountId", column = @Column(name = "account_id", nullable = false)),
			@AttributeOverride(name = "activityId", column = @Column(name = "activity_id", nullable = false)),
			@AttributeOverride(name = "recommendedActivityId", column = @Column(name = "recommended_activity_id", nullable = false)) })
	public RecommendedActivitiesId getId() {
		return this.id;
	}

	public void setId(RecommendedActivitiesId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "activity_id", nullable = false, insertable = false, updatable = false)
	public Activity getActivityByActivityId() {
		return this.activityByActivityId;
	}

	public void setActivityByActivityId(Activity activityByActivityId) {
		this.activityByActivityId = activityByActivityId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account_id", nullable = false, insertable = false, updatable = false)
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "recommended_activity_id", nullable = false, insertable = false, updatable = false)
	public Activity getActivityByRecommendedActivityId() {
		return this.activityByRecommendedActivityId;
	}

	public void setActivityByRecommendedActivityId(
			Activity activityByRecommendedActivityId) {
		this.activityByRecommendedActivityId = activityByRecommendedActivityId;
	}

}

