package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AccountLayerAccessId implements java.io.Serializable{

	private static final long serialVersionUID = 5163260859648902533L;

	private int accountId;
	private int layerId;

	public AccountLayerAccessId() {
	}

	public AccountLayerAccessId(int accountId, int layerId) {
		this.accountId = accountId;
		this.layerId = layerId;
	}

	@Column(name = "account_id", nullable = false)
	public int getAccountId() {
		return this.accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	@Column(name = "layer_id", nullable = false)
	public int getLayerId() {
		return this.layerId;
	}

	public void setLayerId(int layerId) {
		this.layerId = layerId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AccountLayerAccessId))
			return false;
		AccountLayerAccessId castOther = (AccountLayerAccessId) other;

		return (this.getAccountId() == castOther.getAccountId())
				&& (this.getLayerId() == castOther.getLayerId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getAccountId();
		result = 37 * result + this.getLayerId();
		return result;
	}

}

