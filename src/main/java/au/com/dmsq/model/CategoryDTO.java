package au.com.dmsq.model;


public class CategoryDTO {
	
	Category category; 
	int subcategories; 
	int connections; 
	int activities;
	
	
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public int getSubcategories() {
		return subcategories;
	}
	public void setSubcategories(int subcategories) {
		this.subcategories = subcategories;
	}
	public int getConnections() {
		return connections;
	}
	public void setConnections(int connections) {
		this.connections = connections;
	}
	public int getActivities() {
		return activities;
	}
	public void setActivities(int activities) {
		this.activities = activities;
	} 
	
	

}
