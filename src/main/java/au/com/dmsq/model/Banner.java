package au.com.dmsq.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

@Entity
@Table(name = "banner")
public class Banner implements java.io.Serializable {
	
	private static final long serialVersionUID = -5433974363970288635L;
	
	
	private Integer id;
	private String imageUrl;
	private String title;
	private String subtitle;
	private Set<Category> categories = new HashSet<Category>(0);
	private Set<Message> messages = new HashSet<Message>(0);
	private Set<Activity> activities = new HashSet<Activity>(0);
	private Set<Layer> layers = new HashSet<Layer>(0);

	
	public Banner() {
	}

	public Banner(String imageUrl, String title, String subtitle,
			Set<Category> categories, Set<Message> messages,
			Set<Activity> activities, Set<Layer> layers) {
		this.imageUrl = imageUrl;
		this.title = title;
		this.subtitle = subtitle;
		this.categories = categories;
		this.messages = messages;
		this.activities = activities;
		this.layers = layers;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	//@URL
	//@NotEmpty
	//@Size(min=8, max=128, message="Lenght must be minimum 8 characters!")
	@Column(name = "image_url")
	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	//@NotEmpty
	@Column(name = "title", length = 64)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	//@NotEmpty
	@Column(name = "subtitle", length = 64)
	public String getSubtitle() {
		return this.subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "category_banner", joinColumns = { 
		@JoinColumn(name = "banner_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "category_id", nullable = false, updatable = false) })
	public Set<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "message_banner", joinColumns = { 
		@JoinColumn(name = "banner_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "message_id", nullable = false, updatable = false) })
	public Set<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "activity_banners", joinColumns = { 
		@JoinColumn(name = "banner_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "activity_id", nullable = false, updatable = false) })
	public Set<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(Set<Activity> activities) {
		this.activities = activities;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "layer_banners", joinColumns = { 
		@JoinColumn(name = "banner_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayers() {
		return this.layers;
	}

	public void setLayers(Set<Layer> layers) {
		this.layers = layers;
	}

}
