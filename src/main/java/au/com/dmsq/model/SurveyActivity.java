package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import au.com.dmsq.model.DateRange;

@Entity
@Table(name = "survey_activity")
public class SurveyActivity implements java.io.Serializable {

	private static final long serialVersionUID = -9119506981938573336L;

	private int activityId;
	private Activity activity;
	private DateRange dateRange;
	private String url;

	
	public SurveyActivity() {
	}

	public SurveyActivity(Activity activity) {
		this.activity = activity;
	}

	public SurveyActivity(Activity activity, DateRange dateRange, String url) {
		this.activity = activity;
		this.dateRange = dateRange;
		this.url = url;
	}

	
	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "activity"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "activity_id", unique = true, nullable = false)
	public int getActivityId() {
		return this.activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	public Activity getActivity() {
		return this.activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "active_date_range_id")
	public DateRange getDateRange() {
		return this.dateRange;
	}

	public void setDateRange(DateRange dateRange) {
		this.dateRange = dateRange;
	}

	@Column(name = "url")
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}

