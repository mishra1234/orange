package au.com.dmsq.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.CascadeType;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

@Entity
@Table(name = "account")
public class Account implements Serializable, Comparable<Account> {

	private static final long serialVersionUID = -5936573979264201807L;


	//  Attributes 
	private Integer id;
	private Role role;
	private String name;
	private String email;
	private String password;
	private String salt;
	private String timezone;
	private String firstName;
	private String lastName;
	private Integer postcode;
	private boolean enabled;
	private String status;
	private String resetCode;
	private String facebookUid;
	private String googleUid;
	private String imageUrl;
	private boolean subscribe;
	private Integer platform;
	private String address;
	private String mobilephone;
	private String landline;
	private Date newUserDate;
	private String alternateEmail;
	private String ccToken;
	
	// Relationships 
	private Set<Activity> activities = new HashSet<Activity>(0);
	private Set<ActivityCalendar> activityCalendars = new HashSet<ActivityCalendar>(0); //new relationship winter2
	private Set<Auth> auths = new HashSet<Auth>(0);
	private Set<RecommendedActivities> recommendedActivitieses = new HashSet<RecommendedActivities>(0);
	private Set<Activity> activities_1 = new HashSet<Activity>(0);
	private Set<AccountLayerAccess> accountLayerAccesses = new HashSet<AccountLayerAccess>(0);	//new relationship winter2
	private Set<Layer> layers = new HashSet<Layer>(0);
	private Set<Message> messagesForFromId = new HashSet<Message>(0);
	private Set<Booking> bookings = new HashSet<Booking>(0);  //new relationship in winter2	
	private Set<Layer> layers_1 = new HashSet<Layer>(0);	
	private Set<AccountConfirmation> accountConfirmations = new HashSet<AccountConfirmation>(0); //new relationship winter2	
	private Set<Layer> layers_2 = new HashSet<Layer>(0);
	private Set<Message> messagesForToId = new HashSet<Message>(0);	
	

	// Constructors 
	public Account() {
	}

	public Account(Role role, String facebookUid, String googleUid) {
		this.role = role;
		this.facebookUid = facebookUid;
		this.googleUid = googleUid;
	}

	public Account(Role role, String name, String email, String password,
			String salt, String timezone, String firstName, String lastName,
			Integer postcode, Boolean enabled, String status, String resetCode,
			String facebookUid, String googleUid, Set<Activity> activities,
			Set<ActivityCalendar> activityCalendars, Set<Auth> auths,
			Set<RecommendedActivities> recommendedActivitieses,
			Set<Activity> activities_1, Set<AccountLayerAccess> accountLayerAccesses, 
			Set<Layer> layers, Set<Message> messagesForFromId, Set<Booking> bookings,
			Set<Layer> layers_1, Set<AccountConfirmation> accountConfirmations,
			Set<Layer> layers_2, Set<Message> messagesForToId, String imageUrl,
			Boolean subscribe, Integer platform, String address, String mobilephone,
			String landline, String alternateEmail
			) {
		this.role = role;
		this.name = name;
		this.email = email;
		this.password = password;
		this.salt = salt;
		this.timezone = timezone;
		this.firstName = firstName;
		this.lastName = lastName;
		this.postcode = postcode;
		this.enabled = enabled;
		this.status = status;
		this.resetCode = resetCode;
		this.facebookUid = facebookUid;
		this.googleUid = googleUid;
		this.activities = activities;
		this.activityCalendars = activityCalendars;
		this.auths = auths;
		this.recommendedActivitieses = recommendedActivitieses;
		this.activities_1 = activities_1;
		this.accountLayerAccesses=  accountLayerAccesses;
		this.layers = layers;
		this.messagesForFromId = messagesForFromId;
		this.bookings = bookings;
		this.layers_1 = layers_1;
		this.accountConfirmations = accountConfirmations;
		this.layers_2 = layers_2;
		this.messagesForToId = messagesForToId;
		this.imageUrl = imageUrl;
		this.subscribe= subscribe;
		this.platform= platform;
		this.address= address;
		this.mobilephone= mobilephone;
		this.landline= landline;
		this.alternateEmail = alternateEmail;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull(message = "must not be blank.")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id", nullable = false)
	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@NotEmpty(message = "Name must not be blank.")
	@Size(min=2, max=128, message="Length must be between 2 and 128 characters!")
	@Column(name = "name", length = 128)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Email
	@NotEmpty(message = "email must not be blank.")
	@Size(min=8, max=128, message="Lenght must be email format")
	@Column(name = "email", length = 128)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@NotEmpty(message = "password must not be blank.")
	@Size(min=6, max=128, message="Lenght must be minimum 8 characters!")
	@Column(name = "password", length = 40)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "salt", length = 40)
	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@NotEmpty(message = "timezone must not be blank.")
	@Column(name = "timezone", length = 45)
	public String getTimezone() {
		return this.timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@NotEmpty(message = "First Name must not be blank.")
	@Size(min=2, max=45, message="Lenght must be minimum 2 characters!")
	@Column(name = "first_name", length = 45)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@NotEmpty(message = "Last Name must not be blank.")
	@Size(min=2, max=45, message="Lenght must be minimum 2 characters!")
	@Column(name = "last_name", length = 45)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@NotNull(message = "Postcode must not be blank.")
	@Min(value=999, message="Postcode needs to be a number and bigger than 999")
	@NumberFormat(style=Style.NUMBER)
	@Column(name = "postcode")
	public Integer getPostcode() {
		return this.postcode;
	}

	public void setPostcode(Integer postcode) {
		this.postcode = postcode;
	}

	@Column(name = "enabled")
	public boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@NotEmpty(message = "status must not be blank.")
	@Size(min=5, max=12, message="Lenght must be minimum 2 characters!")
	@Column(name = "status", length = 12)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "reset_code", length = 40)
	public String getResetCode() {
		return this.resetCode;
	}

	public void setResetCode(String resetCode) {
		this.resetCode = resetCode;
	}
	
	
	@Column (name = "facebook_uid", length = 255)
	public String getFacebookUid(){
		return this.facebookUid;		
	}
	
	public void setFacebookUid(String facebookUid) {
		this.facebookUid = facebookUid;
	}
	
	
	@Column (name = "google_uid", length = 255)
	public String getGoogleUid(){
		return this.googleUid;		
	}
	
	public void setGoogleUid(String googleUid) {
		this.googleUid = googleUid;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "activity_managers", joinColumns = { 
	@JoinColumn(name = "account_id", nullable = false, updatable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "activity_id", nullable = false, updatable = false) })
	public Set<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(Set<Activity> activities) {
		this.activities = activities;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<ActivityCalendar> getActivityCalendars() {
		return this.activityCalendars;
	}

	public void setActivityCalendars(Set<ActivityCalendar> activityCalendars) {
		this.activityCalendars = activityCalendars;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<Auth> getAuths() {
		return this.auths;
	}

	public void setAuths(Set<Auth> auths) {
		this.auths = auths;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<RecommendedActivities> getRecommendedActivitieses() {
		return recommendedActivitieses;
	}

	public void setRecommendedActivitieses(Set<RecommendedActivities> recommendedActivitieses) {
		this.recommendedActivitieses = recommendedActivitieses;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<Activity> getActivities_1() {
		return this.activities_1;
	}

	public void setActivities_1(Set<Activity> activities_1) {
		this.activities_1 = activities_1;
	}

	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "account")
	public Set<AccountLayerAccess> getAccountLayerAccesses() {
		return accountLayerAccesses;
	}

	public void setAccountLayerAccesses(Set<AccountLayerAccess> accountLayerAccesses) {
		this.accountLayerAccesses = accountLayerAccesses;
	}
	
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "layer_managers", joinColumns = { 
		@JoinColumn(name = "account_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayers() {
		return this.layers;
	}

	public void setLayers(Set<Layer> layers) {
		this.layers = layers;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountByFromId")
	public Set<Message> getMessagesForFromId() {
		return this.messagesForFromId;
	}

	public void setMessagesForFromId(Set<Message> messagesForFromId) {
		this.messagesForFromId = messagesForFromId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<Booking> getBookings() {
		return this.bookings;
	}

	public void setBookings(Set<Booking> bookings) {
		this.bookings = bookings;
	}

	@ManyToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name = "connected_layers", joinColumns = { 
		@JoinColumn(name = "account_id", nullable = false, updatable = false,insertable=true) }, inverseJoinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false,insertable=true) })
	public Set<Layer> getLayers_1() {
		return this.layers_1;
	}

	public void setLayers_1(Set<Layer> layers_1) {
		this.layers_1 = layers_1;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<AccountConfirmation> getAccountConfirmations() {
		return this.accountConfirmations;
	}

	public void setAccountConfirmations(
			Set<AccountConfirmation> accountConfirmations) {
		this.accountConfirmations = accountConfirmations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<Layer> getLayers_2() {
		return this.layers_2;
	}

	public void setLayers_2(Set<Layer> layers_2) {
		this.layers_2 = layers_2;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountByToId")
	public Set<Message> getMessagesForToId() {
		return this.messagesForToId;
	}

	public void setMessagesForToId(Set<Message> messagesForToId) {
		this.messagesForToId = messagesForToId;
	}

	@Column (name = "image_url", length = 255)
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Column
	public boolean getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(boolean subscribe) {
		this.subscribe = subscribe;
	}

	@Column 
	public Integer getPlatform() {
		return platform;
	}

	public void setPlatform(Integer platform) {
		this.platform = platform;
	}

	@Column 
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column 
	public String getMobilephone() {
		return mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	@Column 
	public String getLandline() {
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	@Override
	public int compareTo(Account o) {
		// TODO Auto-generated method stub
		if(this.equals(o))
			{
			return 0;
			}
		else if(this.id<o.id)
		{
			return -1;
		}
		return 1;
	}


	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Account)) {
			return false;
		}
		Account other = (Account) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Column(name = "newuser_date")
	public Date getNewUserDate() {
		return newUserDate;
	}

	public void setNewUserDate(Date newUserDate) {
		this.newUserDate = newUserDate;
	}

	@Column(name = "alternate_email")
	public String getAlternateEmail() {
		return alternateEmail;
	}

	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}

	@Column(name = "cc_token")
	public String getCcToken() {
		return ccToken;
	}

	public void setCcToken(String ccToken) {
		this.ccToken = ccToken;
	}
}
