package au.com.dmsq.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "social_sign_on")
public class SocialSignOn implements java.io.Serializable {

	private static final long serialVersionUID = -5269713220171730005L;
	
	private SocialSignOnId id;
	private String email;
	private String accessToken;
	private Long expires;
	private String refreshToken;
	private String nickname;
	private String name;
	private String firstName;
	private String lastName;
	private String location;
	private String description;
	private String imageUrl;
	private String urls;

	public SocialSignOn() {
	}

	public SocialSignOn(SocialSignOnId id, String email) {
		this.id = id;
		this.email = email;
	}

	public SocialSignOn(SocialSignOnId id, String email, String accessToken,
			Long expires, String refreshToken, String nickname, String name,
			String firstName, String lastName, String location,
			String description, String imageUrl, String urls) {
		this.id = id;
		this.email = email;
		this.accessToken = accessToken;
		this.expires = expires;
		this.refreshToken = refreshToken;
		this.nickname = nickname;
		this.name = name;
		this.firstName = firstName;
		this.lastName = lastName;
		this.location = location;
		this.description = description;
		this.imageUrl = imageUrl;
		this.urls = urls;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "uid", column = @Column(name = "uid", nullable = false)),
			@AttributeOverride(name = "service", column = @Column(name = "service", nullable = false, length = 9)) })
	public SocialSignOnId getId() {
		return this.id;
	}

	public void setId(SocialSignOnId id) {
		this.id = id;
	}

	@Column(name = "email", nullable = false)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "access_token", length = 65535, columnDefinition="text")
	public String getAccessToken() {
		return this.accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@Column(name = "expires")
	public Long getExpires() {
		return this.expires;
	}

	public void setExpires(Long expires) {
		this.expires = expires;
	}

	@Column(name = "refresh_token", length = 65535, columnDefinition="text")
	public String getRefreshToken() {
		return this.refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@Column(name = "nickname")
	public String getNickname() {
		return this.nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "location")
	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(name = "description", length = 65535, columnDefinition="text")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "image_url")
	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Column(name = "urls", length = 65535, columnDefinition="text")
	public String getUrls() {
		return this.urls;
	}

	public void setUrls(String urls) {
		this.urls = urls;
	}

}
