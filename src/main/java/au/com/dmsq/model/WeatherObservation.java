package au.com.dmsq.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherObservation {
	
	WeatherObservation()
	{
		
	}
	
	public WeatherObservation(String location)
	{
		this.setLocation(location);
	}
	
	WeatherObservation(String location, Date lastTime)
	{
		this.setLocation(location);
		this.setUpdatedTime(lastTime);
	}
	
	private String location;
	private Date updatedTime;
	private Double temperature;
	private Double appTemperature;
	private Double dewPointTemp;
	private Double relHumidity;
	private Double deltaTemp;
	
	//Wind Information
	private String windDirection;
	private Double speedKmph;
	private Double gustKmph;
	private Double speedKts;
	private Double gustKts;
	
	private Double pressure;
	private Double rain;
	private Double lowTemp;
	private Date lowTempTime;
	private Double highTemp;
	private Date highTempTime;
	
	//Highest Wind Gust
	private String gustDirection;
	private Double gustSpeedKmph;
	private Date gustSpeedKmphTime;
	private Double gustSpeedKts;
	private Date gustSpeedKtsTime;
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the updatedTime
	 */
	public Date getUpdatedTime() {
		return updatedTime;
	}
	/**
	 * @param updatedTime the updatedTime to set
	 */
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	/**
	 * @return the temperature
	 */
	public Double getTemperature() {
		return temperature;
	}
	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	/**
	 * @return the appTemperature
	 */
	public Double getAppTemperature() {
		return appTemperature;
	}
	/**
	 * @param appTemperature the appTemperature to set
	 */
	public void setAppTemperature(Double appTemperature) {
		this.appTemperature = appTemperature;
	}
	/**
	 * @return the dewPointTemp
	 */
	public Double getDewPointTemp() {
		return dewPointTemp;
	}
	/**
	 * @param dewPointTemp the dewPointTemp to set
	 */
	public void setDewPointTemp(Double dewPointTemp) {
		this.dewPointTemp = dewPointTemp;
	}
	/**
	 * @return the deltaTemp
	 */
	public Double getDeltaTemp() {
		return deltaTemp;
	}
	/**
	 * @param deltaTemp the deltaTemp to set
	 */
	public void setDeltaTemp(Double deltaTemp) {
		this.deltaTemp = deltaTemp;
	}
	/**
	 * @return the windDirection
	 */
	public String getWindDirection() {
		return windDirection;
	}
	/**
	 * @param windDirection the windDirection to set
	 */
	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}
	/**
	 * @return the speedKmph
	 */
	public Double getSpeedKmph() {
		return speedKmph;
	}
	/**
	 * @param speedKmph the speedKmph to set
	 */
	public void setSpeedKmph(Double speedKmph) {
		this.speedKmph = speedKmph;
	}
	/**
	 * @return the gustKmph
	 */
	public Double getGustKmph() {
		return gustKmph;
	}
	/**
	 * @param gustKmph the gustKmph to set
	 */
	public void setGustKmph(Double gustKmph) {
		this.gustKmph = gustKmph;
	}
	/**
	 * @return the speedKts
	 */
	public Double getSpeedKts() {
		return speedKts;
	}
	/**
	 * @param speedKts the speedKts to set
	 */
	public void setSpeedKts(Double speedKts) {
		this.speedKts = speedKts;
	}
	/**
	 * @return the gustKts
	 */
	public Double getGustKts() {
		return gustKts;
	}
	/**
	 * @param gustKts the gustKts to set
	 */
	public void setGustKts(Double gustKts) {
		this.gustKts = gustKts;
	}
	/**
	 * @return the pressure
	 */
	public Double getPressure() {
		return pressure;
	}
	/**
	 * @param pressure the pressure to set
	 */
	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}
	/**
	 * @return the rain
	 */
	public Double getRain() {
		return rain;
	}
	/**
	 * @param rain the rain to set
	 */
	public void setRain(Double rain) {
		this.rain = rain;
	}
	/**
	 * @return the lowTemp
	 */
	public Double getLowTemp() {
		return lowTemp;
	}
	/**
	 * @param lowTemp the lowTemp to set
	 */
	public void setLowTemp(Double lowTemp) {
		this.lowTemp = lowTemp;
	}
	/**
	 * @return the lowTempTime
	 */
	public Date getLowTempTime() {
		return lowTempTime;
	}
	/**
	 * @param lowTempTime the lowTempTime to set
	 */
	public void setLowTempTime(Date lowTempTime) {
		this.lowTempTime = lowTempTime;
	}
	/**
	 * @return the highTemp
	 */
	public Double getHighTemp() {
		return highTemp;
	}
	/**
	 * @param highTemp the highTemp to set
	 */
	public void setHighTemp(Double highTemp) {
		this.highTemp = highTemp;
	}
	/**
	 * @return the highTempTime
	 */
	public Date getHighTempTime() {
		return highTempTime;
	}
	/**
	 * @param highTempTime the highTempTime to set
	 */
	public void setHighTempTime(Date highTempTime) {
		this.highTempTime = highTempTime;
	}
	/**
	 * @return the gustDirection
	 */
	public String getGustDirection() {
		return gustDirection;
	}
	/**
	 * @param gustDirection the gustDirection to set
	 */
	public void setGustDirection(String gustDirection) {
		this.gustDirection = gustDirection;
	}
	/**
	 * @return the gustSpeedKmph
	 */
	public Double getGustSpeedKmph() {
		return gustSpeedKmph;
	}
	/**
	 * @param gustSpeedKmph the gustSpeedKmph to set
	 */
	public void setGustSpeedKmph(Double gustSpeedKmph) {
		this.gustSpeedKmph = gustSpeedKmph;
	}
	/**
	 * @return the gustSpeedTime
	 */
	public Date getGustSpeedTime() {
		return gustSpeedKmphTime;
	}
	/**
	 * @param gustSpeedTime the gustSpeedTime to set
	 */
	public void setGustSpeedTime(Date gustSpeedTime) {
		this.gustSpeedKmphTime = gustSpeedTime;
	}
	/**
	 * @return the gustSpeedKts
	 */
	public Double getGustSpeedKts() {
		return gustSpeedKts;
	}
	/**
	 * @param gustSpeedKts the gustSpeedKts to set
	 */
	public void setGustSpeedKts(Double gustSpeedKts) {
		this.gustSpeedKts = gustSpeedKts;
	}

	/**
	 * @return the relHumidity
	 */
	public Double getRelHumidity() {
		return relHumidity;
	}

	/**
	 * @param relHumidity the relHumidity to set
	 */
	public void setRelHumidity(Double relHumidity) {
		this.relHumidity = relHumidity;
	}

	/**
	 * @return the gustSpeedKtsTime
	 */
	public Date getGustSpeedKtsTime() {
		return gustSpeedKtsTime;
	}

	/**
	 * @param gustSpeedKtsTime the gustSpeedKtsTime to set
	 */
	public void setGustSpeedKtsTime(Date gustSpeedKtsTime) {
		this.gustSpeedKtsTime = gustSpeedKtsTime;
	}
	
	public static Double returnDoubleFromString(String data)
	{
		if(!data.trim().equals("-")&& data.trim().matches("[-+]?[0-9]*\\.?[0-9]+"))
		{
			return Double.parseDouble(data);
		}
		return 0.0;
	}
	
	public static String returnStringFromString(String data)
	{
		if(!data.trim().equals("-"))
		{
			return data;
		}
		return "";
	}
	
	public static TimeData returnTimeDatafromString(String data, String delimiter)
	{
		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mma");
		Date lowtempDate = new Date();
		TimeData returnvar=new TimeData();
		if(!data.contains("-"))
		{
		try {
			lowtempDate.setHours(parseFormat.parse(data.split(delimiter)[1]).getHours());
			lowtempDate.setMinutes(parseFormat.parse(data.split(delimiter)[1]).getMinutes());
			lowtempDate.setSeconds(0);
			returnvar.setTimeDate(lowtempDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!data.split(delimiter)[0].trim().equals("-")&&data.split(delimiter)[0].trim().matches("[-+]?[0-9]*\\.?[0-9]+"))
		{
		Double vdata=Double.parseDouble(data.split(delimiter)[0]);
		returnvar.setValue(vdata);
		}
		}
		else
		{
			lowtempDate.setDate(0);
			lowtempDate.setHours(0);
			lowtempDate.setMinutes(0);
			lowtempDate.setSeconds(0);
			returnvar.setTimeDate(lowtempDate);
			returnvar.setValue(0);
		}
		
		return returnvar;
		
	}
	
	
}

 