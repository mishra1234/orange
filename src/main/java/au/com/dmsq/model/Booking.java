package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "booking")
public class Booking implements java.io.Serializable {

	private static final long serialVersionUID = 5540656289822420784L;

	
	private Integer id;
	private Activity activity;
	private Account account;
	private String ccmeCalHref;
	private String bookingCalHref;

	
	public Booking() {
	}

	public Booking(Activity activity, Account account) {
		this.activity = activity;
		this.account = account;
	}

	public Booking(Activity activity, Account account, String ccmeCalHref,
			String bookingCalHref) {
		this.activity = activity;
		this.account = account;
		this.ccmeCalHref = ccmeCalHref;
		this.bookingCalHref = bookingCalHref;
	}
	

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "activity_id", nullable = false)
	public Activity getActivity() {
		return this.activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account_id", nullable = false)
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Column(name = "ccme_cal_href")
	public String getCcmeCalHref() {
		return this.ccmeCalHref;
	}

	public void setCcmeCalHref(String ccmeCalHref) {
		this.ccmeCalHref = ccmeCalHref;
	}

	@Column(name = "booking_cal_href")
	public String getBookingCalHref() {
		return this.bookingCalHref;
	}

	public void setBookingCalHref(String bookingCalHref) {
		this.bookingCalHref = bookingCalHref;
	}

}

