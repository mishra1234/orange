package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class SocialSignOnId implements java.io.Serializable {

	private static final long serialVersionUID = -5814888793951719361L;
	
	private String uid;
	private String service;

	public SocialSignOnId() {
	}

	public SocialSignOnId(String uid, String service) {
		this.uid = uid;
		this.service = service;
	}

	@Column(name = "uid", nullable = false)
	public String getUid() {
		return this.uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@Column(name = "service", nullable = false, length = 9)
	public String getService() {
		return this.service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SocialSignOnId))
			return false;
		SocialSignOnId castOther = (SocialSignOnId) other;

		return ((this.getUid() == castOther.getUid()) || (this.getUid() != null
				&& castOther.getUid() != null && this.getUid().equals(
				castOther.getUid())))
				&& ((this.getService() == castOther.getService()) || (this
						.getService() != null && castOther.getService() != null && this
						.getService().equals(castOther.getService())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getUid() == null ? 0 : this.getUid().hashCode());
		result = 37 * result
				+ (getService() == null ? 0 : this.getService().hashCode());
		return result;
	}

}

