package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "registration_key")
public class RegistrationKey implements java.io.Serializable {

	private static final long serialVersionUID = 8593404157026448267L;
	
	private String id;
	private Boolean used;
	private Integer userId;

	public RegistrationKey() {
	}

	public RegistrationKey(String id) {
		this.id = id;
	}

	public RegistrationKey(String id, Boolean used, Integer userId) {
		this.id = id;
		this.used = used;
		this.userId = userId;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false, length = 4)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "used")
	public Boolean getUsed() {
		return this.used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}

