package au.com.dmsq.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "category_banner")
public class CategoryBanner implements Serializable 
{

	private static final long serialVersionUID = 2570856830280117683L;

	@Id @OneToOne
	@JoinColumn(name = "category_id")
	private Category categoryId; 	
	

	@Id @ManyToOne ( cascade = { CascadeType.ALL })
	@PrimaryKeyJoinColumn(name="bannerId", referencedColumnName="id")
	private Banner bannerId;
	
	public Category getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(Category categoryId) {
		this.categoryId = categoryId;
	}


	public Banner getBannerId() {
		return bannerId;
	}


	public void setBannerId(Banner bannerId) {
		this.bannerId = bannerId;
	}


	
}