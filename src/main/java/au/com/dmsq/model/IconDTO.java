package au.com.dmsq.model;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class IconDTO {
	
	@NotNull
    @NotEmpty
    private String iconUrl;    
    
    public IconDTO() {
	}


    public IconDTO(String iconUrl) {
		this.setIconUrl(iconUrl);
    }


	public String getIconUrl() {
		return iconUrl;
	}


	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
}
