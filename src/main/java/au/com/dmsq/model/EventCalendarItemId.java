package au.com.dmsq.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class EventCalendarItemId implements java.io.Serializable {

	private static final long serialVersionUID = 916658802921899483L;

	private Integer activityId;
	private String calHref;

	public EventCalendarItemId() {
	}

	public EventCalendarItemId(int activityId, String calHref) {
		this.activityId = activityId;
		this.calHref = calHref;
	}

	@Column(name = "activity_id", nullable = false)
	public int getActivityId() {
		return this.activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	@Column(name = "cal_href", nullable = false)
	public String getCalHref() {
		return this.calHref;
	}

	public void setCalHref(String calHref) {
		this.calHref = calHref;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof EventCalendarItemId))
			return false;
		EventCalendarItemId castOther = (EventCalendarItemId) other;

		return (this.getActivityId() == castOther.getActivityId())
				&& ((this.getCalHref() == castOther.getCalHref()) || (this
						.getCalHref() != null && castOther.getCalHref() != null && this
						.getCalHref().equals(castOther.getCalHref())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getActivityId();
		result = 37 * result
				+ (getCalHref() == null ? 0 : this.getCalHref().hashCode());
		return result;
	}

}

