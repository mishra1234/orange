package au.com.dmsq.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;



@Entity
@Table(name = "layer")
public class Layer implements java.io.Serializable {

	private static final long serialVersionUID = -4591629998220337684L;
	
	private Integer id;
	private Account account;  //Owner of the community
	private String name;
	private Boolean recommended;
	private String iconUrl;
	private Boolean deleted;
	private Boolean connectable;
	private String status;
	private String type;
	private Date createdDatetime;
	private Integer createdAccountId;
	private Set<Layer> layersForChildLayerId = new HashSet<Layer>(0);
	private Set<Layer> layersForParentLayerId = new HashSet<Layer>(0);
	private Set<Category> categories = new HashSet<Category>(0);
	private Set<Banner> banners = new HashSet<Banner>(0);
	private Set<Account> accounts = new HashSet<Account>(0);
	private Set<Account> accounts_1 = new HashSet<Account>(0);
	private Set<Activity> activities = new HashSet<Activity>(0);
	private Set<AccountLayerAccess> accountLayerAccesses = new HashSet<AccountLayerAccess>(0);
	private Set<Message> messages = new HashSet<Message>(0);
	private Set<Activity> activities_1 = new HashSet<Activity>(0);
	private Set<Account> connectedAccount = new HashSet<Account>(0);
	
	
	

	@Transient
	private transient int numberActivities = 0;

	public Layer() {
	}

	public Layer(Account account) {
		this.account = account;
	}
	
	public Layer(Account account, String status, String type) {
		this.account = account;
		this.status = status;
		this.type = type;
	}

	public Layer(Account account, String name, Boolean recommended, String iconUrl, Boolean deleted, Boolean connectable,
			String status, String type, Date createdDatetime, Integer createdAccountId, Set<Layer> layersForChildLayerId,
			Set<Layer> layersForParentLayerId, Set<Category> categories, Set<Banner> banners, Set<Account> accounts,
			Set<Account> accounts_1, Set<Activity> activities, Set<AccountLayerAccess> accountLayerAccesses,
			Set<Message> messages, Set<Activity> activities_1) 
	{
		this.account = account;
		this.name = name;
		this.recommended = recommended;
		this.iconUrl = iconUrl;
		this.deleted = deleted;
		this.connectable = connectable;
		this.status = status;
		this.type = type;
		this.createdDatetime = createdDatetime;
		this.createdAccountId = createdAccountId;
		this.layersForChildLayerId = layersForChildLayerId;
		this.layersForParentLayerId = layersForParentLayerId;
		this.categories = categories;
		this.banners = banners;
		this.accounts = accounts;
		this.accounts_1 = accounts_1;
		this.activities = activities;
		this.accountLayerAccesses = accountLayerAccesses;
		this.messages = messages;
		this.activities_1 = activities_1;
	}
	

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "owner_id", nullable = false)
	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@NotEmpty(message = "Name must not be blank.")
	@Size(min=2, max=45, message="Lenght must be between 2 and 45 characters!")
	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "recommended")
	public Boolean getRecommended() {
		return this.recommended;
	}

	public void setRecommended(Boolean recommended) {
		this.recommended = recommended;
	}

	//@NotEmpty(message = "Name must not be blank.")
	@Column(name = "icon_url")
	public String getIconUrl() {
		return this.iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	@Column(name = "deleted")
	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	@Column(name = "connectable")
	public Boolean getConnectable() {
		return this.connectable;
	}

	public void setConnectable(Boolean connectable) {
		this.connectable = connectable;
	}

	@Column(name = "status", nullable = false, length = 6)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "type", nullable = false, length = 6)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "child_layers", joinColumns = { 
		@JoinColumn(name = "parent_layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "child_layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayersForChildLayerId() {
		return this.layersForChildLayerId;
	}

	public void setLayersForChildLayerId(Set<Layer> layersForChildLayerId) {
		this.layersForChildLayerId = layersForChildLayerId;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "child_layers", joinColumns = { 
		@JoinColumn(name = "child_layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "parent_layer_id", nullable = false, updatable = false) })
	public Set<Layer> getLayersForParentLayerId() {
		return this.layersForParentLayerId;
	}

	public void setLayersForParentLayerId(Set<Layer> layersForParentLayerId) {
		this.layersForParentLayerId = layersForParentLayerId;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "layer_categories", joinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "category_id", nullable = false, updatable = false) })
	public Set<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "layer_banners", joinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "banner_id", nullable = false, updatable = false) })
	public Set<Banner> getBanners() {
		return this.banners;
	}

	public void setBanners(Set<Banner> banners) {
		this.banners = banners;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "layer_managers", joinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "account_id", nullable = false, updatable = false) })
	public Set<Account> getAccounts() {
		return this.accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "connected_layers", joinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "account_id", nullable = false, updatable = false) })
	public Set<Account> getAccounts_1() {
		return this.accounts_1;
	}

	public void setAccounts_1(Set<Account> accounts_1) {
		this.accounts_1 = accounts_1;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "layer_activities", joinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "activity_id", nullable = false, updatable = false) })
	public Set<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(Set<Activity> activities) {
		this.activities = activities;
	}

	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "layer")
	public Set<AccountLayerAccess> getAccountLayerAccesses() {
		return this.accountLayerAccesses;
	}

	public void setAccountLayerAccesses(
			Set<AccountLayerAccess> accountLayerAccesses) {
		this.accountLayerAccesses = accountLayerAccesses;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "message_layer_links", joinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "message_id", nullable = false, updatable = false) })
	public Set<Message> getMessages() {
		return this.messages;
	}
	

	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "activity_layer_links", joinColumns = { 
		@JoinColumn(name = "layer_id", nullable = false, updatable = false) }, inverseJoinColumns = { 
		@JoinColumn(name = "activity_id", nullable = false, updatable = false) })
	public Set<Activity> getActivities_1() {
		return this.activities_1;
	}
	

	public void setActivities_1(Set<Activity> activities_1) {
		this.activities_1 = activities_1;
	}
	
	@Column(name = "created_datetime")
	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Column(name = "created_account_id", nullable = false)
	public Integer getCreatedAccountId() {
		return createdAccountId;
	}

	public void setCreatedAccountId(Integer createdAccountId) {
		this.createdAccountId = createdAccountId;
	}

	@Transient
	public int getNumberActivities() {
		return numberActivities;
	}

	@Transient
	public void setNumberActivities(int numberActivities) {
		this.numberActivities = numberActivities;
	}

}
