package au.com.dmsq.dao;

import java.util.List;

import au.com.dmsq.model.AccountLayerAccess;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface AccountLayerAccessDao extends GenericDAO<AccountLayerAccess, Integer> {

	public List<AccountLayerAccess> getCurrent();	 
		// This goes empty unless we implement our own methods
		// save, delete, findAll, get and others are already available
   
	public AccountLayerAccess getbyGrantToken(String grantToken);
	
	public AccountLayerAccess getbyDenyToken(String denyToken);
	}
