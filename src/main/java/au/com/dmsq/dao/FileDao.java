package au.com.dmsq.dao;

import java.util.List;
import au.com.dmsq.model.File;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface FileDao extends GenericDAO<File, Integer> {

	// This goes empty unless we implement our own methods
	// save, delete, findAll, get and others are already available

	public List<File> checkNonDeleted();	 

}
