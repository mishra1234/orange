package au.com.dmsq.dao;

import java.util.List;

import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Category;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface BannerDao extends GenericDAO <Banner, Integer> {
	   
	public List<Banner> getNonDeleted();

}