package au.com.dmsq.dao;

import java.util.List;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Layer;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface AccountDao extends GenericDAO<Account, Integer> {

		// This goes empty unless we implement our own methods
		// save, delete, findAll, get and others are already available
	
		public List<Account> checkNonDeleted();

		public Account getByResetCode(String token);

		public Account findByIdWithLayers(Integer id);

		public Account findByIdWithLayerManaged(Integer id);

		public Account findByIdWithRelations(Integer id);

		public Account findWithLayers(Integer id);

		public Account findByIdWithLayersFaster(Integer id);

		public Account findAcctByEmail(String email);

		public Account findByIdWithAccConfirmations(Integer id);

		List<Account> getConnection(List<Layer> layerList);

		public Account findByIdWithLayerManagedF(Integer id);

		public Account findByIdWithRelationsFaster(Integer id);

		public Account findALAsById(Integer id);

		public Account findLayersById(Integer id);

		public Account findLayerManagerById(Integer id);

		public Account findBookingsById(Integer id);

		public Account findAccountLayerAccessesById(Integer id);

		public Account findByIdActivities(Integer id);

		public Account findAuthById(Integer id);

		public Account findConnectedLayersById(Integer id);
 
	
	}
