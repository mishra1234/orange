package au.com.dmsq.dao;

import au.com.dmsq.model.Mail;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface MailDao extends GenericDAO<Mail, Integer>{

}
