package au.com.dmsq.dao;

import java.util.List;

import au.com.dmsq.model.Booking;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface BookingDao extends GenericDAO <Booking, Integer> {

	public List<Booking> searchByActivityId(Integer activityId);
	
}
