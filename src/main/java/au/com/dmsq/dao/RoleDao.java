package au.com.dmsq.dao;

import au.com.dmsq.model.Role;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface RoleDao extends GenericDAO<Role, Integer> {

	// This goes empty unless we implement our own methods
	// save, delete, findAll, get and others are already available
}
