package au.com.dmsq.dao;
import java.io.Serializable;
import java.util.List;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Category;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface CategoryDao extends GenericDAO <Category, Integer> 
{
	
	//public <T> T find(Class<T> type, Serializable id);

	public List<Category> getNonDeleted();	 
	
	public int getConnRows(String id); 
	public int getLayerRows(String id); 
	public int getEventRows(String id);

	Category getCategoryCommunities(String id);

}
