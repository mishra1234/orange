package au.com.dmsq.dao;

import java.util.List;

import au.com.dmsq.model.Message;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface MessageDao extends GenericDAO<Message, Integer> {

	public List<Message> getNonDeleted();	 
	
}
