package au.com.dmsq.dao;

import java.util.List;

import au.com.dmsq.model.ActivityCalendar;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface ActivityCalendarDao extends GenericDAO<ActivityCalendar, Integer> {

	public List<ActivityCalendar> getCurrent();	 

	public List<ActivityCalendar> searchByActivityId(Integer activityId);
	
	}
