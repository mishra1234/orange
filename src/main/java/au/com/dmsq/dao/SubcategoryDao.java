package au.com.dmsq.dao;

import au.com.dmsq.model.Subcategory;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface SubcategoryDao extends GenericDAO <Subcategory, Integer> {
   

}
