package au.com.dmsq.dao;

import java.util.List;
import au.com.dmsq.model.ActivityCalendar;
import au.com.dmsq.model.ActivityPayment;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface ActivityPaymentDao extends GenericDAO <ActivityPayment, Integer> {
	
	//This function gets the list of activities paid no matter if the payment is successful or not!
	public List<ActivityPayment> getAllActivitiesPaid();	 


}
