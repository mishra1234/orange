package au.com.dmsq.dao;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountConfirmation;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.googlecode.genericdao.search.Search;

public interface AccountConfirmationDao extends GenericDAO<AccountConfirmation, Integer> {


	public AccountConfirmation findByAccount(AccountConfirmation ac);

	
}
