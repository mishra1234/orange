package au.com.dmsq.dao;

import java.util.List;
import java.util.Set;

import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Layer;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface ActivityDao extends GenericDAO <Activity, Integer> {

	public List<Activity> getNonDeleted();

	public Activity findByIdWithRelations(Integer id);

	public Activity findByIdWithLayers(Integer id);

	public Activity findByIdWithBanners(Integer id);


	public List<Activity> findByLayerId(Integer id);

	public Activity findByIdBanners(Integer id);

	public Activity findByIdLayers(Integer id);

	public Activity findByIdFiles(Integer id);

	public List<Activity> findEventsByLayerId(Integer id);

	public List<Activity> findInfosByLayerId(Integer id);

	public List<Activity> findEventsbyLayer(String type, Long dtend, Layer layer);

	public List<Activity> findEventsbyLayerPast(String type, Long dtend, Layer layer);

	public Integer countEventsbyLayer(String type, Long dtend, Layer layer);

	public Integer countEventsbyLayerPast(String type, Long dtend, Layer layer);

	// public int queryEventsbyLayer(String type, Long dtend, Layer layer);

	// public int queryEventsbyLayerPast(String type, Long dtend, Layer layer);

	public List<Activity> queryFindEventsbyLayerPast(String type, Long dtend, Layer layer);

	public List<Activity> queryFindEventsbyLayer(String type, Long dtend, Layer layer);

	public int queryEventsbyLayer(String type, Long dtend, Integer layerId);

	public int queryEventsbyLayerPast(String type, Long dtend, Integer layerId);

	
}
