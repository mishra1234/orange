package au.com.dmsq.dao;

import java.util.List;

import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.EventCalendarItemId;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface EventCalendarItemDao extends GenericDAO <EventCalendarItem, Integer>{
	
	public List<EventCalendarItem> getNonDeleted();

	public List<Activity> findByActId(Integer id);

	public EventCalendarItem findByEciId(EventCalendarItemId eciId);

	public EventCalendarItem findByActIDandHref(Activity act,String href,Integer actId);

}
