package au.com.dmsq.dao;

import java.util.List;
import au.com.dmsq.model.Event;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface EventDao extends GenericDAO <Event, Integer> {
	   
	public List<Event> getNonDeleted();

	public Event findByIdWithRelations(Integer id);	 
}