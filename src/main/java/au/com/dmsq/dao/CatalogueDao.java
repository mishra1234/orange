package au.com.dmsq.dao;

import au.com.dmsq.model.Catalogue;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface CatalogueDao extends GenericDAO <Catalogue, Integer>{
	
	public Catalogue getCatalogueById(Integer id);
	
}
