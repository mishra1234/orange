package au.com.dmsq.dao;

import au.com.dmsq.model.Auth;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface AuthDao extends GenericDAO <Auth, Integer> {
	   

}