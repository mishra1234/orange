package au.com.dmsq.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.BannerDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Category;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

@Repository
public class BannerDaoImpl extends GenericDAOImpl<Banner, Integer> implements BannerDao {


	 	@Autowired
	    @Override
	    public void setSessionFactory(SessionFactory sessionFactory) {
	        super.setSessionFactory(sessionFactory);     
	    }
	 	
	 	public List<Banner> getNonDeleted()
		{
			 return search(new Search(Account.class).addFilterEqual("deleted", false));
		}
	 	
}