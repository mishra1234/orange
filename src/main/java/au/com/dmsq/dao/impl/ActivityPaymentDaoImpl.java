package au.com.dmsq.dao.impl;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import au.com.dmsq.dao.ActivityPaymentDao;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.ActivityPayment;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;
@Repository
public class ActivityPaymentDaoImpl extends GenericDAOImpl<ActivityPayment, Integer> implements ActivityPaymentDao{
	@Autowired
  @Override
  public void setSessionFactory(SessionFactory sessionFactory) {
      super.setSessionFactory(sessionFactory);
	}
	
	public List<ActivityPayment> getAllActivitiesPaid()
	{
		 return  search(new Search(ActivityPayment.class));
	}

}
