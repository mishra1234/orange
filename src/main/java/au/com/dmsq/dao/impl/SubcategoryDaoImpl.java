package au.com.dmsq.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;

import au.com.dmsq.dao.SubcategoryDao;
import au.com.dmsq.model.Subcategory;

@Repository
public class SubcategoryDaoImpl extends GenericDAOImpl<Subcategory, Integer> implements SubcategoryDao {

	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);     
    }
	/* Available Methods. 
	 * 
	 * 
	 *  public <T> T find(Class<T> type, Serializable id);

        public <T> T[] find(Class<T> type, Serializable... ids);

        public <T> T getReference(Class<T> type, Serializable id);

        public <T> T[] getReferences(Class<T> type, Serializable... ids);

        public boolean save(Object entity);

        public boolean[] save(Object... entities);

        public boolean remove(Object entity);

        public void remove(Object... entities);

        public boolean removeById(Class<?> type, Serializable id);

        public void removeByIds(Class<?> type, Serializable... ids);

        public <T> List<T> findAll(Class<T> type);

        public List search(ISearch search);

        public Object searchUnique(ISearch search);

        public int count(ISearch search);

        public SearchResult searchAndCount(ISearch search);

        public boolean isAttached(Object entity);

        public void refresh(Object... entities);

        public void flush();

        public Filter getFilterFromExample(Object example);

        public Filter getFilterFromExample(Object example, ExampleOptions options);
	 * 
	 * */
	
	
	
}
