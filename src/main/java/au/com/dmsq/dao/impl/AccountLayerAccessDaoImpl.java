package au.com.dmsq.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import au.com.dmsq.dao.AccountLayerAccessDao;
import au.com.dmsq.model.AccountLayerAccess;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

@Repository
public class AccountLayerAccessDaoImpl extends GenericDAOImpl<AccountLayerAccess, Integer> implements AccountLayerAccessDao{

	@Autowired
	@Override
	public void setSessionFactory(SessionFactory sessionFactory) {
	    super.setSessionFactory(sessionFactory);
		}	 
		public List<AccountLayerAccess> getCurrent()
		{
			 return search(new Search(AccountLayerAccess.class).addFilterEqual("timestamp", 2014));
		}

		@Override
		public AccountLayerAccess getbyGrantToken(String grantToken) {
			// TODO Auto-generated method stub
			return searchUnique(new Search(AccountLayerAccess.class).addFilterEqual("grantAccessCode", grantToken));
		}
		@Override
		public AccountLayerAccess getbyDenyToken(String denyToken) {
			// TODO Auto-generated method stub
			return searchUnique(new Search(AccountLayerAccess.class).addFilterEqual("denyAccessCode", denyToken));
		}

}
