package au.com.dmsq.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.query.spi.sql.NativeSQLQueryReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.ActivityDao;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.Layer;

@Repository
public class ActivityDaoImpl extends GenericDAOImpl<Activity, Integer> implements ActivityDao{
	
	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
 	}
	
	public List<Activity> getNonDeleted()
	{
		 return  search(new Search(Activity.class)
				 	.addFilterEqual("deleted", true));
	}
		
	@Override
	public Activity findByIdWithRelations(Integer id) 
	{
		return (Activity) searchUnique(
				new Search(Activity.class)
				.addFilterEqual("id", id)
				.addFetches(
					"account", "bookings", "files",	"recommendedActivitiesesForActivityId",
					"recommendedActivitiesesForRecommendedActivityId", "activitiesForChildActivityId", 
					"layers", "messages", "activityCalendars", "accounts", "activitiesForActivityId", 
					"layers_1", "banners")
					);
	}
	
	@Override
	public Activity findByIdBanners(Integer id) 
	{
		return (Activity) searchUnique(
				new Search(Activity.class)
				.addFilterEqual("id", id)
				.addFetches("banners")
					);
	}
	
	@Override
	public Activity findByIdLayers(Integer id) 
	{
		return (Activity) searchUnique(
				new Search(Activity.class)
				.addFilterEqual("id", id)
				.addFetches("layers")
					);
	}
	
	@Override
	public Activity findByIdFiles(Integer id) 
	{
		return (Activity) searchUnique(
				new Search(Activity.class)
				.addFilterEqual("id", id)
				.addFetches("files")
					);
	}
	
	@Override
	public Activity findByIdWithBanners(Integer id) 
	{
		return (Activity) searchUnique(
				new Search(Activity.class)
				.addFilterEqual("id", id)
				.addFetches("banners","account","files"));
	}
	
	@Override
	public Activity findByIdWithLayers(Integer id) 
	{
		return (Activity) searchUnique(
				new Search(Activity.class)
				.addFilterEqual("id", id)
				.addFetches("account","layers","accountLayerAccesses","layers_1" ));
	}
	
	@Override
	public List<Activity> findByLayerId(Integer id){
		
		return  search(
				new Search(Activity.class)
				.addFilterEqual("deleted", false)
				.addFilterSome("layers", Filter.equal("id", id))
				);
	}

	@Override
	public List<Activity> findEventsByLayerId(Integer id){
			
			return  search(
					new Search(Activity.class)
					.addFilterEqual("deleted", false)
					.addFilterEqual("activityType", "event")
					.addFilterSome("layers", Filter.equal("id", id))
					);
		}
	
	@Override
	public List<Activity> findInfosByLayerId(Integer id){
		
		return  search(
				new Search(Activity.class)
				.addFilterEqual("deleted", false)
				.addFilterEqual("activityType", "info")
				.addFilterSome("layers", Filter.equal("id", id))
				.addSort("id", true)
				);
	}
	
	
	@Override
	public List<Activity> findEventsbyLayer(String type, Long dtend, Layer layer){
	
		Long dtEnd = ((dtend/1000)-30600);
		
		try{
			return  search(
				new Search(Activity.class)
				.addSort("contentTitle", false)
				.addFilterAnd(
							Filter.equal("deleted", false), 
							Filter.equal("activityType", type), 
							Filter.some("layers", Filter.equal("id", layer.getId())),
							Filter.greaterOrEqual("event.eventCalendarItems.dtend", dtEnd)));
		}catch(Exception ex) {
	    	System.out.println("Exception : " + ex.toString());
	    }
		return new ArrayList<Activity>();
	}
	
	
	@Override
	public List<Activity> findEventsbyLayerPast(String type, Long dtend, Layer layer){
	
		Long dtEnd = ((dtend/1000)-30600);
		
		try{
			return  search(
				new Search(Activity.class)
				.addSort("contentTitle", false)
				.addFilterAnd(
							Filter.equal("deleted", false), 
							Filter.equal("activityType", type), 
							Filter.some("layers", Filter.equal("id", layer.getId())),
							Filter.lessThan("event.eventCalendarItems.dtend", dtEnd)));
		}catch(Exception ex) {
	    	System.out.println("Exception : " + ex.toString());
	    }
		return new ArrayList<Activity>();
	}
	
	
	@Override 
	public Integer countEventsbyLayerPast(String type, Long dtend, Layer layer){
	
		Long dtEnd = ((dtend/1000)-30600);
		try{
			Search search= ((Search) new Search(Activity.class)
					.setDistinct(true))
					.addFilterAnd(
							Filter.equal("deleted", false), 
							Filter.equal("activityType", type), 
							Filter.some("layers", 
							Filter.equal("id", layer.getId())),
							Filter.lessThan("event.eventCalendarItems.dtend", dtEnd));
			
			Set<Activity> set = new HashSet<>();
			set.addAll((Collection<? extends Activity>) search);
			
			List<Activity> activities = new ArrayList<>();
			for(Activity act:set){
				activities.add(act);
			}
			
			System.out.println("Search count    "+ count(search) );
			
			return activities.size();
		}catch(Exception ex) {
	    	System.out.println("Exception : " + ex.toString());
	    }
		return 0;
	}
	
	
	@Override 
	public Integer countEventsbyLayer(String type, Long dtend, Layer layer){
	
		Long dtEnd = ((dtend/1000)-30600);
		try{
			Search search= (((Search) new Search(Activity.class)
					.addFilterAnd(
							Filter.equal("deleted", false), 
							Filter.equal("activityType", type), 
							Filter.some("layers", 
							Filter.equal("id", layer.getId())))
					.addFilterGreaterOrEqual("event.eventCalendarItems.dtend", dtEnd)
					.setDistinct(true)).addField("id"));
			
			return count(search);
		}catch(Exception ex) {
	    	System.out.println("Exception : " + ex.toString());
	    }
		return 0;
	}
	
	@Override 
	public int queryEventsbyLayer(String type, Long dtend, Integer layerId){
		Long dtEnd = ((dtend/1000)-30600);
		Session session = null;
		session=getSessionFactory().openSession();
		
 		BigInteger  obj = (BigInteger) session.createSQLQuery(
 				""
 				+ "SELECT count(DISTINCT layer_id, id)FROM ccme_winter_2.layer_activities "
 				+ "JOIN ccme_winter_2.activity ON layer_activities.activity_id=activity.id "
 				+ "JOIN ccme_winter_2.event_calendar_item ON layer_activities.activity_id=ccme_winter_2.event_calendar_item.activity_id "
 				+ "where layer_id="+ layerId +" and activity_type='"+type+"' and deleted=false and dtend>= "+ dtEnd +";"
 				).uniqueResult();
 		
 		session.close();
 		return obj.intValue();
	}
	
	@Override 
	public int queryEventsbyLayerPast(String type, Long dtend, Integer layerId){
		Long dtEnd = ((dtend/1000)-30600);
		Session session = null;
		session=getSessionFactory().openSession();
		
 		BigInteger  obj = (BigInteger) session.createSQLQuery(
 				""
				+ "SELECT count(DISTINCT layer_id, id)FROM ccme_winter_2.layer_activities JOIN ccme_winter_2.activity "
 				+ "ON layer_activities.activity_id=activity.id JOIN ccme_winter_2.event_calendar_item "
 				+ "ON layer_activities.activity_id=ccme_winter_2.event_calendar_item.activity_id "
 				+ "where layer_id="+ layerId +" and activity_type='"+type+"' and deleted=false and dtend< "+ dtEnd +";"
 				).uniqueResult();
 		
 		session.close();
 		return obj.intValue();
	}
	
	@Override
	public List<Activity> queryFindEventsbyLayer(String type, Long dtend, Layer layer){
		Long dtEnd = ((dtend/1000)-30600);
		Session session = null;
		session=getSessionFactory().openSession();
		
		Query query = session.createSQLQuery(
 				""
				+ "SELECT DISTINCT id dtend FROM ccme_winter_2.layer_activities JOIN ccme_winter_2.activity "
 				+ "ON layer_activities.activity_id=activity.id JOIN ccme_winter_2.event_calendar_item "
 				+ "ON layer_activities.activity_id=ccme_winter_2.event_calendar_item.activity_id "
 				+ "where layer_id="+ layer.getId() +" and activity_type='"+ type +"' and deleted=false and dtend>= "+ dtEnd +" order by content_title asc;"
 				);
		
		List acts = query.list();
		List<Integer> listActs = new ArrayList<>();
		
		for(int i = 0; i < acts.size(); i++){
			System.out.println("Object on list " + acts.get(i) );
			listActs.add((int) acts.get(i));
		}
		System.out.println("Size of list " + listActs.size());
		
		List<Activity> actS = new ArrayList<>();
		for(int i:listActs){
			actS.add(findByIdWithBanners(i));
		}
		
		session.close();
 		return actS;
	}
	
	@Override
	public List<Activity> queryFindEventsbyLayerPast(String type, Long dtend, Layer layer){
		Long dtEnd = ((dtend/1000)-30600);
		Session session = null;
		session=getSessionFactory().openSession();
		
		Query query = session.createSQLQuery(
 				""
				+ "SELECT DISTINCT id dtend FROM ccme_winter_2.layer_activities JOIN ccme_winter_2.activity "
 				+ "ON layer_activities.activity_id=activity.id JOIN ccme_winter_2.event_calendar_item "
 				+ "ON layer_activities.activity_id=ccme_winter_2.event_calendar_item.activity_id "
 				+ "where layer_id="+ layer.getId() +" and activity_type='"+ type +"' and deleted=false and dtend< "+ dtEnd +" order by content_title asc;"
 				);
		
		List acts = query.list();
		List<Integer> listActs = new ArrayList<>();
		
		for(int i = 0; i < acts.size(); i++){
			System.out.println("Object on list " + acts.get(i) );
			listActs.add((int) acts.get(i));
		}
		System.out.println("Size of list " + listActs.size());
		
		List<Activity> actS = new ArrayList<>();
		for(int i:listActs){
			actS.add(findByIdWithBanners(i));
		}
		
		session.close();
 		return actS;
	}

}
