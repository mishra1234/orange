package au.com.dmsq.dao.impl;

import java.util.List;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.BookingDao;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Booking;
import au.com.dmsq.model.Layer;

@Repository
public class BookingDaoImpl extends GenericDAOImpl<Booking, Integer> implements BookingDao{

	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);     
    }
	
	@Override
	public List<Booking> searchByActivityId(Integer activityId){
		
		return  search(
				new Search(Booking.class)
				.addFilterSome("activity", Filter.equal("id", activityId))
				);
	}
    

}
