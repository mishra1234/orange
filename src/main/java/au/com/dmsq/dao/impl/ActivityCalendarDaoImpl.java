package au.com.dmsq.dao.impl;

import java.util.List;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import au.com.dmsq.dao.ActivityCalendarDao;
import au.com.dmsq.model.ActivityCalendar;
import au.com.dmsq.model.Booking;

@Repository
public class ActivityCalendarDaoImpl extends GenericDAOImpl<ActivityCalendar, Integer> implements ActivityCalendarDao{
	
	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
 	}	 
		public List<ActivityCalendar> getCurrent()
		{
			 return search(new Search(ActivityCalendar.class).addFilterEqual("timestamp", 2014));
		}
		
		@Override
		public List<ActivityCalendar> searchByActivityId(Integer activityId){
			
			return  search(
					new Search(ActivityCalendar.class)
					.addFilterSome("activity", Filter.equal("id", activityId))
					.addFetches("activity","account")
					);
		}

		
		// This goes empty unless we implement our own methods
		// save, delete, findAll, get and others are already available
	
	/* Available Methods. 
	 * 
	 * 
	 *  public <T> T find(Class<T> type, Serializable id);

        public <T> T[] find(Class<T> type, Serializable... ids);

        public <T> T getReference(Class<T> type, Serializable id);

        public <T> T[] getReferences(Class<T> type, Serializable... ids);

        public boolean save(Object entity);

        public boolean[] save(Object... entities);

        public boolean remove(Object entity);

        public void remove(Object... entities);

        public boolean removeById(Class<?> type, Serializable id);

        public void removeByIds(Class<?> type, Serializable... ids);

        public <T> List<T> findAll(Class<T> type);

        public List search(ISearch search);

        public Object searchUnique(ISearch search);

        public int count(ISearch search);

        public SearchResult searchAndCount(ISearch search);

        public boolean isAttached(Object entity);

        public void refresh(Object... entities);

        public void flush();
	 * */	
}
