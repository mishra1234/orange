package au.com.dmsq.dao.impl;


import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.AccountConfirmationDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountConfirmation;


@Repository
public class AccountConfirmationDaoImpl extends GenericDAOImpl<AccountConfirmation, Integer> implements AccountConfirmationDao{
	
	
	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
 	}	
	
	@Override
	public AccountConfirmation findByAccount(AccountConfirmation acctConfirmation){
		return (AccountConfirmation) searchUnique(
				new Search(AccountConfirmation.class)
					.addFilterEqual("Account.id", acctConfirmation.getAccount().getId()) );
	}
			
}
