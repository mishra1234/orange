package au.com.dmsq.dao.impl;

import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import au.com.dmsq.dao.CatalogueDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.ActivityPayment;
import au.com.dmsq.model.Catalogue;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

public class CatalogueDaoImpl extends GenericDAOImpl<Catalogue, Integer>
		implements CatalogueDao {
	Session session = null;

	private static final Logger logger = LoggerFactory
			.getLogger(CatalogueDaoImpl.class);

	@Autowired
	@Override
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Override
	public Catalogue getCatalogueById(Integer id) {
		return (Catalogue) searchUnique(new Search(Catalogue.class).addFilterEqual(
				"id", id));
	}

}
