package au.com.dmsq.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.AccountDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Layer;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

@Repository
public class AccountDaoImpl extends GenericDAOImpl<Account, Integer> implements AccountDao{
	
	private static final Logger logger = LoggerFactory.getLogger(AccountDaoImpl.class);
	
	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
 	}	
	
	public List<Account> checkNonDeleted()
	{
		 return search(new Search(Account.class).addFilterEqual("deleted", false));
	}
		
	public Account getByResetCode(String tokenId) {
		
		return searchUnique(new Search(Account.class).addFilterEqual("resetCode", tokenId));
	}
	
	// Check this function performance 01/10/15
	@Override
	public Account findByIdWithLayers(Integer id) 
	{
		return (Account) searchUnique(
				new Search(Account.class)
					.addFilterEqual("id", id)
					.addFetches("layers_1","accountLayerAccesses", "layers", "layers_2") );
	}
	
	@Override
	public Account findALAsById(Integer id) 
	{
		return (Account) searchUnique(
				new Search(Account.class)
				.addFilterEqual("id", id)
				.addFetch("accountLayerAccesses")
				);
	}
	
	@Override
	public Account findLayersById(Integer id) 
	{
		return (Account) searchUnique(
				new Search(Account.class)
					.addFilterEqual("id", id)
					.addFetch("layers_1")
				);
	}
		
	@Override
	public Account findAcctByEmail(String email) {

		Search search = new Search(Account.class)
				.addFilterEqual("email", email).addFilterEqual("enabled", true)
				.addFetches("accountConfirmations");

		List<Account> accounts = search(search);
		try {
			return accounts.get(0);

		} catch (Exception e) {

			return new Account(); // empty value
		}
	}
		
		@Override
		public Account findByIdWithLayersFaster(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("layers_1","accountLayerAccesses") );
		}
		
		@Override
		public Account findByIdWithAccConfirmations(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("accountConfirmations") );
		}
		
		@Override
		public Account findWithLayers(Integer id) 
		{
			return (Account)searchUnique(
					new Search(Account.class).addFilterEqual("id", id).addFetch("layers") );
		}
		
		@Override
		public Account findByIdWithLayerManaged(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("layers","activities","activities_1"));
		}
		
		@Override
		public Account findByIdWithLayerManagedF(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetch("layers")
						);
		}
		
		@Override
		public Account findByIdWithRelations(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("activities","activityCalendars", "auths",
								"recommendedActivitieses","activities_1", "accountLayerAccesses", 
								"layers", "messagesForFromId", "bookings", "layers_1", 
								"accountConfirmations", "layers_2", "messagesForToId") );
		}
		
		@Override
		public Account findByIdWithRelationsFaster(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("activities","activityCalendars", "auths", 
								"accountLayerAccesses", 
								"layers", "bookings", "layers_1"));
		}
		@Override
		public Account findByIdActivities(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("activities"
								));
		}
		@Override
		public Account findAuthById(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("auths"
								));
		}
		@Override
		public Account findAccountLayerAccessesById(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("accountLayerAccesses"));
		}
		
		
		@Override
		public Account findLayerManagerById(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("layers"));
		}
		@Override
		public Account findConnectedLayersById(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("layers_1"));
		}
		@Override
		public Account findBookingsById(Integer id) 
		{
			return (Account) searchUnique(
					new Search(Account.class)
						.addFilterEqual("id", id)
						.addFetches("bookings"));
		}
		
		@Override
		public List<Account> getConnection(List<Layer> layerList) {
		  
			  logger.debug("Layer size: " + layerList.size());
			
				 return search(new Search(Account.class)
				.addFilterIn("layers_1", layerList));

		}
		
		// This goes empty unless we implement our own methods
		// save, delete, findAll, get and others are already available
	
	/* Available Methods. 
	 * 
	 * 
	 *  public <T> T find(Class<T> type, Serializable id);

        public <T> T[] find(Class<T> type, Serializable... ids);

        public <T> T getReference(Class<T> type, Serializable id);

        public <T> T[] getReferences(Class<T> type, Serializable... ids);

        public boolean save(Object entity);

        public boolean[] save(Object... entities);

        public boolean remove(Object entity);

        public void remove(Object... entities);

        public boolean removeById(Class<?> type, Serializable id);

        public void removeByIds(Class<?> type, Serializable... ids);

        public <T> List<T> findAll(Class<T> type);

        public List search(ISearch search);

        public Object searchUnique(ISearch search);

        public int count(ISearch search);

        public SearchResult searchAndCount(ISearch search);

        public boolean isAttached(Object entity);

        public void refresh(Object... entities);

        public void flush();
	 * */	
}
