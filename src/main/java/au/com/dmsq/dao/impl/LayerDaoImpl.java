package au.com.dmsq.dao.impl;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import au.com.dmsq.dao.LayerDao;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.MailContentService;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

@Repository
public class LayerDaoImpl extends GenericDAOImpl<Layer, Integer> implements LayerDao {
	
	Session session = null;
//	@Autowired AnAbstractService ab;
	@Autowired MailContentService mcs;
	private static final Logger logger = LoggerFactory.getLogger(CategoryDaoImpl.class);

	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);     
    }
	
	@Override
	public Layer findByIdBanners(Integer id) 
	{
		return (Layer) searchUnique(
				new Search(Layer.class)
				.addFilterEqual("id", id)
				.addFetches("banners")
					);
	}

		public List<Layer> getNonDeleted()
		{
			 return search(new Search(Layer.class).addFilterEqual("deleted", true));
		}
		
		@Override
		public int getActivityRows(String id){
	 		
	 		session=getSessionFactory().openSession();
	 		BigInteger  count = (BigInteger) session.createSQLQuery("SELECT count(layer_activities.activity_id) as activities from layer,  layer_activities " +
	 				"where id = "+id+" and layer.id = layer_activities.layer_id").uniqueResult();
	 		//logger.debug("Fetching Connections for id:"+ id +" result:"+ count.intValue());
	 		return count.intValue();
	 	}
		
		@Override
		public Map<String, Integer> getAccountsGroupByMonth(int id)throws Exception, IllegalAccessException, ClassNotFoundException{
			String connectionURLto =mcs.readDBConn();
			Connection connectionTo = null; 
			ResultSet rs = null;
			Map<String, Integer> map = new HashMap<String, Integer>();
			Map<String, Integer> mapWithAllValues = new HashMap<String, Integer>();
				
    	    Class.forName("com.mysql.jdbc.Driver").newInstance(); 
	        connectionTo = DriverManager.getConnection(connectionURLto, mcs.readDBUser(), mcs.readDBPass());
	        if(!connectionTo.isClosed()){
	        	Statement stmtTo = connectionTo.createStatement(); 	
	    			
				try{	
						String getUsersOnMonth= "Select count(account_id), MONTH(connection_date), YEAR(connection_date) from connected_layers where layer_id in (Select layer_id from layer_managers where account_id = '"+id+"' ) GROUP BY MONTH(connection_date), YEAR(connection_date) order by MONTH(connection_date), YEAR(connection_date)";
						System.out.println("Get the number of users grouped by month>>>"+getUsersOnMonth);
						rs = stmtTo.executeQuery(getUsersOnMonth);
						int currentMonth = Calendar.getInstance().get(Calendar.MONTH)+1;
						
						while(rs.next()){
						map.put(rs.getString("MONTH(connection_date)")+"-"+rs.getString("YEAR(connection_date)"),rs.getInt("count(account_id)"));
						
						
						String year=null;
						
						for(int i=1;i<=currentMonth;i++){
							mapWithAllValues.put(i+"-"+rs.getString("YEAR(connection_date)"), 0);
						}
						}
						Iterator it = map.entrySet().iterator();
						
						while(it.hasNext()){
							Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
							
							System.out.println("Key is --------------------------"+pair.getKey());
								mapWithAllValues.put(pair.getKey(), pair.getValue());
							}
						
							
					
				Iterator it1 = mapWithAllValues.entrySet().iterator();
				
				while(it1.hasNext()){
					Map.Entry<String, Integer> pair1 = (Entry<String, Integer>) it1.next();
				System.out.println(pair1.getKey() +" : "+ pair1.getValue());
				}
				}
					catch(Exception e){
					System.out.println("Exception is: "+e);
				}
			
		}
	        return mapWithAllValues; 
		}
		
		@Override
		public Map<String, Integer> getAccountsGroupByMonthPerCom(int id)throws Exception, IllegalAccessException, ClassNotFoundException{
			String connectionURLto =mcs.readDBConn();
			Connection connectionTo = null; 
			ResultSet rs = null;
			Map<String, Integer> map = new HashMap<String, Integer>();
			Map<String, Integer> mapWithAllValues = new HashMap<String, Integer>();
			int currentMonth = Calendar.getInstance().get(Calendar.MONTH)+1;
					
    	    Class.forName("com.mysql.jdbc.Driver").newInstance(); 
	        connectionTo = DriverManager.getConnection(connectionURLto, mcs.readDBUser(), mcs.readDBPass());
	        if(!connectionTo.isClosed()){
	        	Statement stmtTo = connectionTo.createStatement(); 	
	    			
				try{	
						String getUsersOnMonth= "Select count(account_id), MONTH(connection_date), YEAR(connection_date) from connected_layers where layer_id = '"+id+"' GROUP BY MONTH(connection_date), YEAR(connection_date) order by MONTH(connection_date), YEAR(connection_date)";
						System.out.println("Get the number of users grouped by month>>>"+getUsersOnMonth);
						rs = stmtTo.executeQuery(getUsersOnMonth);
						while(rs.next()){
						map.put(rs.getString("MONTH(connection_date)")+"-"+rs.getString("YEAR(connection_date)"),rs.getInt("count(account_id)"));
						
						String year=null;
						
						for(int i=1;i<=currentMonth;i++){
							mapWithAllValues.put(i+"-"+rs.getString("YEAR(connection_date)"), 0);
						}
					
						}
						Iterator it = map.entrySet().iterator();
						
						while(it.hasNext()){
							Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
							
							System.out.println("Key is --------------------------"+pair.getKey());
								mapWithAllValues.put(pair.getKey(), pair.getValue());
							}
					
				Iterator it1 = mapWithAllValues.entrySet().iterator();
				
				while(it1.hasNext()){
					Map.Entry<String, Integer> pair1 = (Entry<String, Integer>) it1.next();
				System.out.println(pair1.getKey() +" : "+ pair1.getValue());
				}
				}
					catch(Exception e){
					System.out.println("Exception is: "+e);
				}
			
		}
	        return mapWithAllValues; 
		}
		
		
		@Override
		public List<Layer> findLayerByCat(Category category){
			return search(new Search(Layer.class).addFilterEqual("layer.categories", category.getId()));
		}
		
		@Override
		public Layer findByIdActivities(Integer id){
			 return (Layer)searchUnique(new Search(Layer.class)
			 			.addFilterEqual("id", id)
			 			.addFilterEqual("deleted", false)
			 			.addFilterEqual("type", "public")
			 			.addFetch("activities"));
		}
		
		//FIXME - CARLOS
		@Override
		public Layer findInfoById(Integer id){
			 return (Layer)searchUnique(new Search(Layer.class)
			 			.addFilterEqual("id", id)
			 			.addFilterEqual("deleted", false)
			 			.addFilterEqual("activityType", "info")
			 			.addFetch("activities"));
		}
		
		@Override
		public Layer findByIdActivitiesAll(Integer id){
			 return (Layer)searchUnique(new Search(Layer.class)
			 			.addFilterEqual("id", id)
			 			.addFilterEqual("deleted", false)
			 			.addFetches("activities")
			 			);
		}
		
		
		@Override
		public Layer findByIdGatedActivities(Integer id){
			 return (Layer)searchUnique(new Search(Layer.class)
			 			.addFilterEqual("id", id)
			 			.addFilterEqual("deleted", false)
			 			.addFilterEqual("type", "gated")
			 			.addFetches("activities","accountLayerAccesses"));
		}
		
		@Override
		public Layer findByIdWithRelations(Integer id) 
		{
			return (Layer) searchUnique(new Search(Layer.class)
				.addFilterEqual("id", id)
				.addFetches("layersForChildLayerId","layersForParentLayerId","categories","banners","accounts",
						    "accounts_1","activities","accountLayerAccesses","messages","activities_1"));
		}
		
		@Override
		public Layer findByIdWithRequiredRelations(Integer id) 
		{
			return (Layer) searchUnique(new Search(Layer.class)
				.addFilterEqual("id", id)
				.addFetches(
					"layersForChildLayerId",	
					"banners", 
					"accounts", 
					"activities"));
		}
		
		@Override
		public Layer findByIdwithParentChildCategories(Integer id) 
		{
			return (Layer) searchUnique(new Search(Layer.class).addFilterEqual("id", id).addFetches("layersForChildLayerId", 
					"layersForParentLayerId", "categories", "banners"));
		}
		
		@Override
		public Layer findWithManagers(Integer id) 
		{
			return (Layer) searchUnique( new Search(Layer.class).addFilterEqual("id", id).addFetches("accounts", "layersForChildLayerId", 
					"layersForParentLayerId", "activities"));
		}
		
		
		@Override
		public List<Layer> searchByMgr(Integer initial, Integer max, List<Layer> layers) 
		{
			return search(new Search(Layer.class)
				.addFetches("accounts").addFilterIn("accounts", layers).setFirstResult(initial).setMaxResults(max));
		}
		
		@Override
		public Layer findByIdWithActivities(Integer id) 
		{
			return (Layer) searchUnique(
					new Search(Layer.class)
					.addFilterEqual("id", id)
					.addFilterEqual("activities.deleted", false)
					.addFetches("account","activities","accounts"));
		}
		
		@Override
		public Layer findByIdWithActsEv(Integer id) 
		{
			return (Layer) searchUnique(
					new Search(Layer.class)
					.addFilterEqual("id", id)
					//.addFilterEqual("activities.deleted", false)
					.addFetches("account","activities","accounts"));
		}
		
		@Override
		public Layer findByIdWithActsInf(Integer id) 
		{
			return (Layer) searchUnique(
					new Search(Layer.class)
					.addFilterEqual("id", id)
					.addFilterEqual("activities.deleted", false)
					.addFetches("account","activities","accounts"));
		}
		
		
		@Override
		public Layer findActivitiesByIdByType(String type, Integer id) 
		{
			return (Layer) searchUnique(
					new Search(Layer.class)
					.addFilterEqual("id", id)
					.addFilterEqual("activities.activityType", type)
					.addFilterEqual("activities.deleted", false)
					.addFetches("account","activities","accounts"));
		}
		
		
		@Override
		public Layer findEventsByIdWithActivities(String type, Integer id) 
		{
			if (type.equals("event"))
			{
				long epoch = (System.currentTimeMillis()+34200)/1000;  	// +30600 if summer time, +34200 if daylight saving time
				
				return (Layer) searchUnique(
						new Search(Layer.class)
						.addFilterEqual("id", id)
						.addFilterEqual("activities.activityType", type)
						.addFilterEqual("activities.deleted", false)
						.addFilterGreaterThan("activities.event.eventCalendarItems.dtend", epoch)
						.addFetches("account","activities","accounts"));
				
				}else{
					
					return (Layer) searchUnique(
							new Search(Layer.class)
							.addFilterEqual("id", id)
							.addFilterEqual("activities.activityType", type)
							.addFilterEqual("activities.deleted", false)
							.addFetches("account","activities","accounts"));
				}
		}
		
		
		@Override
		public Layer findByIdWithAccounts(Integer id) 
		{
			return (Layer) searchUnique(
					new Search(Layer.class)
					.addFilterEqual("id", id)
					.addFetches("account","accounts"));
		}


//		@Override
//		public Layer find(Integer id) {
//			return (Layer) searchUnique(
//					new Search(Layer.class)
//					.addFilterEqual("id", id));
//		}



		@Override
		public List<Layer> getRegionalCouncils(){
	
	 		Layer cl=(Layer) searchUnique(new Search(Layer.class)
			.addFilterEqual("id", 70530));
	 		List<Layer> regionals=new ArrayList<Layer>();
	 		Layer [] inter =new Layer[100];
	 		regionals.addAll(cl.getLayersForChildLayerId());
	 		  Collections.sort(regionals, new Comparator<Layer>() {
	 		        @Override
	 		        public int compare(final Layer object1, final Layer object2) {
	 		            return object1.getName().compareTo(object2.getName());
	 		        }
	 		       } );
	 		return  regionals;
		}
		
		@Override
		public List<Layer> getStatusCouncils(){
			List<Layer> regionals=new ArrayList<Layer>();
			
			List<Integer> list=new ArrayList<>();
			list.add(120134);
			list.add(120135);
			list.add(120136);
			list.add(120137);
			list.add(120138);
			list.add(120139);
			list.add(120140);
			list.add(120141);
			
			for(int x:list){
				regionals.add((Layer) searchUnique(new Search(Layer.class).addFilterEqual("id", x)));
			}
	 		
	 		return  regionals;
		}


		@Override
		public List<Layer> getManagedLayers(Integer accountId) {
			logger.debug("Account id="+accountId);
			 return search(new Search(Layer.class)
				.addFilterSome("accounts", Filter.equal("id", accountId))   
				.addSortAsc("name")
				.addFetches("account","accounts","activities")
				.setDistinct(true)
			 		);
		}
		
		
		public Layer findLayerWithActivitiesAndAccess(Integer accountId) {
	
			return (Layer) searchUnique(new Search(Layer.class)
			.addFilterEqual("id", accountId)
			.addFilterEqual("deleted", false)
			.addFetches("layersForChildLayerId", "accounts")
			.addSortAsc("name"));
		}
		
		
		@Override
		public List<Activity> findEventsbyLayer(Layer layer, Long dtend){
	
			Long dtEnd = (dtend/1000)-34200;
			
			return search(
					new Search(Layer.class)
					.addFilterEqual("id", 70515)
					//.addFilterEqual("activities.deleted", false)
					//.addFilterEqual("activities.activityType", "event")
					//.addFilterGreaterOrEqual("activities.event.eventCalendarItems.dtend", dtEnd)
					.addFetches("account","activities"));
		}
		
		@Override
		public List<Layer> findEventsbyLayerId(Integer id){
			
			return search(
					new Search(Layer.class)
					.addFilterEqual("id", id)
					.addSortAsc("id")
					.addFetches("account","activities"));
		}
		
		
		@Override
		public Layer findByIdWithActivitiesandAccountId( Integer layerId, String type) 
		{
			//long epoch = (System.currentTimeMillis()+34200)/1000;  	// +30600 if summer time, +34200 if daylight saving time
			return (Layer) searchUnique(
					new Search(Layer.class)
					.addFilterEqual("id", layerId)
					.addFilterEqual("activities.activityType", type)
					.addFilterEqual("activities.deleted", false)
					.addFetches("account","activities","accounts"));
					//.addFilterGreaterThan("activities.event.eventCalendarItems.dtend", epoch)
		}

//		@Override
//		public Layer find(Integer id) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//		@Override
//		public Layer[] find(Integer... ids) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//		@Override
//		public Layer getReference(Integer id) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//		@Override
//		public Layer[] getReferences(Integer... ids) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//		@Override
//		public boolean removeById(Integer id) {
//			// TODO Auto-generated method stub
//			return false;
//		}
//
//		@Override
//		public void removeByIds(Integer... ids) {
//			// TODO Auto-generated method stub
//			
//		}
//

}