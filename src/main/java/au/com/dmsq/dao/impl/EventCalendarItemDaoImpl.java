package au.com.dmsq.dao.impl;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.dmsq.dao.ActivityDao;
import au.com.dmsq.dao.EventCalendarItemDao;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.EventCalendarItemId;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

public class EventCalendarItemDaoImpl extends GenericDAOImpl<EventCalendarItem, Integer> implements EventCalendarItemDao{
	
	Session session = null;
	@Autowired ActivityDao activityDao;
	@Autowired EventCalendarItemDao eventCalendarItemDao;
	
 	@Autowired
 	@Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
 	}
 	
 	public List<EventCalendarItem> getNonDeleted(){
		return search(new Search(EventCalendarItem.class).addFilterEqual("deleted", false));
	}
 	
 	// This function should search for all eventcalendaritems of a speficic event, not used yet 
 	public List<EventCalendarItem> findByActIdwRelations(Event event){
 		session=getSessionFactory().openSession();
 		Query query = session.createSQLQuery("SELECT event.activityId FROM au.com.dmsq.model.EventCalendarItem WHERE event.activityId="+event.getActivityId());
 		List<EventCalendarItem> listCategories = query.list();
 		return listCategories;
 	}
 	
 	@Override
 	public List<Activity> findByActId(Integer id){
 		return search(new Search(Activity.class).addFilterEqual("deleted", false));
 	}

 	@Override
	public EventCalendarItem findByEciId(EventCalendarItemId eciId) {
		return (EventCalendarItem) searchUnique(
				new Search(EventCalendarItem.class)
				   .addFilterEqual("id", eciId)
		);
	}
 	
 	@Override
	public EventCalendarItem findByActIDandHref(Activity act, String href, Integer actId) {
 		try{
			return  (EventCalendarItem) searchUnique(
				new Search(Activity.class)
				.addFilterEqual("event.eventCalendarItems.id.activityId", actId)
				.addFilterEqual("event.eventCalendarItems.id.calHref", href)
				);
		}catch(Exception ex) {
	    	System.out.println("Exception : " + ex.toString());
	    }
 		return new EventCalendarItem();
	}
 	
}
