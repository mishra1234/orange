package au.com.dmsq.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.EventDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Event;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

@Repository
public class EventDaoImpl extends GenericDAOImpl<Event, Integer> implements EventDao {


 	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);     
    }
 	
 	public List<Event> getNonDeleted()
	{
		 return search(new Search(Event.class).addFilterEqual("deleted", false));
	}

 	@Override
 	public Event findByIdWithRelations(Integer activityId) {
		return (Event) searchUnique(new Search(Event.class)
			.addFilterEqual("id", activityId)
			.addFetches("activity.account", "activity.bookings", 
						"activity.files", "activity.recommendedActivitiesesForActivityId",
						"activity.recommendedActivitiesesForRecommendedActivityId", 
						"activity.activitiesForChildActivityId", "activity.layers", 
						"activity.messages", "activity.activityCalendars", 
						"activity.accounts", "activity.activitiesForActivityId", 
						"activity.layers_1"
		)); 
	}	  
}