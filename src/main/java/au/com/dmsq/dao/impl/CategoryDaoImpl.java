package au.com.dmsq.dao.impl;

import java.io.Serializable;
import java.util.List;

import java.math.BigInteger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

import au.com.dmsq.dao.CategoryDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.Layer;
import au.com.dmsq.web.CategoryController;

@Repository
public class CategoryDaoImpl extends GenericDAOImpl<Category, Integer> implements CategoryDao {

		Session session = null;
	
		private static final Logger logger = LoggerFactory.getLogger(CategoryDaoImpl.class);
		
	 	@Autowired
	    @Override
	    public void setSessionFactory(SessionFactory sessionFactory) {
	        super.setSessionFactory(sessionFactory);
	 	}
	 	
	 	
	 	public List<Category> getNonDeleted()
		{
			 return search(new Search(Category.class).addFilterEqual("deleted", false));
		}
	 	
	 	@Override
	 	public Category getCategoryCommunities(String id)
		{
			 return (Category)searchUnique(new Search(Category.class).addFilterEqual("id", id).addFetch("layers"));
		}
	 	
	 	public int getConnRows(String id){
	 		
	 		session=getSessionFactory().openSession();
	 		BigInteger  count = (BigInteger) session.createSQLQuery("SELECT count(connected_layers.layer_id) as connection from layer, layer_categories, connected_layers where category_id = "+id+" and layer.id = layer_categories.layer_id and connected_layers.layer_id = layer.id").uniqueResult();
	 		//logger.debug("Fetching Connections for id:"+ id +" result:"+ count.intValue());
	 		return count.intValue();
	 	} 
		public int getLayerRows(String id){
	 		
			session=getSessionFactory().openSession();
	 		BigInteger  count = (BigInteger) session.createSQLQuery("SELECT count(layer_categories.layer_id) as com from layer, layer_categories where category_id = "+id+" and layer.id = layer_categories.layer_id").uniqueResult();
	 		//logger.debug("Fetching Connections for id:"+ id +" result:"+ count.intValue());
	 		return count.intValue();
	 	}  
		public int getEventRows(String id){
	 		
			session=getSessionFactory().openSession();
	 		BigInteger  count = (BigInteger) session.createSQLQuery("SELECT count(layer_activities.activity_id) as activities from layer, layer_categories, layer_activities where category_id = "+id+" and layer.id = layer_categories.layer_id and layer_categories.layer_id = layer_activities.layer_id").uniqueResult();
	 		//logger.debug("Fetching Connections for id:"+ id +" result:"+ count.intValue());
	 		return count.intValue();
	 	} 

	/* Available Methods. 
	 * 
	 * 
	 *
	 *  
	 	public <T> T find(Class<T> type, Serializable id);

        public <T> T[] find(Class<T> type, Serializable... ids);

        public <T> T getReference(Class<T> type, Serializable id);

        public <T> T[] getReferences(Class<T> type, Serializable... ids);

        public boolean save(Object entity);

        public boolean[] save(Object... entities);

        public boolean remove(Object entity);

        public void remove(Object... entities);

        public boolean removeById(Class<?> type, Serializable id);

        public void removeByIds(Class<?> type, Serializable... ids);

        public <T> List<T> findAll(Class<T> type);

        public List search(ISearch search);

        public Object searchUnique(ISearch search);

        public int count(ISearch search);

        public SearchResult searchAndCount(ISearch search);

        public boolean isAttached(Object entity);

        public void refresh(Object... entities);

        public void flush();
	 * */	
}

