package au.com.dmsq.dao.impl;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.FileDao;
import au.com.dmsq.model.File;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

@Repository
public class FileDaoImpl extends GenericDAOImpl<File, Integer> implements FileDao{
	
	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
 	}	 
		public List<File> checkNonDeleted()
		{
			 return search(new Search(File.class).addFilterEqual("deleted", false));
		}

}

