package au.com.dmsq.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.MailDao;
import au.com.dmsq.model.Mail;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;

@Repository
public class MailDaoImpl extends GenericDAOImpl<Mail, Integer> implements MailDao{

	@Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
 	}
}
