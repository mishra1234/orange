package au.com.dmsq.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.com.dmsq.dao.AuthDao;
import au.com.dmsq.model.Auth;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;

@Repository
public class AuthDaoImpl extends GenericDAOImpl<Auth, Integer> implements AuthDao {


	 	@Autowired
	    @Override
	    public void setSessionFactory(SessionFactory sessionFactory) {
	        super.setSessionFactory(sessionFactory);     
	    }
}