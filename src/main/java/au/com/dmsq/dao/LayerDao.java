package au.com.dmsq.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.Layer;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface LayerDao extends GenericDAO <Layer, Integer>
{
	
 	public List<Layer> getNonDeleted();

	int getActivityRows(String id);

	public List<Layer> findLayerByCat(Category category);
	
	public Layer findByIdActivities(Integer id);

	public Layer findByIdWithRelations(Integer id);
	
	public Layer findByIdBanners(Integer id);

	public Layer findWithManagers(Integer id);
	
	public List<Layer> searchByMgr(Integer initial, Integer max, List<Layer> layers);

	public Layer findByIdWithActivities(Integer id);  
	
	public List<Layer> getRegionalCouncils();

	public Layer findByIdGatedActivities(Integer id);

	public Layer findByIdWithAccounts(Integer id);

	public Layer findEventsByIdWithActivities(String type, Integer id);

	public List<Layer> getStatusCouncils();

	public Layer findByIdActivitiesAll(Integer id);

	public List<Layer> getManagedLayers(Integer accountId);

	public Layer findLayerWithActivitiesAndAccess(Integer id);

	public Layer findInfoById(Integer id);

	public Layer findByIdWithActsEv(Integer id);

	public Layer findByIdWithActsInf(Integer id);

	public Layer findActivitiesByIdByType(String type, Integer id);

	public List<Activity> findEventsbyLayer(Layer layer, Long dtend);

	public Layer findByIdWithActivitiesandAccountId(Integer layerId, String type);

	public Layer findByIdwithParentChildCategories(Integer id);

	public List<Layer> findEventsbyLayerId(Integer id);

	//public List<Layer> getConnectedManagedLayers(Integer accountId);

	
	public Map<String, Integer> getAccountsGroupByMonth(int id)
			throws Exception, IllegalAccessException, ClassNotFoundException;

	public Map<String, Integer> getAccountsGroupByMonthPerCom(int id) throws Exception,
			IllegalAccessException, ClassNotFoundException;

	public Layer findByIdWithRequiredRelations(Integer id);

}