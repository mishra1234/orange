package au.com.dmsq.web;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Layer;

public class CatalogueController {
	private static final Logger logger = LoggerFactory.getLogger(CommunityController.class);

	@RequestMapping(value="/admin/catalogue/new")
	public ModelAndView newCatalogue() throws Exception {
		
		logger.debug("------------------------------ Admin - New Catalogue ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
//		
//		Layer layer =  new Layer();
//		
//		java.util.Date date= new java.util.Date();
//		System.out.println(new Timestamp(date.getTime()));
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
//		String startDatea = sdf.format(date);
//		
//		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		List<Account> accounts = accountService.findByEmail(user.getUsername());
//		
//		for(Account str: accounts){
//			if(accounts.isEmpty()){
//				return mav;
//			}
//		}
//		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
//		
//		System.out.println("Connected User >>>: "+acct.getEmail());
//		Set<Layer> layers = acct.getLayers();
//		Set<Account> listofManagers = new HashSet<Account>();
//		Set<Account> setAccounts = new HashSet<Account>();
//		Set<Integer> sett = new HashSet<Integer>();
//		
//		if(layers.isEmpty()){
//			logger.debug("----- setAccounts.size() ------>>>> "+setAccounts.size());
//			setAccounts.add(getCurrentAccount());
//			logger.debug("----- setAccounts.size() ------>>>> "+setAccounts.size());
//		}
//		
//		for(Layer community: layers){
//			setAccounts.addAll(communityService.getLayerWithManagers(community.getId()).getAccounts());
//		}
//		for(Account account:setAccounts){
//			sett.add(account.getId());
//		}
//		for(Integer intger:sett){
//			listofManagers.add(accountService.get(intger));
//		}
//		
//		mav.addObject("iUrl", 0);  //Current Index of the iconUrl to send to JSP
//		mav.addObject("community", layer);
//		mav.addObject("listOfCommunities", layers);
//		mav.addObject("listOfOwners", listofManagers);
//		mav.addObject("listOfCategories", categoryService.getNonDeleted());
//		mav.addObject("currentTS", startDatea);
//		mav.addObject("currentUser", acct.getId());
		
		mav.setViewName("/admin/catalogue/newCatalogue"); //jsp
		
		return mav; 
	}
	
	@RequestMapping(value="/admin/catalogue")
	public ModelAndView catalogueView() throws Exception {
		
		logger.debug("------------------------------ Admin - Display the catalogue ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		List<Layer> listOfCatItems = new ArrayList<Layer>();
		
		mav.setViewName("/admin/catalogue/catalogue"); //jsp
		return mav; 
	}

}
