package au.com.dmsq.web;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.File;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;

@Controller
public class surveyController extends AbstractController 
{
	
	private static final Logger logger = LoggerFactory.getLogger(surveyController.class);

	@Autowired ActivityService activityService;
	@Autowired AccountService accountService;
	@Autowired BannerService bannerService;
	@Autowired CommunityController commController;

	@Transactional
	@RequestMapping(value="/admin/surveys/{layerId}/{curr}")
	public ModelAndView surveys(@PathVariable(value="layerId") Integer layerId, @PathVariable(value="curr") Integer curr)
	{
		logger.debug("<----------------------------------Admin - List of Paged Surveys ------------------------------------------>");
		
		ModelAndView mav = new ModelAndView();
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		Integer max = 20;
		Integer initial=0;
		Integer prev=1;
		Integer next=2;
		Integer hmany = (activityService.countSurvey(acct.getId())/max)+1;
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		
		if(curr==1 || curr<1){
			initial=0;
			prev=1;
			next=2;
			List<Activity> pagedInfs= activityService.pagedSurveysByLayer(initial, max, acct.getId(), layerId);
			mav.addObject("mySurveyList", pagedInfs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr>1 && curr < hmany){
			max=(curr)*20;
			initial= (curr-1)*20;
			prev=curr-1;
			next=curr+1;
			List<Activity> pagedInfs= activityService.pagedSurveysByLayer(initial, max, acct.getId(), layerId);
			mav.addObject("mySurveyList", pagedInfs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr==hmany){
			initial= (curr-1)*20;
			max=initial + (activityService.pagedSurveysByLayer(initial, activityService.countSurvey(acct.getId())%max,acct.getId(),layerId).size())%20;
			prev=curr-1;
			next=curr+1;
			List<Activity> pagedInfs= activityService.pagedSurveysByLayer(initial, activityService.countSurvey(acct.getId())%max,acct.getId(),layerId);
			mav.addObject("mySurveyList", pagedInfs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}

		mav.addObject("hmany", hmany);
		mav.addObject("curr", curr);
		mav.addObject("numbers", numbers);
		
		mav.setViewName("/admin/survey/surveys"); // jsp
		return mav; 
	}

	@RequestMapping(value="/admin/survey/{id}", method= RequestMethod.GET)
	public ModelAndView getSurvey(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------Admin Get Single Survey  ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity act = activityService.findByIdWithBanners(id); 

		Set<Banner> bannList = new HashSet<Banner>();
		for (Banner ids: act.getBanners()){
			bannList.add(bannerService.get(ids.getId()));
		}
		
		mav.addObject("listOfBanners", bannList);
		mav.addObject("inf", act);
		mav.setViewName("/admin/survey/survey"); //jsp
		return mav; 
	}

	@RequestMapping(value="/admin/survey/editSurvey/{id}", method= RequestMethod.GET)
	public ModelAndView editSurvey(@PathVariable(value="id") Integer id, Model model) throws Exception {
		
		logger.debug("------------------------------ Admin - Edit Survey ------------------------------");
		System.out.println("Controller - EditEvent/ Survey : " + id);
		
		ModelAndView mav = new ModelAndView();
			
			Activity newAct = activityService.findByIdWithRelations(id);
		
			mav.addObject("bannerList", newAct.getBanners());
			
			for (Banner myBanner: newAct.getBanners()){
				logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+myBanner.getTitle());
			}
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String currDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layers){
			setAccounts.addAll(layerService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		mav.addObject("layers", newAct.getLayers());
		mav.addObject("newActSurvey", newAct);
		mav.addObject("currentTS", currDatea);
		mav.addObject("currentUser", accounts.get(0).getId());
		mav.addObject("listOfOwners", listofManagers);
		mav.addObject("managedLayers", layers);
		
		mav.setViewName("/admin/survey/newSurvey"); //jsp	
		return mav; 
	}
	
	
	
	@RequestMapping(value="/admin/survey/save", method= RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("newActSurvey") @Valid Activity newActSurvey, BindingResult result, SessionStatus status, 
			@RequestParam("commodities") String[] commodities,
			@RequestParam("actionUrl") String actionUrl,
			@RequestParam("contentBody") String contentBody,
			@RequestParam(value="titleBanner[]", defaultValue="", required=false) String[] titleBanner,	
			@RequestParam(value="subtitleBanner[]", defaultValue="", required=false) String[] subtitleBanner,	
			@RequestParam(value="bannerUrl[]", defaultValue="", required=false) String[] urlBanner, 		
			@RequestParam(value="bannerId[]", defaultValue="", required=false) Integer[] bannerId)throws SQLException, Exception{
		
		logger.debug("------------------------------ Admin - Save Survey ------------------------------");
		
		ModelAndView mav = setBasicData();
		Account acct = getCurrentAccount();
		
		for (Integer ids: bannerId){
			logger.debug("->Banner Ids:" + ids);
		}
		
		boolean isNew = true;
		System.out.println("ActId:         " + newActSurvey.getId());
		if (newActSurvey.getId() != null){ 
			isNew=false;
			System.out.println("Is new?:      " + isNew);
		}
		
		System.out.println("Result Errors: " + result.toString());
		if (result.hasErrors()) {
			mav.setViewName("/admin/survey/newSurvey"); //jsp
			return mav;
		}else{
			Activity saveActivity = new Activity();
						
			if(isNew){	
				saveActivity = newActSurvey;
				
				Set<Layer> commoditySet = new HashSet<Layer>();
				if (commodities != null) {
					for (String idCommodity:commodities){
							try{
							Layer tempLayer = layerService.findById(Integer.parseInt(idCommodity));
							if (tempLayer != null)
								commoditySet.add(tempLayer);
							}catch(Exception e){
								logger.debug("Number Exception - adding commodity to event");
							}
					}
				}
				saveActivity.setLayers(commoditySet);
			}else{
	        	saveActivity = activityService.findByIdWithRelations(newActSurvey.getId());
	        	
	        	Set<Integer> layers = new HashSet<>();
	        	Set<Integer> layersB = new HashSet<>();
	        	Set<Layer> layersCurr = saveActivity.getLayers();
	        	Set<Layer> layersNew = new HashSet<>();
	        	
	        	for (Layer x:layersCurr){
					layers.add(x.getId());
				}
				
	        	for (String id:commodities){
					layersB.add(Integer.parseInt(id));
				}				
				
				layersB.removeAll(layers);
				layersB.addAll(layers);				
				
				for (Integer idCommodity:layersB){
					Layer tempLayer = layerService.findById(idCommodity);
					layersNew.add(tempLayer);
				}
				
	        	saveActivity.setLayers(layersNew);
	        }
	        
			if(newActSurvey.getContentTitle().equalsIgnoreCase("")){
				saveActivity.setContentTitle("Survey created by "+acct.getName());
			}
			if(newActSurvey.getRecommended()==null){
				saveActivity.setRecommended(false);
			}
			if(newActSurvey.getContentBody()==null){
				saveActivity.setContentBody("No description is given for the survey.");
			}
			if(newActSurvey.getIconUrl().equalsIgnoreCase("")){
				saveActivity.setIconUrl("https://s3.amazonaws.com/ccmeresources/ca024ce7-b753-4351-9e83-45cdf0cbdbdd-logoRuralConnect.png");
			}
			
			saveActivity.setActionUrl(actionUrl);
			saveActivity.setIconUrl(newActSurvey.getIconUrl());
			saveActivity.setActionUrl(newActSurvey.getActionUrl());
			saveActivity.setContentTitle(newActSurvey.getContentTitle());
			saveActivity.setContentBody(newActSurvey.getContentBody());
			Date currentDT= new Date();
	        System.out.println("Current Time Stamp: "+new Timestamp(currentDT.getTime()));
			saveActivity.setCreatedDatetime(currentDT);
			
			if(urlBanner.length!=0){
				Set<Banner> banners = new HashSet<Banner>();
				for(int ba=0; ba<urlBanner.length; ba++){
					Banner newBanner = new Banner();
					System.out.println("VAR bannerId #: "+ bannerId.length);
					System.out.println("VAR BA: "+ ba);
					
					if(bannerId.length > ba && !bannerId[ba].equals("")){
						System.out.println("Var BA: "+ ba);
						System.out.println("Var BannerId[ba]: "+ bannerId[ba]);
						System.out.println("Banner Id: "+ newBanner.getId());
						newBanner=bannerService.findById(new Integer(bannerId[ba]));
						System.out.println("Banner: "+ titleBanner[ba]);
						newBanner.setTitle(titleBanner[ba]);			
						System.out.println("Banner: "+ subtitleBanner[ba]);
						System.out.println("Banner: "+ urlBanner[ba]);
						newBanner.setSubtitle(subtitleBanner[ba]);		  
						bannerService.save(newBanner);
					}
					banners.add(newBanner);
				}
				saveActivity.setBanners(banners);
			}
			if(urlBanner.length==0){
				Set<Banner> banners = new HashSet<Banner>();
				banners.add(bannerService.findById(3));
				saveActivity.setBanners(banners);
			}
			
			mav.addObject("ActSurvey", activityService.save(saveActivity));
			mav = commController.communities(); 
			return mav;	
		}
		
	}
	
	@RequestMapping(value="/admin/survey/delete/{id}")
	public String delete(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Delete Single survey ------------------------------");

		Activity actInfo = activityService.get(id); 
		actInfo.setDeleted(true);
		activityService.save(actInfo);
		
		ModelAndView mav =  commController.communities();
		mav.addObject("message", activityService.save(actInfo));
		
		return "redirect:/admin/surveys/";
	}
	
	
	@RequestMapping(value="/admin/survey/new/{commId}")
	public ModelAndView newSurvey(@PathVariable(value="commId") Integer commId) throws Exception {
		
		logger.debug("------------------------------ Admin - New Survey ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity newAct = new Activity();
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layers){
			setAccounts.addAll(layerService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		newAct.setOwnerCommunity(commId); // New Survey is set to be owned by the community passed as commID
		
		mav.addObject("layers", layerService.getNonDeleted());
		mav.addObject("iUrl", 0);	
		mav.addObject("newActSurvey", newAct);
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		mav.addObject("listOfOwners", listofManagers);
		mav.addObject("managedLayers", layers);
		
		mav.setViewName("/admin/survey/newSurvey"); //jsp
		return mav; 
	}
	
}

