package au.com.dmsq.web;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Mail;
import au.com.dmsq.model.Message;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.MailService;
import au.com.dmsq.service.MessageService;
import au.com.dmsq.service.PushService;

@Controller
public class MessageController extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired MessageService messageService;
	@Autowired AccountService accountService;
	@Autowired LayerService communityService;
	@Autowired ActivityService activityService;
	@Autowired MailService mailService;
	@Autowired PushService pushService;
	
	@Transactional
	@RequestMapping(value="/admin/messages", method= RequestMethod.GET)
	public ModelAndView messages() {
		
		logger.debug("------------------------------ Admin - List Messages ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		
		Integer max = 20;
		Integer initial=( (messageService.count())-max);
		Integer prev=1;
		Integer next=2;
		Integer hmany = (messageService.count()/max)+1;
		List<Message> pagedEvs= messageService.pagedMessages(initial, max);
		mav.addObject("myMessagesList", pagedEvs);
		mav.addObject("prev", prev);
		mav.addObject("next", next);
		mav.addObject("hmany", hmany);
		mav.addObject("curr", 1);
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		mav.addObject("hmany", hmany);
		mav.addObject("numbers", numbers);

		mav.setViewName("/admin/message/messages"); // jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/messages/{curr}")
	public ModelAndView pagedMessages(@PathVariable(value="curr") Integer curr) {
		
		logger.debug("------------------------------Admin - List Paged Messages------------------------------");
		ModelAndView mav = new ModelAndView(); 
		
		
		Integer max = 20;
		Integer next = 0;
		Integer prev = 0;
		Integer initial= 0;
		Integer hmany = (messageService.count()/max)+1;
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		
		if(curr==1||curr<1){
			initial=( (messageService.count())-max);
			prev=1;
			next=2;
			List<Message> pagedMsgs= messageService.pagedMessages(initial,max);
			mav.addObject("myMessagesList", pagedMsgs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr>1 && curr < hmany){
			initial= (messageService.count())-(curr*max);
			prev=curr-1;
			next=curr+1;
			List<Message> pagedMsgs= messageService.pagedMessages(initial, max);
			mav.addObject("myMessagesList", pagedMsgs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr==hmany){
			initial= 0;
			prev=curr-1;
			next=curr;
			List<Message> pagedMsgs= messageService.pagedMessages(initial, messageService.count()%max);
			mav.addObject("myMessagesList", pagedMsgs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		
		mav.addObject("hmany", hmany);
		mav.addObject("curr", curr);
		mav.addObject("numbers", numbers);
		
		mav.setViewName("/admin/message/messages"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/messages", method= RequestMethod.GET)
	public ModelAndView messagesMgr() {
		
		logger.debug("------------------------------ Manager - List Messages ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/manager/message/messages"); // jsp
		
		List<Message> listMessage = messageService.getNonDeleted(); 
		mav.addObject("myMessagesList", listMessage);
		return mav; 
	}
	
	@RequestMapping(value="/admin/message/{id}", method= RequestMethod.GET)
	public ModelAndView getMessage(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get Message ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/admin/message/message"); //jsp
		
		Message mess = messageService.findById(id); 
		mav.addObject("mess", mess);
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/message/{id}", method= RequestMethod.GET)
	public ModelAndView getMessageMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Get Message ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/manager/message/message"); //jsp
		
		Message mess = messageService.findById(id); 
		mav.addObject("mess", mess);
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/message/save", method= RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("message1") @Valid Message message1, 
			BindingResult result, SessionStatus status){
		
		
		logger.debug("------------------------------ Admin - Save Message ------------------------------");
		
		if (result.hasErrors()) {
			//if validator failed
			ModelAndView mav = new ModelAndView();
			mav.addObject("accounts", accountService.getNonDeleted());
			mav.addObject("currDateTime", System.currentTimeMillis()/1000);
			
			mav.setViewName("/admin/message/newMessage"); //jsp
			
			return mav;
			
		} else {
			boolean saveResult = false; 
			
			if (messageService.save(message1)) saveResult = true;
			
			ModelAndView mav = messages(); 
			mav.addObject("message", saveResult);
			return mav;
		}
	}
	
	
	@RequestMapping(value="/admin/message/send", method= RequestMethod.POST)
	public ModelAndView send(@RequestParam("dateSent")String dateSent, 		@RequestParam("subject")String subject, 
							 @RequestParam("message")String message,		@RequestParam("layerId")Integer layerId
							 ){
		
		logger.debug("------------------------------ Admin - Send Message & Mail ------------------------");
		
		ModelAndView mav = new ModelAndView();
		Layer layer = communityService.findByIdWithRelations(layerId);
		String[] mailTo = new String[layer.getAccountLayerAccesses().size()];
		String[] idTo = new String[layer.getAccountLayerAccesses().size()];
		String fromname = "\"Rural Connect Team\"";
		String mailFrom = "<no_reply@ruralconnect.com>";
		String mSubject = "Message from community: "+layer.getName().toString()+" about "+subject;
		
		String fromId = "" + getCurrentAccount().getId();
		
		
		int x = 0;
		for(AccountLayerAccess ids:layer.getAccountLayerAccesses()){		// Send email
			mailTo[x]=(ids.getAccount().getEmail());
			idTo[x]=""+(ids.getAccount().getId());
			mailService.mailToOne(fromname, mailFrom,mailTo[x],mSubject,message);
			x++;
		}
		
		for(AccountLayerAccess ids:layer.getAccountLayerAccesses()){		// Send Message
			Message newMessage = new Message();
				newMessage.setDateSent(System.currentTimeMillis()/1000);
				newMessage.setMessage(message);
				newMessage.setSubject(mSubject);
				newMessage.setIsRead(false);
				newMessage.setDeleted(false);
				newMessage.setLayerId(layer.getId());
				newMessage.setAccountByFromId(layer.getAccount());
				newMessage.setAccountByToId(ids.getAccount());
			messageService.save(newMessage);
			pushService.sendPush(fromId, subject, message, idTo, layerId.toString());
		}
		
		mav = messages(); 
		return mav;
	
	}
	
	
	@RequestMapping(value="/manager/message/save", method= RequestMethod.POST)
	public ModelAndView saveMgr(@ModelAttribute("message1") @Valid Message message1, 
			BindingResult result, SessionStatus status){
		
		logger.debug("------------------------------ Manager - Save Message ------------------------------");
		
		if (result.hasErrors()) {
			//if validator failed
			ModelAndView mav = new ModelAndView();
			mav.addObject("accounts", accountService.getNonDeleted());
			mav.addObject("currDateTime", System.currentTimeMillis()/1000);
			
			mav.setViewName("/manager/message/newMessage"); //jsp
			
			return mav;
			
		} else {
			boolean saveResult = false; 
			
			if (messageService.save(message1)) saveResult = true;
			
			ModelAndView mav = messagesMgr(); 
			mav.addObject("message", saveResult);
			
			return mav;
		}
	}
	
	
	@RequestMapping(value="/admin/message/delete/{id}")
	public String delete(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Delete Message ------------------------------");

		Message mess2 = messageService.findById(id); 
		mess2.setDeleted(true);
		messageService.save(mess2);
		
		ModelAndView mav =  messages();
		mav.addObject("message", messageService.save(mess2));
		
		return "redirect:/admin/messages/";
	}
	
	
	@RequestMapping(value="/manager/message/delete/{id}")
	public String deleteMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Delete Message ------------------------------");

		Message mess2 = messageService.findById(id); 
		mess2.setDeleted(true);
		messageService.save(mess2);
		
		ModelAndView mav =  messagesMgr();
		mav.addObject("message", messageService.save(mess2));
		
		return "redirect:/manager/messages/";
	}
	
	
	@RequestMapping(value="/admin/message/new")
	public ModelAndView newMessage() {
		
		logger.debug("------------------------------ Admin - New Message ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.addObject("accounts", accountService.getNonDeleted());
		mav.addObject("currDateTime", System.currentTimeMillis()/1000);
		mav.addObject("message1", new Message());
		mav.setViewName("/admin/message/newMessage"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/message/compose")
	public ModelAndView composeMail() {
		
		logger.debug("------------------------------ Admin - Compose Message and Mail -------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.addObject("currDateTime", System.currentTimeMillis()/1000);
		mav.addObject("newMail", new Message());
		mav.addObject("mailLayers", communityService.layersIfManager(getCurrentAccount().getId()));
		mav.addObject("from", accountService.get(1));
		mav.setViewName("/admin/message/composeMail"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/message/new")
	public ModelAndView newMessageMgr() {
		
		logger.debug("------------------------------ Manager - New Message ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.addObject("accounts", accountService.getNonDeleted());
		mav.addObject("currDateTime", System.currentTimeMillis()/1000);
		mav.addObject("message1", new Message());
		mav.setViewName("/manager/message/newMessage"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/message/editMessage/{id}")
	public ModelAndView editMessage(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Edit Message ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Message message1 = messageService.findById(id);
		mav.addObject("message1", message1);
		mav.addObject("accounts", accountService.getNonDeleted());
		mav.setViewName("/admin/message/newMessage"); //jsp	
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/message/editMessage/{id}")
	public ModelAndView editMessageMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Edit Message ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Message message1 = messageService.findById(id);
		mav.addObject("message1", message1);
		mav.addObject("accounts", accountService.getNonDeleted());
		mav.setViewName("/manager/message/newMessage"); //jsp	
		
		return mav; 
	}
	
	
	@ModelAttribute ("messageLayers")
	public List<Layer> setLayers()
	{
		return communityService.getNonDeleted();
	}
	
	@ModelAttribute ("messageActivities")
	public List<Activity> setActivities()
	{
		return activityService.getNonDeleted();
	}
}

