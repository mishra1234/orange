package au.com.dmsq.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.UserDTO;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.RoleService;

@Controller
public class ErrorsController extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(ErrorsController.class);
	
	@RequestMapping(value = "/admin/errors/Error-500", method = RequestMethod.GET)
	public ModelAndView error500() {

		ModelAndView model = new ModelAndView();

		model.setViewName("admin/errors/500");
		return model;
	}
	
	@RequestMapping(value = "/admin/errors/Error-501", method = RequestMethod.GET)
	public ModelAndView error501() {

		ModelAndView model = new ModelAndView();

		model.setViewName("admin/errors/501");
		return model;
	}
	
	@RequestMapping(value = "/admin/errors/Error-502", method = RequestMethod.GET)
	public ModelAndView error502() {

		ModelAndView model = new ModelAndView();

		model.setViewName("admin/errors/502");
		return model;
	}
	
	@RequestMapping(value = "/admin/errors/Error-503", method = RequestMethod.GET)
	public ModelAndView error503() {

		ModelAndView model = new ModelAndView();

		model.setViewName("admin/errors/503");
		return model;
	}
	
	@RequestMapping(value = "/admin/errors/Error-504", method = RequestMethod.GET)
	public ModelAndView error504() {

		ModelAndView model = new ModelAndView();

		model.setViewName("admin/errors/504");
		return model;
	}
	
}
