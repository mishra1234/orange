package au.com.dmsq.web;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.RecommendedActivities;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.EventCalendarItemService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.FileService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;


@Controller
public class RecommendedActivitiesController extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired EventService eventService;
	@Autowired EventCalendarItemService eventCalendarItemService;
	@Autowired ActivityService activityService;
	@Autowired LayerService layerService;
	@Autowired AccountService accountService;
	@Autowired BannerService bannerService;
	@Autowired FileService fileService;
	@Autowired MailService mailService;
	@Autowired MailContentService mcs;
	
	@Transactional
	@RequestMapping(value="/admin/recommended/{eventId}/{commId}", method= RequestMethod.GET)
	public ModelAndView events(@PathVariable (value="eventId")Integer eventId, @PathVariable (value="commId")Integer commId
			) {
		
		logger.debug("------------------------------Admin - List of Events ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Activity activity = activityService.get(eventId);
		
		Set<Activity> events = layerService.findLayerWithActivitiesAndType(commId, "event").getActivities();
		System.out.println("Events: " + events.size());
				
		List<RecommendedActivities> recommendedActivities = activityService.findRecommendedActivitiesfromActs(events);
		
		List<Integer> numbers = new ArrayList<Integer>();
		System.out.println("DB Pass" + mcs.readDBPass());
		
		mav.addObject("activity", activity);
		mav.addObject("numbers", numbers);
		mav.addObject("commId", commId);
		mav.setViewName("/admin/recommended/recommendeds"); // jsp
		return mav; 
	}
	
	
		
}


