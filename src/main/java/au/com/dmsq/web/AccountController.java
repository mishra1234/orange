package au.com.dmsq.web;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.IconDTO;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Mail;
import au.com.dmsq.model.Message;
import au.com.dmsq.model.Role;
import au.com.dmsq.service.AccountLayerAccessService;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.RoleService;
import au.com.dmsq.service.MailService;

@Controller
public class AccountController extends AbstractController{
	
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired AccountService accountService;
	@Autowired RoleService roleService;
	@Autowired MailService mailService;
	@Autowired LayerService layerService;
	@Autowired AccountLayerAccessService alasService;
	@Autowired ServletContext servletContext;
	@Autowired CommunityController commController;
	
	@Transactional
	@RequestMapping(value="/admin/accounts/{layerId}/{curr}")
	public ModelAndView pagedAccounts(@PathVariable(value="layerId") Integer layerId, @PathVariable(value="curr") Integer current) {
		
		logger.debug("------------------------------Admin - List Paged Accounts------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		Integer curr = new Integer (current);
		Integer max = 00;
		Integer next = 2;
		Integer prev = 1;
		Integer initial= 0;
		Integer hmany = 0;
		
		
		Layer layer = layerService.findById(layerId);
		if(layer.getType().equalsIgnoreCase("public")){
			Integer total = accountService.countAccts(layerId);
			if( (total/20)>0 && (total%20)==0){
				hmany = (total/20);
			}if( (total/20)>0 && (total%20)>0){
				hmany = (total/20)+1;
			}else{
				hmany = 1;
			}
			
			logger.debug("----- hmany = "+hmany);
			
			List<Integer> numbers = new ArrayList<Integer>();
			for (Integer i=1; i< hmany+1; i++){
				numbers.add(i);
			}
			
			if(curr==1 ){
				if(total%20 != 0 && total >20){
					max = 20; }
				if(total%20 != 0 && total <20){ 
					max = total%20; }
				if(total%20 == 0 && total ==20){ 
					max = 20; }
				initial=0;	
				prev=1;		
				next=2;
				List<Account> pagedAccts= accountService.pagedAccountsByLayer(initial, max, acct.getId(), layerId);
				
				ArrayList<Account> subSet = new ArrayList<Account>();
				subSet.addAll(pagedAccts);
				
				List<Account> subSetF = new ArrayList<Account>();
				subSetF.addAll(subSet.subList(initial, max));
				
				mav.addObject("myList", subSetF);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
			if(curr>1 && curr < hmany){
				max= (curr*20);  		
				initial= (curr*20)-20; 	
				prev=curr-1;			
				next=curr+1;
				List<Account> pagedAccts= accountService.pagedAccountsByLayer(initial, max, acct.getId(), layerId);

				ArrayList<Account> subSet = new ArrayList<Account>();
				subSet.addAll(pagedAccts);
				
				List<Account> subSetF = new ArrayList<Account>();
				subSetF.addAll(subSet.subList(initial, max));
				
				mav.addObject("myList", subSetF);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
			if(curr.intValue()==hmany.intValue() && hmany > 1){
				initial= (curr*20)-20;	
				max= initial + (accountService.pagedAccountsByLayer(initial, accountService.countAccts(layerId)%20,acct.getId(),layerId).size())%20;
				prev=curr-1;			
				next=curr+1;			
				List<Account> pagedAccts= accountService.pagedAccountsByLayer(initial, accountService.countAccts(layerId)%max,acct.getId(),layerId);

				ArrayList<Account> subSet = new ArrayList<Account>();
				subSet.addAll(pagedAccts);
				
				List<Account> subSetF = new ArrayList<Account>();
				subSetF.addAll(subSet.subList(initial, max));
				
				mav.addObject("myList", subSetF);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
			mav.addObject("acct", getCurrentAccount());
			mav.addObject("hmany", hmany);
			mav.addObject("curr", curr);
			mav.addObject("numbers", numbers);
			mav.addObject("commId", layerId);
		}
		if(layer.getType().equalsIgnoreCase("gated")){
			Integer total = accountService.countAcctsGated(layerId);
			if( (total/20)>0){
				hmany = (total/20);
			}else{
				hmany = 1;
			}
			logger.debug("----- hmany = "+hmany);
			
			List<Integer> numbers = new ArrayList<Integer>();
			for (Integer i=1; i< hmany+1; i++){
				numbers.add(i);
			}
			
			if(curr==1){
				if(total%20 != 0 && total >20){
					max = 20; }
				if(total%20 != 0 && total <20){ 
					max = total%20; }
				if(total%20 == 0 && total ==20){ 
					max = 20; }
				initial=0;	
				prev=1;		
				next=2;
				List<Account> pagedAccts= accountService.pagedAccountsByLayerGated(initial, max, acct.getId(), layerId);
				
				ArrayList<Account> subSet = new ArrayList<Account>();
				subSet.addAll(pagedAccts);
				
				List<Account> subSetF = new ArrayList<Account>();
				subSetF.addAll(subSet.subList(initial, max));
				
				mav.addObject("myList", subSetF);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
			if(curr>1 && curr < hmany){
				max= (curr*20);  		
				initial= (curr*20)-20; 	
				prev=curr-1;			
				next=curr+1;
				List<Account> pagedAccts= accountService.pagedAccountsByLayerGated(initial, max, acct.getId(), layerId);

				ArrayList<Account> subSet = new ArrayList<Account>();
				subSet.addAll(pagedAccts);
				
				List<Account> subSetF = new ArrayList<Account>();
				subSetF.addAll(subSet.subList(initial, max));
				
				mav.addObject("myList", subSetF);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
			if(curr.intValue()==hmany.intValue() && hmany > 1){
				initial= (curr*20)-20;	
				max= initial + (accountService.pagedAccountsByLayerGated(initial, accountService.countAcctsGated(layerId)%20,acct.getId(),layerId).size())%20;
				prev=curr-1;			
				next=curr+1;			
				List<Account> pagedAccts= accountService.pagedAccountsByLayerGated(initial, accountService.countAcctsGated(layerId)%max,acct.getId(),layerId);

				ArrayList<Account> subSet = new ArrayList<Account>();
				subSet.addAll(pagedAccts);
				
				List<Account> subSetF = new ArrayList<Account>();
				subSetF.addAll(subSet.subList(initial, max));
				
				mav.addObject("myList", subSetF);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
			
			mav.addObject("hmany", hmany);
			mav.addObject("curr", curr);
			mav.addObject("numbers", numbers);
			mav.addObject("commId", layerId);
			mav.addObject("acct", getCurrentAccount());
			
		}
		
		mav.setViewName("/admin/account/accounts"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/accounts", method= RequestMethod.GET)
	public ModelAndView accountsMgr() {
		
		logger.debug("------------------------------ Manager - List Accounts ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/manager/account/accounts"); // jsp
		
		List<Account> listacc = accountService.getNonDeleted(); 
		mav.addObject("myList", listacc);
		mav.addObject("acct", getCurrentAccount());
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/account/{id}", method= RequestMethod.GET)
	public ModelAndView getAccount(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get Account ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/admin/account/account"); //jsp
		
		Account acc = accountService.findById(id); 
		mav.addObject("acc", acc);
		mav.addObject("acct", getCurrentAccount());
		
		return mav; 
	}
	
	
	@RequestMapping(value="/member/account/{id}", method= RequestMethod.GET)
	public ModelAndView getAccountMember(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Member - Get Account ------------------------------");
		
		ModelAndView mav = setBasicData(); 
		/*
		Account acc = accountService.getbyIdWithLayers(id); 
		
		List<String> gatedChecks=new ArrayList<>();
		List<String> ungatedChecks=new ArrayList<>();
		List<String> gatedChecksrequested=new ArrayList<>();
		
		for(AccountLayerAccess ala: acc.getAccountLayerAccesses())
		{
			if(ala.getStatus().equals("granted"))
			{
				gatedChecks.add(ala.getLayer().getId().toString());
			}
			else if(ala.getStatus().equals("requested"))
			{
				gatedChecksrequested.add(ala.getLayer().getId().toString());
			}
			
		}
		
		for(Layer alayer: acc.getLayers_1())
		{
			ungatedChecks.add(alayer.getId().toString());
		}
		mav.addObject("acc", acc);
		mav.addObject("ungatedPork", ungatedChecks.contains("70515"));
		logger.debug("-----check ungatedPork --"+ungatedChecks.contains("70515"));
		mav.addObject("ungatedGrain", ungatedChecks.contains("70510"));
		mav.addObject("ungatedLivestock", ungatedChecks.contains("70511"));
		mav.addObject("ungatedHorticulture", ungatedChecks.contains("70512"));
		mav.addObject("ungatedDairy", ungatedChecks.contains("70514"));
		mav.addObject("ungatedWine", ungatedChecks.contains("70513"));
		
		
		mav.addObject("gatedPork", gatedChecks.contains("70521")?"granted":(gatedChecksrequested.contains("70521")?"requested":"denied"));
		mav.addObject("gatedGrain", gatedChecks.contains("70516")?"granted":(gatedChecksrequested.contains("70516")?"requested":"denied"));
		mav.addObject("gatedLivestock", gatedChecks.contains("70517")?"granted":(gatedChecksrequested.contains("70517")?"requested":"denied"));
		mav.addObject("gatedHorticulture", gatedChecks.contains("70518")?"granted":(gatedChecksrequested.contains("70518")?"requested":"denied"));
		mav.addObject("gatedDairy", gatedChecks.contains("70520")?"granted":(gatedChecksrequested.contains("70520")?"requested":"denied"));
*/
		//adding councils
		List<Layer> councils=layerService.getregionallayers();
		mav.addObject("councils", councils);
		
		List<Layer> statusComms = layerService.getStatusLayers();
		mav.addObject("statusComm", statusComms);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/member/account/account"); //jsp
		return mav; 
	}
	
	@RequestMapping(value = "/member/community/request", method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public @ResponseBody String messageRequestHandler (@RequestBody Object command, HttpSession session) {
		logger.debug("----------Connect/Disconnect From Community -------------");
		//logger.debug("Current Account ID is"+ getCurrentAccount());
		
		Account currentUser=getCurrentAccount();
		Account currentUserwithConnections= accountService.getbyIdWithLayers(currentUser.getId());
		
		if(currentUser!=null)
		{
			logger.debug("----------Current User is Found -------------");
			JSONObject obj = new JSONObject(command.toString().replace("=", ":"));
			  if(obj.has("CommunityID") && !obj.isNull("CommunityID"))
			  {
				  int commID=obj.getInt("CommunityID");
				  String action=obj.getString("message");
				  Layer thisLayer=layerService.findById(commID);
				  logger.debug("----------Parameters are Read -------------");
				  if(thisLayer!=null )
				  {
					  if(thisLayer.getType().equals("gated"))
					  {
						  logger.debug("----------Layer is Gated-------------");
						  if(action.equals("connect"))
						  {
							  logger.debug("----------Layer Connection Activities-------------");
							  if(addAccountLayerAccess(thisLayer,currentUser))
							  {
								  return "Success--Your request has been submitted successfully";
							  }
						  }
						  else
						  {
							  logger.debug("----------Layer Disconnection Activities-------------");
							 for(AccountLayerAccess alaTemp:currentUserwithConnections.getAccountLayerAccesses())
							 {
								 
								 logger.debug("--Iterated Layer Id ="+ alaTemp.getLayer().getId()+" --Current Layer id --"+ thisLayer.getId() );
								 if(alaTemp.getLayer().getId().equals(thisLayer.getId()))
								 {
									 logger.debug("----------Setting Denied Status-------------");
									 alaTemp.setStatus("denied");
									 
									 logger.debug("----------Start Saving Status-------------");
									 alasService.save(alaTemp);
									 if(true)
									 {
										 logger.debug("----------Saved Successfully-------------");
										 return "Success--Your Access has been revoked successfully";
									 }
								 }
							 }
						  }
					  }
					  else
					  {
						  logger.debug("----------Layer is non- Gated-------------");
						  if(action.equals("connect"))
						  {
							  logger.debug("----------Layer Connection Activities-------------");
							 Set<Layer> currentConnectedLayers= currentUserwithConnections.getLayers_1();
							 logger.debug("----------account total connections before adding---"+ currentUserwithConnections.getLayers_1().size());
							 int initialcount=currentUserwithConnections.getLayers_1().size();
							 currentConnectedLayers.add(thisLayer);
							 currentUserwithConnections.setLayers_1(currentConnectedLayers);
							 logger.debug("----------account total connections before save---"+ currentUserwithConnections.getLayers_1().size());
							 //logger.debug("----------result of account save---"+accountService.save(currentUserwithConnections));
							 currentUser.setLayers_1(currentConnectedLayers);
							 accountService.save(currentUser);
//							 Account checkUserAccess= accountService.getbyIdWithLayers(currentUser.getId());
//							 logger.debug("----------account total connections after save---"+ checkUserAccess.getLayers_1().size());
//							 int finalcount=checkUserAccess.getLayers_1().size();
							 //if(finalcount==(initialcount+1))
							 if (true)
							 {
								logger.debug("----------account modifications saved successfully-------------");
								nonGatedLayerSubscriptionConfirmationMail(thisLayer,currentUser,action);
								return "Success--You have been connected successfully";
							 }
							 else
							 {
								 logger.debug("----------account service failed to save your changes-------------");
								 return "Error--Your changes are not saved. Contact Administrator.";
							 }
						  }
						  else
						  {
							  logger.debug("----------Layer Disconnection Activities-------------");
							  logger.debug("No of layers before removal="+currentUserwithConnections.getLayers_1().size());
								 Set<Layer> currentConnectedLayers= currentUserwithConnections.getLayers_1();
								 Layer rmLayer=null;
								 for(Layer l:currentConnectedLayers)
								 {
									 if(l.getId().equals(thisLayer.getId()))
									 {
										 rmLayer=l;
										// System.out.println("Remove the accurate Layer"+currentConnectedLayers.remove(l));
									 }
								 }
								 logger.debug("Remove the accurate Layer="+currentConnectedLayers.remove(rmLayer));
								 currentUserwithConnections.setLayers_1(currentConnectedLayers);
								 logger.debug("No of layers after set="+currentUserwithConnections.getLayers_1().size());
								 logger.debug("----------result of account save---"+accountService.save(currentUserwithConnections));
//								 Account checkUserAccess= accountService.getbyIdWithLayers(currentUser.getId());
//								 logger.debug("----------No of layers after save---"+ checkUserAccess.getLayers_1().size());
								 if(true)
								 {
									 logger.debug("----------account modifications saved successfully-------------");
									 nonGatedLayerSubscriptionConfirmationMail(thisLayer,currentUser,action);
									return "Success--You have been disconnected successfully";
								 }
								 else
								 {
									 logger.debug("----------account service failed to save your changes-------------");
									 return "Error--Your changes are not saved. Contact Administrator.";
								 }
						  }
					  }
				  }
				  else
				  {
					  return "Error--Invalid Arguments";
				  }
			  }
			  else
			  {
				  return "Error--Missing Arguments"; 
			  }
		}
		
		return "Error--Session Not Found";
	}
	
	
	@RequestMapping(value="/manager/account/{id}", method= RequestMethod.GET)
	public ModelAndView getAccountMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager Get Account ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/manager/account/account"); //jsp
		
		Account acc = accountService.findById(id); 
		mav.addObject("acc", acc);
		mav.addObject("acct", getCurrentAccount());
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/account/save", method= RequestMethod.POST)
	public String save(@ModelAttribute("account") @Valid Account acc, BindingResult result) throws Exception{
		
		logger.debug("------------------------------ Admin - Save Account ------------------------------");
		logger.debug("Saving User: " + acc.getEmail());
		
		if (result.hasErrors()) {
			//if validator failed
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/admin/account/newAccount"); //jsp
			logger.debug("errors:" + result.getErrorCount());
			return "redirect:/member/account/"+acc.getId();
			
		} else {
			boolean saveResult = false;
			
			Account account = accountService.get(acc.getId());
			Account accountLayers =accountService.getLayerManagersById(acc.getId());
			Account accountAuth = accountService.getAuthsById(acc.getId());
			Account accountBookings =accountService.getBookingsById(acc.getId());
			Account accountActivities =accountService.getActivitiesById(acc.getId());
			Account connectedLayers = accountService.getConnectedLayersById(acc.getId());
			System.out.println("number of connected layers:" +connectedLayers.getEmail());
			account.setNewUserDate(new java.util.Date());
			account.setFirstName(acc.getFirstName());
			account.setLastName(acc.getLastName());
			account.setName(account.getFirstName()+" "+account.getLastName());
			account.setMobilephone(acc.getMobilephone());
			account.setLayers(accountLayers.getLayers());
	//		account.setLayers_1(connectedLayers.getLayers_1());
			account.setAuths(accountAuth.getAuths());
			account.setBookings(accountBookings.getBookings());
			account.setActivities(accountActivities.getActivities());
			
			Set<Layer> addLayers = new HashSet<Layer>();
//			for(Layer layer:account.getLayers_1()){
//				if(layer.getId()>70509 && layer.getId()<70516) addLayers.add(layer);
//			}
			
			//Ststus Councils
//			if(!ss.isEmpty())
//			{	
//				Layer temp=layerService.findById(Integer.parseInt(ss));
//				if(temp.getId()!=null)
//				{
//					addLayers.add(temp);
//				}
//			}			
			
			//Regional Councils
//			if(st!=null && st.length>0)
//			{	
//				for(String str:st)
//				{	
//					if(!str.equalsIgnoreCase("allcommunities")){
//						Layer temp=layerService.findById(Integer.parseInt(str));
//						if(temp.getId()!=null)
//						{
//							addLayers.add(temp);
//						}
//					}
//					if(str.equalsIgnoreCase("allcommunities")){
//								addLayers.addAll(layerService.getregionallayers());
//					}
//				}
//			}
			accountService.save(account);
			
			if(acc.getEmail().equalsIgnoreCase(account.getEmail())){
				
				
				if(accountService.save(account)){
					saveResult=true;
					logger.debug("saveResult Result =====>>>> " + saveResult);
				}
			}

			if(!acc.getEmail().equalsIgnoreCase(account.getEmail()) && accountService.emailExist(acc.getEmail())){
				account.setAlternateEmail(acc.getEmail());
				if(accountService.save(account)){
					  saveResult=true;
					  logger.debug("saveResult Result =====>>>> " + saveResult);
				}
				
				Mail mail = new Mail();
				mail.setFrom(FROM_EMAIL);
				mail.setSubject("New Email configuration at Rural Connect for "+account.getName()+".");
				String body = mcs.readNewEmailConfirmationEmail();
				String link = SERVER + servletContext.getContextPath() + "/confirmNewEmail/"+account.getId();
				body += " <a href='"+link+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Click here to confirm now</button></a><p><p>	 If you are not the owner of this account or dont want to proceed <p> just drop this or ignore it.<p> Thank you.<p><p>";
				body += mcs.readConfirmationEmailLower();
				mail.setMessage(mcs.insertUnsubscribe(account,body));
				mailService.welcomeMail(mail.getFrom(), account.getAlternateEmail(), mail.getSubject(), mail.getMessage());
			}
			ModelAndView mav = commController.communities();
			mav.addObject("message", saveResult);
			return "redirect:/admin/home";
		}
	}
	
	
	@RequestMapping(value="/member/account/save", method= RequestMethod.POST)
	public String saveMember(@ModelAttribute("acc") @Valid Account acc, BindingResult result, SessionStatus status,
			@RequestParam(value="regionalMultiSelect", required = false) String[] st,
			@RequestParam(value="statusSelect", required = false) String ss) throws Exception{
		
		logger.debug("------------------------------Member Save Account account/save------------------------------");
		logger.debug("User:" + acc.getEmail());
        
		
		ModelAndView mav = new ModelAndView(); 
		mav.addObject("account", acc);
		
		Account account = accountService.getbyIdWithRelations(acc.getId());
		
		if (result.hasErrors()) {
			mav.setViewName("/member/account/account"); //jsp
			logger.debug("errors:" + result.getErrorCount());
			return "redirect:/member/account/"+account.getId();
			
		} else {
			
			logger.debug("No errors");
			boolean saveResult = false; 
			
			account.setNewUserDate(new java.util.Date());
			account.setFirstName(acc.getFirstName());
			account.setLastName(acc.getLastName());
			account.setName(account.getFirstName()+" "+account.getLastName());
			account.setMobilephone(acc.getMobilephone());
			
			Set<Layer> addLayers = new HashSet<Layer>();
			for(Layer layer:account.getLayers_1()){
				if(layer.getId()>70509 && layer.getId()<70516) addLayers.add(layer);
			}
			
			//Ststus Councils
			if(!ss.isEmpty())
			{
				Layer temp=layerService.findById(Integer.parseInt(ss));
				if(temp.getId()!=null)
				{
					addLayers.add(temp);
				}
			}			
			
			//Regional Councils
			if(st!=null && st.length>0)
			{	
				for(String str:st)
				{	
					if(!str.equalsIgnoreCase("allcommunities")){
						Layer temp=layerService.findById(Integer.parseInt(str));
						if(temp.getId()!=null)
						{
							addLayers.add(temp);
						}
					}
					if(str.equalsIgnoreCase("allcommunities")){
							addLayers.addAll(layerService.getregionallayers());
					}
				}
			}
			account.setLayers_1(addLayers);
			
			if(acc.getEmail().equalsIgnoreCase(account.getEmail())){
				if(accountService.save(account)){
					  saveResult=true;
					  logger.debug("saveResult Result =====>>>> " + saveResult);
				}
			}

			if(!acc.getEmail().equalsIgnoreCase(account.getEmail()) && accountService.emailExist(acc.getEmail())){
				account.setAlternateEmail(acc.getEmail());
				if(accountService.save(account)){
					  saveResult=true;
					  logger.debug("saveResult Result =====>>>> " + saveResult);
				}
				
				Mail mail = new Mail();
				mail.setFrom(FROM_EMAIL);
				mail.setSubject("New Email configuration at CC Me for "+account.getName()+".");
				String body = mcs.readNewEmailConfirmationEmail();
				String link = SERVER + servletContext.getContextPath() + "/confirmNewEmail/"+account.getId();
				body += " <a href='"+link+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Click here to confirm now</button></a><p><p>	 If you are not the owner of this account or dont want to proceed <p> just drop this or ignore it.<p> Thank you.<p><p>";
				body += mcs.readConfirmationEmailLower();
				mail.setMessage(mcs.insertUnsubscribe(account,body));
				mailService.welcomeMail(mail.getFrom(), account.getAlternateEmail(), mail.getSubject(), mail.getMessage());
			}
			
			mav.addObject("message", saveResult);
			return "redirect:/member/home";
		}
	}
	
	
	@RequestMapping(value="/manager/account/save", method= RequestMethod.POST)
	public ModelAndView saveMgr(@ModelAttribute("account") @Valid Account account, @RequestParam("icon")int icon,
			BindingResult result, SessionStatus status) throws Exception{
		
		logger.debug("------------------------------ Manager - Save Account ------------------------------");

		if (result.hasErrors()) {
			//if validator failed
			ModelAndView mav = new ModelAndView();
			mav.addObject("listOfRoles", roleService.findAll());
			//mav.addObject("listOfIcons", layerService.listIcons()); 
			
			mav.setViewName("/manager/account/newAccount"); //jsp
			return mav;
			
		} else {
			boolean saveResult = false;
			
//			List<IconDTO> icons = layerService.listIcons();
//			Integer iconx = icon;
//			IconDTO x = icons.get(iconx);
//			account.setImageUrl(x.getIconUrl().toString());
			
			if (accountService.save(account)) saveResult = true;
			// Todo call to API
			
			ModelAndView mav = accountsMgr();
			mav.addObject("message", saveResult);
			return mav;
		}
	}
	
	
	@RequestMapping("/admin/account/delete/{id}")
	public String delete(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Delete Account ------------------------------");

		Account acc = accountService.findById(id); 
		acc.setEnabled(false);
		accountService.save(acc);
		
		ModelAndView mav = commController.communities();
		mav.addObject("message", accountService.save(acc));
		
		return "redirect:/admin/accounts/"; 
	}
	
	
	@RequestMapping("/manager/account/delete/{id}")
	public String deleteMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Delete Account ------------------------------");

		Account acc = accountService.findById(id); 
		acc.setEnabled(false);
		accountService.save(acc);
		
		ModelAndView mav =  accountsMgr();
		mav.addObject("message", accountService.save(acc));
		
		return "redirect:/manager/accounts/"; 
	}
	
	
	@RequestMapping(value="/admin/account/new")
	public ModelAndView newAccount() throws Exception {
		
		logger.debug("------------------------------ Admin - New Account ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 

		mav.addObject("listOfRoles", roleService.findAll());
		Account acc = new Account();
		String password = "P4ssw0rd";
	    String encoded = SHA1(password);
		acc.setPassword(encoded);
		mav.addObject("account", acc);
		mav.addObject("iUrl", 0); 
		//mav.addObject("listOfIcons", layerService.listIcons()); 
		List<Layer> councils=layerService.getregionallayers();
		mav.addObject("councils", councils);
		
		List<Layer> statusComms = layerService.getStatusLayers();
		mav.addObject("statusComm", statusComms);
		
		mav.setViewName("/admin/account/newAccount"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/account/new")
	public ModelAndView newAccountMgr() throws Exception {
		
		logger.debug("------------------------------ Manager - New Account ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 

		mav.addObject("listOfRoles", roleService.findAll());
		Account acc = new Account();
		String password = "P4ssw0rd";
	    String encoded = SHA1(password);
		acc.setPassword(encoded);
		mav.addObject("account", acc);
		//mav.addObject("listOfIcons", layerService.listIcons()); 
		
		mav.setViewName("/manager/account/newAccount"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/account/editAccount/{id}", method= RequestMethod.GET)
	public ModelAndView editAccount(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Edit Account ------------------------------");
		
		ModelAndView mav = setBasicData(); 
		
		Account account = accountService.getLayersbyId(id);
		mav.addObject("account", account);	
		
		List<Layer> councils=layerService.getregionallayers();
		mav.addObject("councils", councils);
		
		List<Layer> statusComms = layerService.getStatusLayers();
		mav.addObject("statusComm", statusComms);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/account/newAccount"); //jsp
		
		
		return mav; 
	}
	
	
	@RequestMapping(value="/member/account/editAccount/{id}", method= RequestMethod.GET)
	public ModelAndView editAccountMember(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Member - Edit Account ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Account account = accountService.findById(id);
		mav.addObject("account", account);	
		mav.addObject("listOfRoles", roleService.findAll());
		//mav.addObject("listOfIcons", layerService.listIcons()); 
		
		mav.setViewName("/member/account/newAccount"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/account/editAccount/{id}", method= RequestMethod.GET)
	public ModelAndView editAccountMgr(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Manager - Edit Account ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Account account = accountService.findById(id);
		
//		List<IconDTO> icons = layerService.listIcons();
//		Integer icon = null;
//		for(int i=0; i<icons.size(); i++){
//			String xxx = icons.get(i).getIconUrl();
//			if(xxx.equalsIgnoreCase(account.getImageUrl().toString() ) ){
//				icon = i;
//				i = icons.size();
//			}
//		}
//		mav.addObject("iUrl", icon);  //Current Index of the iconUrl to send to JSP
		//System.out.println("Controller - reset password - icon: " + icon.toString());
		
		mav.addObject("account", account);	
		mav.addObject("listOfRoles", roleService.findAll());
		//mav.addObject("listOfIcons", layerService.listIcons()); 
		
		mav.setViewName("/manager/account/newAccount"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/account/resetPassword/{id}", method= RequestMethod.GET)
	public ModelAndView resetPassword(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Reset Account Password ------------------------------");
		
		
		Account acc = accountService.findById(id);
		
	    String x = (RandomStringUtils.randomAlphabetic(10));
	    String xx = (x+"{"+acc.getEmail()+"}");
	    String encoded = SHA1(xx);
		acc.setPassword(encoded);
		
		Mail mail = new Mail();
		mail.setFrom(FROM_EMAIL);
		mail.setSubject("CC Me account password reset confirmation.");
		mail.setMessage("Dear "+acc.getName()+
				" your account password to access CC Me has been set to: "+x+""+"\n"+
				"Please change it as soon as possible!"+"\n\n"+
				"Please use the following link: "+"http://localhost:8080/ccme/login"+"\n\n"+"\n\n"+"Yours: "+
				"\n\n"+"CC Me Support Team.");
		mailService.resetPasswordMail(mail.getFrom(), acc.getEmail(), mail.getSubject(), mail.getMessage());
		
		ModelAndView mav = new ModelAndView(); 
		mav.addObject("acc", acc);
		mav.addObject("acct", getCurrentAccount());
		
		
//		List<IconDTO> icons = layerService.listIcons();
//		Integer icon = null;
//
//		for(int i=0; i<icons.size(); i++){
//			String xxx = icons.get(i).getIconUrl();
//			if(xxx.equalsIgnoreCase(acc.getImageUrl().toString() ) ){
//				icon = i;
//				i = icons.size();
//			}
//		}
//		 
//		mav.addObject("iUrl", icon); 
		mav.setViewName("/admin/account/resetPassword"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/account/resetPassword/{id}", method= RequestMethod.GET)
	public ModelAndView resetPasswordMgr(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Manager - Reset Account Password ------------------------------");
		
		Account acc = accountService.findById(id);
		
	    String x = (RandomStringUtils.randomAlphabetic(10));
	    String xx = (x+"{"+acc.getEmail()+"}");
	    String encoded = SHA1(xx);
		acc.setPassword(encoded);
		
		Mail mail = new Mail();
		mail.setFrom(FROM_EMAIL);
		mail.setSubject("CC Me account password reset confirmation.");
		mail.setMessage("Dear "+acc.getName()+
				" your account password to access CC Me has been set to: "+x+""+
				"Please change it as soon as possible!"+"\n\n"+
				"Please use the following link: "+"SERVER/login"+"\n\n"+"\n\n"+"Yours: "+
				"\n\n"+"CC Me Support Team.");
		mailService.resetPasswordMail(mail.getFrom(), acc.getEmail(), mail.getSubject(), mail.getMessage());
		
		ModelAndView mav = new ModelAndView(); 
		mav.addObject("acc", acc);
		
//		List<IconDTO> icons = layerService.listIcons();
//		Integer icon = null;
		//System.out.println("Controller - reset password - iimageUrl" + acc.getImageUrl().toString());
		//System.out.println("Controller - reset password - urls are: " + icons.size());
//		for(int i=0; i<icons.size(); i++){
//			String xxx = icons.get(i).getIconUrl();
//			if(xxx.equalsIgnoreCase(acc.getImageUrl().toString() ) ){
//				icon = i;
//				i = icons.size();
//			}
//			else{
//				icon = 0;
//			}
//		}
		
		//mav.addObject("iUrl", icon); 
		//mav.addObject("message", mailService.save(mail));
		mav.setViewName("/manager/account/resetPassword"); //jsp
		
		return mav; 
	}
	
	
	private static String convertToHex(byte[] data) { 
	        StringBuffer buf = new StringBuffer();
	        for (int i = 0; i < data.length; i++) { 
	            int halfbyte = (data[i] >>> 4) & 0x0F;
	            int two_halfs = 0;
	            do { 
	                if ((0 <= halfbyte) && (halfbyte <= 9)) 
	                    buf.append((char) ('0' + halfbyte));
	                else 
	                    buf.append((char) ('a' + (halfbyte - 10)));
	                halfbyte = data[i] & 0x0F;
	            } while(two_halfs++ < 1);
	        } 
	        return buf.toString();
	    } 
	public static String SHA1(String text) 
		    throws NoSuchAlgorithmException, UnsupportedEncodingException  { 
		    MessageDigest md;
		    md = MessageDigest.getInstance("SHA-1");
		    byte[] sha1hash = new byte[40];
		    md.update(text.getBytes("iso-8859-1"), 0, text.length());
		    sha1hash = md.digest();
		    return convertToHex(sha1hash);
		    }
	
	@RequestMapping(value="/unsubscribe/{id}", method= RequestMethod.GET)
	public ModelAndView unsubscribe(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Unsubscribe from Weekly Digest ------------------------------");
		
		Account acc = accountService.findById(id);
		acc.setSubscribe(false);
		boolean save = accountService.save(acc);
		logger.debug("SAVED ACCOUNT?? : "+save);	    
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("acc", acc);
		mav.setViewName("/unsubscribe"); //jsp
		return mav; 
	}
}

