package au.com.dmsq.web;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.net.URLEncoder;

import org.springframework.web.multipart.MultipartFile;

import au.com.dmsq.model.FileMeta;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;

public class FileUploadHelper {

	private  String bucketName     	= "ccmeresources";
	private  String accessKey       = "AKIAIVAVSU3WIJP3ARKA";
	private  String secretKey       = "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";
	


	public String upload (MultipartFile mpf, FileMeta fileMeta)
	{
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonS3 conn = new AmazonS3Client(credentials);
		String result = ""; 

		try {

			File file = multipartToFile(mpf);
			conn.putObject(bucketName,file.getName(), file);
			conn.setObjectAcl(bucketName,file.getName(), CannedAccessControlList.PublicRead);
			result = "https://s3.amazonaws.com/ccmeresources/"+URLEncoder.encode(file.getName(), "UTF-8");

		} catch (AmazonServiceException ase) {
			
			result = "error";
		} catch (AmazonClientException e) {
			// TODO Auto-generated catch block
			result = "error 2";
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			result = "error 3";
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			result = "error 4";
			
			e.printStackTrace();
		}


		return result; 
	}
	

	private String generateFileName(String name) {
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
		return randomUUIDString+"-"+name;
	}
	
	public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException 
	{
			String uploadFileName = generateFileName(multipart.getOriginalFilename());
	        File convFile = new File(uploadFileName);
	        multipart.transferTo(convFile);
	        return convFile;
	}

}