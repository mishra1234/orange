package au.com.dmsq.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
 
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Message;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.MessageService;

@Controller
public class MemberController extends AbstractController {
	
		
		private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
		
		@Autowired AccountService accountService;
		@Autowired ActivityService activityService;
		@Autowired MessageService messageService;
		
		public Integer pork=70515;
		public Integer dairy=70514;
		public Integer horticulture=70512;
		public Integer livestock=70511;
		public Integer grain=70510;
		public Integer wine=70513;
		public Integer gatedPork=70521;
		public Integer gatedDairy=70520;
		public Integer gatedHorticulture=70518;
		public Integer gatedLivestock=70517;
		public Integer gatedGrain=70516;
		public Integer gatedWine=70519;
		
		@RequestMapping("/member/partners")
		public ModelAndView index(HttpServletRequest request) {
			
			logger.debug("Partners Video");
			
			ModelAndView mav = setBasicData();
			mav.setViewName("/member/partner");
			return mav;
			
		}
		
		@RequestMapping("/member/primeSuper")
		public ModelAndView indexPrimeSuper(HttpServletRequest request) {
			
			logger.debug("Prime Super Video");
			
			ModelAndView mav = setBasicData();
			mav.setViewName("/member/primeSuper");
			return mav;
			
		}
		
		@RequestMapping("/member/momentum")
		public ModelAndView indexMomentum(HttpServletRequest request) {
			
			logger.debug("Prime Super Video");
			
			ModelAndView mav = setBasicData();
			mav.setViewName("/member/momentum");
			return mav;
			
		}
		
		
		@RequestMapping("/member/newsVideo")
		public ModelAndView newsVideo(HttpServletRequest request) throws ParseException {
			
				logger.debug("Videos");
			
			ModelAndView mav = setBasicData();
			mav.setViewName("/member/video");
			return mav;
			
		}
		
		@RequestMapping("/member/pork")
		public ModelAndView pork(HttpServletRequest request) throws ParseException {
			
			logger.debug("MEMBER/PORK SA");
			
			boolean isGated = false;
			
			ModelAndView mav = setBasicData();
			
			List<Activity> activities = new ArrayList<Activity>();
			List<Activity> latestInfos = new ArrayList<Activity>();
			
			Account account = getCurrentAccount();
			//Account accountWR = accountService.getbyIdWithLayers(account.getId());
			Account acctWithALAS = accountService.getALAsbyId(account.getId());
			Account acctWithLayers = accountService.getLayersbyId(account.getId());
			
			Set<Layer> layers = acctWithLayers.getLayers_1();	//Obtain the connected layers of the logged user
			List<Integer> comms = new ArrayList<Integer>();
			for(Layer e:layers){
				if(e.getId().equals(pork)){
					comms.add(e.getId());
				}
			}
			activities.addAll(activityService.getEventsByCommIds(comms)); //Get the EVENTS of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(comms)); //Get the INFOS of the layers
			
			Set<AccountLayerAccess> gatedLayers = acctWithALAS.getAccountLayerAccesses();   //Obtain the gated layers of logged user
			List<Integer> gatedComms = new ArrayList<Integer>();
			for(AccountLayerAccess e:gatedLayers){
				if(e.getLayer().getId().equals(gatedPork)&&e.getStatus().equalsIgnoreCase("granted")){
					gatedComms.add(e.getLayer().getId());
					isGated = true;
				}
			}
			activities.addAll(activityService.getEventsByCommIds(gatedComms)); //Get the EVENTS with granted access of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(gatedComms)); //Get the INFOS with granted access of the layers
			
			List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities); //Gets all the activities
			Collections.sort(latestActivities, Activity.ActivityComparatorD);
			
			// Changing the date format for events to be displayed using "dd-mm-yyyy"
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	        for(int i=0; i<latestActivities.size();i++){
	        	Activity act = new Activity();
				Date date = sdf.parse(latestActivities.get(i).getStartDate());
				act.setStartDate(sdfDate.format(date));
				latestActivities.get(i).setStartDate(act.getStartDate());
	        }
			
			logger.debug("Access to Gated:"+ isGated);
			mav.addObject("isGated", isGated);
			mav.addObject("infos", latestInfos);
			mav.addObject("activities", latestActivities);
			mav.setViewName("/member/pork");
			return mav;
			
		}
		
		@RequestMapping("/member/grain")
		public ModelAndView grain(HttpServletRequest request) throws ParseException {
			
			logger.debug("GRAIN SA");
			
			ModelAndView mav = setBasicData();
			
			boolean isGated = false;
			
			List<Activity> activities = new ArrayList<Activity>();
			List<Activity> latestInfos = new ArrayList<Activity>();
			
			Account account = getCurrentAccount();
			//Account accountWR = accountService.getbyIdWithLayers(account.getId());
			Account acctWithALAS = accountService.getALAsbyId(account.getId());
			Account acctWithLayers = accountService.getLayersbyId(account.getId());
			
			Set<Layer> layers = acctWithLayers.getLayers_1();	//Obtain the connected layers of the logged user
			List<Integer> comms = new ArrayList<Integer>();
			for(Layer e:layers){
				if(e.getId().equals(grain)){
					comms.add(e.getId());
				}
			}
			activities.addAll(activityService.getEventsByCommIds(comms)); //Get the EVENTS of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(comms)); //Get the INFOS of the layers
			
			Set<AccountLayerAccess> gatedLayers = acctWithALAS.getAccountLayerAccesses();   //Obtain the gated layers of logged user
			List<Integer> gatedComms = new ArrayList<Integer>();
			for(AccountLayerAccess e:gatedLayers){
				if(e.getLayer().getId().equals(gatedGrain)&& e.getStatus().equalsIgnoreCase("granted")){
					gatedComms.add(e.getLayer().getId());
					isGated = true;
				}
			}
			activities.addAll(activityService.getEventsByCommIds(gatedComms)); //Get the EVENTS with granted access of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(gatedComms)); //Get the INFOS with granted access of the layers
			
			List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities); //Gets all the activities
			Collections.sort(latestActivities, Activity.ActivityComparatorD);
			
			// Changing the date format for events to be displayed using "dd-mm-yyyy"
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	        for(int i=0; i<latestActivities.size();i++){
	        	Activity act = new Activity();
				Date date = sdf.parse(latestActivities.get(i).getStartDate());
				act.setStartDate(sdfDate.format(date));
				latestActivities.get(i).setStartDate(act.getStartDate());
	        }
			
			logger.debug("Access to Gated:"+ isGated);
			mav.addObject("isGated", isGated);
			mav.addObject("infos", latestInfos);
			mav.addObject("activities", latestActivities);
			mav.setViewName("/member/grain");
			return mav;
			
		}
		
		@RequestMapping("/member/livestock")
		public ModelAndView livestock(HttpServletRequest request) throws ParseException {
			
			logger.debug("livestock");
			
			ModelAndView mav = setBasicData();
			boolean isGated = false;
			
			List<Activity> activities = new ArrayList<Activity>();
			List<Activity> latestInfos = new ArrayList<Activity>();
			
			Account account = getCurrentAccount();
			//Account accountWR = accountService.getbyIdWithLayers(account.getId());
			Account acctWithALAS = accountService.getALAsbyId(account.getId());
			Account acctWithLayers = accountService.getLayersbyId(account.getId());
			
			Set<Layer> layers = acctWithLayers.getLayers_1();	//Obtain the connected layers of the logged user
			List<Integer> comms = new ArrayList<Integer>();
			for(Layer e:layers){
				if(e.getId().equals(livestock)){
					comms.add(e.getId());
				}
			}
			activities.addAll(activityService.getEventsByCommIds(comms)); //Get the EVENTS of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(comms)); //Get the INFOS of the layers
			
			Set<AccountLayerAccess> gatedLayers = acctWithALAS.getAccountLayerAccesses();   //Obtain the gated layers of logged user
			List<Integer> gatedComms = new ArrayList<Integer>();
			for(AccountLayerAccess e:gatedLayers){
				if(e.getLayer().getId().equals(gatedLivestock)&&e.getStatus().equalsIgnoreCase("granted")){
					gatedComms.add(e.getLayer().getId());
					isGated = true;
				}
			}
			activities.addAll(activityService.getEventsByCommIds(gatedComms)); //Get the EVENTS with granted access of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(gatedComms)); //Get the INFOS with granted access of the layers
			
			List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities); //Gets all the activities
			Collections.sort(latestActivities, Activity.ActivityComparatorD);
			
			// Changing the date format for events to be displayed using "dd-mm-yyyy"
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	        for(int i=0; i<latestActivities.size();i++){
	        	Activity act = new Activity();
				Date date = sdf.parse(latestActivities.get(i).getStartDate());
				act.setStartDate(sdfDate.format(date));
				latestActivities.get(i).setStartDate(act.getStartDate());
	        }
			
			logger.debug("Access to Gated:"+ isGated);
			mav.addObject("isGated", isGated);
			mav.addObject("infos", latestInfos);
			mav.addObject("activities", latestActivities);
			mav.setViewName("/member/livestock");
			return mav;
		}
		
		
		@RequestMapping("/member/dairyfarmers")
		public ModelAndView diary(HttpServletRequest request) throws ParseException {
			
			logger.debug("Dairy");
			
			ModelAndView mav = setBasicData();
			boolean isGated = false;
			
			List<Activity> activities = new ArrayList<Activity>();
			List<Activity> latestInfos = new ArrayList<Activity>();
			
			Account account = getCurrentAccount();
			//Account accountWR = accountService.getbyIdWithLayers(account.getId());
			Account acctWithALAS = accountService.getALAsbyId(account.getId());
			Account acctWithLayers = accountService.getLayersbyId(account.getId());
			
			Set<Layer> layers = acctWithLayers.getLayers_1();	//Obtain the connected layers of the logged user
			List<Integer> comms = new ArrayList<Integer>();
			for(Layer e:layers){
				if(e.getId().equals(dairy)){
					comms.add(e.getId());
				}
			}
			activities.addAll(activityService.getEventsByCommIds(comms)); //Get the EVENTS of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(comms)); //Get the INFOS of the layers
			
			Set<AccountLayerAccess> gatedLayers = acctWithALAS.getAccountLayerAccesses();   //Obtain the gated layers of logged user
			List<Integer> gatedComms = new ArrayList<Integer>();
			for(AccountLayerAccess e:gatedLayers){
				if(e.getLayer().getId().equals(gatedDairy)&&e.getStatus().equalsIgnoreCase("granted")){
					gatedComms.add(e.getLayer().getId());
					isGated = true;
				}
			}
			activities.addAll(activityService.getEventsByCommIds(gatedComms)); //Get the EVENTS with granted access of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(gatedComms)); //Get the INFOS with granted access of the layers
			
			List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities); //Gets all the activities
			Collections.sort(latestActivities, Activity.ActivityComparatorD);
			
			// Changing the date format for events to be displayed using "dd-mm-yyyy"
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	        for(int i=0; i<latestActivities.size();i++){
	        	Activity act = new Activity();
				Date date = sdf.parse(latestActivities.get(i).getStartDate());
				act.setStartDate(sdfDate.format(date));
				latestActivities.get(i).setStartDate(act.getStartDate());
	        }
			
			logger.debug("Access to Gated:"+ isGated);
			mav.addObject("isGated", isGated);
			mav.addObject("infos", latestInfos);
			mav.addObject("activities", latestActivities);
			mav.setViewName("/member/dairyfarmers");
			return mav;
		}
		
		
		@RequestMapping("/member/horticulture")
		public ModelAndView horticulture(HttpServletRequest request) throws ParseException {
			
			logger.debug("Horticulture");
			
			ModelAndView mav = setBasicData();
			boolean isGated = false;

			
			List<Activity> activities = new ArrayList<Activity>();
			List<Activity> latestInfos = new ArrayList<Activity>();
			
			Account account = getCurrentAccount();
			//Account accountWR = accountService.getbyIdWithLayers(account.getId());
			Account acctWithALAS = accountService.getALAsbyId(account.getId());
			Account acctWithLayers = accountService.getLayersbyId(account.getId());
			
			Set<Layer> layers = acctWithLayers.getLayers_1();	//Obtain the connected layers of the logged user
			List<Integer> comms = new ArrayList<Integer>();
			for(Layer e:layers){
				if(e.getId().equals(horticulture)){
					comms.add(e.getId());
				}
			}
			activities.addAll(activityService.getEventsByCommIds(comms)); //Get the EVENTS of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(comms)); //Get the INFOS of the layers
			
			Set<AccountLayerAccess> gatedLayers = acctWithALAS.getAccountLayerAccesses();   //Obtain the gated layers of logged user
			List<Integer> gatedComms = new ArrayList<Integer>();
			for(AccountLayerAccess e:gatedLayers){
				if(e.getLayer().getId().equals(gatedHorticulture)&&e.getStatus().equalsIgnoreCase("granted")){
					gatedComms.add(e.getLayer().getId());
					isGated = true;
				}
			}
			activities.addAll(activityService.getEventsByCommIds(gatedComms)); //Get the EVENTS with granted access of the layers
			latestInfos.addAll(activityService.getInfosByCommIds(gatedComms)); //Get the INFOS with granted access of the layers
			
			List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities); //Gets all the activities
			Collections.sort(latestActivities, Activity.ActivityComparatorD);
			
			// Changing the date format for events to be displayed using "dd-mm-yyyy"
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	        for(int i=0; i<latestActivities.size();i++){
	        	Activity act = new Activity();
				Date date = sdf.parse(latestActivities.get(i).getStartDate());
				act.setStartDate(sdfDate.format(date));
				latestActivities.get(i).setStartDate(act.getStartDate());
	        }
			
			logger.debug("Access to Gated:"+ isGated);
			mav.addObject("isGated", isGated);
			mav.addObject("infos", latestInfos);
			mav.addObject("activities", latestActivities);
			mav.setViewName("/member/horticulture");
			return mav;
		}
		
		
		@RequestMapping("/member/inbox")
		public ModelAndView memberInbox () {
			logger.debug("----------Member Inbox -------------");
			
			ModelAndView mav = setBasicData();
			
			List<Message> messages = messageService.getByUserId(getCurrentAccount().getId());
			
			for(Message e:messages)
			{
				e.setFormattedDate(returnFormattedDate(e.getDateSent()));
			}
			
			mav.addObject("messages", messages);
			mav.setViewName("/member/inbox");
			
			return mav; 
		}
		
		@RequestMapping("/member/inbox/view/{messageid}")
		public ModelAndView memberInboxView (@PathVariable(value="messageid") Integer messageid) {
			logger.debug("----------Member Inbox View -------------");
			
			ModelAndView mav = setBasicData();
			Message message = messageService.findById(messageid);
			message.setIsRead(true);
			messageService.save(message);
			if(message.getBanners().size()>0)
			{
				logger.debug("------------This message has a banner------------ ");
				for(Banner b : message.getBanners())
				{
					logger.debug("------------Gets the Banner------------ ");
					if(b.getImageUrl()!=null)
					{
						logger.debug("----------banner url is = "+ b.getImageUrl());
						mav.addObject("banner", b.getImageUrl());
						break;
					}
				}
				
				for(Activity b : message.getActivities())
				{
					logger.debug("------------This message has a Activity------------ ");
					if(b!=null && b.getActivityType().equals("event"))
					{
						logger.debug("----------activity url is = "+ b.getActionTitle());
						mav.addObject("activity", b);
						break;
					}
				}
			}
			
			message.setFormattedDate(returnFormattedDate(message.getDateSent()));
			mav.addObject("message", message);
			mav.addObject("messagesubject", message.getSubject());
			mav.addObject("messagebody", message.getMessage());
			mav.setViewName("/member/inboxview");
			
			return mav; 
		}
		
		
		
		@RequestMapping(value="/member/inbox/delete", method= RequestMethod.POST)
		public String deleteMessages ( @RequestParam(value="ids", required=true) String[] ids) {
			
			logger.debug("----------Delete message -------------");
			
			if (ids != null && ids.length > 0){
				for (String id:ids){
					logger.debug("----------Deleting:"+id+" -------------");
					try{
						
						Message message = messageService.get(Integer.parseInt(id));
						if (message != null) {
							message.setDeleted(true);
							messageService.save(message);
						}
						
					}catch(Exception e){
						logger.debug("----------Exception for:"+id+" -------------");
						return "redirect:" + "/member/inbox";
					}
				}
			} 
			
			
			return "redirect:" + "/member/inbox";
		}
		
		

		@RequestMapping(value="/member/inbox/deleteCurrent", method= RequestMethod.POST)
		public String deleteMessage ( @RequestParam(value="id", required=true) String id) {
			
			logger.debug("----------Delete message -------------");
			
			if (id != null){
			
					logger.debug("----------Deleting:"+id+" -------------");
					try{
						
						Message message = messageService.get(Integer.parseInt(id));
						if (message != null) {
							message.setDeleted(true);
							messageService.save(message);
						}
						
					}catch(Exception e){
						logger.debug("----------Exception for:"+id+" -------------");
						return "redirect:" + "/member/inbox";
					}
			} 
			
			
			return "redirect:" + "/member/inbox";
		}
	

}
