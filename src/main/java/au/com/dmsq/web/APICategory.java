package au.com.dmsq.web;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Category;
import au.com.dmsq.service.CategoryService;

@Controller
public class APICategory extends AbstractController
{
	private static final Logger logger = LoggerFactory.getLogger(APICategory.class);
	
	@Autowired CategoryService categoryService;
	

	@ResponseBody 
	@RequestMapping(method={RequestMethod.GET}, value={"api/getCategories"})
	public  List<Category> category() {
		
		logger.debug("------------------------------entering getCategories ------------------------------");
		
		return categoryService.findAll();
	}

}
