package au.com.dmsq.web;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.apache.commons.lang.RandomStringUtils;
import org.jboss.logging.annotations.Param;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountConfirmation;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.AccountLayerAccessId;
import au.com.dmsq.model.Mail;
import au.com.dmsq.model.Message;
import au.com.dmsq.model.Role;
import au.com.dmsq.model.UserDTO;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.AccountLayerAccessService;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;
import au.com.dmsq.service.MessageService;
import au.com.dmsq.service.RoleService;
import au.com.dmsq.service.AccountConfirmationService;

@Controller
public class RegisterController extends AbstractController {
	
	
	@Autowired
	ServletContext servletContext;
	String contact_phone=null;
	
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
	
	@Autowired AccountService accountService;
	@Autowired RoleService roleService;
	@Autowired MailService mailService;
	@Autowired MailContentService mcs;
	@Autowired LayerService ls;
	@Autowired AccountLayerAccessService alAccessService;
	@Autowired MessageService messageRService;
	@Autowired AccountConfirmationService accountConfirmationService;
	
	// Enable register page
	@RequestMapping("/register")
	public String showRegistrationForm(WebRequest request, Model model) {
		
		UserDTO userDto = new UserDTO();
		Account account = new Account();
		model.addAttribute("user", userDto);
		model.addAttribute("account", account);
		
		//adding councils
		List<Layer> councils=ls.getregionallayers();
		model.addAttribute("councils", councils);
		return "/secure/user";
		}
	
	// Validates if an user already exist
	@RequestMapping(value = "/registration/", method = RequestMethod.POST)
	public ModelAndView validatesUserAccount(@ModelAttribute("user") @Valid UserDTO user, 
			BindingResult result, SessionStatus status) {  
		
		logger.debug("------------------------------Validating User  ------------------------------");
		
		if (result.hasErrors()) {
			//if validator failed
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/secure/user"); //jsp
			return mav;
			
		} else {
			
			boolean saveResult = false; 
			ModelAndView mav = new ModelAndView(); 
			List<Account> accounts = accountService.findByEmail(user.getUsername());
			if(accounts.isEmpty()){
				Account account = new Account();
				account.setEmail(user.getUsername());
				account.setFirstName(user.getFirstName());
				account.setLastName(user.getLastName());
				Role role = new Role();
				role.setId(3);
				account.setRole(role);
				account.setEnabled(false);
				account.setFacebookUid("0");
				account.setGoogleUid("0");
				account.setImageUrl("https://s3.amazonaws.com/ccmeresources/icons/default_avatar.jpg");
				account.setStatus("unconfirmed");
				account.setName(account.getFirstName()+" "+account.getLastName());
				mav.addObject("account", account);	
				mav.setViewName("/secure/newAccount"); //jsp
				return mav;
			}
			mav.setViewName("/register"); //jsp
			return mav;
		}
	}
	
	// Creates an account 
	@RequestMapping(value = "/newAccount", method = RequestMethod.POST)
	public ModelAndView registerUserAccount(@ModelAttribute(value="user") UserDTO user,
		BindingResult result, SessionStatus status){
		logger.debug("------------------------------Validating User  ------------------------------");
	
		ModelAndView mav = new ModelAndView(); 
		Account account = new Account();
		account.setEmail(user.getUsername());
		account.setFirstName(user.getFirstName());
		account.setLastName(user.getLastName());
		//account.setLayers_1(layers_1);
		mav.addObject("account", account);	
		mav.setViewName("/secure/newAccount"); //jsp
		
		return mav;
		
	}
	
	@RequestMapping(value="/account/save", method= RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("account") @Valid Account account, BindingResult result, SessionStatus status,
			@RequestParam(value="regionalMultiSelect", required = false) String[] st,
			@RequestParam(value="checkboxG1", required = false) String[] industriesInter) throws Exception {
		
		logger.debug("------------------------------Save Account account/save------------------------------");
		logger.debug("User:" + account.getEmail());
        
		
		ModelAndView mav = new ModelAndView(); 
		mav.addObject("account", account);
		List<Account> accounts = accountService.findByEmail(account.getEmail());
		if(accounts.isEmpty()){
		
				if (result.hasErrors()) {
					mav.setViewName("/secure/user"); //jsp
					logger.debug("errors:" + result.getErrorCount());
					return mav;
					
				} else {
					
					logger.debug("No errors");
					boolean saveResult = false; 
					
					Role role = new Role();
					role.setId(3);
					account.setRole(role);
					account.setEnabled(true);
					account.setFacebookUid("0");
					account.setGoogleUid("0");
					account.setImageUrl("https://s3.amazonaws.com/ccmeresources/icons/default_avatar.jpg");
					account.setStatus("unconfirmed");
					account.setName(account.getFirstName()+" "+account.getLastName());
					
					if (accountService.save(account)){
						saveResult = true;
						// This code generates a confirmation code record when a new account is created
						AccountConfirmation ac = new AccountConfirmation();
						String confirmationCode = SHA1(String.valueOf(SecureRandom.getInstance("SHA1PRNG").nextLong()));
						ac.setConfirmationCode(confirmationCode);
						ac.setAccount(accountService.findById(account.getId()));
						ac.setConfirmed(false);
						ac.setId(account.getId());
						ac.setRequestDate((System.currentTimeMillis()+34200)/1000);
						accountConfirmationService.save(ac);
						Set<AccountConfirmation> acs= new HashSet<>();
						acs.add(ac);
						account.setAccountConfirmations(acs);
					}
					
					if(saveResult)
					{
						  JSONObject jsonParam = new JSONObject();
						  
						  jsonParam.put("channel", "#ccme-events");
						  jsonParam.put("username", "RuralConnect");
				          jsonParam.put("icon_emoji", ":trophy:");  
				          jsonParam.put("text", "New Rural Connect User : "+account.getEmail().substring(0, 6)+"...@XXX.com user #" + account.getId());
				            
				          PostinSlack(jsonParam);
					}

					String x = (account.getPassword()+"{"+account.getEmail()+"}");
				    String encoded = SHA1(x);
				    account.setPassword(encoded);
					Set<Layer> addLayers = new HashSet<Layer>();
					
					//Regional Councils
					if(st!=null && st.length>0)
					{
						for(String str:st)
						{
							Layer temp=ls.findById(Integer.parseInt(str));
							if(temp.getId()!=null)
							{
								addLayers.add(temp);
							}
						}
					}
					
					//Industries Interest
					if (industriesInter!=null)
					if(industriesInter.length>0)
					{
						for(String str:industriesInter)
						{
							Layer temp=ls.findById(Integer.parseInt(str));
							if(temp.getId()!=null)
							{
								addLayers.add(temp);
							}
						}
					}
					
					if(addLayers.size()>0)
					{
					account.setLayers_1(addLayers);
					}
				    accountService.save(account);
					Set<AccountConfirmation> accConfs = account.getAccountConfirmations();
					
					Mail mail = new Mail();
					mail.setFrom(FROM_EMAIL);
					mail.setSubject("Welcome to CC-Me "+account.getName()+".");
					String body = mcs.readConfirmationEmail();
					String link = SERVER  + "/confirmAccount/"+accConfs.iterator().next().getConfirmationCode()+"/"+account.getId();
					body += " <a href='"+link+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Click here to confirm now</button></a><p><p></a>";
					body += mcs.readConfirmationEmailLower();
					
					mail.setMessage(mcs.insertUnsubscribe(account, body));
					mailService.welcomeMail(mail.getFrom(), account.getEmail(), mail.getSubject(), mail.getMessage());
					
					mav = new ModelAndView();
					mav.setViewName("/secure/postRegistration");
					mav.getModel().put("acemail", account.getEmail());
					mav.addObject("message", saveResult);
					return mav;
				}
		}else {
			
			mav.setViewName("/secure/user");
			logger.debug("User already exists");
			mav.addObject("emailError", true);
			return mav;
		}
	}
	
	
	@RequestMapping("/success")
	public ModelAndView success() {
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/secure/success"); // jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/postRegistrationProcess", method= RequestMethod.POST)
	public ModelAndView postRegistrationProcess(@RequestParam(value="checkboxGated",required=false) String[] gated,@RequestParam("email") String emailad) {
		
		logger.debug("------------------------------Post Processing------------------------------");
		logger.debug("User:" + emailad);
		ModelAndView mav = new ModelAndView();
		if(gated!=null && gated.length>1)
		{
			//Retrieving the account based on email 
		List<Account> lsAccounts=accountService.findByEmail(emailad);
		Account currentAccount;
		
		if((!lsAccounts.isEmpty())&& lsAccounts.size()>0)
		{
			currentAccount=lsAccounts.get(0);
			AccountLayerAccess alayeraccess=new AccountLayerAccess();
			AccountLayerAccessId alayeraccessId= new AccountLayerAccessId();
			Layer currentLayer=new Layer();
			try
			{
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

			for(String gComm: gated)
			{
				String grantAccessCode= SHA1(String.valueOf(random.nextLong()));
				String denyAccessCode= SHA1(String.valueOf(random.nextLong()));
				currentLayer=ls.findById(Integer.parseInt(gComm));
				alayeraccessId.setAccountId(currentAccount.getId());
				alayeraccessId.setLayerId(Integer.parseInt(gComm));
				
				alayeraccess.setId(alayeraccessId);
				alayeraccess.setAccount(currentAccount);
				alayeraccess.setGrantAccessCode(grantAccessCode);
				alayeraccess.setDenyAccessCode(denyAccessCode);
				alayeraccess.setLayer(currentLayer);
				alayeraccess.setLastStatusUpdate(new Date());
				alayeraccess.setStatus("requested");
				 contact_phone = currentAccount.getMobilephone();
				 if(contact_phone == null)
				 {
					 contact_phone = currentAccount.getLandline();
					 if(contact_phone == null)
						 contact_phone = "Not Provided";
				 }
						
				if(alAccessService.save(alayeraccess))
				{
					//Grant/Deny email to the community owner
					Mail mail = new Mail();
					mail.setFrom(FROM_EMAIL);
					mail.setSubject("Access Request for the Community "+currentLayer.getName()+".");
					String body = mcs.readAccessRequestEmail();
					String mainBody = "User: " + currentAccount.getName()+ " with email: " + currentAccount.getEmail() + " and contact telephone number: " + contact_phone + " has requested  access to " +currentLayer.getName() + " community . Click the below links to grant or deny access.";
					String grantlink = SERVER  + "/token/"+grantAccessCode+"/action/"+"grant";
					String revokelink = SERVER +   "/token/"+denyAccessCode+"/action/"+"deny";
					mainBody += " <a href='"+grantlink+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Click here to confirm now</button></a><p><p>	 Or click the link below to deny:<p><p> <a href='"+revokelink+"'>"+"Deny"+"</a>";
					body = body.replaceAll("pasteBodyHere", mainBody);
					mail.setMessage(mcs.insertUnsubscribe(currentLayer.getAccount(), mainBody));
					mailService.accessRequestManagerEmail(mail.getFrom(), currentLayer.getAccount().getEmail(), mail.getSubject(), mail.getMessage());
					
					
					//Confirmation email to the user
					Mail confirmmail = new Mail();
					confirmmail.setFrom(FROM_EMAIL);
					confirmmail.setSubject("Confirmation - "
							+ " Request for the Community "+currentLayer.getName()+".");
					String bodyMail = mcs.readAccessRequestMemberEmail();
					String cbody = " Confirmation mail for the access request. Your access request for the community "+currentLayer.getName()+" has been submitted to the community owner.";
					bodyMail = bodyMail.replaceAll("pasteBodyHere", cbody);
					confirmmail.setMessage(mcs.insertUnsubscribe(currentAccount, bodyMail));
					mailService.accessRequestMemberEmail(confirmmail.getFrom(), currentAccount.getEmail(), confirmmail.getSubject(), confirmmail.getMessage());
				}
			}
			}
			catch(Exception e)
			{
				logger.error(e.getMessage());
			}
			finally
			{
				mav.setViewName("/secure/success"); // jsp
				return mav; 
			}
		  }
		} 
		mav.setViewName("/secure/success"); // jsp
		return mav; 
		}
	
	
	@RequestMapping(value="/action/changePassword", method= RequestMethod.POST)
	public ModelAndView receivePassword(
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "passwordConfirm", required = true) String passwordConfirm,
			@RequestParam(value = "token", required = true) String token) {

		logger.debug("-----------------------------Changing new Password------------------------------");

		Account currentAccount = accountService.getByResetCode(token);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/resetThree");

		if (currentAccount != null) {
			try {
				logger.debug("Reset password for user:"
						+ currentAccount.getEmail());
				String x = password;
				String xx = (x + "{" + currentAccount.getEmail() + "}");
				String encoded = SHA1(xx);
				currentAccount.setPassword(encoded);
				currentAccount.setResetCode("");
				
				accountService.save(currentAccount);
				
				mav.addObject("done", true);
			} catch (Exception e) {

				logger.debug("Error" + e);
			}
		}	
		return mav; 
	}
	
	
	
	@RequestMapping(value="/action/reset", method= RequestMethod.POST)
	public ModelAndView resetPassword(@RequestParam(value="email",required=true) String email) {
		
		logger.debug("-----------------------------Reset Password------------------------------");
		logger.debug("Reset password for user:" + email);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/resetOne");

		try {
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			String token = SHA1(String.valueOf(random.nextLong()));

			logger.debug("Reset Token:" + token);
			
			List<Account> lsAccounts = accountService.findByEmail(email);
			Account currentAccount;

			if ((!lsAccounts.isEmpty()) && lsAccounts.size() > 0) {
				currentAccount = lsAccounts.get(0);
				currentAccount.setResetCode(token);
				accountService.save(currentAccount);

					Mail mail = new Mail();
					mail.setFrom(FROM_EMAIL);
					mail.setSubject("Password reset for: "
							+ currentAccount.getName() + ".");
					String body = mcs.readForgotPassword();
					String resetPass = "Dear "
							+ currentAccount.getName()
							+ ", to reset your password please. Click the link below.";
					String link = SERVER 
							+ "/action/reset/" + token + "/confirm" ;

					resetPass += " <center><a href='"
							+ link
							+ "'><button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Click here to reset now</button></a></center><p>";
					body = body.replaceAll("pasteBodyHere", resetPass);
					
					mail.setMessage(mcs.insertUnsubscribe(currentAccount, body));
					mailService.resetPasswordMail(mail.getFrom(), currentAccount.getEmail(), mail.getSubject(), mail.getMessage());
			}else {
				mav.addObject("error", "User not found. Creat and account");
			}

		} catch (Exception e) {

			logger.error("Error:" + e);
		}
		return mav;
	}
	

	@RequestMapping(value="/action/reset/{token}/confirm", method = RequestMethod.GET)
	public ModelAndView changePassword(@PathVariable("token") String tokenId) {

		ModelAndView mav = new ModelAndView();
		mav.setViewName("resetTwo");

		Account currentAccount = accountService.getByResetCode(tokenId);

		if (currentAccount != null) {
			mav.addObject("token",tokenId);
		}

		return mav;
	}
	
	
	
	@RequestMapping(value = "/token/{token}/action/{actioncode}", method = RequestMethod.GET)
	public ModelAndView getLogin(@PathVariable("token") String tokenId,
								 @PathVariable("actioncode") String actioncode ) {

		String result = "Failed";
		String message = "Success!";
		ModelAndView mav = new ModelAndView("/secure/actionFeedback");

		if (tokenId != null && actioncode != null) {
			AccountLayerAccess temp = new AccountLayerAccess();
			if (actioncode.equalsIgnoreCase("grant")) {
				temp = alAccessService.getbyGrantToken(tokenId);
				if (temp != null) {
					temp.setStatus("granted");
					temp.setLastStatusUpdate(new Date());
					if (!alAccessService.save(temp)) {
						sendconfirmationMessage(temp);
						result = "Succeded";
						message = "Access granted successfully for the Account ="
								+ temp.getAccount().getEmail()
								+ " to "
								+ temp.getLayer().getName();
						logger.debug("Access granted successfully for the Account ="
								+ temp.getAccount().getEmail()
								+ " to "
								+ temp.getLayer().getName());
						mav.addObject("granted", true);
						mav.addObject("account", temp.getAccount());
						mav.addObject("layer" , temp.getLayer().getName());
					} else {
						logger.error("Unable to save changes");
						mav.addObject("success", false);
					}
				} else {
					logger.error("Unable to find the grant token id : "
							+ tokenId);
				}
			} else {
				temp = alAccessService.getbyDenyToken(tokenId);
				if (temp != null) {
					temp.setStatus("denied");
					temp.setLastStatusUpdate(new Date());

					logger.debug(temp.getAccount().getId() + " layer"
							+ temp.getLayer().getName() + " "
							+ temp.getDenyAccessCode());
					if (!alAccessService.save(temp)) {
						sendconfirmationMessage(temp);
						result = "Succeded";

						message = "Access denied successfully for the Account ="
								+ temp.getAccount().getEmail()
								+ " to the Layer " + temp.getLayer().getName();
						logger.debug("Access denied successfully for the Account ="
								+ temp.getAccount().getEmail()
								+ " to the Layer " + temp.getLayer().getName());
						mav.addObject("denied", true);
						mav.addObject("account", temp.getAccount());
						mav.addObject("layer" , temp.getLayer().getName());
					} else {
						logger.error("Unable to save changes");
					}
				} else {
					logger.error("Unable to find the deny token id : "
							+ tokenId);
				}
			}

		} else {
			logger.error("Illegal number of arguments ");
		}

		mav.addObject("message", message);
		return mav;
	}
	 
	private void sendconfirmationMessage(AccountLayerAccess temp)
	{
		//Message to the user
 		Message userMessage=new Message();
 		userMessage.setAccountByToId(temp.getAccount());
 		userMessage.setAccountByFromId(temp.getLayer().getAccount());
 		userMessage.setSubject("Update on access request for the Community "+temp.getLayer().getName()+".");
 		userMessage.setMessage("Your request to access the community " +temp.getLayer().getName()+" has been "+temp.getStatus() + " by the community owner");
 		userMessage.setDateSent(System.currentTimeMillis()/1000);
 		userMessage.setDeleted(false);
 		userMessage.setIsRead(false);
 		messageRService.save(userMessage);
 		
 		
		//Confirmation email to the user
		Mail confirmmail = new Mail();
		confirmmail.setFrom(FROM_EMAIL);
		confirmmail.setSubject("Update on access request for the Community "+temp.getLayer().getName()+".");
		String body = mcs.readAccessGrantedEmail();
		String cbody = "Your request to access the community " +temp.getLayer().getName()+" has been "+temp.getStatus() + " by the community owner";
		body = body.replaceAll("pasteBodyHere", cbody);
		
		confirmmail.setMessage(mcs.insertUnsubscribe(temp.getAccount(), body));
		mailService.welcomeMail(confirmmail.getFrom(), temp.getAccount().getEmail(), confirmmail.getSubject(), confirmmail.getMessage());
	}

	
	@RequestMapping("/privacy")
	public ModelAndView privacy(){
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/privacy");// jsp
		return mav; 
	}
	
	
	@RequestMapping("/contact")
	public ModelAndView contact(){
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/contact");// jsp
		return mav; 
	}
	
	
	@RequestMapping("/confirmAccount/{cc}/{id}")
	public ModelAndView confirmAccount( @PathVariable(value="id") Integer id,
										@PathVariable("cc") String cc) throws Exception {
		
		Account account = accountService.getbyIdWithAccConfirmations(id);
		Set<AccountConfirmation> acs = account.getAccountConfirmations();
		
		for(AccountConfirmation ac:acs){
			if(ac.getConfirmationCode().equalsIgnoreCase(cc)){
				account.setStatus("active");
				account.setEnabled(true);
				ac.setConfirmed(true);
				ac.setConfirmedDate((System.currentTimeMillis()+34200)/1000);
				accountConfirmationService.save(ac);
				Set<AccountConfirmation> acsy= new HashSet<>();
				acsy.add(ac);
				account.setAccountConfirmations(acsy);
				accountService.save(account);
			}
		}
		
		ModelAndView mav = new ModelAndView(); 
		mav.addObject("account", account);	
		mav.setViewName("/secure/confirmAccount"); // jsp
		return mav; 
	}
	

	@RequestMapping("/secure/resendConfirmation/{id}")
	public String resendConfirmation( @PathVariable(value="id") Integer id) throws Exception {
		
		Account account = accountService.getbyIdWithAccConfirmations(id);
		Set<AccountConfirmation> acs = account.getAccountConfirmations();

		logger.error("account.getAccountConfirmations().size()=====> "+account.getAccountConfirmations().size());
		if(account.getAccountConfirmations().size()<1){
			logger.error("Size < 1");
			// This code generates a confirmation code record when a new account is created
			AccountConfirmation ac = new AccountConfirmation();
			String confirmationCode = SHA1(String.valueOf(SecureRandom.getInstance("SHA1PRNG").nextLong()));
			ac.setConfirmationCode(confirmationCode);
			ac.setAccount(accountService.findById(account.getId()));
			ac.setConfirmed(false);
			ac.setId(account.getId());
			ac.setRequestDate((System.currentTimeMillis()+34200)/1000);
			accountConfirmationService.save(ac);
			acs.add(ac);
			account.setAccountConfirmations(acs);
			for(AccountConfirmation ac0:acs){
				if(ac0.getConfirmed()==false){
					Mail mail = new Mail();
					mail.setFrom(FROM_EMAIL);
					mail.setSubject("RE-Confirmation Email from CC Me.");
					String body = mcs.readConfirmationEmail();
					String link = SERVER  + "/confirmAccount/"+ac0.getConfirmationCode()+"/"+account.getId();
					body += " <a href='"+link+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Click here to confirm now</button></a><p><p></a>";
					body += mcs.readConfirmationEmailLower();
					mail.setMessage(mcs.insertUnsubscribe(account, body));
					mailService.welcomeMail(mail.getFrom(), account.getEmail(), mail.getSubject(), mail.getMessage());
				}
			}
		}else if(account.getAccountConfirmations().size()>0){
			for(AccountConfirmation ac:acs){
				logger.error("Size > 0");
				if(ac.getConfirmed()==false){
					Mail mail = new Mail();
					mail.setFrom(FROM_EMAIL);
					mail.setSubject("RE-Confirmation Email from CC Me.");
					String body = mcs.readConfirmationEmail();
					String link = SERVER  + "/confirmAccount/"+ac.getConfirmationCode()+"/"+account.getId();
					body += " <a href='"+link+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Click here to confirm now</button></a><p><p></a>";
					body += mcs.readConfirmationEmailLower();
					mail.setMessage(mcs.insertUnsubscribe(account, body));
					mailService.welcomeMail(mail.getFrom(), account.getEmail(), mail.getSubject(), mail.getMessage());
				}
			}
		}
		return "redirect:" + "/success"; 
	}
	
	
	@RequestMapping("/confirmNewEmail/{id}")
	public ModelAndView confirmNewEmail(@PathVariable(value="id") Integer id) throws Exception {
		
		Account account = accountService.getbyIdWithRelations(id);
		String alternate = account.getEmail();
		account.setEmail(account.getAlternateEmail());
		account.setAlternateEmail(alternate);
		accountService.save(account);
		
		ModelAndView mav = new ModelAndView(); 
		mav.addObject("account", account);	
		mav.setViewName("/secure/confirmNewEmail"); // jsp
		return mav; 
	}
	
	
	@RequestMapping(value = "/checkUniqueMail",  method = RequestMethod.GET)
	@ResponseBody
	public String checkUniqueEmail(@RequestParam(value = "email") String email)throws Exception {
		logger.debug("CheckUniqueMail for"+ email);
		if (!accountService.findByEmail(email).isEmpty()) {
			return "false";
		} else {
			return "true";
		}
	}
	
	private static String convertToHex(byte[] data) { 
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 

	public static String SHA1(String text) 
		    throws NoSuchAlgorithmException, UnsupportedEncodingException  { 
		    MessageDigest md;
		    md = MessageDigest.getInstance("SHA-1");
		    byte[] sha1hash = new byte[40];
		    md.update(text.getBytes("iso-8859-1"), 0, text.length());
		    sha1hash = md.digest();
		    return convertToHex(sha1hash);
		    } 
	
	private static void PostinSlack(JSONObject message)
	{
        URL url;
        HttpURLConnection connection = null;
    try {
        // TODO code application logic here
    	     url = new URL("https://hooks.slack.com/services/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");
           connection = (HttpURLConnection) url.openConnection();
           connection.setRequestMethod("POST");
           connection.setConnectTimeout(5000);
           connection.setUseCaches(false);
           connection.setDoInput(true);
           connection.setDoOutput(true);        
           String payload = "payload="
           + URLEncoder.encode(message.toString(), "UTF-8");
           // Send request
           DataOutputStream wr = new DataOutputStream(
           connection.getOutputStream());
           wr.writeBytes(payload);
           wr.flush();
           wr.close();
           
           InputStream is = connection.getInputStream();
           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
           String line;
           StringBuffer response = new StringBuffer();
           while ((line = rd.readLine()) != null) {
           response.append(line);
           response.append('\r');
           }
           logger.debug("Response from slack :"+response.toString());
           rd.close();
        //urlConn.connect();
        //Create JSONObject here

    } catch (IOException ex) {
    	logger.error(ex.toString());
    }
	}

}
