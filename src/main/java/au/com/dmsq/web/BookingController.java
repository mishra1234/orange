package au.com.dmsq.web;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;

import au.com.dmsq.dao.BookingDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.ActivityCalendar;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Booking;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.File;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityCalendarService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.BookingService;
import au.com.dmsq.service.EventCalendarItemService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.FileService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;


@Controller
public class BookingController extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired EventService eventService;
	@Autowired EventCalendarItemService eventCalendarItemService;
	@Autowired ActivityService activityService;
	@Autowired LayerService layerService;
	@Autowired AccountService accountService;
	@Autowired BannerService bannerService;
	@Autowired FileService fileService;
	@Autowired MailService mailService;
	@Autowired MailContentService mcs;
	@Autowired BookingService bookingService;
	@Autowired ActivityCalendarService activityCalendarService;
	
	@Transactional
	@RequestMapping(value="/admin/booking/{eventId}/{commId}", method= RequestMethod.GET)
	public String booking(@PathVariable (value="eventId")Integer eventId, @PathVariable (value="commId")Integer commId
			) {
		
		logger.debug("------------------------------Admin - Book Event ------------------------------");
		
		ModelAndView mav = setBasicData();
		Account accountt = getCurrentAccount();
		
		try {
			Activity act = activityService.getStartandEndDatesfromEvent(activityService.findById(eventId));
			EventCalendarItem eci = eventCalendarItemService.getLastECIbyActid(eventId);
			
			UUID uid = UUID.fromString(eci.getId().getCalHref().substring(0, 36)); 
			UUID href = uid.randomUUID();
			String realHref = href+".ics";
			
			System.out.println("Save eci.getCalEtag: "+eci.getCalEtag()); 
			System.out.println("Save act.getStartDate() act.getStartTime(): " +(act.getStartDate()+" "+act.getStartTime())); 
			System.out.println("Save act.getEndDate() act.getEndTime(): "+(act.getEndDate()+" "+act.getEndTime())); 
			System.out.println("Save eci.getCalData(): "+eci.getCalData()); 
			System.out.println("Save realHref: "+realHref);
			eventCalendarItemService.toBaikal(eci.getCalEtag(), (act.getStartDate()+" "+act.getStartTime()), (act.getEndDate()+" "+
					act.getEndTime()), eci.getCalData(), realHref);
			
			Booking booking = new Booking();
			booking.setAccount(accountt);	
			System.out.println("Booking account    : "+accountt.getId()+" "+accountt.getEmail()); 
			booking.setActivity(act);
			System.out.println("Booking activity   : "+act.getId()+ " "+act.getContentTitle()); 
			booking.setBookingCalHref(realHref);
			System.out.println("Booking Ev HREF    : "+realHref);
			booking.setCcmeCalHref(eci.getId().getCalHref());
			System.out.println("Booking Book HREF  : "+eci.getId().getCalHref());
			Boolean result = bookingService.save(booking);
			System.out.println("Save Booking Result: "+ result);
			
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//System.out.println("DB Pass");
		//mav.addObject("numbers", new ArrayList<Integer>());
		logger.debug("----------New Booking -------------");
		return "redirect:/admin/events/1/"+ commId +"/110010011";
	}
	
	
	@RequestMapping(value="/admin/booking/bookings/{eventId}/{commId}", method= RequestMethod.GET)
	public ModelAndView bookings(@PathVariable (value="eventId")Integer eventId, @PathVariable (value="commId")Integer commId
			) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		logger.debug("------------------------------ Admin List Bookings ------------------------------");
		
		ModelAndView mav = setBasicData();
		Account accountt = getCurrentAccount();
		Activity activity = activityService.get(eventId);
		
		List<Booking> bookings = new ArrayList<>();
		bookings.addAll(bookingService.getBookingsByActivityId(eventId));
		
		logger.debug("Bookings -------------");
		logger.debug("For this activity: "+bookings.size());
		
		mav.addObject("commId", commId);
		mav.addObject("activity", activity);
		mav.addObject("account", accountt);
		mav.addObject("bookings", bookings);
		
		mav.setViewName("/admin/booking/bookings"); // jsp
		return mav; 
	}
	
	@RequestMapping(value="/admin/booking/schedules/{eventId}/{commId}", method= RequestMethod.GET)
	public ModelAndView schedules(@PathVariable (value="eventId")Integer eventId, @PathVariable (value="commId")Integer commId
			) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		logger.debug("------------------------------ Admin List Schedules ------------------------------");
		
		ModelAndView mav = setBasicData();
		Account accountt = getCurrentAccount();
		Activity activity = activityService.get(eventId);
		
		List<ActivityCalendar> schedules = new ArrayList<>();
		schedules.addAll(activityCalendarService.getScehdulesByActivityId(eventId));
		
		logger.debug("Schedules -------------");
		logger.debug("For this activity: "+schedules.size());
		
		mav.addObject("commId", commId);
		mav.addObject("activity", activity);
		mav.addObject("account", accountt);
		mav.addObject("schedules", schedules);
		
		mav.setViewName("/admin/booking/schedules"); // jsp
		return mav; 
	}
	
	
//	private static void bookEvent(JSONArray arr,String token)
//	{
//        URL url;
//        HttpURLConnection connection = null;
//        try {
//        	//Get auth header
//        	// TODO code application logic here
//           url = new URL("http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/message/send/batch");
//           connection = (HttpURLConnection) url.openConnection();
//           connection.setRequestMethod("POST");
//           connection.setConnectTimeout(5000);
//           connection.setRequestProperty("Auth-Token", token);
//           connection.setUseCaches(false);
//           connection.setDoInput(true);
//           connection.setDoOutput(true);  
//           // Send request
//           DataOutputStream wr = new DataOutputStream(
//           connection.getOutputStream());
//           wr.writeBytes(arr.toString());
//           wr.flush();
//           wr.close();
//           
//           InputStream is = connection.getInputStream();
//           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//           String line;
//           StringBuffer response = new StringBuffer();
//           while ((line = rd.readLine()) != null){
//        	   response.append(line);
//        	   response.append('\r');
//           }
//           System.out.println("Response :"+response.toString());
//           rd.close();
//        }catch (IOException ex) {
//        	System.out.println(ex.toString());
//        }
//	}
	
	
		
}


