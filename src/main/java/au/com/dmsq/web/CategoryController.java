package au.com.dmsq.web;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Category;
import au.com.dmsq.model.CategoryDTO;
import au.com.dmsq.service.CategoryService;

@Controller
public class CategoryController extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

	@Autowired CategoryService categoryService;
	@Autowired ErrorsController errorsController;
	
	@Transactional
	@RequestMapping(value="/admin/categories", method= RequestMethod.GET)
	public ModelAndView categories() {
		
		logger.debug("------------------------------ Admin - List Categories ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		try{
			mav.setViewName("/admin/category/categories"); // jsp
			
			List<Category> listcat = categoryService.getNonDeleted(); 
			for (Category cat:listcat){
				cat.setConnections(categoryService.countConnections(cat.getId().toString()));
				cat.setNumberLayers(categoryService.countCommunities(cat.getId().toString()));
				cat.setNumberEvents(categoryService.countActivities(cat.getId().toString()));
			}
			mav.addObject("myList", listcat);
			mav.addObject("acct", getCurrentAccount());
			
			return mav; 
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			mav = errorsController.error500();
			return mav;
		}
	}
	
	@RequestMapping(value="/manager/categories", method= RequestMethod.GET)
	public ModelAndView categoriesMgr() {
		
		logger.debug("------------------------------ Manager - List Categories ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/manager/category/categories"); // jsp
		
		List<Category> listcat = categoryService.getNonDeleted(); 
		mav.addObject("myList", listcat);
		mav.addObject("acct", getCurrentAccount());
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/category/save", method= RequestMethod.POST)
	@ResponseBody
	public ModelAndView save(@RequestParam("name")String name, @RequestParam("iconUrl")String iconUrl, @RequestParam("parentIds") String [] parentIds) {
		
		logger.debug("------------------------------ Admin - Save Category ------------------------------");
		
		logger.debug("Name: " + name);
		logger.debug("Name: " + iconUrl);
		logger.debug("Name: " + parentIds);
		
		ModelAndView mav =  categories();
		
		try{
			Category category = new Category(); 
			category.setName(name);
			category.setIconUrl(iconUrl);
			category.setDeleted(false);
			
			Set<Category> parentCategories = new HashSet<Category>(); 
			for (String id: parentIds){
				parentCategories.add(categoryService.findById(new Integer(id)));
			}
			category.setCategoriesForParentCategoryId(parentCategories);
			
			mav.addObject("message", categoryService.save(category));
			mav.addObject("acct", getCurrentAccount());
			
			return mav; 
			
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			mav = errorsController.error500();
			return mav;
		}
	}
	
	@RequestMapping("/admin/category/{id}")
	public ModelAndView getCategory(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get Single Category ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		
		mav.setViewName("/admin/category/category"); //jsp
		
		Category cat = categoryService.findById(id); 
		mav.addObject("cat", cat);
		mav.addObject("acct", getCurrentAccount());
		return mav; 
	}
	
	
	@RequestMapping("/admin/category/delete/{id}")
	public ModelAndView delete(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Delete Single Category ------------------------------");

		ModelAndView mav =  categories();
		
		try{
			Category cat = categoryService.findById(id); 
			cat.setDeleted(true);
			categoryService.save(cat);
			
			mav.addObject("message", categoryService.save(cat));
			mav.addObject("acct", getCurrentAccount());
			
			return mav; 
			
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			mav = errorsController.error500();
			return mav;
		}
	}
	
	
	@RequestMapping("/admin/category/new")
	public ModelAndView newCategory() {
		
		logger.debug("------------------------------ Admin - New Category ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Category cat = new Category(); 
		mav.addObject("listOfCategories", categoryService.getNonDeleted());
		mav.addObject("category", cat);
		mav.setViewName("/admin/category/newCategory"); //jsp
		mav.addObject("acct", getCurrentAccount());
		
		return mav; 
	}
	
	
	@RequestMapping("/admin/category/edit/{id}")
	public ModelAndView editCategory(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Edit Category ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Category cat = categoryService.findById(id); 
		
		mav.addObject("listOfCategories", categoryService.getNonDeleted());
		mav.addObject("parentCats", cat.getCategoriesForParentCategoryId() );
		mav.addObject("category", cat);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/category/newCategory"); //jsp
		return mav; 
	}
}
