package au.com.dmsq.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.io.*;
import java.math.BigDecimal;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.PropertyList;
import net.fortuna.ical4j.model.Recur;
import net.fortuna.ical4j.model.WeekDay;

import org.json.*;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import edu.emory.mathcs.backport.java.util.Collections;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.ActivityPayment;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.File;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.EventCalendarItemId;
import au.com.dmsq.model.IconDTO;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Mail;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityPaymentService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.EventCalendarItemService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.FileService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderGeometry;
import com.google.code.geocoder.model.GeocoderLocationType;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.GeocoderStatus;
import com.google.code.geocoder.model.LatLng;
import com.google.gson.JsonArray;


@Controller
public class EventController extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired EventService eventService;
	@Autowired EventCalendarItemService eventCalendarItemService;
	@Autowired ActivityService activityService;
	@Autowired LayerService layerService;
	@Autowired BannerService bannerService;
	@Autowired FileService fileService;
	@Autowired MailService mailService;
	@Autowired MailContentService mcs;
	@Autowired CommunityController commController;
	@Autowired ActivityPaymentService actPaymentServie;
	@Autowired ErrorsController errorsController;
	
	@Transactional
	@RequestMapping(value="/admin/events", method= RequestMethod.GET)
	public ModelAndView events() {
		
		logger.debug("------------------------------Admin - List of Events ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 	
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		Set<Layer> layers = acct.getLayers();
		List<Integer> layersIds = new ArrayList<Integer>();
		
		for(Layer community: layers){
			layersIds.add(community.getId());
		}
		
		Set<Activity> events = activityService.getEventsByCommIds(layersIds);
		List<Activity> myEvents = new ArrayList<>();
		
		myEvents.addAll(activityService.getEventsByOwnerId(acct.getId()));
		myEvents.removeAll(events);
		
		
		Integer max = 20;
		Integer initial=0;
		Integer curr=1;
		Integer prev=1;
		Integer next=2;
		Integer hmany = (myEvents.size()/max);
		
		if(myEvents.size()>max){
			List<Activity> list = new ArrayList<>(myEvents);
			Set<Activity> subSet = new LinkedHashSet<>(list.subList(initial, max));
			mav.addObject("myActivitiesList", subSet);
		}
		if(myEvents.size()<=max){
			List<Activity> list = new ArrayList<>(myEvents);
			mav.addObject("myActivitiesList", list);
		}
		
		mav.addObject("prev", prev);
		mav.addObject("next", next);
		mav.addObject("hmany", hmany);
		mav.addObject("curr", curr);
		mav.addObject("acct", getCurrentAccount());
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=0; i< hmany+1; i++){
			numbers.add(i+1);
		}
		mav.addObject("numbers", numbers);
		
		System.out.println("DB Conn" + mcs.readDBConn());
		System.out.println("DB User" + mcs.readDBUser());
		System.out.println("DB Pass" + mcs.readDBPass());
		
		mav.setViewName("/admin/event/events"); // jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/events/{curr}")
	public ModelAndView pagedEvents(@PathVariable(value="curr") Integer curr 
			) throws Exception {
		
		logger.debug("------------------------------Admin - List Paged Events ------------------------------");
		ModelAndView mav = new ModelAndView(); 
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		Set<Layer> layers = acct.getLayers();
		List<Integer> layersIds = new ArrayList<Integer>();
		
		for(Layer community: layers){
			layersIds.add(community.getId());
		}
		
		Set<Activity> events = activityService.getEventsByCommIds(layersIds);
		List<Activity> myEvents = new ArrayList<>();
		
		myEvents.addAll(activityService.getEventsByOwnerId(acct.getId()));
		myEvents.removeAll(events);
		
		
		Integer max=20;
		Integer initial=0;
		Integer prev=1;
		Integer next=2;
		Integer hmany = (myEvents.size()/max);
		
		hmany=hmany+1;
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		
		if(curr==1 || curr<1){
			initial=0;
			prev=1;
			next=2;
			List<Activity> list = new ArrayList<>(myEvents);
			if(myEvents.size()%max!=0 && myEvents.size()<=max){
				Set<Activity> subSet = new LinkedHashSet<>(list.subList(initial, myEvents.size()%max));
				mav.addObject("myActivitiesList", subSet);
			}else{
				Set<Activity> subSet = new LinkedHashSet<>(list.subList(initial, max));
				mav.addObject("myActivitiesList", subSet);
			}
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr>1 && curr < hmany){
			max=(curr)*20;
			initial= (curr-1)*20;
			prev=curr-1;
			next=curr+1;
			List<Activity> list = new ArrayList<>(myEvents);
			Set<Activity> subSet = new LinkedHashSet<>(list.subList(initial, max));
			mav.addObject("myActivitiesList", subSet);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
			mav.addObject("acct", getCurrentAccount());
			
		}
		else if(curr==hmany){
			
			initial= (curr-1)*20;
			max= initial + (myEvents.size()%20);
			prev=curr-1;
			next=curr+1;
			List<Activity> list = new ArrayList<>(myEvents);
			Set<Activity> subSet = new LinkedHashSet<>(list.subList(initial, max));
			mav.addObject("myActivitiesList", subSet);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		
		mav.addObject("hmany", hmany);
		mav.addObject("curr", curr);
		mav.addObject("numbers", numbers);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/event/events"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/events/{curr}/{commId}/{evValId}")
	public ModelAndView pagedEventsComm(
			@PathVariable(value="curr") Integer curr, @PathVariable(value="commId") Integer commId, @PathVariable(value="evValId") Integer evValId
			) throws Exception {
		
		logger.debug("------------------------------Admin - List Paged Events of Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		
		Date date = new Date();
		Layer community = layerService.findByIdWithActivityType("event", commId);
		
		Integer max=0;
		Integer initial=0;
		Integer prev=1;
		Integer next=2;
		Integer hmany = 0;
		Integer totalC = activityService.pagedActsPages("event", date.getTime(), commId);
		Integer totalP = activityService.pagedActsPagesPast("event", date.getTime(), commId);
		
		if(evValId == 110010011){
			if( (totalC/20)>0 && (totalC%20)==0){
				hmany = (totalC/20);
			}if( (totalC/20)>0 && (totalC%20)>0){
				hmany = (totalC/20)+1;
			}else{
				hmany = 1;
			}
		}
		if(evValId == 110000011){
			if( (totalP/20)>0 && (totalP%20)==0){
				hmany = (totalP/20);
			}if( (totalP/20)>0 && (totalP%20)>0){
				hmany = (totalP/20)+1;
			}else{
				hmany = 1;
			}
		}
			
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i<= hmany; i++){
			numbers.add(i);
		}
		System.out.println("Curr: "+ curr );
		if(curr==1){
			initial=0;
			prev=1;
			next=2;
			if(evValId == 110010011){
				if(totalC%20 != 0 && totalC >20){
					max = 20; }
				if(totalC%20 != 0 && totalC <20){ 
					max = totalC%20; }
				if(totalC%20 == 0 && totalC ==20){ 
					max = 20; }
				
				List<Activity> subSet= new ArrayList<>(activityService.pagedActs(initial, max , "event", date.getTime(), community)   );
				List<Activity> subSetwithDates = activityService.getStartandEndDatesfromEvents(subSet);
				
				mav.addObject("myActivitiesList", subSetwithDates);
				mav.addObject("acct", getCurrentAccount());
				
				
			}
			if(evValId == 110000011){
				if(totalP%20 != 0 && totalP >20){
					max = 20; }
				if(totalP%20 != 0 && totalP <20){ 
					max = totalP%20; }
				if(totalP%20 == 0 && totalP ==20){ 
					max = 20; }
				
				List<Activity> subSet= new ArrayList<>(activityService.pagedActsPast(initial, max , "event", date.getTime(), community)   );
				List<Activity> subSetwithDates = activityService.getStartandEndDatesfromEventsPast(subSet);
					
				mav.addObject("myActivitiesList", subSetwithDates);
				mav.addObject("acct", getCurrentAccount());
				
			}
			mav.addObject("prev", prev);
			mav.addObject("acct", getCurrentAccount());
			
			mav.addObject("next", next);
			
		}if(curr>1 && curr < hmany){
			max= curr * 20;
			initial= (curr-1)*20;
			prev=curr-1;
			next=curr+1;
			if(evValId == 110010011){
				List<Activity> subSet= new ArrayList<>(activityService.pagedActs(initial, max , "event", date.getTime(), community)   );
				List<Activity> subSetwithDates = activityService.getStartandEndDatesfromEvents(subSet);
				
				mav.addObject("myActivitiesList", subSetwithDates);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
			if(evValId == 110000011){
				List<Activity> subSet= new ArrayList<>(activityService.pagedActsPast(initial, max , "event", date.getTime(), community)   );
				List<Activity> subSetwithDates = activityService.getStartandEndDatesfromEventsPast(subSet);
					
				mav.addObject("myActivitiesList", subSetwithDates);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
		}if(curr.intValue()==hmany.intValue() && hmany > 1){
			initial= (curr*20)-20;	
			prev=curr-1;
			next=curr+1;
			if(evValId == 110010011){
				max= initial + (totalC%20);
				List<Activity> subSet= new ArrayList<>(activityService.pagedActs(initial, max , "event", date.getTime(), community)   );
				List<Activity> subSetwithDates = activityService.getStartandEndDatesfromEvents(subSet);
				
				mav.addObject("myActivitiesList", subSetwithDates);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
			if(evValId == 110000011){
				max= initial + (totalP%20);
				List<Activity> subSet= new ArrayList<>(activityService.pagedActsPast(initial, max , "event", date.getTime(), community)   );
				List<Activity> subSetwithDates = activityService.getStartandEndDatesfromEventsPast(subSet);
					
				mav.addObject("myActivitiesList", subSetwithDates);
				mav.addObject("prev", prev);
				mav.addObject("next", next);
			}
		}
		
		mav.addObject("hmany", hmany);
		mav.addObject("curr", curr);
		mav.addObject("numbers", numbers);
		mav.addObject("commId", commId);
		mav.addObject("community", community);
		mav.addObject("evValId", evValId);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/event/eventsComm"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/events", method= RequestMethod.GET)
	public ModelAndView eventsMgr() {
		
		logger.debug("------------------------------ Manager - List of Events ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);		
		
		//List<Banner> listBanners = bannerService.findAll();
		List<Activity> listActs = activityService.getByOwnerId(acct.getId());
		
		mav.addObject("eventActivities", listActs);
		mav.setViewName("/manager/event/events"); // jsp
		return mav; 
	}
	

	@RequestMapping(value="/admin/event/{id}/{commId}", method= RequestMethod.GET)
	public ModelAndView getEvent(@PathVariable(value="id") Integer id, @PathVariable(value="commId") Integer commId) throws ParseException {
		
		logger.debug("------------------------------Admin Get Single Event  ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity act = activityService.findByIdWithBanners(id);
		Activity actwithDates = new Activity();
		
		Set<Banner> bannList = new HashSet<Banner>();
		for (Banner ids: act.getBanners()){
			bannList.add(bannerService.get(ids.getId()));
		}
		
		double avLat = 0;
		double avLng = 0;
		
		double x = act.getEvent().getAddressLat().doubleValue();
		double y = act.getEvent().getAddressLng().doubleValue();
		
		if(x==0.000000||y==0.000000){
			avLat= -34.9288532;
			avLng= 138.6007248;
		}
		else{
			avLat = x;
			avLng = y;
		}
		
		Set<File> fileList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getUrl().endsWith(".pdf")){
				fileList.add(fileService.findById(ids.getId()));
			}
		}
		Set<File> videoList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getUrl().endsWith(".mp4")){
				videoList.add(fileService.findById(ids.getId()));
			}
		}
		Set<File> weblinksList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getType().equalsIgnoreCase("weblink")){
				weblinksList.add(fileService.findById(ids.getId()));
			}
		}
		
		mav.addObject("listOfFiles", fileList);
		mav.addObject("listOfVideos", videoList);
		mav.addObject("listOfWeblinks", weblinksList);
		mav.addObject("avLat",avLat);
		mav.addObject("avLng",avLng);
		mav.addObject("listOfBanners", bannList);
		mav.addObject("commId", commId);
		mav.addObject("acct", getCurrentAccount());
		
		mav.addObject("ev", activityService.getStartandEndDatesfromEvent(act));
		
		mav.setViewName("/admin/event/event"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/member/event/{id}", method= RequestMethod.GET)
	public ModelAndView getEventMember(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Member Get Single Event  ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity act = activityService.getStartandEndDatesfromEvent(activityService.findById(id)); 
		Activity act2 = activityService.findByIdWithBanners(id); 

		Set<Banner> bannList = new HashSet<Banner>();
		for (Banner ids: act2.getBanners()){
			bannList.add(bannerService.get(ids.getId()));
		}
		Set<File> fileList = new HashSet<File>();
		for (File ids: act2.getFiles()){
			if(ids.getUrl().endsWith(".pdf")){
				fileList.add(fileService.findById(ids.getId()));
			}
		}
		Set<File> videoList = new HashSet<File>();
		for (File ids: act2.getFiles()){
			if(ids.getUrl().endsWith(".mp4")){
				videoList.add(fileService.findById(ids.getId()));
			}
		}
		Set<File> weblinksList = new HashSet<File>();
		for (File ids: act2.getFiles()){
			if(ids.getType().equalsIgnoreCase("weblink")){
				weblinksList.add(fileService.findById(ids.getId()));
			}
		}
		
		double avLat = 0;
		double avLng = 0;
		
		double x = act.getEvent().getAddressLat().doubleValue();
		double y = act.getEvent().getAddressLng().doubleValue();
		
		if(x==0.000000||y==0.000000){
			avLat= -34.9288532;
			avLng= 138.6007248;
		}
		else{
			avLat = x;
			avLng = y;
		}
		mav.addObject("avLat",avLat);
		mav.addObject("avLng",avLng);
		mav.addObject("listOfBanners", act.getBanners());
		mav.addObject("listOfFiles", fileList);
		mav.addObject("listOfVideos", videoList);
		mav.addObject("listOfWeblinks", weblinksList);
		mav.addObject("ev", act);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/member/event/event"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/banners/event/{id}", method= RequestMethod.GET)
	public ModelAndView getBannersByEvent(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get Banners per Event :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/admin/banner/bannersTo"); // jsp
			
		Activity comm = activityService.findById(id);
		if (comm != null)
		{
			mav.addObject("myList", comm.getBanners());
			mav.addObject("myCommId", id);
			mav.addObject("myComm", comm);
			mav.addObject("acct", getCurrentAccount());
			
		}else{	
			mav.addObject("acct", getCurrentAccount());
			mav.addObject("myList", null);
		}
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/event/addBanners/{id}", method= RequestMethod.GET)
	public ModelAndView addBannersToEvent(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Add Banners to a Event ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		Activity act =  activityService.findByIdWithRelations(id);
		
		mav.addObject("act", act);	
		mav.addObject("listOfEvents", bannerService.findAll());	
		mav.addObject("listOfImages", bannerService.listImages()); 
		mav.addObject("iUrl", 0);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/event/addBannersToEvents"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/event/saveAbanners/", method= RequestMethod.POST)
	public ModelAndView saveABanners(@ModelAttribute("act") Activity act,  
			@RequestParam("xevents") String [] xevents ) throws Exception {
		
		logger.debug("---------------------------- Admin - Save Banners Added to Event ------");
		
		System.out.println("comm : "+act);
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity actX = activityService.findById(act.getId());
		System.out.println("actX.getId() : "+actX.getId());
		Set<Banner> bannList = new HashSet<Banner>();
		for (String ids: xevents){
			bannList.add(bannerService.get(new Integer(ids)));
		}
		actX.setBanners(bannList);
		
		mav.addObject("message", activityService.save(actX));
		mav = events();  //Calls the events list function for the Admin Role
			
		return mav;	
	}
	
	
	@RequestMapping(value="/manager/event/{id}", method= RequestMethod.GET)
	public ModelAndView getEventMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager Get Single Event  ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		
		Activity ev = activityService.findById(id);
		
		mav.addObject("ev", ev);
		
		mav.setViewName("/manager/event/event"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/banners/event/{id}", method= RequestMethod.GET)
	public ModelAndView getBannersByEventMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Get Banners per Event :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/manager/banner/bannersTo"); // jsp
			
		Activity comm = activityService.findById(id);
		if (comm != null)
		{
			mav.addObject("myList", comm.getBanners());
			mav.addObject("myCommId", id);
			mav.addObject("myComm", comm);
		}else{	
			mav.addObject("myList", null);
		}
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/event/addBanners/{id}", method= RequestMethod.GET)
	public ModelAndView addBannersToEventMgr(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Manager - Add Banners to a Event ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		Activity act =  activityService.get(id);
		
		mav.addObject("act", act);	
		mav.addObject("listOfEvents", bannerService.findAll());	
		mav.addObject("listOfImages", bannerService.listImages()); 
		mav.addObject("iUrl", 0);
		
		mav.setViewName("/manager/event/addBannersToEvents"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/event/saveAbanners/", method= RequestMethod.POST)
	public ModelAndView saveABannersMgr(@ModelAttribute("act") Activity act,  
			@RequestParam("xevents") String [] xevents ) {
		
		logger.debug("---------------------------- Manager - Save Banners Added to Event ------");
		
		System.out.println("comm : "+act);
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity actX = activityService.findById(act.getId());
		System.out.println("actX.getId() : "+actX.getId());
		Set<Banner> bannList = new HashSet<Banner>();
		for (String ids: xevents){
			bannList.add(bannerService.get(new Integer(ids)));
		}
		actX.setBanners(bannList);
		mav.addObject("message", activityService.save(actX));
		mav = eventsMgr();   //Calls the events list function for the Community Manager Role
			
		return mav;	
	}
	
	
	@RequestMapping(value="/admin/event/save", method= RequestMethod.POST)
	public String save(@ModelAttribute("newEvent") @Valid Event newEvent, BindingResult result, SessionStatus status,  
			@RequestParam("commodities") String[] commodities,		  @RequestParam("sDate")String[]  sDate,					
			@RequestParam("eDate") String[] eDate,			  		  @RequestParam("sTime")String[]  sTime, 			  		
			@RequestParam("eTime") String[] eTime,					  
			@RequestParam(value="days" , 	 		 defaultValue="", 		required=false) String[]  days,				  		
			@RequestParam(value="titleBanner[]", 	 defaultValue="", 		required=false) String[]  titleBanner,	
			@RequestParam(value="subtitleBanner[]",  defaultValue="", 		required=false) String[]  subtitleBanner,	
			@RequestParam(value="bannerUrl[]", 		 defaultValue="", 		required=false) String[]  urlBanner, 		
			@RequestParam(value="bannerRemove[]", 	 defaultValue="", 		required=false) String[]  bannerRemove, 
			@RequestParam(value="bannerId[]", 		 defaultValue="", 		required=false) String[]  bannerId,	
			@RequestParam(value="titleFile[]", 		 defaultValue="", 		required=false) String[]  titleFile,		
			@RequestParam(value="descriptionFile[]", defaultValue="", 		required=false) String[]  descriptionFile,	
			@RequestParam(value="fileUrl[]", 		 defaultValue="", 		required=false) String[]  urlFile, 			
			@RequestParam(value="fileId[]", 		 defaultValue="", 		required=false) Integer[] fileId,
			@RequestParam(value="titleVideo[]", 	 defaultValue="", 		required=false) String[]  titleVideo,	
			@RequestParam(value="descriptionVideo[]",defaultValue="", 		required=false) String[]  descriptionVideo,	
			@RequestParam(value="videoUrl[]", 		 defaultValue="", 		required=false) String[]  urlVideo, 		
			@RequestParam(value="videoId[]", 		 defaultValue="", 		required=false) Integer[] videoId,			
			@RequestParam(value="videoIconUrl[]",	 defaultValue="", 		required=false) String[]  videoIconUrl,	
			@RequestParam(value="periodicity",		 defaultValue="DAILY", 	required=false) String    periodicity,
			@RequestParam(value="dateSelected",		 defaultValue="", 		required=false) String    dateSelected,
			@RequestParam(value="dateType",		 	 defaultValue="", 		required=false) String[]  dateType,
			@RequestParam(value="eciId", 			 defaultValue="",		required=false) Integer[] eciId,
			@RequestParam(value="eciHref",  		 defaultValue="",		required=false) String[]  eciHref,
			@RequestParam(value="titleWL[]", 	 	 defaultValue="", 		required=false) String[]  titleWL,	
			@RequestParam(value="descriptionWL[]",	 defaultValue="", 		required=false) String[]  descriptionWL,	
			@RequestParam(value="urlWL[]", 		 	 defaultValue="", 		required=false) String[]  urlWL, 		
			@RequestParam(value="idWL[]", 		 	 defaultValue="", 		required=false) String[]  idWL,
			@RequestParam(value="commId" , 	 		 defaultValue="", 		required=false) Integer   commId,
			@RequestParam(value="another",			 defaultValue="false",  required=false) boolean   another
			)
		throws SQLException, Exception, MalformedURLException{
		
		logger.debug("------------------------------ Admin - Save Event ------------------------------");
		
		
		//newEvent.setBookable(false);
		//newEvent.getActivity().setRecommended(false);
		
		ModelAndView mav = setBasicData();
		Account acct = getCurrentAccount();
		
//		try{
			
			boolean isNew = true;
			if (newEvent.getActivityId() != null )
					isNew = false;
	
			if (result.hasErrors()) {
				//ModelAndView mav = new ModelAndView(); 
				//mav.addObject("listOfIcons", layerService.listIcons());
				mav.setViewName("/admin/event/newEvent"); //jsp
				return "redirect:/admin/event/newEvent";
			}
			else{
				String connectionURL = getURLDB();
				java.sql.Connection connection = null; 
		        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
	            connection = DriverManager.getConnection(connectionURL, getUSER(), getPASSWORD());
	            
		        
		        if(!connection.isClosed()){
		        	Statement stmt = connection.createStatement(); 		        
			        
			        Event saveEvent = newEvent;
			        
			        System.out.println("sDate length: "+sDate.length);
			        System.out.println("sTime length: "+sTime.length);
			        System.out.println("eDate length: "+eDate.length);
			        System.out.println("eTime length: "+eTime.length);
			        System.out.println("periodicity : "+periodicity);
			        System.out.println("dateType    : "+dateType.length);
			        
			        List<String> sDateNew = new ArrayList<String>();
			        List<String> eDateNew = new ArrayList<String>();
			        List<String> sTimeNew = new ArrayList<String>();
			        List<String> eTimeNew = new ArrayList<String>();
			        List<Integer> durationNew = new ArrayList<Integer>();
			        System.out.println("--- ---Displaying Received Data --- ---");
			        for(int n=0;n<sDate.length;n++){
				        if(sTime[n]!="" && eTime[n]!="" && eDate[n]!="" && sDate[n]!=""){
				        	
				        	System.out.println("Received sDate["+n+"]   : "+sDate[n]);
			        		System.out.println("Received eDate["+n+"]   : "+eDate[n]);
			        		System.out.println("Received sTime["+n+"]   : "+sTime[n]);
			        		System.out.println("Received eTime["+n+"]   : "+eTime[n]);
			        		System.out.println("Received duration["+n+"]: "+ activityService.calculateDurationExtended(sDate[n],eDate[n],sTime[n],eTime[n]));
			        		System.out.println("Received dateType["+n+"]: "+dateType[n]);
			        		System.out.println("Received dateSelected   : "+dateSelected);
			        		System.out.println("--- --- --- ---");
				        		if(!eDate[n].equals("") && !sDate[n].equals("") && !eTime[n].equals("") && dateType[n].equalsIgnoreCase(dateSelected)){
					        		sDateNew.add(sDate[n]);
					        		eDateNew.add(eDate[n]);
					        		sTimeNew.add(sTime[n]);
					        		eTimeNew.add(eTime[n]);
					        		durationNew.add(activityService.calculateDurationExtended(sDate[n],eDate[n],sTime[n],eTime[n]) );
					        		System.out.println("---The event has SD and ED---");
					        		System.out.println("sDate["+n+"]: "+sDate[n]);
					        		System.out.println("eDate["+n+"]: "+eDate[n]);
					        		System.out.println("sTime["+n+"]: "+sTime[n]);
					        		System.out.println("eTime["+n+"]: "+eTime[n]);
					        		System.out.println("duration["+n+"]: "+activityService.calculateDurationExtended(sDate[n],eDate[n],sTime[n],eTime[n]));
					        	}
					        	if(eDate[n].equals("") && sDate[n].equals("")){
					        		System.out.println("--Event does not have startDate / endDate---");
					        		System.out.println("sDate["+n+"]: "+sDate[n]);
						        	System.out.println("sTime["+n+"]: "+sTime[n]);
						        	System.out.println("eDate["+n+"]: "+eDate[n]);
						        	System.out.println("eTime["+n+"]: "+eTime[n]);
						        	System.out.println("duration["+n+"]: "+activityService.calculateDurationExtended(sDate[n],eDate[n],sTime[n],eTime[n]));
					        	}
				        }
			        }
			        
					Activity saveActivity = newEvent.getActivity();
					System.out.println("ControllerSave newEvent.getActivity().getLayers() " + newEvent.getActivity().getLayers().size());
			    	
					saveActivity.setIconUrl(newEvent.getActivity().getIconUrl());
					
					Date currentDT= new Date();
			        System.out.println("Current Time Stamp: "+new Timestamp(currentDT.getTime()));
					
					saveActivity.setCreatedDatetime(currentDT);
					
					
					if(urlBanner.length!=0){
						Set<Banner> banners = new HashSet<Banner>();
						System.out.println("Banners <<< B >>>");
						System.out.println("Banners detected #: "+ urlBanner.length);
						System.out.println("Titles  detected #: "+ titleBanner.length);
						System.out.println("STitles detected #: "+ subtitleBanner.length);
						System.out.println("Ids     detected #: "+ bannerId.length);
						
						for(int ba=0; ba<urlBanner.length; ba++){
							Banner newBanner = new Banner();
							if(bannerId.length > ba && !bannerId[ba].equals("")){
								if(titleBanner.length==0 && subtitleBanner.length==0){
									newBanner=bannerService.findById(new Integer(bannerId[ba]));
									newBanner.setTitle("");
									newBanner.setSubtitle("");
								}else if(titleBanner.length>0 && subtitleBanner.length==0){
									newBanner=bannerService.findById(new Integer(bannerId[ba]));
									newBanner.setTitle(titleBanner[ba]);
									newBanner.setSubtitle("");
								}else if(titleBanner.length==0 && subtitleBanner.length>0){
									newBanner=bannerService.findById(new Integer(bannerId[ba]));
									newBanner.setTitle("");
									newBanner.setSubtitle(subtitleBanner[ba]);				  
								}else{
									newBanner=bannerService.findById(new Integer(bannerId[ba]));
									newBanner.setTitle(titleBanner[ba]);			
									newBanner.setSubtitle(subtitleBanner[ba]);		  
								}
							}
							else{
								if(titleBanner.length>0){
									newBanner.setTitle(titleBanner[ba]);
									newBanner.setSubtitle(subtitleBanner[ba]);	
									newBanner.setImageUrl(urlBanner[ba]);
								}else{
									newBanner.setTitle(" ");
									newBanner.setSubtitle(" ");	
									newBanner.setImageUrl(urlBanner[ba]);
								}
							}
							bannerService.save(newBanner);
							banners.add(newBanner);
						}
						saveActivity.setBanners(banners);
					}
					if(urlBanner.length==0){
						Set<Banner> banners = new HashSet<Banner>();
						banners.add(bannerService.findById(3));
						saveActivity.setBanners(banners);
					}
					
					Set<File> allFiles = new HashSet<File>();
					
					if(urlFile.length!=0){
						Set<File> files = new HashSet<File>();
						System.out.println("Files <<< F >>>");
						System.out.println("#         : "+ urlFile.length);
						System.out.println("#Titles   : "+ titleFile.length);
						System.out.println("#SubTitles: "+ descriptionFile.length);
						System.out.println("#Ids      : "+ fileId.length);
						
						try{
							for(int fi=0; fi<urlFile.length; fi++){
								File newFile = new File();
								
								if(urlFile[fi]!=null && titleFile[fi]!=null && descriptionFile[fi]!=null){
									newFile.setTitle(titleFile[fi]);
									newFile.setDescription(descriptionFile[fi]);	
									newFile.setUrl(urlFile[fi]);					
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultPDF.jpg");
									newFile.setType("document");
								}
								if(urlFile[fi]!=null && titleFile[fi]!=null && descriptionFile[fi]!=null && fileId.length>0){
									newFile=fileService.findById(fileId[fi]);		
									newFile.setTitle(titleFile[fi]);				
									newFile.setDescription(descriptionFile[fi]);
									newFile.setType("document");
								}
								fileService.save(newFile);
								files.add(newFile);
							}
						}catch (Exception e) {
							System.out.println("ArrayIndexOutOfBoundsException : "+e);
							//mav = errorsController.error502();
							return "redirect:/admin/errors/Error-502";
						}
						allFiles.addAll(files);
					}
					
					if(urlVideo.length!=0){						
						Set<File> files = new HashSet<File>();
						System.out.println("Videos <<< V >>>>");
						System.out.println("#         : "+ urlVideo.length);
						System.out.println("#Titles   : "+ titleVideo.length);
						System.out.println("#SubTitles: "+ descriptionVideo.length);
						System.out.println("#Ids      : "+ videoId.length);
						
						try{
							for(int num=0; num<urlVideo.length; num++){
								File newFile = new File();
								
								if(urlVideo[num]!=null && titleVideo[num]!=null && descriptionVideo[num]!=null && videoId.length<1){     
									newFile.setTitle(titleVideo[num]);
									newFile.setDescription(descriptionVideo[num]);
									newFile.setUrl(urlVideo[num]);
									//newFile.setIconUrl(videoIconUrl[num]);
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultVID.jpg");
									newFile.setType("video");
								}
								if(urlVideo[num]!=null && titleVideo[num]!=null && descriptionVideo [num]!=null && videoId.length>0){
									newFile.setUrl(fileService.findById(videoId[num]).getUrl());
									newFile.setIconUrl(fileService.findById(videoId[num]).getIconUrl());
									newFile.setTitle(titleVideo[num]);
									newFile.setDescription(descriptionVideo[num]);
									newFile.setType("video");
								}
								fileService.save(newFile);
								files.add(newFile);
							}
						}catch (Exception e) {
							System.out.println("ArrayIndexOutOfBoundsException : "+e);
							mav = errorsController.error503();
							return "redirect:/admin/errors/error-503";
						}
						allFiles.addAll(files);
					}
					
					System.out.println("<<<<<< <<< WL >>>> >>>>>>>>");
					System.out.println("#urlWL    : "+ urlWL.length);
					System.out.println("#Titles   : "+ titleWL.length);
					System.out.println("#SubTitles: "+ descriptionWL.length);
					System.out.println("#Ids      : "+ idWL.length);
					
					if(urlWL.length!=0){						
						Set<File> filesW = new HashSet<File>();
						for(int num=0; num<urlWL.length; num++){
							if(!urlWL[num].equals("")){
								File newFile = new File();
								if(titleWL.length==0 && descriptionWL.length==0 && idWL.length==0){ System.out.println("<< OPT1 >>");
									newFile.setTitle(" ");
									newFile.setDescription(" ");
									newFile.setUrl(urlWL[num]);
									newFile.setType("weblink");
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
								}if(titleWL.length==0 && descriptionWL.length==0 && idWL.length!=0){ System.out.println("<< OPT2 >>");
									newFile=fileService.findById(new Integer(idWL[num]));
									newFile.setTitle(" ");
									newFile.setDescription(" ");
									newFile.setUrl(urlWL[num]);
									newFile.setType("weblink");
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
								}if(titleWL.length!=0 && descriptionWL.length==0 && idWL.length==0){ System.out.println("<< OPT3 >>");
									if(titleWL[num].equals("")) newFile.setTitle(titleWL[num]);
									else newFile.setTitle(titleWL[num]);
									newFile.setDescription(" ");
									newFile.setUrl(urlWL[num]);
									newFile.setType("weblink");
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
								}if(titleWL.length!=0 && descriptionWL.length==0 && idWL.length!=0){ System.out.println("<< OPT4 >>");
									newFile=fileService.findById(new Integer(idWL[num]));
									if(titleWL[num].equals("")) newFile.setTitle(titleWL[num]);
									else newFile.setTitle(titleWL[num]);
									newFile.setDescription(" ");
									newFile.setUrl(urlWL[num]);
									newFile.setType("weblink");
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
								}if(titleWL.length==0 && descriptionWL.length!=0 && idWL.length==0){ System.out.println("<< OPT5 >>");
									newFile.setTitle(" ");
									if(descriptionWL[num].equals("")) newFile.setDescription(" ");
									else newFile.setDescription(descriptionWL[num]);
									newFile.setUrl(urlWL[num]);
									newFile.setType("weblink");
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
								}if(titleWL.length==0 && descriptionWL.length!=0 && idWL.length!=0){ System.out.println("<< OPT6 >>");
									newFile=fileService.findById(new Integer(idWL[num]));
									newFile.setTitle(" ");
									if(descriptionWL[num].equals("")) newFile.setDescription(" ");
									else newFile.setDescription(descriptionWL[num]);
									newFile.setUrl(urlWL[num]);
									newFile.setType("weblink");
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
								}if(titleWL.length!=0 && descriptionWL.length!=0 && idWL.length==0){ System.out.println("<< OPT7 >>");
									if(titleWL[num].equals("")) newFile.setTitle(titleWL[num]);
									else newFile.setTitle(titleWL[num]);
									if(descriptionWL[num].equals("")) newFile.setDescription(" ");
									else newFile.setDescription(descriptionWL[num]);
									newFile.setUrl(urlWL[num]);
									newFile.setType("weblink");
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
								}if(titleWL.length!=0 && descriptionWL.length!=0 && idWL.length!=0){ System.out.println("<< OPT8 >>");
									System.out.println("idWL: "+idWL[num].toString());
									System.out.println("titleWL: "+titleWL[num].toString());
									System.out.println("descriptionWL: "+descriptionWL[num].toString());
									System.out.println("urlWL: "+urlWL[num].toString());
									
									if(!idWL[num].equals("")){ newFile=fileService.findById(new Integer(idWL[num]));}
									if(!titleWL[num].equals("")){ newFile.setTitle(titleWL[num]); }
									else {newFile.setTitle(" ");}
									if(!descriptionWL[num].equals("")) { newFile.setDescription(descriptionWL[num]);}
									else {newFile.setDescription(" ");}
									if(!urlWL[num].equals("")) { newFile.setUrl(urlWL[num]);}
									newFile.setType("weblink");
									newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
									fileService.save(newFile);
								}
								fileService.save(newFile);
								filesW.add(newFile);
							}
						}
						allFiles.addAll(filesW);
					}
					saveActivity.setFiles(allFiles);
					
					if(newEvent.getContactPhone().equalsIgnoreCase("")){
						saveEvent.setContactPhone("");
					}
					
					if(newEvent.getAddress().equalsIgnoreCase("")){
						saveEvent.setAddress("");
					}
					
					final Geocoder geocoder = new Geocoder();
					String address = newEvent.getAddress();
					if( !address.endsWith("Australia") ){
						address = newEvent.getAddress()+" Australia";
						saveEvent.setAddress(address);
					} 
					GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(address).setLanguage("en").getGeocoderRequest();
					GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
					List<GeocoderResult> results = geocoderResponse.getResults();
					saveEvent.setAddressLat(new BigDecimal(results.get(0).getGeometry().getLocation().getLat().floatValue()));
					saveEvent.setAddressLng(new BigDecimal(results.get(0).getGeometry().getLocation().getLng().floatValue()));
					
					if(newEvent.getContactName().equalsIgnoreCase("")){
						saveEvent.setContactName("");
					}
					if(saveActivity.getContentTitle().equalsIgnoreCase("")){
						saveActivity.setContentTitle("Event by "+acct.getName());
					}
					String s2 = StringEscapeUtils.escapeXml(saveActivity.getContentTitle()); 
					saveActivity.setContentTitle(s2);
					String s3 = StringEscapeUtils.escapeHtml(saveActivity.getContentBody()); 
					saveActivity.setContentBody(s3);
					
					if(newEvent.getContactEmail().equalsIgnoreCase("")){
						newEvent.setContactEmail("");
					}
					if(newEvent.getOrganisation().equalsIgnoreCase("")){
						newEvent.setOrganisation("CC Me");
					}
					if(newEvent.getCost().equalsIgnoreCase("")){
						newEvent.setCost("Free");
					}
					
					Set<Layer> commoditySet = new HashSet<Layer>();
					if (commodities != null) {
						for (String idCommodity:commodities){
							int count = 0; 
							try {
								for (Layer validate : commoditySet) {
									Integer id = validate.getId();
									if (id == Integer.parseInt(idCommodity)) {
										count++;
									}
								}
								if (count == 0) // no match
								{
									Layer tempLayer = layerService.findById(Integer
											.parseInt(idCommodity));
									if (tempLayer != null) {
	
										commoditySet.add(tempLayer);
									}
								}
								}catch(Exception e){
									logger.debug("Number Exception - adding commodity to event");
								}
						}
					}
					
					saveActivity.setLayers(commoditySet);
					saveEvent.setActivity(saveActivity);
					mav.addObject("messageAct", activityService.save(saveActivity));
					mav.addObject("messageEv", eventService.save(saveEvent));
					
					//This function converts spaces into ASCII %20
					//saveActivity.setIconUrl( URLEncoder.encode(newEvent.getActivity().getIconUrl(), "UTF-8") );	
					
					try{
						String decodedURL = URLDecoder.decode(newEvent.getActivity().getIconUrl(), "UTF-8");
				        URL url = new URL(decodedURL);
				        URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef()); 
				        saveActivity.setIconUrl( uri.toURL().toString() );
					}catch (MalformedURLException e ) {
						System.out.println("MalformedURLException : "+e);
						//mav = errorsController.error501();
						return "redirect:/admin/errors/error-501";
					}
					
					try{
						if(!isNew){
							Activity oldAct = activityService.findByIdWithRelations(newEvent.getActivityId());
							saveActivity.setLayers(oldAct.getLayers());
							saveActivity.setLayers_1(oldAct.getLayers_1());
							saveActivity.setAccounts(oldAct.getAccounts());
							saveActivity.setActivitiesForActivityId(oldAct.getActivitiesForActivityId());
							saveActivity.setActivityCalendars(oldAct.getActivityCalendars());
							saveActivity.setBookings(oldAct.getBookings());
							saveActivity.setMessages(oldAct.getMessages());
							saveActivity.setRecommendedActivitiesesForActivityId(oldAct.getRecommendedActivitiesesForActivityId());
							saveActivity.setRecommendedActivitiesesForRecommendedActivityId(oldAct.getRecommendedActivitiesesForRecommendedActivityId());
							
							logger.debug("------------------------------ ECIs ------------------------------");
							Set<EventCalendarItem> oldEcis = oldAct.getEvent().getEventCalendarItems();
							for(EventCalendarItem eci:oldEcis){
								logger.debug("ECI to be deleted: "+eci.getId().getActivityId() +" "+eci.getId().getCalHref());	
								String deleteECIs ="DELETE FROM `"+getDB()+"`.`event_calendar_item` WHERE `activity_id`="+eci.getId().getActivityId()+" and `cal_href`='"+
										eci.getId().getCalHref()+"';";
								System.out.println("UPDATE ECI>>>"+deleteECIs);
								stmt.executeUpdate(deleteECIs);
								
								eventCalendarItemService.deleteToBaikal(eci.getId().getCalHref(), eci.getCalEtag());	
							}
							
							List<EventCalendarItem>ecis=eventCalendarItemService.createEventDates(newEvent.getActivityId(),sDateNew,sTimeNew,eDateNew,eTimeNew,days,durationNew,periodicity,newEvent);						
							if(ecis.size()>0){
								for(int y=0; y<ecis.size();y++){
									String insertRange ="INSERT INTO `"+getDB()+"`.`event_calendar_item` (`activity_id`, `cal_href`, " +
										"`cal_etag`, `cal_data`, `dtend`) "+"VALUES ("+ecis.get(y).getId().getActivityId()+", '"+
										ecis.get(y).getId().getCalHref()+"', '"+ecis.get(y).getCalEtag()+"', '"+ecis.get(y).getCalData()+"', '"+
										ecis.get(y).getDtend()+"')";
									stmt.executeUpdate(insertRange);
									
									eventCalendarItemService.toBaikal(ecis.get(y).getCalEtag(), (sDateNew.get(y)+" "+sTimeNew.get(y)+":00"), (eDateNew.get(y)+
											" "+eTimeNew.get(y)+":00"),	ecis.get(y).getCalData(), ecis.get(y).getId().getCalHref());
									
									System.out.println("INSERT ECI>>>"+insertRange);
									System.out.println("ECIs SIZE>>>"+ecis.size());
								}
							}
							mav.addObject("messageAct", activityService.save(saveActivity));
							mav.addObject("messageEv", eventService.save(saveEvent));
						}else{
							
							List<EventCalendarItem> ecis = eventCalendarItemService.createEventDates(newEvent.getActivityId(), sDateNew, sTimeNew, 
									eDateNew, eTimeNew, days, durationNew, periodicity, newEvent);
							System.out.println("ECIs SIZE>>>"+ecis.size());
							
							for(int z=0; z<ecis.size();z++){
								eventCalendarItemService.toBaikal(ecis.get(z).getCalEtag(), (sDateNew.get(z)+" "+sTimeNew.get(z)+":00"), (eDateNew.get(z)+
										" "+eTimeNew.get(z)+":00"),	ecis.get(z).getCalData(), ecis.get(z).getId().getCalHref());
							}
							for(int y=0; y<ecis.size();y++){
								//long epoch = (System.currentTimeMillis()+34200/1000)+y;
								String insertRange ="INSERT INTO `"+getDB()+"`.`event_calendar_item` (`activity_id`, `cal_href`, " +
									"`cal_etag`, `cal_data`, `dtend`) "+"VALUES ("+ecis.get(y).getId().getActivityId()+", '"+
									ecis.get(y).getId().getCalHref()+"', '"+ecis.get(y).getCalEtag()+"', '"+ecis.get(y).getCalData()+"', '"+
									ecis.get(y).getDtend()+"')";
								stmt.executeUpdate(insertRange);
								System.out.println("INSERT ECI>>>"+insertRange);
							}
							
						}
					}
					catch (SQLException e ) {
						System.out.println("SQL Exception : "+e);
						mav = errorsController.error500();
						return "redirect:/admin/errors/error-500";
					}
					finally{
			            if(stmt != null)
			            stmt.close();
			        }
				}
		        
				//mav = commController.communityDashboardById(commId);
				//mav.setViewName("/admin/event/CommunityDashboard/"+commId+"/"+commController.getCommunity(commId).getViewName());
			}
			
			Layer comm = layerService.findById(commId);
			String ret="";
			if(another==false){
				ret="redirect:/admin/events/community/"+commId+"/"+comm.getName();
			}
			if(another==true){
				ret="redirect:/admin/event/new/"+commId+"";
			}
			
			return ret;
//		}catch (Exception e) {
//			System.out.println("Exception : "+e);
//			mav = errorsController.error500();
//			return mav;
//		}
	}
	
	
//	@RequestMapping(value="/manager/event/save", method= RequestMethod.POST)
//	public ModelAndView saveMgr(@ModelAttribute("newEvent") @Valid Event newEvent, BindingResult result, SessionStatus status, 
//			@RequestParam("days")String[] days, 		@RequestParam("icon")Integer icon, 				@RequestParam("duration")Integer duration,
//			@RequestParam("sDateTime")String sDateTime,	@RequestParam("eDateTime")String eDateTime,		@RequestParam("periodicity")String periodicity
//			) throws SQLException, Exception
//			{
//		
//		logger.debug("------------------------------ Manager - Save Event ------------------------------");
//
//		if (result.hasErrors()) {
//			//if validator failed
//			ModelAndView mav = new ModelAndView(); 
//			//mav.addObject("listOfIcons", layerService.listIcons());
//			mav.setViewName("/manager/event/newEvent"); //jsp
//			return mav;
//			
//		}else{
//			String connectionURL = "jdbc:mysql://winterfell.cbi0v9fte9kc.ap-southeast-2.rds.amazonaws.com:3306/ccme_winter_2_dev";
//	        Connection connection = null; 
//	        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
//	        connection = DriverManager.getConnection(connectionURL, "carlos_dev", "carlos_dev");
//	        if(!connection.isClosed()){
//	        	Statement stmt = connection.createStatement();
//	        	if(days==null)
//	        		days[0]="";
//	        	String dayss = "";
//				for(int i=0; i<days.length;i++){
//					dayss= dayss+days[i].toString();
//					if(i+1!=days.length){
//						dayss= dayss+",";
//					}
//				}
//				
//				//System.out.println("Controller - Save community, sDateTime" + sDateTime);
//				//System.out.println("Controller - Save community, eDateTime" + eDateTime);
//				
//				SimpleDateFormat sdf0 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); // the format of your date
//				 //sdfa.setTimeZone(java.util.TimeZone.getTimeZone("UTC-5"));
//				Date formattedDate0s = sdf0.parse(sDateTime); //System.out.println("Controller - Save community, formattedDate0s" + formattedDate0s);
//				Date formattedDate0e = sdf0.parse(eDateTime); //System.out.println("Controller - Save community, formattedDate0e" + formattedDate0e);
//				
//	        	SimpleDateFormat sdfa = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss"); // the format of your date
//	        					 //sdfa.setTimeZone(java.util.TimeZone.getTimeZone("UTC-5"));
//		        String formattedDatea = sdfa.format(formattedDate0s); //System.out.println("Controller - Save community, formattedDatea" + formattedDatea);
//		        String formattedDatee = sdfa.format(formattedDate0e); //System.out.println("Controller - Save community, formattedDatee" + formattedDatee);
//		        
//		        SimpleDateFormat sdfb = new SimpleDateFormat("yyyy/MM/dd HH:mm");
//				 				 //sdfb.setTimeZone(java.util.TimeZone.getTimeZone("UTC-5"));
//		        String startDatea = sdfb.format(formattedDate0s); //System.out.println("Controller - Save community, formattedDatea" + formattedDatea);
//		        String startDatee = sdfb.format(formattedDate0e); //System.out.println("Controller - Save community, formattedDatea" + formattedDatea);		        
//	        	
//		    	newEvent.setDisplayDatetime("Weekly, From "+startDatea+"hr To "+startDatee+ "hr, Every: "+dayss);
//		    	
//				Event saveEvent = newEvent;
//				Activity saveActivity = newEvent.getActivity();
//		    	
//				//List<IconDTO> icons = layerService.listIcons();
//				//Integer iconx = icon;
//				//IconDTO x = icons.get(iconx);
//				//saveActivity.setIconUrl(x.getIconUrl().toString());
//				
//				Date currentDT= new Date();
//				saveActivity.setCreatedDatetime(currentDT);
//				
//				ModelAndView mav = new ModelAndView(); 
//				mav.addObject("message0", eventService.save(saveEvent));
//				mav.addObject("message2", activityService.save(saveActivity));
//				
//				EventCalendarItem eci = eventCalendarItemService.createEventDates(newEvent.getActivityId(), formattedDatea, formattedDatee, days, duration, periodicity);
//				
//				if (eci.equals(null)){
//					mav.setViewName("/manager/event/newEvent"); //jsp
//					System.out.println("NULL from ECI");
//					return mav;
//					
//				}else{
//					try{					
//						String insertRange = "INSERT INTO `ccme_winter_2_dev`.`event_calendar_item` (`activity_id`, `cal_href`, `cal_etag`, `cal_data`) " +
//								"VALUES ("+eci.getId().getActivityId()+", '"+eci.getId().getCalHref()+"', '"+eci.getCalEtag()+"', '"+eci.getCalData()+"')";
//						stmt.executeUpdate(insertRange);
//					}
//					catch (SQLException e ) {
//						System.out.println("SQL Exception : "+e);
//					}
//					finally{
//			            if(stmt != null)
//			            stmt.close();
//			        }		
//					mav = eventsMgr(); 
//					return mav;
//				}
//			}
//		}
//		return null;
//	}
	
	
	@RequestMapping(value="/admin/event/delete/{id}")
	public String delete(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Delete Single Event ------------------------------");

		ModelAndView mav =  setBasicData();
		
		try{
			Activity event = activityService.get(id); 
			event.setDeleted(true);
			
			mav.addObject("message", activityService.save(event));
			logger.debug("----------New Event list is s -------------");
		
			return "redirect:/admin/communities/";
			
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			return "redirect:/admin/errors/500/";
		}
	}
	
	
	@RequestMapping(value="/manager/event/delete/{id}")
	public String deleteMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Delete Single Event ------------------------------");

		Activity event = activityService.get(id); 
		event.setDeleted(true);
		activityService.save(event);
		
		ModelAndView mav =  eventsMgr();
		mav.addObject("message", activityService.save(event));
		
		return "redirect:/manager/events/";
	}
	
	
	@RequestMapping(value="/admin/event/new/{commId}")
	public ModelAndView newEvent(@PathVariable(value="commId") Integer commId) throws Exception {
		
		logger.debug("------------------------------ Admin - New Event ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		Activity newAct = new Activity();
		Event newEv = new Event(newAct);
		newEv.setActivity(newAct);
		newAct.setEvent(newEv);
		
		System.out.println("From Controller newEvent - newEv.getActivityId : " + newEv.getActivity().getId());
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		// Todo validate current session other will return null and the screen will crash		
		Account acct = accountService.getbyIdWithLayersManaged(getCurrentAccount().getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layers){
			setAccounts.addAll(layerService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		newAct.setOwnerCommunity(commId); // New event is set to be owned by the community passed as commID
		
		mav.addObject("commId", commId);
		mav.addObject("icons", activityService.getIcons(layers));	
		mav.addObject("iUrl", 0);	
		mav.addObject("activity", newAct);
		mav.addObject("newEvent", newEv);
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		mav.addObject("listOfOwners", listofManagers);
		mav.addObject("managedLayers", layers);
		
		mav.setViewName("/admin/event/newEvent"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/event/new")
	public ModelAndView newEventMgr() throws Exception {
		
		logger.debug("------------------------------ Manager - New Event ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity newAct = new Activity();
		Event newEv = new Event(newAct);
		newEv.setActivity(newAct);
		newAct.setEvent(newEv);
		
		System.out.println("From Controller newEvent - newEv.getActivityId : " + newEv.getActivity().getId());
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		mav.addObject("iUrl", 0);		
		mav.addObject("newEvent", newEv);
		//mav.addObject("listOfIcons", layerService.listIcons());
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		mav.addObject("listOfOwners", accountService.getNonDeleted());
		
		mav.setViewName("/manager/event/newEvent"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/event/editEvent/{id}/{commId}", method= RequestMethod.GET)
	public ModelAndView editEvent(
			@PathVariable(value="id") Integer id, @PathVariable(value="commId") Integer commId, Model model) throws Exception {
		
		logger.debug("------------------------------ Admin - Edit Event ------------------------------");
		System.out.println("Controller - EditEvent/ event id : " + id);
		
		ModelAndView mav = new ModelAndView();
			
			Activity newAct = activityService.findByIdWithRelations(id);
//			Set<Layer> layers = new HashSet<Layer>(); 
//			for (Layer ids: newAct.getLayers()){
//				layers.add(layerService.get(ids.getId()));
//			}
//			newAct.setLayers(layers);
		
			mav.addObject("bannerList", newAct.getBanners());
			mav.addObject("acct", getCurrentAccount());
			
			Set<File> files = newAct.getFiles();
			
			Set<File> pdfs = new HashSet<File>(); 
			for(File q: files){
				File pdfAdd = q;
				if(pdfAdd.getType().endsWith("document")){
					pdfs.add(pdfAdd);
				}
			}
			mav.addObject("filesList", pdfs);
			
			Set<File> videos = new HashSet<File>(); 
			for(File q: files){
				File videoAdd = q;
				if(videoAdd.getType().endsWith("video")){
					videos.add(videoAdd);
				}
			}
			mav.addObject("videosList", videos);
			
			Set<File> weblinks = new HashSet<File>(); 
			for(File q: files){
				File webAdd = q;
				if(webAdd.getType().equalsIgnoreCase("weblink")){
					weblinks.add(webAdd);
				}
			}
			mav.addObject("weblinksList", weblinks);
			
			for (Banner myBanner: newAct.getBanners()){
				logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+myBanner.getTitle());
			}
			
		Event newEv = new Event();
		if(eventService.findById(newAct.getId())==null){  //To chatch when an Act doesn't have event
			newEv.setActivity(newAct);
		}else{
			newEv = newAct.getEvent();
		}
		
		Set <EventCalendarItem> listECI = newEv.getEventCalendarItems();
		if(listECI.isEmpty()){
			System.out.println("Controller - EditEvent/ listECI NULL");
		}
		else{
			Object[] ecio0 = listECI.toArray();
			List<String> eciox = new ArrayList<String>();
			
			// FOR, to detect the last event from Timestamp
			for(int a=0; a<ecio0.length; a++){				
				EventCalendarItem cal0 = new EventCalendarItem();
				cal0 = (EventCalendarItem)ecio0[a];
				String calStr0 = cal0.getCalData();
				String[] calStringx = calStr0.split("[:'\\r''\\n''\\u0085']");
				if(calStringx.length>400){
					for(int i=0; i<calStringx.length; i++){
						System.out.println("Controller - EditEvent/ calStringx["+i+"]: " +  calStringx[i]);
					}
					eciox.add(calStringx[419]);  
					System.out.println("calStringx[419] : "+calStringx[419]); // 419 is the index where the timestamp is located
				}
				if(calStringx.length <= 100 ){
					for(int i=0; i<calStringx.length; i++){
						System.out.println("Controller - EditEvent/ calStringx["+i+"]: " +  calStringx[i]);
						if(calStringx[i].equalsIgnoreCase("DTSTAMP")){
							eciox.add(calStringx[i+1]);	System.out.println("Controller - EditEvent/ calStringx["+i+"+1]: " +  calStringx[i+1]);
						}
					}
				}
			}
			Collections.sort(eciox,Collections.reverseOrder());
			System.out.println("Controller - EditEvent/ listECI calStringx.timestamp : "+eciox.get(0)); //Printing the timestamp of the last event
			
			//The following code purpose is to look the array of event calendar items and get the position of the most recent timestamp
			boolean contains = false;
			int x=0;
			for(int a=0; a<ecio0.length; a++){				
				EventCalendarItem cal1 = new EventCalendarItem();
				cal1 = (EventCalendarItem)ecio0[a];
				String calStr0 = cal1.getCalData();
				
				if(contains = calStr0.contains(eciox.get(0))){
					x=a;
					a=ecio0.length;
					
					Object[] xs = listECI.toArray();
					EventCalendarItem eci01 = new EventCalendarItem();
					eci01 = (EventCalendarItem) xs[x];
					String calStr = null;
					Calendar cal = new Calendar(); // Calendar version from ical4j of icalfortuna
					calStr = eci01.getCalData();
						
					//To get eci for editting
					EventCalendarItem edECI = eventCalendarItemService.editECI(eci01);
					
					model.addAttribute("href",edECI.getId().getCalHref());
						
					cal = eventCalendarItemService.getCalendarEvent(calStr); //Extracts the ical info
					
					if(cal!=null){ 
						String calString0 = cal.getComponents().toString();	
						
						String[] calString1 = calString0.split("[:'\\r''\\n''\\u0085']");
						String dtstart = null;	
						String dtend   = null;	
						String tEnd = null;
						String frequency = null;
						Recur freq= null;
						
						for(int b=0; b< calString1.length; b++){
							for(int i=0; i<calString1.length; i++){
								if(calString1[i].equalsIgnoreCase("VEVENT") && i!=calString1.length){
									for(int j=i; j<calString1.length; j++){
										if(calString1[j].equalsIgnoreCase("DTSTART")||calString1[j].equalsIgnoreCase("DTSTART;TZID=Australia/Adelaide")){
											dtstart=calString1[j+1];	
										}
										if(calString1[j].equalsIgnoreCase("DTEND")||calString1[j].equalsIgnoreCase("DTEND;TZID=Australia/Adelaide")){
											tEnd=calString1[j+1];	
										}
										if(calString1[j].equalsIgnoreCase("RRULE")){
											frequency=calString1[j+1];
												System.out.println("Controller - frequency read 0::"+frequency.toString());
											freq = new Recur(frequency);
												System.out.println("Controller - frequency read 1::"+freq.getFrequency());
											dtend=freq.getUntil().toString();
												System.out.println("Controller - dtend::"+dtend);
											tEnd=dtend;
										}
										
									}
								}
							}	
							if(frequency==null){
								frequency="FREQ=DAILY;";
								dtend=tEnd;
							}
						}
					
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss"); // the format of your date
				        Date formattedStartDate = sdf.parse(dtstart);	
				        Date formattedEndDate = sdf.parse(dtend);	
				        Date formattedUntilDate = sdf.parse(tEnd);
				    
				        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
				        String startDate = sdf2.format(formattedStartDate);	
			            String endDate   = sdf2.format(formattedEndDate);	
			            
			            
			            SimpleDateFormat sdf3 = new SimpleDateFormat("HH:mm");
			            String startTime = sdf3.format(formattedStartDate);	
			            String endTime = sdf3.format(formattedUntilDate);
			            
			            String[] sT = startTime.split(":");
						 Integer sHr = Integer.parseInt(sT[0]);
						 Integer sMin= Integer.parseInt(sT[1]);
						String[] eT = endTime.split(":");
						 Integer eHr = Integer.parseInt(eT[0]);
						 Integer eMin= Integer.parseInt(eT[1]);
			            Integer duration = ((eHr-sHr)*60)+(eMin-sMin); 
			            
					    if(calString0.contains("RRULE:FREQ")){
					    		System.out.println("<< ECI has FREQUENCY >>");
					    		System.out.println("Frequency : "+ frequency);
						        Recur recur = new Recur(frequency);
						        System.out.println("Recur     : "+ recur);
					        	Boolean monday = false;
						        Boolean tuesday = false;
						        Boolean wednesday = false;
						        Boolean thursday = false;
						        Boolean friday = false;
						        Boolean saturday = false;
						        Boolean sunday = false;
					        
						        String daysString0 = recur.getDayList().toString();
						        String[] daysString = daysString0.split("[,'\\r''\\n''\\u0085']");
						        for(int ii=0; ii<daysString.length; ii++){
						        	if(daysString[ii].equalsIgnoreCase("MO")){
						        		monday = true;
						        	}else if(daysString[ii].equalsIgnoreCase("TU")){
						        		tuesday = true;
						        	}else if(daysString[ii].equalsIgnoreCase("WE")){
						        		wednesday = true;
						        	}else if(daysString[ii].equalsIgnoreCase("TH")){
						        		thursday = true;
						        	}else if(daysString[ii].equalsIgnoreCase("FR")){
						        		friday = true;
						        	}else if(daysString[ii].equalsIgnoreCase("SA")){
						        		saturday = true;
						        	}else if(daysString[ii].equalsIgnoreCase("SU")){
						        		sunday = true;
						        	}		
						        }
							    model.addAttribute("monday", monday);
							    model.addAttribute("tuesday", tuesday);
							    model.addAttribute("wednesday", wednesday);
							    model.addAttribute("thursday", thursday);
							    model.addAttribute("friday", friday);
							    model.addAttribute("saturday", saturday);
							    model.addAttribute("sunday", sunday);
							}else{
					        	Boolean monday = false;
						        Boolean tuesday = false;
						        Boolean wednesday = false;
						        Boolean thursday = false;
						        Boolean friday = false;
						        Boolean saturday = false;
						        Boolean sunday = false;
						        
							    model.addAttribute("monday", monday);
							    model.addAttribute("tuesday", tuesday);
							    model.addAttribute("wednesday", wednesday);
							    model.addAttribute("thursday", thursday);
							    model.addAttribute("friday", friday);
							    model.addAttribute("saturday", saturday);
							    model.addAttribute("sunday", sunday);
							}
					    	model.addAttribute("sDate", startDate);
					    	model.addAttribute("eDate", endDate);
						    model.addAttribute("sTime", startTime);
						    model.addAttribute("eTime", endTime);
						    model.addAttribute("duration", duration);
						}
				}
			}	
		}
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layers){
			setAccounts.addAll(layerService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		
		Set<EventCalendarItem> ecis = newAct.getEvent().getEventCalendarItems();
		List<EventCalendarItem> ecisNew = new ArrayList<EventCalendarItem>();
		
		for(EventCalendarItem eci: ecis){
			String[] calStringx = eci.getCalData().split("[:'\\r''\\n''\\u0085']");
			SimpleDateFormat sdfx = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
			SimpleDateFormat sdfx2 = new SimpleDateFormat("yyyy/MM/dd");
			SimpleDateFormat sdfx3 = new SimpleDateFormat("HH:mm");
			
			for(int x=0; x<calStringx.length;x++){
				if(calStringx[x].equalsIgnoreCase("DTSTART")||calStringx[x].equalsIgnoreCase("DTSTART;TZID=Australia/Adelaide")){
					Date formattedsDate = sdfx.parse(calStringx[x+1]);									
					String startDate = sdfx2.format(formattedsDate);	
					eci.setStartDate(startDate);
					String startTime = sdfx3.format(formattedsDate);
					eci.setStartTime(startTime);
				}
				if(calStringx[x].equalsIgnoreCase("DTEND")||calStringx[x].equalsIgnoreCase("DTEND;TZID=Australia/Adelaide")){
					Date formattedeDate = sdfx.parse(calStringx[x+1]);									
					String endDate = sdfx2.format(formattedeDate);	
					eci.setEndDate(endDate);
					String endTime = sdfx3.format(formattedeDate);
					eci.setEndTime(endTime);
				}
			}
			ecisNew.add(eci);
		}
		Collections.sort(ecisNew, EventCalendarItem.ECIComparator);
		mav.addObject("commId", commId);
		mav.addObject("ecis", ecisNew);
		mav.addObject("activity", newAct);
		mav.addObject("layers", newAct.getLayers());
		mav.addObject("newEvent", newEv);  // This variable needs to be named as in the NewEvent
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		mav.addObject("listOfOwners", listofManagers);
		mav.addObject("managedLayers", acct.getLayers());
		
		mav.setViewName("/admin/event/newEvent"); //jsp	
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/event/fastevent", method= RequestMethod.GET)
	public ModelAndView fastEvent() throws Exception {
		
		logger.debug("------------------------------ Admin - Fast Event Creation------------------------------");
		
		ModelAndView mav = new ModelAndView();
		
		Activity actBanners = activityService.findwithBanners(80115); 
	
		mav.addObject("bannerList", actBanners.getBanners());
		
		Set<File> files = activityService.findwithFiles(80115).getFiles();
		
		Set<File> pdfs = new HashSet<File>(); 
		for(File q: files){
			File pdfAdd = q;
			if(pdfAdd.getUrl().endsWith(".pdf")){
				pdfs.add(pdfAdd);
			}
		}
		mav.addObject("filesList", pdfs);
		
		Set<File> videos = new HashSet<File>(); 
		for(File q: files){
			File videoAdd = q;
			if(videoAdd.getUrl().endsWith(".mp4")){
				videos.add(videoAdd);
			}
		}
		
		mav.addObject("videosList", videos);
		
		for (Banner myBanner: actBanners.getBanners()){
			logger.debug("Banner >>> "+myBanner.getTitle());
		}
			
		Event newEv = new Event();
		newEv = actBanners.getEvent();
		newEv.setActivityId(null);
		actBanners.setId(null);
		newEv.setActivity(actBanners);
		
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		
		Account acct = accountService.getWithLayers(accounts.get(0).getId());
		logger.debug("Account >>> "+acct.getEmail());
		logger.debug("Layers >>> "+acct.getLayers().size());
		Set<Layer> layers = acct.getLayers();
		logger.debug("Layers >>> "+layers);
		
		Set<Layer> layersMan = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layersMan){
			setAccounts.addAll(layerService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		mav.addObject("activity", acct);
		mav.addObject("layers", activityService.findwithLayers(80115).getLayers());
		mav.addObject("newEvent", newEv);  // This variable needs to be named as in the NewEvent
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		mav.addObject("listOfOwners", listofManagers);
		mav.setViewName("/admin/event/newEvent");
		mav.addObject("managedLayers", layersMan);
		//jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/event/editEvent/{id}", method= RequestMethod.GET)
	public ModelAndView editEventMgr(@PathVariable(value="id") Integer id, Model model) throws Exception {
		
		logger.debug("------------------------------ Manager - Edit Event ------------------------------");
		System.out.println("Controller - EditEvent/ event id : " + id);
		
		ModelAndView mav = new ModelAndView();
		Activity newAct = activityService.findById(id);
		
		Set<Banner> bannList = new HashSet<Banner>();
		for (Banner ids: newAct.getBanners()){
			bannList.add(bannerService.get(ids.getId()));
		}
		
		mav.addObject("bannerList", bannList);
		
		newAct.setBanners(bannList);
		
		Event newEv = eventService.findById(newAct.getId());
		newEv.setActivity(newAct);
		
		Set <EventCalendarItem> listECI = newEv.getEventCalendarItems();
		if(listECI.isEmpty()){
			System.out.println("Controller - EditEvent/ listECI NULL");
		}
		else{
			Object[] ecio0 = listECI.toArray();
			List<String> eciox = new ArrayList<String>();
			
			// For to detect the last event by Timestamp
			for(int a=0; a<ecio0.length; a++){				
				EventCalendarItem cal0 = new EventCalendarItem();
				cal0 = (EventCalendarItem)ecio0[a];
				String calStr0 = cal0.getCalData();
				String[] calStringx = calStr0.split("[:'\\r''\\n''\\u0085']");
				eciox.add(calStringx[16]);
			}
			Collections.sort(eciox,Collections.reverseOrder());
			
			//The following code purpose is to look the array of event calendar items and get the position of the most recent timestamp
			boolean contains = false;
			int x=0;
			for(int a=0; a<ecio0.length; a++){				
				EventCalendarItem cal1 = new EventCalendarItem();
				cal1 = (EventCalendarItem)ecio0[a];
				String calStr0 = cal1.getCalData();
				
				if(contains = calStr0.contains(eciox.get(0))){
					x=a;
					a=ecio0.length;
					
					Object[] xs = listECI.toArray();
					EventCalendarItem eci01 = new EventCalendarItem();
					eci01 = (EventCalendarItem) xs[x];
					String calStr = null;
					Calendar cal = new Calendar();
					calStr = eci01.getCalData();
					cal = eventCalendarItemService.getCalendarEvent(calStr);
					
					if(cal!=null){
						String calString0 = cal.getComponents().toString();
						String[] calString1 = calString0.split("[:'\\r''\\n''\\u0085']");
							String four = calString1[7];	// Getting the value of DTSTART
							SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss"); // the format of your date
											 //sdf.setTimeZone(java.util.TimeZone.getTimeZone("UTC-5"));
					        Date formattedDate = sdf.parse(four);	
					        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					        				 //sdf2.setTimeZone(java.util.TimeZone.getTimeZone("UTC-5"));
					        String startDate = sdf2.format(formattedDate);
				        	String seven = calString1[10];	// Getting the value of DTEND
					        Date formattedDatea = sdf.parse(seven);	
					        String etartDate = sdf2.format(formattedDatea);
					        String frequency = calString1[26];	// Getting the value of FREQ	//System.out.println("Controller - EditEvent/ FREQ: " +  frequency);
					        Recur recur = new Recur(frequency);	// Converting a String with frequency to Recur object
					        
				        	Boolean monday = false;
					        Boolean tuesday = false;
					        Boolean wednesday = false;
					        Boolean thursday = false;
					        Boolean friday = false;
					        Boolean saturday = false;
					        Boolean sunday = false;
					        
					        String daysString0 = recur.getDayList().toString();
					        String[] daysString = daysString0.split("[,'\\r''\\n''\\u0085']");
					        for(int ii=0; ii<daysString.length; ii++){
					        	if(daysString[ii].equalsIgnoreCase("MO")){
					        		monday = true;
					        	}else if(daysString[ii].equalsIgnoreCase("TU")){
					        		tuesday = true;
					        	}else if(daysString[ii].equalsIgnoreCase("WE")){
					        		wednesday = true;
					        	}else if(daysString[ii].equalsIgnoreCase("TH")){
					        		thursday = true;
					        	}else if(daysString[ii].equalsIgnoreCase("FR")){
					        		friday = true;
					        	}else if(daysString[ii].equalsIgnoreCase("SA")){
					        		saturday = true;
					        	}else if(daysString[ii].equalsIgnoreCase("SU")){
					        		sunday = true;
					        	}	
					        }
					    model.addAttribute("monday", monday);
					    model.addAttribute("tuesday", tuesday);
					    model.addAttribute("wednesday", wednesday);
					    model.addAttribute("thursday", thursday);
					    model.addAttribute("friday", friday);
					    model.addAttribute("saturday", saturday);
					    model.addAttribute("sunday", sunday);
					    model.addAttribute("sDateTime", startDate);
					    model.addAttribute("eDateTime", etartDate);
						}
				}
			}	
			
		}
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
//		List<IconDTO> icons = layerService.listIcons();
//		Integer icon = null;
//		for(int i=0; i<icons.size(); i++){
//			String xxx = icons.get(i).getIconUrl();
//			if(xxx.equalsIgnoreCase(newAct.getIconUrl().toString() ) ){
//				icon = i;
//				i = icons.size();
//			}
//		}
		//mav.addObject("iUrl", icon);  //Current Index of the iconUrl to send to JSP
		
		mav.addObject("newEvent", newEv);  // This variable needs to be named as in the NewEvent
		//mav.addObject("listOfIcons", layerService.listIcons());
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		mav.addObject("listOfOwners", accountService.getNonDeleted());
		mav.addObject("managedLayers", acct.getLayers());
		mav.setViewName("/manager/event/newEvent"); //jsp	
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/event/promote/{eventId}")
	public ModelAndView promote(@PathVariable(value="eventId") Integer eventId) {
		
		logger.debug("------------------------------ Admin - Promote Event ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.addObject("eventId", eventId);
		mav.addObject("mailLayers", layerService.getByOwnerId(activityService.findById(eventId).getAccount().getId()));
		mav.setViewName("/admin/event/promoteEvent"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/event/sendPromote/{activityId}" , method= RequestMethod.POST)
	public ModelAndView promoteMail(
						@RequestParam("layerId") Integer communityId, 
						@PathVariable (value="activityId") Integer activityId 
						){
		logger.debug("------------------------------ Admin - Promote Mail  ------------------------------");
	    	
		ModelAndView mav = new ModelAndView(); 		
		
		String type = activityService.get(activityId).getActivityType();
		if(type.equalsIgnoreCase("info"))
			type="information";
		
		mailService.promote(communityId, activityId, type);
		
		mav = events();
		return mav; 
	}
	
	
	// Public Event
	
	@RequestMapping(value = "/event/{id}", method = RequestMethod.GET)
	public ModelAndView getPublicEvent(@PathVariable(value = "id") Integer id)
			throws ParseException {

		logger.debug("------------------------------Admin Get Single Event  ------------------------------");

		ModelAndView mav = getEvent(id, 70510);

		
		Activity newAct = activityService.findByIdWithRelations(id);
		//	Set<Layer> layers = new HashSet<Layer>(); 
		//	for (Layer ids: newAct.getLayers()){
		//		layers.add(layerService.get(ids.getId()));
		//	}
		//	newAct.setLayers(layers);
	
		
		Set<File> files = newAct.getFiles();
		
		List pdfUrls = new ArrayList(); 
		List pdfIconUrls = new ArrayList(); 
		for(File q: files){
			File pdfAdd = q;
			if(pdfAdd.getUrl().contains(".pdf")){
				pdfUrls.add(pdfAdd.getUrl());
				pdfIconUrls.add(pdfAdd.getIconUrl());
			}
		}
		mav.addObject("pdfUrls", pdfUrls);
		mav.addObject("pdfIconUrls", pdfIconUrls);
		//	logger.debug("=----------------------------pdfs :---------"+pdfUrls.get(0));
		//Videos
		List videoUrls = new ArrayList(); 
		List videoIcons = new ArrayList();
		for(File q: files){
			File videoAdd = q;
			if(videoAdd.getUrl().contains(".mp4")){
				videoUrls.add(videoAdd.getUrl());
				videoIcons.add(videoAdd.getIconUrl());
			}
		}
		Set<File> weblinksList = new HashSet<File>();
		for (File ids: newAct.getFiles()){
			if(ids.getType().equalsIgnoreCase("weblink")){
				weblinksList.add(fileService.findById(ids.getId()));
			}
		}
		mav.addObject("listOfWeblinks", weblinksList);
		mav.addObject("videoUrls", videoUrls);
		mav.addObject("videoIconUrls", videoIcons);
		mav.setViewName("publicEvent");
		//logger.debug("=----------------------------pdfs :---------"+videoUrls.get(0));

		return mav;
	}
	
	// Public Event
	
		@RequestMapping(value = "/event/pay/{id}", method = RequestMethod.GET)
		public ModelAndView payEvent(@PathVariable(value = "id") Integer id)
				throws ParseException {

			logger.debug("----------------------------- Pay for Single Event  ------------------------------");

			ModelAndView mav = getEvent(id, 70510);

			mav.setViewName("payEvent");

			return mav;
		}
		// Process Payments
		
			@RequestMapping(value = "/event/pay/process", method = RequestMethod.POST)
			public ModelAndView payEvent(HttpServletRequest request,
					@RequestParam(value="name",           defaultValue="", 		required=true) String name,
					@RequestParam(value="id",             defaultValue="", 		required=true) Integer id,
					@RequestParam(value="expiry",         defaultValue="", 		required=true) String expiry, 
					@RequestParam(value="cvc",            defaultValue="", 		required=true) String cvc,
					@RequestParam(value="amount",         defaultValue="", 		required=true) String amount, 
					@RequestParam(value="number",         defaultValue="", 		required=true) String number)
					throws ParseException {

				logger.debug("----------------------------- Processing Payment ------------------------------");
				
				String reference = name+"-id-"+id+"-"+System.currentTimeMillis();
				Integer accountID = getCurrentAccount().getId();
				System.out.println("Current user Id who is paying is: =================================================="+accountID);
				
				ModelAndView mav = getEvent(id, 70510);
				double amountDouble = Double.parseDouble(amount) * 100;
				expiry = expiry.replace(" ",""); 
				String json = "{\"card_holder\": \""+name+"\", \"card_number\": \""+number+"\", \"card_expiry\": \""+expiry+"\", \"cvv\": \""+cvc+"\", \"amount\": "+amountDouble+", \"reference\": \""+reference+"\", \"customer_ip\": \""+request.getRemoteAddr()+"\" }";
				String response = mintPayment(json); 
				mav.addObject("result", "");
				
				List<String> errors = new ArrayList<String>();
				boolean paid = false;
				String message =""; 
				String auth = ""; 
				String card_token = ""; 
				
				if (response != null) {
					JSONObject obj = new JSONObject(response);
					
					if (!obj.getJSONObject("response").isNull("authorization")){
					auth = obj.getJSONObject("response").getString("authorization");
					card_token = obj.getJSONObject("response").getString("card_token");
					message = obj.getJSONObject("response").getString("message");
					}
					paid = obj.getBoolean("successful");
					
					System.out.println("Outside the paid function "+paid);
					JSONArray errArr = obj.getJSONArray("errors");
					for (int i = 0; i < errArr.length(); i++)
					{
					     errors.add(errArr.getString(i));
					}
					Account saveAccount = new Account();
					if(paid){
						//Save Token
						System.out.println("Paid is: " + paid);
						Account getOldAccount = accountService.getbyIdWithRelationsFaster(accountID);
						getOldAccount.setCcToken(card_token);
						
						Date date = new Date();
						Date currentDateTime = new Timestamp(date.getTime());
						logger.debug("------------------------------ Save Account with card token ------------------------------");
						
						
						
						 boolean saved = accountService.save(getOldAccount);
						 System.out.println(":)========================================Card token is been saved along with the user's information in Account Table========================:)" + saved);
						 ActivityPayment saveActPayment = new ActivityPayment();
						 saveActPayment.setAccount(getCurrentAccount());
						 saveActPayment.setActivity(activityService.findById(id));
						 saveActPayment.setAmount(Integer.parseInt(amount));
						 saveActPayment.setDate(currentDateTime);
						 saveActPayment.setResponse(response);
						 boolean savedActPay =  actPaymentServie.save(saveActPayment);
						 System.out.println(":)========================================Card's information along with user's id is been saved in ActivityPayment table========================:)" + savedActPay);
						
						 String messageBody = null;
						 String messageBodyCCMeApp=null;
						 String lineToEvent  = SERVER + "/event/pay/"+id+"";
						 if(message.equals("Declined")){
							 messageBody = "Dear "+getCurrentAccount().getFirstName() +" "+ getCurrentAccount().getLastName() + " <br/><br/>" +"<font color=red>Your Payment was declined! </font> <br/ >" 
									 + "Your Recent payment of $"+amount+" was <b>declined</b>. <br>Your Application reference number is: "+ auth +"<br>Click the button to go back to the event payment page <br><a href='"+lineToEvent+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #f77333;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Go back to event</button></a>";
						 }
						 else{
							 messageBody = "Dear "+getCurrentAccount().getFirstName() +" "+ getCurrentAccount().getLastName() + " <br/><br/>" +"<font color=green>Congratulations! Your Payment of $" +amount+ "is approved! </font>" +
									 "<br>Your Application reference number is: "+ auth;
						 
						 }
						 if(message.equals("Declined")){
							 messageBodyCCMeApp = "Dear "+getCurrentAccount().getFirstName() +" "+ getCurrentAccount().getLastName() +
									 " Your Recent payment of $"+amount+" was declined. Your Application reference number is: "+ auth;
						 }
						 else{
							 messageBodyCCMeApp = "Dear "+getCurrentAccount().getFirstName() +" "+ getCurrentAccount().getLastName()  +"Congratulations! Your Payment of $" +amount+ "is approved! " +
									 "Your Application reference number is: "+ auth;
						 
						 }
						 int toId = getCurrentAccount().getId();
						 String Subject = "Payment Confirmation";
						 String headers = getAuth();
		      	 JSONObject jsonObject = new JSONObject(headers);
		         String token = jsonObject.getString("access_token");
		        //Sending payment confirmation notification to the mobile app.
		        String jsonStringforCcMe=null;		       
		        jsonStringforCcMe ="["+"{\"from_id\": \"1082\", \"to_id\":\"" +toId+ "\", \"subject\": \"" + Subject+"\", \"message\":\""+messageBodyCCMeApp+ "\", \"layer_id\":\"1\", \"notification\": \"1\"}"+"]"; 
		       	JSONArray jsonNotificationArray = new JSONArray(jsonStringforCcMe);
						pushMessageNotification(jsonNotificationArray, token);
						System.out.println("Notification has been sent to user : " + getCurrentAccount().getEmail());
						
						//Sending confirmation email to the user..
						Mail mail = new Mail();
						mail.setFrom(FROM_EMAIL);
						mail.setSubject("Payment Confirmation");
						String body = mcs.readPaymentConfirmationEmail();
						body = body.replace("ApprovedOrDecline", message);
						body = body.replace("pasteBodyHere", messageBody);
						mail.setMessage(body);
						mailService.welcomeMail("ccme@digitalmarketsquare.com", getCurrentAccount().getEmail(), mail.getSubject(), mail.getMessage());
						System.out.println("Payment Confirmation email is sent to "+ getCurrentAccount().getEmail());
					
						
					}
				
					mav.addObject("paid", paid);
					mav.addObject("auth", auth);
					mav.addObject("message", message);
					if (errors != null && !errors.isEmpty())
					mav.addObject("errors", errors);
					
				}
				

				mav.setViewName("payEventSuccess");

				return mav;
			}
			
		//Get Auth Token for push notification
			private static String getAuth()
			{
		        URL url;
		        HttpURLConnection connection = null;
		        StringBuffer response = new StringBuffer();
		        
		    try {
		        // TODO code application logic here
		           url = new URL("http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/user/auth");
		           connection = (HttpURLConnection) url.openConnection();
		           connection.setRequestMethod("POST");
		           connection.setConnectTimeout(5000);
		           connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

		           connection.setUseCaches(false);
		           connection.setDoInput(true);
		           connection.setDoOutput(true);  
		           String payload = "email="+  URLEncoder.encode("mishra.anoop6@gmail.com", "UTF-8");
		           payload += "&password=" 
		               + URLEncoder.encode("123", "UTF-8");
		           payload += "&device=" 
		               + URLEncoder.encode("web", "UTF-8");
		          System.out.println(payload);
		           // Send request
		           DataOutputStream wr = new DataOutputStream(
		           connection.getOutputStream());
		           wr.writeBytes(payload);
		           wr.flush();
		           wr.close();
		           System.out.println("Connection status: " + connection.getHeaderFields());
		           
		           InputStream is = connection.getInputStream();
		           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		           String line;
		           while ((line = rd.readLine()) != null) {
		           response.append(line);
		           response.append('\r');
		           }
		           
		           System.out.println("Generated Response :"+response.toString());
		           rd.close();
		    } 
		    catch (Exception ex) {
		    	System.out.println("Exception : " + ex.toString());
		    }
				//return null;
				 return response.toString();
			   
			}
			
			//Push Notification
			private static void pushMessageNotification(JSONArray arr,String token)
			{
		        URL url;
		        HttpURLConnection connection = null;
		        try {
		        	//Get auth header
		        	// TODO code application logic here
		           url = new URL("http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/message/send/batch");
		           connection = (HttpURLConnection) url.openConnection();
		           connection.setRequestMethod("POST");
		           connection.setConnectTimeout(5000);
		           connection.setRequestProperty("Auth-Token", token);
		           connection.setUseCaches(false);
		           connection.setDoInput(true);
		           connection.setDoOutput(true);  
		         //
		           // Send request
		           DataOutputStream wr = new DataOutputStream(
		           connection.getOutputStream());
		           wr.writeBytes(arr.toString());
		           wr.flush();
		           wr.close();
		           
		           InputStream is = connection.getInputStream();
		           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		           String line;
		           StringBuffer response = new StringBuffer();
		           while ((line = rd.readLine()) != null) {
		           response.append(line);
		           response.append('\r');
		           }
		           System.out.println("Response :"+response.toString());
		           rd.close();
		       
		    } catch (IOException ex) {
		    	System.out.println(ex.toString());
		    }
			}

			
	private String mintPayment(String arr) {
		URL url;
		HttpURLConnection connection = null;
		String stringResponse = "";
		try {
			// Get auth header
			// TODO code application logic here
			url = new URL("https://gateway-sandbox.pmnts.io/v1.0/purchases");
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setConnectTimeout(5000);
			connection.setRequestProperty("Authorization", "Basic bWludC1kbXNxOjc5NzQ2NDM1YjgxZWZiZWY2NDdlNGRhM2NiNGE4ZjI0");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			//
			// Send request
			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			logger.debug(arr);
			wr.writeBytes(arr);
			wr.flush();
			wr.close();

			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			stringResponse = response.toString();
			System.out.println("Response :" + response.toString());
			rd.close();

		} catch (IOException ex) {
			System.out.println(ex.toString());
		}

		return stringResponse;
	}
	
	
}


