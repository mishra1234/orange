package au.com.dmsq.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.dmsq.model.Layer;
import au.com.dmsq.service.LayerService;

public class APICommunity {
	private static final Logger logger = LoggerFactory.getLogger(APICommunity.class);
	
	@Autowired LayerService communityService;
	

	@ResponseBody 
	@RequestMapping(method={RequestMethod.GET}, value={"api/getCommunities"})
	public  List<Layer> community() {
		
		logger.debug("------------------------------entering getCommunities ------------------------------");
		
		return communityService.findAll();
	}

}
