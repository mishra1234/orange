package au.com.dmsq.web;

import java.io.UnsupportedEncodingException;
import java.rmi.ConnectException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.AccountLayerAccessId;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Mail;
import au.com.dmsq.model.Message;
import au.com.dmsq.service.AccountLayerAccessService;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;
import au.com.dmsq.service.MessageService;
import au.com.dmsq.service.impl.AnAbstractService;


@Controller
public abstract class AbstractController extends AnAbstractService {
	
	@Autowired AccountService accountService;
	@Autowired MessageService messageService;
	@Autowired LayerService layerService;
	@Autowired MailContentService mcs;
	@Autowired AccountLayerAccessService alaService;
	@Autowired MailService mailService;
	@Autowired ServletContext servletContext;
	@Autowired MessageService messageRService;
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractController.class);
	
	public final String SERVER = "http://cc-me.com.au";
	//public final String SERVER = "http://localhost:8080/orange";
	
	public final String rural_email = "ccme@digitalmarketsquare.com";
	
	String contact_phone=null;
	
	public final String FROM_EMAIL = "no_reply@ccme.com.au ";

	public Integer pork=70515;
	public Integer dairy=70514;
	public Integer horticulture=70512;
	public Integer livestock=70511;
	public Integer grain=70510;
	public Integer wine=70513;
	
	public Integer gatedPork=70521;
	public Integer gatedDairy=70520;
	public Integer gatedHorticulture=70518;
	public Integer gatedLivestock=70517;
	public Integer gatedGrain=70516;
	public Integer gatedWine=70519; 
	
	List <Integer> RC = new ArrayList(); 

	

	
public Account getCurrentAccount(){

	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	if (!(auth instanceof AnonymousAuthenticationToken)) {
		User user =  (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		String username = user.getUsername();
		List<Account> current = accountService.findByEmail(username);
		
		return current.get(0); 
	}
		else{
		
	 return new Account(); 	
	}
	
	
}

public List<Activity> getAllInfos(){

	Account account = getCurrentAccount();
	//Account acc = accountService.getbyIdWithLayers(account.getId());
	Account acctWithALAS = accountService.getALAsbyId(account.getId());
	Account acctWithLayers = accountService.getLayersbyId(account.getId());
	
	List<String> gatedChecks=new ArrayList<>();
	List<String> ungatedChecks=new ArrayList<>();
	List<String> gatedChecksrequested=new ArrayList<>();
	
	List<Layer> allConnected = new ArrayList<Layer>();
	
	for(AccountLayerAccess ala: acctWithALAS.getAccountLayerAccesses())  // To get all the gated communities status of access
	{
		if(ala.getStatus().equals("granted"))
		{
			gatedChecks.add(ala.getLayer().getId().toString());
			if (ala.getLayer().getId() != null && RC.contains(ala.getLayer().getId()))
				allConnected.add(ala.getLayer());
		}
		else if(ala.getStatus().equals("requested"))
		{
			gatedChecksrequested.add(ala.getLayer().getId().toString());
		}
		
	}
	
	for(Layer alayer: acctWithLayers.getLayers_1())  //Get connected layers from the logged in account
	{
		ungatedChecks.add(alayer.getId().toString());
		if (alayer.getId() != null && RC.contains(alayer.getId()))
			allConnected.add(alayer);
	}
	
	List<Activity> activities = new ArrayList<>();
	for(Layer layer:allConnected){
		Layer layerAct = layerService.findByIdWithActivitiesAll(layer.getId());
		activities.addAll(layerAct.getActivities());
	}
	
	List<Activity> infos= new ArrayList<>();
	for(Activity act:activities){
		if(act.getActivityType().equalsIgnoreCase("info")){
			infos.add(act);
		}
	}
	System.out.println("Total Infos to display: "+infos.size());
	return infos;
}


public int getNewMessages() {

	Account acct = getCurrentAccount();
	try {
		return messageService.countMyNew(acct.getId());
	} catch (Exception e) {
		return 0;
	}

}


public boolean isAdmin( Account account){
	
	return account.getRole().getId() == 1?true:false; 
}

public boolean isOwner(){
	
	return false; 
}

public List<Layer> getConnectedLayers(){
	
	return layerService.getByOwnerId(getCurrentAccount().getId());
}

public int myFutureActivities(){
	return 0; 
	
}

public ModelAndView setBasicData(){
	
	logger.debug("Staring basic data: " + System.nanoTime());
	
	
	RC.add(pork);
	RC.add(dairy);
	RC.add(horticulture);
	RC.add(livestock);
	RC.add(grain);
	RC.add(wine);
	
	RC.add(gatedWine);
	RC.add(gatedGrain);
	RC.add(gatedLivestock);
	RC.add(gatedHorticulture);
	RC.add(gatedPork);
	RC.add(gatedDairy);
	
	
	List<Layer> connectedLayers = getConnectedLayers();
	
	ModelAndView mav  = new ModelAndView();
	
	Account account = getCurrentAccount();
	
	if (account != null) {
	mav.addObject("isAdmin", isAdmin(account));
	mav.addObject("connectedLayers",connectedLayers);
	mav.addObject("newMessages", getNewMessages());
	

	mav.addObject("account", account);
	Account acc = accountService.getbyIdWithLayersFaster(account.getId()); 
	
	List<String> gatedChecks=new ArrayList<>();
	List<String> ungatedChecks=new ArrayList<>();
	List<String> gatedChecksrequested=new ArrayList<>();
	
	List<Integer> allConnected = new ArrayList<Integer>();
	
	for(AccountLayerAccess ala: acc.getAccountLayerAccesses())
	{
		if(ala.getStatus().equals("granted"))
		{
			gatedChecks.add(ala.getLayer().getId().toString());
			if (ala.getLayer().getId() != null && RC.contains(ala.getLayer().getId()))
				allConnected.add(ala.getLayer().getId());
		}
		else if(ala.getStatus().equals("requested"))
		{
			gatedChecksrequested.add(ala.getLayer().getId().toString());
		}
		
	}
	
	for(Layer alayer: acc.getLayers_1())
	{
		ungatedChecks.add(alayer.getId().toString());
		if (alayer.getId() != null && RC.contains(alayer.getId()))
			allConnected.add(alayer.getId());
	}
	mav.addObject("allConnected", allConnected);
	mav.addObject("acc", acc);
//	mav.addObject("acct", acc);
	mav.addObject("acct", getCurrentAccount());
	
	mav.addObject("ungatedPork", ungatedChecks.contains("70515"));
	mav.addObject("ungatedGrain", ungatedChecks.contains("70510"));
	mav.addObject("ungatedLivestock", ungatedChecks.contains("70511"));
	mav.addObject("ungatedHorticulture", ungatedChecks.contains("70512"));
	mav.addObject("ungatedDairy", ungatedChecks.contains("70514"));
	mav.addObject("ungatedWine", ungatedChecks.contains("70513"));
	
	mav.addObject("gatedPork", gatedChecks.contains("70521")?"granted":(gatedChecksrequested.contains("70521")?"requested":"denied"));
	mav.addObject("gatedGrain", gatedChecks.contains("70516")?"granted":(gatedChecksrequested.contains("70516")?"requested":"denied"));
	mav.addObject("gatedLivestock", gatedChecks.contains("70517")?"granted":(gatedChecksrequested.contains("70517")?"requested":"denied"));
	mav.addObject("gatedHorticulture", gatedChecks.contains("70518")?"granted":(gatedChecksrequested.contains("70518")?"requested":"denied"));
	mav.addObject("gatedDairy", gatedChecks.contains("70520")?"granted":(gatedChecksrequested.contains("70520")?"requested":"denied"));
	
	}
	logger.debug("End basic data: " + System.nanoTime());
	return mav;
}


public String returnFormattedDate(long datetime)
{
	Date temp=new Date(datetime*1000);
	return temp.toString();
}

public boolean addAccountLayerAccess(Layer accessLayer, Account userAccount)
{
	SecureRandom random;
	try {
		random = SecureRandom.getInstance("SHA1PRNG");
		String grantAccessCode= SHA1Gen(String.valueOf(random.nextLong()));
		String denyAccessCode= SHA1Gen(String.valueOf(random.nextLong()));
				AccountLayerAccess alayeraccess=new AccountLayerAccess();
		AccountLayerAccessId alayeraccessId= new AccountLayerAccessId();
		alayeraccessId.setAccountId(userAccount.getId());
		alayeraccessId.setLayerId(accessLayer.getId());
		
		alayeraccess.setId(alayeraccessId);
		alayeraccess.setAccount(userAccount);
		alayeraccess.setGrantAccessCode(grantAccessCode);
		alayeraccess.setDenyAccessCode(denyAccessCode);
		alayeraccess.setLayer(accessLayer);
		alayeraccess.setLastStatusUpdate(new Date());
		alayeraccess.setStatus("requested");
		
		alaService.save(alayeraccess);
		if(true)
		{
			gatedAccessConfirmationMail(accessLayer,userAccount,grantAccessCode,denyAccessCode);
			
			return true;
		}
		
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	return false;
}

public String SHA1Gen(String text) 
	    throws NoSuchAlgorithmException, UnsupportedEncodingException  { 
	    MessageDigest md;
	    md = MessageDigest.getInstance("SHA-1");
	    byte[] sha1hash = new byte[40];
	    md.update(text.getBytes("iso-8859-1"), 0, text.length());
	    sha1hash = md.digest();
	    return convertToHexAbs(sha1hash);
	    } 

private String convertToHexAbs(byte[] data) { 
    StringBuffer buf = new StringBuffer();
    for (int i = 0; i < data.length; i++) { 
        int halfbyte = (data[i] >>> 4) & 0x0F;
        int two_halfs = 0;
        do { 
            if ((0 <= halfbyte) && (halfbyte <= 9)) 
                buf.append((char) ('0' + halfbyte));
            else 
                buf.append((char) ('a' + (halfbyte - 10)));
            halfbyte = data[i] & 0x0F;
        } while(two_halfs++ < 1);
    } 
    return buf.toString();
} 

public void gatedAccessConfirmationMail(Layer currentLayer, Account currentAccount, String grantAccessCode, String denyAccessCode)
{
	 contact_phone = currentAccount.getMobilephone();
	 if(contact_phone == null)
	 {
		 contact_phone = currentAccount.getLandline();
		 if(contact_phone == null)
			 contact_phone = "Not Provided";
	 }
	
		//Grant/Deny email to the community owner
		Mail mail = new Mail();
		mail.setFrom(FROM_EMAIL);
		mail.setSubject("Access Request for the Community "+currentLayer.getName()+".");
		String body = mcs.readAccessRequestEmail();
		String main_body = "User: " + currentAccount.getName()+ " with email: " + currentAccount.getEmail() + " and contact telephone number is: " + contact_phone + " has requested  access to your community. Click the below links to grant or deny access.";
		String grantlink = SERVER + "/token/"+grantAccessCode+"/action/"+"grant";
		String revokelink = SERVER + "/token/"+denyAccessCode+"/action/"+"deny";
		main_body += " <a href='"+grantlink+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Click here to confirm now</button></a><p><p>	 Or click the link below to deny:<p><p> <a href='"+revokelink+"'>"+"Deny"+"</a>";
		body = body.replaceAll("pasteBodyHere", main_body);
		mail.setMessage(mcs.insertUnsubscribe(currentLayer.getAccount(), body));
		mailService.welcomeMail(mail.getFrom(), currentLayer.getAccount().getEmail(), mail.getSubject(), mail.getMessage());
		
		
		//Confirmation email to the user
		Mail confirmmail = new Mail();
		confirmmail.setFrom(FROM_EMAIL);
		confirmmail.setSubject("Confirmation - Access Request for the Community "+currentLayer.getName()+".");
		String bodyMail = mcs.readAccessRequestMemberEmail();
		String cbody = " Confirmation mail for the access request. Your access request for the community "+currentLayer.getName()+" has been submitted to the community owner.";
		bodyMail = bodyMail.replaceAll("pasteBodyHere", cbody);
		confirmmail.setMessage(mcs.insertUnsubscribe(currentAccount, bodyMail));
		mailService.welcomeMail(confirmmail.getFrom(), currentAccount.getEmail(), confirmmail.getSubject(), confirmmail.getMessage());
}


public void nonGatedLayerSubscriptionConfirmationMail(Layer currentLayer, Account currentAccount, String action)
{
	    //Confirmation Message
		Message userMessage=new Message();
		userMessage.setAccountByToId(currentAccount);
		userMessage.setAccountByFromId(currentLayer.getAccount());
		userMessage.setDateSent(System.currentTimeMillis()/1000);
		userMessage.setDeleted(false);
		userMessage.setIsRead(false);
		
	
		//Confirmation email to the user
		Mail confirmmail = new Mail();
		confirmmail.setFrom(FROM_EMAIL);
		String htmlBody = mcs.readSubsEmail();
		String centerBody="";
		if(action.equals("connect"))
		{
			confirmmail.setSubject("Confirmation - subscription to "+currentLayer.getName()+".");
			centerBody = "You have subscribed to the community "+currentLayer.getName()+" successfully.";
			
			userMessage.setSubject("Welcome to "+currentLayer.getName()+".");
			userMessage.setMessage("Your are now conn " +currentLayer.getName()+" has been ");
		}
		else
		{
			confirmmail.setSubject("Confirmation - unsubscribe to "+currentLayer.getName()+".");
			centerBody = "You have unsubscribed to the community "+currentLayer.getName()+" successfully.";
			
			userMessage.setSubject("Disconnected from "+currentLayer.getName()+".");
			userMessage.setMessage("You are disconnected from " +currentLayer.getName()+".");
		}
		
		htmlBody = htmlBody.replaceAll("pasteBodyHere", centerBody);
		messageRService.save(userMessage);
		confirmmail.setMessage(mcs.insertUnsubscribe(currentAccount,htmlBody));
		mailService.subscriptionEmails(confirmmail.getFrom(), currentAccount.getEmail(), confirmmail.getSubject(), confirmmail.getMessage());
}
	
}
