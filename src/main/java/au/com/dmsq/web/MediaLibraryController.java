package au.com.dmsq.web;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Account;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.EventCalendarItemService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.FileService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;


@Controller
public class MediaLibraryController extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired EventService eventService;
	@Autowired EventCalendarItemService eventCalendarItemService;
	@Autowired ActivityService activityService;
	@Autowired LayerService layerService;
	@Autowired AccountService accountService;
	@Autowired BannerService bannerService;
	@Autowired FileService fileService;
	@Autowired MailService mailService;
	@Autowired MailContentService mcs;
	
	@Transactional
	
	@RequestMapping(value="/admin/medialibrary/iconsEvents")
	public ModelAndView getIconsEvent() {
		
		logger.debug("------------------------------ Admin - GET ICONS OF EVENT FROM LAYERS MANAGED ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		Account acct = accountService.getbyIdWithLayersManaged(getCurrentAccount().getId());
		
		mav.addObject("iconsB", activityService.getIconsB(acct.getLayers()));
		mav.setViewName("/admin/medialibrary/icons"); //jsp
		return mav; 
	}
	
	@RequestMapping(value="/admin/medialibrary/iconsInfos")
	public ModelAndView getIconsInfo() {
		
		logger.debug("------------------------------ Admin - GET ICONS OF INFOS FROM LAYERS MANAGED ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		Account acct = accountService.getbyIdWithLayersManaged(getCurrentAccount().getId());
		
		mav.addObject("iconsB", activityService.getIconsInfosB(acct.getLayers()));
		mav.setViewName("/admin/medialibrary/icons"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/medialibrary/imagesBanners")
	public ModelAndView getBanners() {
		
		logger.debug("------------------------------ Admin - GET BANNERS FROM LAYERS MANAGED ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		Account acct = accountService.getbyIdWithLayersManaged(getCurrentAccount().getId());
		
		mav.addObject("banners", activityService.getBanners(acct.getLayers()));	
		mav.setViewName("/admin/medialibrary/banners"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/medialibrary/evVideos")
	public ModelAndView getEvVideos() {
		
		logger.debug("------------------------------ Admin - GET VIDEOS OF EVENTS FROM LAYERS MANAGED ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		Account acct = accountService.getbyIdWithLayersManaged(getCurrentAccount().getId());
		
		mav.addObject("videos", activityService.getEvVideos(acct.getLayers()));	
		mav.setViewName("/admin/medialibrary/videos"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/medialibrary/evFiles")
	public ModelAndView getEvFiles() {
		
		logger.debug("------------------------------ Admin - GET PDFs OF EVENTS FROM LAYERS MANAGED ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		Account acct = accountService.getbyIdWithLayersManaged(getCurrentAccount().getId());
		
		mav.addObject("files", activityService.getEvFiles(acct.getLayers()));	
		mav.setViewName("/admin/medialibrary/files"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/medialibrary/infVideos")
	public ModelAndView getInfVideos() {
		
		logger.debug("------------------------------ Admin - GET VIDEOS OF INFOS FROM LAYERS MANAGED ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		Account acct = accountService.getbyIdWithLayersManaged(getCurrentAccount().getId());
		
		mav.addObject("videos", activityService.getEvVideos(acct.getLayers()));	
		mav.setViewName("/admin/medialibrary/videos"); //jsp
		return mav; 
	}
	
}


