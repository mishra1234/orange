package au.com.dmsq.web;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Message;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.MessageService;

@Controller
public class Admin extends AbstractController  {
 
	@Autowired MessageService messageService;
	@Autowired LayerService layerService;
	@Autowired AccountService accountService;
	@Autowired ActivityService activityService;
	@Autowired BannerService bannerService;
	
	private static final Logger logger = LoggerFactory.getLogger(Admin.class);

	
	@RequestMapping("/admin/addUser")
	public ModelAndView addUser (HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		//mav.addObject("userGrid",  userService.findAll());
		mav.addObject("acct", getCurrentAccount());
		mav.setViewName("/admin/addUser");
		return mav; 
	}
	
	
	@RequestMapping("/admin/saveUser")
	public ModelAndView saveUser () {
		logger.debug("----------Save User -------------");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("acct", getCurrentAccount());
		
		return mav; 
	}
	
	@RequestMapping("/admin/inbox")
	public ModelAndView inbox () {
		logger.debug("----------View Inbox -------------");
		
		ModelAndView mav = new ModelAndView();
		List<Message> readmessages = messageService.getRead(getCurrentAccount().getId());
		List<Message> unreadmessages =messageService.getUnread(getCurrentAccount().getId());

		List<Layer> accessLayers= new ArrayList<>(); 
		//accessLayers = layerService.getByOwnerId(getCurrentAccount().getId());
		logger.debug(">>> No accessLayers : "+accessLayers.size());
		accessLayers.addAll(layerService.layersIfManager(getCurrentAccount().getId()));
		logger.debug(">>> No accessLayers after : "+accessLayers.size());
		
		Set<Layer> layersS = new HashSet<Layer>();
		for(Layer l:accessLayers){
			//logger.debug(">>> Layer : "+l.getId());
			layersS.add(l);
		}
		
		logger.debug("No of Communities this user is owner/ manager for : "+layersS.size());
		
		Set<Account> accessAccounts=new HashSet<>();
		Set<Activity> accessActivites = new HashSet<>();
		
		for(Layer l: layersS)
		{
			accessAccounts.addAll(l.getAccounts_1());
			accessActivites.addAll(l.getActivities());
		}
		logger.debug("No of accounts this user has access to : "+accessAccounts.size());
		logger.debug("No of activities this user has access to : "+accessActivites.size());
		
		
		logger.debug("---No of  read Messages for the user----" + readmessages.size());
		logger.debug("---No of  unread Messages for the user----" + unreadmessages.size());
		mav.addObject("readmessages", readmessages);
		mav.addObject("unreadmessages",unreadmessages);
		mav.addObject("userAccounts", accessAccounts);
		mav.addObject("userLayers", layersS);
		mav.addObject("activities", accessActivites);
		mav.addObject("acct", getCurrentAccount());
		mav.setViewName("/admin/inbox/inbox2");
		return mav; 
	}
	

	@RequestMapping(value = "/admin/inbox/send", method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public @ResponseBody String inboxSend (@RequestBody Object command, HttpSession session) {
		logger.debug("----------Send message -------------");
		
		logger.debug("Current Recipient is"+ command.toString());
		logger.debug("Current Recipient is"+ session.getAttributeNames().hasMoreElements());
		
		
		JSONObject obj = new JSONObject(command.toString().replace("=", ":"));
	  if(obj.has("recipient") && !obj.isNull("recipient"))
	  {
		  JSONArray recipientListJson = obj.getJSONArray("recipient");
		  if(recipientListJson!=null && recipientListJson.length()>0)
		  {
			List<Layer> recipientLayer=new ArrayList<>();
			Set<Account> recipientAccounts=new TreeSet<>();
			String mMessage= obj.getString("message");
			String mSubject = obj.getString("subject");
			
			Set<Activity> mActivities=new HashSet<>();
			Set<Layer> mLayers =new HashSet<>();
			for (int i = 0; i < recipientListJson.length(); ++i) 
			{
			  String access=recipientListJson.getString(i);
			  String [] accessor= access.split("--");
			  
			  for(String s: accessor)
			  {
				  logger.debug("Current String is"+ s);
			  }
			  
			  if(accessor[1].equals("Layer"))
			  {
				  recipientLayer.add(layerService.get(Integer.parseInt(accessor[0])));
				  
				  logger.debug("Added Layer"+ accessor[0]);
			  }
			  else
			  {
				  recipientAccounts.add(accountService.findById(Integer.parseInt(accessor[0])));
			  }
			}
			
			for(Layer l: recipientLayer)
			{
				logger.debug("Added accounts for layer"+ l.getName());
				logger.debug("No of accounts for layer"+ l.getAccounts_1().size());
				recipientAccounts.addAll(l.getAccounts_1());
			}
			
			if(recipientAccounts.size()<=0)
			{
				return "Error -- No Users Found";
			}
			
			//Here add the code
			JSONObject jsonNotification = new JSONObject();
			if(obj.has("activities") && !obj.isNull("activities"))
			{
				JSONArray activitiesListJson = obj.getJSONArray("activities");
				
				if(activitiesListJson!=null && activitiesListJson.length()>0)
				{
					for (int i = 0; i < activitiesListJson.length(); ++i) 
					{
						String activityString=activitiesListJson.getString(i);
					  String [] accessor= activityString.split("--");
					 
					  mActivities.add(activityService.findById(Integer.parseInt(accessor[0])));
						  
						  logger.debug("To be promoted Activity: "+ accessor[0]);
					  }
					}
			}
			
			if(obj.has("communities") && !obj.isNull("communities"))
			{
				JSONArray comListJson = obj.getJSONArray("communities");
				
				if(comListJson!=null && comListJson.length()>0)
				{
					for (int i = 0; i < comListJson.length(); ++i) 
					{
						String comString=comListJson.getString(i);
					  String [] accessor= comString.split("--");
					 
					  mLayers.add(layerService.findById(Integer.parseInt(accessor[0])));
						  
						  logger.debug("To be promoted Community: "+ accessor[0]);
					  }
					}
			}
			
			
//						logger.debug("-------------Activities are: " + activitiesListJson.getInt(0) + "----------");
//						mActivities.add(activityService.findById(activitiesListJson.getInt(i)));
//						
//					}
//				}
//			}
			Set<Banner> mBSet=new HashSet<Banner>();
			
			if(obj.getString("banner")!=null){
			String imgurl=obj.getString("banner");
			Banner mBanner=new Banner();
			if(imgurl!=null)
			{
				mBanner.setImageUrl(imgurl);
				mBanner.setTitle("Banner for a message.");
				mBanner.setSubtitle("Banner for a message.");
				
				if(bannerService.save(mBanner))
				{
					mBSet.add(mBanner);
				}
			}
			}
			for(Account a: recipientAccounts)
			{
				logger.debug("Added accounts ="+ recipientAccounts.size());
				logger.debug("Currently sending to ="+ a.getEmail());
				Message nM=new Message();
				int from_id = a.getId();
				System.out.println("From_id: " + from_id);
				nM.setAccountByFromId(getCurrentAccount());
				nM.setAccountByToId(a);
				nM.setMessage(mMessage);
				nM.setSubject("Test Subject");
				nM.setDateSent((System.currentTimeMillis()/1000));
				nM.setDeleted(false);
				nM.setIsRead(false);
				if(mActivities.size()>0)
				{
					nM.setActivities(mActivities);
				}
				if(mLayers.size()>0){
					nM.setLayers(mLayers);
					logger.debug("--------------Community size is"+mLayers.toString()+"--------------");
				}
				if(mBSet.size()>0)
				{
					nM.setBanners(mBSet);
				}
				String jsonMessageString=null;
				logger.debug("-------------------------------Actiities are"+mActivities + "---------------");
				 String headers = getAuth();
      	 JSONObject jsonObject = new JSONObject(headers);
      	JSONArray jsonMessageArray=null;
        String token = jsonObject.getString("access_token");
     
				if((mActivities.size()>0) && (mLayers.size()>0)){
				for(Activity act:mActivities) for(Layer l:mLayers) {
				
			//	String jsonMessageString = "[{\"from_id\":\"" +getCurrentAccount().getId() + "\",\"to_id\":\"" +from_id+ "\", \"subject\": \"" +mSubject+"\", \"message\":\""+mMessage+ "\", \"layer_id\": \""+act.getId()+"\", \"notification\": \"1\" }]";
			jsonMessageString="[{\"from_id\":\"" +getCurrentAccount().getId() + "\",\"to_id\":\"" +from_id+ "\", \"subject\": \"" +mSubject+"\", \"message\":\""+mMessage+ "\", \"layer_id\":\"1\", \"layer_links\": [{\"id\": \""+l.getId()+"\" } ], \"activity_links\": [{\"id\": \""+act.getId()+"\" } ], \"notification\": \"1\" }]";
			jsonMessageArray= new JSONArray(jsonMessageString); 
			pushMessageNotification(jsonMessageArray, token);
				}
				}
				else if((mActivities.size()>0) && (mLayers.size()==0)){
					for(Activity act:mActivities)  {
						
				jsonMessageString="[{\"from_id\":\"" +getCurrentAccount().getId() + "\",\"to_id\":\"" +from_id+ "\", \"subject\": \"" +mSubject+"\", \"message\":\""+mMessage+ "\", \"layer_id\":\"1\",  \"activity_links\": [{\"id\": \""+act.getId()+"\" } ], \"notification\": \"1\" }]";
				jsonMessageArray= new JSONArray(jsonMessageString); 
				pushMessageNotification(jsonMessageArray, token);
						
				}}
				else if((mActivities.size()==0) && (mLayers.size()>0))
				{
					 for(Layer l:mLayers) {
						
					jsonMessageString="[{\"from_id\":\"" +getCurrentAccount().getId() + "\",\"to_id\":\"" +from_id+ "\", \"subject\": \"" +mSubject+"\", \"message\":\""+mMessage+ "\", \"layer_id\":\"1\", \"layer_links\": [{\"id\": \""+l.getId()+"\" } ], \"notification\": \"1\" }]";
					jsonMessageArray= new JSONArray(jsonMessageString); 
					pushMessageNotification(jsonMessageArray, token);
						
				}
				}else{
					jsonMessageString="[{\"from_id\":\"" +getCurrentAccount().getId() + "\",\"to_id\":\"" +from_id+ "\", \"subject\": \"" +mSubject+"\", \"message\":\""+mMessage+ "\", \"layer_id\":\"1\" , \"notification\": \"1\" }]";
					jsonMessageArray= new JSONArray(jsonMessageString); 
					pushMessageNotification(jsonMessageArray, token);
				
				}
				logger.debug("Message to be sent: " + jsonMessageString);
				Boolean mSent=messageService.save(nM);
				logger.debug("Message Sent = " + mSent);
				
				// EmailService 
				// sendNewMessage(name, subject, id);
				String name = nM.getAccountByFromId().getName();
				String subject = nM.getSubject();
				String id = nM.getId().toString();
			}
			
			//return new ModelAndView("ok");
		//	return "Success -- Email Sent Successfully";
			return "redirect:/admin/inbox/inbox2";
		   }
	  }
	  
	  return "Error -- No recipients Found";
	}
	//Get Auth Token for push notification
	private static String getAuth()
	{
        URL url;
        HttpURLConnection connection = null;
        StringBuffer response = new StringBuffer();
        
    try {
        // TODO code application logic here
           url = new URL("http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/user/auth");
           connection = (HttpURLConnection) url.openConnection();
           connection.setRequestMethod("POST");
           connection.setConnectTimeout(5000);
           connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

           connection.setUseCaches(false);
           connection.setDoInput(true);
           connection.setDoOutput(true);  
           String payload = "email="+  URLEncoder.encode("mishra.anoop6@gmail.com", "UTF-8");
           payload += "&password=" 
               + URLEncoder.encode("123", "UTF-8");
           payload += "&device=" 
               + URLEncoder.encode("web", "UTF-8");
          System.out.println(payload);
           // Send request
           DataOutputStream wr = new DataOutputStream(
           connection.getOutputStream());
           wr.writeBytes(payload);
           wr.flush();
           wr.close();
           System.out.println("Connection status: " + connection.getHeaderFields());
           
           InputStream is = connection.getInputStream();
           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
           String line;
           while ((line = rd.readLine()) != null) {
           response.append(line);
           response.append('\r');
           }
           
           System.out.println("Generated Response :"+response.toString());
           rd.close();
    } 
    catch (Exception ex) {
    	System.out.println("Exception : " + ex.toString());
    }
		//return null;
		 return response.toString();
	   
	}
	
	//Push Notification
	private static void pushMessageNotification(JSONArray arr,String token)
	{
        URL url;
        HttpURLConnection connection = null;
        try {
        	//Get auth header
        	// TODO code application logic here
           url = new URL("http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/message/send/batch");
           connection = (HttpURLConnection) url.openConnection();
           connection.setRequestMethod("POST");
           connection.setConnectTimeout(5000);
           connection.setRequestProperty("Auth-Token", token);
           connection.setUseCaches(false);
           connection.setDoInput(true);
           connection.setDoOutput(true);  
         //
           // Send request
           DataOutputStream wr = new DataOutputStream(
           connection.getOutputStream());
           wr.writeBytes(arr.toString());
           wr.flush();
           wr.close();
           
           InputStream is = connection.getInputStream();
           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
           String line;
           StringBuffer response = new StringBuffer();
           while ((line = rd.readLine()) != null) {
           response.append(line);
           response.append('\r');
           }
           System.out.println("Response :"+response.toString());
           rd.close();
       
    } catch (IOException ex) {
    	System.out.println(ex.toString());
    }
	}

	@RequestMapping("/admin/inbox/sent")
	public ModelAndView sent () {
		logger.debug("----------Save User -------------");
		
		ModelAndView mav = new ModelAndView();
		
		List<Message> messages = messageService.getSent(getCurrentAccount().getId());
		mav.addObject("messages", messages);
		mav.setViewName("/sent");
		
		return mav; 
	}
	
	@RequestMapping("/admin/inbox/archive")
	public ModelAndView archive () {
		logger.debug("----------Save User -------------");
		
		ModelAndView mav = new ModelAndView();
		
		List<Message> messages = messageService.getRead(getCurrentAccount().getId());
		mav.addObject("messages", messages);
		mav.setViewName("/archive");
		
		return mav; 
	}
	
	@RequestMapping("/admin/inbox/view/{curr}")
	public ModelAndView view (@PathVariable(value="curr") Integer curr) {
		logger.debug("----------View Message -------------");
		
		ModelAndView mav = new ModelAndView();
		
		Message message = messageService.findById(curr);
		mav.addObject("message", message);
		mav.setViewName("/admin/inbox/messageView");
		mav.addObject("acct", getCurrentAccount());
		
		return mav; 
	}
	
}
