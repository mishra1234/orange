package au.com.dmsq.web;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import au.com.dmsq.model.FileMeta;
import au.com.dmsq.web.FileUploadHelper;;

@Controller
@RequestMapping("/file")
public class FileUploadController {

	
	LinkedList<FileMeta> files = new LinkedList<FileMeta>();
	FileMeta fileMeta = null;
	
	LinkedList<FileMeta> filesBanner = new LinkedList<FileMeta>();
	FileMeta fileMetaBanner = null;
	
	LinkedList<FileMeta> filesFile = new LinkedList<FileMeta>();
	FileMeta fileMetaFile = null;
	
	LinkedList<FileMeta> filesVideo = new LinkedList<FileMeta>();
	FileMeta fileMetaVideo = null;

	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
	
	
	@RequestMapping(value="/upload", method = RequestMethod.POST)
	public @ResponseBody LinkedList<FileMeta> upload(MultipartHttpServletRequest request, HttpServletResponse response) {
 
		//1. build an iterator
		 Iterator<String> itr =  request.getFileNames();
		 MultipartFile mpf = null;

		 //2. get each file
		 while(itr.hasNext()){
			 
			 //2.1 get next MultipartFile
			 mpf = request.getFile(itr.next()); 

			 //2.2 if files > 10 remove the first from the list
			 if(files.size() >= 5)
				 files.pop();
			 
			 //2.3 create new fileMeta
			 fileMeta = new FileMeta();
			 fileMeta.setFileName(mpf.getOriginalFilename());
			 fileMeta.setFileSize(mpf.getSize()/1024+" Kb");
			 fileMeta.setFileType(mpf.getContentType());
			 
			 try {
				fileMeta.setBytes(mpf.getBytes()); //mpf.getBytes()
				
				// copy file to local disk (make sure the path "e.g. D:/temp/files" exists)
				//FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream("I:/temp/files/"+mpf.getOriginalFilename()));
				FileUploadHelper helper = new FileUploadHelper(); 
				String result = helper.upload(mpf, fileMeta);
				logger.debug("Result for upload:" + result);
				fileMeta.setUrl(result);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 //2.4 add to files
			 files.clear();
			 files.add(fileMeta);
		 }
		 
		// result will be like this
		// [{"fileName":"app_engine-85x77.png","fileSize":"8 Kb","fileType":"image/png"},...]
		return files;
	}
	
	@RequestMapping(value="/uploadBanners", method = RequestMethod.POST)
	public @ResponseBody LinkedList<FileMeta> uploadBanners(MultipartHttpServletRequest request, HttpServletResponse response) {
 
		//1. build an iterator
		 Iterator<String> itr =  request.getFileNames();
		 MultipartFile mpf = null;

		 //2. get each file
		 while(itr.hasNext()){
			 
			 //2.1 get next MultipartFile
			 mpf = request.getFile(itr.next()); 

			 //2.2 if files > 10 remove the first from the list
			 if(filesBanner.size() >= 10)
				 filesBanner.pop();
			 
			 //2.3 create new fileMeta
			 fileMetaBanner = new FileMeta();
			 fileMetaBanner.setFileName(mpf.getOriginalFilename());
			 fileMetaBanner.setFileSize(mpf.getSize()/1024+" Kb");
			 fileMetaBanner.setFileType(mpf.getContentType());
			 
			 try {
				fileMetaBanner.setBytes(mpf.getBytes()); //mpf.getBytes()
				// copy file to local disk (make sure the path "e.g. D:/temp/files" exists)
				//FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream("I:/temp/files/"+mpf.getOriginalFilename()));
				FileUploadHelper helper = new FileUploadHelper(); 
				String result = helper.upload(mpf, fileMetaBanner);
				logger.debug("Result for upload Banner:" + result);
				fileMetaBanner.setUrl(result);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 //2.4 add to files
			 filesBanner.clear();
			 filesBanner.add(fileMetaBanner);
		 }
		// result will be like this
		// [{"fileName":"app_engine-85x77.png","fileSize":"8 Kb","fileType":"image/png"},...]
		return filesBanner;
 
	}
	
	
	@RequestMapping(value="/uploadFiles", method = RequestMethod.POST)
	public @ResponseBody LinkedList<FileMeta> uploadFiles(MultipartHttpServletRequest request, HttpServletResponse response) {
 
		//1. build an iterator
		 Iterator<String> itr =  request.getFileNames();
		 MultipartFile mpf = null;

		 //2. get each file
		 while(itr.hasNext()){
			 
			 //2.1 get next MultipartFile
			 mpf = request.getFile(itr.next()); 

			 //2.2 if files > 10 remove the first from the list
			 if(filesFile.size() >= 10)
				 filesFile.pop();
			 
			 //2.3 create new fileMeta
			 fileMetaFile = new FileMeta();
			 fileMetaFile.setFileName(mpf.getOriginalFilename());
			 fileMetaFile.setFileSize(mpf.getSize()/1024+" Kb");
			 fileMetaFile.setFileType(mpf.getContentType());
			 
			 try {
				fileMetaFile.setBytes(mpf.getBytes()); //mpf.getBytes()
				// copy file to local disk (make sure the path "e.g. D:/temp/files" exists)
				//FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream("I:/temp/files/"+mpf.getOriginalFilename()));
				FileUploadHelper helper = new FileUploadHelper(); 
				String result = helper.upload(mpf, fileMetaFile);
				logger.debug("Result for upload File:" + result);
				fileMetaFile.setUrl(result);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 //2.4 add to files
			 filesFile.clear();
			 filesFile.add(fileMetaFile);
		 }
		// result will be like this
		// [{"fileName":"app_engine-85x77.png","fileSize":"8 Kb","fileType":"image/png"},...]
		return filesFile;
	}
	
	
	@RequestMapping(value="/uploadVideos", method = RequestMethod.POST)
	public @ResponseBody LinkedList<FileMeta> uploadVideos(MultipartHttpServletRequest request, HttpServletResponse response) {
 
		//1. build an iterator
		 Iterator<String> itr =  request.getFileNames();
		 MultipartFile mpf = null;

		 //2. get each file
		 while(itr.hasNext()){
			 
			 //2.1 get next MultipartFile
			 mpf = request.getFile(itr.next()); 

			 //2.2 if files > 10 remove the first from the list
			 if(filesVideo.size() >= 10)
				 filesVideo.pop();
			 
			 //2.3 create new fileMeta
			 fileMetaVideo = new FileMeta();
			 fileMetaVideo.setFileName(mpf.getOriginalFilename());
			 fileMetaVideo.setFileSize(mpf.getSize()/1024+" Kb");
			 fileMetaVideo.setFileType(mpf.getContentType());
			 
			 try {
				 fileMetaVideo.setBytes(mpf.getBytes()); //mpf.getBytes()
				// copy file to local disk (make sure the path "e.g. D:/temp/files" exists)
				//FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream("I:/temp/files/"+mpf.getOriginalFilename()));
				FileUploadHelper helper = new FileUploadHelper(); 
				String result = helper.upload(mpf, fileMetaVideo);
				logger.debug("Result for upload Video:" + result);
				fileMetaVideo.setUrl(result);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 //2.4 add to files
			 filesVideo.clear();
			 filesVideo.add(fileMetaVideo);
		 }
		// result will be like this
		// [{"fileName":"app_engine-85x77.png","fileSize":"8 Kb","fileType":"image/png"},...]
		return filesVideo;
	}
	
	
	@RequestMapping(value = "/get/{value}", method = RequestMethod.GET)
	 public void get(HttpServletResponse response,@PathVariable String value){
		/*
		 FileMeta getFile = files.get(Integer.parseInt(value));
		 try {		
			 	response.setContentType(getFile.getFileType());
			 	response.setHeader("Content-disposition", "attachment; filename=\""+getFile.getFileName()+"\"");
		        FileCopyUtils.copy(getFile.getBytes(), response.getOutputStream());
		 }catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		 }*/
	 }
 
}

