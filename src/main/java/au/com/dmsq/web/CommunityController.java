
package au.com.dmsq.web;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import au.com.dmsq.dao.AccountDao;
import au.com.dmsq.model.AccessDTO;
import au.com.dmsq.model.AccessDenyDTO;
import au.com.dmsq.model.AccessPendingDTO;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.AccountLayerAccessService;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.CategoryService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.FileService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.MailService;
import au.com.dmsq.service.RoleService;


@Controller
public class CommunityController extends AbstractController{
	
	private static final Logger logger = LoggerFactory.getLogger(CommunityController.class);

	@Autowired LayerService communityService;
	@Autowired AccountService accountService;
	@Autowired CategoryService categoryService;
	@Autowired EventService eventService;
	@Autowired ActivityService activityService;
	@Autowired FileService fileService;
	@Autowired MailService mailService;
	@Autowired BannerService bannerService; 
	@Autowired AccountLayerAccessService alasService;
	@Autowired RoleService roleService;
	@Autowired ErrorsController errorsController;
	
	
	@Transactional
	@RequestMapping(value="/admin/communities", method= RequestMethod.GET)
	public ModelAndView communities() throws Exception {
		
		logger.debug("------------------------------ Admin List Communities ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		//My code
		String accounts123 = accountService.getConnections(accounts.get(0));
		logger.debug("-----------Result........."+ accounts123);
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		Integer max = 20;
		Boolean isManager = (acct.getRole().getId() == 1 || acct.getRole().getId() == 2); 
		List<Layer> layers = new ArrayList<Layer>();
		
		Integer initial=0;
			System.out.println("Controller count: " +  initial);
		Integer prev=1;
		Integer next=2;
		Integer hmany = (layers.size()/max)+1;
		
		if(isManager)
			layers = communityService.layersIfManager(acct.getId());
		else 	
			layers = communityService.pagedCommunities(initial,max, acct.getId());
		
		Set<Layer> layersS = new HashSet<Layer>();
		for(Layer l:layers){
			if(!l.getDeleted())
			layersS.add(l);
		}
		//List<Layer> pagedComms= communityService.pagedCommunities(initial,max, acct.getId()); //Repair
		
		mav.addObject("myCommunityList", layersS);
		mav.addObject("prev", prev);
		mav.addObject("next", next);
		mav.addObject("hmany", hmany);
		mav.addObject("curr", 1);
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		mav.addObject("hmany", hmany);
		mav.addObject("numbers", numbers);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/community/communities"); // jsp
		return mav; 
	}
	
	
	
	@Transactional
	@RequestMapping(value="/admin/communitiesHome", method= RequestMethod.GET)
	public ModelAndView communitiesHome() throws Exception {
		
		logger.debug("------------------------------ Admin List Communities ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		Integer max = 20;
		Boolean isManager = (acct.getRole().getId() == 1 || acct.getRole().getId() == 2); 
		List<Layer> layers = new ArrayList<Layer>();
		
		Integer initial=( layers.size()-max);
			//System.out.println("Controller count: " +  initial);
		Integer prev=1;
		Integer next=2;
		Integer hmany = (layers.size()/max)+1;
		
		if(isManager)
			layers = communityService.layersIfManager(acct.getId());
		else 	
			layers = communityService.pagedCommunities(initial,max, acct.getId());
		
		//List<Layer> pagedComms= communityService.pagedCommunities(initial,max, acct.getId()); //Repair
		
		mav.addObject("myCommunityList", layers);
		mav.addObject("prev", prev);
		mav.addObject("next", next);
		mav.addObject("hmany", hmany);
		mav.addObject("curr", 1);
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		mav.addObject("hmany", hmany);
		mav.addObject("numbers", numbers);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/community/communitiesHome"); // jsp
		return mav; 
	}
	
	
	
	
	
	@RequestMapping(value="/admin/communities/{curr}")
	public ModelAndView pagedCommunities(@PathVariable(value="curr") Integer curr) throws Exception {
		
		logger.debug("------------------------------Admin - List Paged Communities ------------------------------");
		ModelAndView mav = new ModelAndView(); 
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		Integer max = 20;
		Integer next = 0;
		Integer prev = 0;
		Integer initial= 0;
		
		Boolean isManager = communityService.testIfManager(acct.getId());
		List<Layer> layers = new ArrayList<Layer>();
		
		if(isManager)
			layers = communityService.layersIfManager(acct.getId());
		else 	
			layers = communityService.pagedCommunities(initial,max, acct.getId());
		
		Integer hmany = (layers.size()/max)+1;
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		
		if(curr==1||curr<1){
			initial=( layers.size()-max);
			prev=1;
			next=2;
			List<Layer> pagedComms= communityService.pagedCommunities(initial,max, acct.getId());
			mav.addObject("myCommunityList", pagedComms);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
			mav.addObject("acct", getCurrentAccount());
			
		}
		else if(curr>1 && curr < hmany){
			initial= (communityService.count())-(curr*max);
			prev=curr-1;
			next=curr+1;
			List<Layer> pagedComms= communityService.pagedCommunities(initial, max, acct.getId());
			mav.addObject("myCommunityList", pagedComms);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr==hmany){
			initial= 0;
			prev=curr-1;
			next=curr;
			List<Layer> pagedComms= communityService.pagedCommunities(initial,layers.size()%max, acct.getId());
			mav.addObject("myCommunityList", pagedComms);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		
		mav.addObject("hmany", hmany);
		mav.addObject("curr", curr);
		mav.addObject("numbers", numbers);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/community/communities"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/communities", method= RequestMethod.GET)
	public ModelAndView communitiesMgr() {
		
		logger.debug("------------------------------ Manager List Communities ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		List<Layer> listcommunity = communityService.getByOwnerId(acct.getId());
		
		mav.setViewName("/manager/community/communities"); // jsp
		mav.addObject("myCommunityList", listcommunity);
		mav.addObject("acct", getCurrentAccount());
		
		return mav; 
	}
	
	
		
	@RequestMapping(value="/admin/community/{id}", method= RequestMethod.GET)
	public ModelAndView getCommunity(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get Single Community ------------------------------");
		
		
		ModelAndView mav = new ModelAndView(); 
		Layer getBannersId = communityService.findwithBanners(id);
		Layer comm = communityService.findByIdWithParentChildCategories(id);
		Layer layerActivities = communityService.findByIdWithActivities(id);
		Set<Banner> bannerList = new HashSet<Banner>();
		for (Banner ids: getBannersId.getBanners()){
			bannerList.add(bannerService.get(ids.getId()));
		}
		
		
		mav.addObject("comm", comm);
		mav.addObject("Banners", bannerList);
		//mav.addObject("layer", getBanners);
		List<Layer> parent= communityService.getNonDeleted();
		mav.addObject("listOfCommunities", parent);
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/community/community"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/community/{id}", method= RequestMethod.GET)
	public ModelAndView getCommunityMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Get Single Community ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/manager/community/community"); //jsp
		
		Layer comm = communityService.findByIdWithRelations(id);
		mav.addObject("comm", comm);
		List<Layer> parent= communityService.getNonDeleted();
		mav.addObject("listOfCommunities", parent);
		mav.addObject("acct", getCurrentAccount());
		
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/communities/category/{id}", method= RequestMethod.GET)
	public ModelAndView getCommunitybyCategory(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get Community by Category :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/admin/community/communities"); // jsp
		Category category = categoryService.getCategoryWithCommunity(""+id);
		for(Layer l: category.layers){
			l.setNumberActivities(communityService.countActivities(l.getId().toString()));
		}
		
		if (category != null)
		{
			mav.addObject("myCommunityList", category.getLayers());
		}
		
		return mav; 
	}

	
	@RequestMapping(value="/manager/communities/category/{id}", method= RequestMethod.GET)
	public ModelAndView getCommunitybyCategoryMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Get Community by Category :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/manager/community/communities"); // jsp
	
		Category category = categoryService.findById(id);
		if (category != null)
		{
			mav.addObject("myCommunityList", category.getLayers());
		}else{
			mav.addObject("myCommunityList", null);
		}
		
		return mav; 
		
	}
	
	
	@RequestMapping(value="/admin/users/community/{id}", method= RequestMethod.GET)
	public ModelAndView getUsersByCommunity(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get Users per Community :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/admin/account/accountsTo"); // jsp
			
		Layer comm = communityService.findById(id);
		if (comm != null)
		{
			mav.addObject("myList", comm.getAccounts_1());
			mav.addObject("myCommId", id);
			mav.addObject("acct", getCurrentAccount());
			
			mav.addObject("myComm", comm);
		}else{	
			mav.addObject("myList", null);
			mav.addObject("acct", getCurrentAccount());
			
		}
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/users/community/{id}", method= RequestMethod.GET)
	public ModelAndView getUsersByCommunityMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Get Users per Community :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/manager/account/accountsTo"); // jsp
			
		Layer comm = communityService.findByIdWithRelations(id);
		if (comm != null)
		{
			mav.addObject("myList", comm.getAccounts_1());
			mav.addObject("myCommId", id);
			mav.addObject("myComm", comm);
			mav.addObject("acct", getCurrentAccount());
			
		}else{	
			mav.addObject("myList", null);
			mav.addObject("acct", getCurrentAccount());
			
		}
		return mav; 
	}
	
	@RequestMapping(value="/admin/events/community/{id}/*", method= RequestMethod.GET)
	public ModelAndView communityDashboardById(@PathVariable(value="id") Integer id) throws IllegalAccessException, ClassNotFoundException, Exception {
		
		logger.debug("------------------------------ Admin - CommunityDashboard :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Layer comm = communityService.findLayerWithActivitiesAndAccess(id);   // Improve this call
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		if (comm == null)
			comm = new Layer();
		//Get Json String for charts
		System.out.println("Community id is: "+ id);
		String jsonNewUsers = accountService.getConnectionOfParticularCommunity(id);
		jsonNewUsers=jsonNewUsers+"]";
		if(jsonNewUsers.equals("[["+"\"year\""+", "+"\"\""+"]]")){
			 jsonNewUsers ="[["+"\"year\""+", "+"\"12\""+"],["+"\"Jan\""+",0],["+"\"Oct\""+",0],["+"\"Feb\""+",0],["+"\"Mar\""+",0],["+"\"Apr\""+",0],["+"\"May\""+",0],["+"\"Jun\""+",0],["+"\"Jul\""+",0],["+"\"Aug\""+",0],["+"\"Sep\""+",0]]";
		}
		System.out.println("Json String on community controller: "+ jsonNewUsers);
		
		List<Activity> acts = activityService.findByLayerId(id);
		Set<Activity> activities = new HashSet<>();
		for(Activity e:acts){
			activities.add(e);
		}
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		
		Set<Account> layersUsers = new HashSet<Account>();
		Boolean hasAccess = false;  								//To control the access of the user into the detail of a community
		List<AccessDTO> grantedDTO = new ArrayList<>();
		List<AccessDenyDTO> denyDTO = new ArrayList<>();
		List<AccessPendingDTO> pendingDTO = new ArrayList<>();

		try {
			layersUsers.addAll(comm.getAccounts_1());
		}catch(Exception e){
			logger.error("Community Controller empty 465");
		}
		
		if(id!=null && isAdmin(getCurrentAccount())){
			mav.setViewName("/admin/event/CommunityDashboard"); 
			//alasService.getById(id);
			Date today=new Date();
			List<String> managerNames=new ArrayList<>();
			List<Account> managers =new ArrayList<>();
			List<Activity> recommendedActivites=new ArrayList<>();
			Set<Activity> pastEvents=new HashSet<>();
			Set<Activity> futureEvents=new HashSet<>();
			List<Activity> infoPages=new ArrayList<>();
			List<Activity> surveys=new ArrayList<>();
			List<Account> connectedUsers=new ArrayList<>();
			List<Account> grantedAccounts=new ArrayList<>();
			List<Account> revokedAccounts=new ArrayList<>();
			List<Account> pendingAccounts=new ArrayList<>();
			List<String> grant = new ArrayList<String>();
			List<String> deny = new ArrayList<String>();
			List<AccountLayerAccess> acctCode=new ArrayList<>();
			
			/*
			logger.debug("community id -----"+ id);
			logger.debug("community object -----"+ comm);
			logger.debug("community object type-----"+ comm.getType());
			*/
		
	
			if(comm != null && comm.getType() != null && comm.getType().equals("gated"))
			{
				for(AccountLayerAccess ala : comm.getAccountLayerAccesses())
				{
					if(ala.getStatus().equals("requested"))
					{
						pendingAccounts.add(ala.getAccount());
						pendingDTO.add(new AccessPendingDTO(ala.getAccount(), ala.getDenyAccessCode(), ala.getGrantAccessCode()));
					}
					else if(ala.getStatus().equals("granted"))
					{
						grantedAccounts.add(ala.getAccount());
						grantedDTO.add(new AccessDTO(ala.getAccount(), ala.getDenyAccessCode()));
				}
					else if(ala.getStatus().equals("denied"))
					{
						revokedAccounts.add(ala.getAccount());
						denyDTO.add(new AccessDenyDTO(ala.getAccount(), ala.getGrantAccessCode()));
					
					}
				}
			}else{
				try{
				connectedUsers.addAll(comm.getAccounts_1());
				}catch(Exception e){
					logger.error("Exception Comm Controller 523");
				}
			}
			
			System.out.println(comm);
			System.out.println(" Manager count "+ comm.getAccounts().size());
			
			for(Account mAccount:comm.getAccounts()){
				managerNames.add(mAccount.getName());
				managers.add(mAccount);
			}
			
			Integer totalC = 0;
			Integer totalP = 0;
			for(Activity ac : activities){
				if(ac.getActivityType().equals("event")){
					if(ac.getRecommended()){
						recommendedActivites.add(ac);
					}
					if(ac.getEvent()!=null){
						Date date = new Date();
						totalC = activityService.pagedActsPages("event", date.getTime(), id);
						totalP = activityService.pagedActsPagesPast("event", date.getTime(), id);
					}
				}if(ac.getActivityType().equals("survey") && ac.getDeleted().equals(false)){
					surveys.add(ac);
				}if(ac.getActivityType().equals("info") && ac.getDeleted().equals(false)){
					infoPages.add(ac);
				}
			}
			int countSurveys = 0;
			for(int i = 0;i<surveys.size();i++){
				countSurveys++;
			}
			//logger.debug("No of Parent Layers :"+ comm.getLayersForParentLayerId().size());
			logger.debug("No of Child Layers :"+ comm.getLayersForChildLayerId().size());
			logger.debug("No of recommended Layers :"+ recommendedActivites.size());
			
			List<Layer> accessLayers=layerService.getByOwnerId(getCurrentAccount().getId());
		
			Set<Account> accessAccounts=new TreeSet<>();
			for(Layer l: accessLayers){
				try{
				accessAccounts.addAll(l.getAccounts_1());
				}catch(Exception e){
					logger.error("Comm cntroller 591");
				}
			}
			if(comm.getType() != null && comm.getType().equals("gated")){
				accessAccounts.removeAll(grantedAccounts);
				accessAccounts.removeAll(pendingAccounts);
				accessAccounts.removeAll(revokedAccounts);
			}else{
				accessAccounts.removeAll(comm.getAccounts_1());
			}
			//	List<AccountLayerAccess> alaa = alasService.getCurrent();
			mav.addObject("addUserManagers",layersUsers);
			mav.addObject("NewUsersJsonString", jsonNewUsers);
			mav.addObject("inviteUsers",accessAccounts);
			mav.addObject("myList", activities);
			mav.addObject("myCommId", id);
			mav.addObject("noOfSurveys", countSurveys);
			mav.addObject("ConnectedUsers",connectedUsers);
			mav.addObject("GrantedUsers",grantedAccounts);
			mav.addObject("PendingUsers",pendingAccounts);
			mav.addObject("DeniedUsers",revokedAccounts);
			mav.addObject("PastEvents",totalP);
			mav.addObject("FutureEvents",totalC);
			mav.addObject("Informations",infoPages);
			mav.addObject("myComm", comm);
			mav.addObject("managers",managers);
			mav.addObject("Managers",managerNames);
			mav.addObject("SurveyCount",surveys);
			mav.addObject("subcommunities",comm.getLayersForChildLayerId());
			mav.addObject("recommendedActs",recommendedActivites);
			mav.addObject("grantedDTO",grantedDTO);
			mav.addObject("denyDTO", denyDTO);
			mav.addObject("pendingDTO", pendingDTO);
			mav.addObject("acct", getCurrentAccount());
			
			System.out.println("Connected users= " +comm.getAccounts_1().size());
		}else{  								//This value comes if the user does not have mgr privileges of the community or the community does not exist
			mav.setViewName("/403"); 
			mav.addObject("acct", getCurrentAccount());
			
		}
		//mav.setViewName("/admin/event/community/"+comm.getId()+"/"+comm.getName()); 
		return mav; 
	}
	

	@RequestMapping(value = "/admin/invite/request", method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public @ResponseBody String inviteUsers (@RequestBody Object command, HttpSession session) {
		logger.debug("----------Connect/Disconnect From Community -------------");
		
		Account currentUser=getCurrentAccount();		
		if(currentUser!=null)
		{
			logger.debug("----------Current User is Found -------------");
			JSONObject obj = new JSONObject(command.toString().replace("=", ":"));
			  if(obj.has("CommunityID") && !obj.isNull("CommunityID"))
			  {
				  int commID=obj.getInt("CommunityID");
				  Layer thisLayer=layerService.findById(commID);
				  Set<Account> invAccounts=new TreeSet<>();
				  logger.debug("----------Parameters are Read -------------");
				  if(thisLayer!=null )
				  {
					  logger.debug("----------Layer is found-------------");
					  if(obj.has("users") && !obj.isNull("users"))
					  {
						  logger.debug("----------JSon has Users -------------");
						  JSONArray userListJson = obj.getJSONArray("users");
						  if(userListJson!=null && userListJson.length()>0)
						  {
							  logger.debug("----------userListJson is not null -------------");
								for (int i = 0; i < userListJson.length(); ++i) 
								{
									logger.debug("----------Current User ID -------------" + userListJson.getInt(i));
									invAccounts.add(accountService.findById(userListJson.getInt(i)));
								}
								if(invAccounts.size()>0)
								{
									logger.debug("----------invited Users Count is -------------"+ invAccounts.size());
								
									  if(thisLayer.getType().equals("gated"))
									  {
										  logger.debug("----------Layer is Gated-------------");
										  	
											  logger.debug("----------Layer Connection Activities-------------");
											  for(Account iAccount: invAccounts)
											  {
												  if(addAccountLayerAccess(thisLayer,iAccount))
												  {
													  return "Success--Your request has been submitted successfully";
												  }
											  }
									  }
									  else
									  {
										  logger.debug("----------Layer is non- Gated-------------");
										  for(Account iAccount: invAccounts)
										  {
											 Account iAccountConnections= accountService.getbyIdWithLayers(iAccount.getId());
											 logger.debug("----------Layer Connection Activities-------------");
											 Set<Layer> currentConnectedLayers= iAccountConnections.getLayers_1();
											 logger.debug("----------account total connections before adding---"+ iAccountConnections.getLayers_1().size());
											 int initialcount=iAccountConnections.getLayers_1().size();
											 currentConnectedLayers.add(thisLayer);
											 iAccountConnections.setLayers_1(currentConnectedLayers);
											 logger.debug("----------account total connections before save---"+ iAccountConnections.getLayers_1().size());
											 logger.debug("----------result of account save---"+accountService.save(iAccountConnections));
											 Account checkUserAccess= accountService.getbyIdWithLayers(iAccount.getId());
											 logger.debug("----------account total connections after save---"+ checkUserAccess.getLayers_1().size());
											 int finalcount=checkUserAccess.getLayers_1().size();
											 if(finalcount==(initialcount+1))
											 {
												logger.debug("----------account modifications saved successfully-------------");
												nonGatedLayerSubscriptionConfirmationMail(thisLayer,iAccountConnections,"connect");
												return "Success--You have been connected successfully";
											 }
											 else
											 {
												 logger.debug("----------account service failed to save your changes-------------");
												 return "Error--Your changes are not saved. Contact Administrator.";
											 }
										  }
									  }
								}
						  }
					  }
				  }
				  else
				  {
					  return "Error--Invalid Arguments";
				  }
			  }
			  else
			  {
				  return "Error--Missing Arguments"; 
			  }
		}
		return "Error--Session Not Found";
	}
	
	
	@RequestMapping(value = "/admin/addMgrs/request", method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public @ResponseBody String addManagers (@RequestBody Object command, HttpSession session) {
		logger.debug("---------- ADD USERS AS MANAGERS TO THE SELECTED COMMUNITY-------------");
		
		Account currentUser=getCurrentAccount();		
		if(currentUser!=null){
			JSONObject obj = new JSONObject(command.toString().replace("=", ":"));
			if(obj.has("CommunityID") && !obj.isNull("CommunityID")){
				int commID=obj.getInt("CommunityID");
				Layer thisLayer=layerService.findById(commID);
				Set<Account> invAccounts=new TreeSet<>();
				if(thisLayer!=null ){
				  if(obj.has("users") && !obj.isNull("users")){
					  JSONArray userListJson = obj.getJSONArray("users");
					  if(userListJson!=null && userListJson.length()>0){
						for (int i = 0; i < userListJson.length(); ++i){
							invAccounts.add(accountService.findById(userListJson.getInt(i)));
						}
						if(invAccounts.size()>0){
							for(Account iAccount: invAccounts){
								 Account iAccountConnections= accountService.getbyIdWithLayersManaged(iAccount.getId());
								 logger.debug("----------Layer Connection Activities-------------");
								 Set<Layer> currentConnectedLayers= iAccountConnections.getLayers();
								 logger.debug("----------account total connections before adding---"+ iAccountConnections.getLayers().size());
								 int initialcount=iAccountConnections.getLayers().size();
								 currentConnectedLayers.add(thisLayer);
								 iAccountConnections.setLayers(currentConnectedLayers);
								 iAccountConnections.setRole(roleService.get(1));
								 logger.debug("----------new role---"+ roleService.get(1).getName());
								 logger.debug("----------account new role---"+ iAccountConnections.getRole().getName());
								 logger.debug("----------account total connections before save---"+ iAccountConnections.getLayers().size());
								 logger.debug("----------result of account save---"+accountService.save(iAccountConnections));
								 Account checkUserAccess= accountService.getbyIdWithLayersManaged(iAccount.getId());
								 logger.debug("----------account total connections after save---"+ checkUserAccess.getLayers().size());
								 int finalcount=checkUserAccess.getLayers().size();
								 if(finalcount==(initialcount+1)){
									logger.debug("----------account modifications saved successfully-------------");
									nonGatedLayerSubscriptionConfirmationMail(thisLayer,iAccountConnections,"connect");
									return "Success--You have been connected successfully";
								 }
								 else{
									 logger.debug("----------account service failed to save your changes-------------");
									 return "Error--Your changes are not saved. Contact Administrator.";
								 }
							}  
						}
					  }
				  }
				}
				else{
					return "Error--Invalid Arguments";
				}
			}
			else{
				return "Error--Missing Arguments"; 
			}
		}
	return "Error--Session Not Found";
	}
	
	
	@RequestMapping(value = "/admin/delMgrs/request", method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public @ResponseBody String delManagers (@RequestBody Object command, HttpSession session) {
		logger.debug("---------- REMOVE USERS AS MANAGERS -------------");
		
		Account currentUser=getCurrentAccount();		
		if(currentUser!=null){
			JSONObject obj = new JSONObject(command.toString().replace("=", ":"));
			if(obj.has("CommunityID") && !obj.isNull("CommunityID")){
				int commID=obj.getInt("CommunityID");
				Layer thisLayer=layerService.findById(commID);
				Set<Account> invAccounts=new TreeSet<>();
				if(thisLayer!=null ){
					  if(obj.has("users") && !obj.isNull("users")){
						  JSONArray userListJson = obj.getJSONArray("users");
						  if(userListJson!=null && userListJson.length()>0){
								for (int i = 0; i < userListJson.length(); ++i){
									invAccounts.add(accountService.findById(userListJson.getInt(i)));
								}
								if(invAccounts.size()>0){
									for(Account iAccount: invAccounts){
										 Account iAccountConnections= accountService.getbyIdWithLayersManaged(iAccount.getId());
										 logger.debug("----------Layer Connection Activities-------------");
										 Set<Layer> currentConnectedLayers= iAccountConnections.getLayers();
										 Set<Layer> newConnectedLayers= new HashSet<>();
										 Set<Integer> layerIds= new HashSet<>();
										 for(Layer i:currentConnectedLayers){ 
											 layerIds.add(i.getId());}
										 logger.debug("----------account total connections before removing---"+ iAccountConnections.getLayers().size());
										 int initialcount=iAccountConnections.getLayers().size();
										 logger.debug("----------layers managed before removal ---"+ currentConnectedLayers.size());
										 layerIds.remove(thisLayer.getId());
										 for(Integer in:layerIds){
											 newConnectedLayers.add(layerService.findById(in));}
										 logger.debug("----------layers managed after removal ---"+ newConnectedLayers.size());
										 iAccountConnections.setLayers(newConnectedLayers);
										 logger.debug("----------account total connections before save---"+ initialcount);
										 logger.debug("----------result of account save---"+accountService.save(iAccountConnections));
										 Account checkUserAccess= accountService.getbyIdWithLayersManaged(iAccount.getId());
										 logger.debug("----------account total connections after save---"+ checkUserAccess.getLayers().size());
										 int finalcount=checkUserAccess.getLayers().size();
										 if(finalcount==(initialcount-1)){
											logger.debug("----------account modifications saved successfully-------------");
											nonGatedLayerSubscriptionConfirmationMail(thisLayer,iAccountConnections,"connect");
											return "Success--You have been removed successfully";
										 }
										 else{
											 logger.debug("----------account service failed to save your changes-------------");
											 return "Error--Your changes are not saved. Contact Administrator.";
										 }
									}  
								}
						  }
					  }
				}
				else{
					return "Error--Invalid Arguments";
				}
			}
			else{
				return "Error--Missing Arguments"; 
			}
		}
	return "Error--Session Not Found";
	}
	
	
	@RequestMapping(value = "/admin/remComm/request", method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public @ResponseBody String removeCommunity (@RequestBody Object command, HttpSession session) {
		logger.debug("---------- REMOVE A COMMUNITY (HARD DELETE) -------------");
		
		Account currentUser=getCurrentAccount();		
		if(currentUser!=null && currentUser.getRole().getId()==1){
			JSONObject obj = new JSONObject(command.toString().replace("=", ":"));
			if(obj.has("CommunityID") && !obj.isNull("CommunityID")){
				Integer commID=obj.getInt("CommunityID");

				if(commID!=null ){
					Layer newLayer = communityService.get(commID);	
					
					Set<AccountLayerAccess> alas = new HashSet<>();
					newLayer.setAccountLayerAccesses(alas);
					Set<Account> acts = new HashSet<>();
					newLayer.setAccounts_1(acts);
					Set<Activity> activities = new HashSet<>();
					newLayer.setActivities_1(activities);
					Set<Layer> childs = new HashSet<>();
					newLayer.setLayersForChildLayerId(childs);
					Set<Layer> parent = new HashSet<>();
					newLayer.setLayersForParentLayerId(parent);
					
					Boolean resultS= communityService.save(newLayer);
					Boolean resultR= communityService.deleteById(newLayer.getId());
					
					if(resultR){
						logger.debug("----------community removed successfully-------------");
						return "Success--the community has been removed successfully";
					}
					else{
						logger.debug("----------community remove service failed to save your changes-------------");
						return "Error--the community removal was not performed. Contact Administrator.";
					}
				}
			}
			else{
				return "Error--Missing Arguments"; 
			}
		}
		return "Error--Session Not Found";
	}
	
	
	@RequestMapping(value="/admin/events/communitydetails/{id}", method= RequestMethod.GET)
	public ModelAndView getEventsByCommunityDetails(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get details of Community :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/admin/event/eventsTo"); // jsp
			
		Layer comm = communityService.findByIdWithActivities(id);
		
		if (comm != null)
		{
			mav.addObject("myList", comm.getActivities());
			mav.addObject("myCommId", id);
			mav.addObject("myComm", comm);
		
		}else{	
			mav.addObject("myList", null);
		}
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/events/community/{id}", method= RequestMethod.GET)
	public ModelAndView getEventsByCommunityMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Get Events per Community :"+ id +"------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		mav.setViewName("/manager/event/eventsTo"); // jsp
			
		Layer comm = communityService.findByIdWithRelations(id);
		
		
		if (comm != null)
		{
			mav.addObject("myList", comm.getActivities());
			mav.addObject("myCommId", id);
			mav.addObject("myComm", comm);
		}else{	
			mav.addObject("myList", null);
		}
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/community/addUsers/{id}", method= RequestMethod.GET)
	public ModelAndView addUsersToCommunity(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Add Users to a Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		Layer layer =  communityService.get(id);
		
		mav.addObject("comm", layer);	
		mav.addObject("listOfUsers", accountService.getNonDeleted());	
		mav.setViewName("/admin/account/addAccountsToCommunities"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/community/addUsers/{id}", method= RequestMethod.GET)
	public ModelAndView addUsersToCommunityMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Add Users to a Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		Layer layer =  communityService.get(id);
		
		mav.addObject("comm", layer);	
		mav.addObject("listOfUsers", accountService.getNonDeleted());	
		mav.setViewName("/manager/account/addAccountsToCommunities"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/community/addEvents/{id}", method= RequestMethod.GET)
	public ModelAndView addEventsToCommunity(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Add Events to a Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		Layer layer =  communityService.findByIdWithActivities(id);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		Set<Layer> layers = acct.getLayers();
		List<Integer> layersIds = new ArrayList<Integer>();
		
		for(Layer community: layers){
			layersIds.add(community.getId());
		}
		
		Set<Activity> events = activityService.getEventsByCommIds(layersIds);
		List<Activity> myEvents = activityService.getByOwnerId(acct.getId());
		for(Activity act:myEvents){
			if(!events.contains(act)){
				events.add(act);
			}
		}
		mav.addObject("acct", getCurrentAccount());
		mav.addObject("comm", layer);	
		mav.addObject("listOfEvents", events);	
		mav.setViewName("/admin/event/addEventsToCommunities"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/community/copyEvents/{id}", method= RequestMethod.GET)
	public ModelAndView copyEventsToCommunity(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Copy Events to a Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		Layer layer =  communityService.findByIdWithRelations(id);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		Set<Layer> layers = acct.getLayers();
		List<Integer> layersIds = new ArrayList<Integer>();
		
		for(Layer community: layers){
			layersIds.add(community.getId());
		}
		
		Set<Activity> events = activityService.getEventsByCommIds(layersIds);
		mav.addObject("comm", layer);	
		mav.addObject("comms", layers);
		mav.addObject("listOfEvents", events);	
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/admin/event/copyEventsToCommunities"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/community/moveEvents/{id}", method= RequestMethod.GET)
	public ModelAndView moveEventsToCommunity(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Move Events to a Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		Layer layer =  communityService.findByIdWithRelations(id);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		Set<Layer> layers = acct.getLayers();
		List<Integer> layersIds = new ArrayList<Integer>();
		
		for(Layer community: layers){
			layersIds.add(community.getId());
		}
		
		Set<Activity> events = activityService.getEventsByCommIds(layersIds);
		
		mav.addObject("comm", layer);
		mav.addObject("comm0Id", id);
		mav.addObject("acct", getCurrentAccount());
		
		mav.addObject("comms", layers);
		mav.addObject("listOfEvents", events);	
		mav.setViewName("/admin/event/moveEventsToCommunities"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/community/addEvents/{id}", method= RequestMethod.GET)
	public ModelAndView addEventsToCommunityMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Add Events to a Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		Layer layer =  communityService.findByIdWithActivities(id);
		
		mav.addObject("comm", layer);	
		mav.addObject("listOfEvents", activityService.getByOwnerId(layer.getAccount().getId()));	
		mav.addObject("acct", getCurrentAccount());
		
		mav.setViewName("/manager/event/addEventsToCommunities"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/community/save", method= RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("community") @Valid Layer community, BindingResult result, SessionStatus status, 
			@RequestParam(value="parent",           defaultValue="", 		required=false) String[] ids, 
			@RequestParam(value="titleBanner[]",    defaultValue="", 		required=false) String[] titleBanner,	
			@RequestParam(value="subtitleBanner[]", defaultValue="", 		required=false) String[] subtitleBanner,	
			@RequestParam(value="bannerUrl[]", 		defaultValue="", 		required=false) String[] urlBanner, 		
			@RequestParam(value="bannerId[]", 		defaultValue="", 		required=false) Integer[]bannerId,			
			@RequestParam(value="pCat", 			defaultValue="", 		required=false) String[] pCat
			)throws Exception{
		
		logger.debug("------------------------------ Admin - Save Community ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		try{
			boolean isNew = true;
			if (community.getId()!=null) isNew=false;
			
			System.out.println("Is NEW?: "+isNew);
			
			if (result.hasErrors()) {
				//if validator failed
				mav = new ModelAndView(); 
				
				List<Layer> parent= communityService.getNonDeleted();
				mav.addObject("listOfCommunities", parent);
				mav.addObject("listOfOwners", accountService.getNonDeleted());	
				mav.addObject("listOfCategories", categoryService.getNonDeleted());
				mav.addObject("acct", getCurrentAccount());
				
				mav.setViewName("/admin/community/newCommunity"); //jsp
				return mav; 
				
			} else {
				Layer newComm = new Layer();
				
				if(!isNew){
					Layer oldComm = communityService.findByIdWithRequiredRelations(community.getId());
					newComm = community;
					newComm.setAccountLayerAccesses(oldComm.getAccountLayerAccesses());
					newComm.setAccounts_1(oldComm.getAccounts_1());
					newComm.setActivities(oldComm.getActivities());
					newComm.setActivities_1(oldComm.getActivities_1());
					newComm.setLayersForChildLayerId(oldComm.getLayersForChildLayerId());
					//newComm.setMessages(oldComm.getMessages());
					System.out.println("Banners in Comm: "+ newComm.getBanners().size());
					Set <Layer> listParent = new HashSet<Layer>();
					Set<Integer> parentComms = new HashSet<>();
					if(!ids.equals(null)){
						logger.debug("Parent received:" + ids.length);
						for(String i:ids){
							parentComms.add(new Integer(i));
						}
						logger.debug("Parent Registered:" + parentComms.size());
					}
					
					if(!parentComms.equals(null)){
						for(Integer str:parentComms){
							listParent.add(communityService.get(str));
						}
						newComm.setLayersForParentLayerId(listParent);
					}
					
					Set<Integer> newpCat = new HashSet<>();
					for(String s:pCat){
						newpCat.add(new Integer(s));
					}
					
					Set <Category> newParentCat = new HashSet<Category>();
					for(Integer s:newpCat){
						newParentCat.add(categoryService.findById(s));
					}
					newComm.setCategories(newParentCat);
					
					Set <Account> layerMgr = new HashSet<Account>();
					layerMgr.addAll(oldComm.getAccounts());
					if(!layerMgr.contains(accountService.findById(community.getAccount().getId())))
						newComm.setAccounts(layerMgr);
				}else{
					newComm = community;
				
					Set <Layer> listParent = new HashSet<Layer>();
					if(!ids.equals("")){
						for(String str: ids){
							logger.debug("Adding Parent:" + str);
							listParent.add(communityService.get(new Integer(str)));
						}
						newComm.setLayersForParentLayerId(listParent);
					}
					else newComm.setLayersForParentLayerId(listParent);
					
					Set <Category> parentCat = new HashSet<Category>();
					//parentCat.addAll(oldComm.getCategories());
					if(pCat != null){
						for(String pCategory:pCat){
							parentCat.add(categoryService.findById(new Integer(pCategory)));
						}
					}
					newComm.setCategories(parentCat);
					
					Set <Account> layerMgr = new HashSet<Account>();
					layerMgr.add(accountService.findById(community.getAccount().getId()));
					newComm.setAccounts(layerMgr);
				}
				
				if(urlBanner.length!=0){
					Set<Banner> banners = new HashSet<Banner>();
					System.out.println("Banners <<< B >>>");
					System.out.println("Banners detected #: "+ urlBanner.length);
					System.out.println("Titles  detected #: "+ titleBanner.length);
					System.out.println("STitles detected #: "+ subtitleBanner.length);
					System.out.println("Ids     detected #: "+ bannerId.length);
					
					for(int ba=0; ba<urlBanner.length; ba++){
						Banner newBanner = new Banner();
						if(bannerId.length > ba && !bannerId[ba].equals("")){
							if(titleBanner.length==0 && subtitleBanner.length==0){
								newBanner=bannerService.findById(new Integer(bannerId[ba]));
								newBanner.setTitle("");
								newBanner.setSubtitle("");
								System.out.println("Saved Banner If1: "+ bannerService.save(newBanner));
							}else if(titleBanner.length>0 && subtitleBanner.length==0){
								newBanner=bannerService.findById(new Integer(bannerId[ba]));
								newBanner.setTitle(titleBanner[ba]);
								newBanner.setSubtitle("");
								System.out.println("Saved Banner If2: "+ bannerService.save(newBanner));
							}else if(titleBanner.length==0 && subtitleBanner.length>0){
								newBanner=bannerService.findById(new Integer(bannerId[ba]));
								newBanner.setTitle("");
								newBanner.setSubtitle(subtitleBanner[ba]);				  
								System.out.println("Saved Banner If3: "+ bannerService.save(newBanner));
							}else{
								newBanner=bannerService.findById(new Integer(bannerId[ba]));
								newBanner.setTitle(titleBanner[ba]);			
								newBanner.setSubtitle(subtitleBanner[ba]);		  
								System.out.println("Saved Banner If4: "+ bannerService.save(newBanner));
							}
						}
						else{
							if(titleBanner.length>0){
								newBanner.setTitle(titleBanner[ba]);
								newBanner.setSubtitle(subtitleBanner[ba]);	
								newBanner.setImageUrl(urlBanner[ba]);
								System.out.println("Saved Banner If5: "+ bannerService.save(newBanner));
							}else{
								newBanner.setTitle(" ");
								newBanner.setSubtitle(" ");	
								newBanner.setImageUrl(urlBanner[ba]);
								System.out.println("Saved Banner If6: "+ bannerService.save(newBanner));
							}
						}
						banners.add(newBanner);
					}
					System.out.println("Saved Banners: "+ banners.size());
					newComm.setBanners(banners);
				}
				if(urlBanner.length==0){
					Set<Banner> banners = new HashSet<Banner>();
					banners.add(bannerService.findById(3));
					newComm.setBanners(banners);
				}
				
				boolean saveResult = false; 
				if (communityService.save(newComm)) saveResult = true;
				
				mav = communities(); 
				mav.addObject("message", saveResult);
				return mav;
			}
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			mav = errorsController.error500();
			return mav;
		}
	}
	
	
	@RequestMapping(value="/manager/community/save", method= RequestMethod.POST)
	public ModelAndView saveMgr(@ModelAttribute("community") @Valid Layer community, BindingResult result, SessionStatus status, 
			@RequestParam(value="parent", required=false) String [] ids, @RequestParam("icon")int icon ) throws Exception
		{
		
		logger.debug("------------------------------ Manager - Save Community ------------------------------");
		
		if (result.hasErrors()) {
			//if validator failed
			ModelAndView mav = new ModelAndView(); 
			
			Layer layer =  new Layer();
			List<Layer> parent= communityService.getNonDeleted();
			
			java.util.Date date= new java.util.Date();
			System.out.println(new Timestamp(date.getTime()));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			String startDatea = sdf.format(date);
			
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			List<Account> accounts = accountService.findByEmail(user.getUsername());
			
			for(Account str: accounts){
				if(accounts.isEmpty()){
					return mav;
				}
			}
			Account acct = accounts.get(0);
			
			mav.addObject("community", layer);
			mav.addObject("listOfCommunities", parent);
			mav.addObject("listOfOwners", accountService.getNonDeleted());
			//mav.addObject("listOfIcons", communityService.listIcons());
			mav.addObject("currentTS", startDatea);
			mav.addObject("currentUser", acct.getId());
			
			mav.setViewName("/manager/community/newCommunity"); //jsp
			return mav; 
			
		} else {
			boolean saveResult = false; 
			Set <Layer> listParent = new HashSet<Layer>();
			
			for(String str: ids){
				logger.debug("Adding Parent:" + str);
				listParent.add(communityService.findByIdWithRelations(new Integer(str)));
			}
			community.setLayersForParentLayerId(listParent);
			
			//List<IconDTO> icons = communityService.listIcons();
			//Integer iconx = icon;
			//System.out.println("Controller - Save community, iconURL" + iconx); //To check the content received from the new form
			//IconDTO x = icons.get(iconx);
			//community.setIconUrl(x.getIconUrl().toString());
			//System.out.println("Controller - Save community, iconURL: " +x.getIconUrl().toString()); 
			//System.out.println("Controller - Save community, iconURL: " + community.getIconUrl().toString());
			
			if (communityService.save(community)) saveResult = true;
			
			ModelAndView mav = communitiesMgr(); 
			mav.addObject("message", saveResult);
			return mav;
		}
	}
	
	
	@RequestMapping(value="/admin/community/saveUsers/", method= RequestMethod.POST)
	public ModelAndView saveUsers(@ModelAttribute("comm") Layer comm, @RequestParam("xusers") String [] users) throws Exception {
		
		logger.debug("---------------------- Admin - Save Added Users to Community---------------------");
		ModelAndView mav = new ModelAndView(); 
		
		try{
			Layer communityX = communityService.findByIdWithRelations(comm.getId());
			
				Set<Account> accounts = new HashSet<Account>(); 
				for (String ids: users){
					accounts.add(accountService.get(new Integer(ids)));
					
				}
				communityX.setAccounts_1(accounts);
				mav.addObject("message", communityService.save(communityX));
				mav = communities(); 
				
			return mav;	
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			mav = errorsController.error500();
			return mav;
		}
	}
	
	
	@RequestMapping(value="/manager/community/saveUsers/", method= RequestMethod.POST)
	public ModelAndView saveUsersMgr(@ModelAttribute("comm") Layer comm, @RequestParam("xusers") String [] users) {
		
		logger.debug("---------------------- Manager - Save Added Users to Community---------------------");
		ModelAndView mav = new ModelAndView(); 
		
		Layer communityX = communityService.findByIdWithRelations(comm.getId());
		
			Set<Account> accounts = new HashSet<Account>(); 
			for (String ids: users){
				accounts.add(accountService.get(new Integer(ids)));
				
			}
			communityX.setAccounts_1(accounts);
			mav.addObject("message", communityService.save(communityX));
			mav = communitiesMgr(); 
			
		return mav;	
	}
	
	
	@RequestMapping(value="/admin/community/saveEvents/", method= RequestMethod.POST)
	public ModelAndView saveEvents(@ModelAttribute("comm") Layer comm, 
			@RequestParam("xevents") String [] xevents) throws Exception {
		
		logger.debug("---------------------------- Admin - Save Added Events to Community------");
		
		System.out.println("Controller - Save added # events" + xevents.length); 
		
		ModelAndView mav = new ModelAndView(); 
		try{
			Layer communityX = communityService.findByIdWithRelations(comm.getId());
				Set<Activity> events = new HashSet<Activity>();
				if(xevents.length>0){
					for (String ids: xevents){
						events.add(activityService.findByIdWithRelations(new Integer(ids)));
					}
				}
			communityX.setActivities(events);
			mav.addObject("message", communityService.save(communityX));
			mav = communities(); 
				
			return mav;	
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			mav = errorsController.error500();
			return mav;
		}
	}
	
	
	@RequestMapping(value="/admin/community/saveCEvents/", method= RequestMethod.POST)
	public ModelAndView saveCEvents(@ModelAttribute("comm") Layer comm,
			@RequestParam("xevents") Collection<? extends Integer> xevents) throws Exception {
		
		logger.debug("---------------------------- Admin - Save Copied Events to Community------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Layer communityX = communityService.findByIdWithRelations(comm.getId());
		
			Set<Activity> events = new HashSet<Activity>();
			events.addAll(communityX.getActivities());
			
			Set<Integer> eventsTo = new HashSet<Integer>();
			eventsTo.addAll(xevents);
			
			Integer s=null;
	        for (Activity ids: events){
				for (Iterator<Integer> iter = eventsTo.iterator(); iter.hasNext();){
					s = iter.next();
					int z = (ids.getId().compareTo(s));
					if(z!=0){
						//System.out.println("They are different");
						//System.out.println("ids "+ids.getId());
						//System.out.println("s "+s);
					}else{
						//System.out.println("They are equal");
						//System.out.println("ids "+ids.getId());
						//System.out.println("s "+ s);
					    iter.remove();
						}
					}	
				}
			for (Integer i: eventsTo){
				//System.out.println("ids avents added "+activityService.findById(i).getId());
				events.add(activityService.findById(i));
			}
			
			communityX.setActivities(events);
			mav.addObject("message", communityService.save(communityX));
			mav = communities(); 
			
		return mav;	
	}
	
	
	@RequestMapping(value="/admin/community/saveMEvents/", method= RequestMethod.POST)
	public ModelAndView saveMEvents(@ModelAttribute("comm") Layer comm, 
			@RequestParam("xevents") Collection<? extends Integer> xevents,
			@RequestParam("xeventsNC") Collection<? extends Integer> xeventsNC,
			@RequestParam("comm0Id") Integer comm0Id							) throws Exception {
		
		logger.debug("---------------------------- Admin - Save Moved Events to Community------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Layer communityW = communityService.findByIdWithRelations(comm0Id);
			System.out.println("comm0 ID: "+ comm0Id);
		Set<Activity> evComm0 = new HashSet<Activity>();
			evComm0.addAll(communityW.getActivities());
		
		Layer communityX = communityService.findByIdWithRelations(comm.getId());
			Set<Activity> events = new HashSet<Activity>();
			events.addAll(communityX.getActivities());
			
			Set<Integer> eventsTo = new HashSet<Integer>();
			eventsTo.addAll(xevents);
			
			Integer s=null;
	        for (Activity ids: events){
				for (Iterator<Integer> iter = eventsTo.iterator(); iter.hasNext();){
					s = iter.next();
					int z = (ids.getId().compareTo(s));
					if(z!=0){
						//System.out.println("They are different");
						//System.out.println("ids "+ids.getId());
						//System.out.println("s "+s);
					}else{
						//System.out.println("They are equal");
						//System.out.println("ids "+ids.getId());
						//System.out.println("s "+ s);
					    iter.remove();
						}
					}	
				}
			
			Integer t=null;
	        for (Integer ids: eventsTo){
				for (Iterator<Activity> iter = evComm0.iterator(); iter.hasNext();){
					t = iter.next().getId();
					int z = (ids.compareTo(t));
					if(z!=0){   // They are different"
						
					}else{		// They are equal"
					    iter.remove();
						}
					}	
				}				
			communityW.setActivities(evComm0);
	        
			for (Integer i: eventsTo){
				//System.out.println("ids avents added "+activityService.findById(i).getId());
				events.add(activityService.findById(i));
			}
			communityX.setActivities(events);
			
			mav.addObject("message", communityService.save(communityX));
			mav.addObject("message", communityService.save(communityW));
			mav = communities(); 
			
		return mav;	
	}
	
	
	@RequestMapping(value="/manager/community/saveEvents/", method= RequestMethod.POST)
	public ModelAndView saveEventsMgr(@ModelAttribute("comm") Layer comm, 
			@RequestParam("xevents") String [] xevents) {
		
		logger.debug("---------------------------- Manager - Save Events Added to Community ------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Layer communityX = communityService.findByIdWithRelations(comm.getId());
		
			Set<Activity> events = new HashSet<Activity>(); 
			for (String ids: xevents){
				events.add(activityService.get(new Integer(ids)));
			}
			communityX.setActivities(events);
			mav.addObject("message", communityService.save(communityX));
			mav = communitiesMgr(); 
			
		return mav;	
	}
	
	
	@RequestMapping(value="/admin/community/delete/{id}")
	public String delete(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Delete Single Community ------------------------------");
		try{
			Layer comm2 = communityService.findById(id); 
			comm2.setDeleted(true);
			communityService.save(comm2);
			
			return "redirect:/admin/communities/";
			
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			return "redirect:/admin/errors/500/";
		}
	}
	
	
	@RequestMapping(value="/manager/community/delete/{id}")
	public String deleteMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Delete Single Community ------------------------------");

		Layer comm2 = communityService.findById(id); 
		comm2.setDeleted(true);
		communityService.save(comm2);
		
		return "redirect:/manager/communities/";
	}
	
	
	@RequestMapping(value="/admin/community/new")
	public ModelAndView newCommunity() throws Exception {
		
		logger.debug("------------------------------ Admin - New Community ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Layer layer =  new Layer();
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		if(layers.isEmpty()){
			logger.debug("----- setAccounts.size() ------>>>> "+setAccounts.size());
			setAccounts.add(getCurrentAccount());
			logger.debug("----- setAccounts.size() ------>>>> "+setAccounts.size());
		}
		
		for(Layer community: layers){
			setAccounts.addAll(communityService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		mav.addObject("iUrl", 0);  //Current Index of the iconUrl to send to JSP
		mav.addObject("community", layer);
		mav.addObject("listOfCommunities", layers);
		mav.addObject("listOfOwners", listofManagers);
		mav.addObject("listOfCategories", categoryService.getNonDeleted());
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		
		mav.setViewName("/admin/community/newCommunity"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/community/new")
	public ModelAndView newCommunityMgr() throws Exception {
		
		logger.debug("------------------------------ Manager - New Community ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Layer layer =  new Layer();
		List<Layer> parent= communityService.getNonDeleted();
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		mav.addObject("iUrl", 0); 
		
		mav.addObject("community", layer);
		mav.addObject("listOfCommunities", parent);
		mav.addObject("listOfOwners", accountService.getNonDeleted());
		//mav.addObject("listOfIcons", communityService.listIcons());
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		
		mav.setViewName("/manager/community/newCommunity"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/community/editCommunity/{id}", method= RequestMethod.GET)
	public ModelAndView editCommunity(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Edit Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		
		Layer layer =  communityService.findByIdWithParentChildCategories(id);
		Set<Banner> banners = layer.getBanners();
		mav.addObject("bannerList", banners);
		
		Set<Layer> listLayerTest = layer.getLayersForParentLayerId();
		Set<Layer> listLayerTest2 = layer.getLayersForChildLayerId();
		
		for (Layer l: listLayerTest){
			logger.debug("Name or parent:" + l.getName());
		}
		
		for (Layer l2: listLayerTest2){
			logger.debug("Name of Child:" + l2.getName());
		}
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layers){
			setAccounts.addAll(communityService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		mav.addObject("listOfCommunities", layers);
		mav.addObject("category", layer.getCategories());
		mav.addObject("listOfCategories", categoryService.getNonDeleted());
		mav.addObject("community", layer);
		mav.addObject("listOfOwners",listofManagers);
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		
		mav.setViewName("/admin/community/newCommunity"); //jsp
		
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/community/editCommunity/{id}", method= RequestMethod.GET)
	public ModelAndView editCommunityMgr(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Manager - Edit Community ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		
		Layer layer =  communityService.findByIdWithRelations(id);
		
		Set<Layer> listLayerTest = layer.getLayersForParentLayerId();
		Set<Layer> listLayerTest2 = layer.getLayersForChildLayerId();
		
		for (Layer l: listLayerTest){
			logger.debug("Name or parent:" + l.getName());
		}
		
		for (Layer l2: listLayerTest2){
			logger.debug("Name of Child:" + l2.getName());
		}
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
//		List<IconDTO> icons = communityService.listIcons();
//		Integer icon = null;
//		for(int i=0; i<icons.size(); i++){
//			String xxx = icons.get(i).getIconUrl();
//			if(xxx.equalsIgnoreCase(layer.getIconUrl().toString() ) ){
//				icon = i;
//				i = icons.size();
//			}
//		}
//		mav.addObject("iUrl", icon);  //Current Index of the iconUrl to send to JSP
		//System.out.println("Controller - reset password - icon: " + icon.toString());
		
		mav.addObject("community", layer);	
		List<Layer> parentLayers= communityService.getNonDeleted();
		mav.addObject("listOfCommunities", parentLayers);
		mav.addObject("listOfOwners", accountService.getNonDeleted());
		//mav.addObject("listOfIcons", communityService.listIcons());
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		
		mav.setViewName("/manager/community/newCommunity"); //jsp
		
		return mav; 
	}
	
	
	@ModelAttribute ("parent")
	public List<Layer> setParent()
	{
		return communityService.getNonDeleted();
	}
	
	
	@RequestMapping(value = "/admin/community/invite", method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public @ResponseBody String messageRequestHandler (@RequestBody Object command, HttpSession session) {
		logger.debug("----------Connect/Disconnect From Community -------------");
		//logger.debug("Current Account ID is"+ getCurrentAccount());
		
		Account currentUser=getCurrentAccount();
		Account currentUserwithConnections= accountService.getbyIdWithLayers(currentUser.getId());
		
		if(currentUser!=null)
		{
			logger.debug("----------Current User is Found -------------");
			JSONObject obj = new JSONObject(command.toString().replace("=", ":"));
			  if(obj.has("CommunityID") && !obj.isNull("CommunityID"))
			  {
				  int commID=obj.getInt("CommunityID");
				  String action=obj.getString("message");
				  Layer thisLayer=layerService.findById(commID);
				  logger.debug("----------Parameters are Read -------------");
				  if(thisLayer!=null )
				  {
					  if(thisLayer.getType().equals("gated"))
					  {
						  logger.debug("----------Layer is Gated-------------");
						  if(action.equals("connect"))
						  {
							  logger.debug("----------Layer Connection Activities-------------");
							  if(addAccountLayerAccess(thisLayer,currentUser))
							  {
								  return "Success--Your request has been submitted successfully";
							  }
						  }
						  else
						  {
							  logger.debug("----------Layer Disconnection Activities-------------");
							 for(AccountLayerAccess alaTemp:currentUserwithConnections.getAccountLayerAccesses())
							 {
								 
								 logger.debug("--Iterated Layer Id ="+ alaTemp.getLayer().getId()+" --Current Layer id --"+ thisLayer.getId() );
								 if(alaTemp.getLayer().getId().equals(thisLayer.getId()))
								 {
									 logger.debug("----------Setting Denied Status-------------");
									 alaTemp.setStatus("denied");
									 
									 logger.debug("----------Start Saving Status-------------");
									 alasService.save(alaTemp);
									 if(true)
									 {
										 logger.debug("----------Saved Successfully-------------");
										 return "Success--Your Access has been revoked successfully";
									 }
								 }
							 }
						  }
					  }
					  else
					  {
						  logger.debug("----------Layer is non- Gated-------------");
						  if(action.equals("connect"))
						  {
							  logger.debug("----------Layer Connection Activities-------------");
							 Set<Layer> currentConnectedLayers= currentUserwithConnections.getLayers_1();
							 logger.debug("----------account total connections before adding---"+ currentUserwithConnections.getLayers_1().size());
							 int initialcount=currentUserwithConnections.getLayers_1().size();
							 currentConnectedLayers.add(thisLayer);
							 currentUserwithConnections.setLayers_1(currentConnectedLayers);
							 logger.debug("----------account total connections before save---"+ currentUserwithConnections.getLayers_1().size());
							 logger.debug("----------result of account save---"+accountService.save(currentUserwithConnections));
							 Account checkUserAccess= accountService.getbyIdWithLayers(currentUser.getId());
							 logger.debug("----------account total connections after save---"+ checkUserAccess.getLayers_1().size());
							 int finalcount=checkUserAccess.getLayers_1().size();
							 if(finalcount==(initialcount+1))
							 {
								logger.debug("----------account modifications saved successfully-------------");
								nonGatedLayerSubscriptionConfirmationMail(thisLayer,currentUser,action);
								return "Success--You have been connected successfully";
							 }
							 else
							 {
								 logger.debug("----------account service failed to save your changes-------------");
								 return "Error--Your changes are not saved. Contact Administrator.";
							 }
						  }
						  else
						  {
							  logger.debug("----------Layer Disconnection Activities-------------");
							  logger.debug("No of layers before removal="+currentUserwithConnections.getLayers_1().size());
								 Set<Layer> currentConnectedLayers= currentUserwithConnections.getLayers_1();
								 Layer rmLayer=null;
								 for(Layer l:currentConnectedLayers)
								 {
									 if(l.getId().equals(thisLayer.getId()))
									 {
										 rmLayer=l;
										// System.out.println("Remove the accurate Layer"+currentConnectedLayers.remove(l));
									 }
								 }
								 logger.debug("Remove the accurate Layer="+currentConnectedLayers.remove(rmLayer));
								 currentUserwithConnections.setLayers_1(currentConnectedLayers);
								 logger.debug("No of layers after set="+currentUserwithConnections.getLayers_1().size());
								 logger.debug("----------result of account save---"+accountService.save(currentUserwithConnections));
								 Account checkUserAccess= accountService.getbyIdWithLayers(currentUser.getId());
								 logger.debug("----------No of layers after save---"+ checkUserAccess.getLayers_1().size());
								 if(true)
								 {
									 logger.debug("----------account modifications saved successfully-------------");
									 nonGatedLayerSubscriptionConfirmationMail(thisLayer,currentUser,action);
									return "Success--You have been disconnected successfully";
								 }
								 else
								 {
									 logger.debug("----------account service failed to save your changes-------------");
									 return "Error--Your changes are not saved. Contact Administrator.";
								 }
						  }
					  }
				  }
				  else
				  {
					  return "Error--Invalid Arguments";
				  }
			  }
			  else
			  {
				  return "Error--Missing Arguments"; 
			  }
		}
		
		return "Error--Session Not Found";
	}
	
}
