package au.com.dmsq.web;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.dmsq.model.Message;
import au.com.dmsq.service.MessageService;

@Controller
public class APIMessage {
	private static final Logger logger = LoggerFactory.getLogger(APIMessage.class);
	
	@Autowired MessageService messageService;
	

	@ResponseBody 
	@RequestMapping(method={RequestMethod.GET}, value={"api/getMessages"})
	public  List<Message> message() {
		
		logger.debug("------------------------------entering getCategories ------------------------------");
		
		return messageService.findAll();
	}

}
