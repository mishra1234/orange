package au.com.dmsq.web;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Banner;
import au.com.dmsq.model.IconDTO;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;


@Controller
public class BannerController{ 

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	
	@Autowired BannerService bannerService;
	@Autowired ActivityService activityService;
	
	@Transactional
	@RequestMapping(value="/admin/banners", method= RequestMethod.GET)
	public ModelAndView events() throws Exception {
		
		logger.debug("------------------------------ Admin - List Banners ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		int max = 20;
		int initial=( (bannerService.count())-max);
		int prev=1;
		int next=2;
		int hmany = (bannerService.count()/max)+1;
		List<Banner> pagedBanners= bannerService.pagedBanners(initial,max);
		mav.addObject("listOfBanners", pagedBanners);
		mav.addObject("prev", prev);
		mav.addObject("next", next);
		mav.addObject("hmany", hmany);
		mav.addObject("curr", 1);
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		mav.addObject("hmany", hmany);
		mav.addObject("numbers", numbers);
		
		mav.setViewName("/admin/banner/banners"); // jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/banners/{curr}")
	public ModelAndView pagedEvents(@PathVariable(value="curr") Integer curr) throws Exception {
		
		logger.debug("------------------------------Admin - List Paged Banners ------------------------------");
		ModelAndView mav = new ModelAndView(); 
		
		
		int max = 20;
		int next = 0;
		int prev = 0;
		int initial= 0;
		int hmany = (bannerService.count()/max)+1;
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		
		if(curr==1||curr<1){
			initial=( (bannerService.count())-max);
			prev=1;
			next=2;
			List<Banner> pagedBanners= bannerService.pagedBanners(initial,max);
			mav.addObject("listOfBanners", pagedBanners);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr>1 && curr < hmany){
			initial= (bannerService.count())-(curr*max);
			prev=curr-1;
			next=curr+1;
			List<Banner> pagedBanners= bannerService.pagedBanners(initial, max);
			mav.addObject("listOfBanners", pagedBanners);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr==hmany){
			initial= 0;
			prev=curr-1;
			next=curr;
			List<Banner> pagedBanners= bannerService.pagedBanners(initial, bannerService.count()%max);
			mav.addObject("listOfBanners", pagedBanners);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		
		mav.addObject("hmany", hmany);
		mav.addObject("curr", curr);
		mav.addObject("numbers", numbers);
		
		mav.setViewName("/admin/banner/banners"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/banners", method= RequestMethod.GET)
	public ModelAndView eventsMgr() {
		
		logger.debug("------------------------------ Manager - List Banners ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/manager/banner/banners"); // jsp
		
		List<Banner> listBannrs = bannerService.findAll();
		mav.addObject("banners", listBannrs);
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/banner/{id}", method= RequestMethod.GET)
	public ModelAndView getEvent(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Admin - Get Banner ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/admin/banner/banner"); //jsp
		
		Banner bann = bannerService.findById(id); 
		mav.addObject("bann", bann);
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/banner/{id}", method= RequestMethod.GET)
	public ModelAndView getEventMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Get Banner ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/manager/banner/banner"); //jsp
		
		Banner bann = bannerService.findById(id); 
		mav.addObject("bann", bann);
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/banner/save", method= RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("newBanner") @Valid Banner newBanner, 
			@RequestParam("image")int image, BindingResult result, SessionStatus status) throws Exception
	{
		
		logger.debug("------------------------------ Admin - Save Banner ------------------------------");
	
		if (result.hasErrors()) {
			//if validator failed
			ModelAndView mav = new ModelAndView();
			mav.addObject("listOfImages", bannerService.listImages()); 
			mav.setViewName("/admin/banner/newBanner"); //jsp
			return mav;
			
		} else {
			boolean saveResult = false; 
			
			List<IconDTO> icons = bannerService.listImages();
			Integer iconx = image;
			IconDTO x = icons.get(iconx);
			newBanner.setImageUrl(x.getIconUrl().toString());

			if (bannerService.save(newBanner)) saveResult = true;
			Banner saveBanner = newBanner; 
			ModelAndView mav = events(); 
			mav.addObject("message", bannerService.save(saveBanner));
			return mav;
		}
	}
	
	
	@RequestMapping(value="/manager/banner/save", method= RequestMethod.POST)
	public ModelAndView saveMgr(@ModelAttribute("newBanner") @Valid Banner newBanner, 
			@RequestParam("image")int image, BindingResult result, SessionStatus status) throws Exception
	{
		
		logger.debug("------------------------------ Manager - Save Banner ------------------------------");
	
		if (result.hasErrors()) {
			//if validator failed
			ModelAndView mav = new ModelAndView();
			mav.addObject("listOfImages", bannerService.listImages()); 
			mav.setViewName("/manager/banner/newBanner"); //jsp
			return mav;
			
		} else {
			boolean saveResult = false; 
			
			List<IconDTO> icons = bannerService.listImages();
			Integer iconx = image;
			IconDTO x = icons.get(iconx);
			newBanner.setImageUrl(x.getIconUrl().toString());

			if (bannerService.save(newBanner)) saveResult = true;
			Banner saveBanner = newBanner; 
			ModelAndView mav = eventsMgr(); 
			mav.addObject("message", bannerService.save(saveBanner));
			return mav;
		}
	}
	
	
	@RequestMapping(value="/admin/banner/delete/{id}")
	public String delete(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Delete Banner ------------------------------");
	
		Boolean banner = bannerService.deleteById(id); 
		
		ModelAndView mav =  events();
		mav.addObject("message", banner);
		
		return "redirect:/admin/banners/";
	}
	
	
	@RequestMapping(value="/manager/banner/delete/{id}")
	public String deleteMgr(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Manager - Delete Banner ------------------------------");
	
		Boolean banner = bannerService.deleteById(id); 
		
		ModelAndView mav =  eventsMgr();
		mav.addObject("message", banner);
		
		return "redirect:/manager/banners/";
	}
	
	
	@RequestMapping(value="/admin/banner/new")
	public ModelAndView newBanner() throws Exception {
		
		logger.debug("------------------------------ Admin - New Banner ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Banner newBann = new Banner();
		
		mav.addObject("newBanner", newBann);
		mav.addObject("listOfImages", bannerService.listImages()); 
		mav.setViewName("/admin/banner/newBanner"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/banner/new")
	public ModelAndView newBannerMgr() throws Exception {
		
		logger.debug("------------------------------ Manager - New Banner ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Banner newBann = new Banner();
		
		mav.addObject("iUrl", 0); 
		mav.addObject("newBanner", newBann);
		mav.addObject("listOfImages", bannerService.listImages()); 
		mav.setViewName("/manager/banner/newBanner"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/banner/editBanner/{id}", method= RequestMethod.GET)
	public ModelAndView editBanner(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Edit Banner ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("newBanner", bannerService.get(id));  // This variable needs to be named as in the NewEvent
		mav.addObject("listOfImages", bannerService.listImages()); 
		mav.setViewName("/admin/banner/newBanner"); //jsp	
		return mav; 
	}
	
	
	@RequestMapping(value="/manager/banner/editBanner/{id}", method= RequestMethod.GET)
	public ModelAndView editBannerMgr(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Manager - Edit Banner ------------------------------");
		
		ModelAndView mav = new ModelAndView();
		
		List<IconDTO> icons = bannerService.listImages();
		Integer icon = null;
		for(int i=0; i<icons.size(); i++){
			String xxx = icons.get(i).getIconUrl();
			if(xxx.equalsIgnoreCase(bannerService.get(id).getImageUrl().toString() ) ){
				icon = i;
				i = icons.size();
			}
		}
		mav.addObject("iUrl", icon);  //Current Index of the iconUrl to send to JSP
		//System.out.println("Controller - reset password - icon: " + icon.toString());
		mav.addObject("newBanner", bannerService.get(id));  // This variable needs to be named as in the NewEvent
		mav.addObject("listOfImages", bannerService.listImages()); 
		mav.setViewName("/manager/banner/newBanner"); //jsp	
		return mav; 
	}
}



