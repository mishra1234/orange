package au.com.dmsq.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountConfirmation;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.TimeData;
import au.com.dmsq.model.WeatherForecast;
import au.com.dmsq.model.WeatherObservation;
import au.com.dmsq.service.AccountConfirmationService;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;

@Controller
public class UserHome extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserHome.class);
	
	@Autowired AccountService accountService;
	@Autowired ActivityService activityService;
	@Autowired AccountConfirmationService accountConfirmationService;
	
	

	@RequestMapping("/")
	public String index(HttpServletRequest request) {
	
		return findRole(request);
		
	}
	
	
	@RequestMapping("/start")
	public ModelAndView start(HttpServletRequest request) throws ParseException {
		
		
		return landing();
	}
	
	
	
	
	@RequestMapping("/secure/home")
	public String delegate (HttpServletRequest request) {
		return findRole(request); 
	}
	
	@RequestMapping("/secure/reConfirm/{id}")
	private ModelAndView unconfirmedAccount(@PathVariable("id") Integer accountId){
		Account account = accountService.getbyIdWithAccConfirmations(accountId);
		
		ModelAndView mav = new ModelAndView(); 
		mav.addObject("account", account);
		mav.setViewName("/secure/reConfirm");
		return mav;
	}
	
	private String adminHome() {
		
		logger.debug("-----------Admin Home  --------------");
		return "redirect:" + "/member/home";
	}

	private String memberHome() {
		
		logger.debug("-----------member Home  --------------");
		return "redirect:" + "/member/home"; 
	}

	public String managerHome () {
		logger.debug("-----------Manager Home  --------------");
		return "redirect:" + "/admin/home"; 
		
	}
	
	@RequestMapping("/admin/home")
	public ModelAndView admin(){
		
		ModelAndView mav = new ModelAndView();
	//	Account account = accountService.getbyIdWithAccConfirmations(getCurrentAccount().get);
		System.out.println("When see user profile the current account is "+ getCurrentAccount().getFirstName());
		if (getCurrentAccount() != null) {
			
			mav.addObject("acct", getCurrentAccount());
		}
		
		mav.setViewName("admin/home");
		return mav; 
	}
	
	@RequestMapping("/manager/home")
	public ModelAndView manager(){
		ModelAndView mav = new ModelAndView();
		if (getCurrentAccount() != null) {
			mav.addObject("acct", getCurrentAccount());
		}
		mav.setViewName("manager/home");
		return mav; 
	}
	

	@RequestMapping("/member/home")
	public ModelAndView member() throws ParseException{
		
		logger.debug("------------------------------------Member Home: " + System.nanoTime());
		ModelAndView mav = setBasicData();
		
		List<Activity> activities = new ArrayList<Activity>();
		List<Activity> latestInfos = new ArrayList<Activity>();
		
		List<Integer> allConnected = (List<Integer>) mav.getModel().get("allConnected");
		
		/* TODO: Perfomance */
		
		logger.debug("------------------------------------Member Activities: " + System.nanoTime());
		activities.addAll(activityService.getEventsByCommIds(allConnected)); //Get the EVENTS of the layers
		latestInfos.addAll(activityService.getInfosByCommIds(allConnected)); //Get the INFOS of the layers
		logger.debug("----------------------------------------End - Activities: " + System.nanoTime());
		
		List<Activity> latestActivities = new ArrayList<>();
		latestActivities = activityService.getStartandEndDatesfromEvents(activities); //Gets all the EVENTS
		
		Collections.sort(latestActivities, Activity.ActivityComparatorD);
		
		// Changing the date format for events to be displayed using "dd-mm-yyyy"
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
        for(int i=0; i<latestActivities.size();i++){
        	Activity act = new Activity();
			Date date = sdf.parse(latestActivities.get(i).getStartDate());
			act.setStartDate(sdfDate.format(date));
			latestActivities.get(i).setStartDate(act.getStartDate());
        }
        
		
		 Collections.sort(latestActivities, new Comparator<Activity>() {
		        @Override
		        public int compare(final Activity object1, final Activity object2) {
		            return object1.getId().compareTo(object2.getId());
		        }
		       } );
		 
		 Collections.sort(latestInfos, new Comparator<Activity>() {
		        @Override
		        public int compare(final Activity object1, final Activity object2) {
		            return object2.getId().compareTo(object1.getId());
		        }
		       } );
		 
		 
		List <Activity> recommended = new ArrayList<Activity>();  
		for (Activity act: latestActivities){
			if (act.getRecommended()){
				recommended.add(act);
			}
		}
		 
		mav.addObject("recommended", recommended);
		mav.addObject("activities", latestActivities);
		mav.addObject("infos", latestInfos);
		mav.setViewName("member/ccmeHome");
		
		logger.debug("------------------------------------Member End: " + System.nanoTime());
		return mav; 
	}
	
	
	private String findRole(HttpServletRequest request){
		Account account = accountService.findAcctByEmail(request.getRemoteUser());		
		
		if (request.isUserInRole("3") && account.getStatus().equalsIgnoreCase("active")) {
			logger.debug("<<<========Member Status Confirmed ======>>>");
			return memberHome(); 
		}else if(request.isUserInRole("2")){
			return managerHome(); 
		}else if(request.isUserInRole("1") && account.getStatus().equalsIgnoreCase("active")){
			logger.debug("<<<========Admin Status Confirmed ======>>>");
			return adminHome(); 
		}else if(request.isUserInRole("3") && account.getStatus().equalsIgnoreCase("unconfirmed")){
			logger.debug("<<<======== Account Status UN-Confirmed ======>>>");
			return "redirect:" + "/secure/reConfirm/"+account.getId();
		}
		else if(request.isUserInRole("1") && account.getStatus().equalsIgnoreCase("unconfirmed")){
			logger.debug("<<<======== Account Status UN-Confirmed ======>>>");
			return "redirect:" + "/secure/reConfirm/"+account.getId();
		}
		else{	
			return "redirect:" + "/start";  //todo Exception 
		}
	}
	
	@RequestMapping("/pork")
	public ModelAndView pork() throws ParseException{
		ModelAndView mav = new ModelAndView();
		
		List<Activity> activities = new ArrayList<Activity>();
		activities.addAll(activityService.getEventsByCommId(pork));  //70515 Pork 
		
		List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities);
		Collections.sort(latestActivities, Activity.ActivityComparatorD);
		
		List<Activity> latestInfos = new ArrayList<Activity>();
		latestInfos.addAll(activityService.getInfosByCommId(pork));
		
		Set<Activity> randomInfos = new HashSet<Activity>();
		for(Activity passInfo:latestInfos){
			randomInfos.add(passInfo);
		}
		
		mav.addObject("infos", randomInfos);
		mav.addObject("activities", latestActivities);
		mav.setViewName("/pork");
		return mav; 
	}
	
	@RequestMapping("/about")
	public ModelAndView about(){
		ModelAndView mav = new ModelAndView();
		
		mav.setViewName("/about");
		return mav; 
	}
	
	@RequestMapping("/more")
	public ModelAndView more(){
		ModelAndView mav = new ModelAndView();
		
		mav.setViewName("/more");
		return mav; 
	}
	
	@RequestMapping("/livestock")
	public ModelAndView livestock() throws ParseException{
		ModelAndView mav = new ModelAndView();
		boolean isGated = false;
		
		List<Activity> activities = new ArrayList<Activity>();
		activities.addAll(activityService.getEventsByCommId(livestock));  //70511 Livestock 
		
		List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities);
		Collections.sort(latestActivities, Activity.ActivityComparatorD);
		
		List<Activity> latestInfos = new ArrayList<Activity>();
		latestInfos.addAll(activityService.getInfosByCommId(livestock));
		
		Set<Activity> randomInfos = new HashSet<Activity>();
		for(Activity passInfo:latestInfos){
			randomInfos.add(passInfo);
		}
		
		mav.addObject("isGated", isGated);
		mav.addObject("infos", latestInfos);
		mav.addObject("activities", latestActivities);
		mav.setViewName("/member/livestock");
		return mav; 
	}
	
	@RequestMapping("/dairyfarmers")
	public ModelAndView dairy() throws ParseException{
		ModelAndView mav = new ModelAndView();
		
		boolean isGated = false;
		
		List<Activity> activities = new ArrayList<Activity>();
		activities.addAll(activityService.getEventsByCommId(dairy));  //70514 Dairy 
		
		List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities);
		Collections.sort(latestActivities, Activity.ActivityComparatorD);
		
		List<Activity> latestInfos = new ArrayList<Activity>();
		latestInfos.addAll(activityService.getInfosByCommId(dairy));
		
		Set<Activity> randomInfos = new HashSet<Activity>();
		for(Activity passInfo:latestInfos){
			randomInfos.add(passInfo);
		}
		
		mav.addObject("isGated", isGated);
		mav.addObject("infos", randomInfos);
		mav.addObject("activities", latestActivities);
		mav.setViewName("/member/dairyfarmers");
		return mav; 
	}
	
	@RequestMapping("/grain")
	public ModelAndView grain() throws ParseException{
		ModelAndView mav = new ModelAndView();
		
		boolean isGated = false;
		
		List<Activity> activities = new ArrayList<Activity>();
		activities.addAll(activityService.getEventsByCommId(grain));  //70510 Grain Producers SA (GPSA) 
		
		List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities);
		Collections.sort(latestActivities, Activity.ActivityComparatorD);
		
		List<Activity> latestInfos = new ArrayList<Activity>();
		latestInfos.addAll(activityService.getInfosByCommId(grain));
		
		Set<Activity> randomInfos = new HashSet<Activity>();
		for(Activity passInfo:latestInfos){
			randomInfos.add(passInfo);
		}
		
		mav.addObject("isGated", isGated);
		mav.addObject("infos", randomInfos);
		mav.addObject("activities", latestActivities);
		mav.setViewName("/member/grain");
		return mav; 
	}
	
	@RequestMapping("/horticulture")
	public ModelAndView horticulture() throws ParseException{
		ModelAndView mav = new ModelAndView();
		
		boolean isGated = false;
		
		List<Activity> activities = new ArrayList<Activity>();
		activities.addAll(activityService.getEventsByCommId(horticulture));  //70512 Horticulture 
		
		List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities);
		Collections.sort(latestActivities, Activity.ActivityComparatorD);
		
		List<Activity> latestInfos = new ArrayList<Activity>();
		latestInfos.addAll(activityService.getInfosByCommId(horticulture));
		
		Set<Activity> randomInfos = new HashSet<Activity>();
		for(Activity passInfo:latestInfos){
			randomInfos.add(passInfo);
		}
		
		mav.addObject("isGated", isGated);
		mav.addObject("infos", randomInfos);
		mav.addObject("activities", latestActivities);
		mav.setViewName("/member/horticulture");
		return mav; 
	}
	
	@RequestMapping("/welcome")
	public ModelAndView ccme(){
		ModelAndView mav = new ModelAndView();
		
		mav.setViewName("/loginCCME");
		return mav; 
	}
	
	@RequestMapping("/landing")
	public ModelAndView landing(){
		ModelAndView mav = new ModelAndView();
		
		mav.setViewName("/landing");
		return mav; 
	}
	
	private static String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	        return buffer.toString();
	    } finally {
	        if (reader != null)
	            reader.close();
	    }
	}

	
	public String getTitle(int c){
		
		if (c == 1) return "ADELAIDE AND MOUNT LOFTY RANGES";
		if (c == 2) return "YORKE PENINSULA AND KANGAROO ISLAND";
		if (c == 3) return "UPPER SOUTH EAST AND LOWER SOUTH EAST";
		if (c == 4) return "MURRAYLANDS AND RIVERLAND";
		if (c == 5) return "FLINDERS AND MID NORTH";
		if (c == 6) return "WEST COAST, LOWER EYRE PENINSULA AND EASTERN EYRE PENINSULA";
		return "NORTH WEST PASTORAL AND NORTH EAST PASTORAL";
	
	}

}
