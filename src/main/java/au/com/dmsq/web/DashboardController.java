package au.com.dmsq.web;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.ActivityPayment;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Mail;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityPaymentService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.CategoryService;
import au.com.dmsq.service.LayerService;

@Controller
public class DashboardController extends AbstractController{
	
	private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);
	
	@Autowired AccountService accountService;
	@Autowired LayerService layerService;
	@Autowired ActivityService activityService;
	@Autowired ActivityPaymentService actPayService;
	@Autowired CategoryService categoryService;
	
	@Transactional
	@RequestMapping(value="/admin/home", method=RequestMethod.GET)
	public ModelAndView dashboardHomeNewUsers() throws Exception {
		
		logger.debug("------------------------------ New users on dashboard ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		List<Layer> listOfCom = new ArrayList<Layer>();
		List<Integer> comId = new ArrayList<Integer>();
		List<Account> managers = new ArrayList<>();
		List<ActivityPayment> actPay = new ArrayList<ActivityPayment>();
		
		int countSurveys = 0;
		int countInfos=0;
		int countCat=0;
		int countManagers = 0;
		double price=0.10;
		int countSurveysForMonthlyInvoice = 0;
		int countEventsForMonthlyInvoice = 0;
		int countInfosForMonthlyInvoice = 0;
		int countRecommended=0;
		double amount=0.0;
		int forMonthlyInvoice = accountService.thisMonthConnections(getCurrentAccount());
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		int month = cal.get(Calendar.MONTH)+1;
		int year = cal.get(Calendar.YEAR);
		
		Date date = new Date();
		//All New User chart Json String
		String jsonNewUsers = accountService.getConnections(getCurrentAccount());
		logger.debug("------The json string is: "+ jsonNewUsers);
		
		actPay = actPayService.getAllPaid(getCurrentAccount());
		System.out.println("Number of act pay records : " + actPay.size() );
		 listOfCom = layerService.layersIfManager(getCurrentAccount().getId());
		 for(int i=0;i<listOfCom.size();i++){
			 List<Activity> acts = activityService.findByLayerId(listOfCom.get(i).getId());
				Set<Activity> activities = new HashSet<>();
				for(Activity e:acts){
					activities.add(e);
				}
				for(Activity ac : activities){
				if(ac.getActivityType().equals("survey")){
				countSurveys++;
				}
				else if(ac.getActivityType().equals("info")){
					countInfos++;
				}
//Ho-----------\
					
				cal.setTime(ac.getCreatedDatetime());
				int surveyMonth = cal.get(Calendar.MONTH)+1;
				int activityYear = cal.get(Calendar.YEAR);
				if(ac.getActivityType().equals("survey") && surveyMonth==month && activityYear==year){
				countSurveysForMonthlyInvoice++;
				}
				else if(ac.getActivityType().equals("info") && surveyMonth==month && activityYear==year){
					countInfosForMonthlyInvoice++;
				}
				else if(ac.getActivityType().equals("event") && surveyMonth==month && activityYear==year){
					countEventsForMonthlyInvoice++;
				}
				else if((ac.getActivityType().equals("event") || ac.getActivityType().equals("info") || ac.getActivityType().equals("survey")) && surveyMonth==month && ac.getRecommended().equals(1)){
					countRecommended++;
				}
		 }
		 }
		 
		 Integer mess = 0;
		 mess  = messageService.countMessagesAsPerMonthIndividual(getCurrentAccount().getId(), month, year);
	
				
		 amount = Math.round((mess*price + countSurveysForMonthlyInvoice*price + countInfosForMonthlyInvoice*price + countEventsForMonthlyInvoice*price + countRecommended*price + forMonthlyInvoice*price)* 100.0)/ 100.0;
				System.out.println("Amount is =================================================="+amount);
		 
		 //No of categories
			List<Category> listcat = categoryService.getNonDeleted(); 
			for(int i=0;i<listcat.size();i++){
				countCat++;
			}
		 //Managers
		 System.out.println("Size: " + listOfCom.size());
		 int 	futureActivities=0;
		for(int i=0;i<listOfCom.size();i++){
			futureActivities += activityService.pagedActsPages("event", date.getTime(), listOfCom.get(i).getId());
			logger.debug("--------------------------Community Id is: ---------------------------"+listOfCom.get(i).getId() + "---and Name are: -----------"+listOfCom.get(i).getName());
			 	comId.add(listOfCom.get(i).getId());
		}
		 logger.debug("---No of Surveys -----------------"+ countManagers+ "----------------------");
			
		Iterator it = comId.iterator();
		List<Layer> l=new ArrayList<Layer>();
		while(it.hasNext()){
			l.add(layerService.findById(Integer.parseInt(it.next().toString())));
		}
		Integer connections = accountService.totalConnections(getCurrentAccount());
		Integer numberOfCommuntities = listOfCom.size();
		
		System.out.println("Total future activities are: " + futureActivities);
		
		Account currentAccount =  getCurrentAccount();
		Set<Layer> managedLayers = (Set<Layer>) currentAccount.getLayers();
		
		mav.addObject("newUserList", jsonNewUsers);
		mav.addObject("acct", getCurrentAccount());
		mav.addObject("Amount", amount);
		mav.addObject("listOfManagedComm", managedLayers);
		mav.addObject("totalConnections", connections);
		mav.addObject("CommSize", numberOfCommuntities);
		mav.addObject("futureActivities", futureActivities);
		mav.addObject("noOfSurveys", countSurveys);
		mav.addObject("noOfInfo", countInfos);
		mav.addObject("noOfCat", countCat);
		mav.addObject("managers", countManagers);
		mav.setViewName("/admin/home"); // jsp
		return mav; 
	}
	
	
	@Transactional
	@RequestMapping(value="/getStatsCom/{id}", method=RequestMethod.GET)
	@ResponseBody
	public String dashboardHomeNewUsersPerCommunity(@PathVariable(value="id") Integer communityId, Model model) throws Exception {
		logger.debug("------------------------------ New users on dashboard based on individual charts " + communityId+" ------------------------------");
		ModelAndView mav = new ModelAndView(); 
		List<Layer> listOfCom = new ArrayList<Layer>();
		List<Integer> comId = new ArrayList<Integer>();
		Date date = new Date();
		String jsonNewUsers = accountService.getConnectionOfParticularCommunity(communityId);
		logger.debug("------The json string for community "+communityId+" is: "+ jsonNewUsers);
		if(communityId.equals("")){
			 jsonNewUsers = accountService.getConnections(getCurrentAccount());
			logger.debug("------The json string is: "+ jsonNewUsers);
			
		}
		
		 listOfCom = layerService.layersIfManager(getCurrentAccount().getId());
		 
		 System.out.println("Size: " + listOfCom.size());
		 int 	futureActivities=0;
		for(int i=0;i<listOfCom.size();i++){
			futureActivities += activityService.pagedActsPages("event", date.getTime(), listOfCom.get(i).getId());
			 	comId.add(listOfCom.get(i).getId());
		}
		Iterator it = comId.iterator();
		List<Layer> l=new ArrayList<Layer>();
		while(it.hasNext()){
			l.add(layerService.findById(Integer.parseInt(it.next().toString())));
		}
		Integer connections = accountService.totalConnections(getCurrentAccount());
		Integer numberOfCommuntities = listOfCom.size();
		
		System.out.println("Total future activities are: " + futureActivities);
		
		Account currentAccount =  getCurrentAccount();
		Set<Layer> managedLayers = (Set<Layer>) currentAccount.getLayers();
		
//		mav.addObject("newUserListForOneCom", jsonNewUsers);
//		mav.addObject("listOfManagedComm", managedLayers);
//		mav.addObject("totalConnections", connections);
//		mav.addObject("CommSize", numberOfCommuntities);
//		mav.addObject("futureActivities", futureActivities);
	//	mav.setViewName("/admin/home"); // jsp
		return jsonNewUsers; 
	}
	public static String getMonth(int month) {
    return new DateFormatSymbols().getMonths()[month-1].substring(0, 3);
	}
	

	
	@Transactional
	@RequestMapping(value="/admin/dashoboardInvoicebalance", method= RequestMethod.GET)
	public ModelAndView dashboardInvoicing() throws IllegalAccessException, ClassNotFoundException, Exception {
		
		logger.debug("------------------------------ Dashboard Invoice balance is called ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		double price= 0.10;
		double amount=0;
		int countSurveys=0;
		int countInfos=0;
		int countEvents = 0;
		int countRecommended = 0;
		int countTransactions = 0;
		int connections = accountService.thisMonthConnections(getCurrentAccount());
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		int month = cal.get(Calendar.MONTH)+1;
		//month = cal.get(Calendar.MONTH)+1;
		System.out.println(" This Months is=================================="+month);
		List<ActivityPayment> actPay = new ArrayList<ActivityPayment>();
		actPay = actPayService.getAllPaid(getCurrentAccount());			
		for(int i=0;i<actPay.size();i++){
			System.out.println("Month of the transcations: " + (actPay.get(i).getDate()).getMonth());
			
		if((actPay.get(i).getDate().getMonth()+1)==month)
					countTransactions++;
		}
		List<Layer> listOfCom = new ArrayList<Layer>();
		 listOfCom = layerService.layersIfManager(getCurrentAccount().getId());
		 System.out.println("Size of the set of communitites: "  + listOfCom.size());
		 for(int i=0;i<listOfCom.size();i++){
			 List<Activity> acts = activityService.findByLayerId(listOfCom.get(i).getId());
				Set<Activity> activities = new HashSet<>();
				for(Activity e:acts){
					activities.add(e);
				}
				
				System.out.println("Size of activityPayment's activities now is: " +actPay.size());
			
				for(Activity ac : activities){
					logger.debug("--------------------------------Created date time is + " + ac.getCreatedDatetime());
					
					cal.setTime(ac.getCreatedDatetime());
					int surveyMonth = cal.get(Calendar.MONTH)+1;
				if(ac.getActivityType().equals("survey") && surveyMonth==month){
				countSurveys++;
				}
				else if(ac.getActivityType().equals("info") && surveyMonth==month){
					countInfos++;
				}
				else if(ac.getActivityType().equals("event") && surveyMonth==month){
					countEvents++;
				}
				else if((ac.getActivityType().equals("event") || ac.getActivityType().equals("info") || ac.getActivityType().equals("survey")) && surveyMonth==month && ac.getRecommended().equals(1)){
					countRecommended++;
				}
		 
		 }
		 }
		 List<Integer> years = new ArrayList<Integer>();
		 int currentYear = cal.get(Calendar.YEAR);
			for(int i2=2014;i2<=currentYear;i2++){
				years.add(i2);
			}
		 Integer mess = 0;
		 mess  = messageService.countIndividual(getCurrentAccount().getId());
		 Map<String, String> months1 = new HashMap<String, String>();
		 Map<String, String> wholeMonth = new HashMap<String, String>();
		 for(int j=1;j<=12;j++){
			 
			 wholeMonth.put(""+j, getMonth(j));
			}	
		 for(int k=1;k<month+1;k++){
			 
				months1.put(""+k, getMonth(k));
			}
		
			// System.out.println("Exception is ======================"+ee);
		//	 List<Message> listMessage = messageService.getNonDeleted(); 
		System.out.println("Messages are : ======================"+ mess);
		System.out.println("Amount is : ======================"+ amount);
		System.out.println("Number of ttansactions are:  " + countTransactions);
		mav.addObject("Amount", amount);
		mav.addObject("acct", getCurrentAccount());
		 mav.addObject("numberMessages", mess);
		 mav.addObject("surveyNumber", countSurveys);
		 mav.addObject("InfoNumber", countInfos);
		 mav.addObject("EventNumber", countEvents);
		 mav.addObject("RecommendedEvents", countRecommended);
		 mav.addObject("Connection", connections);
		 mav.addObject("ActTransactions", countTransactions);
		 mav.addObject("Months", months1);
		 mav.addObject("AllMonths", wholeMonth);
		 mav.addObject("currentYear", currentYear);
		 mav.addObject("Year", years);
		
		return mav;
	}
	
	@Transactional
	@RequestMapping(value="/getMonthlyInvoice/{monthId}/{yearId}", method=RequestMethod.GET)
	@ResponseBody
	public String dashboardInvoiceEachMonth(@PathVariable(value="monthId") Integer month, @PathVariable(value="yearId") Integer year) throws Exception {
		logger.debug("------------------------------ Get invoice of " + month+" ------------------------------");
		ModelAndView mav = new ModelAndView(); 
		int countSurveys=0;
		int countInfos=0;
		int countEvents = 0;
		int countRecommended = 0;
		int countTransactions=0;
		int connections = accountService.particularMonthConnections(getCurrentAccount(), month, year);
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		int month1 = cal.get(Calendar.MONTH)+1;
		int currentYear = cal.get(Calendar.YEAR);
		System.out.println(" This Months is=================================="+month);
		List<ActivityPayment> actPay = new ArrayList<ActivityPayment>();
		actPay = actPayService.getAllPaid(getCurrentAccount());			
		for(int i=0;i<actPay.size();i++){
			System.out.println("Month of the transcations: " + (actPay.get(i).getDate()).getMonth());
			
		if((actPay.get(i).getDate().getMonth()+1)==month)
					countTransactions++;
		}
		
		List<Layer> listOfCom = new ArrayList<Layer>();
		 listOfCom = layerService.layersIfManager(getCurrentAccount().getId());
		 for(int i=0;i<listOfCom.size();i++){
			 List<Activity> acts = activityService.findByLayerId(listOfCom.get(i).getId());
				Set<Activity> activities = new HashSet<>();
				for(Activity e:acts){
					activities.add(e);
				}
				
				
				for(Activity ac : activities){
					logger.debug("--------------------------------Created date time is + " + ac.getCreatedDatetime());
					
					
				cal.setTime(ac.getCreatedDatetime());
				int surveyMonth = cal.get(Calendar.MONTH)+1;
				int activityYear = cal.get(Calendar.YEAR);
				if(ac.getActivityType().equals("survey") && surveyMonth==month && activityYear==year){
				countSurveys++;
				}
				else if(ac.getActivityType().equals("info") && surveyMonth==month && activityYear==year){
					countInfos++;
				}
				else if(ac.getActivityType().equals("event") && surveyMonth==month && activityYear==year){
					countEvents++;
				}
				else if((ac.getActivityType().equals("event") || ac.getActivityType().equals("info") || ac.getActivityType().equals("survey")) && surveyMonth==month && ac.getRecommended().equals(1)){
					countRecommended++;
				}
		 }
		 }
		 
		 Integer mess = 0;
		 mess  = messageService.countMessagesAsPerMonthIndividual(getCurrentAccount().getId(), month, year);
		 
			// System.out.println("Exception is ======================"+ee);
		//	 List<Message> listMessage = messageService.getNonDeleted(); 
		System.out.println("Messages are : ======================"+ mess);
		
		 mav.addObject("surveyNumber", countSurveys);
		 mav.addObject("id", getCurrentAccount().getId());
		 mav.addObject("InfoNumber", countInfos);
		 mav.addObject("acct", getCurrentAccount());
		 mav.addObject("EventNumber", countEvents);
		 mav.addObject("RecommendedEvents", countRecommended);
		 mav.addObject("Connection", connections);
		 mav.addObject("Transactions", countTransactions);
		 String jsonValues = "{"+"\"Message\":"+ mess+","+"\"Survey\":" + countSurveys+","+"\"Infos\":"+countInfos+","+"\"Events\":"+countEvents+","+"\"RecommendedEvents\":"+countRecommended +","+"\"connections\":"+connections+","+"\"Payments\":"+countTransactions+"}";
		 return jsonValues;
	}
	@RequestMapping(value = "/admin/pay", method = RequestMethod.GET)
	public ModelAndView payEvenOne()
			throws ParseException {

		logger.debug("----------------------------- Pay for Single Event  ------------------------------");

		ModelAndView mav = new ModelAndView();

		mav.setViewName("/admin/adminMonthlyPayment");
		mav.addObject("acct", getCurrentAccount());
		

		return mav;
	}
	
	@RequestMapping(value = "/admin/process")
	public ModelAndView payEvent(HttpServletRequest request,
			@RequestParam(value="name",           defaultValue="", 		required=true) String name,
			@RequestParam(value="expiry",         defaultValue="", 		required=true) String expiry, 
			@RequestParam(value="cvc",            defaultValue="", 		required=true) String cvc,
			@RequestParam(value="amount",         defaultValue="", 		required=true) String amount, 
			@RequestParam(value="number",         defaultValue="", 		required=true) String number)
			throws ParseException {

		logger.debug("----------------------------- Processing Payment ------------------------------");
		
		String reference = name+"-id-"+getCurrentAccount().getId()+"739"+"-"+System.currentTimeMillis();
		Integer accountID = getCurrentAccount().getId();
		System.out.println("Current user Id who is paying is: =================================================="+accountID);
		System.out.println("Amount received is: "+ amount);
		System.out.println("Name on Card: " + name);
		System.out.println("Expiry date is: "+ expiry);
		System.out.println("cvc: " + cvc);
		System.out.println("Card Number is: "+ number);
		
		ModelAndView mav = new ModelAndView();
		int amountDouble = Integer.parseInt(amount) * 100;
		expiry = expiry.replace(" ",""); 
		String json = "{\"card_holder\": \""+name+"\", \"card_number\": \""+number+"\", \"card_expiry\": \""+expiry+"\", \"cvv\": \""+cvc+"\", \"amount\": "+amountDouble+", \"reference\": \""+reference+"\", \"customer_ip\": \""+request.getRemoteAddr()+"\" }";
		String response = mintPayment(json); 
		mav.addObject("result", "");
		
		List<String> errors = new ArrayList<String>();
		boolean paid = false;
		String message =""; 
		String auth = ""; 
		String card_token = ""; 
		
		if (response != null) {
			JSONObject obj = new JSONObject(response);
			
			if (!obj.getJSONObject("response").isNull("authorization")){
			auth = obj.getJSONObject("response").getString("authorization");
			card_token = obj.getJSONObject("response").getString("card_token");
			message = obj.getJSONObject("response").getString("message");
			}
			paid = obj.getBoolean("successful");
			
			System.out.println("Outside the paid function "+paid);
			JSONArray errArr = obj.getJSONArray("errors");
			for (int i = 0; i < errArr.length(); i++)
			{
			     errors.add(errArr.getString(i));
			}
			Account saveAccount = new Account();
			if(paid){
				//Save Token
				System.out.println("Paid is: " + paid);
				Account getOldAccount = accountService.getbyIdWithRelationsFaster(accountID);
				getOldAccount.setCcToken(card_token);
				
				Date date = new Date();
				Date currentDateTime = new Timestamp(date.getTime());
				logger.debug("------------------------------ Save Account with card token ------------------------------");
	
				 boolean saved = accountService.save(getOldAccount);
				 System.out.println(":)========================================Card token is been saved along with the user's information in Account Table========================:)" + saved);
				 ActivityPayment saveActPayment = new ActivityPayment();
				 saveActPayment.setAccount(getCurrentAccount());
				// saveActPayment.setActivity(activityService.findById(id));
				 saveActPayment.setAmount(Integer.parseInt(amount));
				 saveActPayment.setDate(currentDateTime);
				 saveActPayment.setResponse(response);
				 boolean savedActPay =  actPayService.save(saveActPayment);
				 System.out.println(":)========================================Card's information along with user's id is been saved in ActivityPayment table========================:)" + savedActPay);
				
				 String messageBody = null;
				 String messageBodyCCMeApp=null;
				 String lineToEvent  = SERVER + "/event/pay/";
				 if(message.equals("Declined")){
					 messageBody = "Dear "+getCurrentAccount().getFirstName() +" "+ getCurrentAccount().getLastName() + " <br/><br/>" +"<font color=red>Your Payment was declined! </font> <br/ >" 
							 + "Your Recent payment of $"+amount+" was <b>declined</b>. <br>Your Application reference number is: "+ auth +"<br>Click the button to go back to the event payment page <br><a href='"+lineToEvent+"'> <button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #f77333;border: 1px solid #46b3d3; border-radius: 5px; cursor: pointer;'>Go back to event</button></a>";
				 }
				 else{
					 messageBody = "Dear "+getCurrentAccount().getFirstName() +" "+ getCurrentAccount().getLastName() + " <br/><br/>" +"<font color=green>Congratulations! Your Payment of $" +amount+ "is approved! </font>" +
							 "<br>Your Application reference number is: "+ auth;
				 
				 }
				 if(message.equals("Declined")){
					 messageBodyCCMeApp = "Dear "+getCurrentAccount().getFirstName() +" "+ getCurrentAccount().getLastName() +
							 " Your Recent payment of $"+amount+" was declined. Your Application reference number is: "+ auth;
				 }
				 else{
					 messageBodyCCMeApp = "Dear "+getCurrentAccount().getFirstName() +" "+ getCurrentAccount().getLastName()  +"Congratulations! Your Payment of $" +amount+ "is approved! " +
							 "Your Application reference number is: "+ auth;
				 
				 }
				 int toId = getCurrentAccount().getId();
				 String Subject = "Payment Confirmation";
				 String headers = getAuth();
      	 JSONObject jsonObject = new JSONObject(headers);
         String token = jsonObject.getString("access_token");
        //Sending payment confirmation notification to the mobile app.
        String jsonStringforCcMe=null;		       
        jsonStringforCcMe ="["+"{\"from_id\": \"1082\", \"to_id\":\"" +toId+ "\", \"subject\": \"" + Subject+"\", \"message\":\""+messageBodyCCMeApp+ "\", \"layer_id\":\"1\", \"notification\": \"1\"}"+"]"; 
       	JSONArray jsonNotificationArray = new JSONArray(jsonStringforCcMe);
				pushMessageNotification(jsonNotificationArray, token);
				System.out.println("Notification has been sent to user : " + getCurrentAccount().getEmail());
				
				//Sending confirmation email to the user..
				Mail mail = new Mail();
				mail.setFrom(FROM_EMAIL);
				mail.setSubject("Payment Confirmation");
				String body = mcs.readPaymentConfirmationEmail();
				body = body.replace("ApprovedOrDecline", message);
				body = body.replace("pasteBodyHere", messageBody);
				mail.setMessage(body);
				mailService.welcomeMail("ccme@digitalmarketsquare.com", getCurrentAccount().getEmail(), mail.getSubject(), mail.getMessage());
				System.out.println("Payment Confirmation email is sent to "+ getCurrentAccount().getEmail());
			
				
			}
		
			
			mav.addObject("paid", paid);
			mav.addObject("auth", auth);
			mav.addObject("message", message);
			if (errors != null && !errors.isEmpty())
			mav.addObject("errors", errors);
			mav.addObject("acct", getCurrentAccount());
			
		}
		

		mav.setViewName("payEventSuccess");

		return mav;
	}
	
	//Get Auth Token for push notification
	private static String getAuth()
	{
        URL url;
        HttpURLConnection connection = null;
        StringBuffer response = new StringBuffer();
        
    try {
        // TODO code application logic here
           url = new URL("http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/user/auth");
           connection = (HttpURLConnection) url.openConnection();
           connection.setRequestMethod("POST");
           connection.setConnectTimeout(5000);
           connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

           connection.setUseCaches(false);
           connection.setDoInput(true);
           connection.setDoOutput(true);  
           String payload = "email="+  URLEncoder.encode("mishra.anoop6@gmail.com", "UTF-8");
           payload += "&password=" 
               + URLEncoder.encode("123", "UTF-8");
           payload += "&device=" 
               + URLEncoder.encode("web", "UTF-8");
          System.out.println(payload);
           // Send request
           DataOutputStream wr = new DataOutputStream(
           connection.getOutputStream());
           wr.writeBytes(payload);
           wr.flush();
           wr.close();
           System.out.println("Connection status: " + connection.getHeaderFields());
           
           InputStream is = connection.getInputStream();
           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
           String line;
           while ((line = rd.readLine()) != null) {
           response.append(line);
           response.append('\r');
           }
           
           System.out.println("Generated Response :"+response.toString());
           rd.close();
    } 
    catch (Exception ex) {
    	System.out.println("Exception : " + ex.toString());
    }
		//return null;
		 return response.toString();
	   
	}
	
	//Push Notification
	private static void pushMessageNotification(JSONArray arr,String token)
	{
        URL url;
        HttpURLConnection connection = null;
        try {
        	//Get auth header
        	// TODO code application logic here
           url = new URL("http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/message/send/batch");
           connection = (HttpURLConnection) url.openConnection();
           connection.setRequestMethod("POST");
           connection.setConnectTimeout(5000);
           connection.setRequestProperty("Auth-Token", token);
           connection.setUseCaches(false);
           connection.setDoInput(true);
           connection.setDoOutput(true);  
         //
           // Send request
           DataOutputStream wr = new DataOutputStream(
           connection.getOutputStream());
           wr.writeBytes(arr.toString());
           wr.flush();
           wr.close();
           
           InputStream is = connection.getInputStream();
           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
           String line;
           StringBuffer response = new StringBuffer();
           while ((line = rd.readLine()) != null) {
           response.append(line);
           response.append('\r');
           }
           System.out.println("Response :"+response.toString());
           rd.close();
       
    } catch (IOException ex) {
    	System.out.println(ex.toString());
    }
	}

private String mintPayment(String arr) {
URL url;
HttpURLConnection connection = null;
String stringResponse = "";
try {
	// Get auth header
	// TODO code application logic here
	url = new URL("https://gateway-sandbox.pmnts.io/v1.0/purchases");
	connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");
	connection.setConnectTimeout(5000);
	connection.setRequestProperty("Authorization", "Basic bWludC1kbXNxOjc5NzQ2NDM1YjgxZWZiZWY2NDdlNGRhM2NiNGE4ZjI0");
	connection.setUseCaches(false);
	connection.setDoInput(true);
	connection.setDoOutput(true);
	//
	// Send request
	DataOutputStream wr = new DataOutputStream(
			connection.getOutputStream());
	logger.debug(arr);
	wr.writeBytes(arr);
	wr.flush();
	wr.close();

	InputStream is = connection.getInputStream();
	BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	String line;
	StringBuffer response = new StringBuffer();
	while ((line = rd.readLine()) != null) {
		response.append(line);
		response.append('\r');
	}
	stringResponse = response.toString();
	System.out.println("Response :" + response.toString());
	rd.close();

} catch (IOException ex) {
	System.out.println(ex.toString());
}

return stringResponse;
}

	
}



