package au.com.dmsq.web;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.File;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.EventCalendarItemService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.FileService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;

@Controller
public class BulkActivitiesController extends AbstractController{
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired EventService eventService;
	@Autowired EventCalendarItemService eventCalendarItemService;
	@Autowired ActivityService activityService;
	@Autowired LayerService layerService;
	@Autowired AccountService accountService;
	@Autowired BannerService bannerService;
	@Autowired FileService fileService;
	@Autowired MailService mailService;
	@Autowired MailContentService mcs;
	@Autowired EventController eventsController;
	
	@Transactional
	@RequestMapping(value="/admin/bulk/save", method= RequestMethod.POST)
	public ModelAndView saveBE(@ModelAttribute("newEvent") @Valid Event newEvent, BindingResult result, SessionStatus status,  
		@RequestParam(value="commodities") Integer[] commodities,		
		@RequestParam(value="bulkFile",			 defaultValue="", required =false) MultipartFile bulkFile,				
		@RequestParam(value="fileId[]", 		 defaultValue="", required =false) Integer[] fileIds,
		@RequestParam(value="titleFile[]", 		 defaultValue="", required =false) String[] titleFile,		
		@RequestParam(value="descriptionFile[]", defaultValue="", required =false) String[] descriptionFile,
		@RequestParam(value="fileUrl[]", 		 defaultValue="", required =false) String[] urlFile, 
		@RequestParam(value="videoId[]",		 defaultValue="", required =false) Integer[] videoIds,	
		@RequestParam(value="titleVideo[]", 	 defaultValue="", required =false) String[] titleVideo,	
		@RequestParam(value="descriptionVideo[]",defaultValue="", required =false) String[] descriptionVideo,
		@RequestParam(value="videoUrl[]", 		 defaultValue="", required =false) String[] urlVideo, 					
		@RequestParam(value="videoIconUrl[]",	 defaultValue="", required =false) String[] videoIconUrl,
		@RequestParam(value="bannerIds[]",		 defaultValue="", required =false) Integer[] bannerIds,
		@RequestParam(value="titleBanner[]", 	 defaultValue="", required =false) String[] titleBanner,	
		@RequestParam(value="subtitleBanner[]",  defaultValue="", required =false) String[] subtitleBanner,	
		@RequestParam(value="bannerUrl[]", 		 defaultValue="", required =false) String[] urlBanner, 
		@RequestParam(value="accountId") Integer accountId
		) throws ServletException, IOException, SQLException, Exception{
		
		logger.debug("------------------------------ Admin - Save Bulk Event ------------------------------");
		
		ModelAndView mav = setBasicData();
		Account acct = getCurrentAccount();
		
		boolean isNew = true;
		if (newEvent.getActivityId() != null )
				isNew = false;

		if (result.hasErrors()) {
			mav.setViewName("/admin/event/newEvent"); //jsp
			return mav;
		}
		else{
			
			System.out.println("Booking     : " + newEvent.getBookable());
			System.out.println("Recommended : " + newEvent.getActivity().getRecommended());
			System.out.println("Manager 	: " + accountId );
			for(Integer commId:commodities){
				System.out.println("Communities : " + commId);
			}
			System.out.println("Icon 		: " + newEvent.getActivity().getIconUrl() );
			
			System.out.println("BannerIds : " + bannerIds.length);
			for(Integer bannId:bannerIds){
				System.out.println("Banners : " + bannId);
			}

			System.out.println("FileIds : " + bannerIds.length);
			for(Integer fileId:fileIds){
				System.out.println("Files : " + fileId);
			}

			System.out.println("VIdeoIds : " + bannerIds.length);
			for(Integer videoId:videoIds){
				System.out.println("Videos : " + videoId);
			}

			System.out.println("CSV File 	: " + bulkFile);
	        
	        mav = eventsController.events();    
		}
        
		ArrayList<Activity> activities = new ArrayList<Activity>();
		BufferedReader br = null;
		String line = "";
		try{
			java.io.File file = new java.io.File(servletContext.getRealPath("/") + "/"+ bulkFile);
			System.out.println("bulkFileName:  " + bulkFile.getOriginalFilename());
			System.out.println("Path:  " + file.toString());
			
			org.apache.commons.io.FileUtils.writeByteArrayToFile(file, bulkFile.getBytes());
			System.out.println("Go to the location:  " + file.toString()+ " on your computer and verify that the image has been stored.");
			
			System.out.println(">>>>>>>>>>>>>>>>>> EXCEL READ STARTS HERE <<<<<<<<<<<<<<<<<<<<");     
			//FileInputStream filee = new FileInputStream(new java.io.File("C:\\Users\\Carlos\\Desktop\\DMSq Working\\BulkUpload\\ColumnSpecificationEvents2.xls"));
			FileInputStream filee = new FileInputStream(file);
		    HSSFWorkbook workbook = new HSSFWorkbook(filee); //Get the workbook instance for XLS file
		    HSSFSheet sheet = workbook.getSheetAt(0); //Get first sheet from the workbook
		    Iterator<Row> rowIterator = sheet.iterator(); //Iterate through each rows from first sheet
		    
		    
		    while(rowIterator.hasNext()) {
		        Row row = rowIterator.next();
		        if(row.getRowNum()>0){
		        	String title= row.getCell(0).getRichStringCellValue().getString();		
					String content= row.getCell(1).getRichStringCellValue().getString();	
					String type= "event";		
					String icon= newEvent.getActivity().getIconUrl();	
					Account owner = accountService.findById(accountId);	
					Set<File> files = new HashSet<>();
					Set<Banner> banners = new HashSet<>(); 
					Set<Layer> layers = new HashSet<>();
					
					for( int x=0; x<urlFile.length; x++ ){
						File newFile = new File();
						newFile.setTitle(titleFile[x]);
						newFile.setDescription(descriptionFile[x]);	
						newFile.setUrl(urlFile[x]);					
						newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/30d2b739-3137-4a3b-b07f-48cdfad5c8cf-pdf.png");									
						fileService.save(newFile);
						files.add(newFile);
					}
					for( int y=0; y<urlVideo.length; y++ ){
						File newFile = new File();
						newFile.setTitle(titleVideo[y]);
						newFile.setDescription(descriptionVideo[y]);
						newFile.setUrl(urlVideo[y]);
						newFile.setIconUrl(videoIconUrl[y]);
						fileService.save(newFile);
						files.add(newFile);
					}
					for( int z=0; z<bannerIds.length; z++ ){
						Banner newBanner = new Banner();
						newBanner.setImageUrl(urlBanner[z]);
						newBanner.setTitle(titleBanner[z]);
						newBanner.setSubtitle(subtitleBanner[z]);
						bannerService.save(newBanner);
						banners.add(newBanner);
					}
					for( int w=0; w<commodities.length; w++ ){
						layers.add(layerService.findById(commodities[w]));
					}
					
					Activity activity = new Activity(title, content, type, icon, owner, files, banners, layers);
					activity.setCreatedAccountId(acct.getId());
					
					java.util.Date date= new java.util.Date();
					activity.setCreatedDatetime(date);
					activity.setDeleted(false);
					activity.setActionUrl("NotUSed");
					activity.setActionTitle("NotUsed");
					activity.setRecommended(newEvent.getActivity().getRecommended());
					
					String address=row.getCell(2).getRichStringCellValue().getString();
					String contactEmail=row.getCell(3).getRichStringCellValue().getString();
					String contactName=row.getCell(4).getRichStringCellValue().getString();
					String contactPhone=row.getCell(5).getRichStringCellValue().getString();
					String organisation=row.getCell(6).getRichStringCellValue().getString();
					String cost=row.getCell(7).getRichStringCellValue().getString();
					activityService.save(activity); 
					
					Event event = new Event(address, contactEmail, contactName, contactPhone, organisation, cost);
					final Geocoder geocoder = new Geocoder();
					if( !address.contains("Australia") ){
						address = newEvent.getAddress()+", Adelaide, Australia";
						event.setAddress(address);
					} 
					GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(address).setLanguage("en").getGeocoderRequest();
					GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
					List<GeocoderResult> results = geocoderResponse.getResults();
					event.setAddressLat(new BigDecimal(results.get(0).getGeometry().getLocation().getLat().floatValue()));
					event.setAddressLng(new BigDecimal(results.get(0).getGeometry().getLocation().getLng().floatValue()));
					event.setBookable(newEvent.getBookable());
					event.setActivity(activity);
					
					activity.setEvent(event);
					eventService.save(event);   
					
					activities.add(activity);
					
					
					
					DataFormatter formatter = new DataFormatter(Locale.UK); // "dd/mm/yyyy"
					SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
					SimpleDateFormat stf = new SimpleDateFormat("HH:mm:ss");
					
					Cell sD= row.getCell(8);
					String startDate = formatter.formatCellValue(sD);		
					String startDateBaikal = df.format(row.getCell(8).getDateCellValue());
					
					String startTime = stf.format(row.getCell(9).getDateCellValue());
					Cell eD= row.getCell(10);
					String endDate = formatter.formatCellValue(eD);
					String endDateBaikal = df.format(row.getCell(10).getDateCellValue());
					String endTime = stf.format(row.getCell(11).getDateCellValue());
					
					List<EventCalendarItem> ecis = eventCalendarItemService.createEventDatesBulk(activity.getId(), startDate, startTime, 
							endDate, endTime, newEvent);
					
					for(int z=0; z<ecis.size();z++){
						System.out.println("INSERT BAIKAL startDate>>>"+startDateBaikal);
						System.out.println("INSERT BAIKAL startDate>>>"+startTime);
						System.out.println("INSERT BAIKAL endDate>>>  "+endDateBaikal);
						System.out.println("INSERT BAIKAL endTime>>>  "+endTime);
						eventCalendarItemService.toBaikal(ecis.get(z).getCalEtag(), (startDateBaikal+" "+startTime), (endDateBaikal+
								" "+endTime),	ecis.get(z).getCalData(), ecis.get(z).getId().getCalHref());
						System.out.println("-- End of insert to Baikal --");
					}
					
					String connectionURL = getURLDB();
					java.sql.Connection connection = null; 
			        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			        connection = DriverManager.getConnection(connectionURL, getUSER(), getPASSWORD());
			        
					if(!connection.isClosed()){
			        	Statement stmt = connection.createStatement();
				        for(int y=0; y<ecis.size();y++){    	
							String insertRange ="INSERT INTO `"+getDB()+"`.`event_calendar_item` (`activity_id`, `cal_href`, " +
								"`cal_etag`, `cal_data`, `dtend`) "+"VALUES ("+ecis.get(y).getId().getActivityId()+", '"+
								ecis.get(y).getId().getCalHref()+"', '"+ecis.get(y).getCalEtag()+"', '"+ecis.get(y).getCalData()+"', '"+
								ecis.get(y).getDtend()+"')";
							stmt.executeUpdate(insertRange);
							System.out.println("INSERT ECI>>>"+insertRange);
						}
					}
		        }
		    }

		    filee.close();
			    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}	
		mav.addObject("activities", activities);
		mav.setViewName("/admin/bulk/eventsInFile"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/bulk/bulkLoad")
	public ModelAndView newEvent() throws Exception {
		
		logger.debug("------------------------------ Admin - Bulk Load ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		Activity newAct = new Activity();
		Event newEv = new Event(newAct);
		newEv.setActivity(newAct);
		newAct.setEvent(newEv);
		
		System.out.println("From Controller newEvent - newEv.getActivityId : " + newEv.getActivity().getId());
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		// Todo validate current session other will return null and the screen will crash		
		Account acct = accountService.getbyIdWithLayersManaged(getCurrentAccount().getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layers){
			setAccounts.addAll(layerService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		mav.addObject("icons", activityService.getIcons(acct.getLayers()));	
		mav.addObject("iUrl", 0);	
		mav.addObject("activity", newAct);
		mav.addObject("newEvent", newEv);
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		mav.addObject("listOfOwners", listofManagers);
		mav.addObject("managedLayers", layers);
		
		mav.setViewName("/admin/bulk/bulkLoad"); //jsp
		return mav; 
	}
	
//	
//	@RequestMapping(value="/admin/eventsInFile")
//	public ModelAndView newEvent(HttpServletRequest request) throws Exception {
//		
//		logger.debug("------------------------------ Admin - New Bulk Events ------------------------------");
//		
//		ModelAndView mav = setBasicData();
//		
//		ArrayList<Activity> activities = new ArrayList<Activity>();
//		
//		//String file = "C:\\Users\\Endry\\Desktop\\Carlos\\BulkEvents\\ColumnSpecificationEvents.csv";
//		String file= servletContext.getRealPath("https://s3.amazonaws.com/ccmeresources/Bulk_Test_190615.csv");
//		
//		BufferedReader br = null;
//		String line = "";
//		
//		try {
//			
//    	    br = new BufferedReader(new FileReader(file));
//    	    
////    	    LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file));
////    	    lineNumberReader.skip(Long.MAX_VALUE);
////    	    int lines = lineNumberReader.getLineNumber();
//    	    
//			while ((line = br.readLine()) != null) {
//				String[] entries = line.split(",");
//				System.out.println("Entries: " + entries.length);
//
//				if(entries.length!=0 && !entries[0].equalsIgnoreCase("title")){
//					String title= entries[00];		
//					String content= entries[01];	
//					String type= entries[02];		
//					String icon= entries[03];		
//					Account owner = accountService.findById(new Integer (entries[04]));	
//					Set<File> files = new HashSet<>();
//					Set<Banner> banners = new HashSet<>(); 
//					Set<Layer> layers = new HashSet<>();
//					
//					for( int y=5; y<11; y++ ){
//						files.add(fileService.findById(new Integer (entries[y])));
//					}
//					for( int z=11; z<14; z++ ){
//						banners.add(bannerService.findById(new Integer (entries[z])));
//					}
//					for( int w=14; w<24; w++ ){
//						layers.add(layerService.findById(new Integer (entries[w])));
//					}
//					
//					Activity activity = new Activity(title, content, type, icon, owner, files, banners, layers);
//					
//					String address=entries[24];
//					String contactEmail=entries[25];
//					String contactName=entries[26];
//					String contactPhone=entries[27];
//					String organisation=entries[28];
//					String cost=entries[29];
//					
//					Event event = new Event(address, contactEmail, contactName, contactPhone, organisation, cost);
//					
//					activity.setEvent(event);
//					activities.add(activity);
//				}
//			}
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			if (br != null) {
//				try {
//					br.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}	
//		mav.addObject("activities", activities);
//		mav.setViewName("/admin/bulk/eventsInFile"); //jsp
//		return mav; 
//	}
	
}


