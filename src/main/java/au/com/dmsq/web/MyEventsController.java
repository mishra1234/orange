package au.com.dmsq.web;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.EventDTO;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.MessageService;

@Controller
public class MyEventsController extends AbstractController{
	
	@Autowired AccountService accountService;
	@Autowired ActivityService activityService;
	@Autowired MessageService messageService;
	
	public Integer pork=70515;
	public Integer dairy=70514;
	public Integer horticulture=70512;
	public Integer livestock=70511;
	public Integer grain=70510;
	public Integer wine=70513;
	public Integer gatedPork=70521;
	public Integer gatedDairy=70520;
	public Integer gatedHorticulture=70518;
	public Integer gatedLivestock=70517;
	public Integer gatedGrain=70516;
	public Integer gatedWine=70519;

	private static final Logger logger = LoggerFactory.getLogger(MyEventsController.class);
	
	
	@ResponseBody 
	@RequestMapping(method={RequestMethod.GET}, value={"/api/myEvents"})
	List<EventDTO> getMyEvents(){

		List<EventDTO> myEventList = new ArrayList<EventDTO>();

		try {

			List<Activity> activities = new ArrayList<Activity>();
			
			Account account = getCurrentAccount();
			Account accountWR = accountService.getbyIdWithLayers(account.getId());
			Set<Layer> layers = accountWR.getLayers_1();	//Obtain the connected layers of the logged user
			List<Integer> comms = new ArrayList<Integer>();
			for(Layer e:layers){
				if(e.getId()>70509&&e.getId()<70516){
					comms.add(e.getId());
				}
			}
			activities.addAll(activityService.getEventsByCommIds(comms)); //Get the activities of the layers
			
			Set<AccountLayerAccess> gatedLayers = accountWR.getAccountLayerAccesses();   //Obtain the gated layers of logged user
			List<Integer> gatedComms = new ArrayList<Integer>();
			for(AccountLayerAccess e:gatedLayers){
				if(e.getLayer().getId()>70515 && e.getLayer().getId()<705122 && e.getStatus().equalsIgnoreCase("granted")){
					gatedComms.add(e.getLayer().getId());
				}
			}
			activities.addAll(activityService.getEventsByCommIds(gatedComms)); //Get the activities with granted access of the layers
			
			List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities); //Gets all the activities
			Set<Activity> latestActivitiesSet = new HashSet<Activity>();	//To delete the repeated activities
			for(Activity act:latestActivities){
				latestActivitiesSet.add(act);
			}
			List<Activity> latestActivitiesFinal = new ArrayList<Activity>();	//We need the activities in a list to send to the comparator
			
			for(Activity act:latestActivitiesSet){
				latestActivitiesFinal.add(act);
			}
			Collections.sort(latestActivitiesFinal, Activity.ActivityComparatorD);

			myEventList.addAll(activityService.generateEventDTOs(latestActivitiesFinal));
			
		} catch (ParseException e) {
			
			logger.error("Error retrieving EventsDTOs");
		}

		return myEventList;
	}
	
	
	@RequestMapping(method={RequestMethod.GET}, value={"/api/myComunity/Events"})
	@ResponseBody public List<EventDTO> getCommEvents 
	(@RequestParam(value="userId", required=false, defaultValue="0") String userId , @RequestParam("communityId")String communityId){	
		
		List<EventDTO> myEventList = new ArrayList<EventDTO>();

		try {
			Integer community = new Integer(communityId);
			List<Activity> activities = new ArrayList<Activity>();
			
			Integer user = new Integer(userId);
			if(user!=0){										//ie... http://localhost:8080/rural/api/myComunity/70521/Events/206
				Account accountWR = accountService.getbyIdWithLayers(new Integer(userId));
				Set<Layer> layers = accountWR.getLayers_1();	//Obtain the connected layers of the logged user
				List<Integer> comms = new ArrayList<Integer>();
				for(Layer e:layers){
					if(community>70509 && community<70516){
						comms.add(community);
					}
				}
				activities.addAll(activityService.getEventsByCommIds(comms)); //Get the activities of the layers
			
				Set<AccountLayerAccess> gatedLayers = accountWR.getAccountLayerAccesses();   //Obtain the gated layers of logged user
				List<Integer> gatedComms = new ArrayList<Integer>();
				for(AccountLayerAccess e:gatedLayers){
					if(e.getLayer().getId()>70515 && e.getLayer().getId()<70522 && e.getStatus().equalsIgnoreCase("granted")){
						gatedComms.add(e.getLayer().getId());
					}
				}
				activities.addAll(activityService.getEventsByCommIds(gatedComms)); //Get the activities with granted access of the layers
			}
			if(user==0 && community>70509 && community<70516){  //ie... http://localhost:8080/rural/api/myComunity/70515/Events/0
				List<Integer> comms = new ArrayList<Integer>();
					comms.add(community);
				activities.addAll(activityService.getEventsByCommIds(comms)); //Get the activities of the public layers
			}
			
			List<Activity> latestActivities = activityService.getStartandEndDatesfromEvents(activities); //Gets all the activities
			Set<Activity> latestActivitiesSet = new HashSet<Activity>();	//To delete the repeated activities
			for(Activity act:latestActivities){
				latestActivitiesSet.add(act);
			}
			List<Activity> latestActivitiesFinal = new ArrayList<Activity>();	//We need the activities in a list to send to the comparator
			
			for(Activity act:latestActivitiesSet){
				latestActivitiesFinal.add(act);
			}
			Collections.sort(latestActivitiesFinal, Activity.ActivityComparatorD);

			myEventList.addAll(activityService.generateEventDTOs(latestActivitiesFinal));
			
		} catch (ParseException e) {
			logger.error("Error retrieving EventsDTOs");
		}

		return myEventList;
	}
	
	
	@RequestMapping(value="/member/calendar", method= RequestMethod.GET)
	public ModelAndView showCalendar() {
		
		logger.debug("------------------------------ Admin - List Accounts ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		mav.setViewName("/member/calendar");
		
		return mav; 
	}
	
}
