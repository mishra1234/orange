package au.com.dmsq.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.io.*;
import java.math.BigDecimal;
import java.sql.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import javax.validation.Valid;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.PropertyList;
import net.fortuna.ical4j.model.Recur;
import net.fortuna.ical4j.model.WeekDay;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import edu.emory.mathcs.backport.java.util.Collections;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.File;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.EventCalendarItemId;
import au.com.dmsq.model.IconDTO;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.EventCalendarItemService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.FileService;


@Controller
public class InfoController extends AbstractController{
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired EventService eventService;
	@Autowired EventCalendarItemService eventCalendarItemService;
	@Autowired ActivityService activityService;
	@Autowired LayerService layerService;
	@Autowired AccountService accountService;
	@Autowired BannerService bannerService;
	@Autowired FileService fileService;
	@Autowired CommunityController commController;
	@Autowired ErrorsController errorsController;
	
	@Transactional
	
//	@RequestMapping(value="/admin/infos/{layerId}/{curr}", method= RequestMethod.GET)
//	public ModelAndView infos(@PathVariable(value="layerId") Integer layerId, @PathVariable(value="curr") Integer current ) {
//		
//		logger.debug("------------------------------Admin - List of Activities Information ------------------------------");
//		
//		ModelAndView mav = new ModelAndView(); 	
//		
//		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		List<Account> accounts = accountService.findByEmail(user.getUsername());
//		
//		for(Account str: accounts){
//			if(accounts.isEmpty()){
//				return mav;
//			}
//		}
//		Account acct = accounts.get(0);
//		
//		Integer max = 20;
//		Integer initial=( (activityService.countInf(acct.getId()))-max);
//		Integer prev=1;
//		Integer next=2;
//		Integer curr=current;
//		Integer hmany = (activityService.countInf(acct.getId())/max);
//		List<Activity> pagedInfos= activityService.pagedInfosByLayer(initial, max, acct.getId(), layerId);
//		mav.addObject("myActivitiesList", pagedInfos);
//		mav.addObject("prev", prev);
//		mav.addObject("next", next);
//		mav.addObject("hmany", hmany);
//		mav.addObject("curr", curr);
//		
//		List<Integer> numbers = new ArrayList<Integer>();
//		for (Integer i=0; i< hmany+1; i++){
//			numbers.add(i+1);
//		}
//		mav.addObject("hmany", hmany);
//		mav.addObject("numbers", numbers);
//		
//		mav.setViewName("/admin/info/infos"); // jsp
//		return mav; 
//	}
	
	
	@RequestMapping(value="/admin/infos/{layerId}/{curr}", method= RequestMethod.GET)
	public ModelAndView pagedInfos(@PathVariable(value="layerId") Integer layerId, @PathVariable(value="curr") Integer curr ) throws Exception {
		
		logger.debug("------------------------------Admin - List Paged Infos ------------------------------");
		ModelAndView mav = new ModelAndView(); 
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accounts.get(0);
		
		Integer max = 20;
		Integer next = 2;
		Integer prev = 1;
		Integer initial= 0;
		Integer hmany = (activityService.countInf(acct.getId())/max)+1;
		
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i=1; i< hmany+1; i++){
			numbers.add(i);
		}
		
		if(curr==1 || curr<1){
			initial=0;
			prev=1;
			next=2;
			List<Activity> pagedInfs= activityService.pagedInfosByLayer(initial, max, acct.getId(), layerId);
			mav.addObject("myActivitiesList", pagedInfs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr>1 && curr < hmany){
			max=(curr)*20;
			initial= (curr-1)*20;
			prev=curr-1;
			next=curr+1;
			List<Activity> pagedInfs= activityService.pagedInfosByLayer(initial, max, acct.getId(), layerId);
			mav.addObject("myActivitiesList", pagedInfs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		else if(curr==hmany){
			initial= (curr-1)*20;
			max=initial + (activityService.pagedInfosByLayer(initial, activityService.countInf(acct.getId())%max,acct.getId(),layerId).size())%20;
			prev=curr-1;
			next=curr+1;
			List<Activity> pagedInfs= activityService.pagedInfosByLayer(initial, activityService.countInf(acct.getId())%max,acct.getId(),layerId);
			mav.addObject("myActivitiesList", pagedInfs);
			mav.addObject("prev", prev);
			mav.addObject("next", next);
		}
		
		mav.addObject("hmany", hmany);
		mav.addObject("curr", curr);
		mav.addObject("numbers", numbers);
		
		mav.setViewName("/admin/info/infos"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/member/infos", method= RequestMethod.GET)
	public ModelAndView infosMember() {
		
		logger.debug("------------------------------Member - List of Activities Information ------------------------------");
		
		ModelAndView mav = setBasicData();
		
		if (getCurrentAccount() != null) {
			mav.addObject("acct", getCurrentAccount());
		}
		
		List<Activity> latestInfos = new ArrayList<Activity>();
		List<Integer> allConnected = (List<Integer>) mav.getModel().get("allConnected");
		latestInfos.addAll(activityService.getInfosByCommIds(allConnected));
		
		mav.addObject("infos", latestInfos);
		mav.setViewName("/member/info/infos"); // jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/info/{id}", method= RequestMethod.GET)
	public ModelAndView getInfo(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------Admin Get Single Info  ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity act = activityService.findByIdWithBanners(id); 

		Set<Banner> bannList = new HashSet<Banner>();
		for (Banner ids: act.getBanners()){
			bannList.add(bannerService.get(ids.getId()));
		}
		
		Set<File> fileList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getUrl().endsWith(".pdf")){
				fileList.add(fileService.findById(ids.getId()));
			}
		}
		Set<File> videoList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getUrl().endsWith(".mp4")){
				videoList.add(fileService.findById(ids.getId()));
			}
		}
		Set<File> weblinksList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getType().equalsIgnoreCase("weblink")){
				weblinksList.add(fileService.findById(ids.getId()));
			}
		}
		
		mav.addObject("listOfFiles", fileList);
		mav.addObject("listOfVideos", videoList);
		mav.addObject("listOfWeblinks", weblinksList);
		mav.addObject("listOfBanners", bannList);
		mav.addObject("inf", act);
		mav.setViewName("/admin/info/info"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/member/info/{id}", method= RequestMethod.GET)
	public ModelAndView getEventMember(@PathVariable(value="id") Integer id) {
		
		logger.debug("------------------------------ Member Get Single Info  ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		Activity act = activityService.findByIdWithBanners(id); 

		Set<Banner> bannList = new HashSet<Banner>();
		for (Banner ids: act.getBanners()){
			bannList.add(bannerService.get(ids.getId()));
		}
		Set<File> fileList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getUrl().endsWith(".pdf")){
				fileList.add(fileService.findById(ids.getId()));
			}
		}
		Set<File> videoList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getUrl().endsWith(".mp4")){
				videoList.add(fileService.findById(ids.getId()));
			}
		}
		Set<File> weblinksList = new HashSet<File>();
		for (File ids: act.getFiles()){
			if(ids.getType().equalsIgnoreCase("weblink")){
				weblinksList.add(fileService.findById(ids.getId()));
			}
		}
		
		mav.addObject("listOfFiles", fileList);
		mav.addObject("listOfVideos", videoList);
		mav.addObject("listOfWeblinks", weblinksList);
		mav.addObject("listOfBanners", bannList);
		mav.addObject("inf", act);
		mav.setViewName("/member/info/info"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/info/save", method= RequestMethod.POST)
	public String save(@ModelAttribute("newActInfo") @Valid Activity newActInfo, BindingResult result, SessionStatus status, 
			@RequestParam("commodities") String[] commodities,
			@RequestParam(value="titleBanner[]", 	 defaultValue="", 	required=false) String[] titleBanner,	
			@RequestParam(value="subtitleBanner[]",  defaultValue="", 	required=false) String[] subtitleBanner,	
			@RequestParam(value="bannerUrl[]", 		 defaultValue="", 	required=false) String[] urlBanner, 		
			@RequestParam(value="bannerRemove[]", 	 defaultValue="", 	required=false) String[] bannerRemove, 
			@RequestParam(value="bannerId[]", 		 defaultValue="", 	required=false) Integer[] bannerId,
			@RequestParam("titleFile[]") String[] titleFile,		
			@RequestParam("descriptionFile[]") String[] descriptionFile,	
			@RequestParam("fileUrl[]") String[] urlFile, 			
			@RequestParam("fileRemove[]") String[] fileRemove, 
			@RequestParam("fileId[]") Integer[] fileId,
			@RequestParam("titleVideo[]") String[] titleVideo,		
			@RequestParam("descriptionVideo[]") String[] descriptionVideo,	
			@RequestParam("videoUrl[]") String[] urlVideo, 			
			@RequestParam("videoRemove[]") String[] videoRemove, 
			@RequestParam("videoId[]") Integer[] videoId,			
			@RequestParam("videoIconUrl[]") String[] videoIconUrl,
			@RequestParam(value="titleWL[]", 	 	 defaultValue="", 		required=false) String[]  titleWL,	
			@RequestParam(value="descriptionWL[]",	 defaultValue="", 		required=false) String[]  descriptionWL,	
			@RequestParam(value="urlWL[]", 		 	 defaultValue="", 		required=false) String[]  urlWL, 		
			@RequestParam(value="idWL[]", 		 	 defaultValue="", 		required=false) String[]  idWL,
			@RequestParam(value="commId" , 	 		 defaultValue="", 		required=false) Integer   commId,
			@RequestParam(value="another",			 defaultValue="false",  required=false) boolean   another
			)throws SQLException, Exception{
		
		logger.debug("------------------------------ Admin - Save Info ------------------------------");
		
		ModelAndView mav = setBasicData();
		Account acct = getCurrentAccount();
		
		for (Integer ids: bannerId){
			logger.debug("->Banner Ids:" + ids);
		}
		
		try{
			boolean isNew = true;
			if (newActInfo.getId() != null) isNew=false;
	
			if (result.hasErrors()) {
				mav.setViewName("/admin/info/newInfo"); //jsp
				return "redirect:/admin/event/newEvent";
			}else{
				Activity saveActivity = new Activity();
				
		        if(isNew){	
					saveActivity = newActInfo;
					
					if(newActInfo.getContentTitle().equalsIgnoreCase("")){
						saveActivity.setContentTitle("Hot News by "+acct.getName());
					}
					if(newActInfo.getRecommended()==null){
						saveActivity.setRecommended(false);
					}
					if(newActInfo.getIconUrl().equalsIgnoreCase("")){
						saveActivity.setIconUrl("https://s3.amazonaws.com/ccmeresources/ca024ce7-b753-4351-9e83-45cdf0cbdbdd-logoRuralConnect.png");
					}
					
					
				}else{
		        	Activity oldInfo = activityService.findByIdWithRelations(newActInfo.getId());
		        	saveActivity.setId(newActInfo.getId());
		        	saveActivity=newActInfo;
		        	saveActivity.setLayers(oldInfo.getLayers());
		        	saveActivity.setLayers_1(oldInfo.getLayers_1());
					saveActivity.setAccounts(oldInfo.getAccounts());
					saveActivity.setActivitiesForActivityId(oldInfo.getActivitiesForActivityId());
					saveActivity.setBanners(oldInfo.getBanners());
		        }
				saveActivity.setIconUrl(newActInfo.getIconUrl());
				
				String s2 = StringEscapeUtils.escapeXml(saveActivity.getContentTitle()); 
				saveActivity.setContentTitle(s2);
				String s3 = StringEscapeUtils.escapeHtml(saveActivity.getContentBody()); 
				saveActivity.setContentBody(s3);
				
				Date currentDT= new Date();
		        System.out.println("Current Time Stamp: "+new Timestamp(currentDT.getTime()));
		        
				saveActivity.setCreatedDatetime(currentDT);
				
				if(urlBanner.length!=0){
					Set<Banner> banners = new HashSet<Banner>();
					for(int ba=0; ba<urlBanner.length; ba++){
						Banner newBanner = new Banner();
						System.out.println("VAR bannerId #: "+ bannerId.length);
						System.out.println("VAR BA: "+ ba);
						
						if(bannerId.length > ba && !bannerId[ba].equals("")){
							System.out.println("Var BA: "+ ba);
							System.out.println("Var BannerId[ba]: "+ bannerId[ba]);
							System.out.println("Banner Id: "+ newBanner.getId());
							newBanner=bannerService.findById(new Integer(bannerId[ba]));
							System.out.println("Banner: "+ titleBanner[ba]);
							newBanner.setTitle(titleBanner[ba]);			
							System.out.println("Banner: "+ subtitleBanner[ba]);
							System.out.println("Banner: "+ urlBanner[ba]);
							newBanner.setSubtitle(subtitleBanner[ba]);		  
							bannerService.save(newBanner);
						}
						else{
							System.out.println("Var BA: "+ ba);
							//System.out.println("Var BannerId[ba]: "+ bannerId[ba]);
							//System.out.println("Banner Id: "+ newBanner.getId());
							System.out.println("Banner: "+ titleBanner[ba]);
							newBanner.setTitle(titleBanner[ba]);		
							System.out.println("Banner: "+ subtitleBanner[ba]);
							newBanner.setSubtitle(subtitleBanner[ba]);	
							System.out.println("Banner: "+ urlBanner[ba]);
							newBanner.setImageUrl(urlBanner[ba]);		
							bannerService.save(newBanner);
						}
						banners.add(newBanner);
					}
					saveActivity.setBanners(banners);
				}
				if(urlBanner.length==0){
					Set<Banner> banners = new HashSet<Banner>();
					banners.add(bannerService.findById(3));
					saveActivity.setBanners(banners);
				}
				
				Set<File> allFiles = new HashSet<File>();
				
				if(fileId.length>1){
					Set<File> files = new HashSet<File>();
					for(int fi=1; fi<fileId.length; fi++){
						File newFile = new File();
						System.out.println("VAR FI: "+ fi);
						if(fileId[fi]==null && urlFile[fi]==null){
							System.out.println("File has no id, title or description, therefore not saved");
						}
						if(fileId[fi]==null && urlFile[fi]!=null){
							newFile.setTitle(titleFile[fi]);				System.out.println("TitleFile: "+ titleFile[fi]);
							newFile.setDescription(descriptionFile[fi]);	System.out.println("DescrFile: "+ descriptionFile[fi]);
							newFile.setUrl(urlFile[fi]);					System.out.println("urlFile: "+ urlFile[fi]);
							newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultPDF.jpg");
																			System.out.println("IconUrl: "+ newFile.getIconUrl());
						}
						if(fileId[fi]!=null && urlFile[fi]!=null){
							newFile=fileService.findById(fileId[fi]);		System.out.println("File Id: "+ newFile.getId());
							newFile.setTitle(titleFile[fi]);				System.out.println("File: "+ titleFile[fi]);
							newFile.setDescription(descriptionFile[fi]);	System.out.println("File: "+ descriptionFile[fi]);
																			System.out.println("File: "+ urlFile[fi]);  
						}
						fileService.save(newFile);
						files.add(newFile);
					}
					allFiles.addAll(files);
				}
				if(urlVideo.length>0){						System.out.println("UrlVideo length: "+ urlVideo.length);
					Set<File> filesV = new HashSet<File>();
					
					for(int num=0; num<urlVideo.length; num++){
						File newFile = new File();
						newFile.setUrl(urlVideo[num]);
						newFile.setIconUrl(videoIconUrl[num]);
						
						if(videoId[num]!=null){
							if(titleVideo[num]=="" && descriptionVideo[num]==""){     
								newFile=fileService.findById(new Integer(videoId[num]));
								newFile.setTitle("");
								newFile.setDescription("");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultVID.jpg");
							}else if(titleVideo[num]!="" && descriptionVideo[num]==""){	
								newFile=fileService.findById(new Integer(videoId[num]));
								newFile.setTitle(titleVideo[num]);
								newFile.setDescription("");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultVID.jpg");
							}else if(titleVideo[num]=="" && descriptionVideo[num]!=""){	
								newFile=fileService.findById(new Integer(videoId[num]));
								newFile.setTitle("");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultVID.jpg");
								newFile.setDescription(descriptionVideo[num]);		
							}else{														
								newFile=fileService.findById(new Integer(videoId[num]));
								newFile.setTitle(titleVideo[num]);			
								newFile.setDescription(descriptionVideo[num]);
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultVID.jpg");
							}
						}else{		
							if(titleVideo[num]=="" && descriptionVideo[num]==""){		
								newFile.setTitle("");
								newFile.setDescription("");
							}else if(titleVideo[num]!="" && descriptionVideo[num]==""){	
								newFile.setTitle(titleVideo[num]);
								newFile.setDescription("");
							}else if(titleVideo[num]=="" && descriptionVideo[num]!=""){
								newFile.setTitle("");
								newFile.setDescription(descriptionVideo[num]);
							}else{														
								newFile.setTitle(titleVideo[num]);			
								newFile.setDescription(descriptionVideo[num]);
							}
						}
						fileService.save(newFile);
						filesV.add(newFile);
					}
					allFiles.addAll(filesV);
				}
				
				System.out.println("<<<<<< <<< WL >>>> >>>>>>>>");
				System.out.println("#urlWL    : "+ urlWL.length);
				System.out.println("#Titles   : "+ titleWL.length);
				System.out.println("#SubTitles: "+ descriptionWL.length);
				System.out.println("#Ids      : "+ idWL.length);
				
				if(urlWL.length!=0){						
					Set<File> filesW = new HashSet<File>();
					for(int num=0; num<urlWL.length; num++){
						if(!urlWL[num].equals("")){
							File newFile = new File();
							if(titleWL.length==0 && descriptionWL.length==0 && idWL.length==0){ System.out.println("<< OPT1 >>");
								newFile.setTitle(" ");
								newFile.setDescription(" ");
								newFile.setUrl(urlWL[num]);
								newFile.setType("weblink");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
							}if(titleWL.length==0 && descriptionWL.length==0 && idWL.length!=0){ System.out.println("<< OPT2 >>");
								newFile=fileService.findById(new Integer(idWL[num]));
								newFile.setTitle(" ");
								newFile.setDescription(" ");
								newFile.setUrl(urlWL[num]);
								newFile.setType("weblink");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
							}if(titleWL.length!=0 && descriptionWL.length==0 && idWL.length==0){ System.out.println("<< OPT3 >>");
								if(titleWL[num].equals("")) newFile.setTitle(titleWL[num]);
								else newFile.setTitle(titleWL[num]);
								newFile.setDescription(" ");
								newFile.setUrl(urlWL[num]);
								newFile.setType("weblink");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
							}if(titleWL.length!=0 && descriptionWL.length==0 && idWL.length!=0){ System.out.println("<< OPT4 >>");
								newFile=fileService.findById(new Integer(idWL[num]));
								if(titleWL[num].equals("")) newFile.setTitle(titleWL[num]);
								else newFile.setTitle(titleWL[num]);
								newFile.setDescription(" ");
								newFile.setUrl(urlWL[num]);
								newFile.setType("weblink");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
							}if(titleWL.length==0 && descriptionWL.length!=0 && idWL.length==0){ System.out.println("<< OPT5 >>");
								newFile.setTitle(" ");
								if(descriptionWL[num].equals("")) newFile.setDescription(" ");
								else newFile.setDescription(descriptionWL[num]);
								newFile.setUrl(urlWL[num]);
								newFile.setType("weblink");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
							}if(titleWL.length==0 && descriptionWL.length!=0 && idWL.length!=0){ System.out.println("<< OPT6 >>");
								newFile=fileService.findById(new Integer(idWL[num]));
								newFile.setTitle(" ");
								if(descriptionWL[num].equals("")) newFile.setDescription(" ");
								else newFile.setDescription(descriptionWL[num]);
								newFile.setUrl(urlWL[num]);
								newFile.setType("weblink");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
							}if(titleWL.length!=0 && descriptionWL.length!=0 && idWL.length==0){ System.out.println("<< OPT7 >>");
								if(titleWL[num].equals("")) newFile.setTitle(titleWL[num]);
								else newFile.setTitle(titleWL[num]);
								if(descriptionWL[num].equals("")) newFile.setDescription(" ");
								else newFile.setDescription(descriptionWL[num]);
								newFile.setUrl(urlWL[num]);
								newFile.setType("weblink");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
							}if(titleWL.length!=0 && descriptionWL.length!=0 && idWL.length!=0){ System.out.println("<< OPT8 >>");
								System.out.println("idWL: "+idWL[num].toString());
								System.out.println("titleWL: "+titleWL[num].toString());
								System.out.println("descriptionWL: "+descriptionWL[num].toString());
								System.out.println("urlWL: "+urlWL[num].toString());
								
								if(!idWL[num].equals("")){ newFile=fileService.findById(new Integer(idWL[num]));}
								if(!titleWL[num].equals("")){ newFile.setTitle(titleWL[num]); }
								else {newFile.setTitle(" ");}
								if(!descriptionWL[num].equals("")) { newFile.setDescription(descriptionWL[num]);}
								else {newFile.setDescription(" ");}
								if(!urlWL[num].equals("")) { newFile.setUrl(urlWL[num]);}
								newFile.setType("weblink");
								newFile.setIconUrl("https://s3.amazonaws.com/ccmeresources/000DefaultIconsCCME/defaultWL.jpg");
								fileService.save(newFile);
							}
							fileService.save(newFile);
							filesW.add(newFile);
						}
					}
					allFiles.addAll(filesW);
				}
				saveActivity.setFiles(allFiles);
					
				Set<Layer> commoditySet = new HashSet<Layer>();
				if (commodities != null) {
					for (String idCommodity:commodities){
						int count = 0; 
						try {
							for (Layer validate : commoditySet) {
								Integer id = validate.getId();
								if (id == Integer.parseInt(idCommodity)) {
									count++;
								}
							}
							if (count == 0) // no match
							{
								Layer tempLayer = layerService.findById(Integer
										.parseInt(idCommodity));
								if (tempLayer != null) {
	
									commoditySet.add(tempLayer);
								}
							}
							}catch(Exception e){
								logger.debug("Number Exception - adding commodity to event");
							}
					}
				}
				saveActivity.setLayers(commoditySet);
				
				mav.addObject("messageAct", activityService.save(saveActivity));
				//mav = commController.communities();
			}
			Layer comm = layerService.findById(commId);
			String ret="";
			if(another==false){
				ret="redirect:/admin/events/community/"+commId+"/"+comm.getName();
			}
			if(another==true){
				ret="redirect:/admin/info/new/"+commId+"";
			}
			return ret;
			
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			mav = errorsController.error500();
			return "redirect:/admin/errors/error-500";
		}
	}
	
	
	@RequestMapping(value="/admin/info/new/{commId}")
	public ModelAndView newInfo(@PathVariable(value="commId") Integer commId) throws Exception {
		
		logger.debug("------------------------------ Admin - New Info ------------------------------");
		
		ModelAndView mav = new ModelAndView(); 
		
		Activity newAct = new Activity();
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String startDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layers){
			setAccounts.addAll(layerService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		newAct.setOwnerCommunity(commId); // New info is set to be owned by the community passed as commID
		
		mav.addObject("commId", commId);
		mav.addObject("layers", layerService.getNonDeleted());
		mav.addObject("iUrl", 0);	
		mav.addObject("newActInfo", newAct);
		mav.addObject("currentTS", startDatea);
		mav.addObject("currentUser", acct.getId());
		mav.addObject("listOfOwners", listofManagers);
		mav.addObject("managedLayers", layers);
		
		mav.setViewName("/admin/info/newInfo"); //jsp
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/info/editInfo/{id}", method= RequestMethod.GET)
	public ModelAndView editInfo(@PathVariable(value="id") Integer id, Model model) throws Exception {
		
		logger.debug("------------------------------ Admin - Edit Info ------------------------------");
		System.out.println("Controller - EditEvent/ event id : " + id);
		
		ModelAndView mav = new ModelAndView();
			
		Activity newAct = activityService.findByIdWithRelations(id);
	
		mav.addObject("bannerList", newAct.getBanners());
		
		Set<File> pdfs = new HashSet<File>(); 
		for(File file:newAct.getFiles()){
			if(file.getUrl().contains(".pdf")){
				pdfs.add(file);
			}	
		}
		mav.addObject("filesList", pdfs);
		
		Set<File> videos = new HashSet<File>(); 
		for(File v:newAct.getFiles()){
			if(v.getUrl().contains(".mp4")){
				videos.add(v);
			}
		}
		
		mav.addObject("videosList", videos);
		
		Set<File> weblinks = new HashSet<File>(); 
		for(File q: newAct.getFiles()){
			File webAdd = q;
			if(webAdd.getType().equalsIgnoreCase("weblink")){
				weblinks.add(webAdd);
			}
		}
		mav.addObject("weblinksList", weblinks);
		
		for (Banner myBanner: newAct.getBanners()){
			logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+myBanner.getTitle());
		}
		
		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String currDatea = sdf.format(date);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Account> accounts = accountService.findByEmail(user.getUsername());
		
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return mav;
			}
		}
		Account acct = accountService.getbyIdWithLayersManaged(accounts.get(0).getId());
		
		System.out.println("Connected User >>>: "+acct.getEmail());
		Set<Layer> layers = acct.getLayers();
		Set<Account> listofManagers = new HashSet<Account>();
		Set<Account> setAccounts = new HashSet<Account>();
		Set<Integer> sett = new HashSet<Integer>();
		
		for(Layer community: layers){
			setAccounts.addAll(layerService.getLayerWithManagers(community.getId()).getAccounts());
		}
		for(Account account:setAccounts){
			sett.add(account.getId());
		}
		for(Integer intger:sett){
			listofManagers.add(accountService.get(intger));
		}
		
		mav.addObject("layers", newAct.getLayers());
		mav.addObject("newActInfo", newAct);
		mav.addObject("currentTS", currDatea);
		mav.addObject("currentUser", accounts.get(0).getId());
		mav.addObject("listOfOwners", listofManagers);
		mav.addObject("managedLayers", layers);
		
		mav.setViewName("/admin/info/newInfo"); //jsp	
		return mav; 
	}
	
	
	@RequestMapping(value="/admin/info/delete/{id}")
	public String delete(@PathVariable(value="id") Integer id) throws Exception {
		
		logger.debug("------------------------------ Admin - Delete Single Info ------------------------------");

		ModelAndView mav =  new ModelAndView();
		try{
			Activity actInfo = activityService.get(id); 
			actInfo.setDeleted(true);
			
			mav.addObject("message", activityService.save(actInfo));
			
			return "redirect:/admin/communities/";
			
		}catch (Exception e) {
			System.out.println("Exception : "+e);
			return "redirect:/admin/errors/500/";
		}
	}
	
}


