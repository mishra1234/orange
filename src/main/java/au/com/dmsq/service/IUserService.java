package au.com.dmsq.service;

import au.com.dmsq.model.UserDTO;

public interface IUserService {
	boolean registerNewUserAccount(UserDTO accountDto)     
		      throws Exception;
		}
