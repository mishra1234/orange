package au.com.dmsq.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import au.com.dmsq.dao.FileDao;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.File;
import au.com.dmsq.service.FileService;

@Service
@Transactional
public class FileServiceImpl implements FileService{
	
	@Autowired 
	FileDao fileDao;
	
	@Override
	public boolean save(File file) {
		return fileDao.save(file);
	}

	@Override
	public boolean deleteById(Integer id) {
		File file = findById(id);
		remove(file);
		return true;
	}
	
	@Override
	public void remove(File file){
		fileDao.remove(file);
	}

	@Override
	public List<File> findAll() {
		return fileDao.findAll();
	}
	
	@Override
	public File findById(Integer id) {
		return fileDao.find(id);
	}
	
	public List<File> getVideos()
	{
		Search search = new Search();
		search.addFilterILike("url", "*.mp4");
		search.addSort("id", true);
		return fileDao.search(search);
	}

}

