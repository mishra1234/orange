package au.com.dmsq.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.dao.ActivityCalendarDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.ActivityCalendar;
import au.com.dmsq.model.Booking;
import au.com.dmsq.service.ActivityCalendarService;

@Service
@Transactional
public class ActivityCalendarServiceImpl implements ActivityCalendarService{

	@Autowired ActivityCalendarDao activityCalendarDao;
	
	@Override
	public boolean save(ActivityCalendar activityCalendar) {
		return activityCalendarDao.save(activityCalendar);
	}

	@Override
	public ActivityCalendar get(Integer id) {
		return activityCalendarDao.find(id);
	}

	@Override
	public boolean deleteById(Integer id) {
		ActivityCalendar actCal = get(id);
		remove(actCal);
		return true;
	}

	@Override
	public List<ActivityCalendar> findAll() {
		return activityCalendarDao.findAll();
	}
	
	@Override
	public void remove(ActivityCalendar activityCalendar){
		activityCalendarDao.remove(activityCalendar);
	}
	
	public List<ActivityCalendar> getCurrent()
	{
		return activityCalendarDao.getCurrent();
	}
	
	@Override
	public List<ActivityCalendar> getScehdulesByActivityId(Integer activityId)
	{
		return activityCalendarDao.searchByActivityId(activityId);
	}
	
}

