package au.com.dmsq.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sendgrid.*;
import javax.servlet.ServletContext;
import au.com.dmsq.dao.MailDao;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Mail;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.PushService;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

@Service

public class PushServiceImpl implements PushService {
	
	@Override
	public void sendPush(String from, String subject, String message, String[] to, String layerId) {
		
		String payload = "["; 
		boolean second = false; 
		
		
		for (String x: to){
			if (second) payload +=",";
			second = true; 
			
		payload += "{";	
		payload += "from_id:\""+from+"\"";
		payload += "to_id:\""+x+"\"";
		payload += "subject:\""+subject+"\"";
		payload += "message:\""+message+"\"";
		payload += "layer_id:\""+layerId+"\"";
		payload += "notification:1";
		payload += "}";
		}
		payload += "]";
		
		String payloadTest = " {\"from_id\": 2, \"to_id\": 1, \"subject\": \"Message push test 2.\",\"message\": \"This is a test for push notification stuff and messages.\",\"email\": 0,\"layer_id\": 2002,\"notification\": 1}";
		
		
		/*
		 * Use fist: POST: http://gryffindor.digitalmarketsquare.com/api/?request=/api/message
		 * 
		SERVER: http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/
			POST: {{server}}/api/?request=/api/message
			
			*/
		
	}
	
	private void sendPost() throws Exception {
		 
		String USER_AGENT = "Mozilla/5.0";
		
		String url = "https://selfsolve.apple.com/wcResults.do";
 
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
 
		// add header
		post.setHeader("User-Agent", USER_AGENT);
 
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("sn", "C02G8416DRJM"));
		urlParameters.add(new BasicNameValuePair("cn", ""));
		urlParameters.add(new BasicNameValuePair("locale", ""));
		urlParameters.add(new BasicNameValuePair("caller", ""));
		urlParameters.add(new BasicNameValuePair("num", "12345"));
 
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
 
		HttpResponse response = client.execute(post);
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + 
                                    response.getStatusLine().getStatusCode());
 
		BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));
 
		String result = "";
		String line = "";
		while ((line = rd.readLine()) != null) {
			result += line;
		}
 
		System.out.println(result.toString());
 
	}
		
}
