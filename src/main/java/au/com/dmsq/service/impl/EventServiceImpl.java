package au.com.dmsq.service.impl;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import au.com.dmsq.dao.impl.EventDaoImpl;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.EventService;

@Service
@Transactional
public class EventServiceImpl implements EventService {

	
	@Autowired 
	EventDaoImpl eventDao;
	
	@Override
	public List<Event> findAll() {
		return eventDao.findAll();
	}

	@Override
	public boolean save(Event saveevent) {
		return eventDao.save(saveevent);
	}

	@Override
	public boolean remove(Event removeevent) {
		return eventDao.remove(removeevent);
	}


	@Override
	public boolean removeById(Event event, Integer id) {	
		return eventDao.removeById(id);
	}
	
	@Override
	public Event get(Integer id) {
		return eventDao.find(id);
	}

	@Override
	public Event findById(Integer id) {
		return eventDao.find(id);
	}
	
	@Override
	public Event findByIdWithRelations(Integer id) {
		
		return eventDao.findByIdWithRelations(id);
	}

}