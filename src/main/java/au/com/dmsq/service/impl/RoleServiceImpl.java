package au.com.dmsq.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import au.com.dmsq.dao.RoleDao;
import au.com.dmsq.model.Role;
import au.com.dmsq.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService{

	@Autowired 
	private RoleDao roleDao;
	
	@Override
	public boolean save(Role role) {
		return roleDao.save(role);
	}

	@Override
	public Role get(Integer id) {
		return roleDao.find(id);
	}

	@Override
	public boolean deleteById(Integer id) {
		Role role = get(id);
		remove(role);
		return true;
	}

	@Override
	public List<Role> findAll() {
		return roleDao.findAll();
	}
	
	@Override
	public void remove(Role role){
		roleDao.remove(role);
	}
	
}

