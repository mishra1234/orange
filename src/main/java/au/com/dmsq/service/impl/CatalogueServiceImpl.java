package au.com.dmsq.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import au.com.dmsq.dao.CatalogueDao;
import au.com.dmsq.model.Catalogue;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.service.CatalogueService;

public class CatalogueServiceImpl extends AnAbstractService implements CatalogueService{
	
	@Autowired 
	private CatalogueDao catalogueDao;
	@Override
	public boolean save(Catalogue catalogue) {
		return catalogueDao.save(catalogue);
	}
	
	@Override
	public List<Catalogue> findAllItems() {
		return catalogueDao.findAll();
	}

}
