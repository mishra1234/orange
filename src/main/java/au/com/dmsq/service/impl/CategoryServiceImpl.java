package au.com.dmsq.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import au.com.dmsq.dao.CategoryDao;
import au.com.dmsq.dao.SubcategoryDao;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Subcategory;
import au.com.dmsq.service.CategoryService;
import au.com.dmsq.web.CategoryController;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	
	private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);
	
	@Autowired CategoryDao categoryDao;
	@Autowired SubcategoryDao subCategoryDao;
	
	@Override
	public List<Category> findAll() {
		return categoryDao.findAll();
	}
	
	@Override
	public Category getCategoryWithCommunity(String id) {
		Category cat = (Category) categoryDao.getCategoryCommunities(id);
		logger.debug("Category found:" + cat.getName());
		logger.debug("Layers:" +cat.getLayers().size());
		for (Layer l: cat.getLayers()){
			logger.debug(" Layer:" + l.getName());
		}
		return cat;
	}

	@Override
	public boolean save(Category category) {
		return categoryDao.save(category);
	}

	@Override
	public List<Category> getNonDeleted() {
		
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);
		search.addSort("name", false);

		return categoryDao.search(search);
	}
	
	@Override
	public List<Category> getDeleted() {
		
		Search search = new Search(); 
		search.addFilterEqual("deleted", "1");

		return categoryDao.search(search);
	}

	@Override
	public Category findById(Integer id) {
		Search search = new Search();
		search.addFilterEqual("id",id);
		search.addFilterEqual("deleted", false);
		search.addSort("name", false);
		search.setDistinct(true);
		return categoryDao.searchUnique(search);
	}

	@Override
	public boolean removeById(Integer id) {
		return categoryDao.removeById(id);
	}

	@Override
	public int countSubcategories(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("parentCategory", id);

		SearchResult<Subcategory> searchResult = subCategoryDao.searchAndCount(search);	
		return (searchResult == null?0:searchResult.getTotalCount());
	}

	@Override
	public int countConnections(String id) {
		
		return categoryDao.getConnRows(id);
	}
	
	@Override
	public int countActivities(String id) {
		
		return categoryDao.getEventRows(id);
	}
	
	@Override
	public int countCommunities(String id) {
		
		return categoryDao.getLayerRows(id);
	}
	
	
	// Makes reference to Subcategory
	

}
