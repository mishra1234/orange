package au.com.dmsq.service.impl;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.codehaus.jackson.map.ObjectMapper;
//import org.hibernate.annotations.common.util.impl.Log_.logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import au.com.dmsq.dao.AccountDao;
import au.com.dmsq.dao.LayerDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Role;
import au.com.dmsq.model.UserDTO;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.IUserService;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;


@Service
@Transactional
public class AccountServiceImpl extends AnAbstractService implements AccountService, IUserService{
	
	@Autowired 
	private AccountDao accountDao;
	@Autowired 
	private LayerDao layerDao;
	
	
	@Override
	public boolean save(Account account) {
		return accountDao.save(account);
	}

	@Override
	public Account get(Integer id) {
		return accountDao.find(id);
	}
	
	@Override
	public Account getbyIdWithAccConfirmations(Integer id) {
		return accountDao.findByIdWithAccConfirmations(id);
	} 
	
	// This function is causing memory error heaps and haltings on RC 01/10/15
	@Override
	public Account getbyIdWithLayers(Integer id) {
		return accountDao.findByIdWithLayers(id);
	}
	
	// Function that gets only the AccountLayerAccesses, it replaces the call of "getbyIdWithLayers"
	@Override
	public Account getALAsbyId(Integer id) {
		return accountDao.findALAsById(id);
	}
	
	// Function that gets only the Layers, it replaces the call of "getbyIdWithLayers"
	@Override
	public Account getLayersbyId(Integer id) {
		return accountDao.findLayersById(id);
	}
	
	@Override
	public Account getbyIdWithLayersFaster(Integer id) {
		return accountDao.findByIdWithLayersFaster(id);
	}
	
	@Override
	public Account getWithLayers(Integer id) {
		return accountDao.findWithLayers(id);
	}
	
	@Override
	public Account getbyIdWithLayersManaged(Integer id) {
		return accountDao.findByIdWithLayerManaged(id);
	}
	
	@Override
	public Account getbyIdWithRelations(Integer id) {
		return accountDao.findByIdWithRelations(id);
	}
	
	@Override
	public Account getbyIdWithRelationsFaster(Integer id) {
		return accountDao.findByIdWithRelationsFaster(id);
	}
	
	@Override
	public Account getConnectedLayersById(Integer id){
		return accountDao.findConnectedLayersById(id);
	}
	@Override
	public Account getBookingsById(Integer id){
		return accountDao.findBookingsById(id);
	}
	@Override
	public Account getAuthsById(Integer id){
		return accountDao.findAuthById(id);
	}
	@Override
	public Account getAccountLayerAccessesById(Integer id){
		return accountDao.findAccountLayerAccessesById(id);
	}
	@Override
	public Account getActivitiesById(Integer id){
		return accountDao.findByIdActivities(id);
	}
	@Override
	public boolean deleteById(Integer id) {
		Account account = get(id);
		remove(account);
		return true;
	}
	
	@Override
	public void remove(Account account){
		accountDao.remove(account);
	}

	@Override
	public List<Account> findAll() {
		return accountDao.findAll();
	}
	
	@Override
	public List<Account> getNonDeleted() {
		
		Search search = new Search(); 
		search.addFilterEqual("enabled", true);
		search.addSort("id", true);
		return accountDao.search(search);
	}
	
	@Override
	public List<Account> getDeleted() {
		
		Search search = new Search(); 
		search.addFilterEqual("enabled", false);

		return accountDao.search(search);
	}
	
	@Override
	public Account findById(Integer id) {
		return accountDao.find(id);
	}
	
	@Override
	public List <Account> findByEmail(String email) {
		Search search = new Search();
		search.addFilterEqual("email" , email);
		search.addFetch("layers");
		return accountDao.search(search);
	}
	
	@Override
	public Account findAcctByEmail(String email) {
		return accountDao.findAcctByEmail(email);
	}

	@Override
	public boolean registerNewUserAccount(UserDTO accountDto)throws Exception{
		if (emailExist(accountDto.getUsername())) {   
	        throw new Exception("There is an account with that email address: "+ accountDto.getUsername());
	    }
	    Account user = new Account();
	    Role role = new Role();
	    role.setId(3);
	    
	    user.setFirstName(accountDto.getFirstName());
	    user.setLastName(accountDto.getLastName());
	    user.setEmail(accountDto.getUsername());
	    user.setRole(role);
	    return accountDao.save(user);       
	}
	
	@Override
	public boolean emailExist(String email) {
		List<Account> accounts = findByEmail(email);
		for(Account str: accounts){
			if(accounts.isEmpty()){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public Integer count() {
		Search search = new Search(); 
		search.addFilterEqual("enabled", true);  
	return accountDao.count(search);
	}
	
	
	@Override
	public List<Account> pagedAccounts(Integer initial, Integer max){
		Search search = new Search(); 
			search.addFilterEqual("enabled", true);  
			search.setFirstResult(initial);
			search.setMaxResults(max);
		return accountDao.search(search);
	}

	@Override
	public Account getByResetCode(String token) {
		return accountDao.getByResetCode(token);
	}
	
	//Convert month number to month name
	public static String getMonth(int month) {
    return new DateFormatSymbols().getMonths()[month-1].substring(0, 3);
	}
	

	@Override
	public String getConnections(Account account) throws IllegalAccessException, ClassNotFoundException, Exception{
		Map<String, Integer> month=new HashMap<String, Integer>();
		String json = "";
		Map accountMap = layerDao.getAccountsGroupByMonth(account.getId());
		Map<String, Integer> treeMap = new TreeMap<String, Integer>(
				new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);
				}

			});
		treeMap.putAll(accountMap);
		System.out.println("-------------Size of the account Map: ----------------" + accountMap.size());
		Iterator it = treeMap.entrySet().iterator();
		//printMap(treeMap);
		
		String result = "[["+"\"year\""+", "+ "\"\""+ "],";
		
		while(it.hasNext()){
			Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				
				  result += "[\""+getMonth(Integer.parseInt(pair.getKey().substring(0, pair.getKey().indexOf("-")))) +"\""+ ",";
				  result += pair.getValue() + "],";
		}
		if (result.endsWith(",")) {
			result = result.substring(0, result.length() - 1);
		}
		
		      
			System.out.println(result);

		      result += "]";
			System.out.println(result);
			try {
				
				ObjectMapper mapper = new ObjectMapper();
				json = mapper.writeValueAsString( month);
				System.out.println("Json String: "+ json);

		}
		catch(Exception whatException){
			System.out.println("Exception occured while creating the json string: "+ whatException);
		}
			return result;
	}
	
	@Override
	public Integer totalConnections(Account account) throws IllegalAccessException, ClassNotFoundException, Exception{
		Map<String, Integer> month=new HashMap<String, Integer>();
		String json = "";
		Map accountMap = layerDao.getAccountsGroupByMonth(account.getId());
		Map<String, Integer> treeMap = new TreeMap<String, Integer>();
		treeMap.putAll(accountMap);
		System.out.println("-------------Size of the account Map: ----------------" + accountMap.size());
		Iterator it = treeMap.entrySet().iterator();
		//printMap(treeMap);
		
		Integer result = 0;
		
		while(it.hasNext()){
			Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
					  result += pair.getValue() ;
		}
		
				
			return result;
	}
	
	@Override
	public Integer thisMonthConnections(Account account) throws IllegalAccessException, ClassNotFoundException, Exception{
		Map<String, Integer> month=new HashMap<String, Integer>();
		Map accountMap = layerDao.getAccountsGroupByMonth(account.getId());
		Map<String, Integer> treeMap = new HashMap<String, Integer>();
		treeMap.putAll(accountMap);
		
		System.out.println("Size of the map is ================================"+treeMap.size());
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		int thisMonth = cal.get(Calendar.MONTH)+1;
		int countConnections = 0;
		System.out.println("Curren month on connection is: ========================"+ thisMonth);
		for (Map.Entry<String, Integer> entry : treeMap.entrySet())
		{
			
			int monthOfTheConnection = Integer.parseInt(entry.getKey().substring(0, entry.getKey().indexOf("-")));
			System.out.println("Connection Month is ======================================"+monthOfTheConnection);
			if(monthOfTheConnection==thisMonth){
				System.out.println("Increase the incrementer : " );
				countConnections+=entry.getValue();
			}
		}	
			return countConnections;
	}
	

	@Override
	public Integer particularMonthConnections(Account account, Integer whichMonth, Integer whichYear) throws IllegalAccessException, ClassNotFoundException, Exception{
		Map<String, Integer> month=new HashMap<String, Integer>();
		Map accountMap = layerDao.getAccountsGroupByMonth(account.getId());
		Map<String, Integer> treeMap = new HashMap<String, Integer>();
		treeMap.putAll(accountMap);
		int m = whichMonth;
		int y =whichYear;
		System.out.println("Size of the map in particular month is ================================"+treeMap.size());
		
		int countConnections = 0;
		System.out.println("Curren month on connection in particular month invoice is: ========================"+ whichMonth);
		for (Map.Entry<String, Integer> entry : treeMap.entrySet())
		{
			
			int monthOfTheConnection = Integer.parseInt(entry.getKey().substring(0, entry.getKey().indexOf("-")));
			int yearOfTheConnection = Integer.parseInt(entry.getKey().substring(entry.getKey().indexOf("-")+1, entry.getKey().length()));
			System.out.println("Year of the connection is  ==========================="+yearOfTheConnection);
			System.out.println("Connection of Month in particular month invoice is ======================================"+monthOfTheConnection);
			if(monthOfTheConnection==m && yearOfTheConnection==y){
				System.out.println("Increase the incrementer in particular month invoice : " );
				countConnections+=entry.getValue();
			}
		}	
			return countConnections;
	}
	
	@Override
	public String getConnectionOfParticularCommunity(Integer layerId) throws IllegalAccessException, ClassNotFoundException, Exception{
		Map<String, Integer> month=new HashMap<String, Integer>();
		String json = "";
		Map accountMap = layerDao.getAccountsGroupByMonthPerCom(layerId);
		Map<String, Integer> treeMap = new TreeMap<String, Integer>(
				new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);
				}

			});
		treeMap.putAll(accountMap);
		System.out.println("-------------Size of the account Map: ----------------" + accountMap.size());
		Iterator it = treeMap.entrySet().iterator();
		//printMap(treeMap);
		
		String result = "[["+"\"year\""+", "+ "\"\""+ "],";
		
		while(it.hasNext()){
			Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				
				  result += "[\""+getMonth(Integer.parseInt(pair.getKey().substring(0, pair.getKey().indexOf("-")))) +"\""+ ",";
				  result += pair.getValue() + "],";
		}
		if (result.endsWith(",")) {
			result = result.substring(0, result.length() - 1);
		}
		
		      
			System.out.println(result);
			try {
				
				ObjectMapper mapper = new ObjectMapper();
				json = mapper.writeValueAsString( month);
				System.out.println("Json String: "+ json);

		}
		catch(Exception whatException){
			System.out.println("Exception occured while creating the json string: "+ whatException);
		}
			return result;
	}
	
	public static void printMap(Map<Integer, String> map) {
		for (Map.Entry<Integer, String> entry : map.entrySet()) {
			System.out.println("Key : " + entry.getKey() 
                                      + " Value : " + entry.getValue());
		}
	}
	
	@Override
	public Integer countAccts(Integer layerId) 
	{
		Search search = new Search(); 
			//search.addFilterEqual("enabled", true);  
			search.addFilterSome("layers_1", Filter.equal("id", layerId));
		return accountDao.count(search);
	}
	
	@Override
	public List<Account> pagedAccountsByLayer(Integer initial, Integer max, Integer id, Integer layerId)
	{
		Search search = new Search(); 
			//search.addFilterEqual("enabled", true);  
			search.addFilterSome("layers_1", Filter.equal("id", layerId));
			//search.setFirstResult(initial);
			//search.setMaxResults(max);
			search.addSort("name", false);
		return accountDao.search(search);
	}
	
	@Override
	public Integer countAcctsGated(Integer layerId) 
	{	
		Search search = new Search();  
			search.addFetch("accountLayerAccesses");
			search.addFilterAnd(Filter.equal("accountLayerAccesses.layer.id", layerId),Filter.like("accountLayerAccesses.status", "granted"));
		return accountDao.count(search);
	}
	
	@Override
	public List<Account> pagedAccountsByLayerGated(Integer initial, Integer max, Integer id, Integer layerId)
	{
		Search search = new Search(); 
			search.addFetch("accountLayerAccesses");
			search.addFilterAnd(Filter.equal("accountLayerAccesses.layer.id", layerId),Filter.like("accountLayerAccesses.status", "granted"));
			//search.setFirstResult(initial);
			//search.setMaxResults(max);
			search.addSort("id", true);
		return accountDao.search(search);
	}

	@Override
	public Account getLayerManagersById(Integer id) {
		// TODO Auto-generated method stub
		return accountDao.findLayerManagerById(id);
	}
}
