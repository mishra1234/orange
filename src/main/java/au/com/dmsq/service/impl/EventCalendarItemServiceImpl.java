package au.com.dmsq.service.impl;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.validation.ValidationException;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.Recur;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.WeekDay;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Name;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.model.property.RRule;
import net.fortuna.ical4j.model.property.XProperty;
import net.fortuna.ical4j.util.UidGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import au.com.dmsq.dao.EventCalendarItemDao;
import au.com.dmsq.dao.impl.EventCalendarItemDaoImpl;
import au.com.dmsq.dao.impl.EventDaoImpl;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.EventCalendarItemId;
import au.com.dmsq.model.File;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.EventCalendarItemService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.MailContentService;

@Service
@Transactional
public class EventCalendarItemServiceImpl extends AnAbstractService implements EventCalendarItemService{
	
	@Autowired 
	MailContentService mcs;
	EventService eventService;
	ActivityService activityService;
	EventDaoImpl eventDao;
	EventCalendarItemDao eventCalendarItemDao;

	@Override
	public List<EventCalendarItem> findAll() {
		return eventCalendarItemDao.findAll();
	}

	@Override
	public boolean save(EventCalendarItem save) {
		return eventCalendarItemDao.save(save);
	}

	@Override
	public EventCalendarItem get(Integer id) {
		return eventCalendarItemDao.find(id);
	}
	
	
	@Override
	public EventCalendarItem removeByHrefAndID(Activity activity, String eciHref, Integer activityId) {
		return eventCalendarItemDao.findByActIDandHref(activity, eciHref, activityId);
	}
	
	@Override
	public Object[] findLast() {
		List<EventCalendarItem> ecis = eventCalendarItemDao.findAll();
		Object[] xs = ecis.toArray();
		for(int i=0; i<ecis.size(); i++){
			String x = xs[i].toString();
			System.out.println("ECI Impl - ecis: " + xs[i].toString());
		}
		return xs;
	}
	
	
	@Override
	public List<EventCalendarItem> getECIByActId(Event event)
	{
		System.out.println("Event received: " + event.getActivityId());
		
		List<EventCalendarItem> ecis = eventCalendarItemDao.findAll();  //Here Im asking to findall the event calendar items
																		//it is a simple findall but returns NULL POINTER
		return ecis;													
	}
	
	
	@Override
	public EventCalendarItem getLastECIbyActid(Integer eventId)
	{
		System.out.println("Event received: " + eventId);
		
		List<EventCalendarItem> ecis = new ArrayList<>();
		ecis.addAll(eventService.get(eventId).getEventCalendarItems());
		
//		List<EventCalendarItem> ecisbyEv = new ArrayList<>();
//		for(EventCalendarItem eci:ecis){
//			if(eci.getEvent().getActivityId()==eventId){
//				ecisbyEv.add(eci);
//			}
//			
//		}
//		Collections.sort(ecisbyEv, new Comparator<EventCalendarItem>(){
//			@Override
//	        public int compare(final EventCalendarItem object1, final EventCalendarItem object2) {
//	            return new Integer((int) object2.getDtend()).compareTo(new Integer((int) object1.getDtend()));
//	        }
//	       } );
		return ecis.get(0);													
	}
	

	@Override
	public List<EventCalendarItem> createEventDates(Integer id, List<String> sDate, List<String> sTime, List<String> eDate, 
			List<String> eTime, String[] days, List<Integer> duration, String periodicity, Event oldEvent){
			System.out.println("<<<< ---- Start Function: CreateEventDates ---- >>>>");
		try{	
			List<EventCalendarItem> ecis = new ArrayList<EventCalendarItem>();
			// To verify the information received
			//System.out.println("Event id as received: "+ id);
			//System.out.println("Event sDateTime as received: "+sDateTime);
			//System.out.println("Event eDateTime as received: "+eDateTime);
			for(int x=0;x<days.length;x++)
			{
				System.out.println("Event days: "+ days[x]);
			}
			
			for(String x:sDate){
				System.out.println("StartDate List : "+x);
			}
			
			for(int x=0;x<sDate.size();x++){
				UUID uid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d"); 
				UUID href = uid.randomUUID();
				String realHref = href+".ics";
				
				SimpleDateFormat sdf0 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); 
				Date formattedDate0s = sdf0.parse(sDate.get(x)+" "+sTime.get(x)+":00");
				Date formattedDate0e = sdf0.parse(eDate.get(x)+" "+eTime.get(x)+":00"); 
				
				String fDateEnd = sdf0.format(formattedDate0e);
				long epochDateEnd =  (((sdf0.parse(fDateEnd).getTime())+30600) / 1000);
					
		        SimpleDateFormat sdfa = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss"); 
			    String formattedDatea = sdfa.format(formattedDate0s); 
			    String formattedDatee = sdfa.format(formattedDate0e); 
				
			    System.out.println("Formated sDateTime: "+formattedDate0s);
			    System.out.println("Formated eDateTime: "+formattedDate0e);
			    System.out.println("Formated sDateTime: "+formattedDatea);
			    System.out.println("Formated eDateTime: "+formattedDatee);
				
				String[] sdt = formattedDatea.split("-");
					int[] d1 = new int[7];
					for(int i=0;i<sdt.length;i++)
					{
						d1[i]=Integer.parseInt(sdt[i]); 
						//System.out.println("Event eDateTime d1[i]: "+d1[i]);
					}
					
				String[] edt = formattedDatee.split("-");
					int[] d2 = new int[7];
					for(int i=0;i<edt.length;i++)
					{
						d2[i]=Integer.parseInt(edt[i]);
					}
				
				// Getting current date, month, year and time from system date epoch
		         long epoch = (System.currentTimeMillis()+30600)/1000;	 
		         java.util.Date dateTimeStamp = new java.util.Date(epoch*1000L);
		         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		         String formattedDate3 = sdf.format(dateTimeStamp);
		           String[] parts3 = formattedDate3.split("-");
		         int[] partstimeStamp=new int[6];
		         for(int i=0;i<=5;i++){
		        	 partstimeStamp[i] = Integer.parseInt(parts3[i]);
		         }
		        //Timestamp
		         java.util.Calendar timeStamp = new GregorianCalendar();
		         timeStamp.set(java.util.Calendar.MONTH, partstimeStamp[1]-1);
		         timeStamp.set(java.util.Calendar.DAY_OF_MONTH, partstimeStamp[2]);
		         timeStamp.set(java.util.Calendar.YEAR, partstimeStamp[0]);
		         timeStamp.set(java.util.Calendar.HOUR_OF_DAY, partstimeStamp[3]);
		         timeStamp.set(java.util.Calendar.MINUTE, partstimeStamp[4]);
		         timeStamp.set(java.util.Calendar.SECOND, partstimeStamp[5]);
				
				//Start date
				java.util.Calendar startDate = new GregorianCalendar();
					System.out.println("Event startDate created: "+startDate.getTime().toString());
				startDate.set(java.util.Calendar.MONTH, d1[1]-1);		
				startDate.set(java.util.Calendar.DAY_OF_MONTH, d1[2]);	
				startDate.set(java.util.Calendar.YEAR, d1[0]);			
				startDate.set(java.util.Calendar.HOUR_OF_DAY, d1[3]);	//System.out.println("Event startDate hour: "+startDate.getTime().toString());
				startDate.set(java.util.Calendar.MINUTE, d1[4]);		//System.out.println("Event startDate min: "+startDate.getTime().toString());
				startDate.set(java.util.Calendar.SECOND, d1[5]);		//System.out.println("Event startDate sec: "+startDate.getTime().toString());
				
				//End Time			
				java.util.Calendar endDate = new GregorianCalendar();
				endDate.set(java.util.Calendar.MONTH, d2[1]-1);
				endDate.set(java.util.Calendar.DAY_OF_MONTH, d2[2]);
				endDate.set(java.util.Calendar.YEAR, d2[0]);
				endDate.set(java.util.Calendar.HOUR_OF_DAY, d2[3]);		//System.out.println("Event endDate hour: "+endDate.getTime().toString());
				endDate.set(java.util.Calendar.MINUTE, (d2[4]));			//System.out.println("Event endDate min: "+endDate.getTime().toString());
				endDate.set(java.util.Calendar.SECOND, d2[5]);			//System.out.println("Event endDate sec: "+endDate.getTime().toString());

				//UNTIL DATE			
				java.util.Calendar untilDate = new GregorianCalendar();
				untilDate.set(java.util.Calendar.MONTH, d2[1]-1);
				untilDate.set(java.util.Calendar.DAY_OF_MONTH, d2[2]);
				untilDate.set(java.util.Calendar.YEAR, d2[0]);
				untilDate.set(java.util.Calendar.HOUR_OF_DAY, d2[3]);		
				untilDate.set(java.util.Calendar.MINUTE, (d2[4]));	
				untilDate.set(java.util.Calendar.SECOND, d2[5]);			

				
				// --- Creating an event ---
				Event ev = oldEvent;
				String eTitle = ev.getActivity().getContentTitle();
				
				DateTime s = new DateTime(startDate.getTime());		System.out.println("Event s: "+s.toString());
				DateTime e = new DateTime(endDate.getTime());		System.out.println("Event e: "+e.toString());
				DateTime u = new DateTime(untilDate.getTime());		System.out.println("Event u: "+u.toString());
				
				//DTSTAMP
			    DateTime timeStamp1 = new DateTime(timeStamp.getTime());
				
				Recur recur=new Recur(Recur.WEEKLY, null);
				
				if(periodicity.equalsIgnoreCase("WEEKLY")) {		//When the event is weekly or one just occurrence
					recur=new Recur(Recur.WEEKLY, null);
					String sRule = "";
					for(int i=0; i<days.length;i++){
						if(!(days[i].isEmpty())){
							System.out.println("Event DAYS: " + days[i]);
							sRule= sRule+days[i].toString();
							recur.getDayList().add(new WeekDay(days[i].toString()));
							if(i+1!=days.length){
								sRule= sRule+",";
							}
						}
					}
				}
				
				if(periodicity.equalsIgnoreCase("MONTHLY")) recur=new Recur(Recur.MONTHLY, null);
				
				recur.setUntil(u);  				// Setting UNTIL
				recur.setInterval(1);				// Setting INTERVAL=1 
				recur.getHourList().add(d1[3]); 	// Adding to RRULE the value for BYHOUR from DTSTART HOUR_OF_DAY
				recur.getMinuteList().add(d1[4]);	// Adding to RRULE the value for BYMINUTE from DTSTART MINUTE
				
				RRule rule = new RRule(recur);				
				
				// NOTE // "DURATION:+PT"+duration+"M"+"\n"+ (Duration is ignored by ANDROID)
				String cal_data = StringUtils.chomp(calDataGenerator(timeStamp1, periodicity, rule, s, e, eTitle)); //Revoving one newline from end of a String if it's there, otherwise leave it alone.
				
				//generating etag
				String etag = md5Java(cal_data.toString());
				
				//generating event calendar item eci object
				EventCalendarItem eci = new EventCalendarItem();
				EventCalendarItemId eciID = new EventCalendarItemId();
					eciID.setActivityId(id);
					eciID.setCalHref(realHref);
					
					eci.setId(eciID);
					eci.setEvent(ev);
					eci.setCalEtag(etag);
					eci.setCalData(cal_data.toString()); 
					
					eci.setDtend(epochDateEnd);
					
					Set <EventCalendarItem> list = new HashSet<EventCalendarItem>();
					list.addAll(ev.getEventCalendarItems());
					list.add(eci);
					ev.setEventCalendarItems(list);
				
					System.out.println("eci id:"+eci.getId().getActivityId());
					System.out.println("eci href:"+eci.getId().getCalHref());
					System.out.println("eci etag:"+eci.getCalEtag());
					System.out.println("eci title:"+eci.getEvent().getActivity().getContentTitle());
					System.out.println("eci data:"+"/n"+eci.getCalData());
					System.out.println("ECI DEND>>>"+eci.getDtend());
				
				ecis.add(eci);
				}
				
			return ecis;
		}
		catch(Exception ee){
			System.out.println("iCal4j Exception : "+ee);
		}
		return null;
		}
	
	
		@Override
		public List<EventCalendarItem> createEventDatesBulk(Integer id, String sDate, String sTime, String eDate, String eTime, Event oldEvent){
			System.out.println("<<<< ---- Start Function: CreateEventDates ---- >>>>");
			String periodicity = "DAILY";
			Integer duration = activityService.calculateDuration(sTime,eTime);
				try{	
				List<EventCalendarItem> ecis = new ArrayList<EventCalendarItem>();
				
				UUID uid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d"); 
				UUID href = uid.randomUUID();
				String realHref = href+".ics";
				
				SimpleDateFormat sdf0 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
				Date formattedDate0s = sdf0.parse(sDate+" "+sTime);
				Date formattedDate0e = sdf0.parse(eDate+" "+eTime); 
				
				String fDateEnd = sdf0.format(formattedDate0e);
				long epochDateEnd =  (((sdf0.parse(fDateEnd).getTime())+34200) / 1000);
					
		        SimpleDateFormat sdfa = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss"); 
			    String formattedDatea = sdfa.format(formattedDate0s); 
			    String formattedDatee = sdfa.format(formattedDate0e); 
				
			    System.out.println("Formated sDateTime: "+formattedDate0s);
			    System.out.println("Formated eDateTime: "+formattedDate0e);
			    System.out.println("Formated sDateTime: "+formattedDatea);
			    System.out.println("Formated eDateTime: "+formattedDatee);
				
				String[] sdt = formattedDatea.split("-");
					int[] d1 = new int[7];
					for(int i=0;i<sdt.length;i++)
					{
						d1[i]=Integer.parseInt(sdt[i]); 
					}
					
				String[] edt = formattedDatee.split("-");
					int[] d2 = new int[7];
					for(int i=0;i<edt.length;i++)
					{
						d2[i]=Integer.parseInt(edt[i]);
					}
				
				// Getting current date, month, year and time from system date epoch
		         long epoch = (System.currentTimeMillis()+34200)/1000;	 
		         java.util.Date dateTimeStamp = new java.util.Date(epoch*1000L);
		         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		         String formattedDate3 = sdf.format(dateTimeStamp);
		           String[] parts3 = formattedDate3.split("-");
		         int[] partstimeStamp=new int[6];
		         for(int i=0;i<=5;i++){
		        	 partstimeStamp[i] = Integer.parseInt(parts3[i]);
		         }
		        //Timestamp
		         java.util.Calendar timeStamp = new GregorianCalendar();
		         timeStamp.set(java.util.Calendar.MONTH, partstimeStamp[1]-1);
		         timeStamp.set(java.util.Calendar.DAY_OF_MONTH, partstimeStamp[2]);
		         timeStamp.set(java.util.Calendar.YEAR, partstimeStamp[0]);
		         timeStamp.set(java.util.Calendar.HOUR_OF_DAY, partstimeStamp[3]);
		         timeStamp.set(java.util.Calendar.MINUTE, partstimeStamp[4]);
		         timeStamp.set(java.util.Calendar.SECOND, partstimeStamp[5]);
				
				//Start date
				java.util.Calendar startDate = new GregorianCalendar();
					System.out.println("Event startDate created: "+startDate.getTime().toString());
				startDate.set(java.util.Calendar.MONTH, d1[1]-1);		
				startDate.set(java.util.Calendar.DAY_OF_MONTH, d1[2]);	
				startDate.set(java.util.Calendar.YEAR, d1[0]);			
				startDate.set(java.util.Calendar.HOUR_OF_DAY, d1[3]);	//System.out.println("Event startDate hour: "+startDate.getTime().toString());
				startDate.set(java.util.Calendar.MINUTE, d1[4]);		//System.out.println("Event startDate min: "+startDate.getTime().toString());
				startDate.set(java.util.Calendar.SECOND, d1[5]);		//System.out.println("Event startDate sec: "+startDate.getTime().toString());
				
				//End Time			
				java.util.Calendar endDate = new GregorianCalendar();
				endDate.set(java.util.Calendar.MONTH, d1[1]-1);
				endDate.set(java.util.Calendar.DAY_OF_MONTH, d1[2]);
				endDate.set(java.util.Calendar.YEAR, d1[0]);
				endDate.set(java.util.Calendar.HOUR_OF_DAY, d1[3]);		//System.out.println("Event endDate hour: "+endDate.getTime().toString());
				endDate.set(java.util.Calendar.MINUTE, (d1[4]+duration));			//System.out.println("Event endDate min: "+endDate.getTime().toString());
				endDate.set(java.util.Calendar.SECOND, d1[5]);			//System.out.println("Event endDate sec: "+endDate.getTime().toString());

				//UNTIL DATE			
				java.util.Calendar untilDate = new GregorianCalendar();
				untilDate.set(java.util.Calendar.MONTH, d2[1]-1);
				untilDate.set(java.util.Calendar.DAY_OF_MONTH, d2[2]);
				untilDate.set(java.util.Calendar.YEAR, d2[0]);
				untilDate.set(java.util.Calendar.HOUR_OF_DAY, d2[3]);		
				untilDate.set(java.util.Calendar.MINUTE, (d2[4]+duration));	
				untilDate.set(java.util.Calendar.SECOND, d2[5]);			

				
				// --- Creating an event ---
				Event ev = oldEvent;
				String eTitle = oldEvent.getActivity().getContentTitle();
				
				DateTime s = new DateTime(startDate.getTime());		System.out.println("Event s: "+s.toString());
				DateTime e = new DateTime(endDate.getTime());		System.out.println("Event e: "+e.toString());
				DateTime u = new DateTime(untilDate.getTime());		System.out.println("Event u: "+u.toString());
				
				//DTSTAMP
			    DateTime timeStamp1 = new DateTime(timeStamp.getTime());
				
				Recur recur=new Recur(Recur.DAILY, null);
				
				recur.setUntil(u);  				// Setting UNTIL
				recur.setInterval(1);				// Setting INTERVAL=1 
				recur.getHourList().add(d1[3]); 	// Adding to RRULE the value for BYHOUR from DTSTART HOUR_OF_DAY
				recur.getMinuteList().add(d1[4]);	// Adding to RRULE the value for BYMINUTE from DTSTART MINUTE
				
				RRule rule = new RRule(recur);				
				
				String cal_data = StringUtils.chomp(calDataGenerator(timeStamp1, periodicity, rule, s, e, eTitle)); //Revoving one newline from end of a String if it's there, otherwise leave it alone.
				
				//generating etag
				String etag = md5Java(cal_data.toString());
				
				//generating event calendar item eci object
				EventCalendarItem eci = new EventCalendarItem();
				EventCalendarItemId eciID = new EventCalendarItemId();
					eciID.setActivityId(id);
					eciID.setCalHref(realHref);
					
					eci.setId(eciID);
					eci.setEvent(ev);
					eci.setCalEtag(etag);
					eci.setCalData(cal_data.toString()); 
					
					eci.setDtend(epochDateEnd);
					
					Set <EventCalendarItem> list = new HashSet<EventCalendarItem>();
					list.addAll(ev.getEventCalendarItems());
					list.add(eci);
					ev.setEventCalendarItems(list);
				
				ecis.add(eci);	
					
				return ecis;
			}
			catch(Exception ee){
				System.out.println("iCal4j Exception : "+ee);
			}
			return null;
	}
	
	
	@Override
	public List<EventCalendarItem> readEventDates(
			List<String> sDate, List<String> sTime, List<String> eDate, List<String> eTime, 
			String[] days, List<Integer> duration, String periodicity, Event oldEvent
		){
		
		try{	
			List<EventCalendarItem> ecis = new ArrayList<EventCalendarItem>();
			for(EventCalendarItem ecionTransition:oldEvent.getEventCalendarItems()){
				ecis.add(ecionTransition);
			}
			List<EventCalendarItem> ecisTS = new ArrayList<EventCalendarItem>();
			
			int x=0;
			for(EventCalendarItem eci:ecis){
				
				SimpleDateFormat sdf0 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); 
				Date formattedDate0s = sdf0.parse(sDate.get(x)+" "+sTime.get(x)+":00");
				Date formattedDate0e = sdf0.parse(eDate.get(x)+" "+eTime.get(x)+":00"); 
				String fDateEnd = sdf0.format(formattedDate0e);
				long epochDateEnd =  (((sdf0.parse(fDateEnd).getTime())+34200) / 1000);
					
		        SimpleDateFormat sdfa = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss"); 
			    String formattedDatea = sdfa.format(formattedDate0s); 
			    String formattedDatee = sdfa.format(formattedDate0e); 
				
			    System.out.println("Formated sDateTime: "+formattedDate0s);
			    System.out.println("Formated eDateTime: "+formattedDate0e);
			    System.out.println("Formated sDateTime: "+formattedDatea);
			    System.out.println("Formated eDateTime: "+formattedDatee);
				
				String[] sdt = formattedDatea.split("-");
					int[] d1 = new int[7];
					for(int i=0;i<sdt.length;i++)
					{
						d1[i]=Integer.parseInt(sdt[i]); 
					}
				String[] edt = formattedDatee.split("-");
					int[] d2 = new int[7];
					for(int i=0;i<edt.length;i++)
					{
						d2[i]=Integer.parseInt(edt[i]);
					}
				
				// Getting current date, month, year and time from system date epoch
		         long epoch = System.currentTimeMillis()/1000;	 
		         java.util.Date dateTimeStamp = new java.util.Date(epoch*1000L);
		         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		         String formattedDate3 = sdf.format(dateTimeStamp);
		           String[] parts3 = formattedDate3.split("-");
		         int[] partstimeStamp=new int[6];
		         for(int i=0;i<=5;i++){
		        	 partstimeStamp[i] = Integer.parseInt(parts3[i]);
		         }
		        //Timestamp
		         java.util.Calendar timeStamp = new GregorianCalendar();
		         timeStamp.set(java.util.Calendar.MONTH, partstimeStamp[1]-1);
		         timeStamp.set(java.util.Calendar.DAY_OF_MONTH, partstimeStamp[2]);
		         timeStamp.set(java.util.Calendar.YEAR, partstimeStamp[0]);
		         timeStamp.set(java.util.Calendar.HOUR_OF_DAY, partstimeStamp[3]);
		         timeStamp.set(java.util.Calendar.MINUTE, partstimeStamp[4]);
		         timeStamp.set(java.util.Calendar.SECOND, partstimeStamp[5]);
				
				//Start date
				java.util.Calendar startDate = new GregorianCalendar();
				startDate.set(java.util.Calendar.MONTH, d1[1]-1);		
				startDate.set(java.util.Calendar.DAY_OF_MONTH, d1[2]);	
				startDate.set(java.util.Calendar.YEAR, d1[0]);			
				startDate.set(java.util.Calendar.HOUR_OF_DAY, d1[3]);	
				startDate.set(java.util.Calendar.MINUTE, d1[4]);		
				startDate.set(java.util.Calendar.SECOND, d1[5]);		
				
				//End Time			
				java.util.Calendar endDate = new GregorianCalendar();
				endDate.set(java.util.Calendar.MONTH, d1[1]-1);
				endDate.set(java.util.Calendar.DAY_OF_MONTH, d1[2]);
				endDate.set(java.util.Calendar.YEAR, d1[0]);
				endDate.set(java.util.Calendar.HOUR_OF_DAY, d1[3]);		
				endDate.set(java.util.Calendar.MINUTE, (d1[4]+duration.get(x)));
				endDate.set(java.util.Calendar.SECOND, d1[5]);			

				//UNTIL DATE			
				java.util.Calendar untilDate = new GregorianCalendar();
				untilDate.set(java.util.Calendar.MONTH, d2[1]-1);
				untilDate.set(java.util.Calendar.DAY_OF_MONTH, d2[2]);
				untilDate.set(java.util.Calendar.YEAR, d2[0]);
				untilDate.set(java.util.Calendar.HOUR_OF_DAY, d2[3]);		
				untilDate.set(java.util.Calendar.MINUTE, (d2[4]+duration.get(x)));	
				untilDate.set(java.util.Calendar.SECOND, d2[5]);			
				
				// --- Creating an event ---
				String eTitle = oldEvent.getActivity().getContentTitle();
				
				DateTime s = new DateTime(startDate.getTime());		System.out.println("Event s: "+s.toString());
				DateTime e = new DateTime(endDate.getTime());		System.out.println("Event e: "+e.toString());
				DateTime u = new DateTime(untilDate.getTime());		System.out.println("Event u: "+u.toString());
				
				//DTSTAMP
			    DateTime timeStamp1 = new DateTime(timeStamp.getTime());
				
				Recur recur=new Recur(Recur.WEEKLY, null);
				
				if(periodicity.equalsIgnoreCase("WEEKLY")) {		//When the event is weekly or one just occurrence
					recur=new Recur(Recur.WEEKLY, null);
					String sRule = "";
					for(int i=0; i<days.length;i++){
						if(!(days[i].isEmpty())){
							System.out.println("Event DAYS: " + days[i]);
							sRule= sRule+days[i].toString();
							recur.getDayList().add(new WeekDay(days[i].toString()));
							if(i+1!=days.length){
								sRule= sRule+",";
							}
						}
					}
				}
				
				if(periodicity.equalsIgnoreCase("MONTHLY")) recur=new Recur(Recur.MONTHLY, null);
				
				recur.setUntil(u);  				// Setting UNTIL
				recur.setInterval(1);				// Setting INTERVAL=1 
				recur.getHourList().add(d1[3]); 	// Adding to RRULE the value for BYHOUR from DTSTART HOUR_OF_DAY
				recur.getMinuteList().add(d1[4]);	// Adding to RRULE the value for BYMINUTE from DTSTART MINUTE
				
				RRule rule = new RRule(recur);				
					
				String cal_data = StringUtils.chomp(calDataGenerator(timeStamp1, periodicity, rule, s, e, eTitle)); 
				
				//generating etag
				String etag = md5Java(cal_data.toString());
				
				//generating event calendar item eci object
				eci.setEvent(oldEvent);
				eci.setCalEtag(etag);
				eci.setCalData(cal_data.toString()); 
				
				// Epoch of the date the event ENDS
				eci.setDtend( epochDateEnd);
			
				System.out.println("eci id:"+eci.getId().getActivityId());
				System.out.println("eci href:"+eci.getId().getCalHref());
				System.out.println("eci etag:"+eci.getCalEtag());
				System.out.println("eci title:"+eci.getEvent().getActivity().getContentTitle());
				System.out.println("eci data:"+"/n"+eci.getCalData());
				System.out.println("ECI DEND>>>"+eci.getDtend());
				
				x++;
				ecisTS.add(eci);
			}
			return ecisTS;
		}
		catch(Exception ee){
			System.out.println("iCal4j Exception : "+ee);
		}
		return null;
	}
	
	
	@Override
	public Set<EventCalendarItem> readEventDatesEdited(Event oldEvent, List<EventCalendarItem> ecisNew )
	{
		try{	
			Set<EventCalendarItem> oldEcis = new HashSet<>();
			oldEcis.addAll(oldEvent.getEventCalendarItems());
			
			
			oldEcis.removeAll(ecisNew);
			
			for(EventCalendarItem eci:oldEcis){
				System.out.println("ECIs to Delete: "+eci.getId().getActivityId() +"-"+eci.getId().getCalHref());
			}
			
			return oldEcis;
		}
		catch(Exception ee){
			System.out.println("Exception : "+ee);
		}
		return null;
	}

	
	public String md5Java(String message){
			String digest = null; 
			try{ 
				MessageDigest md = MessageDigest.getInstance("MD5"); 
				byte[] hash = md.digest(message.getBytes("UTF-8")); //converting byte array to Hexadecimal String 
				StringBuilder sb = new StringBuilder(2*hash.length); 
				for(byte b : hash){ 
					sb.append(String.format("%02x", b&0xff)); 
				} 
				digest = sb.toString(); 
			} 
			catch (UnsupportedEncodingException ex){
				System.out.println("Unsupported Exception:" + ex);
			}
			catch (NoSuchAlgorithmException ex){ 
				System.out.println("No Such Exception: "+ex);
			}
			return digest; 
		}
	
	
	public	Calendar getCalendarEvent(String ical) throws Exception {
		Calendar calendar=null;
		try {
			System.out.println("<<<>>> Result of calling the function getCalendarEvent <<<>>>");
			System.out.println("ical received: "+ical);
		    CalendarBuilder builder=new CalendarBuilder();
		    calendar=builder.build(new StringReader(ical));
		    //calendar.validate(true);
		    System.out.println("<<<>>> Conversion of string into ical <<<>>>");
		    System.out.println("calendar: "+ calendar.toString());
		}
		catch (  ValidationException e) {
		    throw new Exception("Invalid calendar object: " + e.getMessage());
		}
		//if (calendar.getComponents().size() > 1)   
			//throw new Exception("Calendar object contains more than one VTIMEZONE component");
		return calendar;
	}
	
	
	public void toBaikal(String etag, String sDateTime, String eDateTime, String iCaldata, String href) throws Exception, IllegalAccessException, ClassNotFoundException{
		String connectionURLBaikal = getBAIKALHOST()+ "/baikal";
		//String connectionURLBaikal = "jdbc:mysql://dev.digitalmarketsquare.com:3306/baikal";
		//String connectionURLBaikal = "jdbc:mysql://localhost:3306/baikal";
		
        Connection connectionBaikal = null;  
        //connectionBaikal = DriverManager.getConnection(connectionURLBaikal, "carlos_dev", "carlos_dev");
        //connectionBaikal = DriverManager.getConnection(connectionURLBaikal, "cms", "t3qu1l@1");
        //System.out.println("Baikal Conn: "+mcs.readDBBkConn());
        //System.out.println("Baikal User: "+mcs.readDBBkUser());
        //System.out.println("Baikal Passw: "+mcs.readDBBkPass());
        //connectionBaikal = DriverManager.getConnection(connectionURLBaikal, "cms", "t3qu1l@1");
        connectionBaikal = DriverManager.getConnection(connectionURLBaikal, getBAIKALUSER(), getBAIKALPASSWORD());

        if(!connectionBaikal.isClosed()){
        	Statement stmtBaikal = connectionBaikal.createStatement();
				try{					
					SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						Date dateS = df.parse(sDateTime);
						Date dateE = df.parse(eDateTime);

			       	long starts = (dateS.getTime()-30600)/1000L;	
			       	long ends = (dateE.getTime()-30600)/1000L;
			       	
			        long epoch = System.currentTimeMillis()/1000;
			        
					String insertBaikal= "INSERT INTO `baikal`.`calendarobjects`(`calendardata`, `uri`, `calendarid`, `lastmodified`, `etag`, `size`, " +
							"`componenttype`, `firstoccurence`, `lastoccurence`) " +
							"VALUES('"+iCaldata+"', '"+href+"', 3, "+epoch+", '"+etag+"', 300, 'VEVENT', "+starts+", "+ends+")";
					System.out.println("INSERT toBaikal: "+ insertBaikal);
					stmtBaikal.executeUpdate(insertBaikal);
					
				}
				catch (SQLException e ) {
					System.out.println("SQL Exception : "+e);
				}
				finally{
		            if(stmtBaikal != null)
		            	stmtBaikal.close();
		        }	
			}
	}
	
	
	
	@Override
	public void updateToBaikal(String etag, String sDateTime, String eDateTime, String iCaldata, String href) 
			throws Exception, IllegalAccessException, ClassNotFoundException{
		String connectionURLBaikal = getBAIKALHOST() + "/baikal";
        Connection connectionBaikal = null;  
        connectionBaikal = DriverManager.getConnection(connectionURLBaikal, getBAIKALUSER(), getBAIKALPASSWORD());
        if(!connectionBaikal.isClosed()){
        	Statement stmtBaikal = connectionBaikal.createStatement();
				try{					
					SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						Date dateS = df.parse(sDateTime);
						Date dateE = df.parse(eDateTime);

			       	long starts = (dateS.getTime()-30600)/1000L;	
			       	long ends = (dateE.getTime()-30600)/1000L;
			       	
			        long epoch = System.currentTimeMillis()/1000;
			        //
			        //					  
					String insertBaikal= "UPDATE `baikal`.`calendarobjects` " +
							"SET " +
								"`calendardata`='"+iCaldata+"', " +
								"`uri`='"+href+"', "+
								"`calendarid`='3', "+
								"`lastmodified`='"+epoch+"', "+ 
								"`etag`='" +etag+"', "+ 
								"`size`='300', " +
								"`componenttype`='VEVENT', " +
								"`firstoccurence`='"+starts+"', " +
								"`lastoccurence`='"+ends+"' " +
								" WHERE `uri`='"+href+"';";
					System.out.println("UPDATE toBaikal: "+ insertBaikal);
					stmtBaikal.executeUpdate(insertBaikal);
					
				}
				catch (SQLException e ) {
					System.out.println("SQL Exception : "+e);
				}
				finally{
		            if(stmtBaikal != null)
		            	stmtBaikal.close();
		        }	
			}
	}
	
	
	@Override
	public void deleteToBaikal(String uri,String etag)	throws Exception, IllegalAccessException, ClassNotFoundException{
		String connectionURLBaikal = getBAIKALHOST() + "/baikal";
        Connection connectionBaikal = null;  
        connectionBaikal = DriverManager.getConnection(connectionURLBaikal, getBAIKALUSER(), getBAIKALPASSWORD());
        if(!connectionBaikal.isClosed()){
        	Statement stmtBaikal = connectionBaikal.createStatement();
				try{								  
					String deleteBaikal= "DELETE FROM `baikal`.`calendarobjects` WHERE `uri`='"+uri+"'and `etag`='"+etag+"';";
					System.out.println("DELETE toBaikal: "+ deleteBaikal);
					stmtBaikal.executeUpdate(deleteBaikal);
					
				}
				catch (SQLException e ) {
					System.out.println("SQL Exception : "+e);
				}
				finally{
		            if(stmtBaikal != null)
		            	stmtBaikal.close();
		        }	
			}
	}

	
	@Override
	public EventCalendarItem editECI(EventCalendarItem eci){
		try{	
			EventCalendarItem eciGotten = eci;
			return eciGotten;
		}
		catch(Exception ee){
			System.out.println("iCal4j Exception : "+ee);
		}
		return null;
		}
	
	@Override
	public String calDataGenerator(DateTime timeStamp, String periodicity, RRule rrule, DateTime start, DateTime end, String evTitle){
		String generate_data= "";
		if(!periodicity.equalsIgnoreCase("DAILY")){
			String test="BEGIN:VCALENDAR";
			test=test.replaceAll("\r", "").trim();
			generate_data=
						test+"\n"+
						"VERSION:2.0"+"\n"+
						"PRODID:-//www.marudot.com//iCal EventMaker"+"\n"+
						"X-WR-CALNAME:CCME"+"\n"+
						"CALSCALE:GREGORIAN"+"\n"+
						"BEGIN:VTIMEZONE"+"\n"+ 
						"TZID:Australia/Adelaide"+"\n"+ 
						"TZURL:http://tzurl.org/zoneinfo-outlook/Australia/Adelaide"+"\n"+
						"X-LIC-LOCATION:Australia/Adelaide"+"\n"+ 
						"BEGIN:STANDARD"+"\n"+
						"TZOFFSETFROM:+1030"+"\n"+
						"TZOFFSETTO:+0930"+"\n"+
						"TZNAME:ACST"+"\n"+
						"DTSTART:19700405T030000"+"\n"+
						"END:STANDARD"+"\n"+
						"BEGIN:DAYLIGHT"+"\n"+
						"TZOFFSETFROM:+0930"+"\n"+
						"TZOFFSETTO:+1030"+"\n"+
						"TZNAME:ACDT"+"\n"+
						"DTSTART:19701004T020000"+"\n"+
						"END:DAYLIGHT"+"\n"+
						"END:VTIMEZONE"+"\n"+
						"BEGIN:VEVENT"+"\n"+
						"DTSTAMP:"+timeStamp+"Z"+"\n"+
						"UID:"+timeStamp+"Z-646824233@marudot.com"+"\n"+
						rrule+
						"DTSTART;TZID=Australia/Adelaide:"+start+"\n"+
						"DTEND;TZID=Australia/Adelaide:"+end+"\n"+
						"SUMMARY:"+evTitle+"\n"+
						"END:VEVENT"+"\n"+
						"END:VCALENDAR"+"\n";
		}
		else{
			String test="BEGIN:VCALENDAR";
			test=test.replaceAll("\r", "").trim();
			generate_data=
						test+"\n"+
						"VERSION:2.0"+"\n"+
						"PRODID:-//www.marudot.com//iCal EventMaker"+"\n"+
						"X-WR-CALNAME:CCME"+"\n"+
						"CALSCALE:GREGORIAN"+"\n"+
						"BEGIN:VTIMEZONE"+"\n"+ 
						"TZID:Australia/Adelaide"+"\n"+ 
						"TZURL:http://tzurl.org/zoneinfo-outlook/Australia/Adelaide"+"\n"+
						"X-LIC-LOCATION:Australia/Adelaide"+"\n"+ 
						"BEGIN:STANDARD"+"\n"+
						"TZOFFSETFROM:+1030"+"\n"+
						"TZOFFSETTO:+0930"+"\n"+
						"TZNAME:ACST"+"\n"+
						"DTSTART:19700405T030000"+"\n"+
						"END:STANDARD"+"\n"+
						"BEGIN:DAYLIGHT"+"\n"+
						"TZOFFSETFROM:+0930"+"\n"+
						"TZOFFSETTO:+1030"+"\n"+
						"TZNAME:ACDT"+"\n"+
						"DTSTART:19701004T020000"+"\n"+
						"END:DAYLIGHT"+"\n"+
						"END:VTIMEZONE"+"\n"+
						"BEGIN:VEVENT"+"\n"+
						"DTSTAMP:"+timeStamp+"Z"+"\n"+
						"UID:"+timeStamp+"Z-646824233@marudot.com"+"\n"+
						"DTSTART;TZID=Australia/Adelaide:"+start+"\n"+
						"DTEND;TZID=Australia/Adelaide:"+end+"\n"+
						"SUMMARY:"+evTitle+"\n"+
						"END:VEVENT"+"\n"+
						"END:VCALENDAR"+"\n";
		}
		return generate_data;
	}
	
	@Override
	public void toECI(List<EventCalendarItem> ecis, Event newEvent) throws Exception, IllegalAccessException, ClassNotFoundException{
		String connectionURLto = getURLDB();
		Connection connectionTo = null; 
        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
        connectionTo = DriverManager.getConnection(connectionURLto, getUSER(), getPASSWORD());
        
        if(!connectionTo.isClosed()){
        	Statement stmtTo = connectionTo.createStatement(); 	
			try{					
				for(int z=0; z<ecis.size();z++){
					long epoch = (System.currentTimeMillis()/1000)+z;
					String updateString = "UPDATE `"+getDB()+"`.`event_calendar_item` "+"SET "+"`cal_data`='" +
							ecis.get(z).getCalData()+"',`dtend`='"+ epoch +"' WHERE "+"`activity_id`="+ecis.get(z).getId().getActivityId()+" and "+
							"`cal_href`='"+newEvent.getHref()+"';";
					System.out.println("DEBUG>>>"+updateString);
					stmtTo.executeUpdate(updateString);
				}
				
			}
			catch (SQLException e ) {
				System.out.println("SQL Exception : "+e);
			}
			finally{
	            if(stmtTo != null)
	            	stmtTo.close();
			}
        }
	}
	
	@Override
	public void updateECI(List<EventCalendarItem> ecis, Event newEvent) throws Exception, IllegalAccessException, ClassNotFoundException{
		String connectionURLup = getURLDB();
		Connection connectionUp = null; 
        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
        connectionUp = DriverManager.getConnection(connectionURLup, getUSER(), getPASSWORD());
        
        if(!connectionUp.isClosed()){
        	Statement stmtUp = connectionUp.createStatement(); 	
			try{					
				for(int z=0; z<ecis.size();z++){
					long epoch = (System.currentTimeMillis()/1000)+z;
					String updateString = "UPDATE `"+getDB()+"`.`event_calendar_item` "+"SET "+"`cal_data`='" +
							ecis.get(z).getCalData()+"',`dtend`='"+ epoch +"' WHERE "+"`activity_id`="+ecis.get(z).getId().getActivityId()+" and "+
							"`cal_href`='"+newEvent.getHref()+"';";
					System.out.println("DEBUG>>>"+updateString);
					stmtUp.executeUpdate(updateString);
				}
				
			}
			catch (SQLException e ) {
				System.out.println("SQL Exception : "+e);
			}
			finally{
	            if(stmtUp != null)
	            	stmtUp.close();
			}
        }
	}


}
