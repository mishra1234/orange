package au.com.dmsq.service.impl;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import au.com.dmsq.dao.LayerDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.IconDTO;
import au.com.dmsq.model.Layer;
import au.com.dmsq.service.AccountLayerAccessService;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.web.CommunityController;
import com.googlecode.genericdao.search.Search;

@Service
@Transactional
public class LayerServiceImpl implements LayerService {

	private static final Logger logger = LoggerFactory.getLogger(CommunityController.class);
	
	@Autowired private LayerDao layerDao;
	@Autowired MailContentService mcs;
	@Autowired AccountService accountService;
	@Autowired AccountLayerAccessService alaService;
	
	@Override
	public List<Layer> findAll() {
		return layerDao.findAll();
	}

	@Override
	public boolean save(Layer savelayer) 
	{
		return layerDao.save(savelayer);
	}

	@Override
	public void remove(Layer removelayer) {
		layerDao.remove(removelayer);
	}


	@Override
	public boolean deleteById(Integer id) {	
		Layer layer2 = get(id);
		remove(layer2);
		return true;
	}
	
	@Override
	public Layer get(Integer id) {
		return layerDao.find(id);
	}
	
	@Override
	public Layer findwithBanners(Integer id) {
		return layerDao.findByIdBanners(id);
	}
	
	
	@Override
	public Set<Banner> getBanners(Set<Layer> layers)
	{
		Set<Layer> getBannerOfLayer  = new HashSet<>();
//		for(Layer layer:layers){
//			activities.addAll(layerDao.findByIdWithActsEv(layer.getId()).getActivities());
//		}
		Set<Banner> banners = new HashSet<>();
		for(Layer layer:layers){
			banners.addAll(layer.getBanners());
		}
		return banners;
	}
	
	public List<Layer> getNonDeleted()
	{
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);
		search.addSort("id", true);
		return layerDao.search(search);
	}
	
	public List<Layer> getByOwnerId(Integer id)
	{
		return layerDao.findEventsbyLayerId(id);
	}
	
	@Override
	public Layer findById(Integer id) {
			return layerDao.find(id);
	}

	@Override
	public List<Layer> getByCategory(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);
		search.addSort("id", true);
		return layerDao.search(search);
	}

//	@Override
//	public List<IconDTO> listIcons() throws Exception {
//		//String connectionURL = "jdbc:mysql://dev.digitalmarketsquare.com:3306/ccme_winter_2";
//		//String connectionURL = "jdbc:mysql://localhost:3306/ccme_winter_2_dev";
//		String connectionURL = mcs.readDBConn().toString();
//		//String connectionURL = "jdbc:mysql://winterfell.cbi0v9fte9kc.ap-southeast-2.rds.amazonaws.com:3306/ccme_winter_2_dev";
//        java.sql.Connection connection = null; 
//        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
//        connection = DriverManager.getConnection(connectionURL, mcs.readDBUser(), mcs.readDBPass());
//        //connection = DriverManager.getConnection(connectionURL, "carlos_dev", "carlos_dev");
//        if(!connection.isClosed()){
//        	java.sql.Statement stmt = connection.createStatement();       	
//        	try{
//        		String selectUnion = "SELECT icon_url iconUrl FROM ccme_winter_2.activity UNION "+
//        				"SELECT image_url iconUrl FROM ccme_winter_2.account UNION "+
//        				"SELECT icon_url iconUrl FROM ccme_winter_2.`file` UNION "+
//        				"SELECT icon_url iconUrl FROM ccme_winter_2.layer;";
//        		ResultSet icons= stmt.executeQuery(selectUnion);
//        		List<IconDTO> listIcons = new ArrayList<IconDTO>();
//        		while(icons.next()){
//        			IconDTO icon = new IconDTO();
//        			icon.setIconUrl(icons.getString(1));
//        			listIcons.add(icon);
//        		}
////        		for(int i=0; i<listIcons.size(); i++){
////        			System.out.println("Controller - NEW COMM/ List<IconDTO> : " +  listIcons.get(i).getIconUrl());
////        		}        		
//        		
//        		return (List<IconDTO>) listIcons;
//			}
//			catch (SQLException e ) {
//				System.out.println("SQL Exception : "+e);
//			}
//			finally{
//	            if(stmt != null)
//	            stmt.close();
//	        }
//        }
//		return null;
//	}
	
	
	@Override
	public int count() {
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);  
	return layerDao.count(search);
	}
	
	@Override
	public int countByOwner(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);
		search.addFilterEqual("account.id", id);
	return layerDao.count(search);
	}
	
	@Override
	public List<Layer> pagedCommunities(Integer initial, Integer max, Integer id){
 
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("account.id", id);
			search.addSort("id", true);
			search.addFetches("layersForChildLayerId", "layersForParentLayerId", "activities");	
			search.setFirstResult(initial);
			search.setMaxResults(max);
		return layerDao.search(search);
	}
	
	
	@Override
	public int countActivities(String id){
 
		return layerDao.getActivityRows(id);
	}

	
	@Override
	public Layer findByIdWithActivities(Integer id) 
	{
		return layerDao.findByIdWithActsEv(id);
	}
	
	@Override
	public Layer findByIdWithActivityType(String type, Integer id) 
	{
		return layerDao.findActivitiesByIdByType(type, id);
	}
	
	
	@Override
	public Layer findByIdWithActivitiesAll(Integer id) 
	{
		return layerDao.findByIdActivitiesAll(id);
	}
	
	@Override
	public Layer findByIdWithAccounts(Integer id) 
	{
		return layerDao.findByIdWithAccounts(id);
	}
	
	
	public Layer findByIdWithRelations(Integer id) {
		return layerDao.findByIdWithRelations(id);
	}
	
	@Override
	public Layer findByIdWithRequiredRelations(Integer id) {
		return layerDao.findByIdWithRequiredRelations(id);
	}
	
	@Override
	public Layer findByIdWithParentChildCategories(Integer id) {
		return layerDao.findByIdwithParentChildCategories(id);
	}
	
	
	@Override
	public Boolean testIfManager(Integer accountId)
	{
		Boolean result = false;
		Integer accId = accountId;					
		List<Layer> layers = getNonDeleted();		
		for(Layer layer:layers){
			Layer layerM = layerDao.findWithManagers(layer.getId());	
			
			if(layerM.getAccounts().size()>0){
				for(Account acct:layerM.getAccounts()){
					if(acct.getId().equals(accId))
						result = true;
				}
			}
		}
		//System.out.println("test if manager - final result: " +  result);
		return result;
	}
	
	
	@Override
	public List<Layer> layersIfManager(Integer accountId)
	{	
		return layerDao.getManagedLayers(accountId);
		//Change Query 
		/*
		List<Layer> layers = layerDao.findAll();
		List<Layer> managedLayers = new ArrayList<Layer>();
		
		for(Layer ids:layers){
			Layer layerM = layerDao.findWithManagers(ids.getId());
			if(layerM.getAccounts().size()>0){
				Set<Account> accounts = layerM.getAccounts();
				for(Account acct:accounts){
					if(acct.getId().equals(accountId)){
						managedLayers.add(layerM);
					}
				}
			}
		}
		
		//System.out.println("layersManaged : " +  managedLayers.size());
		return managedLayers;*/
	}

	@Override
	public List<Layer> getregionallayers() {
		// TODO Auto-generated method stub
		return layerDao.getRegionalCouncils();
	}
	
	@Override
	public List<Layer> getStatusLayers() {
		// TODO Auto-generated method stub
		return layerDao.getStatusCouncils();
	}
	
	@Override
	public Layer getLayerWithManagers(Integer id)
	{
		return layerDao.findWithManagers(id);
	}
	
	
	
	public Layer findLayerWithActivitiesAndAccess(Integer id){
		
		return layerDao.findLayerWithActivitiesAndAccess(id);
	}
	
	
	@Override
	public Layer findLayerWithActivitiesAndType(Integer layerId, String type){
		
		return layerDao.findByIdWithActivitiesandAccountId(layerId,type);
	}

	
}