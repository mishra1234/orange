package au.com.dmsq.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import au.com.dmsq.model.Account;
import au.com.dmsq.service.MailContentService;

@Service
public class MainContentServiceImpl extends AnAbstractService implements MailContentService {

	final static String FILE  = "mails/confirmationEmail.html";
	final static String FILEC = "mails/confirmationNewEmail.html";
	final static String FILE2 = "mails/confirmationEmail2.html";
	final static String FILE3 = "mails/forgotPasswordEmail.html";
	
	final static String FILE4 = "mails/requestedAccessManagerEmail.html";
	final static String FILE5 = "mails/requestAccessMemberEmail.html";
	
	final static String FILE6 = "mails/accessGrantedEmail.html";
	
	final static String FILE7 = "mails/inviteEmail.html";
	
	final static String FILE8 = "mails/connectedConfirmation.html";
	
	final static String FILE9 = "mails/subscriptionEmail.html";

	final static String FILEPAYMENT="mails/paymentConfirmation.html";
	final static String DBCONN = getURLDB();
	final static String DBUSER = getUSER(); 
	final static String DBPASS = getPASSWORD();
	
	final static String DBBKCONN = getBAIKALHOST() +"/baikal";
	final static String DBBKUSER = "admin";
	final static String DBBKPASS = "conf/dbBkPass.txt";
	
	final static String SGUSER = "lpStlaEHvV";
	final static String SGPASS = "KNe3cezmbc";
	
	final static String WSCONN = "http://cc-me.com.au";
	
	final static String promoEvent = "mails/promoEventEmail.html";
	final static String promoInfo = "mails/promoInfoEmail.html";
	
	public final String SERVER = "http://cc-me.com.au";
    //public final String SERVER = "http://localhost:8080/orange";

	@Autowired
	ServletContext servletContext;

	@Override
	public String readConfirmationEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	
	
	@Override
	public String readNewEmailConfirmationEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILEC);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	
	
	@Override
	public String readConfirmationEmailLower() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE2);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	
	@Override
	public String readSubsEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE8);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	
	@Override
	public String readForgotPassword() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE3);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	@Override
	public String readAccessRequestEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE4);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	
	
	@Override
	public String readPaymentConfirmationEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILEPAYMENT);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	
	@Override
	public String readAccessRequestMemberEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE5);
			file = html.getFile();
			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	@Override
	public String readInviteEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE7);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	@Override
	public String readConnectedComConfirm() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE8);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	@Override
	public String readAccessGrantedEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(FILE6);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	@Override
	public String readPromoEventEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(promoEvent);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;

	}
	
	
	@Override
	public String readPromoInfoEmail() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(promoInfo);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;
	}
	
	@Override
	public String readDBConn() {
		return DBCONN;
	}
	
	@Override
	public String readDBUser() {
		return DBUSER;
	}
	
	@Override
	public String readDBPass() {
		return DBPASS;
	}
	
	@Override
	public String readDBBkConn() {
		return DBBKCONN;
	}
	
	@Override
	public String readDBBkUser() {
		return DBBKUSER;
	}
	
	@Override
	public String readDBBkPass() {

		String result = "";

		File file;
		try {
			final Resource html = new ClassPathResource(DBBKPASS);
			file = html.getFile();

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					result += scanner.nextLine() + "\n";
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}

		return result;
	}
	
	@Override
	public String readSGUser() {
		return SGUSER;
	}
	
	@Override
	public String readSGPass() {
		return SGPASS;
	}
	
	@Override
	public String readWSConn() {
		return WSCONN;
	}
	
	@Override
	public String insertUnsubscribe(Account acct, String body) {

		String unsubscribe= "<p>This message was sent to "+acct.getEmail()+".  If you don't want to continue receiving these emails,"+
				" please click on the following link:  "+SERVER+"/unsubscribe/"+acct.getId()+"</p>"+
                "<p>2/85 Mount Barker Road, Stirling, 5152 South Australia</p>";
		body = body.replace("unsubscribeText", unsubscribe);
				
		return body;
	}
	
}