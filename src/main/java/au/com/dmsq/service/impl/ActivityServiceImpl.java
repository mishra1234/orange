package au.com.dmsq.service.impl;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Date;

import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

import au.com.dmsq.dao.ActivityDao;
import au.com.dmsq.dao.LayerDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.EventCalendarItem;
import au.com.dmsq.model.EventDTO;
import au.com.dmsq.model.File;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Message;
import au.com.dmsq.model.RecommendedActivities;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.web.MyEventsController;

@Service
@Transactional
public class ActivityServiceImpl implements ActivityService {
	
	
	private static final Logger logger = LoggerFactory.getLogger(ActivityServiceImpl.class);
	
	@Autowired ActivityDao activityDao;
	@Autowired LayerDao layerDao;
	@Autowired LayerService communityService;
	@Autowired BannerService bannerService;

	@Override
	public boolean save(Activity activity) {
		return activityDao.save(activity);
	}

	@Override
	public Activity get(Integer id) {
		return activityDao.find(id);
	}

	@Override
	public boolean deleteById(Integer id) {
		Activity activity = get(id);
		remove(activity);
		return true;
	}

	@Override
	public List<Activity> findAll() {
		return activityDao.findAll();
	}
	
	@Override
	public void remove(Activity activity){
		activityDao.remove(activity);
	}
	
	@Override
	public List<Activity> getNonDeleted()
	{
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);
		search.addSort("id", true);
		return activityDao.search(search);
	}
	
	@Override
	public List<Activity> getEvtsNonDeleted()
	{
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);
		search.addFilterEqual("activityType", "event");
		search.addSort("id", true);
		return activityDao.search(search);
	}
	
	@Override
	public List<Activity> getInfsNonDeleted()
	{
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);
		search.addFilterEqual("activityType", "info");
		search.addSort("id", true);
		return activityDao.search(search);
	}
	
	@Override
	public List<Activity> getByOwnerId(Integer id)
	{
		Search search = new Search();
		search.addFilterEqual("deleted", false);
		search.addFilterEqual("account.id", id);
		search.addSort("id", true);
		return activityDao.search(search);
	}
	
	@Override
	public List<Activity> getEventsByOwnerId(Integer id)
	{
		Search search = new Search();
		search.addFilterEqual("deleted", false);
		search.addFilterEqual("account.id", id);
		search.addFilterEqual("activityType", "event");
		search.addSort("id", true);
		return activityDao.search(search);
	}
	
	@Override
	public Set<Activity> getEventsByCommId(Integer id)
	{	
		Set<Activity>activities = new HashSet<>();
		activities.addAll(activityDao.findEventsByLayerId(id));
		/*
		Set<Activity>activities = layerDao.findByIdWithActivities(id).getActivities();
		Set<Activity> filteredActivities = new HashSet<>();
		for(Activity act:activities){
			if(act.getActivityType().equalsIgnoreCase("event")){
				filteredActivities.add(act);
			}
		}
		return filteredActivities;
		*/
		return activities;
	}
	
	@Override
	public Set<Activity> getGatedEventsByCommId(List<Integer> ids)
	{	
		Set<Activity> activities = new HashSet<>();
		for(Integer id:ids){
			activities.addAll(layerDao.findByIdGatedActivities(id).getActivities());
		}
		
		Set<Activity> filteredActivities = new HashSet<>();
		for(Activity act:activities){
			if(act.getActivityType().equalsIgnoreCase("event")){
				filteredActivities.add(act);
			}
		}
		
		return filteredActivities;
	}
	
	
	
	
	
	@Override
	public Set<Activity> getInfosByCommId(Integer id)
	{	
		Set<Activity> activities = new HashSet<>();
		activities.addAll(activityDao.findInfosByLayerId(id));
		/*Set<Activity>activities = layerDao.findByIdWithActivities(id).getActivities();
		Set<Activity> filteredActivities = new HashSet<>();
		for(Activity act:activities){
			if(act.getActivityType().equalsIgnoreCase("info")){
				filteredActivities.add(act);
			}
		}
		return filteredActivities;*/
		return activities;
	}
	
	@Override
	public Set<Activity> getEventsByCommIds(List<Integer> ids)
	{
		Set<Activity> activities = new HashSet<>();
		for(Integer id:ids){
			activities.addAll(getEventsByCommId(id));
		}
		
		
		return activities;
	}
	
	@Override
	public Set<Activity> getInfosByCommIds(List<Integer> ids)
	{
		
		Set<Activity> activities = new HashSet<>();
		for(Integer id:ids){
			activities.addAll(getInfosByCommId(id));
		}
		
		
		return activities;
	}
	
	
	
	
	@Override
	public Activity findById(Integer id) 
	{
		return activityDao.find(id);
	}
	
	
	@Override
	public Integer countEv() 
	{
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "event");	
		return activityDao.count(search);
	}
	
	
	@Override
	public Integer countEvByOwner(Integer id) 
	{
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "event");
			search.addFilterEqual("account.id", id);
		return activityDao.count(search);
	}
	
	@Override
	public Integer countInf(Integer id) 
	{
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "info");
			search.addFilterEqual("account.id", id);
		return activityDao.count(search);
	}
	
	
	@Override
	public Integer countSurvey(Integer id) 
	{
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "survey");
			search.addFilterEqual("account.id", id);
		return activityDao.count(search);
	}
	
	@Override
	public List<Activity> pagedActivities(Integer initial, Integer max, Integer id)
	{
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "event");
			search.addFilterEqual("account.id", id);
			search.setFirstResult(initial);
			search.setMaxResults(max);
		return activityDao.search(search);
	}
	
	@Override
	public Set<String> getIcons(Set<Layer> layers)
	{
		Set<Activity>activities = new HashSet<>();
		for(Layer layer:layers){
			try {
			activities.addAll(layerDao.findByIdWithActsEv(layer.getId()).getActivities());
			}catch(Exception e){
				logger.error("Empty: Get icons issue 297 activity Service");
			}
		}
		//Search search = new Search();
		//List<Activity> acts = activityDao.search(search);
		Set<String> icons = new HashSet<>();
		for(Activity act:activities){
				icons.add(act.getIconUrl());
		}
		return icons;
	}
	
	@Override
	public Set<Activity> getIconsB(Set<Layer> layers)
	{
		Set<Activity> activities = new HashSet<>();
		for(Layer layer:layers){		
			try {
				activities.addAll(layerDao.findByIdWithActsEv(layer.getId()).getActivities());
			}catch(Exception e){
				logger.error("Get icons issue 297 activity Service");
			}
		}
		
		Set<String> iconUrl = new HashSet<>();
		Set<Activity> icons = new HashSet<>();
		
		for(Activity act:activities){
			if(!iconUrl.contains(act.getIconUrl())){
				icons.add(act);
				iconUrl.add(act.getIconUrl());
			}
		}
		return icons;
	}
	
	@Override
	public Set<Activity> getIconsInfos(Set<Layer> layers)
	{
		Set<Activity>activities = new HashSet<>();
		for(Layer layer:layers){
			activities.addAll(layerDao.findByIdWithActsInf(layer.getId()).getActivities());
		}

		Set<String> iconUrl = new HashSet<>();
		Set<Activity> icons = new HashSet<>();
		
		for(Activity act:activities){
			if(!iconUrl.contains(act.getIconUrl())){
				icons.add(act);
				iconUrl.add(act.getIconUrl());
			}
		}
		return icons;
	}
	
	@Override
	public Set<Activity> getIconsInfosB(Set<Layer> layers)
	{
		Set<Activity>activities = new HashSet<>();
		for(Layer layer:layers){
			activities.addAll(layerDao.findByIdWithActsEv(layer.getId()).getActivities());
		}

		Set<String> iconUrl = new HashSet<>();
		Set<Activity> icons = new HashSet<>();
		
		for(Activity act:activities){
			if(!iconUrl.contains(act.getIconUrl())){
				icons.add(act);
				iconUrl.add(act.getIconUrl());
			}
		}
		return icons;
	}
	
	@Override
	public Set<Banner> getBanners(Set<Layer> layers)
	{
		Set<Activity>activities = new HashSet<>();
		
		for(Layer layer:layers){
			Integer i = layer.getId();
			Layer lay = layerDao.findByIdWithActsEv(i);
			Set<Activity> acts =lay.getActivities(); 
			if(acts.size()!=0){
				activities.addAll(layerDao.findByIdWithActsEv(layer.getId()).getActivities());
			}
		}
		
		Set<Banner> bannersAll = new HashSet<>();
		for(Activity act:activities){
			bannersAll.addAll(act.getBanners());
		}
		
		Set<String> bannerUrl = new HashSet<>();
		Set<Banner> banners = new HashSet<>();
		
		for(Banner bann:bannersAll){
			if(!bannerUrl.contains(bann.getImageUrl())){
				banners.add(bann);
				bannerUrl.add(bann.getImageUrl());
			}
		}
		
		if(banners.size()<1) banners.add(bannerService.findById(1));
		return banners;
	}
	
	@Override
	public Set<File> getEvVideos(Set<Layer> layers)
	{
		Set<Activity>activities = new HashSet<>();
		for(Layer layer:layers){
			activities.addAll(layerDao.findByIdWithActsEv(layer.getId()).getActivities());
		}
		Set<File> files = new HashSet<>();
		Set<String> filesUrl = new HashSet<>();
		for(Activity act:activities){
			for(File file:act.getFiles()){
				if(file.getUrl().contains(".mp4") && !filesUrl.contains(file.getUrl())){
					files.add(file);
					filesUrl.add(file.getUrl());
				}	
			}
		}
		return files;
	}
	
	
	@Override
	public Set<File> getEvFiles(Set<Layer> layers)
	{
		Set<Activity>activities = new HashSet<>();
		for(Layer layer:layers){
			activities.addAll(layerDao.findByIdWithActsEv(layer.getId()).getActivities());
		}
		Set<File> files = new HashSet<>();
		Set<String> filesUrl = new HashSet<>();
		for(Activity act:activities){
			for(File file:act.getFiles()){
				if(file.getUrl().contains(".pdf") && !filesUrl.contains(file.getUrl())){
					files.add(file);
					filesUrl.add(file.getUrl());
				}	
			}
		}
		return files;
	}
	
	
	@Override
	public List<Activity> pagedInfosByLayer(Integer initial, Integer max, Integer id, Integer layerId)
	{
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "info");
			search.addFilterSome("layers", Filter.equal("id", layerId));
			search.setFirstResult(initial);
			search.setMaxResults(max);
		return activityDao.search(search);
	}
	
	@Override
	public List<Activity> surveyActivity(Integer initial, Integer max, Integer id)
	{
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "survey");
			search.addFilterEqual("account.id", id);
			search.setFirstResult(initial);
			search.setMaxResults(max);
		return activityDao.search(search);
	}
	
	@Override
	public List<Activity> pagedSurveysByLayer(Integer initial, Integer max, Integer id, Integer layerId)
	{
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "survey");
			search.addFilterSome("layers", Filter.equal("id", layerId));
			search.setFirstResult(initial);
			search.setMaxResults(max);
		return activityDao.search(search);
	}
	
	@Override
	public Activity findByIdWithRelations(Integer id) {
		return activityDao.findByIdWithRelations(id);
	}
	
	@Override
	public Activity findwithBanners(Integer id) {
		return activityDao.findByIdBanners(id);
	}
	
	@Override
	public Activity findwithLayers(Integer id) {
		return activityDao.findByIdLayers(id);
	}
	
	@Override
	public Activity findwithFiles(Integer id) {
		return activityDao.findByIdFiles(id);
	}
	
	
	
	@Override
	public Activity findByIdWithBanners(Integer id) {
		return activityDao.findByIdWithBanners(id);
	}
	
	
	@Override
	public Activity getStartandEndDatesfromEvent(Activity act) throws ParseException{
		
		if(!act.equals(null)){
			Event event = act.getEvent();
			String startT="";
			String endT="";
			Set <EventCalendarItem> ecis = event.getEventCalendarItems();
			for(EventCalendarItem eci:ecis){
				String[] calData = eci.getCalData().split("[:'\\r''\\n''\\u0085']");
				String newECI=eci.getCalData().toString();
		        newECI = newECI.replaceAll("\\r\\n|\\r|\\n" , "%0A");
				act.setCalData(newECI);
				System.out.println(">>> CalDATA: "+newECI);
				for(int i=0; i<calData.length; i++){
					if(calData[i].equalsIgnoreCase("VEVENT") && i!=calData.length){
						for(int j=i; j<calData.length; j++){
							
							java.util.Calendar startDateTime = new GregorianCalendar();
							if(calData[j].equalsIgnoreCase("DTSTART")||
							   calData[j].equalsIgnoreCase("DTSTART;TZID=Australia/Adelaide")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setStartDate(sdfDate.format(date));
								System.out.println(">>> startDate: "+act.getStartDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampA=new int[6];
						         for(int m=0;m<=5;m++){
						        	 partstimeStampA[m] = Integer.parseInt(parts3[m]);
						         }
						         startDateTime.set(java.util.Calendar.MONTH, partstimeStampA[1]-1);
						         startDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampA[2]);
						         startDateTime.set(java.util.Calendar.YEAR, partstimeStampA[0]);
						         startDateTime.set(java.util.Calendar.HOUR_OF_DAY, partstimeStampA[3]);
						         startDateTime.set(java.util.Calendar.MINUTE, partstimeStampA[4]);
						         startT=startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE));
						         System.out.println(">>> startT: "+startDateTime.get(Calendar.HOUR_OF_DAY)+":"+
						        		 			String.format("%02d", startDateTime.get(Calendar.MINUTE)));
						         act.setRangeTime(startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE))+" hrs - ");
						         act.setStartDT(sDate);
						         act.setStartTime(startT+":00");
							}
							if(calData[j].equalsIgnoreCase("DTSTART;VALUE=DATE")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setStartDate(sdfDate.format(date));
								System.out.println(">>> startDate: "+act.getStartDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampA=new int[6];
						         for(int m=0;m<=2;m++){
						        	 partstimeStampA[m] = Integer.parseInt(parts3[m]);
						         }
						         startDateTime.set(java.util.Calendar.MONTH, partstimeStampA[1]-1);
						         startDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampA[2]);
						         startDateTime.set(java.util.Calendar.YEAR, partstimeStampA[0]);
						         startDateTime.set(java.util.Calendar.HOUR_OF_DAY, 10);
						         startDateTime.set(java.util.Calendar.MINUTE, 00);
						         startT=startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE));
						         System.out.println("startT: "+startDateTime.get(Calendar.HOUR_OF_DAY)+":"+
						        		 			String.format("%02d", startDateTime.get(Calendar.MINUTE)));
						         act.setRangeTime(startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE))+" hrs - ");
						         act.setStartDT(sDate);
						         act.setStartTime(startT+":00");
							}
							java.util.Calendar endDateTime = new GregorianCalendar();
							if(calData[j].equalsIgnoreCase("DTEND")||
							   calData[j].equalsIgnoreCase("DTEND;TZID=Australia/Adelaide")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setEndDate(sdfDate.format(date));
								System.out.println(">>> endDate: "+act.getEndDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampB=new int[6];
						         for(int k=0;k<=5;k++){
						        	 partstimeStampB[k] = Integer.parseInt(parts3[k]);
						         }
						         endDateTime.set(java.util.Calendar.MONTH, partstimeStampB[1]-1);
						         endDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampB[2]);
						         endDateTime.set(java.util.Calendar.YEAR, partstimeStampB[0]);
						         endDateTime.set(java.util.Calendar.HOUR_OF_DAY, partstimeStampB[3]);
						         endDateTime.set(java.util.Calendar.MINUTE, partstimeStampB[4]);
						         endT=endDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", endDateTime.get(Calendar.MINUTE));
						         act.setEndDT(sDate);
						         act.setEndTime(endT+":00");
							}
							if(calData[j].equalsIgnoreCase("DTEND;VALUE=DATE")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setEndDate(sdfDate.format(date));
								System.out.println(">>> endDate: "+act.getEndDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampB=new int[6];
						         for(int k=0;k<=2;k++){
						        	 partstimeStampB[k] = Integer.parseInt(parts3[k]);
						         }
						         endDateTime.set(java.util.Calendar.MONTH, partstimeStampB[1]-1);
						         endDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampB[2]);
						         endDateTime.set(java.util.Calendar.YEAR, partstimeStampB[0]);
						         endDateTime.set(java.util.Calendar.HOUR_OF_DAY, 11);
						         endDateTime.set(java.util.Calendar.MINUTE, 00);
						         endT=endDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", endDateTime.get(Calendar.MINUTE));
						         act.setEndDT(sDate);
						         act.setEndTime(endT+":00");
							}
						}
					}
				}
			}
			act.setRangeTime(startT+"hrs-"+endT+"hrs");
			
	        //Check if startDate is still valid, then the activity is not shown.
			long epoch = System.currentTimeMillis()/1000;	 
	        java.util.Date dateTimeStamp = new java.util.Date(epoch*1000L);
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	        String currDay = sdf.format(dateTimeStamp);
			
            if( act != null && act.getStartDate() != null && act.getStartDate().compareTo(currDay)>-1){
            	return act;
			}
		}
		return act;
	}
	
	
	@Override
	public List<Activity> getStartandEndDatesfromEvents(List<Activity> activities) throws ParseException{
		List<Activity> latestActivities = new ArrayList<Activity>();
		
		for(Activity act:activities){
			Event event = act.getEvent();
			String startT="";
			String endT="";
			Set <EventCalendarItem> ecis = event.getEventCalendarItems();
			for(EventCalendarItem eci:ecis){
				String[] calData = eci.getCalData().split("[:'\\r''\\n''\\u0085']");
				String newECI=eci.getCalData().toString();
		        newECI = newECI.replaceAll("\\r\\n|\\r|\\n" , "%0A");
				act.setCalData(newECI);
				System.out.println(">>> CalDATA: "+newECI);
				for(int i=0; i<calData.length; i++){
					if(calData[i].equalsIgnoreCase("VEVENT") && i!=calData.length){
						for(int j=i; j<calData.length; j++){
							
							java.util.Calendar startDateTime = new GregorianCalendar();
							if(calData[j].equalsIgnoreCase("DTSTART")||
							   calData[j].equalsIgnoreCase("DTSTART;TZID=Australia/Adelaide")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setStartDate(sdfDate.format(date));
								System.out.println(">>> startDate: "+act.getStartDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampA=new int[6];
						         for(int m=0;m<=5;m++){
						        	 partstimeStampA[m] = Integer.parseInt(parts3[m]);
						         }
						         startDateTime.set(java.util.Calendar.MONTH, partstimeStampA[1]-1);
						         startDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampA[2]);
						         startDateTime.set(java.util.Calendar.YEAR, partstimeStampA[0]);
						         startDateTime.set(java.util.Calendar.HOUR_OF_DAY, partstimeStampA[3]);
						         startDateTime.set(java.util.Calendar.MINUTE, partstimeStampA[4]);
						         startT=startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE));
						         System.out.println(">>> startT: "+startDateTime.get(Calendar.HOUR_OF_DAY)+":"+
						        		 			String.format("%02d", startDateTime.get(Calendar.MINUTE)));
						         act.setRangeTime(startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE))+" hrs - ");
						         act.setStartDT(sDate);
							}
							if(calData[j].equalsIgnoreCase("DTSTART;VALUE=DATE")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setStartDate(sdfDate.format(date));
								System.out.println(">>> startDate: "+act.getStartDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampA=new int[6];
						         for(int m=0;m<=2;m++){
						        	 partstimeStampA[m] = Integer.parseInt(parts3[m]);
						         }
						         startDateTime.set(java.util.Calendar.MONTH, partstimeStampA[1]-1);
						         startDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampA[2]);
						         startDateTime.set(java.util.Calendar.YEAR, partstimeStampA[0]);
						         startDateTime.set(java.util.Calendar.HOUR_OF_DAY, 10);
						         startDateTime.set(java.util.Calendar.MINUTE, 00);
						         startT=startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE));
						         System.out.println("startT: "+startDateTime.get(Calendar.HOUR_OF_DAY)+":"+
						        		 			String.format("%02d", startDateTime.get(Calendar.MINUTE)));
						         act.setRangeTime(startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE))+" hrs - ");
						         act.setStartDT(sDate);
							}
							java.util.Calendar endDateTime = new GregorianCalendar();
							if(calData[j].equalsIgnoreCase("DTEND")||
							   calData[j].equalsIgnoreCase("DTEND;TZID=Australia/Adelaide")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setEndDate(sdfDate.format(date));
								System.out.println(">>> endDate: "+act.getEndDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampB=new int[6];
						         for(int k=0;k<=5;k++){
						        	 partstimeStampB[k] = Integer.parseInt(parts3[k]);
						         }
						         endDateTime.set(java.util.Calendar.MONTH, partstimeStampB[1]-1);
						         endDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampB[2]);
						         endDateTime.set(java.util.Calendar.YEAR, partstimeStampB[0]);
						         endDateTime.set(java.util.Calendar.HOUR_OF_DAY, partstimeStampB[3]);
						         endDateTime.set(java.util.Calendar.MINUTE, partstimeStampB[4]);
						         endT=endDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", endDateTime.get(Calendar.MINUTE));
						         act.setEndDT(sDate);
							}
							if(calData[j].equalsIgnoreCase("DTEND;VALUE=DATE")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setEndDate(sdfDate.format(date));
								System.out.println(">>> endDate: "+act.getEndDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampB=new int[6];
						         for(int k=0;k<=2;k++){
						        	 partstimeStampB[k] = Integer.parseInt(parts3[k]);
						         }
						         endDateTime.set(java.util.Calendar.MONTH, partstimeStampB[1]-1);
						         endDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampB[2]);
						         endDateTime.set(java.util.Calendar.YEAR, partstimeStampB[0]);
						         endDateTime.set(java.util.Calendar.HOUR_OF_DAY, 11);
						         endDateTime.set(java.util.Calendar.MINUTE, 00);
						         endT=endDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", endDateTime.get(Calendar.MINUTE));
						         act.setEndDT(sDate);
							}
						}
					}
				}
			}
			act.setRangeTime(startT+"hrs-"+endT+"hrs");
			
	        //Check if startDate is still valid, then the activity is not shown.
			long epoch = System.currentTimeMillis()/1000;	 
	        java.util.Date dateTimeStamp = new java.util.Date(epoch*1000L);
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	        String currDay = sdf.format(dateTimeStamp);
			
            if( act != null && act.getEndDate() != null && act.getEndDate().compareTo(currDay)>-1){
				latestActivities.add(act);
			}
		}
		return latestActivities;
	}
	
	
	@Override
	public List<Activity> getStartandEndDatesfromEventsPast(List<Activity> activities) throws ParseException{
		List<Activity> latestActivities = new ArrayList<Activity>();
		
		for(Activity act:activities){
			Event event = act.getEvent();
			String startT="";
			String endT="";
			Set <EventCalendarItem> ecis = event.getEventCalendarItems();
			for(EventCalendarItem eci:ecis){
				String[] calData = eci.getCalData().split("[:'\\r''\\n''\\u0085']");
				String newECI=eci.getCalData().toString();
		        newECI = newECI.replaceAll("\\r\\n|\\r|\\n" , "%0A");
				act.setCalData(newECI);
				System.out.println(">>> CalDATA: "+newECI);
				for(int i=0; i<calData.length; i++){
					if(calData[i].equalsIgnoreCase("VEVENT") && i!=calData.length){
						for(int j=i; j<calData.length; j++){
							
							java.util.Calendar startDateTime = new GregorianCalendar();
							if(calData[j].equalsIgnoreCase("DTSTART")||
							   calData[j].equalsIgnoreCase("DTSTART;TZID=Australia/Adelaide")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setStartDate(sdfDate.format(date));
								System.out.println(">>> startDate: "+act.getStartDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampA=new int[6];
						         for(int m=0;m<=5;m++){
						        	 partstimeStampA[m] = Integer.parseInt(parts3[m]);
						         }
						         startDateTime.set(java.util.Calendar.MONTH, partstimeStampA[1]-1);
						         startDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampA[2]);
						         startDateTime.set(java.util.Calendar.YEAR, partstimeStampA[0]);
						         startDateTime.set(java.util.Calendar.HOUR_OF_DAY, partstimeStampA[3]);
						         startDateTime.set(java.util.Calendar.MINUTE, partstimeStampA[4]);
						         startT=startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE));
						         System.out.println(">>> startT: "+startDateTime.get(Calendar.HOUR_OF_DAY)+":"+
						        		 			String.format("%02d", startDateTime.get(Calendar.MINUTE)));
						         act.setRangeTime(startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE))+" hrs - ");
						         act.setStartDT(sDate);
							}
							if(calData[j].equalsIgnoreCase("DTSTART;VALUE=DATE")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setStartDate(sdfDate.format(date));
								System.out.println(">>> startDate: "+act.getStartDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampA=new int[6];
						         for(int m=0;m<=2;m++){
						        	 partstimeStampA[m] = Integer.parseInt(parts3[m]);
						         }
						         startDateTime.set(java.util.Calendar.MONTH, partstimeStampA[1]-1);
						         startDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampA[2]);
						         startDateTime.set(java.util.Calendar.YEAR, partstimeStampA[0]);
						         startDateTime.set(java.util.Calendar.HOUR_OF_DAY, 10);
						         startDateTime.set(java.util.Calendar.MINUTE, 00);
						         startT=startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE));
						         System.out.println("startT: "+startDateTime.get(Calendar.HOUR_OF_DAY)+":"+
						        		 			String.format("%02d", startDateTime.get(Calendar.MINUTE)));
						         act.setRangeTime(startDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", startDateTime.get(Calendar.MINUTE))+" hrs - ");
						         act.setStartDT(sDate);
							}
							java.util.Calendar endDateTime = new GregorianCalendar();
							if(calData[j].equalsIgnoreCase("DTEND")||
							   calData[j].equalsIgnoreCase("DTEND;TZID=Australia/Adelaide")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setEndDate(sdfDate.format(date));
								System.out.println(">>> endDate: "+act.getEndDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampB=new int[6];
						         for(int k=0;k<=5;k++){
						        	 partstimeStampB[k] = Integer.parseInt(parts3[k]);
						         }
						         endDateTime.set(java.util.Calendar.MONTH, partstimeStampB[1]-1);
						         endDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampB[2]);
						         endDateTime.set(java.util.Calendar.YEAR, partstimeStampB[0]);
						         endDateTime.set(java.util.Calendar.HOUR_OF_DAY, partstimeStampB[3]);
						         endDateTime.set(java.util.Calendar.MINUTE, partstimeStampB[4]);
						         endT=endDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", endDateTime.get(Calendar.MINUTE));
						         act.setEndDT(sDate);
							}
							if(calData[j].equalsIgnoreCase("DTEND;VALUE=DATE")){
								String sDate= calData[j+1];
								System.out.println(">>> sDate: "+sDate);
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
								act.setEndDate(sdfDate.format(date));
								System.out.println(">>> endDate: "+act.getEndDate().toString());
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampB=new int[6];
						         for(int k=0;k<=2;k++){
						        	 partstimeStampB[k] = Integer.parseInt(parts3[k]);
						         }
						         endDateTime.set(java.util.Calendar.MONTH, partstimeStampB[1]-1);
						         endDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampB[2]);
						         endDateTime.set(java.util.Calendar.YEAR, partstimeStampB[0]);
						         endDateTime.set(java.util.Calendar.HOUR_OF_DAY, 11);
						         endDateTime.set(java.util.Calendar.MINUTE, 00);
						         endT=endDateTime.get(Calendar.HOUR_OF_DAY)+":"+String.format("%02d", endDateTime.get(Calendar.MINUTE));
						         act.setEndDT(sDate);
							}
						}
					}
				}
			}
			act.setRangeTime(startT+"hrs-"+endT+"hrs");
			
	        //Check if startDate is still valid, then the activity is not shown.
			long epoch = System.currentTimeMillis()/1000;	 
	        java.util.Date dateTimeStamp = new java.util.Date(epoch*1000L);
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	        String currDay = sdf.format(dateTimeStamp);
			
            if( act != null && act.getEndDate() != null && act.getEndDate().compareTo(currDay)<1){
				latestActivities.add(act);
			}
		}
		return latestActivities;
	}
	
	
	@Override
	public List<Activity> getStartandEndDTfromEvent(List<Activity> activities) throws ParseException{
		List<Activity> latestActivities = new ArrayList<Activity>();
		
		for(Activity act:activities){
			Event event = act.getEvent();
			String startT="";
			String endT="";
			Set <EventCalendarItem> ecis = event.getEventCalendarItems();
			for(EventCalendarItem eci:ecis){
				String[] calData = eci.getCalData().split("[:'\\r''\\n''\\u0085']");
				for(int i=0; i<calData.length; i++){
					if(calData[i].equalsIgnoreCase("VEVENT") && i!=calData.length){
						for(int j=i; j<calData.length; j++){
							
							java.util.Calendar startDateTime = new GregorianCalendar();
							if(calData[j].equalsIgnoreCase("DTSTART")|| calData[j].equalsIgnoreCase("DTSTART;TZID=Australia/Adelaide")){
								String sDate= calData[j+1];
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
								act.setStartDate(sdfDate.format(date));
							}
							if(calData[j].equalsIgnoreCase("DTSTART;VALUE=DATE")){
								String sDate= calData[j+1];
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
								Date date = sdf.parse(sDate);

								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampA=new int[6];
						         for(int m=0;m<=2;m++){
						        	 partstimeStampA[m] = Integer.parseInt(parts3[m]);
						         }
						         
						         startDateTime.set(java.util.Calendar.MONTH, partstimeStampA[1]-1);
						         startDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampA[2]);
						         startDateTime.set(java.util.Calendar.YEAR, partstimeStampA[0]);
						         startDateTime.set(java.util.Calendar.HOUR_OF_DAY, 10);
						         startDateTime.set(java.util.Calendar.MINUTE, 00);
						         startDateTime.set(java.util.Calendar.SECOND, 00);
						         
						         SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
						         logger.debug("Object:"+	startDateTime);
						         try {
						         act.setStartDate(sdfDate.format(startDateTime));
						         }catch (Exception e) {
						        	 logger.error("Object:"+	startDateTime);
								}
							}
							java.util.Calendar endDateTime = new GregorianCalendar();
							if(calData[j].equalsIgnoreCase("DTEND")|| calData[j].equalsIgnoreCase("DTEND;TZID=Australia/Adelaide")){
								String sDate= calData[j+1];
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
								Date date = sdf.parse(sDate);
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
								act.setEndDate(sdfDate.format(date));
							}
							if(calData[j].equalsIgnoreCase("DTEND;VALUE=DATE")){
								String sDate= calData[j+1];
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
								Date date = sdf.parse(sDate);
								
								SimpleDateFormat sdfDaT = new SimpleDateFormat("yyyy-MM-dd");
						        String formattedDate3 = sdfDaT.format(date);
						        String[] parts3 = formattedDate3.split("-");
						         int[] partstimeStampB=new int[6];
						         for(int k=0;k<=2;k++){
						        	 partstimeStampB[k] = Integer.parseInt(parts3[k]);
						         }
						         endDateTime.set(java.util.Calendar.MONTH, partstimeStampB[1]-1);
						         endDateTime.set(java.util.Calendar.DAY_OF_MONTH, partstimeStampB[2]);
						         endDateTime.set(java.util.Calendar.YEAR, partstimeStampB[0]);
						         endDateTime.set(java.util.Calendar.HOUR_OF_DAY, 11);
						         endDateTime.set(java.util.Calendar.MINUTE, 00);
						         endDateTime.set(java.util.Calendar.SECOND, 00);
						         
						         SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
						         act.setStartDate(sdfDate.format(endDateTime));
							}
						}
					}
				}
			}
			latestActivities.add(act);
		}
		return latestActivities;
	}
	
	@Override
	public List<EventDTO> generateEventDTOs(List<Activity> activities) throws ParseException{
		
		List<Activity> acts = getStartandEndDTfromEvent(activities);
		List<EventDTO> generatedEvDTO = new ArrayList<EventDTO>();
		
		for(Activity act:acts){
			EventDTO edto= new EventDTO();
			edto.setId(act.getId());
			edto.setTitle(act.getContentTitle());
			edto.setStart(act.getStartDate());
			edto.setEnd(act.getEndDate());
			generatedEvDTO.add(edto);
		}
		return generatedEvDTO;
	}
	
	public List<Activity> findByLayerId(Integer id){
		
		return activityDao.findByLayerId(id);
	}
	
	public Integer calculateDuration(String startTime, String endTime){
		
		String[] start = startTime.split(":");
		int hrs = Integer.parseInt(start[0]);
		int mins= Integer.parseInt(start[1]);
		
		String[] end = endTime.split(":");
		int hrsB = Integer.parseInt(end[0]);
		int minsB= Integer.parseInt(end[1]);
		
		Integer durHrs = (hrsB-hrs)*60;
		Integer durMins = minsB-mins;
		Integer duration= durHrs+durMins; 
		if(duration==0) duration=60;
		
		return duration;
	}
	
	public Integer calculateDurationExtended(String startDate, String endDate, String startTime, String endTime){
		
		String[] sDate = startDate.split("/");
		int dayA   = Integer.parseInt(sDate[2]);
		
		String[] eDate = endDate.split("/");
		int dayB   = Integer.parseInt(eDate[2]);
		
		Integer durDays = (dayB-dayA)*24*60;
		if(durDays<=0) durDays=0;
		
		String[] start = startTime.split(":");
		int hrs = Integer.parseInt(start[0]);
		int mins= Integer.parseInt(start[1]);
		
		String[] end = endTime.split(":");
		int hrsB = Integer.parseInt(end[0]);
		int minsB= Integer.parseInt(end[1]);
		
		Integer durHrs = (hrsB-hrs)*60;
		Integer durMins = minsB-mins;
		Integer duration= durHrs+durMins+durDays; 
		
		if(duration==0) duration=60;
		
		return duration;
	}
	
	
	@Override 
	public List<Activity> pagedActs(Integer initial, Integer max, String type, Long dtend, Layer layer)
	{		
		List<Activity> size = activityDao.queryFindEventsbyLayer(type, dtend, layer);
		
        return size.subList(initial, max);

	}
	
	
	@Override
	public List<Activity> pagedActsPast(Integer initial, Integer max, String type, Long dtend, Layer layer)
	{	
		List<Activity> size = activityDao.queryFindEventsbyLayerPast(type, dtend, layer);
		
		return size.subList(initial, max);
	}
	
	
	@Override
	public Integer pagedActsPages(String type, Long dtend, Integer layerId)
	{	
		Integer size = activityDao.queryEventsbyLayer(type, dtend, layerId);
		logger.debug("Events = "+size);
		
		if(size>0){
			return size;
		}
		else return 0;
	}
	
	
	@Override
	public Integer pagedActsPagesPast(String type, Long dtend, Integer layerId)
	{	
		Integer size = activityDao.queryEventsbyLayerPast(type, dtend, layerId);
		logger.debug("Events Past = "+size);
		
		if(size>0){
			return size;
		}
		else return 0;
	}
	
	
	@Override 
	public List<RecommendedActivities> findRecommendedActivitiesfromActs(Set<Activity> events){
		List<RecommendedActivities> activs = new ArrayList<>();
		List<RecommendedActivities> recommendedActs = new ArrayList<>();
		for(Activity act:events){
			Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.addFilterEqual("activityType", "survey");
			activs= activityDao.search(search);
			recommendedActs.addAll(activs);
		}
		
	return activs;
	}
	
}
