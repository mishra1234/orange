package au.com.dmsq.service.impl;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import au.com.dmsq.dao.ActivityPaymentDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.ActivityPayment;
import au.com.dmsq.service.ActivityPaymentService;
import com.googlecode.genericdao.search.Search;
@Service
@Transactional
public class ActivityPaymentServiceImpl extends AnAbstractService implements ActivityPaymentService{
	private static final Logger logger = LoggerFactory.getLogger(ActivityPaymentServiceImpl.class);
	@Autowired 
	private ActivityPaymentDao activityPaymentDao;
	
	@Override
	public boolean save(ActivityPayment activityPay) {
		return activityPaymentDao.save(activityPay);
	}
	
	@Override
	public List<ActivityPayment> getAllPaid(Account account)
	{
		Search search = new Search();
		search.addFilterEqual("account" , account);
		return activityPaymentDao.search(search);
	}
}
