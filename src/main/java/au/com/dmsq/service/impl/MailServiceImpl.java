package au.com.dmsq.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sendgrid.*;

import javax.servlet.ServletContext;

import au.com.dmsq.dao.MailDao;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Mail;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;
import au.com.dmsq.service.LayerService;

@Service
@Transactional
public class MailServiceImpl implements MailService {
	
	@Autowired private MailDao mailDao;
	@Autowired LayerService communityService;
	@Autowired ActivityService activityService;
	@Autowired MailContentService mcs;
	@Autowired ServletContext servletContext;
	
	//public final String WEBSVR = mcs.readWSConn();
	
	@Override
	public boolean save(Mail mail) {
		return mailDao.save(mail);
	}
	
	@Override
	public void welcomeMail(String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		String fromname = "Community Connect Me";
		
	    SendGrid.Email email = new SendGrid.Email();
	    email.setFromName(fromname);
	    email.addTo(to);
	    email.setFrom(from);
	    email.setSubject(subject);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	
	@Override
	public void resetPasswordMail(String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		String fromname = "Community Connect Me";
		
	    SendGrid.Email email = new SendGrid.Email();
	   
	    email.setFromName(fromname);
	    email.addTo(to);
	    email.setFrom(from);
	    email.setSubject(subject);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	@Override
	public void accessRequestManagerEmail(String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		String fromname = "Community Connect Me";
		
	    SendGrid.Email email = new SendGrid.Email();
	   
	    email.setFromName(fromname);
	    email.addTo(to);
	    email.setFrom(from);
	    email.setSubject(subject);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	@Override
	public void connectedCommunityConfirmation(String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		String fromname = "Community Connect Me";
		
	    SendGrid.Email email = new SendGrid.Email();
	   
	    email.setFromName(fromname);
	    email.addTo(to);
	    email.setFrom(from);
	    email.setSubject(subject);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	@Override
	public void inviteEmail(String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		String fromname = "Community Connect Me";
		
	    SendGrid.Email email = new SendGrid.Email();
	   
	    email.setFromName(fromname);
	    email.addTo(to);
	    email.setFrom(from);
	    email.setSubject(subject);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	@Override
	public void accessGrantedEmail(String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		String fromname = "Community Connect Me";
		
	    SendGrid.Email email = new SendGrid.Email();
	   
	    email.setFromName(fromname);
	    email.addTo(to);
	    email.setFrom(from);
	    email.setSubject(subject);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	
	
	@Override
	public void accessRequestMemberEmail(String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		String fromname = "Community Connect Me";
		
	    SendGrid.Email email = new SendGrid.Email();
	   
	    email.setFromName(fromname);
	    email.addTo(to);
	    email.setFrom(from);
	    email.setSubject(subject);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	
	@Override
	public void subscriptionEmails(String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		String fromname = "Community Connect Me";
		
	    SendGrid.Email email = new SendGrid.Email();
	   
	    email.setFromName(fromname);
	    email.addTo(to);
	    email.setFrom(from);
	    email.setSubject(subject);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	
	@Override
	public void mailToOne(String fromname, String from, String to, String subject, String text){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
		 //String fromname="Community Connect Me";
	    SendGrid.Email email = new SendGrid.Email();
	    email.setFromName(fromname);
	    email.setFrom(from);
	    email.addTo(to);
	    email.setSubject(subject);
	    email.setText(text);
	    email.setHtml(text);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}

	
	@Override
	public void mailToSome(String fromname, String from, String to, String subject, String text, String[] bcc){
		SendGrid sendgrid = new SendGrid(mcs.readSGUser(), mcs.readSGPass());
	
	    SendGrid.Email email = new SendGrid.Email();
	    email.setFromName(fromname);
	    email.setFrom(from);
	    email.addTo(to);
	    email.addBcc(bcc);
	    email.setSubject(subject);
	    email.setText(text);
	    email.setFromName(fromname);
	
	    try {
	      SendGrid.Response response = sendgrid.send(email);
	      //System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	      //System.err.println(e);
	    }

	}
	
	
	@Override
	public void promote(Integer communityId, Integer activityId, String type){
		
		Layer layer = communityService.findByIdWithRelations(communityId);
		String[] mailTo = new String[layer.getAccountLayerAccesses().size()];
		String fromname = "Rural Connect";
		String mailFrom = "no_reply@ruralconnect.com";
		String mSubject = "Message from community: "+layer.getName().toString()+".";
		String body = "";
		
		if(type.equalsIgnoreCase("information"))
			body = mcs.readPromoInfoEmail();
		else 
			body = mcs.readPromoEventEmail();

		int x = 0;
		for(AccountLayerAccess ids:layer.getAccountLayerAccesses()){
			mailTo[x]=(ids.getAccount().getEmail());
			
			
			String link = mcs.readInviteEmail() + servletContext.getContextPath() + "/member/event/"+ activityId;
			body += "<p><p><p><p>" +
					"Dear "+ids.getAccount().getName()+","+
					"We would like to invite you, read the "+
					type+" about: <strong>"+ 
					activityService.get(activityId).getContentTitle() +"</strong><p><p>"+
					"You can read more by clicking at the following button."+ "<p><p>"+
					"<a href='"+link+"'><button style='margin-top:30px;padding: 11px 25px; color:#fff;background: #03ad5b;border: 1px solid #46b3d3; " +
					"	border-radius: 5px; cursor: pointer;'> Read more about "+
					activityService.get(activityId).getContentTitle()+" "+
					"</button></a><p><p>	 Or click the link below:<p><p> " +
					"	<a href='"+link+"'>"+link+"</a>"+
					"<p>Yours: <p>"+"<p><p>CC Me Support.<p><p>";
			link=link.replaceAll("pasteBodyHere", body);	
			
			mailToOne(fromname, mailFrom, mailTo[x], mSubject, link);
			
			System.out.println("mailTo  ==> "+ mailTo[x]);
			System.out.println("Subejct ==> "+ mSubject  );
			x++;
		}

	}

}
