package au.com.dmsq.service.impl;

import java.util.List;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Catalogue;

public class AnAbstractService {
	
	
	static boolean isProduction = true; 
	static boolean isBaikalProduction = true; 
	
	static public  String HOST = "jdbc:mysql://168.1.89.244:3306";
	static public  String DB = "ccme_winter_2";
	static public  String USER = "ruralConnect";
	static public  String PASSWORD = "t3qu1l@1";
	static public  String URLDB = HOST + "/" + DB;
	
	static public  String DEVHOST = "jdbc:mysql://localhost:3306";
	static public  String DEVDB = "ccme_winter_2";
	static public  String DEVUSER = "root";
	static public  String DEVPASSWORD = "root";
	static public  String DEVURLDB = DEVHOST + "/" + DEVDB;
//	static public  String DEVHOST = "jdbc:mysql://winterfell.cbi0v9fte9kc.ap-southeast-2.rds.amazonaws.com:3306";
//	static public  String DEVDB = "ccme_winter_2_dev";
//	static public  String DEVUSER = "carlos_dev";
//	static public  String DEVPASSWORD = "carlos_dev";
//	static public  String DEVURLDB = DEVHOST + "/" + DEVDB;
	
	static public  String BAIKALHOST = "jdbc:mysql://168.1.89.244:3306";
	static public  String BAIKALDB = "baikal";
	static public  String BAIKALUSER = "ruralConnect";
	static public  String BAIKALPASSWORD = "t3qu1l@1";
	static public  String BAIKALURLDB = BAIKALHOST + "/" + BAIKALDB;
	
	static public  String BAIKALDEVHOST = "jdbc:mysql://localhost:3306";
	static public  String BAIKALDEVDB = "baikal";
	static public  String BAIKALDEVUSER = "root";
	static public  String BAIKALDEVPASSWORD = "root";
	static public  String BAIKALDEVURLDB = BAIKALDEVHOST + "/" + BAIKALDEVDB;
//	static public  String BAIKALDEVHOST = "jdbc:mysql://dev.digitalmarketsquare.com:3306";
//	static public  String BAIKALDEVDB = "baikal";
//	static public  String BAIKALDEVUSER = "cms";
//	static public  String BAIKALDEVPASSWORD = "t3qu1l@1";
//	static public  String BAIKALDEVURLDB = BAIKALDEVHOST + "/" + BAIKALDEVDB;
	
	
	public boolean isProduction() {
		return isProduction;
	}
	
	public static String getHOST() {
		return isProduction?HOST:DEVHOST;
	}
	
	public static String getDB() {
		return isProduction?DB:DEVDB;
	}
	
	public static String getUSER() {
		return isProduction?USER:DEVUSER;
	}
	
	public static String getPASSWORD() {
		return isProduction?PASSWORD:DEVPASSWORD;
	}
	
	public static String getURLDB() {
		return isProduction?URLDB:DEVURLDB;
	}
	
	
	
	public boolean isBaikalProduction() {
		return isBaikalProduction;
	}
	
	public static String getBAIKALHOST() {
		return isBaikalProduction?BAIKALHOST:BAIKALDEVHOST;
	}
	
	public static String getBAIKALDB() {
		return isBaikalProduction?BAIKALDB:BAIKALDEVDB;
	}
	
	public static String getBAIKALUSER() {
		return isBaikalProduction?BAIKALUSER:BAIKALDEVUSER;
	}
	
	public static String getBAIKALPASSWORD() {
		return isBaikalProduction?BAIKALPASSWORD:BAIKALDEVPASSWORD;
	}
	
	public static String getBAIKALURLDB() {
		return isBaikalProduction?BAIKALURLDB:BAIKALDEVURLDB;
	}

	public Integer totalConnections(Account account)
			throws IllegalAccessException, ClassNotFoundException, Exception {
		// TODO Auto-generated method stub
		return null;
	}



}
