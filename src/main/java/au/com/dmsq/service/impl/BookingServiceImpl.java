package au.com.dmsq.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Booking;
import au.com.dmsq.service.BookingService;
import au.com.dmsq.dao.BookingDao;

@Service
@Transactional
public class BookingServiceImpl implements BookingService{
	
	@Autowired BookingDao bookingDao;
	
	@Override
	public boolean save(Booking booking) {
		return bookingDao.save(booking);
	}
	
	@Override
	public Booking getbyId(Integer id) {
		return bookingDao.find(id);
	}
	
	@Override
	public boolean deleteById(Integer id) {
		Booking account = getbyId(id);
		remove(account);
		return true;
	}
	
	@Override
	public void remove(Booking booking){
		bookingDao.remove(booking);
	}
	
	@Override
	public List<Booking> getBookingsByActivityId(Integer activityId)
	{
		return bookingDao.searchByActivityId(activityId);
	}
	
	
}
