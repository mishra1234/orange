package au.com.dmsq.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountConfirmation;
import au.com.dmsq.service.AccountConfirmationService;
import au.com.dmsq.dao.AccountConfirmationDao;

@Service
@Transactional
public class AccountConfirmationServiceImpl implements AccountConfirmationService{
	
	@Autowired private AccountConfirmationDao accountConfirmationDao;
	
	@Override
	public boolean save(AccountConfirmation ac) {
		return accountConfirmationDao.save(ac);
	}

	@Override
	public AccountConfirmation get(Integer id) {
		return accountConfirmationDao.find(id);
	}
	
	@Override
	public AccountConfirmation findById(Integer id) {
		return accountConfirmationDao.find(id);
	}	
	
	@Override
	public AccountConfirmation getByAccount(AccountConfirmation accountC) {
		return accountConfirmationDao.findByAccount(accountC);
	}

	@Override
	public boolean deleteById(Integer id) {
		AccountConfirmation ac = get(id);
		remove(ac);
		return true;
	}
	
	@Override
	public void remove(AccountConfirmation ac){
		accountConfirmationDao.remove(ac);
	}

	@Override
	public List<AccountConfirmation> findAll() {
		return accountConfirmationDao.findAll();
	}
	
	@Override
	public List<AccountConfirmation> getNonConfirmed() {
		Search search = new Search(); 
		search.addFilterEqual("confirmed", false);
		search.addSort("id", true);
		return accountConfirmationDao.search(search);
	}
	
	@Override
	public List<AccountConfirmation> getConfirmed() {
		Search search = new Search(); 
		search.addFilterEqual("confirmed", true);
		search.addSort("id", true);
		return accountConfirmationDao.search(search);
	}

}
