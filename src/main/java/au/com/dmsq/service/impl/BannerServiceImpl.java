package au.com.dmsq.service.impl;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import au.com.dmsq.dao.BannerDao;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.IconDTO;

import au.com.dmsq.service.BannerService;
import au.com.dmsq.service.MailContentService;


@Service
@Transactional
public class BannerServiceImpl extends AnAbstractService implements BannerService {

	
	@Autowired BannerDao bannerDao;
	@Autowired MailContentService mcs;
	
	@Override
	public List<Banner> findAll() {
		return bannerDao.findAll();
	}

	@Override
	public boolean save(Banner savebanner) 
	{
		return bannerDao.save(savebanner);
	}

	@Override
	public boolean remove(Banner removebanner) {
		return bannerDao.remove(removebanner);
	}

	@Override
	public boolean removeById(Banner layer, Integer id) {	
		return bannerDao.removeById(id);
	}
	
	@Override
	public Banner get(Integer id) {
		return bannerDao.find(id);
	}
	
	@Override
	public Banner findById(Integer id) {
		return bannerDao.find(id);
	}
	
	@Override
	public boolean deleteById(Integer id) {
		Banner banner = get(id);
		remove(banner);
		return true;
	}
	
	@Override
	public List<IconDTO> listImages() throws Exception {
        java.sql.Connection connection = null; 
        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
       // connection = DriverManager.getConnection(connectionURL, "carlos_dev", "carlos_dev");
        connection = DriverManager.getConnection(getURLDB(), getUSER(), getPASSWORD());
        
        if(!connection.isClosed()){
        	java.sql.Statement stmt = connection.createStatement();       	
        	try{
                String selectUnion = "SELECT image_url iconUrl FROM "+getDB()+".banner;" ;

        		ResultSet icons= stmt.executeQuery(selectUnion);
        		List<IconDTO> listIcons = new ArrayList<IconDTO>();
        		while(icons.next()){
        			IconDTO icon = new IconDTO();
        			icon.setIconUrl(icons.getString(1));
        			listIcons.add(icon);
        		}   		
        		return (List<IconDTO>) listIcons;
			}
			catch (SQLException e ) {
				System.out.println("SQL Exception : "+e);
			}
			finally{
	            if(stmt != null)
	            stmt.close();
	        }
        }
		return null;
	}
	
	
	@Override
	public int count() {
		Search search = new Search(); 	
	return bannerDao.count(search);
	}
	
	
	public List<Banner> pagedBanners(int initial, int max){
		Search search = new Search(); 
		search.setFirstResult(initial);
		search.setMaxResults(max);
	return bannerDao.search(search);
	}

}