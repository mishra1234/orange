package au.com.dmsq.service.impl;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.googlecode.genericdao.search.Search;
import au.com.dmsq.dao.MessageDao;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Message;
import au.com.dmsq.service.MessageService;
import au.com.dmsq.web.MessageController;

@Service
@Transactional
public class MessageServiceImpl implements MessageService{
	
	private static final Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

	@Autowired 
	private MessageDao messageDao;
	
	@Override
	public boolean save(Message message) {
		return messageDao.save(message);
	}

	@Override
	public Message get(Integer id) {
		return messageDao.find(id);
	}

	@Override
	public boolean deleteById(Integer id) {
		Message message = get(id);
		remove(message);
		return true;
	}

	@Override
	public List<Message> findAll() {
		return messageDao.findAll();
	}
	
	@Override
	public void remove(Message message){
		messageDao.remove(message);
	}
	
	public List<Message> getNonDeleted()
	{
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);
		search.addSort("id", true);
		return messageDao.search(search);
	}

	@Override
	public Message findById(Integer id) {
		return messageDao.find(id);
	}
	
	@Override
	public Integer count() {
		Search search = new Search(); 
		search.addFilterEqual("deleted", false);  
	return messageDao.count(search);
	}
	
	public static String getMonth(int month) {
    return new DateFormatSymbols().getMonths()[month-1].substring(0, 3);
	}
	@Override
	public Integer countIndividual(Integer id) throws ParseException {
		Search search = new Search(); 
		search.addFilterEqual("accountByFromId.id", id);  
		//search.addFilterEqual("deleted", false);  
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		int month = cal.get(Calendar.MONTH);
		System.out.println("Look at here=================================="+month);
		String thisMonth= getMonth(month+1);
		String nextMonth = getMonth(month+2);
		int year = cal.get(Calendar.YEAR);
		String str = thisMonth + " 01 " + year + " 00:00:00 GMT"; 
		String str1 = nextMonth + " 01 " + year + " 00:00:00 GMT"; 
   SimpleDateFormat df = new SimpleDateFormat("MMM dd yyyy HH:mm:ss zzz");
   Date date = df.parse(str);
   Date date1 = df.parse(str1);
   long epochThisMonth = date.getTime()/1000;
   long epochNextMonth = date1.getTime()/1000;
   System.out.println(epochThisMonth);
   System.out.println(epochNextMonth);
 	search.addFilterGreaterThan("dateSent", epochThisMonth);
 	search.addFilterLessThan("dateSent", epochNextMonth);
	
	return messageDao.count(search);
	}
	
	@Override
	public Integer countMessagesAsPerMonthIndividual(Integer id, Integer month, Integer y) throws ParseException {
		Search search = new Search(); 
		search.addFilterEqual("accountByFromId.id", id);  
		//search.addFilterEqual("deleted", false);  
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		
		String thisMonth= getMonth(month) ;
		String nextMonth = getMonth(month+1);
		
		String str = thisMonth + " 01 " + y + " 00:00:00 GMT"; 
		String str1 = nextMonth + " 01 " + y + " 00:00:00 GMT"; 
   SimpleDateFormat df = new SimpleDateFormat("MMM dd yyyy HH:mm:ss zzz");
   Date date = df.parse(str);
   Date date1 = df.parse(str1);
   long epochThisMonth = date.getTime()/1000;
   long epochNextMonth = date1.getTime()/1000;
   System.out.println(epochThisMonth);
   System.out.println(epochNextMonth);
 	search.addFilterGreaterThan("dateSent", epochThisMonth);
 	search.addFilterLessThan("dateSent", epochNextMonth);
	
	return messageDao.count(search);
	}
	
	@Override
	public Integer countMyNew(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("isRead", false); 
		search.addFilterEqual("accountByToId.id", id);  
		search.addFilterEqual("deleted", false);  
	return messageDao.count(search);
	}
	
	
	@Override
	public List<Message> pagedMessages(Integer initial, Integer max){
		Search search = new Search(); 
			search.addFilterEqual("deleted", false);  
			search.setFirstResult(initial);
			search.setMaxResults(max);
		return messageDao.search(search);
	}

	@Override
	public List<Message> getUnread(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("accountByToId.id", id);  
		search.addFilterEqual("deleted", false);  
		search.addFilterEqual("isRead", false);
		logger.debug("Sent Variables, id:"+ id);
	return messageDao.search(search);
	}
	
	@Override
	public List<Message> getRead(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("accountByToId.id", id);  
		search.addFilterEqual("deleted", false);
		search.addFilterEqual("isRead", true);
	return messageDao.search(search);
	}
	
	@Override
	public List<Message> getDeleted(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("accountByToId.id", id);  
		search.addFilterEqual("deleted", true);  
	return messageDao.search(search);
	}
	
	@Override
	public List<Message> getSent(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("accountByFromId.id", id);  
		search.addFilterEqual("deleted", false);  
	return messageDao.search(search);
	}

	@Override
	public List<Message> getByUserId(Integer id) {
		Search search = new Search(); 
		search.addFilterEqual("accountByToId.id", id);  
		search.addFilterEqual("deleted", false);  
		search.addSort("id", true);
	return messageDao.search(search);
	}
	

	
	
}

