package au.com.dmsq.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.dao.AuthDao;
import au.com.dmsq.model.Auth;
import au.com.dmsq.model.Role;
import au.com.dmsq.service.AuthService;

@Service
@Transactional
public class AuthServiceImpl implements AuthService {

	
	@Autowired 
	AuthDao authDao;
	
	@Override
	public List<Auth> findAll() {
		return authDao.findAll();
	}

	@Override
	public boolean save(Auth saveauth) {
		return authDao.save(saveauth);
	}

	@Override
	public boolean remove(Auth removeauth) {
		return authDao.remove(removeauth);
	}


	@Override
	public boolean deleteById(Integer id) {	
		Auth authx = get(id);
		remove(authx);
		return true;
	}
	
	@Override
    public Auth get(Integer id){
		return authDao.find(id);
	}

}