package au.com.dmsq.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sendgrid.*;
import javax.servlet.ServletContext;
import au.com.dmsq.dao.MailDao;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Mail;
import au.com.dmsq.service.ActivityService;
import au.com.dmsq.service.ActivityStreamService;
import au.com.dmsq.service.MailContentService;
import au.com.dmsq.service.MailService;
import au.com.dmsq.service.LayerService;
import au.com.dmsq.service.PushService;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


@Service

public class ActivityStreamImpl implements ActivityStreamService {
	
	@Override
	public String getActivities(String token)
	{
        URL url;
        HttpURLConnection connection = null;
        StringBuffer response = new StringBuffer();
        
        try {
        	
        	//Get auth header
        	// TODO code application logic here
           url = new URL("http://gryffindor.digitalmarketsquare.com/apiv2/api/?request=/api/activity/date/today");
           connection = (HttpURLConnection) url.openConnection();
           connection.setRequestMethod("GET");
           connection.setConnectTimeout(5000);
           connection.setRequestProperty("Auth-Token", token);
           connection.setUseCaches(false);
           connection.setDoInput(true);
           connection.setDoOutput(true);  
         //
           // Send request
           DataOutputStream wr = new DataOutputStream(
           connection.getOutputStream());
           wr.flush();
           wr.close();
           
           InputStream is = connection.getInputStream();
           BufferedReader rd = new BufferedReader(new InputStreamReader(is));
           String line;
         
           while ((line = rd.readLine()) != null) {
           response.append(line);
           response.append('\r');
           }
           System.out.println("Response :"+response.toString());
           rd.close();
           
           return response.toString();
       
    } catch (Exception ex) {
    	System.out.println(ex.toString());
    	response.toString();
    	return "Error";
    }
	}

	
	
}



