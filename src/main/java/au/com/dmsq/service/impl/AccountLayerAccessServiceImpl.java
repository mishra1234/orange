package au.com.dmsq.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import au.com.dmsq.dao.AccountLayerAccessDao;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.AccountLayerAccessId;
import au.com.dmsq.service.AccountLayerAccessService;

@Service
@Transactional
public class AccountLayerAccessServiceImpl implements AccountLayerAccessService{

	@Autowired AccountLayerAccessDao accountLayerAccessDao;
	
	@Override
	public boolean save(AccountLayerAccess accountLayerAccess) {
		return accountLayerAccessDao.save(accountLayerAccess);
	}

	@Override
	public List<AccountLayerAccess> getById(AccountLayerAccessId id) {
		Search search = new Search(); 
		search.equals(id);
		return accountLayerAccessDao.search(search);
	}

	@Override
	public List<AccountLayerAccess> findAll() {
		return accountLayerAccessDao.findAll();
	}
	
	public List<AccountLayerAccess> getCurrent()
	{
		return accountLayerAccessDao.getCurrent();
	}

	@Override
	public AccountLayerAccess getbyGrantToken(String grantToken) {
		// TODO Auto-generated method stub
		return accountLayerAccessDao.getbyGrantToken(grantToken);
	}

	@Override
	public AccountLayerAccess getbyDenyToken(String denyToken) {
		// TODO Auto-generated method stub
		return accountLayerAccessDao.getbyDenyToken(denyToken);
	}
	
}
