package au.com.dmsq.service;

import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.property.RRule;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.EventCalendarItem;

public interface EventCalendarItemService {
    
    public List<EventCalendarItem> findAll(); 

    boolean save(EventCalendarItem save);
  
    public EventCalendarItem get(Integer id);
	
	public String md5Java(String message);
	
	public Calendar getCalendarEvent(String myCalendarString) throws Exception;

	public Object[] findLast();

	public void toBaikal(String etag, String sDateTime, String eDateTime, String iCaldata, String href) throws Exception, IllegalAccessException, ClassNotFoundException;

	public EventCalendarItem editECI(EventCalendarItem eci);

	public void updateToBaikal(String etag, String sDateTime, String eDateTime, String iCaldata, String href) throws Exception, IllegalAccessException, ClassNotFoundException;

	public List<EventCalendarItem> createEventDates(Integer id, List<String> sDate,	List<String> sTime, List<String> eDate, List<String> eTime,
			String[] days, List<Integer> duration, String periodicity, Event event);

	public String calDataGenerator(DateTime timeStamp, String periodicity, RRule rrule, DateTime start, DateTime end, String evTitle);

	public void toECI(List<EventCalendarItem> ecis, Event newEvent) throws Exception, IllegalAccessException, ClassNotFoundException;

	public void updateECI(List<EventCalendarItem> ecis, Event newEvent) throws Exception, IllegalAccessException, ClassNotFoundException;

	public List<EventCalendarItem> getECIByActId(Event event);

	public List<EventCalendarItem> readEventDates(List<String> sDate,List<String> sTime, List<String> eDate, List<String> eTime,
			String[] days, List<Integer> duration, String periodicity, Event oldEvent);

	public List<EventCalendarItem> createEventDatesBulk(Integer id, String sDate, String sTime, String eDate, String eTime, Event oldEvent);

	public EventCalendarItem getLastECIbyActid(Integer eventId);

	public Set<EventCalendarItem> readEventDatesEdited(Event oldEvent,List<EventCalendarItem> ecisNew);

	public EventCalendarItem removeByHrefAndID(Activity activity, String eciHref, Integer activityId);

	public void deleteToBaikal(String href, String etag) throws Exception,IllegalAccessException, ClassNotFoundException;

	//EventCalendarItem findById(Integer id, String href);
	
}
