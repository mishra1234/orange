package au.com.dmsq.service;

import java.util.List;
import java.util.Set;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.Category;
import au.com.dmsq.model.IconDTO;
import au.com.dmsq.model.Layer;

public interface LayerService {
	
	/*Category get (Long id);
	
	boolean save(Category category);
	
    boolean removeById (Integer id);*/
    
    public List<Layer> findAll(); 
    
    public void remove(Layer remove);

    public boolean save(Layer save);
  
    public Layer get(Integer id);
    
    public List<Layer> getNonDeleted();
    
    public List<Layer> getByOwnerId(Integer id);

    public Layer findById (Integer id);

    public boolean deleteById(Integer id);

	public List<Layer> getByCategory(Integer id);
    
	//public List<IconDTO> listIcons() throws Exception;

	public int count();
	
	public int countByOwner(Integer id);

	public int countActivities(String id);

	public Layer findByIdWithActivities(Integer id);
	
	public Layer findByIdWithRelations(Integer id);

	public Boolean testIfManager(Integer accountId);

	public List<Layer> layersIfManager(Integer accountId);

	public List<Layer> pagedCommunities(Integer initial, Integer max, Integer id);
	
	public List<Layer> getregionallayers();
	
	public Layer getLayerWithManagers(Integer id);

	public Layer findByIdWithAccounts(Integer id);

	public List<Layer> getStatusLayers();

	public Layer findByIdWithActivitiesAll(Integer id);

	public Layer findLayerWithActivitiesAndAccess(Integer id);
	
	public Layer findwithBanners(Integer id);
	
	public Set<Banner> getBanners(Set<Layer> layers);

	public Layer findByIdWithActivityType(String type, Integer id);

	public Layer findLayerWithActivitiesAndType(Integer layerId, String type);

	public Layer findByIdWithParentChildCategories(Integer id);

	public Layer findByIdWithRequiredRelations(Integer id);

	
	//public void s3Sample() throws IOException;
	
    //TODO: GetParent GetChilds GetNotDeleted
    

	
}
