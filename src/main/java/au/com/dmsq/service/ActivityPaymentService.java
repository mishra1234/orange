package au.com.dmsq.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.ActivityPayment;

	
	public interface ActivityPaymentService {
		 boolean save(ActivityPayment activityPayment);
		 public List<ActivityPayment> getAllPaid(Account account);
		}
