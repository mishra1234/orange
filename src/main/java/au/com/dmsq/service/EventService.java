package au.com.dmsq.service;

import java.util.List;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.EventCalendarItem;

public interface EventService {
    
    public List<Event> findAll(); 
    
    boolean remove(Event remove);

    boolean save(Event save);
  
    public Event get(Integer id);
   
    boolean removeById(Event event ,Integer id);

	public Event findById(Integer id);

	Event findByIdWithRelations(Integer id);
	
}
