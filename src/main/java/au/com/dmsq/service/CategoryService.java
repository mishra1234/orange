package au.com.dmsq.service;

import java.util.List;

import au.com.dmsq.model.Category;

public interface CategoryService {
	
	Category findById (Integer id);
	
    boolean removeById (Integer id);
    
    List<Category> findAll(); 

    boolean save(Category category);
    
    List<Category> getNonDeleted();

	List<Category> getDeleted();

	int countSubcategories(Integer id);
	
	int countConnections(String id);

	int countActivities(String id);

	int countCommunities(String id);

	Category getCategoryWithCommunity(String id);
    
	
}
