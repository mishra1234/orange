package au.com.dmsq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.model.Auth;
import au.com.dmsq.model.Role;

@Service
@Transactional
public interface AuthService {
	
	/*Category get (Long id);
	
	boolean save(Category category);
	
    boolean removeById (Integer id);*/
    
    public List<Auth> findAll(); 
    
    boolean remove(Auth remove);
    
    public boolean save(Auth save);
    
    public boolean deleteById(Integer id);
    
    public Auth get(Integer id);
    
    //TODO: GetParent GetChilds GetNotDeleted
    

	
}
