package au.com.dmsq.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONString;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.UserDTO;

public interface AccountService {
	
	public boolean save(Account account);
	public Account get(Integer id);
	public boolean deleteById(Integer id);
	public List<Account> findAll();
	public void remove(Account account);
	public List<Account> getNonDeleted();
	public List<Account> getDeleted();
	public Account findById(Integer id);
	public List <Account> findByEmail(String email);
	public boolean emailExist(String email);
	public boolean registerNewUserAccount(UserDTO accountDto) throws Exception;
	public Integer count();
	public List<Account> pagedAccounts(Integer initial, Integer max);
	public Account getByResetCode(String token);
	public Account getbyIdWithLayers(Integer id);
	//public Account getbyIdWithLayersManaged(Integer id);
	public Account getbyIdWithRelations(Integer id);
	public Account getWithLayers(Integer id);
	public Account getbyIdWithLayersFaster(Integer id);
	public Account findAcctByEmail(String email);
	public Account getbyIdWithAccConfirmations(Integer id);
	public String getConnections(Account account) throws IllegalAccessException, ClassNotFoundException, Exception;
	public Integer totalConnections(Account account) throws IllegalAccessException,	ClassNotFoundException, Exception;
	public String getConnectionOfParticularCommunity(Integer layerId) throws IllegalAccessException, ClassNotFoundException, Exception;
	public Integer thisMonthConnections(Account account) throws IllegalAccessException,	ClassNotFoundException, Exception;
	public Integer particularMonthConnections(Account account, Integer whichMonth, Integer whichYear)
			throws IllegalAccessException, ClassNotFoundException, Exception;
	public Integer countAcctsGated(Integer layerId);
	public List<Account> pagedAccountsByLayerGated(Integer initial, Integer max, Integer id, Integer layerId);
	public Account getbyIdWithRelationsFaster(Integer id);
	public List<Account> pagedAccountsByLayer(Integer initial, Integer max,	Integer id, Integer layerId);
	public Integer countAccts(Integer layerId);
	public Account getALAsbyId(Integer id);
	public Account getLayersbyId(Integer id);
	public Account getLayerManagersById(Integer id);
	public Account getBookingsById(Integer id);
	public Account getAccountLayerAccessesById(Integer id);
	public Account getActivitiesById(Integer id);
	public Account getAuthsById(Integer id);
	public Account getConnectedLayersById(Integer id);
	public Account getbyIdWithLayersManaged(Integer id);
	
//	public Integer particularMonthConnections(Account account, String n)
//			throws IllegalAccessException, ClassNotFoundException, Exception;
//	
}
