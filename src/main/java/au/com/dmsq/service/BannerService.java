package au.com.dmsq.service;

import java.util.List;

import au.com.dmsq.model.Banner;
import au.com.dmsq.model.IconDTO;

public interface BannerService {
	
   
    public List<Banner> findAll(); 
    
    public boolean remove(Banner remove);

    public boolean save(Banner save);
  
    public Banner get(Integer id);
   
    public boolean removeById(Banner layer ,Integer id);

	public Banner findById(Integer id);

	public boolean deleteById(Integer id);

	public List<IconDTO> listImages() throws Exception;
	
	public List<Banner> pagedBanners(int initial, int max);

	public int count();
    
    //TODO: GetParent GetChilds GetNotDeleted
    	
}
