package au.com.dmsq.service;

import java.util.List;

import au.com.dmsq.model.Mail;

public interface MailService {

	public void welcomeMail(String from, String to, String subject, String text);

	public boolean save(Mail mail);

	public void mailToSome(String fromname, String from, String to, String subject, String text, String[] bcc);

	public void mailToOne(String fromname, String from, String to, String subject, String text);

	public void promote(Integer communityId, Integer activityId, String Type);

	public void resetPasswordMail(String from, String to, String subject, String text);
	
	public void accessRequestManagerEmail(String from, String to, String subject, String text);
	
	public void accessRequestMemberEmail(String from, String to, String subject, String text);
	
	public void accessGrantedEmail(String from, String to, String subject, String text);
	
	public void inviteEmail(String from, String to, String subject, String text);
	
	public void connectedCommunityConfirmation(String from, String to, String subject, String text);
	
	public void subscriptionEmails(String from, String to, String subject, String text);

}
