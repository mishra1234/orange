package au.com.dmsq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.model.ActivityCalendar;

@Service
@Transactional
public interface ActivityCalendarService {
	
	public boolean save(ActivityCalendar activityCalendar);
	public ActivityCalendar get(Integer id);
	public boolean deleteById(Integer id);
	public List<ActivityCalendar> findAll();
	public void remove(ActivityCalendar activityCalendar);
	public List<ActivityCalendar> getCurrent();
	public List<ActivityCalendar> getScehdulesByActivityId(Integer activityId);
}
