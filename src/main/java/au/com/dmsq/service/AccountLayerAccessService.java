package au.com.dmsq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.model.AccountLayerAccess;
import au.com.dmsq.model.AccountLayerAccessId;

@Service
@Transactional
public interface AccountLayerAccessService {
	
	public boolean save(AccountLayerAccess accountLayerAccess);
	public List<AccountLayerAccess> getById(AccountLayerAccessId accountLayerAccessId);
	public List<AccountLayerAccess> findAll();
	public List<AccountLayerAccess> getCurrent();
	public AccountLayerAccess getbyGrantToken(String grantToken);
	public AccountLayerAccess getbyDenyToken(String denyToken);
}
