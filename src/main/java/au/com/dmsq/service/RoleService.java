package au.com.dmsq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Role;

@Service
@Transactional
public interface RoleService {

	public boolean save(Role role);
	public Role get(Integer id);
	public boolean deleteById(Integer id);
	public List<Role> findAll();
	public void remove(Role role);

	
}
