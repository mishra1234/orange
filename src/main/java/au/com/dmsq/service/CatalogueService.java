package au.com.dmsq.service;

import java.util.List;
import au.com.dmsq.model.Catalogue;


public interface CatalogueService {
	public boolean save(Catalogue catalogue);
	public List<Catalogue> findAllItems();
	
}
