package au.com.dmsq.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Banner;
import au.com.dmsq.model.EventDTO;
import au.com.dmsq.model.File;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.RecommendedActivities;

@Service
@Transactional
public interface ActivityService {
	
	public boolean save(Activity activity);
	public Activity get(Integer id);
	public boolean deleteById(Integer id);
	public List<Activity> findAll();
	public void remove(Activity activity);
	public List<Activity> getNonDeleted();
	public List<Activity> getEvtsNonDeleted();
	public List<Activity> getByOwnerId(Integer id);
	public Activity findById(Integer id);
	public Integer countEv();
	public List<Activity> pagedActivities(Integer initial, Integer max, Integer id);
	public Activity findByIdWithRelations(Integer id);
	
	public List<Activity> surveyActivity(Integer intial, Integer max, Integer id);
	public Integer countEvByOwner(Integer id);
	public Integer countInf(Integer id);
	public Integer countSurvey(Integer id);
	public Set<Activity> getEventsByCommIds(List<Integer> ids);
	public Activity findByIdWithBanners(Integer id);
	public List<Activity> getStartandEndDatesfromEvents(List<Activity> activities) throws ParseException;
	public Set<Activity> getEventsByCommId(Integer id);
	public List<Activity> getInfsNonDeleted();
	public Set<Activity> getInfosByCommId(Integer id);
	public Set<Activity> getGatedEventsByCommId(List<Integer> comms);
	public List<EventDTO> generateEventDTOs(List<Activity> activities) throws ParseException;
	public List<Activity> getStartandEndDTfromEvent(List<Activity> activities)throws ParseException;
	public Set<Activity> getInfosByCommIds(List<Integer> ids);
	public Activity getStartandEndDatesfromEvent(Activity activity) throws ParseException;
	public Set<String> getIcons(Set<Layer> layers);
	public List<Activity> getEventsByOwnerId(Integer id);

	public List<Activity> findByLayerId(Integer id);

	public Activity findwithBanners(Integer id);
	public Activity findwithLayers(Integer id);
	public Activity findwithFiles(Integer id);
	public Set<Banner> getBanners(Set<Layer> layers);
	public Set<Activity> getIconsInfos(Set<Layer> layers);
	public Set<File> getEvVideos(Set<Layer> layers);
	public Set<Activity> getIconsB(Set<Layer> layers);
	public Set<Activity> getIconsInfosB(Set<Layer> layers);
	public Integer calculateDuration(String string, String eDate);
	public Set<File> getEvFiles(Set<Layer> layers);
	public List<Activity> pagedActs(Integer initial, Integer max, String type, Long dtend, Layer layer);
	public List<Activity> pagedActsPast(Integer initial, Integer max, String type, Long dtend, Layer layer);
	// public Integer pagedActsPages(String type, Long dtend, Layer layer) throws Exception, IllegalAccessException, ClassNotFoundException;
	// public Integer pagedActsPagesPast(String type, Long dtend, Layer layer);
	public List<Activity> getStartandEndDatesfromEventsPast(List<Activity> activities) throws ParseException;
	public List<RecommendedActivities> findRecommendedActivitiesfromActs(Set<Activity> events);
	public Integer calculateDurationExtended(String startDate, String endDate, String startTime, String endTime);
	
	//public List<Activity> pagedInfos(Integer initial, Integer max, Integer id);
	public List<Activity> pagedInfosByLayer(Integer initial, Integer max, Integer id, Integer layerId);
	public List<Activity> pagedSurveysByLayer(Integer initial, Integer max,Integer id, Integer layerId);
	public Integer pagedActsPages(String type, Long dtend, Integer layerId);
	public Integer pagedActsPagesPast(String type, Long dtend, Integer layerId);
	

}
