package au.com.dmsq.service;

import au.com.dmsq.model.Account;

public interface MailContentService {

	public String readConfirmationEmail ();

	public String readConfirmationEmailLower();

	public String readPromoEventEmail();

	public String readPromoInfoEmail();

	public String readSGUser();

	public String readSGPass();

	public String readDBBkConn();

	public String readDBBkUser();

	public String readDBBkPass();

	public String readWSConn();

	public String readDBPass();

	public String readDBConn();

	public String readDBUser(); 
	
	public String readForgotPassword();
	
	public String readAccessRequestEmail();
	
	public String readAccessRequestMemberEmail();
	
	public String readAccessGrantedEmail();
	
	public String readInviteEmail();
	
	public String readConnectedComConfirm();
	
	public String readSubsEmail();

	public String readNewEmailConfirmationEmail();

	public String readPaymentConfirmationEmail();

	public String insertUnsubscribe(Account acct, String body);
}
