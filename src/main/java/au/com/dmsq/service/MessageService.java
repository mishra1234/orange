package au.com.dmsq.service;

import java.text.ParseException;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import au.com.dmsq.model.Account;
import au.com.dmsq.model.Message;

@Service
@Transactional
public interface MessageService {
	
	public boolean save(Message message);
	public Message get(Integer id);
	public boolean deleteById(Integer id);
	public List<Message> findAll();
	public void remove(Message message);
	public List<Message> getNonDeleted();
	public Message findById(Integer id);
	public Integer count();
	public List<Message> pagedMessages(Integer initial, Integer max);
	public List<Message> getUnread(Integer id);
	List<Message> getRead(Integer id);
	List<Message> getDeleted(Integer id);
	List<Message> getSent(Integer id);
	Integer countMyNew(Integer id);
	List<Message> getByUserId(Integer id);
	public Integer countIndividual(Integer id) throws ParseException;
	public Integer countMessagesAsPerMonthIndividual(Integer id, Integer month, Integer y)
			throws ParseException;
	
}
