package au.com.dmsq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.model.File;

@Service
@Transactional
public interface FileService {
	
	public boolean save(File file);
	public boolean deleteById(Integer id);
	public List<File> findAll();
	public void remove(File file);
	public File findById(Integer id);

}
