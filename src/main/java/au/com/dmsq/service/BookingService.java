package au.com.dmsq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.model.Booking;

@Service
@Transactional
public interface BookingService {

	public boolean save(Booking booking);

	public Booking getbyId(Integer id);

	public boolean deleteById(Integer id);

	public void remove(Booking booking);

	public List<Booking> getBookingsByActivityId(Integer activityId);
	


}
