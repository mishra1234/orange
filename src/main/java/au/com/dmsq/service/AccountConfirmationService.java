package au.com.dmsq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.AccountConfirmation;
import au.com.dmsq.model.UserDTO;

public interface AccountConfirmationService {

	public boolean save(AccountConfirmation ac);

	public AccountConfirmation get(Integer id);
	public AccountConfirmation findById(Integer id);

	public boolean deleteById(Integer id);

	public void remove(AccountConfirmation ac);

	public List<AccountConfirmation> findAll();

	public List<AccountConfirmation> getNonConfirmed();

	public List<AccountConfirmation> getConfirmed();

	public AccountConfirmation getByAccount(AccountConfirmation accountC);

	
	

}
