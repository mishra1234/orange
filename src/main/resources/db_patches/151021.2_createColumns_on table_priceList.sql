ALTER TABLE `ccme_winter_2`.`price_list` 
ADD COLUMN `item_name` VARCHAR(255) NULL COMMENT '' AFTER `id`,
ADD COLUMN `price` INT(11) NULL COMMENT '' AFTER `item_name`,
ADD COLUMN `per_amount` INT(11) NULL COMMENT '' AFTER `price`,
ADD COLUMN `date_created` DATETIME NULL DEFAULT '2014-01-01 09:21:39' COMMENT '' AFTER `per_amount`;