CREATE TABLE `price_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL DEFAULT '2014-01-01 09:21:39',
  `concept` varchar(255) NOT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;