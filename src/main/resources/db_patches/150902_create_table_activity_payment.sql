CREATE TABLE `activity_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `date` DATETIME NULL DEFAULT '2015-01-01 09:21:39' COMMENT '',
  `amount` int(11) NULL,
  `response` text NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activity_id5_idx` (`activity_id`),
  KEY `fk_account_id6_idx` (`account_id`),
  CONSTRAINT `fk_account_id6` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_activity_id5` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;