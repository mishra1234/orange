/*
Name: 			Forms / Advanced - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.3.0
*/

(function($) {
	
	 $('#addMore').click( function() {
		 var html = '<div id="event[0]"> <div class="panel-body"><div class="form-group">';
		     html += '<label class="col-sm-4 control-label">Type of Event</label><div class="col-sm-8">';
		     html += '<div class="radio-custom radio-primary">';
		     html += '<input type="radio" id="radioSingle" name="radioExample">';
		     html += '<label for="radioExample1">Single Date</label>';
		     html += '</div>';
		     html += 'div class="radio-custom radio-primary">';
		     html += '<input type="radio" id="radioPattern" name="radioExample">';
		     html += '					<label for="radioExample2">Pattern</label>';
		     html += '				</div>';
		     html += '			</div>';
		     html += '		</div>';
			 html += '</div>';
			 html += '';
			 html += '	<div id="hideSingle" style="display: none">';
			 html += '		<div class="panel-body">';
			 html += '			<div class="form-group">';
			 html += '				<label class="col-md-3 control-label">Date</label>';
			 html += '				<div class="col-md-6">';
			 html += '					<div class="input-group">';
			 html += '						<span class="input-group-addon"> <i class="fa fa-calendar"></i>';
			 html += '						</span> <input id="date" data-plugin-masked-input';
			 html += '							data-input-mask="99/99/9999" placeholder="__/__/____"';
			 html += '							class="form-control">';
			 html += '					</div>';
			 html += '				</div>';
			 html += '			</div>';
			 html += '		</div>';
			 html += '	</div>';
			 html += '';
			 html += '	<div id="hidePattern" style="display: none">';
			 html += '		<div class="panel-body">';
			 html += '			<div class="form-group">';
			 html += '				<label class="col-md-3 control-label">Date range</label>';
			 html += '				<div class="col-md-6">';
			 html += '					<div class="input-daterange input-group"';
			 html += '						data-plugin-datepicker>';
			 html += '						<span class="input-group-addon"> <i';
			 html += '							class="fa fa-calendar"></i>';
			 html += '						</span> <input type="text" class="form-control" name="start">';
			 html += '						<span class="input-group-addon">to</span> <input type="text"';
			 html += '							class="form-control" name="end">';
			 html += '					</div>';
			 html += '				</div>';
			 html += '			</div>';
			 html += '		</div>';
			 html += '';
			 html += '	<div class="panel-body">';
			 html += '		<div id="days" class="control-group">';
			 html += '			<div class="col-md-6">';
			 html += '				<div class="form-group">';
			 html += '					<label class="col-md-3 control-label">Week Days of event</label>';
			 html += '					<div class="controls">';
			 html += '						<c:choose>';
			 html += '							<c:when test="${monday==true}">';
			 html += '								<label for="MO"> Monday <input type="checkbox" name="days" value="MO" id="days" checked="checked" /></label>';
			 html += '							</c:when>';
			 html += '							<c:otherwise>';
			 html += '								<label for="MO"> Monday <input type="checkbox" name="days" value="MO" id="days" /></label>';
			 html += '							</c:otherwise>';
			 html += '						</c:choose>';
			 html += '						<c:choose>';
			 html += '							<c:when test="${tuesday==true}">';
			 html += '								<label for="TU">Tuesday <input type="checkbox" name="days" value="TU" id="days" checked="checked" /></label>';
			 html += '							</c:when>';
			 html += '							<c:otherwise>';
			 html += '								<label for="TU">Tuesday <input type="checkbox" name="days" value="TU" id="days" /></label>';
			 html += '							</c:otherwise>';
			 html += '						</c:choose>';
			 html += '						<c:choose>';
			 html += '							<c:when test="${wednesday==true}">';
			 html += '								<label for="WE">Wednesday <input type="checkbox" name="days" value="WE" id="days" checked="checked" /></label>';
			 html += '							</c:when>';
			 html += '							<c:otherwise>';
			 html += '								<label for="WE">Wednesday <input type="checkbox" name="days" value="WE" id="days" /></label>';
			 html += '							</c:otherwise>';
			 html += '						</c:choose>';
			 html += '						<c:choose>';
			 html += '							<c:when test="${thursday==true}">';
			 html += '								<label for="TH">Thursday <input type="checkbox" name="days" value="TH" id="days" checked="checked" /></label>';
			 html += '							</c:when>';
			 html += '							<c:otherwise>';
			 html += '								<label for="TH">Thursday <input type="checkbox" name="days" value="TH" id="days" /></label>';
			 html += '							</c:otherwise>';
			 html += '						</c:choose>';
			 html += '						<c:choose>';
			 html += '							<c:when test="${friday==true}">';
			 html += '								<label for="FR">Friday <input type="checkbox" name="days" value="FR" id="days" checked="checked" /></label>';
			 html += '							</c:when>';
			 html += '							<c:otherwise>';
			 html += '								<label for="FR">Friday <input type="checkbox" name="days" value="FR" id="days" /></label>';
			 html += '							</c:otherwise>';
			 html += '						</c:choose>';
			 html += '						<c:choose>';
			 html += '							<c:when test="${saturday==true}">';
			 html += '								<label for="SA">Saturday <input type="checkbox" name="days" value="SA" id="days" checked="checked" /></label>';
			 html += '							</c:when>';
			 html += '							<c:otherwise>';
			 html += '								<label for="SA">Saturday <input type="checkbox" name="days" value="SA" id="days" /></label>';
			 html += '							</c:otherwise>';
			 html += '						</c:choose>';
			 html += '						<c:choose>';
			 html += '							<c:when test="${sunday==true}">';
			 html += '								<label for="SU">Sunday <input type="checkbox" name="days" value="SU" id="days" checked="checked" /></label>';
			 html += '							</c:when>';
			 html += '							<c:otherwise>';
			 html += '								<label for="SU">Sunday <input type="checkbox" name="days" value="SU" id="days" /></label>';
			 html += '							</c:otherwise>';
			 html += '						</c:choose>';
			 html += '					</div>';
			 html += '				</div>';
			 html += '			</div>';
			 html += '		</div>';
			 html += '	</div>';
			 html += '</div>';
			 html += '';					
			 html += '<div class="panel-body">';
			 html += '	<div class="form-group">';
			 html += '		<label class="col-md-3 control-label">Starts at</label>';
			 html += '		<div class="col-md-6">';
			 html += '			<div class="input-group">';
			 html += '				<span class="input-group-addon"> <i class="fa fa-clock-o"></i>';
			 html += '				</span> <input type="text" data-plugin-timepicker class="form-control"'; 
			 html += '>';
			 html += '			</div>';
			 html += '		</div>';
			 html += '	</div>';
			 html += '</div>';
			 html += '';
			 html += '<div class="panel-body">';
			 html += '	<div class="form-group">';
			 html += '		<label class="col-md-3 control-label">Duration</label>';
			 html += '		<div class="col-md-6">';
			 html += '			<div class="input-group">';
			 html += '				<span class="input-group-addon"> <i class="fa fa-clock-o"></i></span> <input id="duration"';
			 html += '					name="duration" type="text" placeholder="duration"';
			 html += '					class="input-xlarge" value="60" /> (Minutes)';
			 html += '			</div>';
			 html += '		</div>';
			 html += '	</div>';
			 html += '</div>';
			 html += '';
			 html += '<div class="panel-body">';
			 html += '	<div class="form-group">';
			 html += '		<label class="col-md-3 control-label">Ends at</label>';
			 html += '		<div class="col-md-6">';
			 html += '			<div class="input-group">';
			 html += '				<span class="input-group-addon">'; 
			 html += '					<i class="fa fa-clock-o"></i>';
			 html += '				</span> <input type="text" data-plugin-timepicker'; 
			 html += '							class="form-control" >';
			 html += '			</div>';
			 html += '		</div>';
			 html += '	</div>';
			 html += '</div>';
			 html += '</div>';
	 	
	 });
	
	
	
	 $('#radioSingle').click( function() {
		 $('#hideSingle').slideDown();
		 $('#hidePattern').hide();
		
	 });
	 
	 $('#radioPattern').click( function() {
		 $('#hidePattern').slideDown();
		 $('#hideSingle').hide();
		
	 });


	 $('#timepicker4').timepicker();

	/*
	Multi Select: Toggle All Button
	*/
	function multiselect_selected($el) {
		var ret = true;
		$('option', $el).each(function(element) {
			if (!!!$(this).prop('selected')) {
				ret = false;
			}
		});
		return ret;
	}

	function multiselect_selectAll($el) {
		$('option', $el).each(function(element) {
			$el.multiselect('select', $(this).val());
		});
	}

	function multiselect_deselectAll($el) {
		$('option', $el).each(function(element) {
			$el.multiselect('deselect', $(this).val());
		});
	}

	function multiselect_toggle($el, $btn) {
		if (multiselect_selected($el)) {
			multiselect_deselectAll($el);
			$btn.text("Select All");
		}
		else {
			multiselect_selectAll($el);
			$btn.text("Deselect All");
		}
	}

	$("#ms_example7-toggle").click(function(e) {
		e.preventDefault();
		multiselect_toggle($("#ms_example7"), $(this));
	});

	/*
	Slider Range: Output Values
	*/
	$('#listenSlider').change(function() {
		$('.output b').text( this.value );
	});

	$('#listenSlider2').change(function() {
		var min = parseInt(this.value.split('/')[0], 10);
		var max = parseInt(this.value.split('/')[1], 10);

		$('.output2 b.min').text( min );
		$('.output2 b.max').text( max );
	});

}(jQuery));