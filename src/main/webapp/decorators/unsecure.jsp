<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>

<html>
<head>

<!--[if IE]>
			<link rel="stylesheet" href="<c:url value='/ruralAssets/css/ie.css'/>">
		<![endif]-->

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!--[if lte IE 8]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<c:url value='/ruralAssets/vendor/respond/respond.js'/>"></script>
			<script src="<c:url value='/ruralAssets/vendor/excanvas/excanvas.js'/>"></script>
		<![endif]-->

<!-- Basic -->
<meta charset="utf-8">
<title>Rural Connect</title>
<meta name="keywords" content="Rural Connect South Australia" />
<meta name="description" content="Rural Connect South Australia">
<meta name="author" content="Digital Market Square">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Web Fonts  -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/fontawesome/css/font-awesome.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/owlcarousel/owl.carousel.min.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/owlcarousel/owl.theme.default.min.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/magnific-popup/magnific-popup.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/scripts/jquery-ui-1.11.2.custom/jquery-ui.min.css'/>"> 
<!-- Theme CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-elements.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-blog.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-shop.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-animate.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/widgets.css'/>">

<!-- Current Page CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/rs-plugin/css/settings.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/circle-flip-slideshow/css/component.css'/>" media="screen">


<!--Code for tracking  -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46391228-5', 'auto');
  ga('send', 'pageview');

</script>

<!-- Skin CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/skins/default.css'/>">

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/custom.css'/>">

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="<c:url value='assets/stylesheets/theme-custom.css'/>">

<!-- multi select --> 
	<link rel="stylesheet" href="<c:url value="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>">
		
		<link rel="stylesheet" href="<c:url value="/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>">
		<link rel="stylesheet" href="<c:url value="/assets/vendor/select2/select2.css"/>">
			
		<script src="<c:url value="/assets/vendor/modernizr/modernizr.js"/>"></script>
		<script src="<c:url value="/assets/vendor/jquery/jquery.js"/>"></script>
		<script src="<c:url value="/assets/vendor/nanoscroller/nanoscroller.js"/>"></script>
		<!--  <script src="<c:url value="/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"/>"></script>-->
		<script src="<c:url value="/assets/vendor/select2/select2.js"/>"></script>
		<!--  -->
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
					
		
		<script src="<c:url value="/assets/javascripts/theme.init.js"/>"></script>



</head>
<body>
	<div class="body">

		<div id="headerContent">
			<header id="header">
				<div class="container">
					<div class="logo2">
						<a href="<c:url value='/start'/>"><img alt="Rural Connect" width="146" height="50" data-sticky-width="135" data-sticky-height="45" src="<c:url value='/ruralAssets/img/logo.png'/>">
						</a>
					</div>

				
					<!--
                        <nav>
                            <ul class="nav nav-pills nav-top">
                                <li>
                                    <span><i class="fa fa-envelope-o"></i><strong>(4) </strong>Messages</span>
                                </li>
                            </ul>
                        </nav>-->
					<button class="btn btn-responsive-nav btn-inverse"
						data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container" id="lower">
						<nav class="nav-main mega-menu">
							<ul class="nav nav-pills nav-main" id="mainMenu">

								<li><a href="<c:url value='/about'/>">About CC-Me</a></li>
								<!-- Tabs area
								<li id="grainOption"><a href="<c:url value='/grain'/>">GRAIN
										<br> PRODUCERS SA
								</a></li>
								
								<li id="livestockOption"><a
									href="<c:url value='/livestock'/>">LIVESTOCK SA</a></li>

								<li id="horticultureOption"><a
									href="<c:url value='/horticulture'/>">HORTICULTURE</br>
										COALITION of SA
								</a></li>
								
								
								<li><a class="dropdown-toggle" href="http://wgcsa.com.au/"
									target="_blank"> WINE GRAPE COUNCIL OF</br> South Australia

								</a></li>

								<li id="dairyfarmersOption"><a class="dropdown-toggle"
									href="<c:url value='/dairyfarmers'/>"> SA DAIRYFARMERS<BR>
										ASSOCIATION
								</a></li>
								<li id="porkOption"><a href="<c:url value='/pork'/>">PORK
										SA</a></li>
                                  -->
								<!-- Sign in-->

								<li class="dropdown mega-menu-item mega-menu-signin signin"
									id="headerAccount"><a class="dropdown-toggle" href="">
										<i class="fa fa-user"></i> Sign In <i class="fa fa-angle-down"></i>
								</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-12">

														<div class="signin-form">

															<span class="mega-menu-sub-title">Sign In</span>

															<form id="form"
																action="<c:url value='/j_spring_security_check'/>"
																method="post">
																<div class="row">
																	<div class="form-group">
																		<div class="col-md-12">
																			<label>E-mail Address</label> <input type="text"
																				name="username" value=""
																				class="form-control input-lg">
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="form-group">
																		<div class="col-md-12">
																			<a class="pull-right" id="headerRecover" href="#">(Lost
																				Password?)</a> <label>Password</label> <input
																				type="password" value="" name="password"
																				class="form-control input-lg">
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-6">
																		<span class="remember-box checkbox"> <label
																			for="rememberme"> <input type="checkbox"
																				id="remember-me" name="remember-me">Remember
																				Me
																		</label>
																		</span>
																	</div>
																	<div class="col-md-6">
																		<input type="submit" value="Login"
																			class="btn btn-primary pull-right push-bottom"
																			data-loading-text="Loading...">
																	</div>
																</div>
															</form>

															<p class="sign-up-info">
																Don't have an account yet? <a href="<c:url value='/register'/>">Sign
																	Up</a>
															</p>

														</div>

														<div class="signup-form">
															<span class="mega-menu-sub-title">Create Account</span>

															<form action="" id="" method="post">
																<div class="row">
																	<div class="form-group">
																		<div class="col-md-12">
																			<label>E-mail Address</label> <input type="text"
																				value="" class="form-control input-lg">
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="form-group">
																		<div class="col-md-6">
																			<label>Password</label> <input type="password"
																				value="" class="form-control input-lg">
																		</div>
																		<div class="col-md-6">
																			<label>Re-enter Password</label> <input
																				type="password" value=""
																				class="form-control input-lg">
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-12">
																		<input type="submit" value="Create Account"
																			class="btn btn-primary pull-right push-bottom"
																			data-loading-text="Loading...">
																	</div>
																</div>
															</form>

															<p class="log-in-info">
																Already have an account? <a href="#" id="headerSignIn">Log
																	In</a>
															</p>
														</div>

														<div class="recover-form">
														
															<span class="mega-menu-sub-title">Reset My
																Password</span>
															<p>Complete the form below to receive an email with
																the authorization code needed to reset your password.</p>

																<form action="<c:url value='/action/reset'/>" method="post">
																<div class="row">
																	<div class="form-group">
																		<div class="col-md-12">
																			<label>E-mail Address</label> <input type="text" name="email"
																				value="" class="form-control input-lg" required="required">
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-12">
																		<input type="submit" value="Submit"
																			class="btn btn-primary pull-right push-bottom"
																			data-loading-text="Loading...">
																	</div>
																</div>
															</form>

															<p class="log-in-info">
																Already have an account? <a href="#"
																	id="headerRecoverCancel">Log In</a>
															</p>
														</div>

													</div>
												</div>
											</div>
										</li>

										<!-- End Sign in -->
									</ul>
							</ul>
						</nav>


					</div>
				</div>
			</header>
		</div>

		<div id="content">
			<decorator:body />
		</div>

		<!-- End: page -->
		<footer id="footer" class="light">
			<div class="container">
				<div class="row">
					<div class="footer-ribbon">
						<span>Keep Connected</span>
					</div>
					
					<div class="col-md-5">
						<h4>Mobile Version</h4>
						<div class="left">
							<p>
								<img src="<c:url value='/ruralAssets/img/placements/comingsoon.png'/>" height="100" />
							</p>
						</div>
					</div>
					<div class="col-md-5">
						<div class="contact-details">
							<h4>Contact Us</h4>
							<ul class="contact">
								<li><p>
										<i class="fa fa-map-marker"></i> <strong>Address:</strong>
										Unit 4, 780 South Road, Glandore SA 5037
									</p></li>
								<li><p>
										<i class="fa fa-phone"></i> <strong>Phone:</strong> +61  (08) 8297 0899
									</p></li>
								<li><p>
										<i class="fa fa-envelope"></i> <strong>Email:</strong> 
											<a href="mailto:admin@ppsa.org.au">admin@ppsa.org.au</a>
									</p></li>
							</ul>
						</div>
					</div>
					<!--  
					<div class="col-md-2">
						<h4>Follow Us</h4>
						<div class="social-icons">
							<ul class="social-icons">
								<li class="facebook"><a href="http://www.facebook.com/"
									target="_blank" data-placement="bottom" data-tooltip
									title="Facebook">Facebook</a></li>
								<li class="twitter"><a href="http://www.twitter.com/"
									target="_blank" data-placement="bottom" data-tooltip
									title="Twitter">Twitter</a></li>
								<li class="linkedin"><a href="http://www.linkedin.com/"
									target="_blank" data-placement="bottom" data-tooltip
									title="Linkedin">Linkedin</a></li>
							</ul>
						</div>
					</div>-->
				</div>
			</div>
			<!-- 
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-1">
							<a href="index.html" class="logo"> <img alt="Rural Connect"
								class="img-responsive" src="<c:url value='/ruralAssets/img/logo-footer.png'/>"/>
							</a>
						</div>
						<div class="col-md-7">
							<p>Digital Market Square Copyright 2015. All Rights Reserved.</p>
						</div>
						<div class="col-md-4">
							<nav id="sub-menu">
								<ul>
									<li><a href="page-faq.html">FAQ's</a></li>
									<li><a href="sitemap.html">Sitemap</a></li>
									<li><a href="contact-us.html">Contact</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>-->
		</footer>
		
		</div>

		<!-- Head Libs -->
        <script src="<c:url value='/ruralAssets/vendor/modernizr/modernizr.js'/>"></script>

		<!-- Vendor -->
		<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.appear/jquery.appear.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.easing/jquery.easing.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery-cookie/jquery-cookie.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/common/common.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.validation/jquery.validation.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.stellar/jquery.stellar.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.gmap/jquery.gmap.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/isotope/jquery.isotope.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/owlcarousel/owl.carousel.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jflickrfeed/jflickrfeed.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/magnific-popup/jquery.magnific-popup.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/vide/vide.js'/>"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<c:url value='/ruralAssets/js/theme.js'/>"></script>
		
		<!-- Specific Page Vendor and Views -->
		<script src="<c:url value='/ruralAssets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/circle-flip-slideshow/js/jquery.flipshow.js'/>"></script>
		<script src="<c:url value='/ruralAssets/js/views/view.home.js'/>"></script>
		
		<decorator:getProperty property="page.script"></decorator:getProperty>

		<!-- Theme Custom -->
		<script src="<c:url value='/ruralAssets/js/custom.js'/>"></script>


        <!-- Theme Initialization Files-->
		<script src="<c:url value='/ruralAssets/js/theme.init.js'/>"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script type="text/javascript">
/*
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-12345678-1']);
			_gaq.push(['_trackPageview']);

			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
/*
		</script>-->
		
		
		
</body>
</html>


