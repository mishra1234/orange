<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>

<c:url var="profile" value="../member/account/${account.id}"/>

<html>
<head>


<!-- Basic -->
<meta charset="utf-8">
<title>Rural Connect</title>
<meta name="keywords" content="Rural Connect South Australia" />
<meta name="description" content="Rural Connect South Australia">
<meta name="author" content="Digital Market Square">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="<c:url value='/assets/vendor/fullcalendar/lib/cupertino/jquery-ui.min.css'/>" />
<link rel="stylesheet" href="<c:url value='/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css'/>" />
<link rel="stylesheet" href="<c:url value='/assets/vendor/fullcalendar/fullcalendar.css'/>" />
<link rel="stylesheet" href="<c:url value='/assets/vendor/fullcalendar/fullcalendar.print.css'/>" media="print" /> 
<link rel="stylesheet" href="<c:url value='/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css'/>" />		
		
<!-- Vendor CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/fontawesome/css/font-awesome.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/owlcarousel/owl.carousel.min.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/owlcarousel/owl.theme.default.min.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/magnific-popup/magnific-popup.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/scripts/jquery-ui-1.11.2.custom/jquery-ui.min.css'/>"> 

<!-- Theme CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-elements.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-blog.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-shop.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-animate.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/widgets.css'/>">


<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/colorbox/colorbox.css'/>">
<script src="<c:url value='/ruralAssets/vendor/colorbox/jquery.colorbox-min.js'/>"></script>
<!--
<link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />
<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>-->

<script src="<c:url value='/assets/vendor/select2/select2.js"'/>"></script>

<!-- Current Page CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/rs-plugin/css/settings.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/circle-flip-slideshow/css/component.css'/>" media="screen">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<!--Code for tracking  -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46391228-5', 'auto');
  ga('send', 'pageview');

</script>

<!-- Latest compiled and minified CSS 
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <!-- Optional theme 
        <link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/bootstrap/3.3.2/css/bootstrap.min.css'/>">


<!-- Skin CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/skins/default.css'/>">

		
		

</head>
<body>

	<div class="body">
		<div id="headerContent">
			<header id="header">
				<div class="container">
					<div class="logo">
						<img alt="Rural Connect" width="149" height="50" data-sticky-width="100" data-sticky-height="35" src="<c:url value='/ruralAssets/img/logo.png'/>">
					</div>

					

				</div>
			</header>
		</div>
		
		<div id="content">
			<decorator:body />
		</div>
		
	</div>
		 <script src="<c:url value='/assets/javascripts/theme.js'/>"></script>
		 
		         <!-- Theme Custom -->
        <script src="<c:url value='/assets/javascripts/theme.custom.js'/>"></script>

        <!-- Theme Initialization Files -->
        <script src="<c:url value='/assets/javascripts/theme.init.js'/>"></script>    
		 

		<!-- Head Libs -->
        <script src="<c:url value='/ruralAssets/vendor/modernizr/modernizr.js'/>"></script>
		
		
		

		<!-- Vendor -->

</body>

</html>


