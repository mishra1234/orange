<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>

<c:url var="profile" value="../member/account/${account.id}"/>

<html>
	<head>

<link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />
<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>Rural Connect</title>		
		<meta name="keywords" content="Rural Connect" />
		<meta name="description" content="Rural Connect">
		<meta name="author" content="marco salinas @digitalmarketsquare.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.css'/>">
		<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/fontawesome/css/font-awesome.css'/>">
		<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/owlcarousel/owl.carousel.min.css'/>" media="screen">
		<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/owlcarousel/owl.theme.default.min.css'/>" media="screen">
		<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/magnific-popup/magnific-popup.css'/>" media="screen">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme.css'/>">
		<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-elements.css'/>">
		<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-blog.css'/>">
		<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-shop.css'/>">
		<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-animate.css'/>">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<c:url value='/ruralAssets/css/skins/default.css'/>">


		<!-- Head Libs -->
		 <script src="<c:url value='/ruralAssets/vendor/modernizr/modernizr.js'/>"></script>

		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->

	</head>
<body>

		<div id="content">
			<decorator:body />
		</div>


		<footer>
		
			<!-- Vendor -->
			
			<!-- 
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.appear/jquery.appear.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.easing/jquery.easing.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery-cookie/jquery-cookie.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/common/common.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.validation/jquery.validation.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.stellar/jquery.stellar.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery.gmap/jquery.gmap.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/isotope/jquery.isotope.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/owlcarousel/owl.carousel.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jflickrfeed/jflickrfeed.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/magnific-popup/jquery.magnific-popup.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/vide/vide.js'/>"></script>
		
		 <script src="<c:url value='/ruralAssets/js/theme.js'/>"></script>
		
	<script src="<c:url value='/ruralAssets/js/custom.js'/>"></script>

      
        <script src="<c:url value='/ruralAssets/js/theme.init.js'/>"></script>    
		 
	
        <script src="<c:url value='/ruralAssets/vendor/modernizr/modernizr.js'/>"></script>
		 -->
		 <script type="text/javascript">
		$(document).ready(function(){
			$(".infosframe").colorbox({iframe:true, width:"70%", height:"80%"});
			
		});
	</script>
	
		</footer>

</body>

</html>


