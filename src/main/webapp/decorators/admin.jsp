<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>

  <c:url var="profile" value="../admin/account/${acct.id}"/>
  <c:url var="logout" value="/../login"/>
  <c:url var="RC" value="false" scope="session"/>


<c:choose>
	<c:when test="${RC}">
		<c:url var="appName" value=" CCMe" />
		<c:url var="skin" value="ruralConnectSkin.css" />
		<c:url var="logo" value="logoRuralConnect.png" />
	</c:when>
	<c:otherwise>
		<c:url var="appName" value="<strong> CC Me </strong>: Community Connect Me" />
		<c:url var="skin" value="default.css" />
		<c:url var="logo" value="logo.png" />
	</c:otherwise>
</c:choose>




<!doctype html>
<html class="fixed">
	<head>
	<!--Code for tracking  -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46391228-5', 'auto');
  ga('send', 'pageview');

</script>
	

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>CCMe: Start Connecting</title>

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css'/>">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<c:url value='/assets/vendor/bootstrap/css/bootstrap.css'/>" />
		
		<link rel="stylesheet" href="<c:url value='/assets/vendor/font-awesome/css/font-awesome.min.css'/>" />
		<link rel="stylesheet" href="<c:url value='/assets/vendor/magnific-popup/magnific-popup.css'/>" />
		<link rel="stylesheet" href="<c:url value='/assets/vendor/bootstrap-datepicker/css/datepicker3.css'/>" />
		

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<c:url value='/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css'/>" />
		<link rel="stylesheet" href="<c:url value='/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css'/>" />
		<link rel="stylesheet" href="<c:url value='/assets/vendor/morris/morris.css'/>" />
		
		<link rel="stylesheet" href="<c:url value='/assets/vendor/select2/select2.css'/>"/>		
		<link rel="stylesheet" href="<c:url value='/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css'/>" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<c:url value='/assets/stylesheets/theme.css'/>" />

		<link rel="stylesheet" href="<c:url value='/assets/stylesheets/skins/${skin}'/>" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<c:url value='/assets/stylesheets/theme-custom.css'/>">

		<!-- Head Libs -->
		<script src="<c:url value='/assets/vendor/modernizr/modernizr.js'/>"></script>
		
		
		<!-- Vendor -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="<c:url value='/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap/js/bootstrap.js'/>"></script>
		<script src="<c:url value='/assets/vendor/nanoscroller/nanoscroller.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js'/>"></script>
		<script src="<c:url value='/assets/vendor/magnific-popup/magnific-popup.js'/>"></script>
		<script src="<c:url value='/assets/vendor/jquery-placeholder/jquery.placeholder.js'/>"></script>
		
		<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
		<script src="<c:url value='/assets/vendor/gmaps/gmaps.js'/>"></script>		
				
		<!-- Validation -->
		<script src="<c:url value='/assets/vendor/jquery-validation/jquery.validate.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js'/>"></script>
		<script src="<c:url value='/assets/vendor/pnotify/pnotify.custom.js'/>"></script>
		
		<!--[if IE]>
			<link rel="stylesheet" href="<c:url value='/ruralAssets/css/ie.css'/>">
		<![endif]-->

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!--[if lte IE 8]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<c:url value='/ruralAssets/vendor/respond/respond.js'/>"></script>
			<script src="<c:url value='/ruralAssets/vendor/excanvas/excanvas.js'/>"></script>
		<![endif]-->

<link rel="apple-touch-icon" sizes="57x57" href="<c:url value='/assets/images/fav/apple-icon-57x57.png'/>">
<link rel="apple-touch-icon" sizes="60x60" href="<c:url value='/assets/images/fav/apple-icon-60x60.png'/>">
<link rel="apple-touch-icon" sizes="72x72" href="<c:url value='/assets/images/fav/apple-icon-72x72.png'/>">
<link rel="apple-touch-icon" sizes="76x76" href="<c:url value='/assets/images/fav/apple-icon-76x76.png'/>">
<link rel="apple-touch-icon" sizes="114x114" href="<c:url value='/assets/images/fav/apple-icon-114x114.png'/>">
<link rel="apple-touch-icon" sizes="120x120" href="<c:url value='/assets/images/fav/apple-icon-120x120.png'/>">
<link rel="apple-touch-icon" sizes="144x144" href="<c:url value='/assets/images/fav/apple-icon-144x144.png'/>">
<link rel="apple-touch-icon" sizes="152x152" href="<c:url value='/assets/images/fav/apple-icon-152x152.png'/>">
<link rel="apple-touch-icon" sizes="180x180" href="<c:url value='/assets/images/fav/apple-icon-180x180.png'/>">
<link rel="icon" type="image/png" sizes="192x192"  href="<c:url value='/assets/images/fav/android-icon-192x192.png'/>">
<link rel="icon" type="image/png" sizes="32x32" href="<c:url value='/assets/images/fav/favicon-32x32.png'/>">
<link rel="icon" type="image/png" sizes="96x96" href="<c:url value='/assets/images/fav/favicon-96x96.png'/>">
<link rel="icon" type="image/png" sizes="16x16" href="<c:url value='/assets/images/fav/favicon-16x16.png'/>">
<link rel="manifest" href="<c:url value='/assets/images/fav/manifest.json'/>">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<c:url value='/assets/images/fav/ms-icon-144x144.png'/>">
<meta name="theme-color" content="#ffffff">



	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="<c:url value='/admin/communities'/>" class="logo">
					
						<img src="<c:url value='/assets/images/${logo}'/>" height="35" alt="${appName} Dashboard" /> 
						
					
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>

				<!-- start: search & user box -->
				<div class="header-right">

					

					<span class="separator"></span>

					<ul class="notifications">
					<!--  
					<li>
							<a href="<c:url value='/member/home'/>" class="dropdown-toggle notification-icon">
								<abbr title="Click to go to Members Public Dashboard"><i class="fa fa-expand"></i></abbr> 
								
							</a>
					</li>
					
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
								<i class="fa fa-envelope"></i>
								<span class="badge">${newMessages}</span>
							</a>

							<div class="dropdown-menu notification-menu">
								<div class="notification-title">
									<span class="pull-right label label-default">${newMessages}</span>
									Messages
								</div>

								<div class="content">
								<!-- 
									<ul>
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<img src="<c:url value='/assets/images/!sample-user.jpg'/>" alt="Joseph Doe Junior" class="img-circle" />
												</figure>
												<span class="title">Joseph Doe</span>
												<span class="message">Lorem ipsum dolor sit.</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<img src="<c:url value='/assets/images/!sample-user.jpg'/>" alt="Joseph Junior" class="img-circle" />
												</figure>
												<span class="title">Joseph Junior</span>
												<span class="message truncate">Truncated message. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam, nec venenatis risus. Vestibulum blandit faucibus est et malesuada. Sed interdum cursus dui nec venenatis. Pellentesque non nisi lobortis, rutrum eros ut, convallis nisi. Sed tellus turpis, dignissim sit amet tristique quis, pretium id est. Sed aliquam diam diam, sit amet faucibus tellus ultricies eu. Aliquam lacinia nibh a metus bibendum, eu commodo eros commodo. Sed commodo molestie elit, a molestie lacus porttitor id. Donec facilisis varius sapien, ac fringilla velit porttitor et. Nam tincidunt gravida dui, sed pharetra odio pharetra nec. Duis consectetur venenatis pharetra. Vestibulum egestas nisi quis elementum elementum.</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<img src="<c:url value='/assets/images/!sample-user.jpg'/>" alt="Joe Junior" class="img-circle" />
												</figure>
												<span class="title">Joe Junior</span>
												<span class="message">Lorem ipsum dolor sit.</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<img src="<c:url value='/assets/images/!sample-user.jpg'/>" alt="Joseph Junior" class="img-circle" />
												</figure>
												<span class="title">Joseph Junior</span>
												<span class="message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam.</span>
											</a>
										</li>
									</ul>
									 
									<hr />

									
								</div>
							</div>
						</li>

					<!-- 	<li>
							<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
								<i class="fa fa-bell"></i>
								<!--  <span class="badge">12</span>
							</a>

							<div class="dropdown-menu notification-menu">
								<div class="notification-title">
									<!--<span class="pull-right label label-default">12</span>
									Alerts
								</div>

								<div class="content">
								<!--
									<ul>
										<li>
											<a href="#" class="clearfix">
												<div class="image">
													<i class="fa fa-bullhorn bg-danger"></i>
												</div>
												<span class="title">Critical future events</span>
												<span class="message">4 Communities</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<div class="image">
													<i class="fa fa-lock bg-info"></i>
												</div>
												<span class="title">Access Request</span>
												<span class="message">4 Pending</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<div class="image">
													<i class="fa fa-minus bg-warning"></i>
												</div>
												<span class="title">Low future events</span>
												<span class="message">4 Communities</span>
											</a>
										</li> 
									</ul>
									<hr />

									
								</div>
							</div>
						</li>-->
					</ul>
					
					<span class="separator"></span>

					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<c:url value='/assets/images/!logged-user.jpg'/>" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg'/>" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="${acct.email}">
								<span class="name">${acct.firstName}- Admin</span>
								
							</div>

							<i class="fa custom-caret"></i>
						</a>

						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="<c:url value='/admin/account/${acct.id}'/>"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="<c:url value='${logout}'/>"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">

					<div class="sidebar-header">
						<div class="sidebar-title">
							Connect
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>

					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-active">
										<a href="<c:url value='/admin/home'/>">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>

									<li>
										<a href="<c:url value='/admin/inbox'/>">
											<span class="pull-right label label-primary">${newMessages}</span>
											<i class="fa fa-envelope" aria-hidden="true"></i>
											<span>Inbox</span>
										</a>
									</li>
									
									<li class="nav-children">
										<a href="<c:url value='/admin/categories'/>">
											<i class="fa fa-book" aria-hidden="true"></i>
											<span>Categories</span>
										</a>
									</li>
									
									<li class="nav-parent">
										<a>
											<i class="fa fa-copy" aria-hidden="true"></i>
											<span>Communities</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="<c:url value='/admin/communities'/>">
													All Communities
												</a>
											</li>
											<li>
												<a href="<c:url value='/admin/community/new/'/>">
													 New Community
												</a>
											</li>
											
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-tasks" aria-hidden="true"></i>
											<span>Events</span>
										</a>
										<ul class="nav nav-children">
											<!-- 
											<li>
												<a href="<c:url value='/admin/events'/>">All Events</a>
											</li>
											 
											<li>
												<a href="<c:url value='/admin/event/new/'/>">New Event</a>
											</li> 
											--> 
											<li>
												<a href="<c:url value='/admin/bulk/bulkLoad'/>">Bulk Loading</a>
											</li>
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-tasks" aria-hidden="true"></i>
											<span>Info Pages</span>
										</a>
										<ul class="nav nav-children">
											<!-- 
											<li>
												<a href="<c:url value='/admin/infos'/>">
													 All Info Pages
												</a>
											</li>
											 
											<li>
												<a href="<c:url value='/admin/info/new/'/>">
													 New Info
												</a>
											</li>
											-->
										</ul>
									</li>
									<li class="nav-parent">
										<a><i class="fa fa-tasks" aria-hidden="true"></i><span>Accounts Management</span></a>
										<ul class="nav nav-children">
											<!--
											<li>
		                                        <a href="<c:url value='/admin/accounts'/>">
		                                            <i class="fa fa-user" aria-hidden="true"></i>
		                                            <span>Accounts</span>
		                                        </a>
                                    		</li>
                                    		-->
                                    		<li>
		                                        <a href="<c:url value='/admin/account/new/'/>">
													<i class="fa fa-plus" /></i>
		                                            <span>Create New Account</span>
		                                        </a>
                                    		</li>
										</ul>
									</li>
									<!-- Surveys -->
									<li class="nav-parent">
										<a>
											<i class="fa fa-tasks" aria-hidden="true"></i>
											<span>Surveys</span>
										</a>
										<ul class="nav nav-children">
											<!--
											<li>
												<a href="<c:url value='/admin/surveys'/>">
													 All Surveys
												</a>
											</li>
											
											<li>
												<a href="<c:url value='/admin/survey/new/'/>">
													 New Survey
												</a>
											</li>
											-->
										</ul>
									</li>
									 
									
									<li class="nav">
										<a href="<c:url value='/member/home'/>">
											<i class="fa fa-link" aria-hidden="true"></i>
											<span>Member Home</span>
										</a>
									</li>
									 
									
									
									
									<!-- 
									<li class="nav-parent">
										<a>
											<i class="fa fa-table" aria-hidden="true"></i>
											<span>Information Activity</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="tables-basic.html">
													All Information pages
												</a>
											</li>
											<li>
												<a href="tables-advanced.html">
													 Recommended
												</a>
											</li>
											<li>
												<a href="tables-responsive.html">
													 Archive
												</a>
											</li>
										</ul>
									</li>
									 
									<li class="nav-parent">
										<a>
											<i class="fa fa-map-marker" aria-hidden="true"></i>
											<span>Activities by Location</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="maps-google-maps.html">
													 Basic
												</a>
											</li>
											<li>
												<a href="maps-google-maps-builder.html">
													 Map Builder
												</a>
											</li>
											<li>
												<a href="maps-vector.html">
													 Vector
												</a>
											</li>
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-align-left" aria-hidden="true"></i>
											<span>Engagement Index</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a>First Level...</a>
											</li>
										</ul>
									</li><!--
									<li>
										<a href="http://themeforest.net/item/porto-responsive-html5-template/4106987?ref=Okler" target="_blank">
											<i class="fa fa-external-link" aria-hidden="true"></i>
											<span>Front-End <em class="not-included">(Not Included)</em></span>
										</a>
									</li>-->
								</ul>
							</nav>

							<hr class="separator" />

							<div class="sidebar-widget widget-stats" style='display:none;'>
								<div class="widget-header">
									<h6>Community Stats</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul>
										<li>
											<span class="stats-title">Crush 2015</span>
											<span class="stats-complete">85%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
													<span class="sr-only">85% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Golden Grove Village</span>
											<span class="stats-complete">20%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
													<span class="sr-only">20% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Victoria What's On</span>
											<span class="stats-complete">15%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
													<span class="sr-only">15% Complete</span>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>

					</div>

				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Dashboard</h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<c:url value='/admin/communities'/>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Dashboard</span></li>
							</ol>

							<a class="sidebar-right-toggle" data-fire-event="sidebar-left-opened"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

  
    <div id="content">
        <decorator:body/>
    </div>

</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>

						<div class="sidebar-right-wrapper">

							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>

								<ul>
									<li>
										<time datetime="2014-04-19T00:00+00:00">04/19/2014</time>
										<span>Company Meeting</span>
									</li>
								</ul>
							</div>

							<div class="sidebar-widget widget-friends">
								<h6>Friends</h6>
								<ul>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="<c:url value='/assets/images/!sample-user.jpg'/>" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="<c:url value='/assets/images/!sample-user.jpg'/>" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="<c:url value='/assets/images/!sample-user.jpg'/>" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="<c:url value='/assets/images/!sample-user.jpg'/>" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
								</ul>
							</div>

						</div>
					</div>
				</div>
			</aside>
		</section>

		

		<!-- Specific Page Vendor -->
		<script src="<c:url value='/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js'/>"></script>
		<script src="<c:url value='/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js'/>"></script>
		<script src="<c:url value='/assets/vendor/jquery-appear/jquery.appear.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js'/>"></script>
		<script src="<c:url value='/assets/vendor/jquery-easypiechart/jquery.easypiechart.js'/>"></script>
		<script src="<c:url value='/assets/vendor/flot/jquery.flot.js'/>"></script>
		<script src="<c:url value='/assets/vendor/flot-tooltip/jquery.flot.tooltip.js'/>"></script>
		<script src="<c:url value='/assets/vendor/flot/jquery.flot.pie.js'/>"></script>
		<script src="<c:url value='/assets/vendor/flot/jquery.flot.categories.js'/>"></script>
		<script src="<c:url value='/assets/vendor/flot/jquery.flot.resize.js'/>"></script>
		<script src="<c:url value='/assets/vendor/jquery-sparkline/jquery.sparkline.js'/>"></script>
		<script src="<c:url value='/assets/vendor/raphael/raphael.js'/>"></script>
		<script src="<c:url value='/assets/vendor/morris/morris.js'/>"></script>
		<script src="<c:url value='/assets/vendor/gauge/gauge.js'/>"></script>
		<script src="<c:url value='/assets/vendor/snap-svg/snap.svg.js'/>"></script>
		<script src="<c:url value='/assets/vendor/liquid-meter/liquid.meter.js'/>"></script>
	
		<script src="<c:url value='/assets/vendor/codemirror/lib/codemirror.js'/>"></script>
		
			<!-- DataTables -->
		<script src="<c:url value='/assets/vendor/select2/select2.js"'/>"></script>
		<script src="<c:url value='/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"'/>"></script>
		<script src="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"'/>"></script>
		<script src="<c:url value='/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"'/>"></script>


		<!-- Theme Base, Components and Settings -->
		<script src="<c:url value='/assets/javascripts/theme.js'/>"></script>

		<!-- Theme Custom -->
		<script src="<c:url value='/assets/javascripts/theme.custom.js'/>"></script>

		<!-- Theme Initialization Files -->
		<script src="<c:url value='/assets/javascripts/theme.init.js'/>"></script>


		<!-- Examples -->
		<script src="<c:url value='/assets/javascripts/dashboard/examples.dashboard.js'/>"></script>
		<script src="<c:url value='/assets/javascripts/forms/examples.advanced.form.js'/>" /></script>
		
		
		
		<script>$(document).ready(function () {
			    $('#go').click(function () {
			        var address = $('#address').val();
			        if (address) {
			            var geocoder = new google.maps.Geocoder();
			            geocoder.geocode({ 'address': address }, function (results, status) {
			                if (status == google.maps.GeocoderStatus.OK) {
			                    if (results[0]) {
			                        (
			                        	document.getElementById('latitude').value=  results[0].geometry.location.lat(), 
			                        	document.getElementById('longitude').value= results[0].geometry.location.lng());
			                    } else {
			                        error('Google did not return any results.');
			                    }
			                } else {
			                    error("Reverse Geocoding failed due to: " + status);
			                }
			            });
			        }
			        else {
			            error('Please enter an address');
			        }
			    });
			});
			function error(msg) {
			    alert(msg);
			}
		</script>
		
	</body>
</html>
  
 
</body>

</html>