<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html>
<html>
<head>
    <title><decorator:title default="Community Connect"/></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<c:url value='/styles/bootstrap.css"'/>">	
	<link rel="stylesheet" href="<c:url value='/styles/bootstrap-responsive.css"'/>">	

    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.css">

<script src="<c:url value='/scripts/jquery-1.10.2.min.js'/>"></script>
	<script src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
					
	
	
	
	  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
         <!--Code for tracking  -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46391228-5', 'auto');
  ga('send', 'pageview');

</script>
                                   
          <style type="text/css">
      body {
        padding-top: 20px;
        padding-bottom: 60px;
      }

      /* Custom container */
      .container {
        margin: 0 auto;
        max-width: 1000px;
      }
      .container > hr {
        margin: 60px 0;
      }

      /* Main marketing message and sign up button */
      .jumbotron {
        margin: 80px 0;
        text-align: center;
      }
      .jumbotron h1 {
        font-size: 100px;
        line-height: 1;
      }
      .jumbotron .lead {
        font-size: 24px;
        line-height: 1.25;
      }
      .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
      }

      /* Supporting marketing content */
      .marketing {
        margin: 60px 0;
      }
      .marketing p + h4 {
        margin-top: 28px;
      }


      /* Customize the navbar links to be fill the entire space of the .navbar */
      .navbar .navbar-inner {
        padding: 0;
      }
      .navbar .nav {
        margin: 0;
        display: table;
        width: 100%;
      }
      .navbar .nav li {
        display: table-cell;
        width: 1%;
        float: none;
      }
      .navbar .nav li a {
        font-weight: bold;
        text-align: center;
        border-left: 1px solid rgba(255,255,255,.75);
        border-right: 1px solid rgba(0,0,0,.1);
      }
      .navbar .nav li:first-child a {
        border-left: 0;
        border-radius: 3px 0 0 3px;
      }
      .navbar .nav li:last-child a {
        border-right: 0;
        border-radius: 0 3px 3px 0;
      }
    </style>                        


<script type="text/javascript">
		$(document).ready(function() {

						
		});
</script>

</head>

<body>
    <div class="container">

      <div class="masthead">
        <h3 class="muted">Community Connect Me - Administrator</h3>
        <h5><a href="/ccme/login?log=520ce4e03480fcb95b6b6bbcf7120271">Logout</a>
        </h5>

        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
              <ul class="nav">
                <li><a href="<c:url value='/admin/home'/>">Home</a></li>
                <li><a href="<c:url value='/admin/categories'/>">Categories</a></li>
                <li><a href="<c:url value='/admin/communities'/>">Communities</a></li>
                <li><a href="<c:url value='/admin/events'/>">Events</a></li>
                <li><a href="<c:url value='/admin/messages'/>">Messages</a></li>
                <li><a href="<c:url value='/admin/banners'/>">Banners</a></li>
                <li><a href="<c:url value='/admin/accounts'/>">Accounts</a></li>
                
              </ul>
            </div>
          </div>
        </div><!-- /.navbar -->
      </div>

      <!-- 
      <div class="jumbotron">
        <h1>Admin stuff!</h1>
        <p class="lead"></p>
        <a class="btn btn-large btn-success" href="#">Get started today</a>
      </div>

      <hr>Jumbotron -->

    
    <div id="content">
        <decorator:body/>
    </div>
 
    <div class="footer">
    <p>
    <p>
    <hr>
        <p>&copy; DMSq 2014</p>
      </div>

    </div> <!-- /container -->
  
 
</body>

	

</html>
