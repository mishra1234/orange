<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>

<c:url var="profile" value="/member/account/"/>

<html>
<head>


<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Rural Connect</title>
<meta name="keywords" content="Rural Connect South Australia" />
<meta name="description" content="Rural Connect South Australia">
<meta name="author" content="Digital Market Square">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Web Fonts  -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="<c:url value='/assets/vendor/fullcalendar/lib/cupertino/jquery-ui.min.css'/>" />
<link rel="stylesheet" href="<c:url value='/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css'/>" />
<link rel="stylesheet" href="<c:url value='/assets/vendor/fullcalendar/fullcalendar.css'/>" />
<link rel="stylesheet" href="<c:url value='/assets/vendor/fullcalendar/fullcalendar.print.css'/>" media="print" /> 
<link rel="stylesheet" href="<c:url value='/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css'/>" />		
		
<!-- Vendor CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/fontawesome/css/font-awesome.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/owlcarousel/owl.carousel.min.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/owlcarousel/owl.theme.default.min.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/magnific-popup/magnific-popup.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/scripts/jquery-ui-1.11.2.custom/jquery-ui.min.css'/>"> 

<!-- Theme CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-elements.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-blog.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-shop.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/theme-animate.css'/>">
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/widgets.css'/>">

<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/colorbox/colorbox.css'/>">

<link rel="stylesheet" href="<c:url value='/styles/youmax.css'/>"> 

<!-- Current Page CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/rs-plugin/css/settings.css'/>" media="screen">
<link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/circle-flip-slideshow/css/component.css'/>" media="screen">


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>

<script src="<c:url value='/assets/vendor/select2/select2.js"'/>"></script>

<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
					


<!--Code for tracking  -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46391228-5', 'auto');
  ga('send', 'pageview');

</script>

<!-- Latest compiled and minified CSS 
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <!-- Optional theme 
        <link rel="stylesheet" href="<c:url value='/ruralAssets/vendor/bootstrap/3.3.2/css/bootstrap.min.css'/>">


<!-- Skin CSS -->
<link rel="stylesheet" href="<c:url value='/ruralAssets/css/skins/default.css'/>">

<!-- Theme Custom CSS -->
<!-- <link rel="stylesheet" href="<c:url value='/assets/stylesheets/theme-custom.css'/>"> -->

<!--[if IE]>
			<link rel="stylesheet" href="<c:url value='/css/ie.css'/>">
		<![endif]-->

<!--[if lte IE 8]>
			<script src="<c:url value='ruralAssets/vendor/respond/respond.js'/>"></script>
			<script src="<c:url value='ruralAssets/vendor/excanvas/excanvas.js'/>"></script>
		<![endif]-->


</head>
<body>

	<div class="body">

		<div id="headerContent">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="<c:url value='/member/home'/>" alt="Member Area"><img alt="Rural Connect" width="149" height="50" data-sticky-width="100" data-sticky-height="35" src="<c:url value='/ruralAssets/img/logo.png'/>">
						</a>
					</div>

					<button class="btn btn-responsive-nav btn-inverse"
						data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>

				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container" id="lower">
						<nav class="nav-main mega-menu">
						    <ul class="nav nav-pills nav-main" id="mainMenu">		
						    
						    <li id="HomeOption">
						            <a class="dropdown-toggle" href="<c:url value='/member/home'/>">Home <br>&nbsp;</a>
						        </li>
						    
						     <c:if test="${ungatedGrain}">			
								<li id="grainOption">
						            <a class="dropdown-toggle" href="<c:url value='/member/grain'/>">GRAIN PRODUCERS <br> SA</a>
						        </li>
						     </c:if>
						     <c:if test="${ungatedLivestock}">	
						        <li id="livestockOption">
						            <a class="dropdown-toggle" href="<c:url value='/member/livestock'/>">LIVESTOCK <br>SA<BR></a>
						        </li>
						        </c:if>
						     <c:if test="${ungatedHorticulture}">	
								<li id="horticultureOption">
						            <a class="dropdown-toggle" href="<c:url value='/member/horticulture'/>" >HORTICULTURE</br> COALITION of SA</a>
						        </li>
						        </c:if>
						     <c:if test="${ungatedWine}">	
						        <li>
						            <a class="dropdown-toggle" href="http://wgcsa.com.au/" target="_blank">WINE GRAPE COUNCIL OF</br> South Australia</a>
						        </li>
						        </c:if>
						     <c:if test="${ungatedDairy}">	
						        <li id="dairyfarmersOption">
						            <a class="dropdown-toggle" href="<c:url value='/member/dairyfarmers'/>">SA DAIRYFARMERS<BR> ASSOCIATION</a>
						        </li>
						        </c:if>
<%-- 						     <c:if test="${ungatedPork}">	 --%>
<!-- 						        <li id="porkOption"> -->
<%-- 						            <a class="dropdown-toggle" href="<c:url value='/member/pork'/>">PORK <br>SA<BR></a> --%>
<!-- 						        </li> -->
<%-- 						        </c:if> --%>
						    
							
        
						        <!-- Sign in-->
						
						        <li class="dropdown mega-menu-item mega-menu-signin signin logged" id="headerAccount">
						            <a class="dropdown-toggle" href="#" style="background-color:transparent;">
						                <i class="fa fa-user" style="color:#1B5E20; margin-top:5px"></i> 
						                
						            </a>
						            <ul class="dropdown-menu">
						                <li>
						                    <div class="mega-menu-content">
						                        <div class="row">
						                            <div class="col-md-7">
						                                <div class="user-avatar">
						                                    <div class="img-thumbnail">
						                                        <img src="<c:url value='${account.imageUrl}'/>" alt="">
						                                    </div>
						                                    <p><strong>${account.name}</strong><span>${account.email}</span></p>
						                                </div>
						                            </div>
						                            <div class="col-md-5">
						                                <ul class="list-account-options">
						                                    <li>
						                                    	<a role="menuitem" tabindex="-1" href="<c:url value='/member/account/${account.id}'/>" style="background-color: transparent !important;">
						                                    	<i class="fa fa-user"></i> My Account</a>
						                                    </li>
						                                    <c:if test="${isAdmin}">
						                                      <li>
						                                    	<a role="menuitem" tabindex="-1" href="<c:url value='/admin/communities'/>" style="background-color: transparent !important;">
						                                    	<abbr title="Click to go to Admin Panel"><i class="fa fa-gear"></i></abbr> Admin Panel</a>
						                                    </li>
						                                    </c:if>
						                                    <li>
						                                        <a href="<c:url value='/login?logout'/>" style="background-color: transparent !important;">Log Out</a>
						                                    </li>
						                                </ul>
						                            </div>
						                        </div>
						                    </div>
						                </li>
						            </ul>
						        </li>
					        
								<li>
									<a href="<c:url value='/member/inbox'/>" class="notification-icon warning" style="background-color:transparent;">
										<i class="fa fa-envelope" style="color:#1B5E20"></i>
										<span class="badge" style="background-color:#BF360C">${newMessages}</span>
									</a>
								</li>
								<li>
									<a href="http://rural-resources.ruralconnect.com.au/user-guide/userguide.pdf" target="_BLANK" class="notification-icon warning" style="background-color:transparent;color:#FFD700;margin-left:-20px">
										<i class="fa fa-book" style="color:#1B5E20"></i><i class="fa fa-question" style="color:#1B5E20"></i>
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</header>
		</div>

		<div id="content">
			<decorator:body />
		</div>

		<!-- End: page -->
		<footer id="footer" class="light">
			<div class="container">
				<div class="row">
					<div class="footer-ribbon">
						<span>Keep Connected</span>
					</div>
					
					<div class="col-md-5">
						<h4>Mobile Version</h4>
						<div class="left">
							<p>
								<img src="<c:url value='/ruralAssets/img/placements/comingsoon.png'/>" height="100" />
							</p>
						</div>
					</div>
					<div class="col-md-5">
						<div class="contact-details">
							<h4>Contact Us</h4>
							<ul class="contact">
								<li><p>
										<i class="fa fa-map-marker"></i> <strong>Address:</strong>
										Unit 4, 780 South Road, Glandore SA 5037
									</p></li>
								<li><p>
										<i class="fa fa-phone"></i> <strong>Phone:</strong> +61  (08) 8297 0899
									</p></li>
								<li><p>
										<i class="fa fa-envelope"></i> <strong>Email:</strong> 
											<a href="mailto:admin@ppsa.org.au">admin@ppsa.org.au</a>
									</p></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
		
		</div>
		 <script type="text/javascript" src="<c:url value='/ruralAssets/js/theme.js'/>"></script>
		 
		         <!-- Theme Custom -->
        <script src="<c:url value='/ruralAssets/js/custom.js'/>"></script>

        <!-- Theme Initialization Files -->
        <script type="text/javascript" src="<c:url value='/ruralAssets/js/theme.init.js'/>"></script>    
		 

		<!-- Head Libs -->
        <script type="text/javascript" src="<c:url value='/ruralAssets/vendor/modernizr/modernizr.js'/>"></script>
		
		
		

		<!-- Vendor -->
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/jquery.appear/jquery.appear.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/jquery.easing/jquery.easing.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/jquery-cookie/jquery-cookie.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/common/common.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/jquery.validation/jquery.validation.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/jquery.stellar/jquery.stellar.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/jquery.gmap/jquery.gmap.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/isotope/jquery.isotope.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/owlcarousel/owl.carousel.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/jflickrfeed/jflickrfeed.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/magnific-popup/jquery.magnific-popup.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/vide/vide.js'/>"></script>

		
		<!-- Specific Page Vendor and Views -->
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/circle-flip-slideshow/js/jquery.flipshow.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/ruralAssets/js/views/view.home.js'/>"></script>

		<!-- DataTables -->
		<script type="text/javascript" src="<c:url value='/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"'/>"></script>
		<script type="text/javascript" src="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"'/>"></script>
		<script type="text/javascript" src="<c:url value='/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"'/>"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-12345678-1']);
			_gaq.push(['_trackPageview']);

			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

		</script>-->
	<script type="text/javascript" >
		(function( $ ) {
			var datatableInit = function() {
				var $table = $('#eventsTable');
				$table.DataTable({
					sDom: "<'text-right mb-md'>" + $.fn.DataTable.defaults.sDom,
					"paging":false,
					"info":false,
					"order":[[1,"asc"]],
					"oLanguage": {
					    "sSearch": "Search: "},
					"bDestroy": true
				});
			};
			$(function() {
				datatableInit();
			});
		}).apply( this, [ jQuery ]);
	</script>	
		
	<script type="text/javascript">
		$(document).ready(function(){
			$(".infosframe").colorbox({iframe:true, width:"70%", height:"80%", style:"overflow: hidden; height: 100%; width: 80%; position: absolute"});
			
			$('.bxslider').bxSlider({
			  mode: 'fade',
			  captions: true,
			  auto: true,
			  slideWidth: 700
			});
		});
	</script>
	
	<link rel="stylesheet" href="http://bxslider.com/lib/jquery.bxslider.css" type="text/css" />
	<link rel="stylesheet" href="http://bxslider.com/css/github.css" type="text/css" />
	<script type="text/javascript" src="http://bxslider.com/lib/jquery.bxslider.js"></script>
	<script type="text/javascript" src="http://bxslider.com/js/rainbow.min.js"></script>
	<script type="text/javascript" src="http://bxslider.com/js/scripts.js"></script>
	
	<script type="text/javascript" src="<c:url value='/ruralAssets/vendor/colorbox/jquery.colorbox-min.js'/>"></script>
	
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="<c:url value='/assets/vendor/gmaps/gmaps.js'/>"></script>	
	
	<script type="text/javascript" src="<c:url value='/assets/vendor/fullcalendar/lib/moment.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/assets/vendor/fullcalendar/fullcalendar.js'/>"></script>
		
		
		<!-- 
Grain: ${ungatedGrain}
Pork: ${ungatedGrain}
Livestock: ${ungatedLivestock}
wine: ${ungatedWine}
hort: ${ungatedHorticulture}
hort: ${ungatedDairy}

<br> Gated <br>
Grain: ${gatedGrain}
Pork: ${gatedGrain}
Livestock: ${gatedLivestock}
wine: ${gatedWine}
hort: ${gatedHorticulture}
hort: ${gatedDairy}
 -->
		
</body>

</html>


