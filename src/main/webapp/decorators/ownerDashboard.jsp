<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html lang="en">
  <html>
<head>
    <title><decorator:title default="Community Connect"/></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

	<link rel="stylesheet" href="<c:url value='/styles/bootstrap.css"'/>">	
	<link rel="stylesheet" href="<c:url value='/styles/bootstrap-responsive.css"'/>">	
	<script src="<c:url value='/scripts/jquery-1.10.2.min.js'/>"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {

						
		});
</script>
	
	
	<div id="header">
	
    <decorator:head/>
    <!--Code for tracking  -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46391228-5', 'auto');
  ga('send', 'pageview');

</script>
    
</head>
  <body>

    <div class="container">

      <form class="form-signin" action="auth" method="post">
        <h2 class="form-signin-heading">CC Me [SELfie]</h2>
        <input name="username" type="text" class="input-block-level" placeholder="Email address">
        <input name="password" type="password" class="input-block-level" placeholder="Password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->

  </body>
</html>
