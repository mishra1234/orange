<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- start: page -->
<html>

<body>

	<div role="main" class="main">

		<section class="page-top">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1>
							<img alt="Rural Connect" height="60"
								src="<c:url value='/ruralAssets/img/icons/grainproducers.jpg'/>">
							Grain Producers SA
						</h1>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="javascript:startConnecting()"
								class="btn btn-lg btn-primary">Gated Access Unlocked</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="container">

			<ul class="nav nav-pills center">
				<li class="pork" id="aboutPorkTab"><a
					href="javascript:aboutPork()">News</a></li>


				<li class="pork" id="porkCommTab"><a
					href="javascript:porkComm()">Communications</a></li>
				<li class="pork" id="porkEventsTab"><a
					href="javascript:porkEvents()">Events</a></li>
<!-- 
				<li class="pork" id="porkBusinesTab"><a
					href="javascript:porkBusiness()">Biosecurity</a></li>
				<li class="pork" id="porkBioSecurityTab"><a
					href="javascript:porkBioSecurity()">Business</a></li>
				<li class="pork" id="grainIndustrailTab"><a
					href="javascript:grainIndustrail()">Industrial Relations</a></li>

				<li class="pork" id="porkFormTab"><a
					href="javascript:porkKeyContacts()">Forms</a></li>

				<li class="pork" id="porkLegalTab"><a
					href="javascript:porkLegal()">Transport</a></li>

				<li class="pork" id="porkOtherTab"><a
					href="javascript:grainTraining()">Training & Education</a></li>

				<li class="pork" id="grainWorkTab"><a
					href="javascript:grainWork()">Work Health Safety </a></li>
			 -->
			</ul>

			<hr />

			<div id="porkContent">

				<!-- About Grain -->
				<div class="row" id="porkAbout">
					<div class="col-sm-7">
						<h2>
							Latest <strong>News</strong>
						</h2>

						<div class="widget">
							<div class="widget-extra themed-background-dark"></div>
							<div class="widget">

								<div class="widget-extra">
									<!-- Timeline Content -->
									<div id="dvNewsRSS" class="timeline"></div>
									<!-- END Timeline Content -->
								</div>
							</div>
						</div>

						</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top"></div>

					</div>


					<!-- Grain Communications -->

					<div class="row" id="porkComm" style="display: none;">
						<div class="col-sm-12">
							<h2>
								Grain <strong> Communications</strong>
							</h2>
							<hr>
							<div class="toggle" data-plugin-toggle="">
								<c:forEach var="act" items="${infos}" begin="0" end="2"
									varStatus="row">
									<section class="toggle">
										<label>
											<div class="img-thumbnail">
												<img class="avatar" alt=""
													src="<c:url value='${act.iconUrl}'/>" width="45px">
											</div> ${act.contentTitle}
										</label>
										<div class="toggle-content" style="display: none;">
											<div class="detail" style="display: block;">
												<c:forEach var="banner" items="${act.banners}" begin="0"
													end="0" varStatus="counter">
													<p>
														<img src="<c:url value='${banner.imageUrl}'/>"
															width="250px" height="150" class="imgRight" />
													<p align="left"
														style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">
														${act.contentBody}</p>
													</br>
													</br>
													</br>
													</br>
													<div class="post-meta">
														<a href="<c:url value='/member/info/${act.id}'/>"
															class="btn btn-xs btn-primary pull-right">More
															info...</a>
													</div>
													</br>
													</p>
												</c:forEach>
											</div>
										</div>
									</section>
								</c:forEach>
							</div>
						</div>
					</div>

					<!-- PORK Events -->
					<div class="row" id="porkEvents" style="display: none;">
						<h2>
							Grain SA <strong> Events</strong>
						</h2>
						<div class="col-md-12">
							<ul class="history">
								<c:forEach var="act" items="${activities}" varStatus="row">
									<li data-appear-animation="fadeInUp">
										<div class="thumb">
											<img src="${act.iconUrl}" alt="" />
										</div>
										<div class="featured-box">
											<div class="box-content">
												<div class="row">
													<div class="col-md-7">
														<div class="post-content">
															<h4>${act.contentTitle}</h4>
															<br>
															<br>
															<p align="left"
																style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">
																${act.contentBody}</p>
														</div>
													</div>
													<div class="col-md-5">
														<c:forEach var="banner" items="${act.banners}" begin="0"
															end="0" varStatus="counter">
															<div class="post-image">
																<img src="<c:url value='${banner.imageUrl}'/>"
																	width="250px" class="imgRight" />
															</div>
														</c:forEach>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="post-meta">
															<span><i class="fa fa-calendar"></i>
																${act.startDate} at ${act.rangeTime}</span> <a target="_blank"
																href="<c:url value='/member/event/${act.id}'/>"
																class="btn btn-xs btn-primary pull-right">More
																info...</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>


					<!-- PORK Hey Contacts -->

					<div class="row" id="porkKeyContact" style="display: none;">
					<h2>
								Grain <strong> Forms</strong>
							</h2>
						<style>
.accordion_container {
	width: 1140px;
}

.accordion_head {
	color: #03ad5b;
	cursor: pointer;
	font-family: sans-serif;
	font-size: 16px;
	margin: 0 0 1px 0;
	padding: 10px 15px;
	font-weight: bold;
	display: block;
}

.accordion_body {
	background-color: white;
}

.accordion_body a {
	padding-right: 0px;
	padding-top: 0px;
	padding-left: 0px;
	padding-bottom: 0px;;
	font-family: sans-serif;
	font-size: 14px;
}

.accordion_body a:hover {
	text-decoration: underline;
}

.plusminus {
	float: left;
}
</style>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											SafeWork SA Codes of Practice <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">

												<ul>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113693#.VLL9bCuUd8F	'
														target="_blank">First Aid in the Workplace </a></li>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113694#.VLL92SuUd8F'
														target="_blank">Hazardous Manual Tasks </a></li>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113695#.VLL-GyuUd8F'
														target="_blank">How to Manage Work Health and Safety
															Risks </a></li>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113699#.VLMCiCuUd8F'
														target="_blank">Managing Noise and Preventing Hearing
															Loss at Work </a></li>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113700#.VLMC3CuUd8F'
														target="_blank">Managing the Risks of Plant in the
															Workplace </a></li>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113702#.VLMDFyuUd8F'
														target="_blank">Managing Electrical Risks in the
															Workplace </a></li>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113704#.VLMDMyuUd8F'
														target="_blank">Managing the Work Environment and
															Facilities </a></li>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113705#.VK94zSuUeSo'
														target="_blank">Preparation of Safety Data Sheets for
															Hazardous Chemicals </a></li>
													<li><a
														href='http://www.safework.sa.gov.au/show_page.jsp?id=113708#.VK944CuUeSo'
														target="_blank">Work Health and Safety Consultation
															Cooperation and Coordination</a></li>

												</ul>
											</div>
										</div>
									</h4>
								</div>
							</div>
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="accordion_head">
												Injury Report Forms <span class="plusminus">+</span>
											</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">

													<ul>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+Capacity+information+form+(1)++FORMS.doc'
															target="_blank">Basic template forms for reporting
																injury to doctors and medical release forms</a></li>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+WC+Letter+to+Doctor+(1)++FORMS.doc'
															target="_blank">Form for doctor </a></li>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+WC+Medical+release+authority+(1)+FORMS.doc'
															target="_blank">Medical Release form </a></li>
													</ul>
												</div>
											</div>
										</h4>
									</div>
								</div>
								<div class="panel-group">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="accordion_head">
													Work Safety Guide <span class="plusminus">+</span>
												</div>
												<div class="accordion_body" style="display: none;">
													<div class="panel-body">
														<ul>
															<li><a
																href='http://www.safework.sa.gov.au/uploaded_files/20131028_seven_steps_small_business.pdf'
																target="_blank">Seven steps for small busines</a></li>
														</ul>
													</div>
												</div>
											</h4>
										</div>
									</div>
									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<div class="accordion_head">
														Machinery and Tools <span class="plusminus">+</span>
													</div>
													<div class="accordion_body" style="display: none;">
														<div class="panel-body">

															<ul>
																<li><a
																	href="http://www.safework.sa.gov.au/uploaded_files/sgAll-terrainVehicles.pdf"
																	target="_blank">Agricultural motorbikes </a></li>
																<li><a
																	href="http://www.safework.sa.gov.au/uploaded_files/sgAngleGrinders.pdf"
																	target="_blank">Angle grinder safety </a></li>
																<li><a
																	href="https://drive.google.com/viewerng/viewer?url=http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf"
																	target="_blank">Disc grinder in the shearing
																		industry </a></li>
																<li><a
																	href="http://www.safework.sa.gov.au/uploaded_files/Take10at10Forkliftsafety.pdf"
																	target="_blank">Forklift safety </a></li>
																<li><a
																	href="http://www.safework.sa.gov.au/uploaded_files/sgBowLadders.pdf"
																	target="_blank">Ladder safety </a></li>
															</ul>
														</div>
													</div>
												</h4>
											</div>
										</div>
										<div class="panel-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
														<div class="accordion_head">
															Chemicals <span class="plusminus">+</span>
														</div>
														<div class="accordion_body" style="display: none;">
															<div class="panel-body">

																<ul>
																	<li><a
																		href="http://www.safework.sa.gov.au/show_page.jsp?id=113698#.VLMCMiuUd8F"
																		target="_blank">Labelling of Workplace Hazardous
																			Chemicals </a></li>
																	<li><a
																		href="http://www.safework.sa.gov.au/show_page.jsp?id=113701#.VLMC9SuUd8F"
																		target="_blank">Managing Risks of Hazardous
																			Chemicals in the Workplace </a></li>
																	<li><a
																		href="http://www.safework.sa.gov.au/show_page.jsp?id=112871#.VQjUeWSUdK4"
																		target="_blank">Pesticide (Chemical) safety</a></li>
																</ul>
															</div>
														</div>
													</h4>
												</div>
											</div>

											<div class="panel-group">
												<div class="panel panel-default">
													<div class="panel-heading">
														<h4 class="panel-title">
															<div class="accordion_head">
																Spray drift <span class="plusminus">+</span>
															</div>
															<div class="accordion_body" style="display: none;">
																<div class="panel-body">

																	<ul>
																		<li><a
																			href="https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockMers/RC+BIOSECURITY+Reducing_Spray_Drift_and_Damage_-_Checklist.pdf"
																			target="_blank">Reducing Spray drift and damage
																				Checklist (BioSecurity SA) </a>
																	</ul>
																</div>
															</div>
														</h4>
													</div>
												</div>
												<script>
													$(document)
															.ready(
																	function() {
																		//toggle the component with class accordion_body
																		$(
																				".accordion_head")
																				.click(
																						function() {
																							if ($(
																									'.accordion_body')
																									.is(
																											':visible')) {
																								$(
																										".accordion_body")
																										.slideUp(
																												300);
																								$(
																										".plusminus")
																										.text(
																												'+');
																							}
																							if ($(
																									this)
																									.next(
																											".accordion_body")
																									.is(
																											':visible')) {
																								$(
																										this)
																										.next(
																												".accordion_body")
																										.slideUp(
																												300);
																								$(
																										this)
																										.children(
																												".plusminus")
																										.text(
																												'+');
																							} else {
																								$(
																										this)
																										.next(
																												".accordion_body")
																										.slideDown(
																												300);
																								$(
																										this)
																										.children(
																												".plusminus")
																										.text(
																												'-');
																							}
																						});
																	});
												</script>
												<!-- --------------------------------------------------------------------------- -->

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- PORK Business -->

					<div class="row" id="porkBusiness" style="display: none;">
						<h2>
							Grain<strong> BioSecurity</strong>
						</h2>
						<p class="lead">Guidelines, checklists and links for managing
							spray drift and safe use of farm chemicals.</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											BIOSECURITY<span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">
												<h4>
													<strong>Safe Chemical Use</strong>
												</h4>
												<ul>
													<li><a
														href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Storing+and+disposing+of+chemicals+(PIRSA)+.pdf'
														target="_blank">Chemical Misuse brochure (PIRSA)</a></li>
												</ul>
												<h4>
													<strong>Spray Drift Management</strong>
												</h4>
												<ul>
													<li><a
														href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/WorkingTogether+to+Mininise+Chemical+Spray+Drift+0913+(PIRSA).pdf'
														target="_blank">Working Together to Minimise Chemical
															Spray Drift (PIRSA)</a></li>
												</ul>
											</div>
										</div>
									</h4>
								</div>
							</div>
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="accordion_head">
												Emergency Management <span class="plusminus">+</span>
											</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<h4>
														<strong>Checklists</strong>
													</h4>

													<ul>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG"
															target="_blank">Biosecurity Emergency hotlines
																(PIRSA)</a></li>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG"
															target="_blank">Heatwaves Risk (PIRSA)</a></li>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf"
															target="_blank">Personal Medical Details Checklist
																(Emergency Alert)</a></li>

													</ul>
													<h4>
														<strong>Bushfire</strong>
													</h4>
													<ul>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf"
															target="_blank">Disaster Planning - Bushfire
																Checklist (CFS)</a></li>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf"
															target="_blank">Disaster Planning - Earthquake Action
																Plan (SES)</a></li>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf"
															target="_blank">Disaster Planning - Emergency
																Contacts(SES)</a></li>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf"
															target="_blank">Disaster Planning - Extreme Heat
																Checklist(SES)</a></li>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf"
															target="_blank">Disaster Planning - Flood
																Checklist(SES)</a></li>
														<li><a
															href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf"
															target="_blank">Extreme heat Checklist (SA Health)</a></li>
														<li><a
															href="ttp://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf"
															target="_blank">Extreme Heat Guide (SA Health)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/61003298-32e8-4452-8a41-d86071e89196-Bushfire_Prevention_and_Preparedness_S2.pdf'
															target="_blank">Bushfire Prevention and Preparedness
																(PIRSA) </a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/97289472-5763-4ec0-ae30-1fe16850a2a1-CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf'
															target="_blank">Bushfire - Care of Pets and Livestock
																(CFS) </a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/d3d9575b-97b1-4d1e-acd4-520b98904157-CFS FACT SHEET - AFTER THE FIRE.pdf'
															target="_blank">Fact Sheet - After the Fire (CFS)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/0fed75f1-34bf-437a-84a0-736cac01d87c-Emergency animal disease preparedness.pdf'
															target="_blank">Emergency animal disease preparedness
																(PIRSA)</a></li>
														<li><a
															href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST%20-%20HEATWAVES%20RISK.PNG'
															target="_blank">Biosecurity Emergency hotlines
																(PIRSA)</a></li>

													</ul>



												</div>
											</div>
										</h4>
									</div>
								</div>

								<div class="panel-group">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="accordion_head">
													Land Management web links<span class="plusminus">+</span>
												</div>
												<div class="accordion_body" style="display: none;">
													<div class="panel-body">

														<ul>
															<li><a href='http://www.pir.sa.gov.au/sarms-iiip'
																target="_blank">South Australian River Murray
																	Sustainability Program (PIRSA)</a></li>
															<li><a href='http://www.pir.sa.gov.au/newhorizons'
																target="_blank">New Horizons (PIRSA)</a></li>
															<li><a
																href='http://www.daff.gov.au/about/budget/budget-2013-14/reforming-drought-programs-factsheet'
																target="_blank">National Drought Program Reform
																	Factsheet (PIRSA)</a></li>
															<li><a
																href='http://www.daff.gov.au/agriculture-food/drought'
																target="_blank">Drought Programs and Rural
																	Assistance webpage (Department of Agriculture,
																	Fisheries and Forestry)</a></li>
															<li><a
																href='http://www.sardi.sa.gov.au/climate/information_for_farmers'
																target="_blank">South Australia Research and
																	Development Institute's Climate Support program (PIRSA)</a></li>
															<li><a href='http://www.nrm.sa.gov.au/'
																target="_blank">Natural Resource Management Boards
																	(PIRSA)</a></li>
															<li><a
																href='http://www.ruralsolutions.sa.gov.au/markets/community_indigenous_services'
																target="_blank">Property management planning (PIRSA)</a></li>
															<li><a
																href='http://www.legislation.sa.gov.au/listPolicies.aspx?key=E'
																target="_blank">Burn off regulations and dates (EPA)</a></li>
															<li><a href='http://www.cfs.sa.gov.au/site/home.jsp'
																target="_blank">Fire Bans and Risk Management (EPA)</a></li>
															<li><a
																href='http://www.environment.sa.gov.au/firemanagement/home'
																target="_blank">Controlled burns on Crown (the
																	Department of Environment, Water Natural Resources)</a></li>

														</ul>
													</div>
												</div>
											</h4>
										</div>
									</div>
									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<div class="accordion_head">
														Water Management web links<span class="plusminus">+</span>
													</div>
													<div class="accordion_body" style="display: none;">
														<div class="panel-body">

															<ul>
																<li><a href='http://www.pir.sa.gov.au/sarms-iiip'
																	target="_blank">South Australian River Murray
																		Sustainability Program (PIRSA)</a></li>
																<li><a
																	href='http://www.ruralsolutions.sa.gov.au/markets/water_management'
																	target="_blank">Water management (PIRSA)</a></li>
																<li><a
																	href='http://www.legislation.sa.gov.au/index.aspx'
																	target="_blank">Dam building regulations (PIRSA)</a></li>

															</ul>
														</div>
													</div>
												</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Business -->

					<div class="row" id="porkBioSecurity" style="display: none;">
						<div class="col-sm-12">
							<h2>
								Grain <strong> Business</strong>
							</h2>
							<p class="lead">Reports, strategies and checklists to support
								farm business sustainability and growth.</p>
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="accordion_head">
												BUSINESS <span class="plusminus">+</span>
											</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">

													<ul>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Plant+Export+Operations+Stakeholder+Registration+Form+(Dept+of+Agriculture).pdf'
															target="_blank">Plant Export Operations Stakeholder
																Registration Form (Dept of Agriculture)</a></li>

														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/Farm_finance_strategy_2007.pdf'
															target="_blank">Farm Finance Strategy 2007 (SAFF)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+Guide+2015+pdf.pdf'
															target="_blank"> Farm Gross Margin Guide 2015 (GRDC)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+and+Enterprise+Planning+Guide+2012.pdf'
															target="_blank"> Farm Gross Margin and Enterprise
																Planning Guide 2012 (GRDC)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farming+the+Business+introductory+guide.pdf'
															target="_blank"> Farming the Business Introductory
																Guide (GRDC)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/How_does_my_farm_business_compare+DEWNR+doc.pdf'
															target="_blank">How does my farm business compare
																(DEWNR)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/Key+financial+ratios+for+farm+sustainability+Grains+Research+and+Development+Corp.pdf'
															target="_blank">Key financial ratios for farm
																sustainability Grains Research and Development
																Corporation (GRDC)</a></li>
														<li><a
															href='http://www.pir.sa.gov.au/consultancy/major_programs/new_horizons'
															target="_blank"> New Horizons soil and productivity
																program (PIRSA)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockbusiness/Mineral+exploration+and+faming+booklet+pdf+(Dept+State+Development).pdf'
															target="_blank">Mineral exploration and faming
																booklet (Dept State Development)</a></li>

													</ul>
												</div>
											</div>
										</h4>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Work Health Safety -->

					<div class="row" id="grainWorkContent" style="display: none;">
						<div class="col-sm-12">
							<h2>
								Grain<strong> Work Health Safety</strong>
							</h2>
							<p class="lead">SafeWork SA workplace guidelines, procedures,
								checklists and links to web pages and files for download.</p>

							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="accordion_head">
												General <span class="plusminus">+</span>
											</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<ul>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/BioSecurity+and+on-farm+workers+(Dept+of+Agri).pdf'
															target="_blank">BioSecurity and on-farm workers (Dept
																of Agri)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/Guidelines+for+entering+farms+or+animal+facilities+(Dept+of+Agriculture)+(2).pdf'
															target="_blank">Guidelines for entering farms or
																animal facilities (Dept of Agriculture)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/Guidelines+for+visiting+farms+during+disease+outbreak+(Dept+of+Agriculture)+(1).pdf'
															target="_blank"> Guidelines for visiting farms during
																disease outbreak (Dept of Agriculture)</a></li>

														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/general_whs/Farm+Safety+Fact+Sheet+(SafeWork+SA).pdf'
															target="_blank">Farm Safety Fact Sheet (SafeWork SA)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/general_whs/Children+On+Farms+Safety+Guide+(SafeWork+SA).pdf'
															target="_blank">Children on Farms Safety Guide
																(SafeWork SA)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockbusiness/2014+Agricultural+Self+Assessment+Guide+PDF.pdf'
															target="_blank"> Agricutural Self Assessment Guide
																(SafeWork SA)</a></li>

													</ul>
												</div>
											</div>
										</h4>
									</div>
								</div>
								<div class="panel-group">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="accordion_head">
													Employer obligations <span class="plusminus">+</span>
												</div>
												<div class="accordion_body" style="display: none;">
													<div class="panel-body">
														<ul>
															<li><a
																href='http://www.mers.com.au/wp-content/uploads/2013/07/Member-Update-WHS-June-2013-What-is-reasonably-practicable-in-meeting-obligations-to-ensure-the-health-and-safety-of-workers.doc'
																target="_blank">Member Update WHS June 2013
																	(SafeWork SA) (Downloadable doc)</a></li>

														</ul>
													</div>
												</div>
											</h4>
										</div>
									</div>
									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<div class="accordion_head">
														Preparing a safe workplace <span class="plusminus">+</span>
													</div>
													<div class="accordion_body" style="display: none;">
														<div class="panel-body">
															<ul>
																<li><a
																	href='http://www.safework.sa.gov.au/show_page.jsp?id=5892'
																	target="_blank">Labelling of Workplace Hazardous
																		Chemicals</a></li>
																<li><a
																	href='http://www.safework.sa.gov.au/show_page.jsp?id=113699'
																	target="_blank">Managing Noise and Preventing
																		Hearing Loss at Work</a></li>
																<li><a
																	href='http://www.safework.sa.gov.au/show_page.jsp?id=113700'
																	target="_blank">Managing the Risks of Plant in the
																		Workplace </a></li>
																<li><a
																	href='http://www.safework.sa.gov.au/show_page.jsp?id=113701'
																	target="_blank">Managing Risks of Hazardous
																		Chemicals in the Workplace</a></li>
																<li><a
																	href='http://www.safework.sa.gov.au/show_page.jsp?id=113702'
																	target="_blank">Managing Electrical Risks in the
																		Workplace </a></li>

																<li><a
																	href='http://www.safework.sa.gov.au/show_page.jsp?id=113704'
																	target="_blank">Managing the Work Environment and
																		Facilities </a></li>
																<li><a
																	href='http://www.safework.sa.gov.au/show_page.jsp?id=113705'
																	target="_blank">Preparation of Safety Data Sheets
																		for Hazardous Chemicals </a></li>
																<li><a
																	href='http://www.safework.sa.gov.au/show_page.jsp?id=113708'
																	target="_blank">Work Health and Safety Consultation
																		Cooperation and Coordination</a></li>
															</ul>
														</div>
													</div>
												</h4>
											</div>
										</div>

										<div class="panel-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
														<div class="accordion_head">
															Harvesters Code of Practice <span class="plusminus">+</span>
														</div>
														<div class="accordion_body" style="display: none;">
															<div class="panel-body">
																<ul>
																	<li><a
																		href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/harvesti/Grain_harvesting_code_of_practice+CHECKLIST+(1).pdf'
																		target="_blank">Grain Harvesting Code of Practice
																			Checklist </a></li>
																	<li><a
																		href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/harvesti/Grain_harvesting_code_of_practice_STICKER.pdf'
																		target="_blank">Grain Harvesting Code of Practice
																			Sticker </a></li>
																	<li><a
																		href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/harvesti/Harvesting+Lentils+WARNING.pdf'
																		target="_blank">Harvesting Lentils Warnings</a></li>

																</ul>
															</div>
														</div>
													</h4>
												</div>
											</div>

											<div class="panel-group">
												<div class="panel panel-default">
													<div class="panel-heading">
														<h4 class="panel-title">
															<div class="accordion_head">
																Harvesting hazard <span class="plusminus">+</span>
															</div>
															<div class="accordion_body" style="display: none;">
																<div class="panel-body">
																	<ul>
																		<li><a
																			href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Power+lines+warning+at+Harvest+time.pdf'
																			target="_blank">Power Lines warning at Harvest
																				(SafeWork SA)</a></li>

																	</ul>
																</div>
															</div>
														</h4>
													</div>
												</div>

												<div class="panel-group">
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<div class="accordion_head">
																	Safety guidelines and procedures <span
																		class="plusminus">+</span>
																</div>
																<div class="accordion_body" style="display: none;">
																	<div class="panel-body">
																		<ul>
																			<li><a
																				href='http://www.safework.sa.gov.au/show_page.jsp?id=5892'
																				target="_blank">SafeWorkSA Codes of Practice</a></li>
																			<li><a
																				href='http://www.safework.sa.gov.au/uploaded_files/CoPHazardousManualTasks.pdf'
																				target="_blank">SafeWork SA Hazardous Tasks
																					Safety </a></li>
																			<li><a
																				href='https://www.safework.sa.gov.au/uploaded_files/How_to_Manage_Work_Health_and_Safety_Risks.pdf'
																				target="_blank">Safework SA Work Safety Guide </a></li>
																			<li><a
																				href='http://www.safework.sa.gov.au/uploaded_files/youngWorkersGuide.pdf'
																				target="_blank">SafeWork SA Young Workers Guide</a></li>
																			<li><a
																				href='https://www.safework.sa.gov.au/uploaded_files/CoPFirstAidWorkplace.pdf'
																				target="_blank">SafeWork SA First Aid Workplace
																					Guide </a></li>

																			<li><a
																				href='http://www.safework.sa.gov.au/uploaded_files/sevenStepsSmallBuiness.pdf'
																				target="_blank">SafeWork SA Seven Step Safety
																					Checklist </a></li>
																			<li><a
																				href='http://www.safework.sa.gov.au/uploaded_files/notification.pdf'
																				target="_blank">SafeWork SA Notifiable Incident
																					Report Form </a></li>

																		</ul>
																	</div>
																</div>
															</h4>
														</div>
													</div>


													<div class="panel-group">
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="panel-title">
																	<div class="accordion_head">
																		Equipment and Vehicle Safety <span class="plusminus">+</span>
																	</div>
																	<div class="accordion_body" style="display: none;">
																		<div class="panel-body">
																			<ul>
																				<li><a
																					href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Forklift+Safety+(Safework+SA).pdf'
																					target="_blank">Forklift safety (SafeWork SA)</a></li>
																				<li><a
																					href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/All-terrain+Vehicles+Safety+(WorkSAfe+SA).pdf'
																					target="_blank">All- terrain Vehicles Safety
																						(SafeWork SA)</a></li>

																			</ul>
																		</div>
																	</div>
																</h4>
															</div>
														</div>


														<div class="panel-group">
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<div class="accordion_head">
																			Worker and Personal Health <span class="plusminus">+</span>
																		</div>
																		<div class="accordion_body" style="display: none;">
																			<div class="panel-body">
																				<ul>
																					<li><a
																						href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Zoonoses+Disease+Prevention+(BioSecurity+SA).pdf'
																						target="_blank">Zoonoses Disease Prevention
																							(BioSecurity SA) </a></li>
																					<li><a
																						href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Farm+Health+%26+Safety+Toolkit+for+Rural+GPs+(Australian+Centre+for+Agri+Health+%26+Safety%2C+Uni+of+Sydney).pdf'
																						target="_blank">Farm Health & Safety Toolkit
																							for Rural GPs (Australian Centre for Agri Health
																							& Safety, Uni of Sydney) </a></li>

																				</ul>
																			</div>
																		</div>
																	</h4>
																</div>
															</div>

														</div>

													</div>


												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- PORK Legal -->

					<div class="row" id="porkLegal" style="display: none;">
						<div class="col-sm-12">
							<h2>
								Grain <strong> Transport</strong>
							</h2>
							<p class="lead">Information on heavy vehicle regulations,
								vehicle escorting guidelines, driver work diaries and license
								application forms. Sourced from Government websites and
								including live links to websites for latest news and updates.</p>
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="accordion_head">
												Regulations for heavy vehicles <span class="plusminus">+</span>
											</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<ul>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf
											'
															target="_blank">Regulations for Driving Oversize or
																Overmass Agricultural Vehicles (NHVR)</a></li>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf'
															target="_blank">Regulations for Transporting
																Agricultural Vehicles as Loads (NHVR)</a></li>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf'
															target="_blank">Required documents to be carried
																(NHVR)</a></li>
														<li><a target="_blank"
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting+Guidelines+for+oversize+and+overmass+vechicles+and+loads.pdf'>Escorting
																Guidelines for Drivers and Escorting Vehicles (NHVR)</a></li>

													</ul>

												</div>
											</div>
										</h4>
									</div>
								</div>
								<div class="panel-group">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="accordion_head">
													Licence Application Forms & Driver Work Diaries <span
														class="plusminus">+</span>
												</div>
												<div class="accordion_body" style="display: none;">
													<div class="panel-body">
														<ul>

															<li><a
																href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33+Upgrade+Truck+Licence.pdf
											'
																target="_blank">Restricted Licence Application Form
																	MR 33 (DPTI) </a></li>
															<li><a
																href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf
											'
																target="_blank"> Driver Work Diary (NHVR)</a></li>
															<li><a
																href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD+201405-0028-supplementary-work-diary-record.pdf
											'
																target="_blank">Supplementary Work Diary Record
																	(NHVR) </a></li>

														</ul>
													</div>
												</div>
											</h4>
										</div>
									</div>

									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<div class="accordion_head">
														NHVR Live Weblinks <span class="plusminus">+</span>
													</div>
													<div class="accordion_body" style="display: none;">
														<div class="panel-body">
															<ul>

																<li><a
																	href='https://www.nhvr.gov.au/resources/faqs
											'
																	target="_blank">https://www.nhvr.gov.au/resources/faqs
																</a></li>
																<li><a
																	href='https://www.nhvr.gov.au/resources/forms-and-services
'
																	target="_blank">https://www.nhvr.gov.au/resources/forms-and-services
																</a></li>
																<li><a
																	href='https://www.nhvr.gov.au/news-events/stakeholder-events
											'
																	target="_blank">https://www.nhvr.gov.au/news-events/stakeholder-events
																</a></li>
																<li><a
																	href='https://www.nhvr.gov.au/resources/rss-feeds
											'
																	target="_blank">https://www.nhvr.gov.au/resources/rss-feeds

																</a></li>

															</ul>

														</div>
													</div>
												</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>




					<!-- PORK SURVEY -->

					<div class="row" id="porkSurvey" style="display: none;">
						<div class="col-sm-12">
							<h2>
								Grain<strong> Surveys</strong>
							</h2>
							<p class="lead">No survey available at this time</p>

						</div>


					</div>


					<!-- Industrial Relations -->
					<div class="row" id="grainIndustrailContent" style="display: none;">
						<div class="col-sm-12">
							<h2>
								Grain<strong> Industrial Relations</strong>
							</h2>
							<p class="lead">Links to MERS reports updates on
								superannuation, WorkCover and arrangements for different types
								of employment.</p>
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="accordion_head">
												Grain Award link <span class="plusminus">+</span>
											</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<ul>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/Lucerne-Growers-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2014.pdf'
															target="_blank">Lucerne-Growers-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading
																(MERS)</a></li>
														<li><a
															href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Grain-Producers-SA-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2013.doc'
															target="_blank">Minimum-Wage-Adjustment-SGC-and-Casual-Loading
																(MERS) (Downloadable link)</a></li>

													</ul>
												</div>
											</div>
										</h4>
									</div>
								</div>
								<div class="panel-group">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="accordion_head">
													General Employment and Information links <span
														class="plusminus">+</span>
												</div>
												<div class="accordion_body" style="display: none;">
													<div class="panel-body">
														<ul>
															<li><a
																href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/pig/Fair+Work+Information+Booklet+for+Agricultural+Producers.pdf'>Fair
																	Work Information Booklet for Agricultural Producers
																	(NFF and Dept of Agriculture)</a></li>

															<li><a
																href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Member-Update-Superannuation-May-2014.pdf'
																target="_blank">Member Updates Superannuation May
																	2014 (MERS)</a></li>
															<li><a
																href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Work-Experience.pdf'
																target="_blank">Work Experience (MERS)</a></li>

														</ul>
													</div>
												</div>
											</h4>
										</div>
									</div>

								</div>


							</div>
						</div>
					</div>

					<!-- Transport -->
					<div class="row" id="porkLegal" style="display: none;">
						<div class="col-sm-12">
							<h2>
								Grain <strong> Transport</strong>
							</h2>
							<p class="lead">Information on heavy vehicle regulations,
								vehicle escorting guidelines, driver work diaries and license
								application forms. Sourced from Government websites and
								including live links to websites for latest news and updates.</p>
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="accordion_head">
												Regulations for heavy vehicles <span class="plusminus">+</span>
											</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<ul>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf
											'
															target="_blank">Regulations for Driving Oversize or
																Overmass Agricultural Vehicles (NHVR)</a></li>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf'
															target="_blank">Regulations for Transporting
																Agricultural Vehicles as Loads (NHVR)</a></li>
														<li><a
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf'
															target="_blank">Required documents to be carried
																(NHVR)</a></li>
														<li><a target="_blank"
															href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting+Guidelines+for+oversize+and+overmass+vechicles+and+loads.pdf'>Escorting
																Guidelines for Drivers and Escorting Vehicles (NHVR)</a></li>

													</ul>

												</div>
											</div>
										</h4>
									</div>
								</div>
								<div class="panel-group">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="accordion_head">
													Licence Application Forms & Driver Work Diaries <span
														class="plusminus">+</span>
												</div>
												<div class="accordion_body" style="display: none;">
													<div class="panel-body">
														<ul>
															<li><a
																href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33+Upgrade+Truck+Licence.pdf
											'
																target="_blank">Restricted Licence Application Form
																	MR 33 </a></li>
															<li><a
																href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf
											'
																target="_blank">NHVR Driver Work Diary</a></li>
															<li><a
																href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD+201405-0028-supplementary-work-diary-record.pdf
											'
																target="_blank">Supplementary Work Diary Record </a></li>

														</ul>

													</div>
												</div>
											</h4>
										</div>
									</div>

									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<div class="accordion_head">
														NHVR Live Weblinks <span class="plusminus">+</span>
													</div>
													<div class="accordion_body" style="display: none;">
														<div class="panel-body">
															<ul>

																<li><a
																	href='https://www.nhvr.gov.au/resources/faqs
											'
																	target="_blank">https://www.nhvr.gov.au/resources/faqs
																</a></li>
																<li><a
																	href='https://www.nhvr.gov.au/resources/forms-and-services
'
																	target="_blank">https://www.nhvr.gov.au/resources/forms-and-services
																</a></li>
																<li><a
																	href='https://www.nhvr.gov.au/news-events/stakeholder-events
											'
																	target="_blank">https://www.nhvr.gov.au/news-events/stakeholder-events
																</a></li>
																<li><a
																	href='https://www.nhvr.gov.au/resources/rss-feeds
											'
																	target="_blank">https://www.nhvr.gov.au/resources/rss-feeds

																</a></li>

															</ul>

														</div>
													</div>
												</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>



					<!-- Training And Education -->
					<div class="row" id="grainTraining" style="display: none;">
						<div class="col-sm-12">
							<h2>
								Grain <strong> Training & Education</strong>
							</h2>
							<p class="lead">Links to scholarships and courses for grain
								industry.</p>
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="accordion_head">
												Links to agricultural further education courses <span
													class="plusminus">+</span>
											</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<ul>
														<li><a
															href='http://www.adelaide.edu.au/scholarships/undergrad/isolated/ '
															target="_blank">Scholarships at Adelaide University</a></li>
														<li><a
															href='http://www.flinders.edu.au/medicine/sites/nt-clinical-school/students/scholarships.cfm'
															target="_blank">Scholarships at Flinders University</a></li>
														<li><a
															href='http://www.tafesa.edu.au/apply-enrol/before-starting/scholarships-grants'
															target="_blank">Scholarships at TAFE SA</a></li>
														<li><a
															href='https://www.sa.gov.au/topics/education-skills-and-learning/financial-help-scholarships-and-grants/scholarships'
															target="_blank">Selection of SA Government
																scholarships</a></li>
													</ul>
												</div>
											</div>
										</h4>
									</div>
								</div>

								<div class="panel-group">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="accordion_head">
													Courses <span class="plusminus">+</span>
												</div>
												<div class="accordion_body" style="display: none;">
													<div class="panel-body">
														<ul>
															<li><a
																href='http://www.tafesa.edu.au/courses/searchresults?search=Short%20course%20Grain'
																target="_blank">TAFE SA</a></li>

														</ul>
													</div>
												</div>
											</h4>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
			<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
			<script src="<c:url value='/ruralAssets/js/logic.js'/>"></script>
			
			<script
				src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
			<script src="<c:url value='/ruralAssets/js/theme.js'/>"></script>

			<script src="<c:url value='/ruralAssets/js/newsRSS.js'/>"></script>
			<script src="<c:url value='/ruralAssets/js/pages/index.js'/>"></script>
			<script type="text/javascript" src="https://www.google.com/jsapi"></script>

			<script type="text/javascript">
		google.load("feeds", "1");
		google.setOnLoadCallback(newsRSS.init);
	</script>

			<script>
		$(document).ready(function() {
			$('#grainOption').addClass("active");
			Index.initGrain();
		});
	</script>
</body>
</html>

