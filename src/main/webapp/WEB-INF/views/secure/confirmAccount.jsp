<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>

<html>
<body>

	<div role="main" class="main">
		<div class="home-intro" id="home-intro">
			<div class="container">

				<div class="row">
					<div class="col-md-8">
						<p>
						 	<br>
							<strong>Handle your event requirements right here at <em>Community Connect Me</em></br> 
							Enjoy the time savings and efficiency.
							</strong>
						</p>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="<c:url value='/register'/>"
								class="btn btn-lg btn-primary">Start Connecting Now!</a> <strong><a
								href="javascript:learnMore()" style="color: #FFFFFF">or
									learn more.</a></strong>
							</p>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<br>
					<br>
						<p>
							<h1>Welcome Back <strong>${account.name}</strong>, </br>
							your account has been confirmed!!</h1>
						</p>
						
						<div class="featured-box featured-box-secundary default info-content">
							<div class="box-content">
								<h3>Now you can log in to CC-Me</h3>
								<form id="form" action="<c:url value='/j_spring_security_check'/>" method="post">
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
												<label style="text-align:left">Username or E-mail Address</label>
												<input type="text" value="" name="username" class="form-control input-lg">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
												<label style="text-align:left">Password</label>
												<input type="password" name="password" value="" class="form-control input-lg">
											</div>
										</div>
									</div>
									<br>
									<div class="row">
										<input type="submit" value="Login" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
								
					
				</div>

			</div>
		</div>
	</div>

</body>
</html>