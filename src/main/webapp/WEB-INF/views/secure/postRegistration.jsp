<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<html>
<body>

<c:url var="var" value='/postRegistrationProcess'/>
	<div role="main" class="main">
	
	  <div class="home-intro" id="home-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p>
                    </br>
                      
                </div>
                <div class="col-md-4">
                    <div class="get-started">
                        
                        
                    </div>
                </div>
            </div>

        </div>
    </div>
	
		<div class="container">
			<div class="row">
				<div class="col-md-12 center">
					<h1 class="short small">Welcome to Community Connect Me</h1>
					<h3>Are you a member of:</h3>
					 <div class="col-md-12 center">
						 <form:form id="reg" class="form-horizontal form-bordered form-bordered" method="POST" action="${var}">
						    <input type="hidden" value="${acemail}" name="email" />
                                    	<table align="center">
<!--                                     		<tr> -->
<!--                                     		<td> -->
<!--                                     			<input type="checkbox" name="checkboxGated" id="checkboxPork" class="css-checkbox" value="70521" /> -->
<!-- 												<label for="checkboxPork" class="css-label">PORK SA</label>	 -->
<!-- 												</td> -->
<!-- 												</tr> -->
												<tr>
                                    		
                                    		<td>
                                    			<input type="checkbox" name="checkboxGated" id="checkboxGrain" class="css-checkbox" value="70516"/>
												<label for="checkboxGrain" class="css-label">GRAIN PRODUCERS SA</label>
                                    		</td>
                                    		</tr>
                                    		<tr>
                                    		
                                    		<td>
                                    			<input type="checkbox" name="checkboxGated" id="checkboxLivestock" class="css-checkbox" value="70517"/>
												<label for="checkboxLivestock" class="css-label">LIVESTOCK SA</label>
                                    		</td>
                                    		</tr>
                                    		
                                    		
                                    		<tr>                                   		
                                    		<td>
                                    			<input type="checkbox" name="checkboxGated" id="checkboxH" class="css-checkbox" value="70518"/>
												<label for="checkboxH" class="css-label">HORTICULTURE COALITION OF SA</label>
												<div id='sub' style="margin-left:100px;display:none;">
													<h4>Select Association or Group:</h4>
													<input type="checkbox" name="checkboxGated" id="1" class="css-checkbox" value="120143"/>
													<label for="1" class="css-label">Adelaide Produce Market Ltd</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="2" class="css-checkbox" value="120144"/>
													<label for="2" class="css-label">Almond Board of Australia Ltd</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="3" class="css-checkbox" value="120145"/>
													<label for="3" class="css-label">Apple and Pear Growers Association Inc</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="4" class="css-checkbox" value="120146"/>
													<label for="4" class="css-label">AUSVEG SA</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="5" class="css-checkbox" value="120147"/>
													<label for="5" class="css-label">Citrus Australia (SA Region)</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="6" class="css-checkbox" value="120148"/>
													<label for="6" class="css-label">Hortex Alliance Inc</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="7" class="css-checkbox" value="120149"/>
													<label for="7" class="css-label">Australian Mushroom Growers Association</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="8" class="css-checkbox" value="120150"/>
													<label for="8" class="css-label">Nursery and Garden Industry of SA Inc</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="9" class="css-checkbox" value="120151"/>
													<label for="9" class="css-label">Olives South Australia Inc</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="10" class="css-checkbox" value="120152"/>
													<label for="10" class="css-label">Onions Australia</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="11" class="css-checkbox" value="120153"/>
													<label for="11" class="css-label">Pistachio Growers' Association Inc</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="12" class="css-checkbox" value="120154"/>
													<label for="12" class="css-label">South Australian Chamber of Fruit and Vegetables</label>
													<br>
													<input type="checkbox" name="checkboxGated" id="13" class="css-checkbox" value="120155"/>
													<label for="13" class="css-label">Women in Horticulture</label>
													
												
												
												</div>
                                    		</td>
                                    		</tr>
                                    		<tr>
                                    		
                                    		<td>
                                    			<input type="checkbox" name="checkboxGated" id="checkboxD" class="css-checkbox" value="70520"/>
												<label for="checkboxD" class="css-label">SA DAIRYFARMERS ASSOCIATION</label>
                                    		</td>
                                    		</tr>
                                    		<tr>
                                    		
                                    		<td>
                                    			<input type="checkbox" name="checkboxGated" id="checkboxD" class="css-checkbox" value="70519"/>
												<label for="checkboxD" class="css-label">WINE GRAPE COUNCIL OF SOUTH AUSTRALIA</label>
                                    		</td>
                                    		</tr>
										</table>	
										</br></br>
										 <input type="submit" value="Next"  class="btn btn-primary pull-center push-bottom" data-loading-text="Saving..."> 
		</form:form>
				</div>

				</div>
			</div>
		</div>
	</div>
	
	<script>

            $(document).ready(function () {
               $('#checkboxH').click(function(){
            	   $('#sub').toggle();
               });
            });
            
    </script>
	

</body>
</html>