<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<html>
<body>
	<div role="main" class="main">
	
	  <div class="home-intro" id="home-intro">
        <div class="container">

            <div class="row">
                <div class="col-md-8">
                	</br>
                    <p>
                        <strong>Handle your event requirements right here at <em>Community Connect Me</em></br>
                        Enjoy the time savings and efficiency. </strong>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="get-started">
                      
                    </div>
                </div>
            </div>

        </div>
    </div>
	
		<div class="container">
			<div class="row">
				<div class="col-md-12 center">
					<h1>Thank you for signing up</h1>
					<h3>What is next?</h3>
					<p>You will receive an email to your registered email address.</p>

					<p>Please enter to the link provided to activate your account
						and start enjoying our services.</p>
					<br>
					<p>Thank you.</p>
					<br> <br>
					<div class="submitbutton">
						<a class="btn btn btn-warning" href="<c:url value='/start'/>">Goto Community Connect Me Home</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>