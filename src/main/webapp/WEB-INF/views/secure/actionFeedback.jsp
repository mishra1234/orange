<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>


<html>
<head>

</head>
<body>
<c:url var="var" value='/postRegistrationProcess'/>
	<div role="main" class="main">
	
	  <div class="home-intro" id="home-intro">
        <div class="container">


            

        </div>
    </div>
    <style>
#horrizontalLine {
	margin-top:20px; 
	background-color:#B6B6B4; 
	border: 10px solid transparent;
	width:100%;
	height:80px;
} 
#logoImage {
height:100px;
width:100px; 
margin-left:30px; 
margin-top:10px;
} 
#android{
float:right;
margin-top:20px;
}
#ios{
float:right;
margin-top:20px;
margin-right:10px;
}
</style>
     <img id="logoImage" src="<c:url value='/assets/images/logo.png'/>"/>
     <a id="android" href="https://play.google.com/store/apps/details?id=com.digitalmarketsquare.ccme" style="text-decoration:none" title="Android" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/l5L9ThJrz7uSHmnP8-vnlv6Pg4vtNxtpUMqr4ViuyndAnuw_OyKi5Jup2DvsNDOh8uXL2cDiJzkIUQ5K90-AS4MZN1OEU-KA4isIFvT3odDSE_PLAv8GeXZPvtKMkuwGLEbJ3B-fBPVW=s0-d-e1-ft#http://img.agoda.net/images/emailmarketing/friday-elements/app-download-android.png" /></a>
    	<a id="ios" href="https://itunes.apple.com/au/genre/ios/id36?mt=8" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/S1g5pBAZuaOyTvLVE6270zzx0s2wcVf6eZi1PcBROU-k5dVYpWANASXDoibaRSbBeMJ8D4kWVrqRhVYJnK6A7b2R_6AIc_Lb977rFifRL7mLpvb4A5NdIfvFkNg8qvOgIjF-8qM=s0-d-e1-ft#http://img.agoda.net/images/emailmarketing/friday-elements/app-download-ios.png"></a>
    	<div id="horrizontalLine">
    	<div class="row">
                <div class="col-md-8">
                </div>
                <div class="col-md-4">
                    <div class="get-started">
                        <a href="<c:url value='/register'/>" class="btn btn-lg btn-primary">Start Connecting Now!</a>
                    </div>
                </div>
            </div>
    	</div>
			<br><br>
		<div class="container">
			<div class="row">
				<div class="col-md-4 center">
				<c:if test="${granted}">
					<img src="<c:url value='/assets/images/tick-ccme.png'/>"/>
				</c:if>
				<c:if test="${denied}">
					<img src="<c:url value='/ruralAssets/img/cancel2.png'/>"/>
				</c:if>
				</div>
				<div class="col-md-8">
					<c:if test="${granted}">
					</c:if>
					<c:if test="${denied}">
					<p><h3>${layer}:<strong><em>denied</em></strong> to</h3>
					</c:if>
					<p>Member: <strong>${account.name}</strong>
					<p>Will let the user know about his decision on
			    	<p>Email: <strong>${account.email}</strong></p>
					
					</div>
				
				
				
				

				</div>
			</div>
		</div>
	</div>

</body>
</html>