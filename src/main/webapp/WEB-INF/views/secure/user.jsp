<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html class="fixed">
<head>
	<style type="text/css"> </style>
	
	<link rel="stylesheet" href="<c:url value="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/theme.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/skins/default.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/theme-custom.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/select2/select2.css"/>">
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
</head>

<body>
 	<script src="<c:url value="/assets/javascripts/theme.js"/>"></script>
	<c:url var="var" value='/account/save'/>
	
	

	<section class="page-top">
	<br>
	    <div class="container">
	        <div class="row pull-right">
	            <div class="col-md-12">
	               <a class="btn btn-lg btn-warning" href="<c:url value='/start'/>">CC-Me Home</a>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-12">
	                <h1>Sign Up</h1>
	            </div>
	        </div>
	    </div>
	</section>

	<div class="container">
		<c:choose>
			<c:when test="${param.log == '520ce4e03480fcb95b6b6bbcf7120271'}">Error: Wrong combination username / password</c:when>
		</c:choose>
	    <div class="row">
	        <div class="col-md-12">
	            <div class="row featured-boxes login">
	           		<div class="col-sm-4">
	                    <div class="featured-box featured-box-quartenary">
	                        <div class="box-content">
	                            <h4>You're only <strong>ONE</strong> step away from:</h4>    
	                            <div class="row">  
		                            <div class="col-md-4">
		                            	<span class="arrow hrb"></span>
		                            </div>
		                            <div class="col-md-8">
		                            	<ul>
		                            		<li>Alerts</li>
		                            		<li>Guides</li>
		                            		<li>News</li>
		                            		<li>Your network</li>
		                            	</ul>
		                            </div>
			                   	</div>
		                        <div class="row center middle">  
		                        	<img src="<c:url value='/ruralAssets/img/placements/sign-up-here.png'/>" width="80%">
		                        </div>
	                         </div>       
	                     </div>
	                </div>   
	             
	                <div class="col-sm-8">
	                    <div class="featured-box featured-box-tertiary default info-content">
	                        <div class="box-content">
	                            <h4>Register</h4>
								<div class="form-group">
									<div class="col-md-12">
										<label>* Mandatory fields</label>
									</div>
								</div>
								<form:form modelAttribute="account" id="reg" class="form-horizontal form-bordered form-bordered" method="POST" action="${var}">
	                            
								    <form:input type="hidden" id="id" 		   name="id" 			path="id"/>
									<form:input type="hidden" id="role"		   name="role" 			path="role.id"/>
									<form:input type="hidden" id="salt" 	   value="no_salt" 		path="salt"/>
									<form:input type="hidden" id="resetCode"   name="resetCode" 	path="resetCode"/>
									<form:input type="hidden" id="enabled"     name="enabled" 		path="enabled"/>
									<form:input type="hidden" id="facebookUid" name="facebookUid"  	value="0"				path="facebookUid"/>
									<form:input type="hidden" id="googleUid"   name="googleUid"		value="0"				path="googleUid"/>
									<form:input type="hidden" id="status"      name="status" 	 	value="unconfirmed" 	path="status"/>
									<form:input type="hidden" id="imageUrl"    name="imageUrl" 	 	path="imageUrl"/>
									<form:input type="hidden" id="timezone"    value="+9:30" 	 	path="timezone"/>
									<form:input type="hidden" id="name"    	   value="name"			path="name"/>
									<form:input type="hidden" id="platform"    value="0"			path="platform"/>
									<form:input type="hidden" id="subscribe"    value="true"		path="subscribe"/>
									<form:input type="hidden" id="postcode"    value="5000"			path="postcode"/>          
		                            
		                            <div class="row">
	                                  <div class="form-group">
		                                  <div class="col-md-12">
		                                      <label>First Name<span class="required">*</span></label>
		                                      <form:input path="firstName"  class="form-control input-lg"  maxlength="45" type="pattern" required="required" autofocus="autofocus" placeholder="First Name" />
		                                  </div>
	                                   </div>
		                            </div>
		                               
		                            <form:errors path="*" /> 
	                                <div class="row">
	                                    <div class="form-group">
	                                        <div class="col-md-12">
	                                            <label>Surname*</label>
	                                             <form:input path="lastName" required="required" type="pattern" maxlength="45" class="form-control input-lg" placeholder="Last Name" />
	                                        </div>
	                                    </div>
	                                </div>
		                                
	                                <div class="row">
	                                    <div class="form-group">
	                                        <div class="col-md-12">
	                                            <label>E-mail Address*</label>
	                                            <form:input id="email" path="email" maxlength="128" type = "username" class="form-control input-lg" placeholder="email@email.com" required="required" />
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row">
	                                    <div class="form-group">
	                                        <div class="col-md-6">
	                                            <label>Password* (at least 6 characters long)</label>
	                                            <form:input type="password" maxlength="128" path="password" minlength="6" required="required" class="form-control input-lg"/>
	                                        </div>
	                                       
	                                    </div>
	                                </div>
		                              <!--   
	                                <div class="row">
	                                    <div class="form-group">
	                                        <div class="col-md-6">
	                                            <label>Contact phone number</label>
	                                            <form:input id="mobilephone" maxlength="40" type="contact" class="form-control input-lg" path="mobilephone" placeholder="ie 04XX XXX XXX"/>
	                                        </div>
	                                    </div>
	                                </div>
									 -->
		                            <br>
											
									<div class="row">
										<div class="col-md-12">
											<div class="form-group" >
												<input type="checkbox" id="agree" style="margin-top:50px"> I agree with Digital Market Square 
												<a href="https://s3.amazonaws.com/ccmeresources/CCMeParticipationAgreement.pdf" target="_blank">User Privacy Terms</a>  
											</div>
										</div>
									</div>
											
									<div class="row">
										<div class="col-md-12">
											<div class="form-group" >
												<br> <br> <br> <input type="submit" id="register"
													value="Register"
													class="btn btn-lg btn-success center push-bottom"
													data-loading-text="Saving..." style="margin-top:50px">
											</div>
										</div>
									</div>			
								</form:form>             
	        				</div>
	                    </div>
	                </div>
            	</div>
	        </div>
	    </div>
	</div>


	<script type="text/javascript" src="<c:url value='/assets/vendor/jquery-validation/jquery.validate.js'/>"></script>
	<script src="<c:url value='/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js'/>"></script>
	<script src="<c:url value='/assets/vendor/pnotify/pnotify.custom.js'/>"></script>
		
	 <script>
			(function() {

				'use strict';
				//console.log("Enter into Validate");

				jQuery.validator.addMethod(
								"pattern",
								function(value, element) {
									return this.optional(element)
											|| /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/
													.test(value);
								}, "Your data contain invalid characters");
				
			
				

				jQuery.validator.addMethod("postcode",
						function(value, element) {
							return isValidPostCode();
						}, "Invalid postcode, enter a 4-digit number.");

				function isValidPostCode() {

					var pLength = $('#pc').val().length;
					var postcodeVal = $('#pc').val();
					console.log(postcodeVal);
					console.log(pLength);
					if (pLength > 3 && $.isNumeric(postcodeVal))
						return true;
				};
				
				$.validator.addMethod('username',
	                     function(value, element) {
						return myValidator(value);}
	                         , "Email address already in use. Please use other email.");
				
				function myValidator(value) {
					   var isSuccess = false;

					   $.ajax({ url: '${pageContext.request.contextPath}/checkUniqueMail?email='+ encodeURIComponent(value).replace(/%20/g,'+'), 
					            data: {}, 
					            async: false, 
					            success: 
					                function(msg) { isSuccess = msg === "true" ? true : false }
					          });
					    return isSuccess;
					}

				
				$.validator.addMethod("checkbox", function(value, elem, param) {
				    if($(".roles:checkbox:checked").length > 0){
				       return true;
				   }else {
				       return false;
				   }
				},"Select at least one industry of interest.</br>");
				

				$.validator.addMethod("submit", function(value, elem, param) {
				    if($(".agree:checkbox:checked").length > 0){
				       return true;
				   }else {
				       return false;
				   }
				},"Please agree with our Termes of Service");
				
				

				//Basic
				$("#reg")
						.validate({
					            highlight : function(label) {
										$(label).closest('.row')
												.addClass('has-error');
									},
									success : function(label) {
										console.log("validation starts");
										$(label).closest('.row')
												.removeClass('has-error');
										label.remove();
									},
									errorPlacement : function(error, element) {
										var placement = element
												.closest('.input');
										if (!placement.get(0)) {
											placement = element;
										}
										if (error.text() !== '') {
											console.log(element.input);
											placement.before(error);
										}
									}
								});

			}).apply(this, [ jQuery ]);
			
			
				$( document ).ready(function() {
				
				$("#register").prop('disabled', true);
				$("#register").addClass('disabled');
				
				$("#agree").click(function(){
					if ($('#agree').is(':checked')){
						$("#register").prop('disabled', false);
						$("#register").removeClass('disabled');
					}else{
						$("#register").prop('disabled', true);
						$("#register").addClass('disabled');
					}
				});
				
			});
			
			
		</script>


</body>