<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<html>
<body>
<section class="level1  contentWidth clearBoth">

	
	<fieldset>
	<div role="main" class="main">
	
	  <div class="home-intro" id="home-intro">
        <div class="container">

            <div class="row">
                <div class="col-md-8">
                    <p>
                        Handle your farming requirements right here at <em>Rural Connect</em></br>
                        <strong>Enjoy the time savings and efficiency. </strong>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="get-started">
                      
                    </div>
                </div>
            </div>

        </div>
    </div>
	
		<div class="container">
			<div class="row">
				<div class="col-md-12 left">
					<h2>Howdy!</h2><h3 ><em>${account.name}</em></h3>
					 <h2>As a final step you need to confirm your email address.</h2>
					<p>We recently sent you a <em>Welcome</em> message with a verification link. Hotmail users: please check your spam / junk folder.</p>
					<p>
						<a class="btn btn btn-primary" href="<c:url value='/secure/resendConfirmation/${account.id}'/>">Click to re-send confirmation message</a>
					</p>
					<br>
					<p>Thank you!</p>
				</div>
			</div>
		</div>
	</div>
	</fieldset>
	</section>

</body>
</html>