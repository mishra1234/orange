<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>

<html>
<body>

<section class="level1  contentWidth clearBoth">
	<c:url var="var" value='/secure/save'/>
	<form:form modelAttribute="account" class="form-horizontal" method="POST" action="${var}">
	
	<fieldset>
	<br>
    <h1 class="form-signin-heading h6">Please complete the following:</h1>
    <br>
	<!-- Hidden ID -->
	<form:input type="hidden" id="id" 		   name="id" placeholder="id"					value=""	required="" path="id"/>
	<form:input type="hidden" id="role"		   name="role" placeholder="role"				value="" 	required="" path="role.id"/>
	<form:input type="hidden" id="salt" 	   name="salt" placeholder="salt" 				value="" 	required="" path="salt"/>
	<form:input type="hidden" id="resetCode"   name="resetCode" placeholder="resetCode" 	value="" 	required="" path="resetCode"/>
	<form:input type="hidden" id="enabled"     name="enabled" placeholder="enabled" 		value=""	required="" path="enabled"/>
	<form:input type="hidden" id="facebookUid" name="facebookUid" placeholder="facebookUid" value=""	required="" path="facebookUid"/>
	<form:input type="hidden" id="googleUid"   name="googleUid" placeholder="googleUid" 	value=""	required="" path="googleUid"/>
	<form:input type="hidden" id="status"      name="status" placeholder="status" 			value=""	required="" path="status"/>
	<form:input type="hidden" id="imageUrl"    name="imageUrl" placeholder="imageUrl" 		value=""	required="" path="imageUrl"/>
	<br>
		
	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label" for="password">Password</label>
	  <div class="controls">
	    <form:password path="password"/>
	    	<font color="red"><form:errors path="password">Password must be between 2 and 128 characters lenght</form:errors></font>
	    	<p class="help-block"></p>    
	  </div>
	</div>
	
	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label" for="name">Name</label>
	  <div class="controls">
	    <form:input id="name" name="name" type="text" placeholder="" class="input-xlarge" required="" readonly="true" cssStyle="color:grey" path="name"/>
	    	
	    	<p class="help-block"></p>    
	  </div>
	</div>
	
	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label" for="email">email</label>
	  <div class="controls">
	    <form:input id="email" name="email" type="text" placeholder="" class="input-xlarge" required="" readonly="true" cssStyle="color:grey" path="email"/>
	    	
	    	<p class="help-block"></p>
	  </div>
	</div>
	
	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label" for="timezone">timezone</label>
	  <div class="controls">
	    <form:input id="timezone" name="timezone" type="text" placeholder="" class="input-xlarge" required="" path="timezone"/> <!-- Todo: Get a list of timezones -->
	    	<font color="red"><form:errors path="timezone">Timezone cannot be empty</form:errors></font>
	    	<p class="help-block"></p>
	  </div>
	</div>
	
	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label" for="firstName">firstName</label>
	  <div class="controls">
	    <form:input id="firstName" name="firstName" type="text" placeholder="firstName" class="input-xlarge" required="" readonly="true" cssStyle="color:grey" path="firstName"/>
	    	
	    	<p class="help-block"></p>
	  </div>
	</div>
	
	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label" for="lastName">lastName</label>
	  <div class="controls">
	    <form:input id="lastName" name="lastName" type="text" placeholder="lastName" class="input-xlarge uneditable-input" required="" readonly="true" cssStyle="color:grey" path="lastName"/>
	    	
	    	<p class="help-block"></p>
	  </div>
	</div>
	
	<!-- Text input-->
	<div class="control-group">
	  <label class="control-label" for="postcode">postcode</label>
	  <div class="controls">
	    <form:input id="postcode" name="postcode" type="text" placeholder="" class="input-xlarge" required="" path="postcode"/>
	    	<font color="red"><form:errors path="postcode" cssclass="error"></form:errors></font>
	    	<p class="help-block"></p>
	  </div>
	</div>
	
	<br>
	
	<!-- Button (Double) -->
	<div class="control-group">
	  <label class="control-label" for="cancel"></label>
	  <div class="controls">
	    <a class="btn btn-medium btn-cancel" href="<c:url value='http://localhost:8080/ccme'/>">Cancel</a>
	    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
	  </div>
	</div>
	
	</fieldset>
	</form:form>
</section>
</body>
</html>