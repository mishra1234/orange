<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- start: page -->
<html>

<body>

	<div role="main" class="main">

		<section class="page-top">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1>
							<img alt="Rural Connect" height="60" src="<c:url value='/ruralAssets/img/logos/hort.jpg'/>">
							Horticulture Coalition of SA
						</h1>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="<c:url value='/register'/>"
								class="btn btn-lg btn-primary">Request Access for full Access</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="container">
			<ul class="nav nav-pills center">
			<!-- <li class="active pork" id="porkMembershipTab">	<a href="javascript:porkMembership()">About</a></li> -->
				<li class="pork" id="aboutPorkTab">				<a href="javascript:aboutPork()">News</a></li>				
				<li class="pork" id="porkCommTab">				<a href="javascript:porkComm()">Communications</a></li>
				<li class="pork" id="porkEventsTab">			<a href="javascript:porkEvents()">Events</a></li>
				<!--  
				<li class="pork" id="porkBusinesTab">			<a href="javascript:porkBusiness()">Biosecurity</a></li>
					<li class="pork" id="porkBioSecurityTab">			<a href="javascript:porkBioSecurity()">Business</a></li>
	
				<li class="pork" id="porkLegalTab">				<a href="javascript:porkLegal()">Transport</a></li>
				<li class="pork" id="porkSurveyTab">
				<li class="pork" id="porkOtherTab">				<a href="javascript:grainTraining()">Training & Education</a></li>
				<li class="pork" id="grainIndustrailTab"><a href="javascript:grainIndustrail()">Industrial Relations</a></li>
				<li class="pork" id="grainWorkTab"><a href="javascript:grainWork()">Work Health Safety </a></li> 
				-->
			</ul>

			<hr />

			<div id="porkContent">
			
					<!-- About Grain -->

<!-- 				<div class="row" id="porkMembership" > -->
<!-- 					<div class="col-sm-7"> -->
<!-- 						 <h2> -->
<!-- 							About<strong> Horticulture Coalition of SA</strong> -->
<!-- 						</h2> -->
<!-- 						<p class="lead">We welcome our many grower groups to Rural Connect – a space made for us - to see what’s happening in our industry, to share our news and to collaborate. -->

<!-- Connecting for mutual benefit and to help grow our community.</p> -->

<!-- 						<div class="rr-margin-5" id="_dvLiveLinks"> -->
<!-- 							<h4> -->
<!-- 								<strong> -->
<!-- 									<h4>Live website links and contact details:</h4> -->
<!-- 								</strong> -->
<!-- 							</h4> -->
<!-- 							<ul> -->
<!-- 								<li><a href="http://www.adelaidemarkets.com.au" data-index="0" target="_blank" data-href="#" -->
<!-- 									class="lnk2578">Adelaide Produce Market Ltd</a></li> -->
<!-- 								<li><a href="http://www.australianalmonds.com.au/" data-index="1" target="_blank" data-href="#" -->
<!-- 									class="lnk2578">Almond Board of Australia Ltd</a></li> -->
<!-- 								<li><a href="http://apal.org.au/" data-index="2" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">Apple and Pear Growers Association Inc</a></li> -->
<!-- 								<li><a href="http://ausveg.com.au/AUSVEGSA/ausvegsa.htm" data-index="3" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">AUSVEG SA</a></li> -->
<!-- 								<li><a href="http://www.citrusaustralia.com.au/latest-news/citrus-australia-sa-regional-wrap" data-index="4" target="_blank" data-href="#" -->
<!-- 									class="lnk2578">Citrus Australia (SA Region)</a></li> -->
<!-- 								<li><a href="http://www.hortexalliance.com.au/" data-index="5" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">Hortex Alliance Inc</a></li> -->
<!-- 								<li><a href="http://www.powerofmushrooms.com.au/" data-index="6" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">Australian Mushroom Growers Association</a></li> -->
<!-- 								<li><a href="http://www.ngisa.com.au/" data-index="7" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">Nursery and Garden Industry of SA Inc</a></li> -->
<!-- 								<li><a href="http://australianolives.com.au/blog-detail/olives-south-australia" target="_blank" data-index="8" data-href="#" -->
<!-- 									class="lnk2578">Olives South Australia Inc</a></li> -->
<!-- 								<li><a href="http://www.onionsaustralia.org.au/" data-index="9" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">Onions Australia</a></li> -->
<!-- 								<li><a href="http://www.pgai.com.au/" data-index="10" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">Pistachio Growers' Association Inc</a></li> -->
<!-- 								<li><a href="#" data-index="11" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">South Australian Chamber of Fruit and -->
<!-- 										Vegetable Industries</a></li> -->
<!-- 								<li><a href="http://www.growcom.com.au/projects/women-in-horticulture/ -->
<!-- 								" data-index="12" data-href="#" target="_blank" -->
<!-- 									class="lnk2578">Women in Horticulture</a></li> -->
<!-- 							</ul> -->
<!-- 						</div> -->

<!-- 					</div> -->
<!-- 					<div class="col-sm-4 col-sm-offset-1 push-top"> -->
<!-- 						<a class="btn btn-primary" -->
<!-- 							href="https://s3.amazonaws.com/ccmeresources/000RuralPDF/EAD-Risk-Management-Manual.pdf" target="_blank">Employment Info Essentials</a> </br> </br> <a class="btn btn-primary" -->
<!-- 							href="https://s3.amazonaws.com/ccmeresources/000RuralPDF/Emergency+animal+disease+preparedness.pdf" target="_blank">Guidelines for safe chemical use</a> </br> </br> -->

<!-- 					</div> -->
<!-- 					<div class="col-sm-4 col-sm-offset-1 push-top"> -->
<!-- 						<img class="img-responsive" -->
<%-- 							src="<c:url value='/ruralAssets/img/placements/horti1.jpg'/>"> --%>
<!-- 					</div> -->
					
<!-- 				</div>			 -->
			
				<div class="row" id="porkAbout">
					<div class="col-sm-7">
						<h2>
							Latest <strong>News</strong>
						</h2>
						
						<div class="widget">
                                <div class="widget-extra themed-background-dark">
                                    
                                </div>
                                <div class="widget">

													<div class="widget-extra">
														<!-- Timeline Content -->
														<div id="dvNewsRSS" class="timeline">
															
														</div>
														<!-- END Timeline Content -->
													</div>
												</div>
                            </div>
						
						</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive"
							src="<c:url value='/ruralAssets/img/placements/horti1.jpg'/>">
					</div>
				</div>

			

				

			


				<!-- Grain Communications -->

				<div class="row" id="porkComm" style="display: none;">
					<div class="col-sm-9">
						<h2>Horticulture <strong> Communications</strong></h2>	
						<hr>
						<div class="toggle" data-plugin-toggle="">
						<c:forEach var="act" items="${infos}" begin="0" end="2" varStatus="row">
							<section class="toggle">
								<label>
									<div class="img-thumbnail">
										<img class="avatar" alt="" src="<c:url value='${act.iconUrl}'/>" width="45px">
									</div>
									${act.contentTitle}
								</label>
								<div class="toggle-content" style="display: none;">
									<div class="detail" style="display: block;">
										<c:forEach var="banner" items="${act.banners}" begin="0" end="0" varStatus="counter">
											<p><img src="<c:url value='${banner.imageUrl}'/>" width="250px" height="150" class="imgRight" />
											<p align="left" style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"> 
											 ${act.contentBody}
											</p></br></br></br></br>
											<div class="post-meta"> 
												<a href="<c:url value='/member/info/${act.id}'/>" class="btn btn-xs btn-primary pull-right">More info...</a>
											</div></br>
											</p>
										</c:forEach>
									</div>
								</div>
							</section>
						</c:forEach>
						</div>
					</div>
				</div>

				<!-- Events -->
				<div class="row" id="porkEvents" style="display: none;">
					<h2>Horticulture <strong> Events</strong></h2>
					<div class="col-md-12">
						<ul class="history">
							<c:forEach var="act" items="${activities}" varStatus="row">
								<li data-appear-animation="fadeInUp">
									<div class="thumb"><img src="${act.iconUrl}" alt="" /></div>
									<div class="featured-box">
										<div class="box-content">
											<div class="row">
												<div class="col-md-7"><div class="post-content">
													<h4>${act.contentTitle}</h4><br><br>
													<p align="left" style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"> 
													 ${act.contentBody}
													</p>
												</div>
											</div>
											<div class="col-md-5">
												   <c:forEach var="banner" items="${act.banners}" begin="0" end="0" varStatus="counter">
													 <div class="post-image"><img src="<c:url value='${banner.imageUrl}'/>" width="250px" class="imgRight" /></div>
												   </c:forEach>
											</div>
											</div>
												<div class="row">
													<div class="col-md-12">
														<div class="post-meta"><span><i class="fa fa-calendar"></i> ${act.startDate} at ${act.rangeTime}</span> 
															<a href="<c:url value='/member/event/${act.id}'/>" class="btn btn-xs btn-primary pull-right">More info...</a>
									</div></div></div></div></div>
								</li>
							</c:forEach>
						</ul>
				</div></div>


				<!-- PORK Hey Contacts -->



				<!-- PORK Business -->

				<div class="row" id="porkBusiness" style="display:none;">
				<h2>
						Horticulture	<strong> BioSecurity</strong>
						</h2>
						<p class="lead">
						Guidelines, checklists and links for managing biosecurity and the safe use of farm chemicals.
						</p>
				<style>
						.accordion_container { width: 1140px; } 
						.accordion_head {  color: #03ad5b; cursor: pointer; font-family:sans-serif; font-size: 16px; margin: 0 0 1px 0; padding: 10px 15px; font-weight: bold; display:block;} 
						.accordion_body {background-color:white;} 
						.accordion_body a {padding-right:0px;padding-top:0px;padding-left:0px;padding-bottom:0px;;font-family:sans-serif;font-size:14px; } 
						.accordion_body a:hover{text-decoration:underline;}
						.plusminus { float:left; } 
						</style>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												BIOSECURITY
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									<h4><strong>Plant Health</strong></h4>	
										<ul>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/Fruit_fly_factsheet++PIRSA.pdf' target="_blank">Fruit Fly introduction (BioSecurity SA)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/Sterile_fruit_fly_release_program.pdf' target="_blank">Sterile Fruit Fly Release Program (BioSecurity SA)</a>
											
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/grain/Myrtle+Rust+Management+Plan+2012+Final+V2.pdf' target="_blank">Myrtle Rust management (BioSecurity SA)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/grain/Weed+Control+On_Property_Management_of_Branched_Broomrape_v2.pdf' target="_blank">Branched Brooomrape Management (BioSecurity SA)</a></li>
											
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Product+identification+list+(Dept+of+Agriculture).doc' target="_blank">Product identification list (Dept of Agriculture)</a>
											
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/New+Plant+Introduction+Form+(Dept+of+Agriculture).pdf' target="_blank">New Plant Introduction Form (Dept of Agriculture)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Farmers+markets+biosecurity+(Dept+of+Agri).pdf' target="_blank">Farmers markets biosecurity (Dept of Agri)</a></li>
											
											</ul>
											<h4><strong>Safe chemical use</strong></h4>			
										<ul>
											<li><a href='http://www.pir.sa.gov.au/biosecurity/rural_chemicals/storing_and_disposal_of_chemicals' target="_blank">Storage and safe use of chemicals (PIRSA) (Web link)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockMers/RC+BIOSECURITY+Reducing+harm+to+Honey+Bees+from+Pesticides.pdf' target="_blank">Reducing Harm to Honeybees from Pesticides (PIRSA) (Web link)</a>
											</ul>
											<h4><strong>Pest management</strong></h4>	
										<ul>
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Australian_Plague_Locust.pdf' target="_blank">The Australian Plague Locust (BioSecurity SA)</a></li>
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Glassy_winged_sharpshooter_-_FS.pdf' target="_blank">Glassy-winged sharpshooter (BioSecurity SA)</a>
											
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Myrtle_Rust_-_awarness_sheet.pdf' target="_blank">Exotic Plant Pest Alert (BioSecurity SA)</a></li>
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest%20Alert%20-%20European%20House%20Borer%20fact_sheet_August_2011.pdf' target="_blank"> European House Borer (BioSecurity SA)</a></li>
											
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest%20Alert%20-%20GreenSnail-Cantareusapertus.pdf' target="_blank">Pest Alert - GreenSnail-Cantareusapertus (BioSecurity SA)</a></li>
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest%20Alert%20Fire%20Ant%20PaDIL-Redimportedfireant-Solenopsisinvicta.pdf' target="_blank">Pest Alert Fire Ant PaDIL-Redimportedfireant-Solenopsisinvicta (BioSecurity SA)</a>
											
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Citrus_Canker.pdf' target="_blank">Pest Alert-Citrus Canker (BioSecurity SA)</a></li>
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Citrus_Greening.pdf' target="_blank">  Pest Alert-Citrus_Greening (BioSecurity SA)</a></li>
										
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Citrus_variegated_chlorosis_CVC.pdf' target="_blank">Pest Alert-Citrus variegated chlorosis CVC (BioSecurity SA)</a></li>
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Melon_necrotic_spot_virus.pdf' target="_blank"> Pest Alert-Melon necrotic spot virus (BioSecurity SA)</a>
											
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Pepper_Chat_Fruit_Viroid_PCFVd.pdf' target="_blank">Pest Alert-Pepper Chat Fruit Viroid PCFVd (BioSecurity SA)</a></li>
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Potato_Spindle_Tuber_Viroid.pdf' target="_blank"> Pest Alert-Potato Spindle Tuber Viroid (BioSecurity SA)</a></li>
										
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_The_Tomato_Potato_Psyllid_TPP.pdf' target="_blank"> Pest Alert-TheTomato Potato Psyllid TPP (BioSecurity SA)</a></li>
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Tomato_Red_Spider_Mite.pdf' target="_blank"> Pest Alert -Tomato Red Spider Mite (BioSecurity SA)</a>
											
											<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Zebra_Chip.pdf' target="_blank">Pest Alert-Zebra Chip (BioSecurity SA)</a></li>
																				
											</ul>
											</div>
							</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Industry Information
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
																	
											<ul>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/grain/RC+HORT+30-Year+Vision+and+Plan+-+final.pdf' target="_blank">30 Year Vision & Plan (HCSA)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/grain/RC+HORT+SECURING+FOOD+FOR+SA+2010+FINAL.pdf' target="_blank">Securing Food Production (HCSA)</a></li>
											
											</ul>
											</div>
							</div>
									</h4>
								</div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Forms
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
												<h4><strong>Plant Health</strong></h4>			
											<ul>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/grain/Guidelines+for+completing+Plant+Health+Certificates.pdf' target="_blank">Guidance for completing Plant Health Certificate (BioSecurity SA)</a></li>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/grain/ICA_LEAFLET.pdf' target="_blank">(ICA) Interstate Certification Assurance Scheme (BioSecurity SA)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/grain/INFO_to_ICA_APPLICANTS.pdf' target="_blank">Info to ICA applicants (BioSecurity SA)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/grain/CA-01-_IVCA_Operational_Procedure_Version_5_2-fixed+HORT.pdf' target="_blank">Import Verifcation Compliance Arrangement(IVCA) (BioSecurity SA)</a></li>
											<li><a href='http://www.planthealthaustralia.com.au/' target="_blank">Plant Health Australia (Web link)</a></li>
											<li><a href='http://www.agriculture.gov.au/' target="_blank">Australian Department of Agriculture (Web link)</a></li>
											<li><a href='http://www.citrusaustralia.com.au/' target="_blank">Citrus Australia (Web link)</a></li>
											<li><a href='http://www.pir.sa.gov.au/biosecurity/plant_health/exporting_commercial_plants_and_plant_products_from_south_australia/ica_and_ca_operational_procedures_and_forms' target="_blank">Interstate Certification Assurance Scheme - Guidelines and Forms (PIRSA) (Web link)</a></li>
											<li><a href='http://www.pir.sa.gov.au/biosecuritysa/planthealth/exporters/ica_and_ca_operational_procedures' target="_blank">Interstate Certification Assurance Scheme and CA Operational Procedures (Web link)</a></li>
											</ul>
													
										</div>
							</div>
									</h4>
								</div></div>
								
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Emergency Management
										<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<h4><strong>Checklists</strong></h4>			
									
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG" target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG" target="_blank">Heatwaves Risk (PIRSA)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf" target="_blank">Personal Medical Details Checklist (Emergency Alert)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/a8454d80-38e7-42b7-a6f2-c493ba2d7d97-STOCK OWNERS Risk-management-in-times-of-fire-and-flood.pdf' target="_blank">STOCK OWNERS Risk-management-in-times-of-fire-and-flood</a></li>
										
										</ul><h4><strong>Bushfire</strong></h4>			
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf" target="_blank">Disaster Planning - Bushfire Checklist (CFS)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf" target="_blank">Disaster Planning - Earthquake Action Plan (SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf" target="_blank">Disaster Planning - Emergency Contacts(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf" target="_blank">Disaster Planning - Extreme Heat Checklist(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf" target="_blank">Disaster Planning - Flood Checklist(SES)</a></li>
											<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf" target="_blank">Extreme heat Checklist (SA Health)</a></li>
												<li><a href="ttp://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf" target="_blank">Extreme Heat Guide (SA Health)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/61003298-32e8-4452-8a41-d86071e89196-Bushfire_Prevention_and_Preparedness_S2.pdf' target="_blank">Bushfire Prevention and Preparedness (PIRSA) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/97289472-5763-4ec0-ae30-1fe16850a2a1-CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf' target="_blank">Bushfire - Care of Pets and Livestock (CFS) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/d3d9575b-97b1-4d1e-acd4-520b98904157-CFS FACT SHEET - AFTER THE FIRE.pdf' target="_blank">Fact Sheet - After the Fire (CFS)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/0fed75f1-34bf-437a-84a0-736cac01d87c-Emergency animal disease preparedness.pdf' target="_blank">Emergency animal disease preparedness (PIRSA)</a></li>
												<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST%20-%20HEATWAVES%20RISK.PNG' target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												
												</ul>
											
											
										
											</div>
							</div>
									</h4>
								</div></div>
								</div></div></div></div></div>
					<script>
						$(document).ready(function () {
						    //toggle the component with class accordion_body
						    $(".accordion_head").click(function () {
						        if ($('.accordion_body').is(':visible')) {
						            $(".accordion_body").slideUp(300);
						            $(".plusminus").text('+');
						        }
						        if ($(this).next(".accordion_body").is(':visible')) {
						            $(this).next(".accordion_body").slideUp(300);
						            $(this).children(".plusminus").text('+');
						        } else {
						            $(this).next(".accordion_body").slideDown(300);
						            $(this).children(".plusminus").text('-');
						        }
						    });
						});
						
</script>
	
				<!-- PORK Legal -->

				<div class="row" id="porkLegal" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Horticulture <strong> Transport</strong>
						</h2>
						<p class="lead">
						Information on heavy vehicle regulations, vehicle escorting guidelines, driver work diaries and license application forms. Sourced from Government websites and including live links to websites for latest news and updates.
						
						</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Regulations for heavy vehicles
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf
											' target="_blank">Regulations for Driving Oversize or Overmass Agricultural Vehicles (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf' target="_blank">Regulations for Transporting Agricultural Vehicles as Loads (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf' target="_blank">Required documents to be carried (NHVR)</a></li>
											<li><a target="_blank" href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting+Guidelines+for+oversize+and+overmass+vechicles+and+loads.pdf'>Escorting Guidelines for Drivers and Escorting Vehicles (NHVR)</a></li>
						
											</ul>
										
											</div>
							</div>
									</h4></div>
								</div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Licence Application Forms & Driver Work Diaries
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
										
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33+Upgrade+Truck+Licence.pdf
											' target="_blank">Restricted Licence Application Form MR 33 (DPTI) </a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf
											' target="_blank"> Driver Work Diary (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD+201405-0028-supplementary-work-diary-record.pdf
											' target="_blank">Supplementary Work Diary Record (NHVR) </a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												NHVR Live Weblinks 
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
				
											<li><a href='https://www.nhvr.gov.au/resources/faqs
											' target="_blank">https://www.nhvr.gov.au/resources/faqs
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/forms-and-services
' target="_blank">https://www.nhvr.gov.au/resources/forms-and-services
											</a></li>
											<li><a href='https://www.nhvr.gov.au/news-events/stakeholder-events
											' target="_blank">https://www.nhvr.gov.au/news-events/stakeholder-events
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/rss-feeds
											' target="_blank">https://www.nhvr.gov.au/resources/rss-feeds

											</a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div></div></div></div>
								</div></div>
								
								
								
								<!-- Business section -->
								<div class="row" id="porkBioSecurity" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Horticulture <strong> Business</strong>
						</h2>
						<p class="lead">
						Reports, strategies and checklists to support farm business sustainability and growth.
						</p>
							<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												BUSINESS
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									
											<ul>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Plant+Export+Operations+Stakeholder+Registration+Form+(Dept+of+Agriculture).pdf' target="_blank">Plant Export Operations Stakeholder Registration Form (Dept of Agriculture)</a></li>
											
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/Farm_finance_strategy_2007.pdf' target="_blank">Farm Finance Strategy 2007 (SAFF)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+Guide+2015+pdf.pdf' target="_blank"> Farm Gross Margin Guide 2015 (GRDC)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+and+Enterprise+Planning+Guide+2012.pdf' target="_blank"> Farm Gross Margin and Enterprise Planning Guide 2012 (GRDC)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farming+the+Business+introductory+guide.pdf' target="_blank"> Farming the Business Introductory Guide (GRDC)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/How_does_my_farm_business_compare+DEWNR+doc.pdf' target="_blank">How does my farm business compare (DEWNR)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/Key+financial+ratios+for+farm+sustainability+Grains+Research+and+Development+Corp.pdf' target="_blank">Key financial ratios for farm sustainability Grains Research and Development Corporation (GRDC)</a></li>
											<li><a href='http://www.pir.sa.gov.au/consultancy/major_programs/new_horizons' target="_blank"> New Horizons soil and productivity program (PIRSA)</a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div></div></div></div>
							<!-- PORK SURVEY -->

				<div class="row" id="porkSurvey" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Horitculture<strong> Surveys</strong>
						</h2>
						<p class="lead">No survey available at this time</p>
						
					</div>
					
				
				</div>
				<!-- Work Health Safety -->
				<div class="row" id="grainWorkContent" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Horticulture<strong> Work Health Safety</strong>
						</h2>
						<p class="lead">
						SafeWork SA workplace guidelines, procedures, checklists and links to web pages and files for download.
						</p>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Employer obligations
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Member-Update-WHS-June-2013-What-is-reasonably-practicable-in-meeting-obligations-to-ensure-the-health-and-safety-of-workers.pdf' target="_blank">Member-Update-WHS-June-2013 (SafeWork SA)</a></li>
									
									</ul>
								</div></div></h4></div></div>
					
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										General Information
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/general_whs/Farm+Safety+Fact+Sheet+(SafeWork+SA).pdf' target="_blank">Farm Safety Fact Sheet (SafeWork SA)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Children+On+Farms+Safety+Guide+(SafeWork+SA).pdf' target="_blank">Children on Farms Safety Guide (SafeWork SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockbusiness/2014+Agricultural+Self+Assessment+Guide+PDF.pdf' target="_blank"> Agricutural Self Assessment Guide (SafeWork SA)</a></li>
								
								
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/BioSecurity+and+on-farm+workers+(Dept+of+Agri).pdf' target="_blank">BioSecurity and on-farm workers (Dept of Agri)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/Guidelines+for+entering+farms+or+animal+facilities+(Dept+of+Agriculture)+(2).pdf' target="_blank">Guidelines for entering farms or animal facilities (Dept of Agriculture)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/Guidelines+for+visiting+farms+during+disease+outbreak+(Dept+of+Agriculture)+(1).pdf' target="_blank"> Guidelines for visiting farms during disease outbreak (Dept of Agriculture)</a></li>
								
								
									</ul>
								</div></div></h4></div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Workplace Safety guidelines and procedures
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Power+lines+warning+at+Harvest+time.pdf' target="_blank">Power Lines warning at Harvest (SafeWork SA)</a></li>
						
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=5892' target="_blank"> Codes of Practice (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/CoPHazardousManualTasks.pdf' target="_blank"> Hazardous Tasks Safety (SafeWork SA) </a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/How_to_Manage_Work_Health_and_Safety_Risks.pdf' target="_blank"> Work Safety Guide (Safework SA) </a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/youngWorkersGuide.pdf' target="_blank"> Young Workers Guide (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/CoPFirstAidWorkplace.pdf' target="_blank"> First Aid Workplace Guide (SafeWork SA) </a></li>
								
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/sevenStepsSmallBuiness.pdf' target="_blank"> Seven Step Safety Checklist (SafeWork SA)  </a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/notification.pdf' target="_blank"> Notifiable Incident Report Form (SafeWork SA) </a></li>
									<li><a href='http://www.safework.sa.gov.au/uploaded_files/farming_community.pdf' target="_blank">Familiar Work Health and Safety principles for Farmers  (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/CoPFirstAidWorkplace.pdf' target="_blank">First Aid in the Workplace Code of Practice (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/FSCoPFirstAidWorkplace.pdf' target="_blank">First Aid in the Workplace Fact Sheet (SafeWork SA)</a></li>
							<li><a href='http://www.epa.sa.gov.au/xstd_files/Water/Other/pesticide_commercial.pdf' target="_blank">Safe and Effective Pesticide Use (EPA SA)</a></li>
						<li><a href='http://www.safework.sa.gov.au/uploaded_files/youngWorkersGuide.pdf' target="_blank">Young Worker Safety (SafeWork SA)</a></li>
								
								
									</ul>
								</div></div></h4></div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Safety preparation in the workplace 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=5892' target="_blank">Labelling of Workplace Hazardous Chemicals (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113699' target="_blank">Managing Noise and Preventing Hearing Loss at Work (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113700' target="_blank">Managing the Risks of Plant in the Workplace (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113701' target="_blank">Managing Risks of Hazardous Chemicals in the Workplace (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113702' target="_blank">Managing Electrical Risks in the Workplace (SafeWork SA)</a></li>
								
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113704' target="_blank">Managing the Work Environment and Facilities (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113705' target="_blank">Preparation of Safety Data Sheets for Hazardous Chemicals (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113708' target="_blank">Work Health and Safety Consultation Cooperation and Coordination (SafeWork SA)</a></li>
									</ul>
								</div></div></h4></div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Equipment and Vehicle Safety 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/general_whs/Forklift+Safety+(Safework+SA).pdf' target="_blank">Forklift safety (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/sgAngleGrinders.pdf' target="_blank">Angle grinder safety (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/sgEarthmovingEquipmentLoads.pdf' target="_blank">Earth Moving Equipment to lift load Safeguard (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/sgConveyors.pdf' target="_blank">Plant and Machinery Conveyor Safeguard (SafeWork SA)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Bow+Ladders+Safety+Guide+for+Fruit+Picking+(SafeWork+SA).pdf' target="_blank">Bow Ladders Safety Guide for Fruit Picking (SafeWork SA)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/All-terrain+Vehicles+Safety+(WorkSAfe+SA).pdf' target="_blank">All-terrain Vehicles Safety (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Worker and Personal Health  
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Mental+Health+Digital+Telehealth+Network+(SA+Health).pdf' target="_blank">Mental Health Digital Telehealth Network (SA Health)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Farm+Health+%26+Safety+Toolkit+for+Rural+GPs+(Australian+Centre+for+Agri+Health+%26+Safety%2C+Uni+of+Sydney).pdf' target="_blank">Farm Health & Safety Toolkit for Rural GPs (Australian Centre for Agri Health & Safety, Uni of Sydney) </a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Country+mental+health+services+(SA+Health).pdf' target="_blank">Country mental health services (SA Health) </a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Being+Active+Fact+Sheet+Nov2012+(SA+Health).pdf' target="_blank">Being Active Fact Sheet Nov2012 (SA Health) </a></li>
								
									</ul>
								</div></div></h4></div></div>
								
							</div></div>
						
					</div>
					
					
				</div></div></div></div></div>
				
				<div class="row" id="grainIndustrailContent" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Horitculture<strong> Industrial Relations</strong>
						</h2>
						<p class="lead">
						Links to MERS reports and updates on superannuation, WorkCover and arrangements for different types of employment.
						</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Horticulture Award link
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/pig/Fair+Work+Information+Booklet+for+Agricultural+Producers.pdf'>Fair Work Information Booklet for Agricultural Producers (NFF and Dept of Agriculture)</a></li>
											
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/Lucerne-Growers-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2014.pdf' target="_blank">Lucerne-Growers-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/Horticulture-Award-2010-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2014.pdf' target="_blank">Horticulture-Award-2010-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading</a></li>
									
									</ul>
								</div></div></h4></div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											General Employment and Information links <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">
												<ul>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Member-Update-Superannuation-May-2014.pdf' target="_blank">Member Updates Superannuation May 2014 (MERS)</a></li>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Work-Experience.pdf' target="_blank">Work Experience (MERS)</a></li>
										
											</ul>	
											</div>
										</div>
									</h4>
								</div>
							</div></div>
					</div>
					
					
				</div></div>

			
			<div class="row" id="grainTraining" style="display:none;">
				<div class="col-sm-12">
					<h2>Horitculture <strong> Training & Education</strong></h2>
					<p class="lead">
						Links to scholarships and courses for horticulture industry.
						</p>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Links to agricultural further education courses
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.adelaide.edu.au/scholarships/undergrad/isolated/ ' target="_blank">Scholarships at Adelaide University</a></li>
									<li><a href='http://www.flinders.edu.au/medicine/sites/nt-clinical-school/students/scholarships.cfm' target="_blank">Scholarships at Flinders University</a></li>
									<li><a href='http://www.tafesa.edu.au/apply-enrol/before-starting/scholarships-grants' target="_blank">Scholarships at TAFE SA</a></li>
									<li><a href='https://www.sa.gov.au/topics/education-skills-and-learning/financial-help-scholarships-and-grants/scholarships' target="_blank">Selection of SA Government scholarships</a></li>
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Courses
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.tafesa.edu.au/courses/agriculture-science/horticulture.aspx' target="_blank">TAFE SA</a></li>
									<li><a href='http://www.adelaide.edu.au/search/search.html?words=horticulture&handler=search&restrict=adelaide.edu.au' target="_blank">ADELAIDE UNIVERSITY</a></li>
									
									</ul>
								</div></div></h4></div></div>
					</div>
					
					</div>
					
					
					</div>
				</div>
			</div>

</div>
		</div>
	</div>

	</div>
	<script src="<c:url value='/ruralAssets/js/logic.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/theme.js'/>"></script>
	
	<script src="<c:url value='/ruralAssets/js/newsRSS.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/pages/index.js'/>"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	
	<script type="text/javascript">
		google.load("feeds", "1");
		google.setOnLoadCallback(newsRSS.init);
	</script>

	<script>
		$(document).ready(function() {
			$('#horticultureOption').addClass("active");
			Index.initHorticulture();
		});
	</script>
</body>
</html>

