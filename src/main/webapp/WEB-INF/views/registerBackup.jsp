<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html>
<body>
<section class="level1  contentWidth clearBoth">
	<h1 >Register</h1>
    <div class="crumbs" style="margin-bottom: 30px;"><a href="index.php" class="home">Home</a> &gt; Register</div>

<nav class="left">
    	<ul>
        	<li><a href="#">Home</a></li>
        </ul>
	</nav>
 
		
		<form:form modelAttribute="userCommand" method="POST" action="addMember" id="userForm">
 <div class="content">
		<h2>Register</h2>
        <p>If you wish you can add a member under the same PIC.</p>
        <p>Please complete the following form:</p>
		<form:input type="hidden" path="${user.id}"/>
        <div class="form">
            <div class="row">
                <label>First Name</label><form:input type="text" path="firstname"/>
            </div>
            <div class="row">
                <label>Surname</label><form:input type="text" path="surname"/>
            </div>
            <div class="row">
                <label>Contact</label><form:input type="text" path="phone"/>
            </div>

            <div class="row">
                <label>Email Address</label><form:input type="text" path="username"/>
            </div>

            <div class="row">
                <label>Photo</label><input value="Upload Image" type="button" class="button" />
            </div>

            <div class="row">
                <label>Password</label><form:input type="password" path="password"/>
            </div>
            <div class="row">
                <label>Confirm Password</label><input type="password" />
            </div>

            <div class="label">
                Which industries are you apart of?
            </div>
            <div class="checkboxes singlelist row">
                    <label><input type="checkbox" />Grain Grower</label>
                    <label><input type="checkbox" />Pork Farmer</label>
                    <label><input type="checkbox" />Dairy Farmer</label>
                    <label><input type="checkbox" />Liverstock Producer</label>
                    <label><input type="checkbox" />Horticulturist</label>
                    <label><input type="checkbox" />Wine Grape Grower</label>
            </div>

            <div class="label">
                Which Associations do you belong to?
            </div>
            <div class="checkboxes row">
                    <label><input type="checkbox" />GPSA</label>
                    <label><input type="checkbox" />Pork SA</label>
                    <label><input type="checkbox" />SADA</label>
                    <label><input type="checkbox" />LPSA</label>
                    <label><input type="checkbox" />HSCA</label>
                    <label><input type="checkbox" />WGCSA</label>
            </div>
            <div class="row">
                <label>PIC No</label><input type="radio" name="pic"/>Yes <form:input type="text" path="pic"/>
            </div>
    
            <div class="row">
                <label>PPSA Footy Tips</label><input type="checkbox" value="1"/>Sign me up
            </div>    
    
   
            <div class="row ">
                <label>Verification</label>17 subtract 8 =</label><input type="text" class="w100" style="margin-left: 10px;"/>
            </div>
            	

			<div class="submitbutton">
				<a href="#" class="button grey">Clear</a>            
				<input type="submit" class="button" value="Submit"/>          
			</div>
		</div>
    </div>
    </form:form>
		
         </div>
</section>
</body>
</html>