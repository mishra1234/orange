<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- start: page -->
<html>

<body>

	<div role="main" class="main">

		<section class="page-top">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1>
							<img alt="Rural Connect" height="60"
								src="<c:url value='/ruralAssets/img/icons/porkIcon.png'/>">
							PORK SA
						</h1>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="<c:url value='/register'/>"
								class="btn btn-lg btn-primary">Signup for Full Access</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="container">

			<ul class="nav nav-pills">
				
				<li class="pork" id="aboutPorkTab"><a
					href="javascript:aboutPork()">About Pork SA</a></li>
				<li class="pork" id="porkCommitteeTab"><a
					href="javascript:porkCommittee()">Pork SA Committee</a></li>
				<li class="pork" id="porkMembershipTab"><a
					href="javascript:porkMembership()">Charter</a></li>
				<li class="pork" id="porkCommTab">
				<!-- 
				<a
					href="javascript:porkComm()">Communications</a></li>
				<li class="pork" id="porkEventsTab">
				 
				
				<li class="pork" id="porkBusinesTab"><a
					href="javascript:porkBusiness()">Business</a></li>
				<li class="pork" id="porkLegalTab"><a
					href="javascript:porkLegal()">Legal</a></li>
				<li class="pork" id="porkSurveyTab"><a
					href="javascript:porkSurvey()">Surveys</a></li>
				<li class="pork" id="porkInfosTab">			<a href="javascript:porkInfos()">Info Activities</a></li>
				-->
			</ul>

			<hr />

			<div id="porkContent">
				<div class="row" id="porkAbout">
					<div class="col-sm-7">
						<h2>
							Pork <strong>SA</strong>
						</h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<p class="tall" style="text-align: justify">In 2005 â 06, the
							South Australian livestock industry contributed $2.7 billion or
							27% of South Australia's gross food revenue. The SA pig meat
							industry contributed $573 million or 21% of the total South
							Australia has about 550 pig farmers with the main pig producing
							regions the Murray lands, Barossa, Yorke, and Mid North and
							Northern. Membership of Pork SA is free to producers who pay
							levies into the Pig Industry Fund. Producer members are also able
							to nominate associate members, again with no joining fee, in the
							anticipation that farm managers and staff will join Pork SA and
							keep informed about Agribusinesses who supply and support SA pork
							producers, such as feedmillers, premix manufacturers and
							processors, can join as members upon payment of a nominal fee.

							Under the terms of incorporation, Pork SA is required to maintain
							its own register of members. Pork SA recognises that while
							profitability is critical, it is just as important to have the
							confidence of the community and consumers in how responsibly the
							industry operates in areas such as welfare, environmental
							sustainability and food safety. Pork SA will specifically address
							such matters and enable the industry to be ahead of the issues
							rather than reacting in an uncoordinated manner.</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a class="btn btn-primary"
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf">Pork
							SA Membership Form (live link)</a> </br> </br> <a class="btn btn-primary"
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf">Pork
							SA Charter (live link)</a> </br> </br> Membership forms are also available from
						Andy Pointon, Pork SA Executive Officer <a
							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
						or on 0418 848 845
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive"
							src="<c:url value='/ruralAssets/img/placements/china-pork.jpg'/>">
					</div>
				</div>

				<!-- PORK COMMITTEE -->

				<div id="porkCommittee">
					<div class="row pork">
						<div class="col-sm-7">
							<h2>
								Committee<strong> 2014</strong>
							</h2>
							<p class="lead">
							<h3>Pork SA Committee for 2014</h3>
							<ul>
								<li>Chair: Matthew Starick</li>
								<li>Vice Chairs: Andrew Johnson and Mark McLean</li>
								<li>Secretary/Treasurer: Tony Richardson</li>
								<li>Executive Committee: Barry Lloyd and Peter Brechin</li>
								<li>Committee Members: Garry Tiss, Nick Lienert, David Reu,</li>
								<li>Butch Moses, Rod Hamann, Christine Sapwell.</li>
							</ul>
							</p>
							<p class="tall" style="text-align: justify"></p>
						</div>
						<div class="col-sm-4 col-sm-offset-1 push-top">
							<a
								href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf">Pork
								SA Membership Form (live link)</a> <a
								href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf">Pork
								SA Charter (live link)</a> Membership forms are also available from
							Andy Pointon, Pork SA Executive Officer <a
								href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
							or on 0418 848 845
						</div>
						<div class="col-sm-4 col-sm-offset-1 push-top">
							<img class="img-responsive"
								src="<c:url value='/ruralAssets/img/placements/committee.jpg'/>">
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>

							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Andrew<strong> Johnson: </strong>
							</h2>
							<h4>Vice Chair</h4>
							<p style="text-align: justify;">Contact M: 0427849467 email:
								asjohnson.mtb@bigpond.com Piggery/Business profile â Director of
								Mt. Boothby Pastoral a mixed family farming operation spread
								over multiple sites in SA, producing beef, lamb, wool, mixed
								cropping, lucerne seed and have a multi-site farrow to finish
								pork production operation.Andrew holds several positions in
								industry bodies both at the state and nationally. These include:
							</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Pork SA Vice Chairman</li>
								<li><i class="fa fa-check"></i> APL Director</li>
								<li><i class="fa fa-check"></i> PPSA Councillor</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> APL Animal Welfare and
									Quality Assurance Committee Chairman</li>
								<li><i class="fa fa-check"></i> Vice Chairman Nuffield
									Australia</li>
							</ul>

						</div>
					</div>
					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Mark<strong> McLean: </strong>
							</h2>
							<h4>Vice Chair</h4>
							<p style="text-align: justify;">Contactâ M: 0427 138 919
								Email: mclean_mark@bigpond.com Piggery/Business profile - Owner
								and General Manager of Riverhaven Enterprises, 700 sow farrow to
								finish pig farm combined with horticultural interests.
								Riverhaven Enterprises is a family farm which has been operating
								in the Riverland for over 50 years. The business operates using
								principles of sustainability and resource re-use to complement
								its various operations. It composts straw-based pig shelter
								bedding which is sold externally and used on its Citrus and
								Olive orchards to provide nutrient value and increase soil water
								holding capacity to reduce water requirements. The company
								constantly review procedures and production techniques to try
								and remain competitive and achieve good outcomes for our pork
								quality. Modern housing and innovation with genetics and
								husbandry are vital to ensure long-term pig farm profitability.
								The family has been co-founders of Top Multiplier, 1,000 sow
								farrow to finish pig unit at Bower. Mark has wide industry
								interests including:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Commitment to assist in
									helping achieve a sustainable future for all Australian pig
									farmers</li>
								<li><i class="fa fa-check"></i> Commitment to see
									Australian pork consumption grow and the national industry
									remain profitable</li>
								<li><i class="fa fa-check"></i> Improving the image of
									farming and encourage future generations to participate</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> Overseas travel to
									investigate different methods of farming and food marketing *
									APL Delegate since 2010</li>
								<li><i class="fa fa-check"></i> Committee Member of APL
									Specialist Group 1 â âMarketing, Supply Chain and Product

									Qualityâ.</li>
							</ul>

						</div>
					</div>
					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Tony <strong>Richardson</strong>
							</h2>
							<h4>Secretary/Treasurer: Tony</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Barry Lloyd <strong>and Peter Brechin</strong>
							</h2>
							<h4>Executive Committee</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">Garry Tiss, Nick Lienert, David Reu,
								Butch Moses, Hamann, Christine Sapwell</h2>
							<h4>Committee Members</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Garry <strong>Tiss</strong>
							</h2>
							<h4>Committee Members</h4>
							<p style="text-align: justify;">Tiss: Contact â M: 0418817703
								Email: gtiss@bigpond.net .au Piggery/Business profile - Has
								interest in two piggeries with more than 2000 sows. T & D Pig

								Marketers (agents for Elders Ltd) specialising in marketing live
								pigs and organising contracts with processors for past 36 years.
								Particular interests of Garry include trying to bring pork

								processors and growers closer together for a better outcome for
								both parties i.e. pig pricing structures and carcase gradings.

								Butch Moses: Contact â M: 0428 64 2243 Email:
								butch.moses@internode.on.net Piggery/Business profile - Salt
								Lake Bacon. 550 sows farrow to finish (sow stall free). Butch
								has a wide interest in the industry and is involved as:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Commitment to assist in
									helping achieve a sustainable future for all Australian pig
									farmers</li>
								<li><i class="fa fa-check"></i> Top Pork Board Director</li>
								<li><i class="fa fa-check"></i> * ex SABOR Board Director</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> ex Chairman SAFF Pork
									Committee</li>
							</ul>
						</div>
					</div>


					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Rod<strong> Hamann</strong>
							</h2>
							<h4>Committee Members</h4>
							<p style="text-align: justify;">Piggery/Business profile: Rod
								is a South Australian who is a past graduate of Roseworthy

								Agricultural College. Through an initial career with Pig
								Improvement Company that took him to the UK and then a transfer
								to the USA, Rod has now lived and worked in the pig industries

								of three continents, plus had the opportunity to travel and
								consult in various capacities in South America, Europe and the
								USA. Prior to returning to Australia in 1999 to establish an Ag
								consulting business, he held senior executive positions within
								Murphy Family Farms (the largest pig producer in the world) and

								Heartland Pork Enterprises. In the latter role, as Chief
								Operating Officer he was directly responsible to the Chairman
								for all operational areas for this 60,000-sow Production and

								Genetics operation. He currently has various consulting
								positions, but has a primary role and responsibility as the

								Chief Executive Officer / Managing Director of Australian Pork
								Farms Group Limited, which includes a number of pig production
								businesses including Wasleys and Sheaoak Piggeries. The group
								has an investment interest in Big River Pork; plus they are a
								core foundation investor of the Pork CRC.Industry interests and
								positions include:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Director Auspork marketing
									group</li>
								<li><i class="fa fa-check"></i> Director Big River Pork
									Abattoir</li>
								<li><i class="fa fa-check"></i> Director Pork CRC and
									member of Pork CRC R & D Sub Committee</li>
								<li><i class="fa fa-check"></i> Director Porkscan</li>
								<li><i class="fa fa-check"></i> Member SA Pig Industry
									Advisory Group (PIAG)</li>
							</ul>
						</div>
					</div>


					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Christine <strong>Sapwell</strong>
							</h2>
							<h4>Pork SA Secretary/Treasurer:</h4>
							<p style="text-align: justify;">PContact M: 0408 800 011
								Email: Piggery/Business profile: Owner of Norsap which commenced
								in the pig breeding industry in1968. The piggery has recently
								come to an end of itâs useful life with the last pigs leaving

								the property on 26th November2012. Christine still retains an
								interest in the Australian Pork Farms Group. Christine has been
								involved in Industry organizations from the early 1970âs with
								involvement in SA Farmers Federation, Australian Pork
								Corporation, Delegate to Pork Council of Australia, Australian
								Pork Limited, Pig Industry Advisory Group (current), & the Pig
								Industry Development Board. She has also escorted a number of
								Pig Study Tours both overseas and within Australia. Christine is
								keen to see pork producers well represented to Government and
								legislators and are assisted to cope with change to enable
								survival in the Other current industry positions held by
								Christine include:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Shareholder within the
									Australian Pork Farms Group which has piggery interests on the

									Adelaide Plains and Murray Bridge areas</li>
								<li><i class="fa fa-check"></i> Treasurer of the Ronald J
									Lienert Memorial Scholarship Fund whose aim is to provide a

									bursary to a student to encourage their involvement in a
									research project</li>

							</ul>
						</div>
					</div>



				</div>

				<!-- PORK MEMBERSHIP -->

				<div class="row" id="porkMembership">
					<div class="col-sm-7">
						<h2>
							Pork<strong> Charter</strong>
						</h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<p class="tall" style="text-align: justify">
							<iframe
								src="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf"
								style="width: 100%; height: 100%;" frameborder="0"></iframe>
						</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf">Pork
							SA Membership Form (live link)</a> <a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf">Pork
							SA Charter (live link)</a> Membership forms are also available from
						Andy Pointon, Pork SA Executive Officer <a
							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
						or on 0418 848 845
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive" src="img/placements/china-pork.jpg">
					</div>
				</div>


				<!-- PORK Communications -->

				<div class="row" id="porkComm">
					<div class="col-sm-12">
						<h2>Pork<strong> Communications</strong></h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<h3>Please sign up and request membership for full access</h3>
						<img src="<c:url value='/ruralAssets/img/placements/res.jpg'/>"/>
					</div>
				</div>

				
				<!-- PORK Events -->
				<div class="row" id="porkEvents">
					<div class="col-sm-12">
						<h2>Pork<strong> Events</strong></h2>
						<h3>Please sign up and request membership for full access</h3>
						<img src="<c:url value='/ruralAssets/img/placements/res.jpg'/>"/>
					</div>
				</div>


				<!-- PORK Hey Contacts -->

				<div class="row" id="porkKeyContact">
					<div class="col-sm-7">
						<h2>
							Pork<strong> Key Contact</strong>
						</h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<p class="tall" style="text-align: justify">In 2005 â 06, the
							South Australian livestock industry contributed $2.7 billion or
							27% of South Australia's gross food revenue. The SA pig meat
							industry contributed $573 million or 21% of the total South
							Australia has about 550 pig farmers with the main pig producing
							regions the Murray lands, Barossa, Yorke, and Mid North and
							Northern. Membership of Pork SA is free to producers who pay
							levies into the Pig Industry Fund. Producer members are also able
							to nominate associate members, again with no joining fee, in the
							anticipation that farm managers and staff will join Pork SA and
							keep informed about Agribusinesses who supply and support SA pork
							producers, such as feedmillers, premix manufacturers and
							processors, can join as members upon payment of a nominal fee.

							Under the terms of incorporation, Pork SA is required to maintain
							its own register of members. Pork SA recognises that while
							profitability is critical, it is just as important to have the
							confidence of the community and consumers in how responsibly the
							industry operates in areas such as welfare, environmental
							sustainability and food safety. Pork SA will specifically address
							such matters and enable the industry to be ahead of the issues
							rather than reacting in an uncoordinated manner.</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf">Pork
							SA Membership Form (live link)</a> <a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf">Pork
							SA Charter (live link)</a> Membership forms are also available from
						Andy Pointon, Pork SA Executive Officer <a
							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
						or on 0418 848 845
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive" src="img/placements/china-pork.jpg">
					</div>
				</div>


				<!-- PORK Business -->

				<div class="row" id="porkBusiness">
				<h3>Please sign up and request membership for full access</h3>
						<img src="<c:url value='/ruralAssets/img/placements/res.jpg'/>"/>
				
	</div>
				<!-- PORK Legal -->

				<div class="row" id="porkLegal">
					<h3>Please sign up and request membership for full access</h3>
						<img src="<c:url value='/ruralAssets/img/placements/res.jpg'/>"/>
					</div>


				<!-- PORK SURVEY -->

				<div class="row" id="porkSurvey">
					<h3>Please sign up and request membership for full access</h3>
						<img src="<c:url value='/ruralAssets/img/placements/res.jpg'/>"/>
				</div>

				<!-- PORK INFOS -->
				<div class="row" id="porkInfos">
					<h3>Please sign up and request membership for full access</h3>
						<img src="<c:url value='/ruralAssets/img/placements/res.jpg'/>"/>
				</div>



			</div>


		</div>


	</div>
	<!--  <script src="<c:url value='/ruralAssets/js/logic.js'/>"></script>-->
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script
		src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>

	<script>
		$(document).ready(function() {
			$('#porkOption').addClass("active");
			$('.row').hide();
			$('#porkAbout').show();
			
			
			
		});
		
		function porkCommittee(){
			$('.pork').removeClass("active"); 
			$('#porkCommitteeTab').addClass('active');
			$('.row').hide();
			$('.pork').show();
			$('#porkCommittee').show();
		}
		
		function porkMembership(){
			$('.pork').removeClass("active"); 
			$('#porkMembershipTab').addClass('active');
			$('.row').hide();
			$('#porkMembership').show();
		}
		
		function aboutPork(){
			$('.pork').removeClass("active"); 
			$('#aboutPorkTab').addClass('active');
			$('.row').hide();
			$('#porkAbout').show();
		}
		
		
		
		
		
	</script>
</body>
</html>

