<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



	<!-- Page Title
		============================================= -->
<section id="page-title">
	<div class="container clearfix">
		<div class="row">
			<div class="col-md-3 center">
				<img src="${ev.iconUrl}" height="100" />
			</div>
			<div class="col-md-6">
				<h2>${ev.contentTitle}</h2>
				<span>Community Connect Me</span>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</section>


<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="single-event">

						<div class="col_three_fourth">
							<div class="entry-image nobottommargin">
							<c:forEach var="banner" items="${ev.banners}">
								<c:set var="url" value="${banner.imageUrl}"/>
							</c:forEach>
								<a href="#"><img src="${url}" alt="${ev.contentTitle}"></a>
								<div class="entry-overlay">
									<span class="hidden-xs">Starts in: </span><div id="event-countdown" class="countdown"></div>
								</div>
								<script>
									jQuery(document).ready( function($){
										var date = '${ev.startDate}';
										var res = date.split("/");
										
										var eventStartDate = new Date();
										eventStartDate = new Date(res[0], (res[1]-1), res[2]);
										$('#event-countdown').countdown({until: eventStartDate});
									});
							</script>
							</div>
						</div>
						<div class="col_one_fourth col_last">
							<div class="panel panel-default events-meta">
								<div class="panel-heading">
									<h3 class="panel-title">Event Info:</h3>
								</div>
								<div class="panel-body">
									<ul class="iconlist nobottommargin">
										<li><i class="icon-calendar3"></i> ${ev.startDate}</li>
										<li><i class="icon-time"></i> ${ev.rangeTime}</li>
										<li><i class="icon-phone"></i> ${ev.event.contactPhone}</li>
										<li><i class="icon-dollar"></i> <strong>${ev.event.cost} </strong></li>
									</ul>
								</div>
							</div>
							<a href="<c:url value='/event/pay/${ev.id}'/>" class="btn btn-success btn-block btn-lg">Pay Online</a>
							<a href="#" class="btn btn-default btn-block btn-lg">Book Event</a>
							<a href="#" class="btn btn-default btn-block btn-lg">Add to your Calendar</a>
						</div>

						<div class="clear"></div>

						<div class="col_three_fourth">

							<h3>Details </h3>

							<p>${ev.contentBody}</p>

							<p><strong>Contact:</strong> ${ev.event.contactName}</p>
							<p><strong>Email:</strong> ${ev.event.contactEmail}</p>
							<p><strong>Address:</strong> ${ev.event.address}</p>
							<c:if test="${fn:endsWith(videoUrls, 'mp4]')}">
							<c:set var="string3" value="${fn:substringAfter(videoUrls, '[')}"/>
							<c:set var="string4" value="${fn:substringAfter(videoIconUrls, '[')}"/>
							<p><strong>Video:</strong><a href="${fn:substringBefore(string3, ']')}"><img src="${fn:substringBefore(string4, ']')}" height="50" /></a></p>
							</c:if>
							
							<c:if test="${fn:endsWith(pdfUrls, 'pdf]')}">
							<c:set var="string1" value="${fn:substringAfter(pdfUrls, '[')}"/>
							<c:set var="string2" value="${fn:substringAfter(pdfIconUrls, '[')}"/>
							<p><strong>File:</strong><a href="${fn:substringBefore(string1, ']')}"><img src="${fn:substringBefore(string2, ']')}" height="50" /></a></p>
							</c:if>
							
							
							<!-- 
							<h4>Inclusions</h4>

							<div class="col_half nobottommargin">

								<ul class="iconlist nobottommargin">
									<li><i class="icon-ok"></i> Return Flight Tickets</li>
									<li><i class="icon-ok"></i> All Local/Airport Transfers</li>
									<li><i class="icon-ok"></i> Resort Accomodation</li>
									<li><i class="icon-ok"></i> All Meals Included</li>
									<li><i class="icon-ok"></i> Adventure Activities</li>
								</ul>

							</div>

							<div class="col_half nobottommargin col_last">

								<ul class="iconlist nobottommargin">
									<li><i class="icon-ok"></i> Games</li>
									<li><i class="icon-ok"></i> Local Guides</li>
									<li><i class="icon-ok"></i> Support Staff</li>
									<li><i class="icon-ok"></i> Personal Security</li>
									<li><i class="icon-ok"></i> VISA Fees &amp; Medical Insurance</li>
								</ul>

							</div>
 -->
						</div>
						
						

						<div class="col_one_fourth col_last">

							<h4>Location</h4>

							<section id="event-location" class="gmap" style="height: 300px;"></section>

							<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
							<script type="text/javascript" src="<c:url value='/js/jquery.gmap.js'/>"></script>

							<script type="text/javascript">

								jQuery('#event-location').gMap({

									address: '${ev.event.address}',
									maptype: 'ROADMAP',
									zoom: 15,
									markers: [
										{
											address: "${ev.event.address}"
										}
									],
									doubleclickzoom: false,
									controls: {
										panControl: true,
										zoomControl: true,
										mapTypeControl: true,
										scaleControl: false,
										streetViewControl: false,
										overviewMapControl: false
									}

								});

							</script>

						</div>

						<div class="clear"></div>
<!--
						<div class="col_two_fifth nobottommargin">

							<h4>Event Managers</h4>

							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Timings</th>
											<th>Location</th>
											<th>Events</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="label label-danger">10:00 - 12:00</span></td>
											<td>Main Auditorium</td>
											<td>WWDC Developer Conference</td>
										</tr>
										<tr>
											<td><span class="label label-danger">12:00 - 12:45</span></td>
											<td>Cafeteria</td>
											<td>Lunch</td>
										</tr>
										<tr>
											<td><span class="label label-danger">13:00 - 14:00</span></td>
											<td>Audio-Visual Lab</td>
											<td>Apple Engineers Brain-Storming &amp; Questionaire</td>
										</tr>
										<tr>
											<td><span class="label label-danger">15:00 - 18:00</span></td>
											<td>Room - 25, 2nd Floor</td>
											<td>Hardware Testing &amp; Evaluation</td>
										</tr>
									</tbody>
								</table>
							</div><!-- Event Single Gallery Thumbs End -->

						</div>

						<div class="col_three_fifth nobottommargin col_last">

							<h4>Other Dates</h4>

							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Timings</th>
											<th>Location</th>
											<th>Events</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="label label-danger">10:00 - 12:00</span></td>
											<td>Main Auditorium</td>
											<td>WWDC Developer Conference</td>
										</tr>
										<tr>
											<td><span class="label label-danger">12:00 - 12:45</span></td>
											<td>Cafeteria</td>
											<td>Lunch</td>
										</tr>
										<tr>
											<td><span class="label label-danger">13:00 - 14:00</span></td>
											<td>Audio-Visual Lab</td>
											<td>Apple Engineers Brain-Storming &amp; Questionaire</td>
										</tr>
										<tr>
											<td><span class="label label-danger">15:00 - 18:00</span></td>
											<td>Room - 25, 2nd Floor</td>
											<td>Hardware Testing &amp; Evaluation</td>
										</tr>
									</tbody>
								</table>
							</div>

						</div>
 -->
					</div>

				</div>

			</div>
			
			<!-- $.ajax
({
  type: "GET",
  url: "index1.php",
  dataType: 'json',
  async: false,
  headers: {
    "Authorization": "Basic " + btoa(USERNAME + ":" + PASSWORD)
  },
  data: '{ "comment" }',
  success: function (){
    alert('Thanks for your comment!'); 
  }
}); -->

		</section><!-- #content end -->