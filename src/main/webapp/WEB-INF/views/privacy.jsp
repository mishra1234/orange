<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html>
<html>
<head>

    <title><decorator:title default="Rural Connect"/></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

	<link rel="stylesheet" href="<c:url value='/styles/normalize.css'/>">
	<link rel="stylesheet" href="<c:url value='/styles/core.css'/>">
	<link rel="stylesheet" href="<c:url value='/styles/internal3.css'/>">
	<link rel="stylesheet" href="<c:url value='/styles/farmer.css'/>">
	<script src="<c:url value='/scripts/jquery-1.10.2.min.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.marquee.min.js'/>"></script>
	<script src="<c:url value='/scripts/generic_bits.js'/>"></script>
	
	<script src="http://cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.0/css/jquery.dataTables.css">

</head>
</head>
<body>
        <div class="contentWidth" style="position: relative;">
     <div class="homeHeadingBG">
		<div class="contentWidth homeHeading">
            <h1>For landowners who get things done</h1>
            <a href="<c:url value='/register'/>" class="bigButton">Start Connecting</a>
		</div>
    </div>
        </div>

<section class="level1  contentWidth clearBoth">
    <div id="content">
   
 <div class="contentWidth homeHow clearBoth">
		<h2> Rural Connect Privacy Policy</h2>
		 <p> <p> <p> <p> <p> <p> <p>	
        <ul>
        	<li>
            	<h3 class="icon-pen">Registered</h3>
				<p>Rural Connect is available to approved Subscribers who register and have their credentials
<p> checked with the appropriate Commodity Assicati0on or PPSA.
            </li>
        	<li>
				<h3 class="icon-report">Confidential</h3>
				<p>
All Subscriber personal information provided is kept confidential,
<p> unless volunteered by individuals as part of Forums or other open communications.
            </li>
        	<li>
            	<h3 class="icon-tick">Accredited</h3>
				<p>All bookings and transactions information is managed by approved Government or other accredited banking organisations.</p>
            </li>
             <p><p><p><p><p><p><p>
        
        </ul>
        
        <p><p><p><p><p><p><p>
        <br>
        
	</div>
 
   </section>
  
</body>
</html>


