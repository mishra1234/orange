<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>


					<!-- start: page -->
					<section class="content-with-menu mailbox">
						<div class="content-with-menu-container" data-mailbox data-mailbox-view="email">
							
							<div class="inner-body mailbox-email">
								<div class="mailbox-email-header mb-lg">
									<h3 class="mailbox-email-subject m-none text-light">
										${message.subject}
									</h3>
							
									<p class="mt-lg mb-none text-md">From <a href="#">${message.accountByFromId.name}</a> to <a href="#">You</a>, started on July, 05, 2014</p>
								</div>
								<div class="mailbox-email-container">
									<div class="mailbox-email-screen">
										<div class="panel">
											<div class="panel-heading">
												<div class="panel-actions">
													<a href="#" class="fa fa-caret-down"></a>
													<a href="#" class="fa fa-mail-reply"></a>
													<a href="#" class="fa fa-mail-reply-all"></a>
													<a href="#" class="fa fa-star-o"></a>
												</div>
							
												<p class="panel-title">${message.accountByFromId.name}<i class="fa fa-angle-right fa-fw"></i> You</p>
											</div>
											<div class="panel-body">
												<p>${message.message}</p>
											</div>
											<div class="panel-footer">
												<p class="m-none"><small>July 07, 2014. 9:51pm</small></p>
											</div>
										</div>
							
															
								
										</div>
									</div>
							
									<div class="compose">
										<div id="compose-field" class="compose">
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- end: page -->
				