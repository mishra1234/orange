<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8">
		<title>Member Page</title>
		
	</head>
	<body>
	<script type="text/javascript">
		
		$(document).ready(function(){
			$("#submitLink").click(function(){
				$("#eventForm").submit();
			});
			
		});
		</script>
	<form:form modelAttribute="event" method="POST" action="saveEvent" id="eventForm">
 <div class="content">
		<h2>New Event</h2>
        <p>You can add a new event targeting specific Association or Industry.</p>
        <p>Please complete the following form:</p>
		<form:input type="hidden" path="${user.id}"/>
        <div class="form">
            <div class="row">
                <label>Title</label><form:input type="text" path="title"/>
            </div>
            <div class="row">
                <label>Date</label><form:input type="text" path="location"/>
            </div>
            <div class="row">
                <label>Time</label><form:input type="text" path="cost"/>
            </div>

            <div class="row">
                <label>Description</label><form:input type="textarea" path="description"/>
            </div>

            <div class="row">
                <label>Banner</label><input value="Upload Image" type="button" class="button" />
            </div>

			<div class="submitbutton">
				<a href="#" class="button grey">Clear</a>            
				<input type="submit" class="button" value="Submit"/>          
			</div>
		</div>
    </div>
    </form:form>
	</body>
</html>