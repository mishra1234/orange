<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>
<body>
<br>
<h5>Community Information</h5> 
<table>
    <tr><td>Name:</td>				<td> ${comm.name}</td></tr>
   	<tr><td>Owner:</td>				<td> ${comm.account.name}</td></tr>
   	<tr><td>Icon URL:</td>			<td> <img src="${comm.iconUrl}" width="75" height="75"></td></tr>
	<tr><td>Recommended:</td>		<td> ${comm.recommended}</td></tr>
	<tr><td>Connectable:</td>		<td> ${comm.connectable}</td></tr>
	<tr><td>Parent Community:</td>	<td>
										 <c:forEach var="ownerId" items="${comm.layersForParentLayerId}" varStatus="status">${ownerId.name}
										 </c:forEach></td></tr>
    <tr><td>Status:			</td>	<td> ${comm.status}</td></tr>
    <tr><td>Type:			</td>	<td> ${comm.type}</td></tr>	
</table>

 <h5>  <a href="<c:url value='/manager/community/editCommunity/${comm.id}'/>">Edit Community</a> </h5>
</body>

</html>