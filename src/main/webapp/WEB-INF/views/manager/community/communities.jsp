<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<body>

<script>$(document).ready(function(){$('#communityTable').dataTable({"paging":false,"info":false,"order":[[1,"asc"]]});});</script>

<h4>Communities List</h4> 
 <h5> <a href ="<c:url value='/manager/community/new/'/>">Add new</a> </h5>
	<table id="communityTable">
		<thead>
			<tr>
				<th>Icon</th>			 
				<th>Name</th>		 
				<th>Owner</th>
				<th>Parent Community</th>
				<th># Users</th>
				<th># Activities</th>
				<th>Type</th>
				<th>   Options        </th>
			</tr>
		</thead>
<tbody>
	<c:forEach var="community" items="${myCommunityList}" varStatus="row">
		<tr>
			<td><a href= "<c:url value='/manager/events/community/${community.id}'/>"><img src="${community.iconUrl}" width="25" height="25" onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
			<td>${community.name}</td>
			<td>${community.account.name}</td>
			<td><c:forEach var="parent" items="${community.layersForParentLayerId}"  varStatus="status">${parent.name}</c:forEach></td>
			<td><a href= "<c:url value='/manager/users/community/${community.id}'/>">	${fn:length(community.accounts_1)}</a></td>
			<td><a href= "<c:url value='/manager/events/community/${community.id}'/>">	${fn:length(community.activities)}</a></td>
			<td>${community.type}</td>
            <td> 
               	<a href="<c:url value='/manager/community/delete/${community.id}'/>"> <i class=" icon-remove"></i> </a> 
                <a href="<c:url value='/manager/community/editCommunity/${community.id}'/>"> <i class="icon-wrench"></i> </a> 
                <a href="<c:url value='/manager/community/${community.id}'/>"> <i class="icon-tasks"></i></a>
            </td>
		</tr>
	</c:forEach>
	
</tbody>
</table>
</body>
</html>