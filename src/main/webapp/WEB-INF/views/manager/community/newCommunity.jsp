<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<style>
	    #sync1 .item{
	    	background: #0c83e7;
	        padding: 80px 0px;
	        margin: 5px;
	        color: #FFF;
	        -webkit-border-radius: 3px;
	        -moz-border-radius: 3px;
	        border-radius: 3px;
	        text-align: center;
	    }
	    #sync2 .item{
	        background: #C9C9C9;
	        padding: 2px 0px;
	        margin: 2px;
	        color: #FFF;
	        -webkit-border-radius: 2px;
	        -moz-border-radius: 2px;
	        border-radius: 2px;
	        text-align: center;
	        cursor: pointer;
	    }
	    #sync2 .item h1{
	      font-size: 18px;
	    }
	    #sync2 .synced .item{
	      background: #0c83e7;
	    }
    </style>
</head>
<body>
<c:url var="var" value='/manager/community/save'/>
<form:form modelAttribute="community" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<!-- Form Name -->
<legend>New Community </legend>

<!-- Hidden ID -->
<form:input type="hidden" path="id" value=""/>
<form:input type="hidden" path="iconUrl" value=""/>
<form:input type="hidden" id="deleted" name="deleted" placeholder="deleted" class="input-xlarge" required="" path="deleted" value="false"/>
<form:input type="hidden" id="createdDatetime" name="createdDatetime" placeholder="createdDatetime" path="createdDatetime" value="${currentTS}"/>
<form:input type="hidden" id="createdAccountId" name="createdAccountId" placeholder="createdAccountId" path="createdAccountId" value="${currentUser}"/>
<input 		type="hidden" id="inputId" name="icon" value="${inputId}"/>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="Name">Name</label>
  <div class="controls">
    <form:input id="Name" name="name" type="text" placeholder="Community Name" class="input-xlarge" required="" path="name"/>
    <font color="red">
    	<form:errors path="name" >Please verify the field is not empty</form:errors>
    </font>
  </div>
</div>

<!-- Select from list -->
<div class="control-group">
  <label class="control-label" for="iconUrl">Icon selected</label>
	<div id="iconUrl">
        <div class="controls">
          <div class="row">
            <div class="span12">
              <div id="sync1" class="owl-carousel">
                	<c:forEach var="bann" items="${listOfIcons}" varStatus="row">
  						<img src="${bann.iconUrl}"/>
  					</c:forEach>
  			  </div>
  			  <div id="sync2" class="owl-carousel">
                	<c:forEach var="bann" items="${listOfIcons}" varStatus="row">
  						<img src="${bann.iconUrl}"/>
  					</c:forEach>
  			  </div>
            </div>
          </div>
        </div>
    </div>
</div>
</br>

<!-- Select Multiple -->
<div class="control-group">
  <label class="control-label" for="account">Community Owner</label>
  <div class="controls">
    <form:select id="account" name="account" class="input-xlarge" path="account.id">
      	<form:options items="${listOfOwners}" itemValue="id" itemLabel="name"/>
    </form:select>
  </div>
</div>

<!-- Select Multiple -->
<div class="control-group">
  <label  style="display:block; margin-bottom:10px; margin-left:35px">Parent Communities</label>
 	<ul class="checkbox-grid" style="display:block" >
 	<c:forEach items="${listOfCommunities}" var="parent">
 		<c:set var="x" value="false"/> 
    		<c:forEach items="${community.layersForParentLayerId}" var="selected">
    		<c:choose>
	    		<c:when test="${selected.id == parent.id}">
	    			<c:set var="x" value="true"/> 
	    		</c:when>
    		</c:choose>
    	 	</c:forEach>
    	 	<c:choose>
	    		<c:when test="${x}">
    	<li><input id="parent" type="checkbox" name="parent" value="${parent.id}" checked="checked"/><label for="text1">${parent.name}</label></li>
	    	</c:when>
		    		<c:otherwise>
		<li><input type="checkbox" name="parent" value="${parent.id}" /><label for="text1">${parent.name}</label></li>
		    		</c:otherwise>
		      </c:choose>
    </c:forEach>
    </ul>

  </div>
  
<!-- Radio Buttons-->
<fieldset>
   <div class="control-group">
    <label class="control-xs-3 control-label">Status ?  </label>
    <div class="controls">
        <div class="radio-inline"><form:radiobutton value="draft" path="status"/>draft</div> 
        <div class="radio-inline"><form:radiobutton value="active"  path="status"/>active</div>
        <div class="radio-inline"><form:radiobutton value="hidden"  path="status"/>Hidden</div>
    </div>
   </div>
</fieldset>


<!-- Radio Buttons-->
<fieldset>
   <div class="control-group">
    <label class="control-xs-3 control-label">Type ?  </label>
    <div class="controls">
        <div class="radio-inline"><form:radiobutton value="gated" path="type"/>Gated</div> 
        <div class="radio-inline"><form:radiobutton value="public"  path="type"/>Public</div>
    </div>
   </div>
</fieldset>

<!-- Radio Buttons-->
<fieldset>
   <div class="control-group">
    <label class="control-xs-3 control-label">Connectable ?</label>
    <div class="controls">
        <div class="radio-inline"><form:radiobutton value="false" path="connectable"/>No</div> 
        <div class="radio-inline"><form:radiobutton value="true"  path="connectable"/>Yes</div>
    </div>
   </div>
</fieldset>

<!-- Radio Buttons-->
<fieldset>
   <div class="control-group">
    <label class="control-xs-3 control-label">Recommended ?</label>
    <div class="controls">
        <div class="radio-inline"><form:radiobutton value="false" path="recommended"/> No </div>
    	<div class="radio-inline"><form:radiobutton value="true" path="recommended"/> Yes </div>
    </div>
    </div>
</fieldset>

<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
    <a class="btn btn-medium btn-cancel" href="<c:url value='/manager/communities'/>">Cancel</a>
    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
  </div>
</div>



<!---- SCRIPTS ---->
 <script>
    $(document).ready(function() {
        var sync1  = $("#sync1");
        var status = $("#owlStatus");
        var sync2  = $("#sync2");
        var iconUrl  = ${iUrl};
        
        sync1.owlCarousel({
          singleItem:true,
          items:1,
          margin:10,
          autoHeight:true,
          itemsMobile : true,
          slideSpeed : 100,
          navigation: true,
          pagination:false,
          afterAction : syncPosition,
          responsiveRefreshRate : 200,
        });
        sync1.trigger("owl.goTo",iconUrl);  <!-- This function sets the current community icon in the list of icons - Big Image/carousel1-->
        
        sync2.owlCarousel({
          items : 8,
          itemsDesktop      : [1000,8],
          itemsDesktopSmall : [979,6],
          itemsTablet       : [768,4],
          itemsMobile       : [479,2],
          pagination:false,
          responsiveRefreshRate : 100,
          beforeInit : function(el){
              el.find(".owl-item").eq(0).addClass("synced");
            }
        });
        sync2.trigger("owl.goTo",iconUrl);  <!-- This function sets the current community icon in the list of icons - Big Image/carousel1-->
        
        function updateResult(pos,value){
            status.find(pos).find(".result").text(value);
            document.getElementById('inputId').value =(value);
            document.getElementById('formId').checked = true;
          }
        function afterAction(){
            updateResult(".currentItem", this.sync1.currentItem);
          }
        function syncPosition(el){
          var current = this.currentItem;
          $("#sync2")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")
          if($("#sync2").data("owlCarousel") !== undefined){
            center(current)
          }
        }
        $("#sync2").on("click", ".owl-item", function(e){
          e.preventDefault();
          var number = $(this).data("owlItem");
          sync1.trigger("owl.goTo",number);
          updateResult(".currentItem", number);
        });
        function center(number){
          var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
          var num = number;
          var found = false;
          for(var i in sync2visible){
            if(num === sync2visible[i]){
              	var found = true;
            }
          }
          if(found===false){
            	if(num>sync2visible[sync2visible.length-1]){
              		sync2.trigger("owl.goTo", num - sync2visible.length+10)
            	}else{
              		if(num - 1 === -1){
                		num = 0;
              		}
              	sync2.trigger("owl.goTo", num);
            	}
          }else if(num === sync2visible[sync2visible.length-10]){
            	sync2.trigger("owl.goTo", sync2visible[10])
          }else if(num === sync2visible[0]){
            	sync2.trigger("owl.goTo", num-1)
          }
        }
      });
    </script>
    
	<script src="http://owlgraphic.com/owlcarousel/assets/js/jquery-1.9.1.min.js"></script> 
    <script src="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.js"></script>
    <link href="http://owlgraphic.com/owlcarousel/assets/css/custom.css" rel="stylesheet">
    <link href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.theme.css" rel="stylesheet">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="http://owlgraphic.com/owlcarousel/assets/ico/favicon.png">

    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-collapse.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-transition.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-tab.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/google-code-prettify/prettify.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/application.js"></script>

</fieldset>
</form:form>
	
</body>
</html>