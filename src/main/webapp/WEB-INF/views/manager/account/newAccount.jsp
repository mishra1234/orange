<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<style>
	    #sync1 .item{
	    	background: #0c83e7;
	        padding: 80px 0px;
	        margin: 5px;
	        color: #FFF;
	        -webkit-border-radius: 3px;
	        -moz-border-radius: 3px;
	        border-radius: 3px;
	        text-align: center;
	    }
	    #sync2 .item{
	        background: #C9C9C9;
	        padding: 2px 0px;
	        margin: 2px;
	        color: #FFF;
	        -webkit-border-radius: 2px;
	        -moz-border-radius: 2px;
	        border-radius: 2px;
	        text-align: center;
	        cursor: pointer;
	    }
	    #sync2 .item h1{
	      font-size: 18px;
	    }
	    #sync2 .synced .item{
	      background: #0c83e7;
	    }
    </style>     	
</head>

<body>

<c:url var="var" value='/manager/account/save'/>
<form:form modelAttribute="account" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<!-- Form Name -->
<legend>New Account</legend>

<!-- Hidden ID -->
<form:input type="hidden" id="id" 		   path="id" value=""/>
<form:input type="hidden" id="password"    path="password" value=""/>
<form:input type="hidden" id="salt" 	   name="salt" placeholder="salt" class="input-xlarge" path="salt"/>
<form:input type="hidden" id="resetCode"   name="resetCode" placeholder="resetCode" class="input-xlarge" required="" path="resetCode"/>
<form:input type="hidden" id="enabled"     name="resetCode" placeholder="resetCode" class="input-xlarge" required="" path="enabled" value="true"/>
<form:input type="hidden" id="facebookUid" name="facebookUid" placeholder="facebookUid" class="input-xlarge" path="facebookUid" value="0"/>
<form:input type="hidden" id="googleUid"   name="googleUid" placeholder="googleUid" class="input-xlarge" path="googleUid" value="0"/>
<form:input type="hidden" id="imageUrl"    name="imageUrl" placeholder="imageUrl" value="" path="imageUrl"/>
<input 		type="hidden" id="inputId"     name="icon" value="${inputId}"/>


<!-- Select from list -->
<div class="control-group">
  <label class="control-label" for="iconUrl">Icon selected</label>
	<div id="iconUrl">
        <div class="controls">
          <div class="row">
            <div class="span12">
              <div id="sync1" class="owl-carousel">
                	<c:forEach var="bann" items="${listOfIcons}" varStatus="row">
  						<img src="${bann.iconUrl}"/>
  					</c:forEach>
  			  </div>
  			  <div id="sync2" class="owl-carousel">
                	<c:forEach var="bann" items="${listOfIcons}" varStatus="row">
  						<img src="${bann.iconUrl}"/>
  					</c:forEach>
  			  </div>
            </div>
          </div>
        </div>
    </div>
</div>
</br>

<!-- Select Multiple -->
<div class="control-group">
  <label class="control-label" for="role">Role</label>
  <div class="controls">
    <form:select id="role" name="role" class="input-xlarge" multiple="multiple" path="role.id">
     <form:option value="" label="--Select one Role--" />
     <form:options items="${listOfRoles}"  itemValue="id" itemLabel="name" ></form:options>
    </form:select>
    <p class="help-block">Select one Role</p>
    <font color="red">
    	<form:errors path="role" >It is required to select one role</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="subject">name</label>
  <div class="controls">
    <form:input id="name" name="name" type="text" placeholder="name" class="input-xlarge" required="" path="name"/>
    <p class="help-block">name</p>
    <font color="red">
    	<form:errors path="name">Name must be between 2 and 128 characters lenght</form:errors>
    </font>    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="email">email</label>
  <div class="controls">
    <form:input id="email" name="email" type="text" placeholder="email" class="input-xlarge" required="" path="email"/>
    <p class="help-block">email</p>
    <font color="red">
    	<form:errors path="email">Please verify email address, must not be empty</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="timezone">timezone</label>
  <div class="controls">
    <form:input id="timezone" name="timezone" type="text" placeholder="timezone" class="input-xlarge" required="" path="timezone"/> <!-- Todo: Get a list of timezones -->
    <p class="help-block">timezone</p>
    <font color="red">
    	<form:errors path="timezone">Please verify, it may not be empty</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="firstName">firstName</label>
  <div class="controls">
    <form:input id="firstName" name="firstName" type="text" placeholder="firstName" class="input-xlarge" required="" path="firstName"/>
    <p class="help-block">firstName</p>
    <font color="red">
    	<form:errors path="firstName">Please verify, it may not be empty</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="lastName">lastName</label>
  <div class="controls">
    <form:input id="lastName" name="lastName" type="text" placeholder="lastName" class="input-xlarge" required="" path="lastName"/>
    <p class="help-block">lastName</p>
    <font color="red">
    	<form:errors path="lastName">Please verify, it may not be empty</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="postcode">postcode</label>
  <div class="controls">
    <form:input id="postcode" name="postcode" type="text" placeholder="postcode" class="input-xlarge" required="" path="postcode"/>
    <p class="help-block">Postcode</p>
    <font color="red">
   		<form:errors path="postcode" cssclass="error"></form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="status">status</label>
  <div class="controls">
    <form:input id="status" name="status" type="text" placeholder="status" class="input-xlarge" required="" path="status"/> <!--  todo: list -->
    <p class="help-block">status can be either active or unconfirmed</p>
    <font color="red">
    	<form:errors path="status">Please verify status, it may not be empty</form:errors>
    </font>
  </div>
</div>
<br>

<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
    <a class="btn btn-medium btn-cancel" href="<c:url value='/manager/accounts'/>">Cancel</a>
    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
  </div>
</div>

<!---- SCRIPTS ---->
<script>
    $(document).ready(function() {
        var sync1  = $("#sync1");
        var status = $("#owlStatus");
        var sync2  = $("#sync2");
        var iconUrl  = ${iUrl};
        
        sync1.owlCarousel({
          singleItem:true,
          items:1,
          margin:10,
          autoHeight:true,
          itemsMobile : true,
          slideSpeed : 100,
          navigation: true,
          pagination:false,
          afterAction : syncPosition,
          responsiveRefreshRate : 200,
        });
        sync1.trigger("owl.goTo",iconUrl);  <!-- This function sets the current community icon in the list of icons - Big Image/carousel1-->
        
        sync2.owlCarousel({
          items : 8,
          itemsDesktop      : [1000,8],
          itemsDesktopSmall : [979,6],
          itemsTablet       : [768,4],
          itemsMobile       : [479,2],
          pagination:false,
          responsiveRefreshRate : 100,
          afterInit : function(el){
            el.find(".owl-item").eq(0).addClass("synced");
          }
        });
        sync2.trigger("owl.goTo",iconUrl);  <!-- This function sets the current community icon in the list of icons - Big Image/carousel1-->
        
        function updateResult(pos,value){
            status.find(pos).find(".result").text(value);
            document.getElementById('inputId').value =(value);
            document.getElementById('formId').checked = true;
          }
        function afterAction(){
            updateResult(".currentItem", this.sync1.currentItem);
          }
        function syncPosition(el){
          var current = this.currentItem;
          $("#sync2")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")
          if($("#sync2").data("owlCarousel") !== undefined){
            center(current)
          }
        }
        $("#sync2").on("click", ".owl-item", function(e){
          e.preventDefault();
          var number = $(this).data("owlItem");
          sync1.trigger("owl.goTo",number);
          updateResult(".currentItem", number);
        });
        function center(number){
          var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
          var num = number;
          var found = false;
          for(var i in sync2visible){
            if(num === sync2visible[i]){
              	var found = true;
            }
          }
          if(found===false){
            	if(num>sync2visible[sync2visible.length-1]){
              		sync2.trigger("owl.goTo", num - sync2visible.length+10)
            	}else{
              		if(num - 1 === -1){
                		num = 0;
              		}
              	sync2.trigger("owl.goTo", num);
            	}
          }else if(num === sync2visible[sync2visible.length-10]){
            	sync2.trigger("owl.goTo", sync2visible[10])
          }else if(num === sync2visible[0]){
            	sync2.trigger("owl.goTo", num-1)
          }
        }
      });
    </script>
	
	<script src="http://owlgraphic.com/owlcarousel/assets/js/jquery-1.9.1.min.js"></script> 
    <script src="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.js"></script>
    <link href="http://owlgraphic.com/owlcarousel/assets/css/custom.css" rel="stylesheet">
    <link href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.theme.css" rel="stylesheet">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="http://owlgraphic.com/owlcarousel/assets/ico/favicon.png">

    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-collapse.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-transition.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-tab.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/google-code-prettify/prettify.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/application.js"></script>

</fieldset>
</form:form>
	
</body>
</html>