<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>
<body>

<script>$(document).ready(function() {$('#accountsTable').dataTable( {"paging": false, "info": false, "order": [[ "acc.id","desc"]]});});</script>

<h4>List of users on the community:  ${myComm.name}</h4> 
 <h5> 
 	<a class="btn" href ="<c:url value='/manager/community/addUsers/${myCommId}'/>">Add/Remove Users to a Community</a>  
 </h5>
	<table id="accountsTable">
		<thead>
			<tr>
				<th>Avatar</th>					
				<th>Role</th>				
				<th>Name</th>
				<th>FirstName</th>			
				<th>LastName</th>
				<th>eMail    </th>			
				<th>TimeZone</th>			
				<th>PostCode</th>			
			</tr>
		</thead>
<tbody>
		
	<c:forEach var="acc" items="${myList}" varStatus="row">
			<tr>
				<td><img src="<c:url value="${acc.imageUrl}" />" alt="TestDisplay"
						 width="50" height="50" 
						 onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"/></td>			
				<td>${acc.role.name}</td>	
				<td>${acc.name}</td>		<td>${acc.firstName}</td>	<td>${acc.lastName}</td>
				<td>${acc.email}</td>		<td>${acc.timezone}</td>	<td>${acc.postcode}</td>
          	</tr>
	</c:forEach>
	
</tbody>
</table>

</body>

</html>