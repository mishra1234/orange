<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>
<body>

<script>$(document).ready(function() {$('#accountsTable').dataTable( {"paging": false, "info": false, "order": [[ "acc.id","desc"]]});});</script>

<h4>Accounts List</h4> 
 <h5> 
 	<a class="btn" href="<c:url value='/manager/account/new/'/>">Create New Account</a>
 </h5>
	<table id="accountsTable">
		<thead>
			<tr>
				<th>Avatar</th>					
				<th>Role</th>				
				<th>Name</th>
				<th>FirstName</th>			
				<th>LastName</th>
				<th>eMail    </th>			
				<th>TimeZone</th>			
				<th>PostCode</th>			
				<th>   Options             </th>
			</tr>
		</thead>
<tbody>
		
	<c:forEach var="acc" items="${myList}" varStatus="row">
			<tr>
				<td><img src="<c:url value="${acc.imageUrl}" />" alt="TestDisplay"
						 width="50" height="50" 
						 onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"/></td>			
				<td>${acc.role.name}</td>	
				<td>${acc.name}</td>		<td>${acc.firstName}</td>	<td>${acc.lastName}</td>
				<td>${acc.email}</td>		<td>${acc.timezone}</td>	<td>${acc.postcode}</td>
				<td>    
				    <a href="<c:url value='/manager/account/delete/${acc.id}'/>"> <i class=" icon-remove"></i> </a> 
                    <a href="<c:url value='/manager/account/editAccount/${acc.id}'/>"> <i class="icon-wrench"></i> </a> 
                    <a href="<c:url value='/manager/account/${acc.id}'/>"> <i class="icon-tasks"></i></a>
                </td>
          	</tr>
	</c:forEach>
	
</tbody>
</table>

</body>

</html>