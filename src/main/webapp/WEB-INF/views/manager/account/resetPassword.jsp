<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<body>

<c:url var="var" value='/manager/account/save'/>
<form:form modelAttribute="acc" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<!-- Form Name -->
<legend>Reset Account Password</legend>

<!-- Hidden ID -->
<form:input type="hidden" id="id" 	   	   name="id" 	      placeholder="id" 	    	required="" path="id"/>
<form:input type="hidden" id="password"    name="password" 	  placeholder="password" 	required="" path="password"/>
<form:input type="hidden" id="salt" 	   name="salt" 	      placeholder="salt" 	    required="" path="salt"/>
<form:input type="hidden" id="resetCode"   name="resetCode"   placeholder="resetCode"   required="" path="resetCode"/>
<form:input type="hidden" id="enabled" 	   name="resetCode"   placeholder="resetCode"   required="" path="enabled"/>
<form:input type="hidden" id="facebookUid" name="facebookUid" placeholder="facebookUid" required="" path="facebookUid"/>
<form:input type="hidden" id="googleUid"   name="googleUid"   placeholder="googleUid"   required="" path="googleUid"/>
<form:input type="hidden" id="imageUrl"    name="imageUrl"    placeholder="imageUrl"    required="" path="imageUrl"/>
<form:input type="hidden" id="role.id"     name="role.id"     placeholder="role.id"     required="" path="role.id"/>
<form:input type="hidden" id="name" 	   name="name" 		  placeholder="name" 		required="" path="name"/>
<form:input type="hidden" id="email" 	   name="email" 	  placeholder="email" 		required="" path="email"/>
<form:input type="hidden" id="timezone"    name="timezone"    placeholder="timezone" 	required="" path="timezone"/> 
<form:input type="hidden" id="firstName"   name="firstName"   placeholder="firstName"   required="" path="firstName"/>
<form:input type="hidden" id="lastName"    name="lastName"    placeholder="lastName" 	required="" path="lastName"/>
<form:input type="hidden" id="postcode"    name="postcode"    placeholder="postcode" 	required="" path="postcode"/>
<form:input type="hidden" id="status"      name="status" 	  placeholder="status" 		required="" path="status"/> 
<input 		type="hidden" id="inputId"     name="icon" 		  value="${iUrl}"/>
<br>
<br>
<h5>The password to the account has been reset,</h5> 
<br>
<h5>an email HAS BEING SENT to the user account with a temporary password"</h5>
<br>
<h5>Press OK to continue to Accounts Section.</h5>

<br>
<br>
<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
    <button type="submit" value="Save" name="save"   class="btn btn-primary">OK</button>
  </div>
</div>

</fieldset>
</form:form>
	
</body>
</html>