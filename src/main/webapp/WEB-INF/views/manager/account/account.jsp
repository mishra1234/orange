<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>
<body>
<br>
<h5>Account Information</h5> 
	<table>
		<thead>
			<tr><td>Avatar:     </td><td><img src="<c:url value="${acc.imageUrl}" />" alt="TestDisplay"
						 width="50" height="50" 
						 onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"/></td></tr>	
			<tr><td>Role:       </td><td>${acc.role.name}</td></tr>				
			<tr><td>Name:       </td><td>${acc.name}</td></tr>
			<tr><td>First Name: </td><td>${acc.firstName}</td></tr>			
			<tr><td>Last Name:  </td><td>${acc.lastName}</td></tr>
			<tr><td>eMail:      </td><td>${acc.email}</td></tr>				
			<tr><td>TimeZone:   </td><td>${acc.timezone}</td></tr>			
			<tr><td>Post Code:  </td><td>${acc.postcode}</td></tr>		
			<tr><td>Status: 	</td><td>${acc.status}</td></tr>
		</thead>												
</table>

 <h5>  
 	<a class="btn btn-xs btn-primary disabled" href="<c:url value='/manager/account/editAccount/${acc.id}'/>">Edit Account</a>
 	<a class="btn btn-xs disabled" href="<c:url value='/manager/account/resetPassword/${acc.id}'/>">Reset Account Password</a>
 </h5>
</body>

</html>