<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
	<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<c:url value='/styles/default.css'/>">
		
		
		
	</head>
	<body>
	
	<script type="text/javascript">
	$(document).ready(function() {
			$("#userTable").dataTable( {
		        "paging":   false,
		        "ordering": false,
		        "info":     false
		    } );
			console.log("enter");
		});
		
		</script>
		<h1>Events</h1>

		 <div class="content">
		<!-- TABLE -->
		<table id='userTable' class="display" cellspacing="0">
				<thead><tr><th></th><th>Title</th><th>location</th><th>cost</th></tr></thead>
				<tbody>
					<c:forEach var="eventEntry" items="${events}" varStatus="row">
						<tr>
							<td>
								${eventEntry.id}
							</td>
							<td>
								${eventEntry.title}
							</td>
							<td>
								${eventEntry.location}
							</td>
							<td>
								${eventEntry.cost}
							</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
	</body>
</html>
