<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>

<body>
<c:url var="var" value='/admin/category/save'/>
<form:form class="form-horizontal" method="POST" action="${var}">
<fieldset>

<!-- Form Name -->
<legend>New Category </legend>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="Name">Name</label>
  <div class="controls">
    <input id="Name" name="name" type="text" placeholder="Category Name" class="input-xlarge" required="">
    <p class="help-block">The name of the category</p>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="Icon">Icon</label>
  <div class="controls">
    <input id="Icon" name="iconUrl" type="text" placeholder="valid URL" class="input-xlarge" required="">
    <p class="help-block">Should end with png, or jpg</p>
  </div>
</div>

<!-- Select Multiple -->
<div class="control-group">
  <label class="control-label" for="parentIds">Parent Categories</label>
  <div class="controls">
    <select id="parentIds" name="parentIds" class="input-xlarge" multiple="multiple">
      <c:forEach var="category" items="${listOfCategories}" varStatus="row">
      		<option value="${category.id}">${category.name}</option>
      </c:forEach>
    </select>
  </div>
</div>

<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
    <a class="btn btn-medium btn-cancel" href="<c:url value='/admin/categories'/>">Cancel</a>
    <button type="submit" id="Save" class="btn btn-primary">save</button>
  </div>
</div>

</fieldset>
</form:form>
	
</body>
</html>