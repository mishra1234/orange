
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>

<body>

<script>
$(document).ready(function() {
	$('#categoryTable').dataTable( {
        "paging":   false,
        "info":     false,
        "order": [[ 1, "asc" ]]
    } );
});
</script>
<h4>Categories List</h4> 

	<table id="categoryTable">
		<thead>
			<tr>
				<th></th>
				<th>Name</th>
				<th>Parent</th>
				<th>Communities</th>
				<th>Connections</th>
				<th>Activities</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="cat" items="${myList}" varStatus="row">
			<tr>
				<td><a href= "<c:url value='/manager/communities/category/${cat.id}'/>"><img src="${cat.iconUrl}" width="25" height="25" onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
				<td>${cat.name}</td>
				<td><c:forEach var="subcat" items="${cat.categoriesForParentCategoryId}" varStatus="row">${subcat.name}</c:forEach></td>
				<td>${fn:length(cat.layers)}</td>
				<td></td>
				<td>Events</td>
          	</tr>
</c:forEach>
</tbody>
</table>

</body>

</html>