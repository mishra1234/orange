<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
<script>$(document).ready(function() {$('#eventTable').dataTable({"paging":false, "info":false, "order":[[1,"asc"]]});});</script>
<br>
<h5>${ev.contentTitle} - Event Information</h5> 
<table id="eventTable">
    <tr><td>Icon:</td>				<td><img src="${ev.iconUrl}" width="25" height="25"  
						 					 onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></td>
   	<tr><td>Event Title:</td>		<td>${ev.contentTitle}</td>
	<tr><td>Detail:</td>			<td>${ev.contentBody}</td>
	<tr><td>Address:</td>			<td>${ev.event.address}</td>
	<tr><td>Contact Name:</td>		<td>${ev.event.contactName}</td>
	<tr><td>Contact Email:</td>		<td>${ev.event.contactEmail}</td>
	<tr><td>Organisation:</td>		<td>${ev.event.organisation}</td>
	<tr><td>Cost:</td>				<td>${ev.event.cost}</td>
	<tr><td>DateTime:</td>			<td>${ev.event.displayDatetime}</td>
	<tr><td>addressLat:</td>		<td>${ev.event.addressLat}</td>
	<tr><td>addressLng:</td>		<td>${ev.event.addressLng}</td>
	<tr><td>bookable:</td>			<td>${ev.event.bookable}</td>
	<tr><td>contactPhone:</td>		<td>${ev.event.contactPhone}</td>
	<tr><td>Recommended?:</td>		<td><c:choose>
											<c:when test="${ev.recommended}">Yes</c:when>
											<c:otherwise>No</c:otherwise>
									</c:choose></td>									
</table>

 <h5>  <a href="<c:url value='/manager/event/editEvent/${ev.id}'/>">Edit Event</a> </h5>
</body>

</html>