<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>
<body>
	<script>$(document).ready(function() {$('#eventsTable').dataTable( {"paging": false, "info": false, "order": [[ "acc.id","desc"]]});});</script>
	
	<h4>List of events on the community:  ${myComm.name}</h4> 
	
	<h5>
	 <a class="btn" href ="<c:url value='/manager/community/addEvents/${myCommId}'/>">Add/Remove Events to a Community</a>
	 <a class="btn" href ="<c:url value='/manager/community/copyEvents/${myCommId}'/>">Copy Events to other Community</a>
	</h5>
	
	<table id="eventsTable">
		<thead>
			<tr>
				<th>Icon</th>
				<th>Event Title</th>
				<th>Cost</th>
				<th>DateTime</th>
				<th>Recommended </th>
				<th>   Options        </th>		
			</tr>
		</thead>
		<tbody>
			<c:forEach var="ev" items="${myList}" varStatus="row">
					<tr>
						<td><a href= "<c:url value='/manager/event/${ev.id}'/>"><img src="${ev.iconUrl}" width="25" height="25" onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
						<td>${ev.contentTitle}</td>
						<td>${ev.event.cost}</td>
						<td>${ev.event.displayDatetime}</td>
						<td><c:choose>
								<c:when test="${ev.recommended}">Yes</c:when>
								<c:otherwise>No</c:otherwise>
							</c:choose></td>
						<td>    
						    <a href="<c:url value='/manager/event/delete/${ev.id}'/>">   <i class=" icon-remove"></i></a> 
		                    <a href="<c:url value='/manager/event/editEvent/${ev.id}'/>"><i class="icon-wrench"></i> </a> 
		                    <a href="<c:url value='/manager/event/${ev.id}'/>"> 		   <i class="icon-tasks"></i>  </a>
		                </td>
			</c:forEach>
		</tbody>
	</table>
</body>

</html>