<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<body>
<script>$(document).ready(function() {$('#eventTable').dataTable({"paging":false, "info":false, "order":[[1,"asc"]]});});</script>

<h4>Events List</h4> 
 <h5> <a href ="<c:url value='/manager/event/new/'/>">Add New Event</a> </h5>
	<table id="eventTable">
		<thead>
			<tr>
				<th>Icon</th>
				<th>Event Title</th>
				<th>Cost</th>
				<th>DateTime</th>
				<th># Banners </th>
				<th>   Options        </th>
			</tr>
		</thead>
		<tbody>
		
	<c:forEach var="ev" items="${eventActivities}" varStatus="row">
			<tr>
				<td><a href= "<c:url value='/manager/event/${ev.id}'/>"><img src="${ev.iconUrl}" width="25" height="25" onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
				<td>${ev.contentTitle}</td>
				<td>${ev.event.cost}</td>
				<td>${ev.event.displayDatetime}</td>
				<td><a href= "<c:url value='/manager/banners/event/${ev.id}'/>">	  ${fn:length(ev.banners)}</a></td>
				<td>    
				    <a href="<c:url value='/manager/event/delete/${ev.id}'/>">   <i class=" icon-remove"></i></a> 
                    <a href="<c:url value='/manager/event/editEvent/${ev.id}'/>"><i class="icon-wrench"></i> </a> 
                    <a href="<c:url value='/manager/event/${ev.id}'/>"> 		   <i class="icon-tasks"></i>  </a>
                </td>
          	</tr>
	</c:forEach>
	
</tbody>
</table>

</body>

</html>