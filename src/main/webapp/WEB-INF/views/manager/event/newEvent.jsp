<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	
</head>

<body>
<c:url var="var" value='/manager/event/save'/>
<form:form modelAttribute="newEvent" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<!-- Form Name -->
<legend>New Event </legend>

<!-- Hidden ID -->
<form:input type="hidden" path="activityId" value=""/>
<form:input type="hidden" path="activity.Id" value=""/>
<form:input type="hidden" path="activity.activityType" value="event"/>
<form:input type="hidden" id="deleted" 	name="deleted" 	  placeholder="deleted" 	 class="input-xlarge" required="" path="activity.deleted" value="false"/>
<form:input type="hidden" id="ActionUrl" name="actionUrl" placeholder="ActionUrl" path="activity.actionUrl" value="NotUsedAnymore"/>
<form:input type="hidden" id="ActionTitle" name="actionTitle" placeholder="ActionTitle" path="activity.actionTitle" value="NotUsedAnymore"/>
<form:input type="hidden" id="createdDatetime" name="createdDatetime" placeholder="createdDatetime" path="activity.createdDatetime" value="${currentTS}"/>
<form:input type="hidden" id="createdAccountId" name="createdAccountId" placeholder="createdAccountId" path="activity.createdAccountId" value="${currentUser}"/>
<form:input type="hidden" id="IconUrl" name="iconUrl"  value="www.xxx.com" path="activity.iconUrl"/>
<input 		type="hidden" id="inputId" name="icon" value="${inputId}"/>
<form:input type="hidden" id="addressLat" name="addressLat" placeholder="addressLat" value="0.000000" path="addressLat"/>
<form:input type="hidden" id="addressLng" name="addressLng" placeholder="addressLng" value="0.000000" path="addressLng"/>

<!-- Select from list -->
<div class="control-group">
  <label class="control-label" for="iconUrl">Icon selected</label>
	<div id="iconUrl">
        <div class="controls">
          <div class="row">
            <div class="span12">
              <div id="sync1" class="owl-carousel">
                	<c:forEach var="bann" items="${listOfIcons}" varStatus="row">
  						<img src="${bann.iconUrl}"/>
  					</c:forEach>
  			  </div>
  			  <div id="sync2" class="owl-carousel">
                	<c:forEach var="bann" items="${listOfIcons}" varStatus="row">
  						<img src="${bann.iconUrl}"/>
  					</c:forEach>
  			  </div>
            </div>
          </div>
        </div>
    </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="contentTitle">Event Title</label>
  <div class="controls">
    <form:input id="ContentTitle" name="contentTitle" type="text" placeholder="ContentTitle" class="input-xlarge" required="" path="activity.contentTitle"/>
    <!--<font color="red"><form:errors path="*" >Please verify the field is not empty</form:errors></font>-->
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="contentBody">Event Description</label>
  <div class="controls">
    <form:textarea id="ContentBody" name="contentBody" type="text" placeholder="ContentBody" 
    			   class="input-xxlarge" required="" path="activity.contentBody" rows="20"/>
    <!--<font color="red"><form:errors path="*" >Please verify the field is not empty</form:errors></font>-->
  </div>
</div>


<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="account.id">Owner id of event</label>
  <div class="controls">
    <form:select id="account.id" name="account.id" class="input-xlarge" path="activity.account.id">
      	<form:options items="${listOfOwners}" itemValue="id" itemLabel="name"/>
    </form:select>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="address">Address</label>
  <div class="controls">
    <form:input id="Address" name="address" type="text" placeholder="Address" class="input-xlarge" required="" path="address"/>
    <font color="red"><form:errors path="address" >Please verify the field is not empty</form:errors></font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="contactName">ContactName</label>
  <div class="controls">
    <form:input id="ContactName" name="contactName" type="text" placeholder="ContactName" class="input-xlarge" required="" path="contactName"/>
    <font color="red"><form:errors path="contactName" >Please verify the field is not empty</form:errors></font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="contactEmail">ContactEmail</label>
  <div class="controls">
    <form:input id="ContactEmail" name="contactEmail" type="text" placeholder="ContactEmail" class="input-xlarge" required="" path="contactEmail"/>
    <font color="red"><form:errors path="contactEmail" >Please verify the field is not empty and meets email structure requirements</form:errors></font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="organisation">Organisation</label>
  <div class="controls">
    <form:input id="Organisation" name="organisation" type="text" placeholder="Organisation" class="input-xlarge" required="" path="organisation"/>
    <font color="red"><form:errors path="organisation" >Please verify the field is not empty</form:errors></font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="cost">Cost</label>
  <div class="controls">
    <form:input id="Cost" name="cost" type="text" placeholder="Cost" class="input-xlarge" required="" path="cost"/>
    <font color="red"><form:errors path="cost" >Please verify the field is not empty</form:errors></font>
  </div>
</div>

<div id="datetimepicker" class="control-group">
	<label class="control-label" for="cost">Start Date and Time</label>
	<div class="controls">
      <input type="text" name="sDateTime" value="${sDateTime}"></input>
      <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
    </div>  
</div>

<div id="datetimepicker1" class="control-group">
	<label class="control-label" for="cost">End Date and Time</label>
	<div class="controls">
    	<input type="text" name="eDateTime" value="${eDateTime}"></input>
    	<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
  	</div>  
</div>

<div id="days" class="control-group"><label class="control-label">Week Days of event</label>
	<div class="controls">
		<c:choose>
			<c:when test="${monday==true}"><label for="MO">  Monday <input type="checkbox" name="days" value="MO" id="days" checked="checked"/></label></c:when>
			<c:otherwise><label for="MO">  Monday <input type="checkbox" name="days" value="MO" id="days"/></label></c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${tuesday==true}"><label for="TU">Tuesday <input type="checkbox" name="days" value="TU" id="days" checked="checked"/></label></c:when>
			<c:otherwise><label for="TU">Tuesday <input type="checkbox" name="days" value="TU" id="days"/></label></c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${wednesday==true}"><label for="WE">Wednesday <input type="checkbox" name="days" value="WE" id="days" checked="checked"/></label></c:when>
			<c:otherwise><label for="WE">Wednesday <input type="checkbox" name="days" value="WE" id="days"/></label></c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${thursday==true}"><label for="TH">Thursday <input type="checkbox" name="days" value="TH" id="days" checked="checked"/></label></c:when>
			<c:otherwise><label for="TH">Thursday <input type="checkbox" name="days" value="TH" id="days"/></label></c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${friday==true}"><label for="FR">Friday <input type="checkbox" name="days" value="FR" id="days" checked="checked"/></label></c:when>
			<c:otherwise><label for="FR">Friday <input type="checkbox" name="days" value="FR" id="days"/></label></c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${saturday==true}"><label for="SA">Saturday <input type="checkbox" name="days" value="SA" id="days" checked="checked"/></label></c:when>
			<c:otherwise><label for="SA">Saturday <input type="checkbox" name="days" value="SA" id="days"/></label></c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${sunday==true}"><label for="SU">Sunday <input type="checkbox" name="days" value="SU" id="days" checked="checked"/></label></c:when>
			<c:otherwise><label for="SU">Sunday <input type="checkbox" name="days" value="SU" id="days"/></label></c:otherwise>
		</c:choose>
    </div>  
</div>   

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="contactPhone">Contact Phone</label>
  <div class="controls">
    <form:input id="contactPhone" name="contactPhone" type="text" placeholder="contactPhone" class="input-xlarge" required="" path="contactPhone"/>
    <font color="red"><form:errors path="contactPhone" >Please verify the field is not empty</form:errors></font>
  </div>
</div>

<!-- Radio Buttons-->
<fieldset>
   <div class="control-group">
    <label class="control-label">bookable ?</label>
    <div class="controls">
        <div class="radio-inline"><form:radiobutton value="false" path="bookable"/> No </div>
    	<div class="radio-inline"><form:radiobutton value="true" path="bookable"/> Yes </div>
    	<font color="red"><form:errors path="bookable" >Please verify the field is not empty</form:errors></font>
    </div>
    </div>
</fieldset>

<!-- Radio Buttons-->
<fieldset>
   <div class="control-group">
    <label class="control-label">Recommended ?</label>
    <div class="controls">
        <div class="radio-inline"><form:radiobutton value="false" path="activity.recommended"/> No </div>
    	<div class="radio-inline"><form:radiobutton value="true" path="activity.recommended"/> Yes </div>
    	<!--<font color="red"><form:errors path="*" >Please verify the field is not empty</form:errors></font> -->
    </div>
    </div>
</fieldset>
<br/><br/>
<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
    <a class="btn btn-medium btn-cancel" href="<c:url value='/manager/events'/>">Cancel</a>
    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
  </div>
</div>

<!---- SCRIPTS ---->
<script type="text/javascript">
	$(function() {
	   $('#datetimepicker').datetimepicker({
		   format: 'yyyy/MM/dd hh:mm:ss'
	   });
	});
</script>

<script type="text/javascript">
	$(function() {
	   $('#datetimepicker1').datetimepicker({
	       format: 'yyyy/MM/dd hh:mm:ss'
	   });
   });
</script>

    <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.io/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://tarruda.github.io/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script> 
	<script type="text/javascript" src="http://tarruda.github.io/bootstrap-datetimepicker/assets/js/index.js"></script> 
	<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>

<script>
    $(document).ready(function() {
        var sync1  = $("#sync1");
        var status = $("#owlStatus");
        var sync2  = $("#sync2");
        var iconUrl  = ${iUrl};
        
        sync1.owlCarousel({
          singleItem:true,
          items:1,
          margin:10,
          autoHeight:true,
          itemsMobile : true,
          slideSpeed : 100,
          navigation: true,
          pagination:false,
          afterAction : syncPosition,
          responsiveRefreshRate : 200,
        });
        sync1.trigger("owl.goTo",0);  <!-- This function sets the current community icon in the list of icons - Big Image/carousel1-->
        
        sync2.owlCarousel({
          items : 8,
          itemsDesktop      : [1000,8],
          itemsDesktopSmall : [979,6],
          itemsTablet       : [768,4],
          itemsMobile       : [479,2],
          pagination:false,
          responsiveRefreshRate : 100,
          afterInit : function(el){
            el.find(".owl-item").eq(0).addClass("synced");
          }
        });
        sync2.trigger("owl.goTo",iconUrl);  <!-- This function sets the current community icon in the list of icons - Big Image/carousel1-->
        
        function updateResult(pos,value){
            status.find(pos).find(".result").text(value);
            document.getElementById('inputId').value =(value);
            document.getElementById('formId').checked = true;
          }
        function afterAction(){
            updateResult(".currentItem", this.sync1.currentItem);
          }
        function syncPosition(el){
          var current = this.currentItem;
          $("#sync2")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")
          if($("#sync2").data("owlCarousel") !== undefined){
            center(current)
          }
        }
        $("#sync2").on("click", ".owl-item", function(e){
          e.preventDefault();
          var number = $(this).data("owlItem");
          sync1.trigger("owl.goTo",number);
          updateResult(".currentItem", number);
        });
        function center(number){
          var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
          var num = number;
          var found = false;
          for(var i in sync2visible){
            if(num === sync2visible[i]){
              	var found = true;
            }
          }
          if(found===false){
            	if(num>sync2visible[sync2visible.length-1]){
              		sync2.trigger("owl.goTo", num - sync2visible.length+10)
            	}else{
              		if(num - 1 === -1){
                		num = 0;
              		}
              	sync2.trigger("owl.goTo", num);
            	}
          }else if(num === sync2visible[sync2visible.length-10]){
            	sync2.trigger("owl.goTo", sync2visible[10])
          }else if(num === sync2visible[0]){
            	sync2.trigger("owl.goTo", num-1)
          }
        }
      });
    </script>
	
	<script src="http://owlgraphic.com/owlcarousel/assets/js/jquery-1.9.1.min.js"></script> 
    <script src="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.js"></script>
    <link href="http://owlgraphic.com/owlcarousel/assets/css/custom.css" rel="stylesheet">
    <link href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.theme.css" rel="stylesheet">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="http://owlgraphic.com/owlcarousel/assets/ico/favicon.png">

    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-collapse.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-transition.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-tab.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/google-code-prettify/prettify.js"></script>
    <script src="http://owlgraphic.com/owlcarousel/assets/js/application.js"></script>

</fieldset>
</form:form>
	<script type="text/javascript" src="http://tarruda.github.io/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script> 
</body>
</html>