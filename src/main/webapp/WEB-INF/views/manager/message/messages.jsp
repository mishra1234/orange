<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>

<body>

<script>
$(document).ready(function() {
	$('#messageTable').dataTable( {
        "paging":   false,
        "info":     false,
        "order": [[ 0, "desc" ]]
    } );
});
</script>
<h4>Messages List</h4> 
 <h5> <a href ="<c:url value='/manager/message/new/'/>">Add New Message</a> </h5>
	<table id="messageTable">
		<thead>
			<tr>
				<th>Id</th>
				<th>From </th>
				<th>To </th>
				<th>Subject</th>
				<th>Message</th>
				<th>Date Sent</th>
				<th>Read Status</th>
				<th>Community Id</th>
				<th>Activity Id</th>
				<th>   Options        </th>
			</tr>
		</thead>
		<tbody>
		
	<c:forEach var="mess" items="${myMessagesList}" varStatus="row">
			<tr>
				<td>${mess.id}</td>
				<td>${mess.accountByFromId.name}</td>
				<td>${mess.accountByToId.name}</td>
				<td>${mess.subject}</td>
				<td>${mess.message}</td>
				<td>${mess.dateSent}</td>
				<td><c:choose>
						<c:when test="${mess.isRead}">Read</c:when>
						<c:otherwise>Unread</c:otherwise>
					</c:choose></td>
				<td>${mess.layerId}</td>
				<td>${mess.activityId}</td>
				<td>    
				    <a href="<c:url value='/manager/message/delete/${mess.id}'/>"> <i class=" icon-remove"></i> </a> 
                    <a href="<c:url value='/manager/message/editMessage/${mess.id}'/>"><i class="icon-wrench"></i> </a> 
                    <a href="<c:url value='/manager/message/${mess.id}'/>"> <i class="icon-tasks"></i></a>
                </td>
          	</tr>
	</c:forEach>
	
</tbody>
</table>

</body>

</html>