<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<body>
<c:url var="var" value='/manager/message/save'/>
<form:form modelAttribute="message1" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<!-- Form Name -->
<legend>New Message </legend>

<!-- Hidden ID -->
<form:input type="hidden" path="id" value=""/>
<form:input id="isRead" name="isRead" type="hidden" placeholder="isRead" class="input-xlarge" required="" value="false" path="isRead" />
<form:input id="deleted" name="deleted" type="hidden" placeholder="deleted" class="input-xlarge" required=""  value="false" path="deleted"/>
<form:input id="dateSent" name="dateSent" type="hidden" placeholder="dateSent" class="input-xlarge" required="" path="dateSent" value="${currDateTime}"/>


<!-- Select Multiple -->
<div class="control-group">
  <label class="control-label" for="accountByFromId">Account From</label>
  <div class="controls">
    <form:select id="accountByFromId" name="accountByFromId" class="input-xlarge" multiple="multiple" path="accountByFromId.id" size="4">
      <form:options items="${accounts}" itemValue="id" itemLabel="name"></form:options>
    </form:select>
    <p class="help-block">Select one or more accounts to set as senders</p>
    <font color="red">
    	<form:errors path="accountByFromId" >Please select an account</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="subject">Message Subject</label>
  <div class="controls">
    <form:input id="subject" name="subject" type="text" placeholder="Subject" class="input-xlarge" required="" path="subject"/>
    <p class="help-block">Max Subject Lenght of 45 characters</p>
    <font color="red">
    	<form:errors path="subject" >Subject must be between 2 and 128 characters lenght</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="message">Message Text</label>
  <div class="controls">
    <form:textarea 	id="message" 	name="message" 	type="text" 	placeholder="Message" 	class="input-xlarge" 
    				required="" 	path="message" 	rows="6" 		cols="25"/>
    <p class="help-block">Max Text Lenght of 65535 characters</p>
    <font color="red">
    	<form:errors path="message" >Message text must be minimum 8 characters long</form:errors>
    </font>
  </div>
</div>

<div class="control-group">
  <label class="control-label" for="accountByToId">Account To</label>
  <div class="controls">
    <form:select id="accountByToId" name="accountByToId" class="input-xlarge" multiple="multiple" path="accountByToId.id" size="4">
      <form:options items="${accounts}" itemValue="id" itemLabel="name"></form:options>
    </form:select>
    <p class="help-block">Select one or more accounts to send message</p>
    <font color="red">
    	<form:errors path="accountByToId" >Please select an account to send a message</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="layerId">Community Id</label>
  <div class="controls">
    <form:select id="layerId" name="layerId" class="input-xlarge" multiple="multiple" path="layerId" size="4">
      <form:options items="${messageLayers}" itemValue="id" itemLabel="name"></form:options>
    </form:select>
    <p class="help-block">Select one or more Communitites to send message</p>
    <font color="red">
    	<form:errors path="layerId" >Please select a community to send a message</form:errors>
    </font>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="activityId">Activity Id</label>
  <div class="controls">
    <form:select id="activityId" name="activityId" class="input-xlarge" multiple="multiple" path="activityId" size="4">
      <form:options items="${messageActivities}" itemValue="id" itemLabel="contentTitle"></form:options>
    </form:select>
    <p class="help-block">Select one or more Activities to send message</p>
    <font color="red">
    	<form:errors path="activityId" >Please select an activity to send a message</form:errors>
    </font>
  </div>
</div>

<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
    <a class="btn btn-medium btn-cancel" href="<c:url value='/manager/messages'/>">Cancel</a>
    <button type="submit" value="Save"   name="save"    class="btn btn-primary">Save</button>
  </div>
</div>

</fieldset>
</form:form>
	
</body>
</html>