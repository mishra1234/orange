<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>
<body>
<br>
<h5>Message Information</h5> 
<table>
    <tr><td>Message From:</td>				<td>${mess.accountByFromId.name}</td></tr>
   	<tr><td>Directed To:</td>				<td>${mess.accountByToId.name}</td></tr>
   	<tr><td>Subject:</td>					<td>${mess.subject}</td></tr>
	<tr><td>Message Text:</td>				<td>${mess.message}</td></tr>
	<tr><td>Date Sent:</td>					<td>${mess.dateSent}</td></tr>
	<tr><td>Read Status:</td>				<td><c:choose>
													<c:when test="${mess.isRead}">Read</c:when>
														<c:otherwise>Unread</c:otherwise></c:choose></td>
	<tr><td>Related Community ID:</td>		<td>${mess.layerId}</td></tr>	
	<tr><td>Related Activity ID:</td>		<td>${mess.activityId}</td></tr>												
</table>

 <h5>  <a href="<c:url value='/manager/message/editMessage/${mess.id}'/>">Edit Message</a> </h5>
</body>

</html>