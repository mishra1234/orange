<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
<script>$(document).ready(function() {$('#Table').dataTable({"paging":false, "info":false, "order":[[1,"asc"]]});});</script>
<br>
<h5>${bann.id} - Banner Information</h5> 
	<table id="Table">
		<tr><td>Banner ID:		 </td><td>${bann.id}</td>				
		<tr><td>Banner Image:	 </td><td><img src="${bann.imageUrl}" width="500" height="500"  
					 				   onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></td>
		<tr><td>Banner Image Url:</td><td>${bann.imageUrl}</td>
		<tr><td>Banner Title: 	 </td><td>${bann.title}</td>		
		<tr><td>Subtitle:  		 </td><td>${bann.subtitle}</td>										
	</table>
 <h5>  <a href="<c:url value='/manager/banner/editBanner/${bann.id}'/>">Edit Banner</a> </h5>
</body>

</html>