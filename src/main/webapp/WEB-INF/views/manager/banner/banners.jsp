<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<body>
<script>$(document).ready(function() {$('#bannerTable').dataTable({"paging":false, "info":false, "order":[[0,"desc"]]});});</script>

<h4>Banners List</h4> 
 <h5> <a href ="<c:url value='/manager/banner/new/'/>">Add New Banner</a> </h5>
	<table id="bannerTable">
		<thead>
			<tr>
				<th>image ID</th>
				<th>Banner Image</th>
				<th>Banner Title</th>
				<th>subtitle</th>
				<th>   Options        </th>
			</tr>
		</thead>
		<tbody>
		
	<c:forEach var="bann" items="${banners}" varStatus="row">
			<tr>
				<td>${bann.id}</td>
				<td><img src="<c:url value="${bann.imageUrl}" />" alt="TestDisplay"
						 width="50" height="50" 
						 onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"/></td>
				<td>${bann.title}</td>
				<td>${bann.subtitle}</td>
				<td>    
				    <a href="<c:url value='/manager/banner/delete/${bann.id}'/>">    <i class=" icon-remove"></i> </a> 
                    <a href="<c:url value='/manager/banner/editBanner/${bann.id}'/>"><i class="icon-wrench"></i> </a> 
                    <a href="<c:url value='/manager/banner/${bann.id}'/>"> 		   <i class="icon-tasks"></i>  </a>
                </td>
          	</tr>
	</c:forEach>

</tbody>
</table>

</body>

</html>