<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html class="fixed">


<body>
	<div class="content-wrap nopadding">
    	<div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; 
    	background: url('../images/parallax/home/Slider-A.jpg') center center no-repeat; background-size: cover;">

	        <div class="section nobg full-screen nopadding nomargin">
	           	<div class="container vertical-middle divcenter clearfix">
	                <div class="row center">
	                    <a href="index.html"><img src="../images/logo-dark.png" alt="CC Me"></a>
	                
						<br><br><br><br><br><br>
		                <div class="panel panel-default divcenter noradius noborder row center" style="max-width: 850px; background-color: rgba(255,255,255,0.93);">
		                	<div class="panel-body" style="padding: 40px;">
								<div class="container">
									<div class="home-intro" id="home-intro">
								        <div class="container">
								            <div class="row">
								                <div class="col-md-6">
								                    <p><strong>Handle all your event requirements right here at <em>Community Connect Me,</em>
								                    enjoy the time savings and efficiency.</strong></p>
								                </div>
								                <div class="col-md-4">
								                    <div class="get-started">
								                        <p><a class="btn btn-lg btn-warning" href="<c:url value='/register'/>" >Start Connecting Now!</a></p>
								                        <strong><a href="javascript:learnMore()" style="color:#FF0A00">or learn more.</a></strong>
								                    </div>
								                </div>
								            </div>
								        </div>
							    	</div>
							    	<br>
									<div class="container">
										<div class="row">
											<div class="col-md-12 center">
												<c:choose>
													<c:when test="${error}">
														
														<h1 class="short small">User not found </h1>
														<p>Click here to register: </p>
														<a href="<c:url value='/register'/>">Sign-Up</a>
													
													</c:when>
													<c:otherwise>
													
													<h1>Password Reset</h1>
													<h3>What is next?</h3>
													<p>You will receive an email to reset your password.</p>
								
													<p>Don't forget to check your junk folder.</p>
													<br>
													<p><strong>Thank you.</strong></p>
													 <a href="<c:url value='/start'/>" class="btn btn-lg btn-success">Go to Community Connect Me!</a>
													</c:otherwise>
												</c:choose>		
											</div>
										</div>
										<br>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row text-center">
				<small>Copyrights &copy; All Rights Reserved by Digital Market Square</small>
			</div>
		</div>
	</div>
</body>
</html>
