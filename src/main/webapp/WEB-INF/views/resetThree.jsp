<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>

<html>
<body>
	<div role="main" class="main">
	
	  <div class="home-intro" id="home-intro">
        <div class="container">

            <div class="row">
                <div class="col-md-8">
                	</br>
                    <p>
                        <strong>Handle all your event requirements right here at <em>Community Connect Me</em></br>
                        Enjoy the time savings and efficiency. </strong>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="get-started">
                        <a href="<c:url value='/register'/>" class="btn btn-lg btn-primary">Start Connecting Now!</a>
                        <strong><a href="javascript:learnMore()" style="color:#FFFFFF">or learn more.</a></strong></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
	
		<div class="container">
			<div class="row">
				<div class="col-md-12 center">
				<c:choose>
					<c:when test="${done}">
						
						<div class="featured-box featured-box-primary">
                        <div class="box-content">
                           <h4>All Done!</h4>
											
                         </div>      
                         <br> <br>
					<div class="submitbutton">
						<a class="btn btn btn-warning" href="<c:url value='/start'/>">Goto Community Connect Me Home</a>
					</div> 
                     </div>
                 </div>   
					
					</c:when>
					<c:otherwise>
					
						   <h4>Something went wrong</h4>
						   <p>Looks like you already updated your password, or typed incorrect URL </p>
						   
						   <p> </p>
						   <p>Please try again</p>
						   
						   <br> <br>
					<div class="submitbutton">
						<a class="btn btn btn-warning" href="<c:url value='/start'/>">Goto Community Connect Me Home</a>
					</div>
					
					</c:otherwise>
				</c:choose>				
				</div>
			</div>
		</div>
	</div>

</body>
</html>