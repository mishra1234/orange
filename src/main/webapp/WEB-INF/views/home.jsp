<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- start: page -->
<html>

	<body>
	
	 <script src="<c:url value='ruralAssets/js/newsRSS.js'/>"></script> 
	 <script src="<c:url value='ruralAssets/myJS/custom/firealerts.js'/>"></script>   
	 <script src="<c:url value='ruralAssets/myJS/custom/weather.js'/>"></script>   
	 <script src="<c:url value='ruralAssets/myJS/custom/currencyexchange.js'/>"></script>   
	  
	   
	  <div role="main" class="main">
				<div class="slider-container">
					<div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 500}'>
						<ul>
							<li data-transition="fade" data-slotamount="13" data-masterspeed="300" >
				
								<img src="https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/welcome-page/RC-Home-screen.png" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
				
								<div class="tp-caption sft stb visible-lg"
									 data-x="30"
									 data-y="190"
									 data-speed="300"
									 data-start="1000"
									 data-easing="easeOutExpo"><img src="img/slides/slide-title-border.png" alt=""></div>
				
								<div class="tp-caption top-label lfl stl"
									 data-x="70"
									 data-y="180"
									 data-speed="300"
									 data-start="450"
									 style="font-size:30px; font-weight:bold;"
									 data-easing="easeOutExpo">Key information for business</div>
				
								<div class="tp-caption sft stb visible-lg"
									 data-x="507"
									 data-y="190"
									 data-speed="300"
									 data-start="1000"
										 data-easing="easeOutExpo"><img src="img/slides/slide-title-border.png" alt=""></div>
				
								<div class="tp-caption main-label sft stb"
									 data-x="70"
									 data-y="210"
									 data-speed="300"
									 data-start="1500"
									  style="font-size:40px;"
								
									 data-easing="easeOutExpo">and life on the land </div>
				
								<div class="tp-caption bottom-label sft stb"
									 data-x="115"
									 data-y="280"
									 data-speed="500"
									 data-start="2000"
									 style="font-size:50px; font-weight:bold;  "
									 data-easing="easeOutExpo">in one place</div>
									
									<div class="tp-caption bottom-label sft stb"
										 data-x="745"
										 data-y="50"
										 data-speed="800"
										 data-start="3200"
										 data-easing="easeOutExpo"><img src="https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/welcome-page/rc_shadow+(1).png"></div>
									
									<div class="tp-caption bottom-label sft stb"
										 data-x="973"
										 data-y="60"
										 data-speed="800"
										 data-start="3200"
										 data-easing="easeOutExpo"><img src="https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/welcome-page/rc_imageforbanner2+(1).jpg"></div>
								
									
										<div class="tp-caption bottom-label sft stb"
										 data-x="755"
										 data-y="60"
										 data-speed="800"
										 data-start="3200"
										 data-easing="easeOutExpo"><img src="https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/welcome-page/rc_imageforbanner3+(1).jpg"></div>
							
									<div class="tp-caption bottom-label sft stb"
										 data-x="755"
										 data-y="220"
										 data-speed="800"
										 data-start="3200"
										 data-easing="easeOutExpo"><img src="https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/welcome-page/rc_imageforbanner1+(1).jpg"></div>
				
										
									<div class="tp-caption bottom-label sft stb"
										 data-x="973"
										 data-y="220"
										 data-speed="800"
										 data-start="3200"
										 data-easing="easeOutExpo"><img src="https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/welcome-page/rc_imageforbanner4+(1).jpg"></div>
									
<!-- 								<div class="tp-caption randomrotate" -->
<!-- 									 data-x="905" -->
<!-- 									 data-y="248" -->
<!-- 									 data-speed="500" -->
<!-- 									 data-start="2500" -->
<!-- 									 data-easing="easeOutBack"><img src="img/slides/slide-concept-2-1.png" alt=""></div> -->
				
<!-- 								<div class="tp-caption sfb" -->
<!-- 									 data-x="955" -->
<!-- 									 data-y="200" -->
<!-- 									 data-speed="400" -->
<!-- 									 data-start="3000" -->
<!-- 									 data-easing="easeOutBack"><img src="img/slides/slide-concept-2-2.png" alt=""></div> -->
				
<!-- 								<div class="tp-caption sfb" -->
<!-- 									 data-x="925" -->
<!-- 									 data-y="170" -->
<!-- 									 data-speed="700" -->
<!-- 									 data-start="3150" -->
<!-- 									 data-easing="easeOutBack"><img src="img/slides/slide-concept-2-3.png" alt=""></div> -->
				
<!-- 								<div class="tp-caption sfb" -->
<!-- 									 data-x="875" -->
<!-- 									 data-y="130" -->
<!-- 									 data-speed="1000" -->
<!-- 									 data-start="3250" -->
<!-- 									 data-easing="easeOutBack"><img src="img/slides/slide-concept-2-4.png" alt=""></div> -->
				
<!-- 								<div class="tp-caption sfb" -->
<!-- 									 data-x="605" -->
<!-- 									 data-y="80" -->
<!-- 									 data-speed="600" -->
<!-- 									 data-start="3450" -->
<!-- 									 data-easing="easeOutExpo"><img src="img/slides/slide-concept-2-5.png" alt=""></div> -->
				
<!-- 								<div class="tp-caption blackboard-text lfb " -->
<!-- 									 data-x="635" -->
<!-- 									 data-y="300" -->
<!-- 									 data-speed="500" -->
<!-- 									 data-start="3450" -->
<!-- 									 data-easing="easeOutExpo" style="font-size: 37px;">What</div> -->
				
<!-- 								<div class="tp-caption blackboard-text lfb " -->
<!-- 									 data-x="660" -->
<!-- 									 data-y="350" -->
<!-- 									 data-speed="500" -->
<!-- 									 data-start="3650" -->
<!-- 									 data-easing="easeOutExpo" style="font-size: 47px;">would be</div> -->
				
<!-- 								<div class="tp-caption blackboard-text lfb " -->
<!-- 									 data-x="685" -->
<!-- 									 data-y="400" -->
<!-- 									 data-speed="500" -->
<!-- 									 data-start="3850" -->
<!-- 									 data-easing="easeOutExpo" style="font-size: 32px;">really helpful...</div> -->
							</li>
<!-- 							<li data-transition="fade" data-slotamount="5" data-masterspeed="100" > -->
				
<!-- 								<img src="https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/RC_windmill.png" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat"> -->
				
<!-- 								<div class="tp-caption sft stb visible-lg" -->
<!-- 									 data-x="110" -->
<!-- 									 data-y="110" -->
<!-- 									 data-speed="300" -->
<!-- 									 data-start="450" -->
<!-- 									 data-easing="easeOutExpo"><img src="img/slides/slide-title-border.png" alt=""></div> -->
<!-- 								<div class="tp-caption top-label lfl stl" -->
<!-- 									 data-x="150" -->
<!-- 									 data-y="100" -->
<!-- 									 data-speed="300" -->
<!-- 									 data-start="450" -->
<!-- 									 style="font-size:30px; font-weight:bold;" -->
<!-- 									 data-easing="easeOutExpo">Key information for business</div> -->
				
<!-- 								<div class="tp-caption sft stb visible-lg" -->
<!-- 									 data-x="587" -->
<!-- 									 data-y="110" -->
<!-- 									 data-speed="300" -->
<!-- 									 data-start="450" -->
<!-- 										 data-easing="easeOutExpo"><img src="img/slides/slide-title-border.png" alt=""></div> -->
				
<!-- 								<div class="tp-caption main-label sft stb" -->
<!-- 									 data-x="150" -->
<!-- 									 data-y="130" -->
<!-- 									 data-speed="300" -->
<!-- 									 data-start="450" -->
<!-- 									  style="font-size:40px;" -->
								
<!-- 									 data-easing="easeOutExpo">and life on the land </div> -->
				
<!-- 								<div class="tp-caption bottom-label sft stb" -->
<!-- 									 data-x="155" -->
<!-- 									 data-y="200" -->
<!-- 									 data-speed="500" -->
<!-- 									 data-start="450" -->
<!-- 									 style="font-size:50px; font-weight:bold;  " -->
<!-- 									 data-easing="easeOutExpo">in one place</div> -->

									
<!-- 									<div class="tp-caption sft stb" -->
<!-- 										 data-x="155" -->
<!-- 										 data-y="100" -->
<!-- 										 data-speed="600" -->
<!-- 										 data-start="100" -->
<!-- 										 data-easing="easeOutExpo"><img src="img/slides/slide-concept.png" alt=""></div> -->
				
<!-- 									<div class="tp-caption blackboard-text sft stb" -->
<!-- 										 data-x="285" -->
<!-- 										 data-y="180" -->
<!-- 										 data-speed="900" -->
<!-- 										 data-start="1000" -->
<!-- 										 data-easing="easeOutExpo" style="font-size: 30px;">All business info</div> -->
				
<!-- 									<div class="tp-caption blackboard-text sft stb" -->
<!-- 										 data-x="285" -->
<!-- 										 data-y="220" -->
<!-- 										 data-speed="900" -->
<!-- 										 data-start="1200" -->
<!-- 										 data-easing="easeOutExpo" style="font-size: 40px;">In one place!</div> -->
				
				<!--  
									<div class="tp-caption main-label sft stb"
										 data-x="685"
										 data-y="190"
										 data-speed="300"
										 data-start="900"
										 data-easing="easeOutExpo">DESIGN IT!</div>-->
				
							</li>
						</ul>
					</div>
				</div>
				<div class="home-intro" id="home-intro">
					<div class="container">
				
						<div class="row">
							<div class="col-md-9">
								<p>
									 <em> Rural Connect</em>  provides:
                        <h5 style="color:#FFFFFF">Industry news & information &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Events on Industry sectors & regions&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Surveys on key issues</h5><br>
                        <h5 style="color:#FFFFFF">Relevant government forms &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alerts (biosecurity, other emergencies)&nbsp; &nbsp;&nbsp; &nbsp; Weather information</h5>
						
							
							</div>
							 <div class="col-md-3">
                    <div class="get-started">
                        <a href="<c:url value='/register'/>" class="btn btn-lg btn-warning">Start Connecting Now!</a>
                        
                    </div>
                </div>
						</div>
				
					</div>
				</div>
				<!-- 
				<div class="container">
				
					<div class="row center">
						<div class="col-md-12">
						 <h1 class="short word-rotator-title">
					           Rural Connect is
					            <strong class="inverted">
														<span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
															<span class="word-rotate-items">
																<span>incredibly</span>
																<span>especially</span>
																<span>extremely</span>
															</span>
														</span>
					            </strong>
					            useful and fully loaded.
					        </h1>
							<p class="featured lead">
								Text in here
							</p>
						</div>
					</div>
				
				</div> -->

<!-- 
		<div class="home-concept">
			<div class="container">
				<div class="row center">
					<span class="sun"></span> <span class="cloud"></span>
					<div class="col-md-2 col-md-offset-1">
						<div class="process-image" data-appear-animation="bounceIn">
							<img
								src="<c:url value='/ruralAssets/img/placements/farm-1.jpg'/>"
								height="150" alt="" /> <strong>Strategy</strong>
						</div>
					</div>
					<div class="col-md-2">
						<div class="process-image" data-appear-animation="bounceIn"
							data-appear-animation-delay="200">
							<img
								src="<c:url value='/ruralAssets/img/placements/farm-5.jpg'/>"
								height="150px" alt="" /> <strong>Planning</strong>
						</div>
					</div>
					<div class="col-md-2">
						<div class="process-image" data-appear-animation="bounceIn"
							data-appear-animation-delay="400">
							<img
								src="<c:url value='/ruralAssets/img/placements/wingrape.jpg'/>"
								height="150px" alt="" /> <strong>Build</strong>
						</div>
					</div>
					<div class="col-md-4 col-md-offset-1">
						<div class="project-image">
							<div id="fcSlideshow" class="fc-slideshow">
								<ul class="fc-slides">
									<li><a href="portfolio-single-project.html"> <img
											src="<c:url value='/ruralAssets/img/placements/slide1.jpg'/>" /></a></li>
									<li><a href="portfolio-single-project.html"> <img
											src="<c:url value='/ruralAssets/img/placements/slide2.png'/>" /></a></li>

								</ul>
							</div>
							<strong class="our-work">Our Services</strong>
						</div>
					</div>
				</div>

			</div>
		</div> -->
		<!-- Bullets -->

<div class="container" style="text-align:left">

    <div class="row">
        <div class="col-md-12">
            <h2>At Rural Connect you will <strong>find:</strong></h2>
            <div class="row">
                <div class="col-sm-6">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-group" style="margin-top:10px"></i>
                        </div>
                          <div class="feature-box-info">
                            <h4 class="shorter">News and Updates</h4>
                            <p class="tall">Hot news</p>
                        </div>
                        
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-file" style="margin-top:10px"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Surveys</h4>
                            <p class="tall">Help shape policy</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-refresh " style="margin-top:10px"></i>
                        </div>
                     <div class="feature-box-info">
                            <h4 class="shorter">Essential Government information</h4>
                            <p class="tall">Find forms and guidelines</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-adjust" style="margin-top:10px"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Alerts</h4>
                            <p class="tall">Emergency alerts as you need them</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-microphone" style="margin-top:10px"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Radio and Video News Podcasts</h4>
                            <p class="tall">Media for Rural Communities</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-sun-o" style="margin-top:10px"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Your Weather</h4>
                            <p class="tall">Weather forecast directly from your closest station</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-envelope" style="margin-top:10px"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Messages</h4>
                            <p class="tall">Receive newsletters from your connected groups</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-desktop" style="margin-top:10px"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Network</h4>
                            <p class="tall">Engage with your community</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>


<!--  Footer  -->
				
				<div class="container">
				
					<div class="row center">
					<br>
					</div>
					   <div class="row center">
            <div class="col-md-12">
                <h2 class="short word-rotator-title">
                 <!--    We're not the only ones
                    <strong>
									<span class="word-rotate" data-plugin-options='{"delay": 3500, "animDelay": 400}'>
										<span class="word-rotate-items">
											<span>excited</span>
											<span>happy</span>
										</span>
									</span>
                    </strong>
                    about Rural Connect-->Rural Connect Partners
                </h2>
               <!--   <h4 class="lead tall">more than 5,500 farmers use it. Meet our partners.</h4> -->
            </div>
        </div>
        <div class="row center">
            <div class="owl-carousel" data-plugin-options='{"items": 4, "autoplay": false, "autoplayTimeout": 3000}'>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/primesuper_bi_2BW.jpg'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-2.png'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-3.png'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-4.png'/>" alt="">
                </div>

               
            </div>
        </div>
				
				</div>
				
				
				
				
			</div> 
	   
     </body>
	
	
	
</html>
<!-- end: page -->