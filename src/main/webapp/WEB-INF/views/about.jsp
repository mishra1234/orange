<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />

</head>
</head>
<body>


	<div role="main" class="main">

		<div class="home-intro" id="home-intro">
			<div class="container">

				<div class="row">
					<div class="col-md-8">
						<p>
							Handle all your farming requirements right here at <em>Rural
								Connect</em></br> <strong>Enjoy the time savings and efficiency.
							</strong>
						</p>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="<c:url value='/register'/>"
								class="btn btn-lg btn-primary">Start Connecting Now!</a> <strong><a
								href="<c:url value='/more'/>" style="color: #FFFFFF">or
									learn more.</a></strong>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<h1>About <strong>Rural Connect</strong></h1>
						<div id="content">
							<p>The Rural Connect toolkit for primary producers is
								designed to provide – in one place – some special support for
								business, farming and family life on the land. It has been
								provided by Primary Producers SA (PPSA) in partnership with
								Primary Industries and Regions SA and commercial and strategic
								partners. Primary Producers SA (PPSA) is a coalition of major
								commodity associations representing primary producers in South
								Australia. PPSA is a federation of major commodity associations.
							</p>
							</h3>

							<img src="<c:url value='images/farmerM.jpg'/>" width="250px"
								class="imgRight" />

							<p>The commodity associations are: Grain Producers SA,
								Livestock SA, the Horticulture Coalition of SA, the Wine Grape
								Council of SA, the South Australian Dairyfarmers Association and
								Pork SA. Rural Connect includes a Calendar of local events,
								industry events and Commodity Association events. Rural Connect
								is provided to Subscribers both as a web browser version and a
								mobile app.</p>
							<ul>
								<li>Subscribers are invited to book a place at Events and
									save them into their personal Calendar.</li>
								<li>Subscribers receive Reminders for events they have
									added to their personal Calendar – and get Alerts for
									forthcoming events or emergencies direct to their mobile phone.</li>
								<li>PPSA, Commodity Associations or PIRSA can also
									circulate Surveys to selected Associations or the broader
									industry.</li>
								<li>Rural Connect also publishes industry-wide or selected
									news, market reports, weather and other industry-specific
									information.</li>
								<li>Content management, data security, secure booking and
									payment functions are also provided.</li>

							</ul>
						</div>
						<Br>
						<Br>
						<h1>Privacy</h1>
						<div id="content">
							<p>Rural Connect’s general information is available to all
								who subscribe via Registration. The Commodity Associations
								listed on the menu have Members Areas with specific information
								for them. These can be accessed by registered members - and
								those wishing to apply can complete and submit membership forms.
								These credentials will be checked by the appropriate Commodity
								Association or PPSA.</p>
								
								<p>All personal information provided by Subscribers is kept confidential, unless volunteered by individuals as part of Forums or other open communications. All bookings and transactions information is managed by approved Government or other accredited banking organisations.
								</p>
						</div>


					</div>
				</div>
					</div>
				</div>
</body>
</html>