<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<body>
<script src="<c:url value='/scripts/lib/sweet-alert.min.js'/>"></script>
 <link rel="stylesheet" href="<c:url value='/scripts/lib/sweet-alert.css'/>" type="text/css" />


	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>
		<c:set var="price" value="0.10" scope="page" />

			<h2 class="panel-title">Invoice Dashboard</h2>
		</header>
		<div class="panel-body">
												<div class="form-group">
					<label class="col-sm-3 control-label" for="activity.account.id">Select the Month
						
					</label>
					
						<div class="col-sm-3" >
						<select id="yearSelector" name="accountId"  class="form-control">
						<option value="">Select Year</option>
                		
							<c:forEach items="${Year}" var="year"> 
									<option value="${year}">${year}</option>
            					
            				</c:forEach>
						</select>
					</div>
					<input type="hidden" id="currentYearVal" value=${currentYear }>
					<script>
				
					
					$( "#yearSelector" ).change(function () {
					    if($( "option:selected", this ).text()==$("#currentYearVal").val()){
					       $("#monthWholeSelector").hide();
					       $("#monthSelector").show();     
					    }else if($( "option:selected", this ).text()!=$("#currentYearVal").val()){
					       $("#monthSelector").hide(); 
					       $("#monthWholeSelector").show();
					    }
					});
   </script>

					<div class="col-sm-3" >
						<select id="monthSelector" name="accountId" class="form-control" >
						<option value="">Select Month</option>
                		
							<c:forEach items="${Months}" var="owner"> 
									<option value="${owner['key']}">${owner['value']}</option>
            					</c:forEach>
            			
						</select>
					
							<select id="monthWholeSelector" name="accountId" class="form-control" style="display:none">
						<option value="">Select Month</option>
                		
							<c:forEach items="${AllMonths}" var="owner"> 
									<option value="${owner['key']}">${owner['value']}</option>
            					</c:forEach>
            			
						</select>
						</div>
					<button type="submit"  onclick="callTheDetail();" class="btn btn-primary">Get Invoice</button>
					
				</div>
				<br><br>
			<table class="table table-bordered table-striped mb-none" id="communityTable" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
				<thead>
					<tr>
						<th>Item Detail</th>
						<th>Quantity per Month</th>
						<th>Price per Item ($)</th>
						
						<th>Total</th>
					
					</tr>
				</thead>
				<tbody>
							<tr>
							<td>Surveys</td>
							<td><div id="totalSurveys">${surveyNumber}</div></td>
							<td>${price }</td>
<c:set var="totalSurveyPrice"> <fmt:formatNumber minFractionDigits="2"  value="${surveyNumber*price }" /></c:set>
							
							<td><div id="totalSurveysPrice">${totalSurveyPrice }</div></td>
								</tr>
								
									<tr>
							<td>Connections</td>
							<td><div id="totalConnections">${Connection}</div></td>
							<td>${price }</td>
<c:set var="totalConnectionPrice"> <fmt:formatNumber minFractionDigits="2" value="${Connection*price }"  /></c:set>

							<td><div id="totalConnectionsPrice">${totalConnectionPrice}</div></td>
							</tr>
									<tr>
							<td>Messages</td>
							<td><div id="totalMessages">${numberMessages}</div></td>
							<td>${price }</td>
<c:set var="totalMessagePrice"><fmt:formatNumber minFractionDigits="2"  value="${numberMessages*price }" /></c:set>

							<td><div id="totalMessagesPrice">${totalMessagePrice }</td>
								</tr>
									<tr>
							<td>Information Pages</td>
							<td><div id="totalInfos">${InfoNumber}</div></td>
							<td>${price }</td>
<c:set var="totalInformationPagesPrice" ><fmt:formatNumber minFractionDigits="2"  value="${InfoNumber*price }" /></c:set> 

							<td><div id="totalInfosPrice">${totalInformationPagesPrice }</div></td>
								</tr>
									<tr>
							<td>Events</td>
							<td><div id="totalEvents">${EventNumber}</div></td>
							<td>${price }</td>
<c:set var="totalEventPrice"><fmt:formatNumber minFractionDigits="2"  value="${EventNumber*price }"  /></c:set>

							<td><div id="totalEventsPrice">${totalEventPrice}</div></td>
								</tr>
								
									<tr>
							<td>Collaborations</td>
							<td><div id="totalCollaborations">0</div></td>
							<td>${price }</td>
<c:set var="totalCollaborationPrice"><fmt:formatNumber minFractionDigits="2"  value="${0*price }"  /></c:set>

							<td><div id="totalCollaborationsPrice">0</div></td>
								</tr>
									<tr>
							<td>Recommended Activites</td>
							<td><div id="totalRecommended">${RecommendedEvents}</div></td>
							<td>${price }</td>
<c:set var="totalRecommendedPrice"><fmt:formatNumber minFractionDigits="2"  value="${RecommendedEvents*price }" /></c:set>

							<td><div id="totalRecommendedsPrice">${totalRecommendedPrice }</div></td>
								</tr>
									<tr>
							<td>Payments</td>
							<td><div id="totalPayments">${ActTransactions}</div></td>
							<td>${price }</td>
							<c:set var="totalPaymentsPrice"><fmt:formatNumber minFractionDigits="2"  value="${ActTransactions*price }" /></c:set>
							
							<td><div id="totalPaymentPrice">${totalPaymentsPrice}</div></td>
								</tr>
					
				</tbody>
			</table>
			<c:set var="grandTotal" ><fmt:formatNumber minFractionDigits="2"  value="${ totalSurveyPrice+totalRecommendedPrice+totalCollaborationPrice+totalEventPrice+totalInformationPagesPrice+totalMessagePrice+totalConnectionPrice+totalPaymentsPrice}" /></c:set>
<style>
#grandTotals
{
font-size:35px;
float: right;
font-weight: bold;
}
#grands
{
font-size:35px;
float: right;
font-weight: bold;
}
.payButton
{
font-size:35px;
float: right;
font-weight: bold;
}
#ajaxBusy {
  display: none;
  background: #f6f6f6 url(https://s3.amazonaws.com/ccmeresources/000RuralPDF/process/ajax-loader.gif) no-repeat center center;
  background-size: 80px;
  margin: 0px 0px 0px -20px; /* left margin is half width of the div, to centre it */
  padding: 30px 10px 10px 10px;
  font-size: 50px;
  position: absolute;
  left: 40%;
  top: 325px;
  width: 500px;
  height: 150px;
  text-align: center;
  border: 1px solid #f78d3e;

}
</style>
			<hr>
			<div id="grandTotals">Total:$${grandTotal}
			
			</div><br><br>
			<div class="payButton">
			<a class="btn btn-primary"
								href="<c:url value='/admin/pay'/>">Pay Now</a>
			</div>
			</div>
			
			</section>
				<script type= "text/javascript">
		
    function callTheDetail(){
    	if($("select#monthSelector option:selected").val()){
      getID= $("select#monthSelector option:selected").val();
    	}
    	else{
       getID = $("select#monthWholeSelector option:selected").val();
    	}
        var getYearId = $("select#yearSelector option:selected").val();
       if(getYearId ==''){
    	   getYearId=2015;
       }
       getYearId = '/'+getYearId;
        //var checkOutDate= $("#checkOutDate").val();
		console.log("Month id is: " + getID);
		console.log("Year is: "+getYearId);
		
		if(getID=='All'){
			return all();
		}
		
		var actionUrl = "<c:url value='/getMonthlyInvoice/'/>"
		
      $.ajax({
            type: "GET",
          
            url: actionUrl+getID+getYearId,
            
            success: function(json) {
  				var text = json;
		//	var text = '{"Message":36,"Survey":0,"Infos":0,"Events":10,"RecommendedEvents":0,"connections":111}';
				console.log(text);
					obj = JSON.parse(text);	
					
					document.getElementById('totalSurveys').innerHTML = obj.Survey;
                  document.getElementById('totalSurveysPrice').innerHTML = Math.round(obj.Survey*${price}* 100) / 100;
                   document.getElementById('totalConnections').innerHTML = obj.connections;
                   document.getElementById('totalConnectionsPrice').innerHTML =   Math.round(obj.connections*${price}* 100) / 100;
                   
                  document.getElementById('totalMessages').innerHTML = obj.Message;
                  document.getElementById('totalMessagesPrice').innerHTML = Math.round(obj.Message*${price}* 100) / 100;
                  document.getElementById('totalInfos').innerHTML = obj.Infos;
                  document.getElementById('totalInfosPrice').innerHTML = Math.round(obj.Infos*${price}* 100) / 100;
                  document.getElementById('totalEvents').innerHTML = obj.Events;
                  document.getElementById('totalEventsPrice').innerHTML = Math.round(obj.Events*${price}* 100) / 100;
                  //document.getElementById('totalCollaborations').innerHTML;
                //  document.getElementById('totalCollaborationsPrice').innerHTML;
                  
                  document.getElementById('totalRecommended').innerHTML = obj.RecommendedEvents;
                  document.getElementById('totalRecommendedsPrice').innerHTML = Math.round(obj.RecommendedEvents*${price}* 100) / 100;
                  document.getElementById('totalPayments').innerHTML =obj.Payments;
                  document.getElementById('totalPaymentPrice').innerHTML=  Math.round(obj.Payments*${price}* 100) / 100;
                  document.getElementById('grandTotals').innerHTML= "Total:$" + (Math.round(obj.Survey*${price}* 100) / 100 + Math.round(obj.Events*${price}* 100) / 100 + Math.round(obj.connections*${price}* 100) / 100 + Math.round(obj.Message*${price}* 100) / 100 + Math.round(obj.Infos*${price}* 100) / 100 + Math.round(obj.RecommendedEvents*${price}* 100) / 100).toFixed(2);
                  
                  //document.getElementById("g").innerText=Math.round(obj.Message*${price}* 100) / 100;;
                      }
       });
		
		$('body').append('<div id="ajaxBusy"><p id="ajaxBusyMsg">Please wait...</p></div>');

		
    	 $(document).ajaxStart(function () {
     	    $('#ajaxBusy').show();
     	    
     	  }).ajaxStop(function () {
     	    $('#ajaxBusy').hide();
     	  });
     	 
    }
 
</script>

</body>
</html>