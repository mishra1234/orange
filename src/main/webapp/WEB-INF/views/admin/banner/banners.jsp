<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<script>
		$(document).ready(function() {
			$('#bannerTable').dataTable({
				"paging" : false,
				"info" : false,
				"order" : [ [ 0, "desc" ] ],
				"columnDefs" : [ {
					"visible" : false,
					"targets" : 0
				} ]
			});
		});
	</script>

	<section class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a> <a href="#"
						class="fa fa-times"></a>
				</div>
	
				<h2 class="panel-title">Banners</h2>
			</header>
			<div class="panel-body">
	
			<h5>
				<a href="<c:url value='/admin/banner/new/'/>">Add New Banner</a>
			</h5>
			
			<table id="bannerTable" class="table table-bordered table-striped mb-none">
				<thead>
					<tr>
						<th>image ID</th>
						<th>Banner Image</th>
						<th>Banner Title</th>
						<th>subtitle</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>
	
					<c:forEach var="bann" items="${listOfBanners}" varStatus="row">
						<tr>
							<td>${bann.id}</td>
							<td><a href="<c:url value='/admin/banner/${bann.id}'/>">
								<img src="<c:url value="${bann.imageUrl}" />" alt="TestDisplay" width="60" height="40"
								onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';" /></a></td>
							<td>${bann.title}</td>
							<td>${bann.subtitle}</td>
							<td><a href="<c:url value='/admin/banner/delete/${bann.id}'/>"> <i class="fa fa-minus-circle"></i></a> 
								<a href="<c:url value='/admin/banner/editBanner/${bann.id}'/>"><i class="fa fa-edit"></i> </a> 
								<a href="<c:url value='/admin/banner/${bann.id}'/>"> <i class="fa fa-tasks"></i></a></td>
						</tr>
					</c:forEach>
	
				</tbody>
			</table>
	
			<h5>
				<c:set var="curr" value="${curr}" />
				<c:choose>
					<c:when test="${curr <= 10 }">
						<c:forEach var="num" items="${numbers}" varStatus="row" begin="0"
							end="10">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/banners/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/banners/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<a class="btn" href="<c:url value='/admin/banners/${next}'/>">Next</a>
					</c:when>
					<c:when test="${curr>10 && curr<(hmany-5)}">
						<a class="btn" href="<c:url value='/admin/banners/${prev}'/>">Prev</a>
						<c:forEach var="num" items="${numbers}" varStatus="row"
							begin="${curr-5}" end="${curr+5}">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/banners/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/banners/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<input id="curr" type="hidden" name="curr" value="${next}" />
						<a class="btn" href="<c:url value='/admin/baners/${next}'/>">Next</a>
					</c:when>
					<c:otherwise>
						<a class="btn" href="<c:url value='/admin/banners/${prev}'/>">Prev</a>
						<c:forEach var="num" items="${numbers}" varStatus="row"
							begin="${curr-8}" end="${hmany+1}">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/banners/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/banners/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</h5>
		</div>
	</section>
</body>

</html>