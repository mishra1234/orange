<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>
<body>
	<script>$(document).ready(function() {$('#bannerTable').dataTable( {"paging": false, "info": false, "order": [[ "acc.id","desc"]]});});</script>
	
	<h4>List of banners on the event:   " ${myComm.contentTitle} "</h4>
	
	<h5><a class="btn" href ="<c:url value='/admin/event/addBanners/${myCommId}'/>">Add/Remove Banners to an Event</a></h5>
	
	<table id="bannerTable">
		<thead>
			<tr>
				<th>image ID</th>
				<th>Banner Image</th>
				<th>Banner Title</th>
				<th>subtitle</th>
			</tr>
		</thead>
		<tbody>
		
	<c:forEach var="bann" items="${myList}" varStatus="row">
			<tr>
				<td>${bann.id}</td>
				<td><a href= "<c:url value='/admin/banner/${bann.id}'/>"><img src="<c:url value="${bann.imageUrl}" />" alt="TestDisplay"
						 width="100" height="100" 
						 onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"/></a></td>
				<td>${bann.title}</td>
				<td>${bann.subtitle}</td>
          	</tr>
	</c:forEach>
</tbody>
</table>
</body>

</html>