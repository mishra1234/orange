<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />
	<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>
<body>  
	<div class="input-group pull-right" >
		<input type="text" class="form-control" placeholder="Search by words" name="srch-term" id="box"/>
	</div>
	</br></br>
	<div id="image_container" class="row">
		<ul class="portfolio-list sort-destination lightbox" data-sort-id="portfolio" data-plugin-options="{&quot;delegate&quot;: &quot;a&quot;, &quot;type&quot;: &quot;image&quot;, &quot;gallery&quot;: {&quot;enabled&quot;: true}}" 
		style="position: relative; height: 494px;">
			<li style="position: absolute; left: 0px; top: 0px;">
				<c:forEach var="icon" items="${iconsB}" varStatus="row">
					<div class="portfolie col-sm-6 col-md-4">
						<p style="display: block;">
							<input id="mytxt" type="image" alt="125x125" src='${icon.iconUrl}'class="thumbnail" width="200" height="200"/>
							<span class="thumb-info-title"><span class="thumb-info-type">${icon.contentTitle}</span></span>
						</p>
					</div>
				</c:forEach>
			</li>
		</ul>
	</div>
	
	<script>
		    $("div#image_container .thumbnail").click(function () {
		        $("div#image_container .thumbnail").css("border", "0");
		        $(this).css("border", "14px double green");
		        val = $(this).attr("src"); 
		    	localStorage.setItem("myIcon", val);
		    	parent.$.colorbox.close();
		    	return false;
		    });
		    $('#box').keyup(function(){
		    	   var valThis = $(this).val().toLowerCase();
		    	    if(valThis == ""){
		    	        $('.portfolie > p').show();           
		    	    } else {
		    	        $('.portfolie > p').each(function(){
		    	            var text = $(this).text().toLowerCase();
		    	            (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
		    	        });
		    	   };
		    	});
	</script>
	
</body>
</html>

