<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

	<link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />
	<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/theme.css"/>">	
	<link rel="stylesheet" href="<c:url value="/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/select2/select2.css"/>">
<script src="<c:url value='/scripts/lib/sweet-alert.min.js'/>"></script>
 <link rel="stylesheet" href="<c:url value='/scripts/lib/sweet-alert.css'/>" type="text/css" />

<!-- Compose Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="<c:url value='/admin/inbox/send'/>" id="modalForm">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Recipient:</label>
            <select name="layerMultiSelect" id="layerMultiSelect" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px">
            <optgroup label="Communities">
				<c:forEach var="listValue" items="${userLayers}">
					<option value=${listValue.id}--Layer>${listValue.name}</option>
				</c:forEach>
			</optgroup>	
			<optgroup label="Users">
				<c:forEach var="listValue" items="${userAccounts}">
					<option value=${listValue.id}--User>${listValue.name}</option>
				</c:forEach>
			</optgroup>
			</select>
          </div>
          
          <!-- Subject -->
          <div class="form-group">
            <label for="message-text" class="control-label">Subject:</label>
            <input class="form-control" id="subject">
          </div>
          
          <div class="form-group">
            <label for="message-text" class="control-label">Message:</label>
            <textarea class="form-control" id="textareaBody"></textarea>
          </div>
          <br>
          
          <!-- BANNER -- IMAGE UPLOAD -->
		  <div id="dropzoneBanner">	
			  <div class="form-group">
				<label class="col-sm-3 control-label" for="imageUrl">Banner Image</label>
				<div class="col-sm-9">
					<div class="input-group mb-md"> 
						<input id="bannerImage" name="imageUrl" type="text" class="form-control"/>
						<span class="input-group-btn">
							<a class="iframeBanner btn btn-success" href="<c:url value='/admin/medialibrary/imagesBanners'/>">
							<abbr title="Click the button if want a banner from the media library">Library</abbr></a></span>
			  </div></div></div>
			  <div class="form-group">
				<label class="col-sm-3 control-label">Upload Banner </label>
				<div class="col-sm-9">
					<input id="fileuploadBanner" type="file" name="filesBanner[]" data-url="<c:url value="/file/uploadBanners?"/>">Drop file here
					<div id="progress" class="progress">
						<div class="progress-bar progress-bar-primary" id="progressbar" role="progressbar"  style="width: 0%;">Complete</div>
					</div>
					<div id="displayBanner"></div>
				</div>
			  </div>
			  <div class="form-group">
				<div class="col-sm-9">				
					<div style="width:200px; padding: 20px">
						
					</div>
				</div>
			  </div>
		  </div>
		  
		  <div class="form-group">
			  <label for="recipient-name" class="control-label">Promote Community:</label>
	          <select name="communityMultiSelect" id="communityMultiSelect" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px">
	            <optgroup label="Activities">
					<c:forEach var="listValue" items="${userLayers}">
						<option value=${listValue.id}--Layer>${listValue.name}</option>
					</c:forEach>
				</optgroup>	
			  </select>
		  </div>
		
		  <div class="form-group">
			  <label for="recipient-name" class="control-label">Promote Activity:</label>
	          <select name="activityMultiSelect" id="activityMultiSelect" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px">
	            <optgroup label="Activities">
					<c:forEach var="listValue" items="${activities}">
						<option value=${listValue.id}--Activity>${listValue.contentTitle}</option>
					</c:forEach>
				</optgroup>	
			  </select>
		  </div>
		  <br>
        </form>
      </div>
      <div class="modal-footer">
      	<input type="button" class="btn btn-default" data-dismiss="modal" value="Close"></input>
        <button type="submit" class="btn btn-primary" onClick="myConfirm()" id="submit">Send message</button>
      </div>
    </div>
  </div>
</div>
<!-- End of Compose Modal -->

<!-- <!-- 	<script> --> -->
<!-- // 		$('#exampleModal').on('shown.bs.modal', function (event) { -->
<!-- // 			  var button = $(event.relatedTarget) // Button that triggered the modal -->
<!-- // 			  //var recipient = button.data('whatever') // Extract info from data-* attributes -->
<!-- // 			  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback). -->
<!-- // 			  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead. -->
<!-- // 			  var modal = $(this) -->
<!-- // 			  modal.find('.modal-footer button').click(function() {  -->
<!-- // 				 	var recipientDetails = $("#layerMultiSelect").val(); -->
<!-- // 				 	var activityDetails = $("#activityMultiSelect").val(); -->
<!-- // 				 	var comDetails = $("#communityMultiSelect").val(); -->
<!-- // 				 	var bannerUrl=$("#bannerImage").val(); -->
<!-- // 				 	console.log("activity = "+activityDetails); -->
<!-- // 				 	console.log("community = " + comDetails); -->
<!-- // 				 	console.log("recipient = "+recipientDetails); -->
<!-- // 				 	var textMessage = modal.find('.modal-body textarea').val(); -->
<!-- // 				 	var textSubject = modal.find('.modal-body #subject').val(); -->
<!-- // 				 	console.log("Subject= " + textSubject); -->
<%-- // 					var controllerurl= "<c:url value='/admin/inbox/send'/>"; --%>
		
<!-- // 					var jqxhr = $.ajax({ -->
<!-- // 						  type: "POST", -->
<!-- // 							url: controllerurl, -->
<!-- // 							data: JSON.stringify({ recipient: recipientDetails, subject: textSubject, message: textMessage, activities: activityDetails, communities:comDetails, banner: "\""+bannerUrl+"\""}), -->
<!-- // 							dataType: "json", -->
<!-- // 							beforeSend: function(xhr) { -->
<!-- // 							xhr.setRequestHeader("Accept", "application/json"); -->
<!-- // 							xhr.setRequestHeader("Content-Type", "application/json"); -->
<!-- // 							} -->
<!-- // 						    }) -->
<!-- // 						  .always(function( response ) { -->
<!-- // 							    if ( response.responseText.indexOf("Success")>-1 ) { -->
<!-- // 								    	$('#modalForm').trigger("reset"); -->
<!-- // 								    	$("#layerMultiSelect").val(''); -->
<!-- // 								    	$("#activityMultiSelect").val(''); -->
<!-- // 								    	$("#bannerImage").val(''); -->
<!-- // 								    	$('#exampleModal').hide(); -->
<!-- // 								    	$('#progressbar').css({"width": "0%"}); -->
<!-- // 								    	var z= response.responseText.split("--"); -->
<!-- // 								    	alert (z[1]); -->
<!-- // 							    	} -->
<!-- // 							    else -->
<!-- // 							    	{ -->
<!-- // 							    		var z= response.responseText.split("--"); -->
<!-- // 							    		alert (z[1]); -->
<!-- // 							    	} -->
<!-- // 							  }) ; -->
<!-- // 			  }); -->
			  
<!-- // 			  modal.find('.modal-footer input').click(function() {  -->
				  
<!-- // 					$('#modalForm').trigger("reset"); -->
<!-- // 			    	$("#layerMultiSelect").val(''); -->
<!-- // 			    	$("#activityMultiSelect").val(''); -->
<!-- // 			    	$("#bannerImage").val(''); -->
<!-- // 			    	$('#exampleModal').hide(); -->
<!-- // 			    	$('#progressbar').css({"width": "0%"}); -->
<!-- // 			  }); -->
<!-- // 			}); -->
<!-- <!-- 	</script> --> 


<script>


function myConfirm(){
	swal({   
		title: "Are you sure?",   
		text: "You want to send it?",   
		type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes",   
		cancelButtonText: "No",   
		closeOnConfirm: false,   
		closeOnCancel: false 
		}, 
		function(isConfirm){   
			if (isConfirm) 
			{     
				 	var recipientDetails = $("#layerMultiSelect").val();
				 	var activityDetails = $("#activityMultiSelect").val();
				 	var comDetails = $("#communityMultiSelect").val();
				 	var bannerUrl=$("#bannerImage").val();
				 	console.log("activity = "+activityDetails);
				 	console.log("community = " + comDetails);
				 	console.log("recipient = "+recipientDetails);
				 	var textMessage =$('#textareaBody').val();
				 	var textSubject = $('#subject').val();
				 	console.log("Subject= " + textSubject);
					var controllerurl= "<c:url value='/admin/inbox/send'/>";

					var jqxhr = $.ajax({ 
											  type: "POST", 
												url: controllerurl, 
												data: JSON.stringify({ recipient: recipientDetails, subject: textSubject, message: textMessage, activities: activityDetails, communities:comDetails, banner: "\""+bannerUrl+"\""}),
												dataType: "json",
												beforeSend: function(xhr) { 
												xhr.setRequestHeader("Accept", "application/json"); 
												xhr.setRequestHeader("Content-Type", "application/json"); 
												} 
											    })
					  .always(function( response ) {
						  if ( response) {
							// console.log(response.responseText.indexOf("Success"));
					    		
					    	   swal("Sent!", "success");
					    	}
						    else
					    	{
					   // 		var z= response.responseText.split("--");
					    		swal("Cancelled", "error");
					    	}
					  }) ;
				} 
			
			else {    
				swal("Cancelled", "error");   
				} 
			})
		}
$(document).ajaxStart(function(){
	swal({
		  title: "Please wait...",
		  text:  "While your request is been processed",
		  allowOutsideClick:false,
		  allowEscapeKey:false,
		  showConfirmButton: false
		});
});



</script>

	</script>


	<!-- fileupload scripts -->
	<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
	<script src="<c:url value='/scripts/myUploadImgsForBanners.js'/>"></script>


	<header class="page-header">
		<h2>Inbox</h2>
	</header>

	<!-- start: page -->
	<section class="content-with-menu mailbox">
		<div class="content-with-menu-container" data-mailbox data-mailbox-view="folder">
		
			<div class="inner-body mailbox-folder">
				<!-- START: .mailbox-header -->
				<!-- END: .mailbox-header -->
			
				<!-- START: .mailbox-actions -->
				<div class="mailbox-actions" >
					<ul class="list-unstyled m-none pt-lg pb-lg">
						<li class="ib mr-sm">
							<div class="btn-group">
								<a href="#" class="item-action fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#" id="sAll">All</a></li>
									<li><a href="#" id="sNone">None</a></li>
									<li><a href="#" id="sRead">Read</a></li>
									<li><a href="#" id="sUnRead">Unread</a></li>
								</ul>
							</div>
						</li>
						<li class="ib mr-sm">
							<a class="item-action fa fa-refresh" href="<c:url value='/admin/inbox'/>"></a>
						</li>
						<li class="ib">
							<a class="item-action fa fa-times text-danger" href="#"></a>
						</li>
						<li class="ib">
							<a class="item-action fa fa-plus-square text-danger" data-toggle="modal" data-target="#exampleModal"></a>
						</li>
					</ul>
				</div>
				<!-- END: .mailbox-actions -->
			
				<div id="mailbox-email-list" class="mailbox-email-list">
					<div class="nano">
						<div class="nano-content">
							<ul id="unreadList" class="list-unstyled">
								<c:forEach var="message" items="${unreadmessages}" varStatus="row">
								<li class="unread" >
									<a href="<c:url value='/admin/inbox/view/${message.id}'/>">
										<div class="col-sender">
											<div class="checkbox-custom checkbox-text-primary ib">
												<input type="checkbox" id="mail1" class="checku">
												<label for="mail1"></label>
											</div>
											<p class="m-none ib">${message.accountByFromId.name}</p>
										</div>
										<div class="col-mail">
											<p class="m-none mail-content">
												<span class="subject">${message.subject} &nbsp;–&nbsp;</span>
												<c:set var="string1" value="${message.message}"/>
												<c:set var="string2" value="${fn:substring(string1, 0, 40)}" />
												<span class="mail-partial">${string2}</span>
											</p>
											<p class="m-none mail-date">${message.formattedDate}</p>
										</div>
									</a>
								</li>
			                    </c:forEach>
			                    </ul>
			                    <ul id="readList" class="list-unstyled">
			                    
			                    <c:forEach var="message" items="${readmessages}" varStatus="row">
								<li>
									<a href="<c:url value='/admin/inbox/view/${message.id}'/>">
										<i class="mail-label" style="border-color: #EA4C89"></i>
										<div class="col-sender">
											<div class="checkbox-custom checkbox-text-primary ib">
												<input type="checkbox" id="mail2" class="checkr">
												<label for="mail2"></label>
											</div>
											<p class="m-none ib" style="width:20%;">${message.accountByFromId.name}</p>
										</div>
										<div class="col-mail">
											<p  style="width:77%;" class="m-none mail-content">
												<span class="subject">${message.subject} &nbsp;–&nbsp;</span>
												<c:set var="string1" value="${message.message}"/>
												<c:set var="string2" value="${fn:substring(string1, 0, 40)}" />
												<span class="mail-partial">${string2}</span>
											</p>
											<i class="mail-attachment fa fa-paperclip"></i>
											<p class="m-none mail-date">${message.formattedDate}</p>
										</div>
									</a>
								</li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
					
	<script type="text/javascript">
	jQuery(document).ready(function(){
	
		jQuery('#sAll').click(function(){
			console.log('all check');
			console.log(jQuery(".checku").prop("checked",true));
			console.log(jQuery(".checkr").prop("checked",true));
	
		});
	
	
			jQuery('#sNone').click(function(){
				
				console.log('None check');
				console.log(jQuery(".checku").prop('checked',false));
				console.log(jQuery(".checkr").prop('checked',false));
			
			});
	
			jQuery('#sRead').click(function(){
			
				console.log('Read check');
				console.log(jQuery(".checkr").prop('checked',true));
				console.log(jQuery(".checku").prop('checked',false));
				console.log('debug');
			});
	
	
	
	
			jQuery('#sUnRead').click(function(){
				
				console.log('UnRead check');
				console.log(jQuery(".checkr").prop('checked',false));
				console.log(jQuery(".checku").prop('checked',true));
			
			});
			
	
	
	});
	</script>
	
	<script>
		$ (document).ready(function(){
		    
			// COLORBOX - IFRAME
			
			$(".iframeBanner").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var otherbg = $.colorbox.element().data('otherbg');
		            $('#cboxLoadedContent').css('backgroundColor', otherbg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myBanner;
					$('#bannerImage').val(test);
					$("#displayBanner").empty();
		       	 	$("#displayBanner").append('<img src="'+test+'" width="250px" height="125px"/>');
				}
			});


		});
	</script>
