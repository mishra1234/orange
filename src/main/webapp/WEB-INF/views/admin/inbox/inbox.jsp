<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


 <script src="<c:url value='/scripts/lib/sweet-alert.min.js'/>"></script>
 <link rel="stylesheet" href="<c:url value='/scripts/lib/sweet-alert.css'/>" type="text/css" />

<style>
body{ margin-top:50px;}
.nav-tabs .glyphicon:not(.no-margin) { margin-right:10px; }
.tab-pane .list-group-item:first-child {border-top-right-radius: 0px;border-top-left-radius: 0px;}
.tab-pane .list-group-item:last-child {border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;}
.tab-pane .list-group .checkbox { display: inline-block;margin: 0px; }
.tab-pane .list-group input[type="checkbox"]{ margin-top: 2px; }
.tab-pane .list-group .glyphicon { margin-right:5px; }
.tab-pane .list-group .glyphicon:hover { color:#FFBC00; }
a.list-group-item.read { color: #222;background-color: #F3F3F3; }
hr { margin-top: 5px;margin-bottom: 10px; }
.nav-pills>li>a {padding: 5px 10px;}

.ad { padding: 5px;background: #F5F5F5;color: #222;font-size: 80%;border: 1px solid #E5E5E5; }
.ad a.title {color: #15C;text-decoration: none;font-weight: bold;font-size: 110%;}
.ad a.url {color: #093;text-decoration: none;}
</style>



<!-- Compose Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="<c:url value='/admin/inbox/send'/>" id="modalForm">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Recipient:</label>
            <select name="layerMultiSelect" id="layerMultiSelect" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px">
            <optgroup label="Communities">
				<c:forEach var="listValue" items="${userLayers}">
					<option value=${listValue.id}--Layer>${listValue.name}</option>
				</c:forEach>
			</optgroup>	
			<optgroup label="Users">
				<c:forEach var="listValue" items="${userAccounts}">
					<option value=${listValue.id}--User>${listValue.name}</option>
				</c:forEach>
			</optgroup>
			</select>
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
					<!-- Upload files -->		
			<div class="form-group">
						<label class="col-sm-3 control-label" for="imageUrl">Banner Image</label>
						<div class="col-sm-9">
							<input id="bannerImage" name="imageUrl" type="text" class="form-control"/>
					</div>
			</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Banner </label>
						<div class="col-sm-9">
							<input id="fileuploadBanner" type="file" name="filesBanner[]" data-url="<c:url value="/file/uploadBanners?"/>">
							Drop file here
							<div id="progress" class="progress">
								<div class="progress-bar progress-bar-primary" id="progressbar" role="progressbar"  style="width: 0%;">Complete</div>
							</div>
					</div>
					</div>
				<!-- End of upload files -->
				  <label for="recipient-name" class="control-label">Activities:</label>
            		<select name="activityMultiSelect" id="activityMultiSelect" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px">
			            <optgroup label="Activities">
							<c:forEach var="listValue" items="${activities}">
								<option value=${listValue.id}--Layer>${listValue.contentTitle}</option>
							</c:forEach>
						</optgroup>	
					</select>
        </form>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-default" data-dismiss="modal" value="Close"></input>
        <button type="submit" class="btn btn-primary" id="submit">Send message</button>

      </div>
    </div>
  </div>
</div>

<!-- End of Compose Modal -->
<script>


$('#exampleModal').on('shown.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  //var recipient = button.data('whatever') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-footer button').click(function() { 
		 	var recipientDetails = $("#layerMultiSelect").val();
		 	var activityDetails = $("#activityMultiSelect").val();
		 	console.log("activity = "+activityDetails);
		 	console.log("recipient = "+recipientDetails);
		 	var textMessage = modal.find('.modal-body textarea').val();
			var controllerurl= "<c:url value='/admin/inbox/send'/>";

			var jqxhr = $.ajax({
				  type: "POST",
					url: controllerurl,
					data: JSON.stringify({ recipient: recipientDetails, message: textMessage, activities: activityDetails}),
					dataType: "json",
					beforeSend: function(xhr) {
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
					}
				    })
				  .always(function( response ) {
					    if ( response.responseText.indexOf("Success")>-1 ) {
						    	$('#modalForm').trigger("reset");
						    	$("#layerMultiSelect").val('');
						    	$("#activityMultiSelect").val('');
						    	$("#bannerImage").val('');
						    	$('#exampleModal').hide();
						    	$('#progressbar').css({"width": "0%"});
						    	var z= response.responseText.split("--");
						    	//alert (z[1]);
						    	//todo modalbox
						    	swal("Sent", z[1], "success");
					    	}
					    else
					    	{
					    		var z= response.responseText.split("--");
					    		//alert (z[1]);
					    		swal("Error", z[1], "success");
					    	}
					  }) ;
	  });
	  
	  modal.find('.modal-footer input').click(function() { 
		  
			$('#modalForm').trigger("reset");
	    	$("#layerMultiSelect").val('');
	    	$("#activityMultiSelect").val('');
	    	$("#bannerImage").val('');
	    	$('#exampleModal').hide();
	    	$('#progressbar').css({"width": "0%"});
	  });
	})
</script>

		<!-- fileupload scripts -->
		<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadImgsForBanners.js'/>"></script>

<!-- End of compose Modal -->



<section class="content-with-menu mailbox" id="inboxSection">
<div class="container">
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <div class="btn-group">
                <button type="button" class="btn btn-primary">
                    Inbox
                </button>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <a href="#" class="btn btn-danger btn-sm btn-block" data-toggle="modal" data-target="#exampleModal">COMPOSE</a>
            <hr />
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"><span class="badge pull-right"></span> Inbox </a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <!-- Nav tabs -->
            <ul id="pageTab" class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span>Primary</a></li>             
            </ul>
            <!-- Tab panes -->
            <div id="pageTabContent class="tab-content" >
                <div class="tab-pane fade in active" id="home">
                    <div class="list-group">
                    <c:choose>
						    <c:when test="${fn:length(messages) > 0}">
						      <c:forEach var="message" items="${messages}" varStatus="row">
			                        <a href="#" class="list-group-item">
			                            <div class="checkbox">
			                                <label>
			                                    <input type="checkbox">
			                                </label>
			                            </div>
			                            <span class="glyphicon glyphicon-star-empty"></span>
			                            <span class="name" style="min-width: 120px;display: inline-block;">${message.accountByFromId.name}</span> 
			                            <span class="">${message.subject}</span>
										<c:set var="string1" value="${message.message}"/>
										<c:set var="string2" value="${fn:substring(string1, 0, 100)}" />
			                            <span class="text-muted" style="font-size: 11px;">- ${string2}</span> 
			                            <span class="badge">${message.formattedDate}</span> 
			                            <span class="pull-right">
			                            	<span class="glyphicon glyphicon-paperclip">
			                                </span>
			                            </span>
			                         </a>
                     			</c:forEach>
						    </c:when>
						    <c:otherwise>
						        <a href="#" class="list-group-item">
						        There are no messages in your inbox.
						        </a>
						    </c:otherwise>
					</c:choose>
                   
                    </div>
                </div>
            </div>
            <!-- Ad -->
            <div class="row-md-12">
            </div>
        </div>
    </div>
</div>					
</section>