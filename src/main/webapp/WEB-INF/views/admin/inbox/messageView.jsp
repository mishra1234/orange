<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


								<div class="mailbox-email-header mb-lg">
									<h3 class="mailbox-email-subject m-none text-light">
										${message.subject}
									</h3>
							
									<p class="mt-lg mb-none text-md">From <a href="#">${message.accountByFromId.name}</a> to <a href="#">You</a>, started on July, 05, 2014</p>
								</div>
								<div class="mailbox-email-container">
									<div class="mailbox-email-screen">
										<div class="panel">
											<div class="panel-heading">
												<div class="panel-actions">
													<a href="javascript:history.go(-1)" class="fa fa-mail-reply"></a>
												</div>
												<p class="panel-title">Content</p>
											</div>
											<div class="panel-body">
												<p>${message.message}</p>
											</div>
											<div class="panel-footer">
												<p class="m-none"><small>${message.formattedDate}</small></p>
											</div>
										</div>
									</div>
								</div>
