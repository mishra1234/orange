<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<body>

	<script>
		(function( $ ) {
			'use strict';
			var datatableInit = function() {
				var $table = $('#messageTable');
				$table.dataTable({
					sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
					oTableTools: {
						sSwfPath: $table.data('swf-path'),
						aButtons: [{
								sExtends: 'pdf',
								sButtonText: 'PDF'
								},{
								sExtends: 'csv',
								sButtonText: 'CSV'
								},{
								sExtends: 'xls',
								sButtonText: 'Excel'
								},{
								sExtends: 'print',
								sButtonText: 'Print',
								sInfo: 'Please press CTR+P to print or ESC to quit'
								}]
					}, 
					"paging":false,
					"info":false,
					"order":[[0,"desc"]], 
					"columnDefs":[{ "visible": false, "targets": 0 }],
				});
			};
	
			$(function() {
				datatableInit();
			});
	
		}).apply( this, [ jQuery ]);
	</script>
	
<section class="panel">
	<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Messages</h2>
	</header>
	<div class="panel-body">
	
		<h5> 
			<a href ="<c:url value='/admin/message/new/'/>">Add New Message</a> 
		</h5>
		<table id="messageTable" class="table table-bordered table-striped mb-none" 
		data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
			<thead>
				<tr>
					<th>Id</th>
					<th>From </th>
					<th>To </th>
					<th>Subject</th>
					<th>Date Sent</th>
					<th>Read Status</th>
					<th>Community Id</th>
					<th>Activity Id</th>
					<th>   Options        </th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="mess" items="${myMessagesList}" varStatus="row">
						<tr>
							<td>${mess.id}</td>
							<td>${mess.accountByFromId.name}</td>
							<td>${mess.accountByToId.name}</td>
							<td>${mess.subject}</td>
							<td>${mess.dateSent}</td>
							<td><c:choose>
									<c:when test="${mess.isRead}">Read</c:when>
									<c:otherwise>Unread</c:otherwise>
								</c:choose></td>
							<td>${mess.layerId}</td>
							<td>${mess.activityId}</td>
							<td>    
							    <a href="<c:url value='/admin/message/delete/${mess.id}'/>"> <i class="fa fa-minus-circle"></i> </a> 
			                    <a href="<c:url value='/admin/message/editMessage/${mess.id}'/>"><i class="fa fa-edit"></i> </a> 
			                    <a href="<c:url value='/admin/message/${mess.id}'/>"><i class="fa fa-tasks"></i></a>
			                </td>
			          	</tr>
				</c:forEach>
			</tbody>
		</table>
	
		<h5> 
		  <c:set var="curr" value="${curr}"/>
		  <c:choose>
			<c:when test="${curr <= 5 }"> 
				<c:forEach var="num" items="${numbers}" varStatus="row" begin="0" end="10">
					<c:choose>
					  	<c:when test="${num==curr}">
					    	<a class="btn btn-primary btn-lg" href="<c:url value='/admin/messages/${num}'/>">${num}</a>
					    </c:when>
					    <c:otherwise>
							<a class="btn" href="<c:url value='/admin/messages/${num}'/>">${num}</a>
						</c:otherwise> 
					</c:choose>
				</c:forEach>
			</c:when>
			<c:when test="${curr>10 && curr<(hmany-5)}">
			 	<a class="btn" href="<c:url value='/admin/messages/${prev}'/>">Prev</a>
				<c:forEach var="num" items="${numbers}" varStatus="row" begin="${curr-5}" end="${curr+5}">
					<c:choose>
						<c:when test="${num==curr}">
					    	<a class="btn btn-primary btn-lg" href="<c:url value='/admin/messages/${num}'/>">${num}</a> 
					    </c:when>
					    <c:otherwise>
							<a class="btn" href="<c:url value='/admin/messages/${num}'/>">${num}</a>
						</c:otherwise> 
					</c:choose>
			  	</c:forEach>
			  	<input id="curr" type="hidden" name="curr" value="${next}"/>
			  	<a class="btn" href="<c:url value='/admin/messages/${next}'/>">Next</a>
			</c:when>
			<c:otherwise>
			 	<a class="btn" href="<c:url value='/admin/messages/${prev}'/>">Prev</a>
			 	<c:forEach var="num" items="${numbers}" varStatus="row" begin="${curr-5}" end="${hmany+1}">
					<c:choose>
						<c:when test="${num==curr}">
					    	<a class="btn btn-primary btn-lg" href="<c:url value='/admin/messages/${num}'/>">${num}</a> 
					    </c:when>
					    <c:otherwise>
							<a class="btn" href="<c:url value='/admin/messages/${num}'/>">${num}</a>
						</c:otherwise> 
					</c:choose>
			  	</c:forEach>
			</c:otherwise>
		  </c:choose> 
		</h5>
	</div>
</section>

</body>

</html>