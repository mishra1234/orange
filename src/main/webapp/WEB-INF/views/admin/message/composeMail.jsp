<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>

<body>
<c:url var="var" value='/admin/message/send'/>
<form class="form-horizontal" id="form" method="POST" action="${var}">

<fieldset>

	<!-- Form Name -->
	<header class="panel-heading">
		<div class="panel-actions">
			<a href="#" class="fa fa-caret-down"></a> <a href="#"
				class="fa fa-times"></a>
		</div>

		<h2 class="panel-title">New Message</h2>
		<p class="panel-subtitle">Create a new message</p>
	</header>
	
	<!-- Hidden ID -->
	<input id="dateSent" name="dateSent" type="hidden" placeholder="dateSent" value="${currDateTime}">

	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-3 control-label" for="subject">Mail Subject<span class="required">*</span></label>
			<div class="col-md-9">
				<input id="subject" name="subject" type="text" placeholder="Subject" class="form-control">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="message">Mail Text<span class="required">*</span>
			</label>
			<div class="col-md-9">
				<textarea id="message" name="message" rows="6" cols="25"></textarea>
			</div>
		</div>
	
		<div class="form-group">
			<label class="col-sm-3 control-label" for="layerId">Community to Send Mail (Users of)</label>
			<div class="col-md-9">
				<select id="layerId" name="layerId" class="form-control" multiple="multiple" size="4"> 
					<c:forEach var="layer" items="${mailLayers}"  varStatus="row">
						<option value="${layer.id}">${layer.name}"</option>
					</c:forEach>
				</select>
			</div>
		</div>
							
	</div>
				
	<br />

	<!-- Button (Double) -->
	<footer class="panel-footer">
		<div class="row">
			<div class="col-sm-9 col-sm-offset-3">
				<a class="btn btn-default"
					href="<c:url value='/admin/messages'/>">Cancel</a>
				<button type="submit" id="Save" class="btn btn-primary">save</button>
			</div>
		</div>
	</footer>
					
</fieldset>
</form>

<script src="../../../assets/vendor/jquery/jquery.js"></script>
		<script src="../../../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../../../assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="../../../assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="../../../assets/vendor/pnotify/pnotify.custom.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../assets/javascripts/theme.init.js"></script>
		<script src="../../../assets/javascripts/forms/examples.wizard.js"></script>
	
</body>
</html>