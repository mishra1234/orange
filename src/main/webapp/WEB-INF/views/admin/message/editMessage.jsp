<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
 
<h3>Edit Message</h3>
<c:url var="save" value="/admin/message/save/${message.id}" />
<form:form modelAttribute="message" method="POST" action="../save">
 <table>
  <tr>
   <td><form:label for="fromId" path="fromId">Message From:</form:label></td>
   <td><form:input path="fromId.id"/></td>
   
  </tr>
     
  <tr>
   <td><form:label path="toId">Message To</form:label></td>
   <td><form:input path="toId.name"/></td>
  </tr>
 
 <tr>
   <td><form:label path="subject">Subject</form:label></td>	
   <td><form:input path="subject"/></td>
 </tr>
   
  <tr>
   <td><form:label path="message">Message:</form:label></td>
   <td><form:input path="message"/></td>
  </tr>
  
  <tr>
   <td><form:label path="dateSent">DateSent:</form:label></td>
   <td><form:input path="dateSent"/></td>
  </tr>
  
  <tr>
   <td><form:label path="read">read:</form:label></td>
   <td><form:input path="read"/></td>
  </tr>
 
   <tr>
   <td><form:label path="layerId">layerId:</form:label></td>
   <td><form:input path="layerId"/></td>
  </tr>
 
   <tr>
   <td><form:label path="activityId">activityId:</form:label></td>
   <td><form:input path="activityId"/></td>
  </tr>
 </table> 
 
 
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
	<button type="submit" value="Cancel" name="_cancel" class="btn btn-default">Cancel</button>
    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
  </div>
</div>

</form:form>
</body>
</html>