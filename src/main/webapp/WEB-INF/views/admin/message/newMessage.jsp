<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/pnotify/pnotify.custom.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>
		
			
</head>
<body>
<c:url var="var" value='/admin/message/save'/>


<form:form modelAttribute="message1" class="form-horizontal" id="form" method="POST" action="${var}">
<fieldset>

<!-- Form Name -->
			<section class="panel form-wizard">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a> <a href="#"
							class="fa fa-times"></a>
					</div>

					<h2 class="panel-title">New Message</h2>
					<p class="panel-subtitle">Create a new message</p>
				</header>
				<!-- Hidden ID -->
				<form:input type="hidden" path="id" value="" />
				<form:input id="isRead" name="isRead" type="hidden"
					placeholder="isRead" class="input-xlarge" required="" value="false"
					path="isRead" />
				<form:input id="deleted" name="deleted" type="hidden"
					placeholder="deleted" class="input-xlarge" required=""
					value="false" path="deleted" />
				<form:input id="dateSent" name="dateSent" type="hidden"
					placeholder="dateSent" class="input-xlarge" required=""
					path="dateSent" value="${currDateTime}" />


				<!-- Select Multiple -->
				<div class="panel-body">
					


					<!-- Text input-->
							<div class="form-group">
								<label class="col-sm-3 control-label" for="subject">Message
									Subject<span class="required">*</span>
								</label>
								<div class="col-md-9">
									<form:input id="subject" name="subject" type="pattern"
										autofocus="autofocus"
										data-plugin-maxlength="data-plugin-maxlength" maxlength="255"
										placeholder="Subject" class="form-control" required="required"
										path="subject" />
								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<label class="col-sm-3 control-label" for="message">Message
									Text<span class="required">*</span>
								</label>
								<div class="col-md-9">
									<form:textarea id="message" name="message" type="pattern"
										placeholder="Message" class="form-control" required="required"
										path="message" rows="6" cols="25" />
								</div>
							</div>
						

							<div class="form-group">
								<label class="col-sm-3 control-label" for="accountByFromId">Account
									From<span class="required">*</span>
								</label>
								<div class="col-md-9">
									<form:select id="accountByFromId" name="accountByFromId"
										class="form-control" required="required"
										path="accountByFromId.id">
										<form:options items="${accounts}" itemValue="id"
											itemLabel="name"></form:options>
									</form:select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="accountByToId">Account
									To<span class="required">*</span>
								</label>
								<div class="col-md-9">
									<form:select id="accountByToId" name="accountByToId"
										class="form-control" required="required" multiple="multiple"
										path="accountByToId.id" size="4">
										<form:options items="${accounts}" itemValue="id"
											itemLabel="name"></form:options>
									</form:select>
								</div>
							</div>
						
							<div class="form-group">
								<label class="col-sm-3 control-label" for="layerId">Community
									Id</label>
								<div class="col-md-9">
									<form:select id="layerId" name="layerId" class="form-control"
										multiple="multiple" path="layerId" size="4">
										<form:options items="${messageLayers}" itemValue="id"
											itemLabel="name"></form:options>
									</form:select>
								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<label class="col-sm-3 control-label" for="activityId">Activity
									Id</label>
								<div class="col-md-9">
									<form:select id="activityId" name="activityId"
										class="form-control" multiple="multiple" path="activityId"
										size="4">
										<form:options items="${messageActivities}" itemValue="id"
											itemLabel="contentTitle"></form:options>
									</form:select>
								</div>
							</div>
						</div>
				
				<br />

				<!-- Button (Double) -->
				<footer class="panel-footer">
							<div class="row">
								<div class="col-sm-9 col-sm-offset-3">
									<a class="btn btn-default"
										href="<c:url value='/admin/categories'/>">Cancel</a>
									<button type="submit" id="Save" class="btn btn-primary">save</button>
								</div>
							</div>
						</footer>
					
						</section>

		</fieldset>
	</form:form>
	<!-- Vendor -->
		<script src="../../../assets/vendor/jquery/jquery.js"></script>
		<script src="../../../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../../../assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="../../../assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="../../../assets/vendor/pnotify/pnotify.custom.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../assets/javascripts/theme.init.js"></script>
		<script src="../../../assets/javascripts/forms/examples.wizard.js"></script>
<script>
	
	
	(function() {
		

		'use strict';
		jQuery.validator.addMethod("pattern", function(value, element) {
		    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
		}, "Your data contain invalid characters");
		     
			  
		jQuery.validator.addMethod("urlIcon", function(value, element) {
			    	return isValidIconSize();
				}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only.");
		
		 function isValidIconSize(){

			 var imgWidth = $("#theImg").width();
			 var imgHeight = $("#theImg").height();
			 var extension = $('#theImg').attr("src").split('.').pop().toLowerCase();;
			 console.log("URL: " + extension);
			 console.log("width:" + imgWidth);
			 console.log("height:" + imgHeight);
			 if (imgHeight == 200 && imgWidth == 200 && (extension == "jpg" || extension == "png"))
				return true;
			
				 };   

				 
		// basic
		$("#form").validate({
			highlight: function( label ) {
				console.log("validation starts");
				$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function( label ) {
				$(label).closest('.form-group').removeClass('has-error');
				label.remove();
			},
			errorPlacement: function( error, element ) {
				var placement = element.closest('.input-group');
				if (!placement.get(0)) {
					placement = element;
				}
				if (error.text() !== '') {
					placement.after(error);
				}
			}
		});

	}).apply( this, [ jQuery ]);
	
	

</script>
	

	
</body>
</html>