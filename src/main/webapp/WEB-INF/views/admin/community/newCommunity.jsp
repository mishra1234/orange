<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
</head>

<body>

	<link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />
	<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/theme.css"/>">	
	<link rel="stylesheet" href="<c:url value="/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/select2/select2.css"/>">
	
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Community</h2>
		
		</header>
		
		<c:url var="var" value='/admin/community/save'/>

		<form:form modelAttribute="community" id="form" class="form-horizontal" method="POST" action="${var}">

		<fieldset>

			<!-- Hidden ID -->
			<form:input type="hidden" path="id" value=""/>
			<!-- <form:input type="hidden" path="iconUrl" value=""/>  -->
			<form:input type="hidden" id="deleted" name="deleted" placeholder="deleted" class="input-xlarge" required="" path="deleted" value="false"/>
			<form:input type="hidden" id="createdDatetime" name="createdDatetime" placeholder="createdDatetime" path="createdDatetime" value="${currentTS}"/>
			<form:input type="hidden" id="createdAccountId" name="createdAccountId" placeholder="createdAccountId" path="createdAccountId" value="${currentUser}"/>
			<input 		type="hidden" id="inputId" name="icon" value="${inputId}"/>

			<!-- Text input-->
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="Name">Name<span class="required">*</span></label>
					<div class="col-sm-9">
						<form:input id="Name" name="name" type="pattern" placeholder="Community Name" class="form-control" required="required" autofocus="autofocus" path="name"/>
					</div>
				</div>

				<div id="dropzone">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="iconUrl">Icon<span class="required">*</span></label>
						<div class="col-sm-9">
							<div class="input-group mb-md"> 
								<form:input id="theIconImage" name="iconUrl" type="text" class="form-control urlImage" value="" path="iconUrl"/>
								<span class="input-group-btn">
									<a class="iframeIcon btn btn-success" href="<c:url value='/admin/medialibrary/iconsEvents'/>">
										<abbr title="Click the button if want an icon from the media library">Library</abbr></a></span> 
					</div></div></div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Icon</label>
						<div class="col-sm-9">
							<input id="iconUploadComm" type="file" name="files[]" data-url="<c:url value="/file/upload?"/>">
								Drop files here
							<div id="progressCommIcon" class="progress">
								<div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">Complete</div>
							</div>
							<div id="displayIconDiv"></div>
				</div></div></div>
				<br>

				<!-- BANNER -- IMAGE UPLOAD -->
				<div id="dropzoneBanner">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="imageUrl">Banner Image</label>
						<div class="col-sm-9">
							<div class="input-group mb-md"> 
								<input id="bannerImage" name="imageUrl" type="text" class="form-control"/>
								<span class="input-group-btn">
									<a class="iframeBanner btn btn-success" href="<c:url value='/admin/medialibrary/imagesBanners'/>">
									<abbr title="Click the button if want a banner from the media library">Library</abbr></a></span>
					</div></div></div>
					<!-- Upload files for Banner -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Banner </label>
						<div class="col-sm-9">
							<input id="fileuploadBanner" type="file" name="filesBanner[]" data-url="<c:url value="../../../file/uploadBanners?"/>">
								Drop file here
							<div id="progress" class="progress">
								<div class="progress-bar progress-bar-primary" role="progressbar"  style="width: 0%;">Complete</div>
							</div>
							<div id="displayBanner"></div>
						</div>
					</div>
					<!-- BANNER -- FILE FILEUPLOAD TABLE-->
					<div class="form-group">
						<label class="col-sm-3 control-label">Banners</label>
						<div class="col-sm-9">				
							<div style="width:200px; padding: 20px">
								<table id="uploaded-files-banner"  class="table">
									<tr class="currB">
										<th>Banner Title</th>
										<th>Banner SubTitle</th>
										<th>Banner Image</th>
										<th>Option Remove</th>
									</tr>
									<c:forEach var="banner" items="${bannerList}" varStatus="row">
										<tr class="currB">
											<td><input value="${banner.title}" name="titleBanner[]"/></td>
											<td><input value="${banner.subtitle}" name="subtitleBanner[]"/></td>
											<td><input type="hidden" value="${banner.id}" name="bannerId[]"/>
												<input type="hidden" value="${banner.imageUrl}" name="bannerUrl[]"/>
												<img src="<c:url value="${banner.imageUrl}"/>" alt="TestDisplay" width="30" height="20" name="urlBanner"/></td>
											<td><input type="button" class="remo btn-xs btn-danger" value="Remove"/></td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>

				<!-- Select Multiple -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="account">Community Owner<span class="required">*</span></label>
					<div class="col-md-9">
						<form:select id="account" name="account" class="form-control" data-plugin-multiselect="data-plugin-multiselect" path="account.id">
							<form:options items="${listOfOwners}" itemValue="id" itemLabel="name" />
						</form:select>
					</div>
				</div>
				<hr>
					<div class="form-group">
						<label class="col-md-6 control-label""><strong> Please select either a Category or
							a Parent Community </strong><span class="required">*</span>
						</label>
						<div class="col-sm-6"></div>
					</div>



					<div class="form-group">
					<label class="col-md-3 control-label" for="pCat">Select Category</label>
					<div class="col-sm-9"> 
						<div class="input-group mb-md">
							<select id="pCat" name="pCat" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px; width:100%">
								<c:forEach var="listValue" items="${listOfCategories}">
									<c:forEach var="comm" items="${category}">
										<c:choose>
											<c:when test="${listValue.id == comm.id}">
												<option value="${listValue.id}" selected="selected">${listValue.name}</option>
											</c:when>
										</c:choose>
									</c:forEach>
									<option value="${listValue.id}">${listValue.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
				
				
				 
				<div class="form-group">
					<label class="col-sm-3 control-label">Parent Communities</label>
					<div class="col-sm-9">
					<div class="input-group mb-md">
							<select id="pCat" name="parent" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px; width:100%">
								<c:forEach var="listValue" items="${listOfCommunities}">
									<c:forEach var="comm" items="${community.layersForParentLayerId}">
										<c:choose>
											<c:when test="${listValue.id == comm.id}">
												<option value="${listValue.id}" selected="selected">${listValue.name}</option>
											</c:when>
										</c:choose>
									</c:forEach>
									<option value="${listValue.id}">${listValue.name}</option>
								</c:forEach>
								<option value="${community.id}">Home</option>
							</select>
						</div>
						
						
					</div>
				</div>

				<hr>
				<!-- Radio Buttons-->
				<div class="form-group">
					<label class="col-sm-3 control-label">Status<span class="required">*</span> </label>
					<div class="col-sm-9">
						<div class="radio-custom radio-primary">
							<form:radiobutton value="draft" path="status" name="status"/>
							<label>draft</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="active" path="status" name="status"/>
							<label>active</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="hidden" path="status" name="status"/>
							<label>Hidden</label>
						</div>
					</div>
				</div>
				<hr>

				<!-- Radio Buttons-->
				<div class="form-group">
					<label class="col-sm-3 control-label">Type</label>
					<div class="col-sm-9">
						<div class="radio-custom radio-primary">
							<form:radiobutton value="public" path="type" name="type"/>
							<label>Public</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="gated" path="type" name="type"/>
							<label>Gated</label>
						</div>

					</div>
				</div>
				<hr>
				<!-- Radio Buttons-->
				<div class="form-group">
					<label class="col-sm-3 control-label">Connectable</label>
					<div class="col-sm-9">
						<div class="radio-custom radio-primary">
							<form:radiobutton value="false" path="connectable" name="connect"/>
							<label>No</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="true" path="connectable" name="connect"/>
							<label>Yes</label>
						</div>
					</div>
				</div>

				<hr>
				<!-- Radio Buttons-->
				<div class="form-group">
					<label class="col-sm-3 control-label">Recommended</label>
					<div class="col-sm-9">
						<div class="radio-custom radio-primary">
							<form:radiobutton value="false" path="recommended" name="recomend"/>
							<label>No</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="true" path="recommended" name="recomend"/>
							<label>Yes</label>
						</div>
					</div>
				</div>
				<hr>	
			
				<!-- Button (Double) -->
				<footer class="panel-footer">
					<div class="row">
						<div class="col-sm-9 col-sm-offset-3">
							<a class="btn btn-default"
								href="<c:url value='/admin/categories'/>">Cancel</a>
							<button type="submit" id="Save" class="btn btn-primary">save</button>
						</div>
					</div>
				</footer>
			</div>
		</fieldset>
		</form:form>
		
		<!-- fileupload scripts -->
		<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadIconForCommunity.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadBannerForCommunity.js'/>"></script> 
		
		<script>
		$ (document).ready(function(){
			$('table[id="uploaded-files-banner"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.currB").remove();
			});
		    
			// COLORBOX - IFRAME
			$(".iframeIcon").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var bg = $.colorbox.element().data('bg');
		            $('#cboxLoadedContent').css('backgroundColor', bg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myIcon;
					$('#theIconImage').val(test);
					$("#displayIconDiv").empty();
		       	 	$("#displayIconDiv").append("<img id='theIconImage' src='"+test+"'/>");
				}
			});
			
			$(".iframeBanner").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var otherbg = $.colorbox.element().data('otherbg');
		            $('#cboxLoadedContent').css('backgroundColor', otherbg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myBanner;
					$('#bannerImage').val(test);
					$("#displayBanner").empty();
		       	 	$("#displayBanner").append("<img id='bannerImage' src='"+test+"'/>");
		       	 	$("#uploaded-files-banner").append($('<tr class="newB"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="subtitleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="bannerUrl[]" value="'+test+'"/>'+
													'<img src="'+test+'" width="30px" height="20px"/>'))
	                		.append($('<td/>').html('<input type="button" class="remo btn-xs btn-danger" value="Remove"/>'))
	                		);
				}
			});
			$('table[id="uploaded-files-banner"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newB").remove();
			});

		});
		</script>
		
</section>
</body>
</html>