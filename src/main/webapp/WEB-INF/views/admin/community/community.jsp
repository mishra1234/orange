<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<head>

</head>
<html>
<body>
<style type="text/css">
button
{
background-color:#f78d3e;
width:80px;
height:30px;
}
a
{
text-decoration:  none;
color:black;
}
a hover
{
 text-decoration:  none;
}
</style>
 <div class="text-right">
  <button type="submit" > <a style="text-decoration:  none;color:black;" class="view-more" href="<c:url value='/admin/communities'/>"> View All</a></button>
 </div>
 
<h3 style="margin-left:100px;"><strong>Community: </strong>${comm.name}</h3>
<br>
<div class="slider">
	<ul class="bxslider">
		<c:forEach var="bann" items="${Banners}" varStatus="row">
	  			<li>
	  			<img src="${bann.imageUrl}"  title="${bann.subtitle}">
	  				
	  			<div class="text-section">
                    	<p class="subtitle"><h6>${bann.title}</h6></p>
                    	<br><br>
                </div>
	  			</li>
	  	</c:forEach>
	  
	</ul>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.bxslider').bxSlider({
		  mode: 'fade',
		  captions: true,
		  auto: true,
		  slideWidth: 700
		});
	});
	</script>
	

</div>

<link rel="stylesheet" href="http://bxslider.com/lib/jquery.bxslider.css" type="text/css" />
<link rel="stylesheet" href="http://bxslider.com/css/github.css" type="text/css" />
<script src="http://bxslider.com/js/jquery.min.js"></script>
<script src="http://bxslider.com/lib/jquery.bxslider.js"></script>
<script src="http://bxslider.com/js/rainbow.min.js"></script>
<script src="http://bxslider.com/js/scripts.js"></script>
<style type="text/css">
.khush
{
 float:right;
    margin-top:55%;;
}
table{
   width: 70% ;
}
td
{
height: 40px;
font-size:15px;
}
</style>
	
<table id="communityTable">
    <tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Name:</td>				<td>${comm.name}</td></tr>
   	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Owner:</td>				<td> ${comm.account.name}</td></tr>
   	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Icon:</td>			<td><img src="${comm.iconUrl}"></td></tr>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Recommended:</td>		<td>${comm.recommended}</td></tr>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Connectable:</td>		<td>${comm.connectable}</td></tr>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Parent Community:</td>	<td>
								      <c:forEach var="ownerId" items="${comm.layersForParentLayerId}"  varStatus="status">
								      		Id ${ownerId.id}- ${ownerId.name}
								      </c:forEach>
      	  							</td></tr>
    <tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Status:			</td>	<td>${comm.status}</td></tr>
    <tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Type:			</td>	<td>${comm.type}</td></tr>	
</table>
<br>


 <h5> <button type="submit" style="width:150px; height:40px; margin-left:80px;" ><a  style="text-decoration:  none;color:black;" href="<c:url value='/admin/community/editCommunity/${comm.id}'/>">Edit Community</a></button></h6>
</body>

</html>