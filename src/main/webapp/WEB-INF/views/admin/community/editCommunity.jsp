<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
 
<h3>Edit Community</h3>
<c:url var="save" value="/admin/community/save/${community.id}" />
<form:form modelAttribute="community" method="POST" action="../save">
 <table>
  <tr>
   <td><form:label path="name">Name:</form:label></td>
   <td><form:input path="name"/></td>
  </tr>
     
  <tr>
   <td><form:label path="iconUrl">IconUrl</form:label></td>
   <td><form:input path="iconUrl"/></td>
  </tr>
 
 <tr>
   <td><form:label path="layersForParentLayerId">Parent Community</form:label></td>	
   <td><form:select multiple="true" path="layersForParentLayerId">
            <c:forEach items="layersForParentLayerId" var="rol">
                <c:set var="isSelected" value="false" />
                <c:forEach items="${myCommunityList}" var="comm">
                    <option value="${comm.id}"
                    
                    <c:forEach items="${comm.layersForParentLayerId}" var="parentLayer">
	                   	 <c:if test="${parentLayer.id==comm.id}"> 
	                   	 	selected="select"
	                    </c:if>
                    </c:forEach>
                    
                    >${comm.name} </option>
                    <c:set var="isSelected" value="true" />
                </c:forEach>
            </c:forEach>
        </form:select>
   		
   </td>
 </tr>
 
 <tr>
   <td><form:label path="account">Community Owner</form:label></td>	
   <td><form:input path="account"/></td>
 </tr>
  
  <tr>
   <td><form:label path="recommended">Recommended:</form:label></td>
   <td><form:input path="recommended"/></td>
  </tr>
  
  <tr>
   <td><form:label path="connectable">Connectable:</form:label></td>
   <td><form:input path="connectable"/></td>
  </tr>
 </table> 
 
 <tr>
   <td><form:label path="status">status</form:label></td>
   <td><form:input path="status"/></td>
  </tr>
  
  <tr>
   <td><form:label path="type">type</form:label></td>
   <td><form:input path="type"/></td>
  </tr>
 
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
<button type="submit" value="Cancel" name="_cancel" class="btn btn-default">Cancel</button>
    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
  </div>
</div>

</form:form>
</body>
</html>