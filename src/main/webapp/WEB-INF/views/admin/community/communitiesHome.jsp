<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<body>

<script>
(function( $ ) {

	'use strict';

	var datatableInit = function() {
		var $table = $('#communityTable');

		$table.dataTable({
			sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
			oTableTools: {
				sSwfPath: $table.data('swf-path'),
				aButtons: [
					{
						sExtends: 'pdf',
						sButtonText: 'PDF'
					},
					{
						sExtends: 'csv',
						sButtonText: 'CSV'
					},
					{
						sExtends: 'xls',
						sButtonText: 'Excel'
					},
					{
						sExtends: 'print',
						sButtonText: 'Print',
						sInfo: 'Please press CTR+P to print or ESC to quit'
					}
				]
			}, 
			"paging":false,
			"info":false,
			"order":[[0,"desc"]], 
			"columnDefs":[{ "visible": false, "targets": 0 }],
		});

	};

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);

</script>

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Communities</h2>
		</header>
		<div class="panel-body">

			<h5>
				<a class="btn btn-primary" href="<c:url value='/admin/community/new/'/>">Add new</a>
				<a class="btn btn-danger" href="#" data-toggle="modal" data-target="#myModalRemove">Remove Community</a>
			</h5>
			<table class="table table-bordered table-striped mb-none" id="communityTable" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
				<thead>
					<tr>
						<th>ID</th>
						<th>Icon</th>
						<th>Name</th>
						<th>Owner</th>
						<th>Parent Community</th>
						<th># Users</th>
						<th># Activities</th>
						<th>Type</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="community" items="${myCommunityList}"
						varStatus="row">
						<tr>
							<td>${community.id}</td>
							<td><a href="<c:url value='/admin/events/community/${community.id}'/>"><img
									src="${community.iconUrl}" width="25" height="25"
									onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
							<td>${community.name}</td>
							<td>${community.account.name}</td>
							<td><c:forEach var="parent" items="${community.layersForParentLayerId}" varStatus="status">
									${parent.name}
								</c:forEach></td>
							<td><a href="<c:url value='/admin/users/community/${community.id}'/>">
									${fn:length(community.accounts_1)}</a></td>
							<td><a href="<c:url value='/admin/events/community/${community.id}'/>">${community.numberActivities}</a></td>
							<td>${community.type}</td>
							<td>
								<a href="<c:url value='/admin/community/editCommunity/${community.id}'/>">
									<i class="fa fa-edit" title="Edit"></i>
								</a>
								<a href="<c:url value='/admin/community/${community.id}'/>">
									<i class="fa fa-tasks" title="View"></i>
								</a>
								<a href="<c:url value='/admin/community/delete/${community.id}'/>">
									<i class="fa fa-minus-circle text-warning" title="Deacivate"></i>
								</a> 
							</td>
						</tr>
					    
					</c:forEach>
				</tbody>
			</table>

			<h5>
				<c:choose>
					<c:when test="${curr != null}">
						<c:set var="curr" value="${curr}" />
						<c:choose>
							<c:when test="${curr <= 5 }">
								<c:forEach var="num" items="${numbers}" varStatus="row"
									begin="0" end="10">
									<c:choose>
										<c:when test="${num==curr}">
											<a class="btn btn-primary btn-lg"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:when>
										<c:otherwise>
											<a class="btn"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:when>
							<c:when test="${curr <= 10 && curr > 5}">
								<c:forEach var="num" items="${numbers}" varStatus="row"
									begin="0" end="10">
									<c:choose>
										<c:when test="${num==curr}">
											<a class="btn btn-primary btn-lg"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:when>
										<c:otherwise>
											<a class="btn"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<a class="btn"
									href="<c:url value='/admin/communities/${next}'/>">Next</a>
							</c:when>
							<c:when test="${curr>10 && curr<(hmany-5)}">
								<a class="btn"
									href="<c:url value='/admin/communities/${prev}'/>">Prev</a>
								<c:forEach var="num" items="${numbers}" varStatus="row"
									begin="${curr-5}" end="${curr+5}">
									<c:choose>
										<c:when test="${num==curr}">
											<a class="btn btn-primary btn-lg"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:when>
										<c:otherwise>
											<a class="btn"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<input id="curr" type="hidden" name="curr" value="${next}" />
								<a class="btn"
									href="<c:url value='/admin/communities/${next}'/>">Next</a>
							</c:when>
							<c:otherwise>
								<a class="btn"
									href="<c:url value='/admin/communities/${prev}'/>">Prev</a>
								<c:forEach var="num" items="${numbers}" varStatus="row"
									begin="${curr-8}" end="${hmany+1}">
									<c:choose>
										<c:when test="${num==curr}">
											<a class="btn btn-primary btn-lg"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:when>
										<c:otherwise>
											<a class="btn"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</c:when>
				</c:choose>
			</h5>
			
			</div>
			</section>
			
			<!-- Modal Form for Adding Users as Community Manager -->
		    <div class="modal fade" id="myModalRemove" role="dialog">
			    <div class="modal-dialog">
			    	<div class="modal-content">
				        <div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal">&times;</button>
				        	<h3 class="modal-title">Community Removal Alert !!!</h3>
				        </div>
				        <div class="modal-body">
					    	<div class="form-group">
					    		<label class="col-md-3 control-label">Select the Community to REMOVE</label>
					    		<input type="hidden" id="communityID" value="${community.id}">
								<div class="col-md-6">
									<select data-plugin-selectTwo class="form-control populate" id="selectComm">
							            <optgroup label="Communities">
											<c:forEach var="listValue" items="${myCommunityList}">
												<option value="${listValue.id}">${listValue.name}</option>
											</c:forEach>
										</optgroup>	
									</select>
								</div>
							</div>
				      	</div>
				      	<div class="modal-footer">
				        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        	<button type="button" class="btn btn-danger remComm">Remove</button>
				    	</div>
			        </div>
			    </div>
		    </div>	<!-- end of modal --> 
					    
			<script src="<c:url value='/scripts/lib/sweet-alert.min.js'/>"></script>
			<link rel="stylesheet" href="<c:url value='/scripts/lib/sweet-alert.css'/>" type="text/css" />
			
  			<script>
				$(document).ready(function(){
				    $(".remComm").click(function(){	
				    	var inviteUserDetails = $("#selectComm").val();    
						var x= $('#myModalRemove').modal('hide');
						var controllerurl= "<c:url value='/admin/remComm/request'/>";
						var jqxhr = $.ajax({
							  type: "POST",
								url: controllerurl,
								data: JSON.stringify({CommunityID: inviteUserDetails}),
								dataType: "json",
								beforeSend: function(xhr) {
								xhr.setRequestHeader("Accept", "application/json");
								xhr.setRequestHeader("Content-Type", "application/json");
								}
							    })
							  .always(function( response ) {
								    if ( response.responseText.indexOf("Success")>-1 ) {
								    	var z= response.responseText.split("--");
								    	swal("Community REMOVED!", z[1], "success");
								    	}
								    else
								    	{
								    	var z= response.responseText.split("--");
							    		swal("Action Cancelled", z[1], "error");
								    	}
								  }) ;
						location.reload();
					});
				});
			</script>
</body>
</html>