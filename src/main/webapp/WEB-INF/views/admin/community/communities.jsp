<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<body>
<script src="<c:url value='/scripts/lib/sweet-alert.min.js'/>"></script>
 <link rel="stylesheet" href="<c:url value='/scripts/lib/sweet-alert.css'/>" type="text/css" />


<script>

(function( $ ) {

	'use strict';

	var datatableInit = function() {
		var $table = $('#communityTable');

		$table.dataTable({
			sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
			oTableTools: {
				sSwfPath: $table.data('swf-path'),
				aButtons: [
					{
						sExtends: 'pdf',
						sButtonText: 'PDF'
					},
					{
						sExtends: 'csv',
						sButtonText: 'CSV'
					},
					{
						sExtends: 'xls',
						sButtonText: 'Excel'
					},
					{
						sExtends: 'print',
						sButtonText: 'Print',
						sInfo: 'Please press CTR+P to print or ESC to quit'
					}
				]
			}, 
			"paging":false,
			"info":false,
			"order":[[2,"asc"]], 
			"columnDefs":[{ "visible": false, "targets": 0 }],
		});

	};

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);

</script>

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Communities</h2>
		</header>
		<div class="panel-body">

			<h5>
				<a href="<c:url value='/admin/community/new/'/>">Add new</a>
			</h5>
			<table class="table table-bordered table-striped mb-none" id="communityTable" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
				<thead>
					<tr>
						<th>ID</th>
						<th>Icon</th>
						<th>Name</th>
						<th>Owner</th>
						<th>Type</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="community" items="${myCommunityList}"
						varStatus="row">
						<tr>
							<td>${community.id}</td>
							<td><a href="<c:url value='/admin/events/community/${community.id}/${community.name }'/>">
								<img src="${community.iconUrl}" width="25" height="25" onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
							<td>${community.name}</td>
							<td>${community.account.name}</td>
							<td>${community.type}</td>
							<td>
<%-- 							<a href="<c:url value='/admin/community/delete/${community.id}'/>"><i class="fa fa-minus-circle"></i></a>  --%>
	  						    <a href="#" onClick="myConfirm(${community.id})"><i class="deleteCheck" title="Delete"><i class="fa fa-minus-circle"></i></i></a>			
								<a href="<c:url value='/admin/community/editCommunity/${community.id}'/>"><i class="fa fa-edit"></i></a> 
 								<a href="<c:url value='/admin/community/${community.id}'/>"><i class="fa fa-tasks"></i></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<h5>
				<c:choose>
					<c:when test="${curr != null}">
						<c:set var="curr" value="${curr}" />
						<c:choose>
							<c:when test="${curr <= 5 }">
								<c:forEach var="num" items="${numbers}" varStatus="row"
									begin="0" end="10">
									<c:choose>
										<c:when test="${num==curr}">
											<a class="btn btn-primary btn-lg"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:when>
										<c:otherwise>
											<a class="btn"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:when>
							<c:when test="${curr <= 10 && curr > 5}">
								<c:forEach var="num" items="${numbers}" varStatus="row"
									begin="0" end="10">
									<c:choose>
										<c:when test="${num==curr}">
											<a class="btn btn-primary btn-lg"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:when>
										<c:otherwise>
											<a class="btn"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<a class="btn"
									href="<c:url value='/admin/communities/${next}'/>">Next</a>
							</c:when>
							<c:when test="${curr>10 && curr<(hmany-5)}">
								<a class="btn"
									href="<c:url value='/admin/communities/${prev}'/>">Prev</a>
								<c:forEach var="num" items="${numbers}" varStatus="row"
									begin="${curr-5}" end="${curr+5}">
									<c:choose>
										<c:when test="${num==curr}">
											<a class="btn btn-primary btn-lg"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:when>
										<c:otherwise>
											<a class="btn"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<input id="curr" type="hidden" name="curr" value="${next}" />
								<a class="btn"
									href="<c:url value='/admin/communities/${next}'/>">Next</a>
							</c:when>
							<c:otherwise>
								<a class="btn"
									href="<c:url value='/admin/communities/${prev}'/>">Prev</a>
								<c:forEach var="num" items="${numbers}" varStatus="row"
									begin="${curr-5}" end="${hmany+1}">
									<c:choose>
										<c:when test="${num==curr}">
											<a class="btn btn-primary btn-lg"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:when>
										<c:otherwise>
											<a class="btn"
												href="<c:url value='/admin/communities/${num}'/>">${num}</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</c:when>
				</c:choose>
			</h5>
			
			</div>
			</section>
			<script>

			function myConfirm(communityID){
				swal({   
					title: "Are you sure?",   
					text: "You want to delete it?",   
					type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   
					confirmButtonText: "Yes",   
					cancelButtonText: "No",   
					closeOnConfirm: false,   
					closeOnCancel: false 
					}, 
					function(isConfirm){   
						if (isConfirm) 
						{     
							console.log("communityID is ="+communityID);
						 	var action = "delete";
						 	//var realUrl = 
							var controllerurl= "<c:url value='/admin/community/delete/'/>";
							console.log("Controller URl is:"+ controllerurl+communityID);

							var jqxhr = $.ajax({
								  type: "POST",
									url: controllerurl+communityID,
									//data: JSON.stringify({ id: communityID}),
									dataType: "json",
									 beforeSend: function(xhr) {
									        xhr.setRequestHeader("Accept", "application/json");
									        xhr.setRequestHeader("Content-Type", "application/json");
									    }
								    })
								  .always(function( response ) {
									  if ( response.responseText.indexOf("Success")>-1 ) {
										 location.reload();
								    	 console.log($( usercontrol ).val());
								    	 console.log(response.responseText.indexOf("Success"));
								    		
								    	   swal("Deleted!", "success");
								    	}
									    else
								    	{
								   // 		var z= response.responseText.split("--");
								    		swal("Cancelled", "error");
								    	}
								  }) ;
							} 
						
						else {    
							swal("Cancelled", "error");   
							} 
						})
					}
			$(document).ajaxStart(function(){
				swal({
					  title: "Please wait...",
					  text:  "While your request is been processed",
					  allowOutsideClick:false,
					  allowEscapeKey:false,
					  showConfirmButton: false
					});
			});



			</script>
		
		
</body>
</html>