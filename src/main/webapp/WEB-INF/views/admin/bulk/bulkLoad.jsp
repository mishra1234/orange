<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
</head>

<body>	

	<link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />
	<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/theme.css"/>">	
	<link rel="stylesheet" href="<c:url value="/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/select2/select2.css"/>">
	

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> 
				<a href="#" class="fa fa-times"></a>
			</div>
			<h2 class="panel-title">Bulk Load</h2>
			<p class="panel-subtitle"><strong>Please select the default values for all the events</strong></p>
		</header>
		
		<c:url var="var" value="/admin/bulk/save"/>
		<form:form modelAttribute="newEvent" id="form" method="POST" action="${var}" enctype="multipart/form-data">

		<fieldset>
			<!-- Hidden IDs -->
			<input type="hidden" value="" name="titleFile[]"/>
			<input type="hidden" value="" name="descriptionFile[]"/>
			<input type="hidden" value="" name="fileUrl[]"/>
			<input type="hidden" value="" name="fileRemove[]"/>
			<input type="hidden" value="" name="fileId[]"/>
			<input type="hidden" value="" name="titleVideo[]"/>
			<input type="hidden" value="" name="descriptionVideo[]"/>
			<input type="hidden" value="" name="videoUrl[]"/>
			<input type="hidden" value="" name="videoRemove[]"/>
			<input type="hidden" value="" name="videoId[]"/>
			<input type="hidden" value="" name="videoIconUrl[]"/>
				
			<!-- -- Form Elements -- -->
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">* Mandatory fields</label>
				</div>
			</div>
			
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Enable Booking</label>
					<div class="col-sm-8">
						<div class="radio-custom radio-primary">
							<form:radiobutton value="false" checked="checked" path="bookable" /> <label>No</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="true" path="bookable" /> <label>Yes</label>
				</div></div></div> 

				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Recommend Events</label>
					<div class="col-sm-8">
						<div class="radio-custom radio-primary">
							<form:radiobutton value="false" checked="checked" path="activity.recommended" /> <label>No</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="true" path="activity.recommended" />
							<label>Yes</label>
				</div></div></div><br>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="activity.account.id">Event Manager
						<span class="required">*</span>
					</label>
					<div class="col-sm-3">
						<select id="accountId" name="accountId" class="form-control">
							<c:forEach items="${listOfOwners}" var="owner"> 
                				<option value="${owner.id}">${owner.name}</option>
            				</c:forEach>
						</select>
					</div>
				</div><br>
				
				<div class="form-group">
					<label class="col-sm-3 control-label" for="commodities">Communities
						<abbr title="Select commodity groups"><i
							class="fa fa-question-circle"></i></abbr>
					</label>
					<div class="col-sm-9">
						<div class="input-group mb-md">
							<select name="commodities" multiple
								data-plugin-selectTwo class="form-control populate"
								style="padding: 0px 0px; width:100%">
								<c:forEach var="listValue" items="${managedLayers}">
									<c:forEach var="comm" items="${activity.layers}">
										<c:choose>
											<c:when test="${listValue.id == comm.id}">
												<option value="${listValue.id}" selected="selected">${listValue.name}</option>
											</c:when>
										</c:choose>
									</c:forEach>
									<option value="${listValue.id}">${listValue.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div><br>

				<!-- ICON input-->
				<div id="dropzone">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="iconUrl">Icon
							<abbr title="Drag and drop an image close to the DROP FILES HERE to start uploading an Icon or just click on the Choose File button"><i class="fa fa-question-circle"></i></abbr> 
							<span class="required">*</span></label>
						<div class="col-sm-9">
							<div class="input-group mb-md"> 
								<form:input id="theIcon" name="iconUrl" type="text" class="form-control urlImage" value="" path="activity.iconUrl"/>
								<span class="input-group-btn">
									<a class="iframeIcon btn btn-success" href="<c:url value='/admin/medialibrary/iconsEvents'/>">
										<abbr title="Click the button if want an icon from the media library">Library</abbr>
									</a>
								</span> 
							</div>
						</div>
					</div>
					<!-- Upload files -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Icon</label>
						<div class="col-sm-9">
							<input id="fileuploadEvent" type="file" name="files[]" data-url="<c:url value="../../file/upload?"/>">
							Drop files here
							<div id="progressEventIcon" class="progress">
								<div class="progress-bar progress-bar-primaryIcon" role="progressbar"  style="width: 0%;">Complete</div>
							</div>
							<div id="displayIcon"></div>
						</div>
					</div>
				</div>
				<br>
				<br>
			
				<!-- BANNER -- IMAGE UPLOAD -->
				<div id="dropzoneBanner">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="imageUrl">Banner Image</label>	
						<div class="col-sm-9">
							<div class="input-group mb-md"> 
								<input id="bannerImage" name="imageUrl" type="text" class="form-control"/>
								<span class="input-group-btn">
									<a class="iframeBanner btn btn-success" href="<c:url value='/admin/medialibrary/imagesBanners'/>">
										<abbr title="Click the button if want a banner from the media library">Library</abbr>
									</a>
								</span>
							</div>
						</div>
					</div>
					<!-- Upload files for Banner -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Banner </label>
						<div class="col-sm-9">
							<input id="fileuploadBanner" type="file" name="filesBanner[]" data-url="<c:url value="../../file/uploadBanners?"/>">
							Drop file here
							<div id="progress" class="progress">
								<div class="progress-bar progress-bar-primary" role="progressbar"  style="width: 0%;">Complete</div>
							</div>
							<div id="displayBanner"></div>
					</div></div>
					<!-- BANNER -- FILE FILEUPLOAD TABLE-->
					<div class="form-group">
						<label class="col-sm-3 control-label">Banners</label>
						<div class="col-sm-9">				
							<div style="width:200px; padding: 20px">
								<table id="uploaded-files-banner"  class="table">
									<tr class="currB">
										<th>Banner Title</th>
										<th>Banner SubTitle</th>
										<th>Banner Image</th>
										<th>Option Remove</th>
									</tr>
								</table>
				</div></div></div></div>			
				<br>
				<br>	
				<!-- FILE FILE LOAD -->
				<div id="dropzoneFile">
					<div class="control-group">
						<div class="form-group">
							<label class="col-sm-3 control-label">PDF File</label>
							<div class="col-sm-9">
								<div class="input-group mb-md"> 
									<input id="eventFile" type="text" name="eventFile" class="form-control" />
									<span class="input-group-btn">
											<a class="iframeFile btn btn-success" href="<c:url value='/admin/medialibrary/evFiles'/>">
												<abbr title="Click the button if want a PDF from the media library">Library</abbr>
											</a>
										</span>
								</div>
							</div>
						</div>
						<!-- Upload files -->
						<div class="form-group">
							<label class="col-sm-3 control-label">Upload PDF File</label>
							<div class="col-sm-9">
								<input id="fileuploadFile" type="file" name="fileFile[]" data-url="<c:url value="../../file/uploadFiles?"/>">
								Drop file here
								<div id="progressFile" class="progress">
									<div class="progress-bar progress-bar-primaryFile" role="progressbar"  style="width: 0%;">Complete</div>
								</div>
							</div>
						</div>
						<!-- FILES -- FILE FILEUPLOAD -->
						<div class="form-group">
							<label class="col-sm-3 control-label">PDF Files</label>
							<div class="col-sm-9">				
								<div style="width:200px; padding: 20px">
									<table id="uploaded-files-eventFile" class="table">
										<tr class="currF">
											<th>File Name</th>
											<th>File Description</th>
											<th>File Image</th>
											<th>Option Remove</th>
										</tr>
									</table>
				</div></div></div></div></div>
				<br>
				<br>
				<!-- VIDEO FILE LOAD -->
				<div id="dropzonevideo">
					<div class="control-group">
						<div class="form-group">
							<label class="col-sm-3 control-label">Add Video</label>
							<div class="col-sm-9">
								<div class="input-group mb-md"> 
									<input id="eventvideo" type="text" name="eventVideo" class="form-control">
									<span class="input-group-btn">
										<a class="iframeVideo btn btn-success" href="<c:url value='/admin/medialibrary/evVideos'/>">
											<abbr title="Click the button if want a video from the media library">Library</abbr>
										</a>
									</span>
								</div>
							</div>
						</div>
						<!-- Upload files -->
						<div class="form-group">
							<label class="col-sm-3 control-label">Upload Video</label>
							<div class="col-sm-9">
								<input id="fileuploadvideo" type=file name="fileVideo[]" data-url="<c:url value="../../file/uploadVideos?"/>">
								Drop file here
								<div id="progressvideo" class="progress">
									<div class="progress-bar progress-bar-primaryVideo" role="progressbar"  style="width: 0%;">Complete</div>
								</div>
								<div id="displayVideo"></div>
						</div></div>
						<!-- VIDEO -- FILE FILEUPLOAD -->
						<div class="form-group">
							<label class="col-sm-3 control-label">Videos</label>
							<div class="col-sm-9">				
								<div style="width:200px; padding: 20px">
									<table id="uploaded-files-eventvideo"  class="table">
										<tr class="currV">
											<th>Video Name</th>
											<th>Video Description</th>
											<th>Video Image</th>
											<th>Option Remove</th>
										</tr>
									</table>
				</div></div></div></div></div>
				<br>
				<br>
				<!-- CSV UPLOAD -->
				<div id="dropzone">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="fileUrl">CSV File</label>
						<div class="col-sm-9">
							<input id="bulkFile" type="file" name="bulkFile" class="form-control" />
						</div>
					</div>
				</div>
				<br>
				<br>	
				<!-- Button (Double) -->
				<footer class="panel-footer">
					<div class="row">
						<div class="col-sm-9 col-sm-offset-3">
							<a class="btn btn-default" href="<c:url value='/admin/events'/>">Cancel</a>
							<button type="submit" id="Save" name="submit" class="btn btn-success">save</button>
					</div></div>
				</footer>
			</div>		
		</fieldset>
		</form:form>
			<!-- fileupload scripts -->
		<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadIconForEvent.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadImgsForBanners.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadFilesForEvents.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadVideosForEvents.js'/>"></script>
	
		<!-- Specific Page Vendor -->
		<script src="<c:url value='/assets/vendor/jquery-maskedinput/jquery.maskedinput.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js'/>"></script>
	
		<script src="<c:url value='/assets/vendor/codemirror/mode/css/css.js'/>"></script>
		<script src="<c:url value='/assets/vendor/summernote/summernote.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js'/>"></script>
		<script src="<c:url value='/assets/vendor/ios7-switch/ios7-switch.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/assets/vendor/jquery-validation/jquery.validate.js'/>"></script>
		
		<script>
		$ (document).ready(function(){
			// COLORBOX - IFRAME
			$(".iframeIcon").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var bg = $.colorbox.element().data('bg');
		            $('#cboxLoadedContent').css('backgroundColor', bg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myIcon;
					$('#theIcon').val(test);
					$("#displayIcon").empty();
		       	 	$("#displayIcon").append("<img id='theIcon' src='"+test+"'/>");
				}
			});
			
			$(".iframeBanner").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var otherbg = $.colorbox.element().data('otherbg');
		            $('#cboxLoadedContent').css('backgroundColor', otherbg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myBanner;
					var bannId = localStorage.myBannerId;
					$('#bannerImage').val(test);
					$("#displayBanner").empty();
		       	 	$("#displayBanner").append("<img id='bannerImage' src='"+test+"'/>");
		       	 	$("#uploaded-files-banner").append(
	                		$('<tr class="newB"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="subtitleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="bannerUrl[]" value="'+test+'"/>'+
													'<img src="'+test+'" width="30px" height="20px"/>'+
													'<input type="hidden" name="bannerIds[]" value="'+bannId+'"/>'))
	                		.append($('<td/>').html('<input type="button" class="remo btn-xs btn-danger" value="Remove"/>'))
	                		);
				}
			});
			$('table[id="uploaded-files-banner"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newB").remove();
			});
			
			$(".iframeFile").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var videobg = $.colorbox.element().data('videobg');
		            $('#cboxLoadedContent').css('backgroundColor', videobg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var fileId = localStorage.myFileId;
					var fileIconUrl = localStorage.myFileIconUrl;
					var fileUrl = localStorage.myFileUrl;
					
		       	 	$("#displayFile").append("<img id='fileFile' src='"+fileIconUrl+"'/>");
		       	 	$("#uploaded-files-eventFile").append(
	                		$('<tr class="newF"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleFile[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="descriptionFile[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="fileId[]"  value="'+fileId+'"/>'+
	    	                						'<input type="hidden" name="fileUrl[]" value="'+fileUrl+'"/>'+
	    	                						'<input type="hidden" name="fileIconUrl[]" value="'+fileIconUrl+'"/>'+
													'<img src="'+fileIconUrl+'" width="32" height="25"/>'))
	                		.append($('<td/>').html('<input type="button" class="remoFi btn-xs btn-danger" value="Remove"/>'))
	                		);
				}
			});
			$('table[id="uploaded-files-eventFile"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newF").remove();
			});
			
			$(".iframeVideo").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var videobg = $.colorbox.element().data('videobg');
		            $('#cboxLoadedContent').css('backgroundColor', videobg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myVideo;
					$('#fileVideo').val(test);
					var vidId = localStorage.myVideoId;
					var vidUrl = localStorage.myVideoUrl;
					
					$("#displayVideo").empty();
		       	 	$("#displayVideo").append("<img id='fileVideo' src='"+test+"'/>");
		       	 	$("#uploaded-files-eventvideo").append(
	                		$('<tr class="newV"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleVideo[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="descriptionVideo[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="videoId[]"  value="'+vidId+'"/>'+
	    	                						'<input type="hidden" name="videoUrl[]" value="'+vidUrl+'"/>'+
	    	                						'<input type="hidden" name="videoIconUrl[]" value="'+test+'"/>'+
													'<img src="'+test+'" width="32" height="25"/>'))
	                		.append($('<td/>').html('<input type="button" class="remoVi btn-xs btn-danger" value="Remove"/>'))
	                		);
				}
			});
			$('table[id="uploaded-files-eventvideo"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newV").remove();
			});
			
			$("#radioSingle").click(function(){
				$("#hideSingle").show();
				$("#hidePattern").hide();
			});
			$("#radioPattern").click(function(){
				$("#hideSingle").hide();
				$("#hidePattern").show();
			});
			$("#weekly").click(function(){
				$("#hideWeekly").show();
				$("#hideMOnthly").hide();
			});	
			$("#monthly").click(function(){
				$("#hideWeekly").hide();
				$("#hideMonthly").show();
			});	
			var clonedInput = $('#inputx1').clone();
		    $('#btnAdd').click(function() {
		           var num     = $('.clonedInput').length; 
		           var newNum  = new Number(num + 1);      
		           var newElem = clonedInput.clone().attr('id', 'inputx' + newNum);
		           newElem.children(':first').attr('id', 'name' + newNum).attr('name', 'name' + newNum);
		           $('#inputx' + num).after(newElem);
		           $('#btnDele').attr('disabled',false);
		           if (newNum == 10)
		             $('#btnAdd').attr('disabled',false);
		    });
		    $('#btnDele').click(function() {
		           var num = $('.clonedInput').length;
		           $('#inputx' + num).remove();    
		           $('#btnAdd').attr('disabled',false);
		           if (num-1 == 1)
		               $('#btnDele').attr('disabled',true);
		    });
		    $('#btnDele').attr('disabled',true);

		    $('#submit').click(function(){
		        var radioExample=$('#radioExample').val();
		        if ($("#radioExample:checked").length == 0){
		            $('#dis').slideDown().html('<span id="error">Please select a periodicity</span>');
		            return false;
		        }
		    });
		    
		    function addZero(i) { if (i < 10) {i = "0" + i;} return i;} 
		    $('select').change(function(){
		        var time =  $('#sTime').val();
		        var str = time.split(":");
		        var hour = str[0];
		        var minutes = str[1];
		        var duration = 0;
		        
		        $('select.add').each(function(){
		            duration += parseInt($(this).val()) ;
		        });
		        var hoursToAdd = parseInt((duration+ parseInt(minutes)) / 60) ; 
		        var minutesToAdd = (duration+ parseInt(minutes)) % 60;
		        
		        var finalHours = addZero(hoursToAdd + parseInt(hour));
		    	var finalMins = addZero(minutesToAdd);
		        $('#endTime').val(finalHours + ":" + finalMins);
		    });
		});
		</script>
		
		<script>
      	$(function () {
      		var id = 1;
          	$(document).on('click', '#addnewitem', function () {
      		var currentTable = $(this).closest('table').attr('id');
          	id++;
      		$('#'+ currentTable ).append(
      				'<tr><td><div class="col-sm-6 col-lg-12">'+
	                	'<input id="datepicker" class="start_date" name="sDate" value="" placeholder="yyyy/mm/dd">'+
	                	'<input id="days" type="hidden" name="days" value=""></div></td>'+
	                '<td><div class="col-sm-6 col-lg-12">'+
	                    '<input id="stime" type="text" class="start_time" placeholder="Start Time" name="sTime" value=""></div></td>'+
	                '<td><div class="col-sm-6 col-lg-12">'+
	                	'<select name="duration" class="duration_Add">'+
	                    	'<option value="0">Time to add</option>'+
	                    	'<option value="30">30 min</option>'+
	                 		'<option value="60">60 min</option>'+
	                 		'<option value="90">90 min</option>'+
	                 		'<option value="120">120 min</option>'+
	                 		'<option value="180">180 min</option></select></div></td>'+
	                '<td><div class="col-sm-6 col-lg-12">'+
	                	'<input id="etime" type="text" class="end_Time" placeholder="End Time" name="eTime" value=""></div></td>'+
	                '<td>'+
      				'<button type="button" class="removeItem btn btn-danger removeItem" value="-">'+
      				'<span class="glyphicon glyphicon-trash"> Del Date&Time</span></button></td></tr>');
      		$('.start_date').datepicker({dateFormat: "yy/mm/dd"});
      		$('.start_time').timepicker({ showMeridian: false, showSeconds: false });
          	$('.duration_Add').on('change', onChangeCallback);});
              
          	$(document).on('click', '.removeItem', function () {
	            var currentTable = $(this).closest('table').attr('id');
		    	$(this).closest('tr').remove();
	    });
	    function addZero(i) { if (i < 10) {i = "0" + i;} return i;}   
        var onChangeCallback = function () { 
	    	        var currentTable = $(this).closest('table').attr('id');
	    	        var datetoAdd = new Date();
                    var theTime = $(this).parents('tr').find('.start_time').val();
                    var str = theTime.split(":");
				    var hour = str[0];
				    var minute = str[1];
                    var durtoAdd = $(this).parents('tr').find('.duration_Add').val();
                    var hoursAdd = parseInt( (parseInt(durtoAdd) + parseInt(minute)) / 60 );
                    var minsAdd = (parseInt(durtoAdd) + parseInt(minute))%60;
                    var finalHours = addZero(hoursAdd + parseInt(hour));
                    var finalMins = addZero(minsAdd);
                    $(this).parents('tr').find('.end_Time').val(finalHours+":"+ finalMins);
                    $('#eTime').val(finalHours+":"+ finalMins);
	    	 	};
	    $('.duration_Add').on('change', onChangeCallback);
	    
	    });    
	    $('.start_time').timepicker({ showMeridian: false, showSeconds: false });
	    $('.start_date').datepicker({dateFormat: "yy/mm/dd"});
  </script>
  <script>
  Dropzone.options.filedrop = {
		    maxFilesize: 15,
		    init: function () {
		        var totalFiles = 0,
		            completeFiles = 0;
		        this.on("addedfile", function (file) {
		            totalFiles += 1;
		        });
		        this.on("removed file", function (file) {
		            totalFiles -= 1;
		        });
		        this.on("complete", function (file) {
		            completeFiles += 1;
		            if (completeFiles === totalFiles) {
		                doSomething();
		            }
		        });
		    }
		};
</script>
	</section>
</body>
</html>