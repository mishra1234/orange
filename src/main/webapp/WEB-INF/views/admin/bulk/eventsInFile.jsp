<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<body>
	<script>
		(function( $ ) {
			'use strict';
			var datatableInit = function() {
				var $table = $('#eventTable');
				$table.dataTable({
					sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
					oTableTools: {
						sSwfPath: $table.data('swf-path'),
						aButtons: [{sExtends: 'pdf',sButtonText: 'PDF'},
								   {sExtends: 'csv',sButtonText: 'CSV'},
								   {sExtends: 'xls',sButtonText: 'Excel'},
								   {sExtends: 'print',sButtonText: 'Print',sInfo: 'Please press CTR+P to print or ESC to quit'}]
					}, 
					"paging":false,
					"info":false,
					"order":[[0,"asc"]], 
					"columnDefs":[{ "visible": true, "targets": 0 }],
				});
			};
	
			$(function() {
				datatableInit();
			});
		}).apply( this, [ jQuery ]);
	</script>

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> 
				<a href="#" class="fa fa-times"></a>
			</div>
			<h2 class="panel-title">Bulk Activities Read</h2>
		</header>
		<div class="panel-body">
			<table id="eventTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
				<thead>
					<tr>
						<th>ID</th>
						<th>Icon</th>
						<th>Event Title</th>
						<th>Event Content</th>
						<th>Activity Type</th>
						<th>Act Owner</th>
						<th># Banners</th>
						<th># Files</th>
						<th># Layer Connected</th>
						<th>address</th>
						<th>contactEmail</th>
						<th>contactName</th>
						<th>contactPhone</th>
						<th>organisation</th>
						<th>cost</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="ev" items="${activities}" varStatus="row">
					<tr>
						<td>${ev.id}</td>
						<td><img src="${ev.iconUrl}" width="25" height="25"></td>
						<td>${ev.contentTitle}</td>
						<td>${ev.contentBody}</td>
						<td>${ev.activityType}</td>
						<td>${ev.account.name}</td>
						<td>${fn:length(ev.banners)}</td>
						<td>${fn:length(ev.files)}</td>
						<td>${fn:length(ev.layers)}</td>
						<td>${ev.event.address}</td>
						<td>${ev.event.contactEmail}</td>
						<td>${ev.event.contactName}</td>
						<td>${ev.event.contactPhone}</td>
						<td>${ev.event.organisation}</td>
						<td>${ev.event.cost}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</section>


</body>

</html>