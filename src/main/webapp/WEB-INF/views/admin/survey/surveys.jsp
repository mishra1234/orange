<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<body>
	<script>
		(function( $ ) {
			'use strict';
	
			var datatableInit = function() {
				var $table = $('#infoTable');
	
				$table.dataTable({
					sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
					oTableTools: {
						sSwfPath: $table.data('swf-path'),
						aButtons: [
							{
								sExtends: 'pdf',
								sButtonText: 'PDF'
							},
							{
								sExtends: 'csv',
								sButtonText: 'CSV'
							},
							{
								sExtends: 'xls',
								sButtonText: 'Excel'
							},
							{
								sExtends: 'print',
								sButtonText: 'Print',
								sInfo: 'Please press CTR+P to print or ESC to quit'
							}
						]
					}, 
					"paging":false,
					"info":false,
					"order":[[2,"asc"]], 
					"columnDefs":[{ "visible": false, "targets": 0 }],
				});
	
			};
	
			$(function() {
				datatableInit();
			});
		}).apply( this, [ jQuery ]);
	</script>

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Active Surveys</h2>
		</header>
		<div class="panel-body">
			<!--
			<h5>
				<a href="<c:url value='/admin/survey/new/'/>">Add New Survey</a>
			</h5>
			-->
			<table id="infoTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
				<thead>
					<tr>
						<th>ID</th>
						<th>Icon</th>
						<th>Title</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ev" items="${mySurveyList}" varStatus="row">
						<tr>
							<td>${ev.id}</td>
							<td><a href="<c:url value='/admin/survey/${ev.id}'/>"><img src="${ev.iconUrl}" width="25" height="25"
									onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
							<td>${ev.contentTitle}</td>
							
							<td><a href="<c:url value='/admin/survey/delete/${ev.id}'/>"><i class="fa fa-minus-circle" title="Delete"></i></a> 
								<a href="<c:url value='/admin/survey/editSurvey/${ev.id}'/>"><i class="fa fa-edit" title="Edit"></i> </a> 
								<a href="<c:url value='/admin/survey/${ev.id}'/>"> <i class="fa fa-tasks" title="View"></i></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<h5>
				<c:set var="curr" value="${curr}" />
				<c:choose>
					<c:when test="${curr <= 5 }">
						<c:forEach var="num" items="${numbers}" varStatus="row" begin="0"
							end="10">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/infos/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/infos/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:when>
					<c:when test="${curr <= 10 && curr >5}">
						<c:forEach var="num" items="${numbers}" varStatus="row" begin="0"
							end="10">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/infos/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/infos/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<a class="btn" href="<c:url value='/admin/infos/${next}'/>">Next</a>
					</c:when>
					<c:when test="${curr>10 && curr<(hmany-5)}">
						<a class="btn" href="<c:url value='/admin/infos/${prev}'/>">Prev</a>
						<c:forEach var="num" items="${numbers}" varStatus="row"
							begin="${curr-5}" end="${curr+5}">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/infos/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/infos/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<input id="curr" type="hidden" name="curr" value="${next}" />
						<a class="btn" href="<c:url value='/admin/infos/${next}'/>">Next</a>
					</c:when>
					<c:otherwise>
						<a class="btn" href="<c:url value='/admin/infos/${prev}'/>">Prev</a>
						<c:forEach var="num" items="${numbers}" varStatus="row"
							begin="${curr-5}" end="${hmany+1}">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/infos/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/surveys/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</h5>
		</div>
	</section>


</body>

</html>