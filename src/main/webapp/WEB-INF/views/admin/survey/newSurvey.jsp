<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/pnotify/pnotify.custom.css" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />
		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">
		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>
</head>

<body>
	<link rel="stylesheet" href="<c:url value="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/theme.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/select2/select2.css"/>">

	<c:url var="var" value='/admin/survey/save' />
	<form:form modelAttribute="newActSurvey" id="form" class="form-horizontal" method="POST" action="${var}">
	
		<fieldset>
			<section class="panel form-wizard">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a> <a href="#" class="fa fa-times"></a>
					</div>
					<h2 class="panel-title">New Survey</h2>
					<p class="panel-subtitle">Create new survey</p>
				</header>

				<!-- Hidden IDs -->
				<form:input type="hidden" name="ownerCommunity" 	path="ownerCommunity" value="" />
				<form:input type="hidden" name="id" 			  	path="id" 				value="" />
				<form:input type="hidden" name="activityType" 	  	path="activityType" 	value="survey" />
				<form:input type="hidden" name="deleted" 		  	path="deleted" 			value="false" />
				<form:input type="hidden" name="actionTitle" 	  	path="actionTitle" 		value="NotUsedAnymore" />
				<form:input type="hidden" name="createdDatetime"  	path="createdDatetime" 	value="${currentTS}" />
				<form:input type="hidden" name="createdAccountId" 	path="createdAccountId"	value="${currentUser}" />
				<form:input type="hidden" name="recommended" 	  	path="recommended" 		value="" />

				<div class="panel-body">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="iconUrl">Title<span
							class="required">*</span></label>
						<div class="col-sm-9">
							<form:input id="ContentTitle" name="contentTitle" type="pattern" placeholder="ContentTitle" 
									    class="form-control" path="contentTitle" required="required" />
						</div>
					</div>
							
					<div class="form-group">
						<div class="col-md-12">
							<label class="col-sm-3 control-label" for="commodities">Commodities
								<abbr title="Select commodity groups"><i
									class="fa fa-question-circle"></i></abbr>
							</label>
							<div class="col-sm-9">
								<div class="input-group mb-md">
									<select name="commodities" multiple
										data-plugin-selectTwo class="form-control populate"
										style="padding: 0px 0px; width:100%">
									<!--  <optgroup label="SA Regional Councils">-->
										<c:forEach var="listValue" items="${managedLayers}">
											<c:forEach var="comm" items="${newActSurvey.layers}">
												<c:choose>
													<c:when test="${listValue.id == comm.id}">
														<option value="${listValue.id}" selected="selected">${listValue.name}</option>
													</c:when>
												</c:choose>
											</c:forEach>
											<option value="${listValue.id}">${listValue.name}</option>
										</c:forEach>
										<!--</optgroup>-->	
									</select>
								</div>
							</div>
						
							<!-- Survey Link -->
							<div class="form-group">
								<label class="col-sm-3 control-label" for="iconUrl">Survey Link<span
									class="required">*</span></label>
								<div class="col-sm-9">
									<form:input id="actionUrl" name="actionUrl" type="pattern" placeholder="http://..." 
											    class="form-control" path="actionUrl" required="required" />
								</div>
							</div>
							
							<!-- ICON input-->
							<div id="dropzone">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="iconUrl">Icon</label>
									<div class="col-sm-9">
										<form:input id="theIcon" name="iconUrl" type="urlIcon" class="form-control" value="" path="iconUrl"/>
									</div>
								</div>
								<!-- Upload files -->
								<div class="form-group">
									<label class="col-sm-3 control-label">Upload Icon</label>
									<div class="col-sm-9">
										<input id="fileuploadInfo" type="file" data-url="../../../file/upload?"> Drop files here
										<div id="progressInfoIcon" class="progress">
											<div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">Complete</div>
										</div>
										<div id="displayIcon"></div>
									</div>
								</div>
							</div>
	
							<div class="form-group">
								<label class="col-sm-3 control-label" for="iconUrl">Description</label>
								<div class="col-sm-9">
									<form:textarea id="ContentBody" name="contentBody" type="pattern" placeholder="All details about the event"
										class="form-control" path="contentBody" rows="10" cols="50" />
								</div>
							</div>
	
							<div class="form-group">
								<label class="col-sm-3 control-label" for="account.id">Survey Owner<span class="required">*</span>
								</label>
								<div class="col-sm-9">
									<form:select id="account.id" name="account.id" class="form-control" path="account.id">
										<form:options items="${listOfOwners}" itemValue="id" itemLabel="name"></form:options>
									</form:select>
								</div>
							</div>
						</div>
						<br>

						<!-- BANNER -- IMAGE UPLOAD -->
						<div id="dropzoneBanner" class="panel-body">
							<div class="control-group">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="imageUrl">Banner Image</label>
									<div class="col-sm-9">
										<input id="bannerImage" name="imageUrl" type="urlBanner" class="form-control" />
									</div>
								</div>
								<!-- Upload files for Banner -->
								<div class="form-group">
									<label class="col-sm-3 control-label">Upload Banner </label>
									<div class="col-sm-9">
										<input id="fileuploadBanner" type="file" name="filesBanner[]" data-url="../../../file/uploadBanners?"> Drop file here
										<div id="progress" class="progress">
											<div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">Complete</div>
										</div>
										<div id="displayBanner"></div>
									</div>
								</div>
								<!-- BANNER -- FILE FILEUPLOAD TABLE-->
								<div class="form-group">
									<label class="col-sm-3 control-label">Banners</label>
									<div class="col-sm-9">
										<div style="width: 200px; padding: 20px">
											<table id="uploaded-files-banner" class="table">
												<tr class="currB">
													<th>Banner Title</th>
													<th>Banner SubTitle</th>
													<th>Banner Image</th>
													<th>Option Remove</th>
												</tr>
												<c:forEach var="banner" items="${bannerList}" varStatus="row">
													<tr class="currB">
														<td><input value="${banner.title}" name="titleBanner[]" /></td>
														<td><input value="${banner.subtitle}" name="subtitleBanner[]" /></td>
														<td><input type="hidden" value="${banner.id}" name="bannerId[]" /> 
														 	<input type="hidden" value="${banner.imageUrl}" name="bannerUrl[]" /> 
															<input type="hidden" value="0" name="bannerRemove[]" /> 
															<img src="<c:url value="${banner.imageUrl}"/>" alt="TestDisplay" width="30" height="20" name="urlBanner" />
														</td>
													</tr>
												</c:forEach>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Button (Double) -->
				<footer class="panel-footer">
					<div class="row">
						<div class="col-sm-9 col-sm-offset-3">
							<a class="btn btn-default" href="<c:url value='/admin/surveys'/>">Cancel</a>
							<button type="submit" id="Save" name="submit" class="btn btn-success">save</button>
					</div></div>
				</footer>
			</section>
		</fieldset>
	</form:form>

	<!-- Vendor -->
	<script src="../../../assets/vendor/jquery/jquery.js"></script>
	<script src="../../../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
	<script src="../../../assets/vendor/bootstrap/js/bootstrap.js"></script>
	<script src="../../../assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="../../../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/vendor/magnific-popup/magnific-popup.js"></script>
	<script src="../../../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
	
	<!-- Specific Page Vendor -->
	<script src="../../../assets/vendor/jquery-validation/jquery.validate.js"></script>
	<script src="../../../assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
	<script src="../../../assets/vendor/pnotify/pnotify.custom.js"></script>
	
	<!-- Theme Base, Components and Settings -->
	<script src="../../../assets/javascripts/theme.js"></script>
	
	<!-- Theme Custom -->
	<script src="../../../assets/javascripts/theme.custom.js"></script>
	
	<!-- Theme Initialization Files -->
	<script src="../../../assets/javascripts/theme.init.js"></script>
	<script src="../../../assets/javascripts/forms/examples.wizard.js"></script>
	
	<script>(function() {		
						'use strict';
						jQuery.validator.addMethod("pattern", function(value, element) {
						    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
						}, "Your data contain invalid characters");
						   
// 						jQuery.validator.addMethod("urlIcon", function(value, element) {
// 							    	return isValidIconSize();
// 								}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only.");
						
					 
		jQuery.validator.addMethod("pattern", function(value, element) {
		    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
		}, "Your data contain invalid characters");
		   
// 		jQuery.validator.addMethod("urlIcon", function(value, element) {
// 			    	return isValidIconSize();
// 				}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only.");
		
// 		 function isValidIconSize(){
// 			 var imgWidth = $("#theInfoIcon").width();
// 			 var imgHeight = $("#theInfoIcon").height();
// 			 var extension = $('#theInfoIcon').attr("src").split('.').pop().toLowerCase();;
// 			 console.log("URL: " + extension);
// 			 console.log("width:" + imgWidth);
// 			 console.log("height:" + imgHeight);
// 			 if (imgHeight == 200 && imgWidth == 200 && (extension == "jpg" || extension == "png"))
// 				return true;
// 				 };   
			

			  
// 				 jQuery.validator.addMethod("urlBanner", function(value, element) {
// 				    	return isValidBannerSize();
// 					}, "Invalid Banner, use 640px X 320px size and use .jpg or .png images only.");
			
// 			 function isValidBannerSize(){

// 				 var imgWidth = $("#theInfoBanner").width();
// 				 var imgHeight = $("#theInfoBanner").height();
// 				 var extension = $('#theInfoBanner').attr("src").split('.').pop().toLowerCase();;
// 				 console.log("Banner URL: " + extension);
// 				 console.log("Banner width:" + imgWidth);
// 				 console.log("Banner height:" + imgHeight);
// 				 if (imgHeight == 320 && imgWidth == 640 && (extension == "jpg" || extension == "png"))
// 					return true;
// 					 };   
// 						//Wizard validation
						var $w3finish = $('#form').find('ul.pager li.finish'),
						$w3validator = $("#form").validate({
						highlight: function(element) {
							
							$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
						},
						success: function(element) {
							$(element).closest('.form-group').removeClass('has-error');
							$(element).remove();
						},
						errorPlacement: function( error, element ) {
							element.parent().append( error );
						}
					});
					
					// Submit the from when click finish
					$w3finish.on('click', function( ev ) {
						//ev.preventDefault();
						var validated = $('#form').valid();
						if ( validated ) {
							$("#Save").click(function() {
								  $("#form").submit();
								});
						}
					});

					$('#form').bootstrapWizard({
						tabClass: 'wizard-steps',
						nextSelector: 'ul.pager li.next',
						previousSelector: 'ul.pager li.previous',
						firstSelector: null,
						lastSelector: null,
						onNext: function( tab, navigation, index, newindex ) {
							console.log("Wizard validation started!!");
							var validated = $('#form').valid();
							if( !validated ) {
								$w3validator.focusInvalid();
								return false;
							}
						},
						onTabClick: function( tab, navigation, index, newindex ) {
							if ( newindex == index + 1 ) {
								return this.onNext( tab, navigation, index, newindex);
							} else if ( newindex > index + 1 ) {
								return false;
							} else {
								return true;
							}
						},
						onTabChange: function( tab, navigation, index, newindex ) {
							var $total = navigation.find('li').size() - 1;
							$w3finish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
							$('#form').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
						},
						onTabShow: function( tab, navigation, index ) {
							var $total = navigation.find('li').length - 1;
							var $current = index;
							var $percent = Math.floor(( $current / $total ) * 100);
							$('#form').find('.progress-indicator').css({ 'width': $percent + '%' });
							tab.prevAll().addClass('completed');
							tab.nextAll().removeClass('completed');
						}
					});
						// basic
						$("#form").validate({
							highlight: function( label ) {
								console.log("validation starts");
								$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
							},
							success: function( label ) {
								$(label).closest('.form-group').removeClass('has-error');
								label.remove();
							},
							errorPlacement: function( error, element ) {
								var placement = element.closest('.input-group');
								if (!placement.get(0)) {
									placement = element;
								}
								if (error.text() !== '') {
									placement.after(error);
								}
							}
						});

					}).apply( this, [ jQuery ]);	
	</script>
	
	<!-- fileupload scripts -->
	<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
	<script src="<c:url value='/scripts/myUploadIconForInfos.js'/>"></script>
	<script src="<c:url value='/scripts/myUploadImgsForBanners.js'/>"></script>
	<script src="<c:url value='/scripts/myUploadFilesForInfos.js'/>"></script>
	<script src="<c:url value='/scripts/myUploadVideosForEvents.js'/>"></script>
	<!-- Specific Page Vendor -->
	<script src="<c:url value='/assets/vendor/jquery-maskedinput/jquery.maskedinput.js'/>"></script>
	<script src="<c:url value='/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js'/>"></script>
	<script src="<c:url value='/assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js'/>"></script>
	<script src="<c:url value='/assets/vendor/codemirror/mode/css/css.js'/>"></script>
	<script src="<c:url value='/assets/vendor/summernote/summernote.js'/>"></script>
	<script src="<c:url value='/assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js'/>"></script>
	<script src="<c:url value='/assets/vendor/ios7-switch/ios7-switch.js'/>"></script>

</body>
</html>