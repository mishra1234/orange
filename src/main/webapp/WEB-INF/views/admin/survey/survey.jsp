<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html>
<body>
<fieldset>

<br>
<h4><strong>Survey Title:</strong>  ${inf.contentTitle}</h4>
<br>
<!-- <div class="slider"> -->
<!-- 	<ul class="bxslider"> -->
<%-- 		<c:forEach var="bann" items="${listOfBanners}" varStatus="row"> --%>
<!-- 	  			<li> -->
<%-- 	  				<img src="${bann.imageUrl}" title="${bann.subtitle}"> --%>
<!-- 	  				<div class="text-section"> -->
<%--                     	<p class="subtitle"><h6>${bann.title}</h6></p> --%>
<!--                     	<br><br> -->
<!--                 </div> -->
<!-- 	  			</li> -->
<%-- 	  	</c:forEach> --%>
<!-- 	</ul> -->
<!-- </div> -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.bxslider').bxSlider({
		  mode: 'fade',
		  captions: true,
		  auto: true,
		  slideWidth: 700
		});
		(function($){
			'use strict';
			var initBasicWithMarkers = function() {
				var map = new GMaps({
					div: '#gmap-basic-marker',
					lat: ${avLat},
					lng: ${avLng},
					markers: [{
						lat: ${avLat},
						lng: ${avLng},
						infoWindow: {
							content: '<p>Basic</p>'
						}
					}]
				});
				map.addMarker({
					lat: ${avLat},
					lng: ${avLng},
					infoWindow: {
						content: '<p>${inf.contentTitle}</p>'
					}
				});
			};
			// auto initialize
			$(function() {
				initBasicWithMarkers();
			});
		}).apply(this, [ jQuery ]);
	});
</script>

<link rel="stylesheet" href="http://bxslider.com/lib/jquery.bxslider.css" type="text/css" />
<link rel="stylesheet" href="http://bxslider.com/css/github.css" type="text/css" />
<script src="http://bxslider.com/js/jquery.min.js"></script>
<script src="http://bxslider.com/lib/jquery.bxslider.js"></script>
<script src="http://bxslider.com/js/rainbow.min.js"></script>
<script src="http://bxslider.com/js/scripts.js"></script>

<iframe style="width:100%; height:500px;" src="<c:url value='${inf.actionUrl}'/>">
  <p>Your browser does not support iframes.</p>
</iframe>
	
<h5>  
  <button type="submit" class="btn btn-success"><a class="btn" style="color:white;" href="<c:url value='/admin/survey/editSurvey/${inf.id}'/>">Edit Survey</a> </button>
	  <button type="submit" class="btn btn-success"><a class="btn" style="color:white;" href="<c:url value='/admin/event/promote/${inf.id}'/>">Promote Survey</a></button>
</h5>

</fieldset>
</body>

</html>