<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
	<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Admin</title>
		<link rel="stylesheet" href="<c:url value='/styles/default.css'/>">
		
		
		
	</head>
	<body>
	
	<script type="text/javascript">
	$(document).ready(function() {
			$("#userTable").dataTable( {
		        "paging":   false,
		        "ordering": false,
		        "info":     false
		    } );
			console.log("enter");
		});
		
		</script>
		<h1>Users</h1>

		 <div class="content">
		<!-- TABLE -->
		<table id='userTable' class="display" cellspacing="0">
				<thead><tr><th></th><th>Name</th><th>Surname</th><th>Contact</th></tr></thead>
				<tbody>
					<c:forEach var="userEntry" items="${userGrid.userMap}" varStatus="row">
						<tr>
							<td>
								${userEntry.value.id}
							</td>
							<td>
								${userEntry.value.firstname}
							</td>
							<td>
								${userEntry.value.surname}
							</td>
							<td>
								${userEntry.value.phone}
							</td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
	</body>
</html>
