<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
	<!-- start: page -->
					<div class="row">
						<div class="col-md-6 col-lg-12 col-xl-6">
							<section class="panel">
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12">
											<div class="chart-data-selector" id="salesSelectorWrapper">
												<h2>
													New Users
													<strong>
														<select class="form-control" id="salesSelector">
														<c:forEach var="listValue" items="${listOfManagedComm}" >
															<option  value="${listValue.id}">${listValue.name}</option>
														</c:forEach>
															<option value="All"   selected>All</option>
														</select>
														
													</strong>
												</h2>
												<h4 class="title"><a href="<c:url value='/admin/community/new'/>">Catalogue</a></h4>
													
											<!-- iOS -->
											
												<div  id="salesSelectorItems" class="chart-data-selector-items mt-sm">
													<!-- Flot: Sales Porto Admin -->
													<div class="col-lg-12">
										
													<div id="curve_chart" data-sales-rel="Porto Drupal"></div></div>

													<!-- Flot: Sales Porto Wordpress -->
<!-- 													<div class="chart chart-sm" data-sales-rel="All" id="flotDashSales3" class="chart-hidden"></div> -->
												 <script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>
     
		
													<script>
													window.onload = function all(){
													        	var getID = JSON.stringify(${newUserList});
													       
													        //var checkOutDate= $("#checkOutDate").val();
															console.log("Id is: " + getID);
															
														  window.onload =   google.setOnLoadCallback(drawChart(getID));
											        
        function drawChart(jsonData1) {
            console.log("Here");
           
            console.log("JSON data: " +jsonData1);
            var data = google.visualization.arrayToDataTable(JSON.parse(jsonData1));
           var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
           var options = {
                   legend: 'none',
                   colors: ['#f78d3e'],
                   pointSize: 7
                 };
               chart.draw(data, options);
              
             }
													}
													    	
       // window.onload =  google.setOnLoadCallback(drawChart(${newUserList}));
        </script>
        
												</div>

											</div>
										</div>
<!-- 										<div class="col-lg-4 text-center"> -->
<!-- 											<h2 class="panel-title mt-md">New Users Goal</h2> -->
<!-- 											<div class="liquid-meter-wrapper liquid-meter-sm mt-lg"> -->
<!-- 												<div class="liquid-meter"> -->
<!-- 													<meter min="0" max="1000" value="25" id="meterSales"></meter> -->
<!-- 												</div> -->
<!-- 												<div class="liquid-meter-selector" id="meterSalesSel"> -->
<!-- 													<a href="#" data-val="35" class="active">Monthly Goal</a> -->
<!-- 													<a href="#" data-val="3">Annual Goal</a> -->
<!-- 												</div> -->
<!-- 											</div> -->
<!-- 										</div> -->
									</div>
								</div>
							</section>
						</div>
						<div class="col-md-6 col-lg-12 col-xl-6">
							<div class="row">
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary">
														<i class="fa fa-life-ring"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
													
														<h4 class="title">Connections</h4>
														<div class="info">
																<strong class="amount">${totalConnections}</strong>
														
<!-- 															<span class="text-primary">(12% increment)</span> -->
														</div>
														
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase" href="<c:url value='/admin/accounts'/>">(view all)</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div> 
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-secondary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-secondary">
														<i class="fa fa-usd"></i>
													</div>
												</div>
												 <div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">This Month Invoice</h4>
														<div class="info">
															<strong class="amount">$${Amount}</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase" href="<c:url value='/admin/dashoboardInvoicebalance'/>">(Invoice Balance)</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-tertiary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-tertiary">
														<i class="fa fa-calendar"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">All Future Activities</h4>
														<div class="info">
															<strong class="amount">${futureActivities}</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase" href="<c:url value='/admin/events'/>">(view all)</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<!-- Managers -->
								
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-quartenary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-quartenary">
														<i class="fa fa-user"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Total Communities</h4>
												
														<div class="info">
															<strong class="amount">${CommSize}</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase" href="<c:url value='/admin/communities'/>">(view all	)</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<!-- Managers -->
<!-- 									<div class="col-md-12 col-lg-6 col-xl-6"> -->
<!-- 									<section class="panel panel-featured-left panel-featured-tertiary"> -->
<!-- 										<div class="panel-body"> -->
<!-- 											<div class="widget-summary"> -->
<!-- 												<div class="widget-summary-col widget-summary-col-icon"> -->
<!-- 													<div class="summary-icon bg-primary"> -->
<!-- 													<i class="fa fa-users"></i> -->
<!-- 													</div> -->
<!-- 												</div> -->
<!-- 												<div class="widget-summary-col"> -->
<!-- 													<div class="summary"> -->
<!-- 														<h4 class="title">Managers</h4> -->
<!-- 														<div class="info"> -->
<!-- 															<strong class="amount"></strong> -->
<!-- 														</div> -->
<!-- 													</div> -->
<!-- 													<div class="summary-footer"> -->
<!-- 														<a class="text-muted text-uppercase">(view all)</a> -->
<!-- 													</div> -->
<!-- 												</div> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</section> -->
<!-- 								</div> -->
								<!-- Categories -->
									<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-tertiary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-secondary">
														<i class="fa fa-th"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Total Categories</h4>
														<div class="info">
															<strong class="amount">${noOfCat}</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase" href="<c:url value='/admin/categories'/>">(view all)</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<!-- Info pages -->
									<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-tertiary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-tertiary">
														<i class="fa fa-info"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Total Info Pages</h4>
														<div class="info">
															<strong class="amount">${noOfInfo}</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase">(view all)</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<!-- Surveys -->
									<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-tertiary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-quartenary">
														<i class="fa fa-list"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Total Surveys</h4>
														<div class="info">
															<strong class="amount">${noOfSurveys}</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase">(view all)</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<!-- New Community -->
									<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-tertiary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary">
														<i class="fa fa-plus"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
													<h4 class="title"><a href="<c:url value='/admin/community/new'/>">Add New Community</a></h4>
													
															<div class="info">
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase"></a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>
					
					<script type= "text/javascript">
					

    $(document).ready(function () {

    	console.log("Id is:");
    	 $("#salesSelector").on('change',function (ev2) {
    	
        var getID= $("select#salesSelector option:selected").val();
       
        //var checkOutDate= $("#checkOutDate").val();
		console.log("Id is: " + getID);
		if(getID=='All'){
			return all();
		}
		var actionUrl = "<c:url value='/getStatsCom/'/>"
      $.ajax({
            type: "GET",
            url: actionUrl+getID,
            success: function(json) {
               // if(response != null && response !="" && response !="null"){
                    //do you stuff here
                    console.log("Json returned: " + json);
                    var jsonData1 =   json + "]";
                    
                    if(jsonData1.indexOf("Jan") == -1){
                    	jsonData1 =JSON.stringify ([["year",""],["Jan",0],["Feb",0],["Mar",0],["Apr",0],["May",0],["Jun",0],["Jul",0],["Aug",0]]);
                    }
                    google.setOnLoadCallback(drawChart(jsonData1));
                    
                    function drawChart(jsonData1) {
                        console.log("Here");
                       
                        console.log("JSON data: " +jsonData1);
                      
                        var data = google.visualization.arrayToDataTable(JSON.parse(jsonData1));
                       var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                       var options = {
                               legend: 'none',
                               colors: ['#f78d3e'],
                               pointSize: 7
                             };
                           chart.draw(data, options);
                          
                         }
            },
            error: function(e) {
                alert('Error: ' + e.message);
            },
            
        });
		 
    	 });
    	
    	 });
    
</script>
	<script>
												 	function all(){
													        	var getID = JSON.stringify(${newUserList});
													       
													        //var checkOutDate= $("#checkOutDate").val();
															console.log("Hi in all");
															
														  google.setOnLoadCallback(drawChart(getID));
											        
        function drawChart(jsonData1) {
            console.log("Here");
           
            console.log("JSON data: " +jsonData1);
            var data = google.visualization.arrayToDataTable(JSON.parse(jsonData1));
           var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
           var options = {
                   legend: 'none',
                   colors: ['#f78d3e'],
                   pointSize: 7
                 };
               chart.draw(data, options);
              
             }
													}
													    	
       // window.onload =  google.setOnLoadCallback(drawChart(${newUserList}));
        </script>
      

<!-- 					<div class="row"> -->
<!-- 						<div class="col-xl-9"> -->

<!-- 						<section class="panel"> -->
<!-- 														<header class="panel-heading panel-heading-transparent"> -->
<!-- 															<div class="panel-actions"> -->
<!-- 																<a href="#" class="fa fa-caret-down"></a> -->
<!-- 																<a href="#" class="fa fa-times"></a> -->
<!-- 															</div> -->

<!-- 															<h2 class="panel-title">My Activities</h2> -->
<!-- 														</header> -->
<!-- 														<div class="panel-body"> -->
<!-- 															<div class="timeline timeline-simple mt-xlg mb-md"> -->
<!-- 																<div class="tm-body"> -->
<!-- 																	<div class="tm-title"> -->
<!-- 																		<h3 class="h5 text-uppercase">January 2015</h3> -->
<!-- 																	</div> -->
<!-- 																	<ol class="tm-items"> -->
<!-- 																		<li> -->
<!-- 																			<div class="tm-box"> -->
<!-- 																				<p class="text-muted mb-none">7 months ago.</p> -->
<!-- 																				<p> -->
<!-- 																					It's awesome when we find a good solution for our projects, Porto Admin is <span class="text-primary">#awesome</span> -->
<!-- 																				</p> -->
<!-- 																			</div> -->
<!-- 																		</li> -->
<!-- 																		<li> -->
<!-- 																			<div class="tm-box"> -->
<!-- 																				<p class="text-muted mb-none">7 months ago.</p> -->
<!-- 																				<p> -->
<!-- 																					Checkout! How cool is that! -->
<!-- 																				</p> -->
<!-- 																				<div class="thumbnail-gallery"> -->
<!-- 																					<a class="img-thumbnail lightbox" href="assets/images/projects/project-4.jpg" data-plugin-options='{ "type":"image" }'> -->
<!-- 																						<img class="img-responsive" width="215" src="assets/images/projects/project-4.jpg"> -->
<!-- 																						<span class="zoom"> -->
<!-- 																							<i class="fa fa-search"></i> -->
<!-- 																						</span> -->
<!-- 																					</a> -->
<!-- 																				</div> -->
<!-- 																			</div> -->
<!-- 																		</li> -->
<!-- 																		<li> -->
<!-- 																			<div class="tm-box"> -->
<!-- 																				<p class="text-muted mb-none"></p> -->
<!-- 																				<p> -->
<!-- 																					<span class="text-primary">Display more</span> -->
<!-- 																				</p> -->
<!-- 																			</div> -->
<!-- 																		</li> -->
<!-- 																	</ol> -->
<!-- 																</div> -->
<!-- 															</div> -->
<!-- 														</div> -->
<!-- 							</section> -->


<!-- 						</div> -->


<!-- 						<div class="col-xl-3"> -->

<!-- 							<section class="panel panel-transparent"> -->
<!-- 														<header class="panel-heading"> -->
<!-- 															<div class="panel-actions"> -->
<!-- 																<a href="#" class="fa fa-caret-down"></a> -->
<!-- 																<a href="#" class="fa fa-times"></a> -->
<!-- 															</div> -->

<!-- 															<h2 class="panel-title">My Stats</h2> -->
<!-- 														</header> -->
<!-- 														<div class="panel-body"> -->
<!-- 															<section class="panel"> -->
<!-- 																<div class="panel-body"> -->
<!-- 																	<div class="small-chart pull-right" id="sparklineBarDash"></div> -->
<!-- 																	<script type="text/javascript"> -->
<!--  																		var sparklineBarDashData = [5, 6, 7, 2, 0, 4 , 2, 4, 2, 0, 4 , 2, 4, 2, 0, 4]; -->
<!-- 																	</script> -->
<!-- 																	<div class="h4 text-bold mb-none">488</div> -->
<!-- 																	<p class="text-xs text-muted mb-none">My Communities</p> -->
<!-- 																</div> -->
<!-- 															</section> -->
<!-- 															<section class="panel"> -->
<!-- 																<div class="panel-body"> -->
<!-- 																	<div class="circular-bar circular-bar-xs m-none mt-xs mr-md pull-right"> -->
<!-- 																		<div class="circular-bar-chart" data-percent="45" data-plugin-options='{ "barColor": "#2baab1", "delay": 300, "size": 50, "lineWidth": 4 }'> -->
<!-- 																			<strong>Average</strong> -->
<!-- 																			<label><span class="percent">45</span>%</label> -->
<!-- 																		</div> -->
<!-- 																	</div> -->
<!-- 																	<div class="h4 text-bold mb-none">125,852</div> -->
<!-- 																	<p class="text-xs text-muted mb-none">Active Sessions</p> -->
<!-- 																</div> -->
<!-- 															</section> -->
<!-- 															<section class="panel"> -->
<!-- 																<div class="panel-body"> -->
<!-- 																	<div class="small-chart pull-right" id="sparklineLineDash"></div> -->
<!-- 																	<script type="text/javascript"> -->
<!--  																		var sparklineLineDashData = [15, 16, 17, 19, 10, 15, 13, 12, 12, 14, 16, 17]; -->
<!-- 																	</script> -->
<!-- 																	<div class="h4 text-bold mb-none">89</div> -->
<!-- 																	<p class="text-xs text-muted mb-none">My Upcoming Activities</p> -->
<!-- 																</div> -->
<!-- 															</section> -->
<!-- 														</div> -->
<!-- 													</section> -->
													<!--  
													<section class="panel">
														<header class="panel-heading">
															<div class="panel-actions">
																<a href="#" class="fa fa-caret-down"></a>
																<a href="#" class="fa fa-times"></a>
															</div>

															<h2 class="panel-title">
																<span class="label label-primary label-sm text-normal va-middle mr-sm">198</span>
																<span class="va-middle">Connected Users</span>
															</h2>
														</header>
														<div class="panel-body">
															<div class="content">
																<ul class="simple-user-list">
																	<li>
																		<figure class="image rounded">
																			<img src="assets/images/!sample-user.jpg" alt="Joseph Doe Junior" class="img-circle">
																		</figure>
																		<span class="title">Joseph Doe Junior</span>
																		<span class="message truncate">Lorem ipsum dolor sit.</span>
																	</li>
																	<li>
																		<figure class="image rounded">
																			<img src="assets/images/!sample-user.jpg" alt="Joseph Junior" class="img-circle">
																		</figure>
																		<span class="title">Joseph Junior</span>
																		<span class="message truncate">Lorem ipsum dolor sit.</span>
																	</li>
																	<li>
																		<figure class="image rounded">
																			<img src="assets/images/!sample-user.jpg" alt="Joe Junior" class="img-circle">
																		</figure>
																		<span class="title">Joe Junior</span>
																		<span class="message truncate">Lorem ipsum dolor sit.</span>
																	</li>
																</ul>
																<hr class="dotted short">
																<div class="text-right">
																	<a class="text-uppercase text-muted" href="#">(View All)</a>
																</div>
															</div>
														</div>
														<div class="panel-footer">
															<div class="input-group input-search">
																<input type="text" class="form-control" name="q" id="q" placeholder="Search...">
																<span class="input-group-btn">
																	<button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
																	</button>
																</span>
															</div>
														</div>
													</section>-->

<!-- 						</div> -->
<!-- 					</div> -->