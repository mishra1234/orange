<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html class="fixed">
<head>
<style type="text/css"> </style>

</head>
<body>
	<link href="<c:url value='/styles/dropzone.css'/>" type="text/css" rel="stylesheet" />
	<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
	<script src="<c:url value='/scripts/myuploadfunction.js'/>"></script>

	<c:url var="var" value='/admin/category/save' />
	<form:form modelAttribute="category" id="form" class="form-horizontal" method="POST" action="${var}">

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">New Category</h2>
			<p class="panel-subtitle">Create a new Category</p>
		</header>
		<div id="dropzone" class="panel-body">

			
				<fieldset>
					<!-- Text input-->
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-3 control-label">Name<span
								class="required">*</span></label>
							<div class="col-sm-9">
								<form:input type="pattern" id="Name" name="name" class="form-control" placeholder="Category Name" path="name" required="required"/>
							</div>
						</div>
						
						<div id="dropzone">
							<div class="form-group">
								<label class="col-sm-3 control-label">Icon<span
									class="required">*</span></label>
								<div class="col-sm-9">
									<form:input id="Icon" name="iconUrl" type="urlIcon" class="form-control" placeholder="http://" required="required" autofocus="autofocus" path="iconUrl"/>
								</div>
							</div>
							<!-- Upload Icon -->
							<div class="form-group">
								<label class="col-sm-3 control-label">Upload Icon</label>
								<div class="col-sm-9">
									<input id="fileupload" type="file" name="files[]" class="dropzone dz-square" data-url="../../file/upload?${_csrf.parameterName}=${_csrf.token}">
									<div class="panel-body">Drop files here</div>
									<div id="progress" class="progress"> 
										<div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">Complete</div>
									</div>
									<div id="displayIcon"></div>
								</div>
							</div>
						</div>
						
						<!-- 	
						<div class="form-group">
							<label class="col-md-3 control-label" for="parentIds">Parent Categories<span class="required">*</span></label>
							<div class="col-md-6">
								<select id="parentIds" type="text" name="parentIds" class="form-control" data-plugin-multiselect multiple="multiple" required>
									<c:forEach var="category" items="${listOfCategories}" varStatus="row">
										<option value="${category.id}">${category.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						 -->
						 
						<div class="form-group">
							<label class="col-md-3 control-label" for="parentIds">Parent Category<span class="required">*</span></label>
							
							<div class="col-sm-9"> 
								<div class="input-group mb-md">
									<select name="parentIds" multiple data-plugin-selectTwo class="form-control populate" style="padding:0px 0px; width:100%">
										<c:forEach var="listValue" items="${listOfCategories}">
											<c:forEach var="comm" items="${parentCats}">
												<c:choose>
													<c:when test="${listValue.id == comm.id}">
														<option value="${listValue.id}" selected="selected">${listValue.name}</option>
													</c:when>
												</c:choose>
											</c:forEach>
											<option value="${listValue.id}">${listValue.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
					</div>					
					
					<!-- Button (Double) -->
						<footer class="panel-footer">
							<div class="row">
								<div class="col-sm-9 col-sm-offset-3">
									<a class="btn btn-default"
										href="<c:url value='/admin/categories'/>">Cancel</a>
									<button type="submit" id="Save" class="btn btn-primary">save</button>
								</div>
							</div>
						</footer>
						<div style="width: 500px; padding: 20px"></div>
				</fieldset>
			</form:form>
		
			<!-- HTML FILEUPLOAD -->
			
		</div>
	</section>
	
	<script>
	
	
	(function() {
		

		'use strict';
		jQuery.validator.addMethod("pattern", function(value, element) {
		    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
		}, "Your data contain invalid characters");
		 
		    jQuery.validator.addMethod("urlIcon", function(value, element) {
			    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
			}, "Invalid format use PNG or JPG file format");
		    
			     
				  
				jQuery.validator.addMethod("urlIcon", function(value, element) {
					    	return isValidIconSize();
						}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only.");
				
				 function isValidIconSize(){

					 var imgWidth = $("#theImg").width();
					 var imgHeight = $("#theImg").height();
					 var extension = $('#theImg').attr("src").split('.').pop().toLowerCase();;
					 console.log("URL: " + extension);
					 console.log("width:" + imgWidth);
					 console.log("height:" + imgHeight);
					 if (imgHeight == 200 && imgWidth == 200 && (extension == "jpg" || extension == "png"))
						return true;
					
						 }; 
		
		// basic
		$("#form").validate({
			highlight: function( label ) {
				console.log("validation starts");
				$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function( label ) {
				$(label).closest('.form-group').removeClass('has-error');
				label.remove();
			},
			errorPlacement: function( error, element ) {
				var placement = element.closest('.input-group');
				if (!placement.get(0)) {
					placement = element;
				}
				if (error.text() !== '') {
					placement.after(error);
				}
			}
		});

	}).apply( this, [ jQuery ]);
	</script>
	

</body>

<!-- Examples -->



</html>