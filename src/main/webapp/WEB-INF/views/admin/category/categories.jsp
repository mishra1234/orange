
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><!DOCTYPE html>
<html>

<body>

	<script>
		$(document).ready(function() {
			$('#categoryTable').dataTable({
				"paging" : false,
				"info" : false,
				"order" : [ [ 1, "asc" ] ]
			});
		});
	</script>


	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Categories</h2>
		</header>
		<div class="panel-body">
			<h5>
				<a href="<c:url value='/admin/category/new'/>">Add new</a>
			</h5>
			<table class="table table-bordered table-striped mb-none"
				id="categoryTable">
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
						<th>Parent</th>
						<th>Communities</th>
						<th>Connections</th>
						<th>Activities</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="cat" items="${myList}" varStatus="row">
						<tr>
							<td><a
								href="<c:url value='/admin/communities/category/${cat.id}'/>"><img
									src="${cat.iconUrl}" width="25" height="25"
									onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
							<td>${cat.name}</td>
							<td><c:forEach var="subcat"
									items="${cat.categoriesForParentCategoryId}" varStatus="row">
						${subcat.name}
						</c:forEach></td>
							<td>${cat.numberLayers}</td>
							<td>${cat.connections}</td>
							<td>${cat.numberEvents}</td>
							<td>
								<a href="<c:url value='/admin/category/delete/${cat.id}'/>"><i class="fa fa-minus-circle" title="Delete"></i></a>
								<a href="<c:url value='/admin/category/edit/${cat.id}'/>"> <i class="fa fa-edit" title="Edit"></i></a>
								<a href="<c:url value='/admin/category/${cat.id}'/>"><i class="fa fa-tasks" title="View"></i></a></td>
						
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</section>
</body>

</html>