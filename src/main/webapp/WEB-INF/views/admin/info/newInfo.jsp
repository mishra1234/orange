<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/pnotify/pnotify.custom.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>
		
</head>


<body>
	
	<link rel="stylesheet" href="<c:url value="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/theme.css"/>">
	
	<link rel="stylesheet" href="<c:url value="/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/select2/select2.css"/>">


<c:url var="var" value='/admin/info/save' />
	<form:form modelAttribute="newActInfo" id="form"
		class="form-horizontal" method="POST" action="${var}">
		<fieldset>

			<section class="panel form-wizard">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a> <a href="#"
							class="fa fa-times"></a>
					</div>

					<h2 class="panel-title">New Information Activity</h2>
					<p class="panel-subtitle">Create new information activity</p>
				</header>



				<!-- Hidden IDs -->
				<form:input type="hidden" name="ownerCommunity" 	path="ownerCommunity" value="" />
				<form:input type="hidden" name="id" 			  	path="id" 				value="" />
				<form:input type="hidden" name="activityType" 	  	path="activityType" 	value="info" />
				<form:input type="hidden" name="deleted" 		  	path="deleted" 			value="false" />
				<form:input type="hidden" name="actionUrl" 		  	path="actionUrl" 		value="NotUsedAnymore" />
				<form:input type="hidden" name="actionTitle" 	  	path="actionTitle" 		value="NotUsedAnymore" />
				<form:input type="hidden" name="createdDatetime"  	path="createdDatetime" 	value="${currentTS}" />
				<form:input type="hidden" name="createdAccountId" 	path="createdAccountId"	value="${currentUser}" />
				<form:input type="hidden" name="recommended" 	  	path="recommended" 		value="" />
				<input 		type="hidden" name="titleFile[]" 		value="" /> 
				<input 		type="hidden" name="descriptionFile[]" 	value="" /> 
				<input 		type="hidden" name="fileUrl[]" 			value="" /> 
				<input 		type="hidden" name="fileRemove[]" 		value="" /> 
				<input 		type="hidden" name="fileId[]" 			value="" /> 
				<input 		type="hidden" name="titleVideo[]" 		value="" /> 
				<input 		type="hidden" name="descriptionVideo[]" value="" /> 
				<input 		type="hidden" name="videoUrl[]" 		value="" /> 
				<input 		type="hidden" name="videoRemove[]" 		value="" /> 
				<input 		type="hidden" name="videoId[]" 			value="" />
				<input 		type="hidden" name="videoIconUrl[]"		value="" />
				<input 		type="hidden" id="commId" name="commId" value="${commId}"/>


				<!-- Wizard Steps -->
				<div class="panel-body">
					<div class="wizard-progress wizard-progress-lg">
						<div class="steps-progress">
							<div class="progress-indicator"></div>
						</div>
						<ul class="wizard-steps">
							<li class="active">
								<a href="#w4-account" data-toggle="tab"><span>1</span>Info Detail</a>
							</li>
							<li>
								<a href="#w4-profile" data-toggle="tab"><span>2</span>Banners/Files</a>
							</li>
						</ul>
					</div>

					<div class="tab-content">
						<div id="w4-account" class="tab-pane active">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="iconUrl">Title</label>
								<div class="col-sm-9">
									<form:input id="ContentTitle" name="contentTitle" type="patternTitle" placeholder="ContentTitle" class="form-control" path="contentTitle" />
								</div>
							</div>
							
						<div class="form-group">
							<div class="col-md-12">
								<label class="col-sm-3 control-label" for="commodities">Commodities
									<abbr title="Select commodity groups"><i
										class="fa fa-question-circle"></i></abbr>
								</label>
								<div class="col-sm-9">
									<div class="input-group mb-md">
										<select name="commodities" multiple
											data-plugin-selectTwo class="form-control populate"
											style="padding: 0px 0px; width:100%">
										<!--  <optgroup label="SA Regional Councils">-->
											<c:forEach var="listValue" items="${managedLayers}">
												<c:forEach var="comm" items="${newActInfo.layers}">
													<c:choose>
														<c:when test="${listValue.id == comm.id}">
															<option value="${listValue.id}" selected="selected">${listValue.name}</option>
														</c:when>
													</c:choose>
												</c:forEach>
												<option value="${listValue.id}">${listValue.name}</option>
											</c:forEach>
											<!--</optgroup>-->	
										</select>
									</div>
								</div>
							</div>
						</div>
							
								
						<!-- ICON input-->
						<div id="dropzone">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="iconUrl">Icon
									<abbr title="Drag and drop an image close to the DROP FILES HERE to start uploading an Icon or just click on the Choose File button"><i class="fa fa-question-circle"></i></abbr> 
									<span class="required">*</span></label>
								<div class="col-sm-9">
									<div class="input-group mb-md"> 
										<form:input id="theIcon" name="iconUrl" type="urlIcon" class="form-control" value="" path="iconUrl"/>
										<span class="input-group-btn">
											<a class="iframeIcon btn btn-success" href="<c:url value='/admin/medialibrary/iconsInfos'/>">
												<abbr title="Click the button if want an icon from the media library">Library</abbr>
											</a>
										</span>
									</div>	 
								</div>
							</div>
							<!-- Upload files -->
							<div class="form-group">
								<label class="col-sm-3 control-label">Upload Icon</label>
								<div class="col-sm-9">
									<input id="fileuploadInfo" type="file" data-url="../../../file/upload?"> Drop files here
									<div id="progressInfoIcon" class="progress">
										<div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">Complete</div>
									</div>
									<div id="displayIcon"></div>
								</div>
							</div>
						</div>
						<br>
					
						<div class="form-group">
							<label class="col-sm-3 control-label" for="iconUrl">Description<span
								class="required">*</span></label>
							<div class="col-sm-9">
								<form:textarea id="ContentBody" name="contentBody" type="patternBody" placeholder="All details about the event"
									class="form-control" path="contentBody" rows="10" cols="50" required="required" />
							</div>
						</div>
	
						<div class="form-group">
							<label class="col-sm-3 control-label" for="account.id">Info Owner<span class="required">*</span>
							</label>
							<div class="col-sm-9">
								<form:select id="account.id" name="account.id" class="form-control" path="account.id" required="required">
									<c:forEach var="manager" items="${listOfOwners}" varStatus="row">
										<option value="${manager.id}">${manager.name}</option>
									</c:forEach>
								</form:select>
							</div>
						</div>
						</div>
	
						<br>
	
						<!-- BANNER -- IMAGE UPLOAD -->
						<div id="w4-profile" class="tab-pane">
							<div id="dropzoneBanner" class="panel-body">
								<div class="control-group">
									<div class="form-group">
										<label class="col-sm-3 control-label" for="imageUrl">Banner Image</label>
										<div class="col-sm-9">
											<div class="input-group mb-md"> 
												<input id="bannerImage" name="imageUrl" type="urlBanner" class="form-control" />
												<span class="input-group-btn">
													<a class="iframeBanner btn btn-success" href="<c:url value='/admin/medialibrary/imagesBanners'/>">
														<abbr title="Click the button if want a banner from the media library">Library</abbr>
													</a>
												</span>
											</div>
										</div>
									</div>
									<!-- Upload files for Banner -->
									<div class="form-group">
										<label class="col-sm-3 control-label">Upload Banner </label>
										<div class="col-sm-9">
											<input id="fileuploadBanner" type="file" name="filesBanner[]" data-url="../../../file/uploadBanners?"> Drop file here
											<div id="progress" class="progress">
												<div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">Complete</div>
											</div>
											<div id="displayBanner"></div>
										</div>
									</div>
									<!-- BANNER -- FILE FILEUPLOAD TABLE-->
									<div class="form-group">
										<label class="col-sm-3 control-label">Banners</label>
										<div class="col-sm-9">
											<div style="width: 200px; padding: 20px">
												<table id="uploaded-files-banner" class="table">
													<tr class="currB">
														<th>Banner Title</th>
														<th>Banner SubTitle</th>
														<th>Banner Image</th>
														<th>Option Remove</th>
													</tr>
													<c:forEach var="banner" items="${bannerList}" varStatus="row">
														<tr class="currB">
															<td><input value="${banner.title}" name="titleBanner[]" /></td>
															<td><input value="${banner.subtitle}" name="subtitleBanner[]" /></td>
															<td><input type="hidden" value="${banner.id}" name="bannerId[]" /> 
															 	<input type="hidden" value="${banner.imageUrl}" name="bannerUrl[]" /> 
																<input type="hidden" value="0" name="bannerRemove[]" /> 
																<img src="<c:url value="${banner.imageUrl}"/>" alt="TestDisplay" width="30" height="20" name="urlBanner" /></td>
															<td><input type="button" class="remo btn-xs btn-danger" value="Remove"/></td>
														</tr>
													</c:forEach>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- FILE FILE LOAD -->
							<div id="dropzoneFile" class="panel-body">
								<div class="control-group">
									<div class="form-group">
										<label class="col-sm-3 control-label">PDF File</label>
										<div class="col-sm-9">
											<div class="input-group mb-md"> 
												<input id="infoFile" type="text" name="infoFile" class="form-control" />
												<span class="input-group-btn">
													<a class="iframeFile btn btn-success" href="<c:url value='/admin/medialibrary/evFiles'/>">
														<abbr title="Click the button if want a PDF from the media library">Library</abbr></a></span>
									</div></div></div>
									<!-- Upload files -->
									<div class="form-group">
										<label class="col-sm-3 control-label">Upload PDF File</label>
										<div class="col-sm-9">
											<input id="infoUploadFile" type="file" name="filesFile[]" data-url="../../../file/uploadFiles?"> Drop file here
											<div id="progressFile" class="progress">
												<div class="progress-bar progress-bar-primaryFile" role="progressbar" style="width: 0%;">Complete</div>
											</div>
										</div>
									</div>
									<!-- FILES -- FILE FILEUPLOAD -->
									<div class="form-group">
										<label class="col-sm-3 control-label">PDF Files</label>
										<div class="col-sm-9">
											<div style="width: 200px; padding: 20px">
												<table id="uploaded-files-infoFile" class="table">
													<tr class="currF">
														<th>File Name</th>
														<th>File Description</th>
														<th>File Image</th>
														<th>Option Remove</th>
													</tr>
													<c:forEach var="file" items="${filesList}" varStatus="row">
														<tr class="currF">
															<td><input value="${file.title}" name="titleFile[]" /></td>
															<td><input value="${file.description}" name="descriptionFile[]" /></td>
															<td><input type="hidden" value="${file.id}" name="fileId[]" /> 
																<input type="hidden" value="${file.url}" name="fileUrl[]" /> 
																<input type="hidden" value="0" name="fileRemove[]" /> 
																<img src="<c:url value="https://s3.amazonaws.com/ccmeresources/30d2b739-3137-4a3b-b07f-48cdfad5c8cf-pdf.png"/>"	
																alt="TestDisplay" width="20" height="20" name="iconUrlFile" /></td>
															<td><input type="button" class="remo btn-xs btn-danger" value="Remove"/></td>
														</tr>
													</c:forEach>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- VIDEO FILE LOAD -->
							<div id="dropzonevideo" class="panel-body">
								<div class="control-group">
									<div class="form-group">
										<label class="col-sm-3 control-label">Add Video</label>
										<div class="col-sm-9">
											<div class="input-group mb-md"> 
												<input id=eventvideo type="text" name="eventVideo" class="form-control" />
												<span class="input-group-btn">
													<a class="iframeVideo btn btn-success" href="<c:url value='/admin/medialibrary/infVideos'/>">
														<abbr title="Click the button if want a video from the media library">Library</abbr>
													</a>
												</span>
											</div>
										</div>
									</div>
									<!-- Upload files -->
									<div class="form-group">
										<label class="col-sm-3 control-label">Upload Video</label>
										<div class="col-sm-9">
											<input id="fileuploadvideo" type=file name="filesVideo[]" data-url="../../../file/uploadVideos?"> Drop file here
											<div id="progressvideo" class="progress">
												<div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">Complete</div>
											</div>
											<div id="displayVideo"></div>
										</div>
									</div>
									<!-- VIDEO -- FILE FILEUPLOAD -->
									<div class="form-group">
										<label class="col-sm-3 control-label">Videos</label>
										<div class="col-sm-9">
											<div style="width: 200px; padding: 20px">
												<table id="uploaded-files-eventvideo" class="table">
													<tr class="currV">
														<th>Video Name</th>
														<th>Video Description</th>
														<th>Video Image</th>
														<th>Option Remove</th>
													</tr>
													<c:forEach var="video" items="${videosList}" varStatus="row">
														<tr class="currV">
															<td><input value="${video.title}" name="titleVideo[]" /></td>
															<td><input value="${video.description}" name="descriptionVideo[]" /></td>
															<td><input type="hidden" value="${video.id}" name="videoId[]" /> 
																<input type="hidden" value="${video.url}" name="videoUrl[]"/>
																<input type="hidden" value="${video.iconUrl}" name="videoIconUrl[]"/>
																<input type="hidden" value="0" name="videoRemove[]" /> 
																<img src="<c:url value="${video.iconUrl}"/>" alt="TestDisplay" width="32" height="25"/></td>
															<td><input type="button" class="remo btn-xs btn-danger" value="Remove"/></td>
														</tr>
													</c:forEach>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- WEB LINK ADD -->
							<div class="form-group">
								<label class="col-sm-3 control-label">Add Weblinks</label>
								<div class="col-sm-9">				
									<table id="weblinks"  class="table">
										<thead>
											<tr class="currWLHeaders">
												<th class="col-md-2">Weblink Name</th>
												<th class="col-md-3">Weblink Description</th>
												<th class="col-md-6">Weblink Address</th>
												<th class="col-md-1">Options</th>
											</tr>
										</thead>
										<tbody>
								          	<c:forEach var="wl" items="${weblinksList}" varStatus="row">
												<tr class="currWL">
													<td class="col-md-2"><input id="titleWL" class="titleWL col-md-12" value="${wl.title}" 		 name="titleWL[]"/>
																		 <input type="hidden" value="${wl.id}" 									 name="idWL[]"/></td>
													<td class="col-md-3"><input id="descrWL" class="descrWL col-md-12" value="${wl.description}" name="descriptionWL[]"/></td>
													<td class="col-md-6"><input id="addrsWL" class="addrsWL col-md-12" value="${wl.url}" 		 name="urlWL[]"/></td>
													<td class="col-md-1"><a id="addWL"><i class="fa fa-plus-circle fa-lg text-success"></i></a>
								                	 					 <a class="removeItem"><i class="fa fa-minus-circle fa-lg text-danger"></i></a>
													</td>
												</tr>
											</c:forEach>
											<tr>
												<td class="col-md-2"><input id="titleWL" class="titleWL  col-md-12" value="${wl.title}" 	   name="titleWL[]"/>
																	 <input type="hidden" value="" 											   name="idWL[]"/></td>
												<td class="col-md-3"><input id="descrWL" class="descrWL  col-md-12" value="${wl.description}"  name="descriptionWL[]"/></td>
												<td class="col-md-6"><input id="addrsWL" class="addrsWL  col-md-12" value="${wl.url}" 	 	   name="urlWL[]"/></td>
												<td class="col-md-1"><a id="addWL"><i class="fa fa-plus-circle fa-lg text-success"></i></a>
							                	 	 				 <a class="removeWL"><i class="fa fa-minus-circle fa-lg text-danger"></i></a>
												</td>
								            </tr>
								    	</tbody>
									</table>
								</div>
							</div>
						</div>

						<!-- Button (Double) -->
						<div class="panel-footer">
							<ul class="pager">
								<li class="previous disabled"><a><i class="fa fa-angle-left"></i> Previous</a></li>
								<li class="finish hidden pull-right">
									<button type="submit" id="Save" class="btn btn-primary">Save</button>
								</li>
								<li class="next"><a>Next <i class="fa fa-angle-right"></i></a></li>
							</ul>
						</div>
					</div>
				
				</div>
		</fieldset>
	</form:form>

	<!-- Vendor -->
	<script src="../../../assets/vendor/jquery/jquery.js"></script>
	<script src="../../../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
	<script src="../../../assets/vendor/bootstrap/js/bootstrap.js"></script>
	<script src="../../../assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="../../../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/vendor/magnific-popup/magnific-popup.js"></script>
	<script src="../../../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
	
	<!-- Specific Page Vendor -->
	<script src="../../../assets/vendor/jquery-validation/jquery.validate.js"></script>
	<script src="../../../assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
	<script src="../../../assets/vendor/pnotify/pnotify.custom.js"></script>
	
	<!-- Theme Base, Components and Settings -->
	<script src="../../../assets/javascripts/theme.js"></script>
	
	<!-- Theme Custom -->
	<script src="../../../assets/javascripts/theme.custom.js"></script>
	
	<!-- Theme Initialization Files -->
	<script src="../../../assets/javascripts/theme.init.js"></script>
	<script src="../../../assets/javascripts/forms/examples.wizard.js"></script>
	
	<script>(function() {		
				'use strict';	
			    
				/* function isValidContentBody() {
					var s = $('#ContentBody').val();
					s = s.replace(/[\u00B8]/g, ",");
				    s = s.replace(/[\u201E|\u201C|\u201D|\u00AB|\u00BB]/g, '\"');
					s = s.replace(/[\u02C6|\u2018|\u2019|\u201A|\u00B4|\u00B7|\u02C8|\u02CA|\u02CB|\u02B9|\u02BB\u02C1|\u0300|\u0301]/g, "\'");
					s = s.replace(/[\u030D|\u0312\u0315|\u031B|\u0340\u0343|\u0374|\u2032|\u2035|\u0091|\u0092|\u]/g, "\'");
				    s = s.replace(/[\u2013|\u2014]/g, "-");
					s = s.replace(/[\u2026]/g, "...");
                    //s = s.replace(/[^A-Za-z-_0-9!@#$%?^&*.,+=;:\[\]\\\/|`~()><\n\s]/g, 00000);
					s = s.replace(/[\u201C|\u201D|\u201E]/g, "\"");
					s = s.replace(/\u02C6/g, "^");
					s = s.replace(/\u203A/g, ">");
					s = s.replace(/[\u02DC|\u00A0]/g, " ");
				    console.log(s);
					return true;
				}; 

				jQuery.validator.addMethod("patternBody", function(value, element) {
					return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;:\[\]\\\\/|`~()><\n\s]+)$/.test(value);
					return isValidContentBody();
					    }, "Your data contain invalid characters"); */
				
				/* function isValidContentTitle() {
						var s = $('#ContentTitle').val();
						s = s.replace(/[\u00B8]/g, ",");
					    s = s.replace(/[\u201E|\u201C|\u201D|\u00AB|\u00BB]/g, '\"');
						s = s.replace(/[\u02C6|\u2018|\u2019|\u201A|\u00B4|\u00B7|\u02C8|\u02CA|\u02CB|\u02B9|\u02BB\u02C1|\u0300|\u0301]/g, "\'");
						s = s.replace(/[\u030D|\u0312\u0315|\u031B|\u0340\u0343|\u0374|\u2032|\u2035|\u0091|\u0092|\u]/g, "\'");
					    s = s.replace(/[\u2013|\u2014]/g, "-");
						s = s.replace(/[\u2026]/g, "...");
						s = s.replace(/[^A-Za-z-_0-9!@#$%?^&*.,+=;:\[\]\\\/|`~()><\n\s]/g, 00000);
						s = s.replace(/[\u201C|\u201D|\u201E]/g, "\"");
						s = s.replace(/\u02C6/g, "^");
						s = s.replace(/\u203A/g, ">");
						s = s.replace(/[\u02DC|\u00A0]/g, " ");
					    console.log(s);
						return true;
				};
				
				jQuery.validator.addMethod("patternTitle", function(value, element) {
					return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;:\[\]\\\\/|`~()><\n\s]+)$/.test(value);
					return isValidContentTitle();
				}, "Your data contain invalid characters");   */	
					
			}).apply( this, [ jQuery ]);
			</script>
			<script>(function() {			
 				/* jQuery.validator.addMethod("urlIcon", function(value, element) {
			    	return isValidIconSize();
				}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only."); */
						
					 
				/* jQuery.validator.addMethod("pattern", function(value, element) {
   					return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;:\[\]\\\\/|`~()>< \n\s]+)$/.test(value);
					}, "Your data contain invalid characters"); */
		   
 				/* jQuery.validator.addMethod("urlIcon", function(value, element) {
 			    	return isValidIconSize();
 				}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only."); */
		
			/*  function isValidIconSize(){
	 			 var imgWidth = $("#theInfoIcon").width();
	 			 var imgHeight = $("#theInfoIcon").height();
	 			 var extension = $('#theInfoIcon').attr("src").split('.').pop().toLowerCase();;
	 			 console.log("URL: " + extension);
	 			 console.log("width:" + imgWidth);
	 			 console.log("height:" + imgHeight);
	 			 if (imgHeight == 200 && imgWidth == 200 && (extension == "jpg" || extension == "png"))
	 				return true;
	 				 };  */  
			

			  
			 /* jQuery.validator.addMethod("urlBanner", function(value, element) {
			    	return isValidBannerSize();
				}, "Invalid Banner, use 640px X 320px size and use .jpg or .png images only."); */
			
 			 /* function isValidBannerSize(){
 				 var imgWidth = $("#theInfoBanner").width();
 				 var imgHeight = $("#theInfoBanner").height();
 				 var extension = $('#theInfoBanner').attr("src").split('.').pop().toLowerCase();;
 				 console.log("Banner URL: " + extension);
 				 console.log("Banner width:" + imgWidth);
 				 console.log("Banner height:" + imgHeight);
 				 if (imgHeight == 320 && imgWidth == 640 && (extension == "jpg" || extension == "png"))
 					return true;
 					 }; */   
 			
					//Wizard validation
					
					var $w3finish = $('#form').find('ul.pager li.finish'), $w3validator = $("#form").validate({
						highlight: function(element) {
							$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
						},
						success: function(element) {
							$(element).closest('.form-group').removeClass('has-error');
							$(element).remove();
						},
						errorPlacement: function( error, element ) {
							element.parent().append( error );
						}
					});
					
					// Submit the from when click finish
					$w3finish.on('click', function( ev ) {
						//ev.preventDefault();
						var validated = $('#form').valid();
						if ( validated ) {
							$("#Save").click(function() {
								  $("#form").submit();
								});
						}
					});

					$('#form').bootstrapWizard({
						tabClass: 'wizard-steps',
						nextSelector: 'ul.pager li.next',
						previousSelector: 'ul.pager li.previous',
						firstSelector: null,
						lastSelector: null,
						onNext: function( tab, navigation, index, newindex ) {
							console.log("Wizard validation started!!");
							var validated = $('#form').valid();
							if( !validated ) {
								$w3validator.focusInvalid();
								return false;
							}
						},
						onTabClick: function( tab, navigation, index, newindex ) {
							if ( newindex == index + 1 ) {
								return this.onNext( tab, navigation, index, newindex);
							} else if ( newindex > index + 1 ) {
								return false;
							} else {
								return true;
							}
						},
						onTabChange: function( tab, navigation, index, newindex ) {
							var $total = navigation.find('li').size() - 1;
							$w3finish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
							$('#form').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
						},
						onTabShow: function( tab, navigation, index ) {
							var $total = navigation.find('li').length - 1;
							var $current = index;
							var $percent = Math.floor(( $current / $total ) * 100);
							$('#form').find('.progress-indicator').css({ 'width': $percent + '%' });
							tab.prevAll().addClass('completed');
							tab.nextAll().removeClass('completed');
						}
					});
					
					// basic
					$("#form").validate({
						highlight: function( label ) {
							console.log("validation starts");
							$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
						},
						success: function( label ) {
							$(label).closest('.form-group').removeClass('has-error');
							label.remove();
						},
						errorPlacement: function( error, element ) {
							var placement = element.closest('.input-group');
							if (!placement.get(0)) {
								placement = element;
							}
							if (error.text() !== '') {
								placement.after(error);
							}
						}
					});

				}).apply( this, [ jQuery ]);
		</script>
		
		<script>$(document).ready(function(){
			$('table[id="uploaded-files-banner"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.currB").remove();
			});
			
			$('table[id="uploaded-files-eventFile"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.currF").remove();
			});
			$('table[id="uploaded-files-infoFile"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newF").remove();
			});	
			$('table[id="uploaded-files-eventVideo"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.currV").remove();
			});
			
			$('table[id="weblinks"]').on("click", 'a.removeItem',  function(event){
				$(event.target).closest("tr.currWL").remove();
			});
			
			// COLORBOX - IFRAME
			$(".iframeIcon").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var bg = $.colorbox.element().data('bg');
		            $('#cboxLoadedContent').css('backgroundColor', bg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myIcon;
					$('#theIcon').val(test);
					$("#displayIcon").empty();
		       	 	$("#displayIcon").append("<img id='theIcon' width='100' height='100' src='"+test+"'/>");
				}
			});
			
			$(".iframeBanner").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var otherbg = $.colorbox.element().data('otherbg');
		            $('#cboxLoadedContent').css('backgroundColor', otherbg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myBanner;
					$('#bannerImage').val(test);
					$("#displayBanner").empty();
		       	 	$("#displayBanner").append("<img id='bannerImage' src='"+test+"'/>");
		       	 	$("#uploaded-files-banner").append(
	                		$('<tr class="newB"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="subtitleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="bannerUrl[]" value="'+test+'"/>'+
													'<img src="'+test+'" width="30px" height="20px"/>'))
	                		.append($('<td/>').html('<input type="button" class="remo btn-xs btn-danger" value="Remove"/>'))
	                		)
				}
			});
			$('table[id="uploaded-files-banner"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newB").remove();
			});
			
			$(".iframeFile").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var videobg = $.colorbox.element().data('videobg');
		            $('#cboxLoadedContent').css('backgroundColor', videobg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var fileId = localStorage.myFileId;
					var fileIconUrl = localStorage.myFileIconUrl;
					var fileUrl = localStorage.myFileUrl;
					
		       	 	$("#displayFile").append("<img id='fileFile' src='"+fileIconUrl+"'/>");
		       	 	$("#uploaded-files-infoFile").append(
	                		$('<tr class="newF"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleFile[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="descriptionFile[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="fileId[]"  value="'+fileId+'"/>'+
	    	                						'<input type="hidden" name="fileUrl[]" value="'+fileUrl+'"/>'+
	    	                						'<input type="hidden" name="fileIconUrl[]" value="'+fileIconUrl+'"/>'+
													'<img src="'+fileIconUrl+'" width="32" height="25"/>'))
	                		.append($('<td/>').html('<input type="button" class="remoFi btn-xs btn-danger" value="Remove"/>'))
	                		);
				}
			});
			$('table[id="uploaded-files-infoFile"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newF").remove();
			});
			
			$(".iframeVideo").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var videobg = $.colorbox.element().data('videobg');
		            $('#cboxLoadedContent').css('backgroundColor', videobg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myVideo;
					$('#fileVideo').val(test);
					var vidId = localStorage.myVideoId;
					$("#displayVideo").empty();
		       	 	$("#displayVideo").append("<img id='fileVideo' src='"+test+"'/>");
		       	 	$("#uploaded-files-eventvideo").append(
	                		$('<tr class="newV"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleVideo[]"/>'))
                			.append($('<td/>').html('<input type="text"   name="descriptionVideo[]"/>'))
                			.append($('<td/>').html('<input type="hidden" name="videoId[]" value=""/>'+
				                					'<input type="hidden" name="videoUrl[]" value="'+vidId+'"/>'+
				            						'<input type="hidden" name="videoIconUrl[]" value="'+test+'"/>'+
													'<img src="'+test+'" width="32" height="25"/>'))
	                		.append($('<td/>').html('<input type="button" class="remoVi btn-xs btn-danger" value="Remove"/>'))
	                		)
				}
			});
			$('table[id="uploaded-files-eventvideo"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newV").remove();
			});
		});	
		</script>
		
		<script>
		$(function () {
      		var id = 1;
          	$(document).on('click', '#addWL', function () {
      		var currentTable = $(this).closest('table').attr('id');
          	id++;
      		$('#'+ currentTable ).append(
      				'<tr>'+
      				'<td class="col-md-2"><input id="titleWL" class="titleWL  col-md-12" value="${wl.title}" 	   name="titleWL[]"/>'+
      				'<input type="hidden" value="" name="idWL[]"/></td>'+
					'<td class="col-md-3"><input id="descrWL" class="descrWL  col-md-12" value="${wl.description}" name="descriptionWL[]"/></td>'+
					'<td class="col-md-6"><input id="addrsWL" class="addrsWL  col-md-12" value="${wl.url}" 	 	   name="urlWL[]"/></td>'+
					'<td class="col-md-1">'+
	                	'<a id="addWL"><i class="fa fa-plus-circle fa-lg text-success"></i></a>'+
	                	'<a class="removeWL"><i class="fa fa-minus-circle fa-lg text-danger"></i></a></td></tr>');
          	});
              
		    $(document).on('click', '.removeWL', function () {
		            var currentTable = $(this).closest('table').attr('id');
			    	$(this).closest('tr').remove();
		    });
      	});
		</script>
		
		<link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />	
		<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>
		
		<!-- fileupload scripts -->
		<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadIconForInfos.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadImgsForBanners.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadFilesForInfos.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadVideosForEvents.js'/>"></script>

		<!-- Specific Page Vendor -->
		<script src="<c:url value='/assets/vendor/jquery-maskedinput/jquery.maskedinput.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js'/>"></script>
		<script src="<c:url value='/assets/vendor/codemirror/mode/css/css.js'/>"></script>
		<script src="<c:url value='/assets/vendor/summernote/summernote.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js'/>"></script>
		<script src="<c:url value='/assets/vendor/ios7-switch/ios7-switch.js'/>"></script>

	</section>
</body>
</html>