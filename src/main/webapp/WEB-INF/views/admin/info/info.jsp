<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html>
<body>
<fieldset>

<br>
<h4>Information Activity:  ${inf.contentTitle}</h4>
<br>
<div class="slider">
	<ul class="bxslider">
		<c:forEach var="bann" items="${listOfBanners}" varStatus="row">
	  			<li>
	  				<img src="${bann.imageUrl}" title="${bann.subtitle}">
	  				<div class="text-section">
                    	<p class="subtitle"><h6>${bann.title}</h6></p>
                    	<br><br>
                </div>
	  			</li>
	  	</c:forEach>
	</ul>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.bxslider').bxSlider({
		  mode: 'fade',
		  captions: true,
		  auto: true,
		  slideWidth: 700
		});
		(function($){
			'use strict';
			var initBasicWithMarkers = function() {
				var map = new GMaps({
					div: '#gmap-basic-marker',
					lat: ${avLat},
					lng: ${avLng},
					markers: [{
						lat: ${avLat},
						lng: ${avLng},
						infoWindow: {
							content: '<p>Basic</p>'
						}
					}]
				});
				map.addMarker({
					lat: ${avLat},
					lng: ${avLng},
					infoWindow: {
						content: '<p>${inf.contentTitle}</p>'
					}
				});
			};
			// auto initialize
			$(function() {
				initBasicWithMarkers();
			});
		}).apply(this, [ jQuery ]);
	});
</script>

<link rel="stylesheet" href="http://bxslider.com/lib/jquery.bxslider.css" type="text/css" />
<link rel="stylesheet" href="http://bxslider.com/css/github.css" type="text/css" />
<script src="http://bxslider.com/js/jquery.min.js"></script>
<script src="http://bxslider.com/lib/jquery.bxslider.js"></script>
<script src="http://bxslider.com/js/rainbow.min.js"></script>
<script src="http://bxslider.com/js/scripts.js"></script>
	

<table id="eventTable">
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Information Activity Owner:</FONT></td>		<td>${inf.account.name}</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Event Details:</FONT>				</td>		<td>${inf.contentBody}</td>
	<tr><td>																		</td>		<td></td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Recommended?:</FONT>				</td>		<td><c:choose>
																									  <c:when test="${inf.recommended}">Yes</c:when>
																									  <c:otherwise>No</c:otherwise>
																									</c:choose></td>	
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Icon:</FONT>						</td>		<td><img src="${inf.iconUrl}" width="50" height="50" 
						 					 										onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';">
						 					 										</td>
	<tr><td>																		</td>		<td></td>
	<tr><td>																		</td>		<td><br></td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Files:</FONT></td>				<td>
																					<c:forEach var="file" items="${listOfFiles}" varStatus="row">
																						<a href="${file.url}"> ${file.title}</a>
																					</c:forEach></td>
	<tr><td>																		</td>		<td><br></td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Videos:</FONT></td>				<td>
																					<c:forEach var="video" items="${listOfVideos}" varStatus="row">
																						<video src="${video.url}" controls style="width:240px;height:120px;"></video>
																					</c:forEach></td>
	<tr><td>																		</td>		<td><br></td>																					
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Web-Links:</FONT></td>			<td>
																					<c:forEach var="wls" items="${listOfWeblinks}" varStatus="row"> 	
																						<a href="${wls.url}"> ${wls.title}</a>
																					</c:forEach>
																					</td>						 					 										
	<tr><td>																		</td>		<td><br></td>						 					 								
</table>

<br>
 
<h5>  
	  <a class="btn" href="<c:url value='/admin/info/editInfo/${inf.id}'/>">Edit Information</a> 
	  <a class="btn" href="<c:url value='/admin/event/promote/${inf.id}'/>">Promote Activity</a>
</h5>

</fieldset>
</body>

</html>