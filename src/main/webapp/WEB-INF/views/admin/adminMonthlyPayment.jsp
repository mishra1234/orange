<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="<c:url value='/js/lib/jquery.payment.js'/>"></script>
	
<script type="text/javascript" src="<c:url value='/assets/vendor/jquery-validation/jquery.validate.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js'/>"></script>
		<script src="<c:url value='/assets/vendor/pnotify/pnotify.custom.js'/>"></script>
		
	
	<!-- Page Title
		============================================= -->
<section id="page-title">
	<div class="container clearfix">
		<div class="row">
			
			<div class="col-md-3"></div>
		</div>
	</div>
</section>


<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="single-event">

						<div class="col_three_fourth">
						<h3>Secure Pay</h3>
							<div class="entry-image nobottommargin">
						<form novalidate autocomplete="on" method="POST" action="process" method="POST">
						
							
						   <div class="row">
                                  <div class="form-group">
                                        <div class="col-md-8">
								<label for="cc-nname" class="control-label">Name in card</label>
								<input id="cc-nname" name="name" class="form-control input-lg" required>
							</div>
							</div>
							

							<div class="form-group">
							 <div class="col-md-8">
								<label for="cc-number" class="control-label">Card number  <small class="text-muted"><span class="cc-brand"></span></small></label>
								<input id="cc-number" type="tel" name="number"
									class="input-lg form-control cc-number"
									autocomplete="cc-number" type="pattern" placeholder="•••• •••• •••• ••••" required>
							</div>
							</div>

							<div class="form-group">
							 <div class="col-md-8">
								<label for="cc-exp" class="control-label">Card expiry
									date</label> <input id="cc-exp" type="tel" name="expiry"
									class="input-lg form-control cc-exp" autocomplete="cc-exp"
									placeholder="•• / ••••" required>
							</div>
</div>
							<div class="form-group">
							 <div class="col-md-8">
								<label for="cc-cvc" class="control-label">Card CVC</label> <input id="cc-cvc" type="tel" name="cvc"
									class="input-lg form-control cc-cvc" autocomplete="off"
									placeholder="•••" required>
							</div>
</div>
							<div class="form-group">
							 <div class="col-md-8">
								<label for="numeric" class="control-label">
									Total amount:</label> <input name="amount"
									class="input-lg form-control">
							</div>
							</div>
							</div>
<br>
<%-- 							<a href="<c:url value='/admin/pay/process'/>"  class="btn btn-lg btn-primary">Pay Now</a> --%>
							<button type="submit" id ="reg" class="btn btn-lg btn-primary">Pay now</button>
						
							<h2 class="validation"></h2>
						</form>
						 
				
						<script>
						jQuery(function($) {
						      $('[data-numeric]').payment('restrictNumeric');
						      $('.cc-number').payment('formatCardNumber');
						      $('.cc-exp').payment('formatCardExpiry');
						     
						      $('.cc-cvc').payment('formatCardCVC');

						      $.fn.toggleInputError = function(erred) {
						        this.parent('.form-group').toggleClass('has-error', erred);
						        return this;
						      };

						      $('form').change(function(e) {
						        e.preventDefault();

						        var cardType = $.payment.cardType($('.cc-number').val());
						        $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));
						        $('.cc-exp').toggleInputError(!$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
						        $('.cc-cvc').toggleInputError(!$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
						        if($('.cc-number').val()==''){
						         $('.cc-brand').text('');
						        }
						        else{
						        $('.cc-brand').text(cardType);
						        }
	
						        $('.validation').removeClass('text-danger text-success');
						        $('.validation').addClass($('.has-error').length ? 'text-danger' : 'text-success');
						      });
						     

						    });
						  </script>
					</div>
						</div>
			
						<div class="clear"></div>


					</div>

				</div>

			</div>
			

		</section><!-- #content end -->