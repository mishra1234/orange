<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
</head>

<body>	

	<link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />
	<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/stylesheets/theme.css"/>">	
	<link rel="stylesheet" href="<c:url value="/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>">
	<link rel="stylesheet" href="<c:url value="/assets/vendor/select2/select2.css"/>">
	

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#" class="fa fa-times"></a>
					</div>
			<h2 class="panel-title">New Event</h2>
			<p class="panel-subtitle">Create a new Event</p>
		</header>
		
		<c:url var="var" value="/admin/event/save"/>
		<form:form modelAttribute="newEvent" id="form" class="form-horizontal" method="POST" action="${var}">
			
		<fieldset>
			<!-- Hidden IDs -->
			<form:input type="hidden" name="ownerCommunity" path="activity.ownerCommunity" value="" />
			<form:input type="hidden" name="href" path="href" value="${href}" />
			<form:input type="hidden" name="activityId" path="activityId" value="" />
			<form:input type="hidden" name="id" path="activity.Id" value="" />
			<form:input type="hidden" path="activity.activityType" value="event" />
			<form:input type="hidden" id="deleted" name="deleted" required="" path="activity.deleted" value="false" />
			<form:input type="hidden" id="ActionUrl" name="actionUrl" path="activity.actionUrl" value="NotUsedAnymore" />
			<form:input type="hidden" id="ActionTitle" name="actionTitle" path="activity.actionTitle" value="NotUsedAnymore" />
			<form:input type="hidden" id="createdDatetime" name="createdDatetime" path="activity.createdDatetime" value="${currentTS}" />
			<form:input type="hidden" id="createdAccountId" name="createdAccountId" path="activity.createdAccountId" value="${currentUser}" />
			<input 		type="hidden" id="commId" name="commId" value="${commId}"/>
					
					
			<!-- -- Form Elements -- -->
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">* Mandatory fields</label>
				</div>
			</div>
			
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Enable Booking </label>
					<div class="col-sm-8">
						<div class="radio-custom radio-primary">
							<form:radiobutton value="false" checked="checked" path="bookable" /> <label>No</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="true" path="bookable" /> <label>Yes</label>
				</div></div></div> 

				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Recommend Event</label>
					<div class="col-sm-8">
						<div class="radio-custom radio-primary">
							<form:radiobutton value="false" checked="checked" path="activity.recommended" /> <label>No</label>
						</div>
						<div class="radio-custom radio-primary">
							<form:radiobutton value="true" path="activity.recommended" />
							<label>Yes</label>
				</div></div></div>


				<div class="form-group">
					<label class="col-sm-3 control-label" for="commodities">Communities
						<abbr title="Select commodity groups"><i class="fa fa-question-circle"></i></abbr>
					</label>
					<div class="col-sm-9">
						<div class="input-group mb-md">
							<select name="commodities" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px; width:100%">
								<c:forEach var="listValue" items="${managedLayers}">
									<c:forEach var="comm" items="${activity.layers}">
										<c:choose>
											<c:when test="${listValue.id == comm.id}">
												<option value="${listValue.id}" selected="selected">${listValue.name}</option>
											</c:when>
										</c:choose>
									</c:forEach>
									<option value="${listValue.id}">${listValue.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>


				<!-- ICON input-->
				<div id="dropzoneIcon">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="iconUrl">Icon
							<abbr title="Drag and drop an image close to the DROP FILES HERE to start uploading an Icon or just click on the Choose File button"><i class="fa fa-question-circle"></i></abbr> 
							<span class="required">*</span></label>
						<div class="col-sm-9">
							<div class="input-group mb-md"> 
								<form:input id="theIcon" name="iconUrl" type="text" class="form-control urlImage" value="" path="activity.iconUrl"/>
								<span class="input-group-btn">
									<a class="iframeIcon btn btn-success" href="<c:url value='/admin/medialibrary/iconsEvents'/>">
										<abbr title="Click the button if want an icon from the media library">Library</abbr>
									</a>
								</span> 
							</div>
						</div>
					</div>
					<!-- Upload files -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Icon</label>
						<div class="col-sm-9">
							<input id="fileuploadEvent" type="file" name="files[]" data-url="<c:url value="../../../file/upload?"/>">
							Drop files here
							<div id="progressEventIcon" class="progress">
								<div class="progress-bar progress-bar-primaryIcon" role="progressbar"  style="width: 0%;">Complete</div>
							</div>
							<div id="displayIcon"></div>
				</div></div></div>
				<br>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Title<span class="required">*</span></label>
					<div class="col-sm-9">
						<form:input id="ContentTitle" name="contentTitle" type="patternTitle" 
							placeholder="ContentTitle" class="form-control" path="activity.contentTitle" required="required" />
				</div></div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Description<span class="required">*</span></label>
					<div class="col-sm-9">
						<form:textarea id="ContentBody" name="contentBody" type="patternBody"
							placeholder="All details about the event" class="form-control"
							path="activity.contentBody" rows="10" cols="50" required="required" />
				</div></div>
					
				<div class="form-group">
					<label class="col-sm-3 control-label" for="activity.account.id">Event Manager<span class="required">*</span></label>
					<div class="col-sm-9">
						<form:select id="accountId" name="accountId" class="form-control" path="activity.account.id" required="required">
							<form:option value="">&nbsp;</form:option>
							<form:options items="${listOfOwners}" itemValue="id" itemLabel="name"></form:options>
						</form:select>
				</div></div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Main Address<span class="required">*</span></label>
					<div class="col-sm-9">
						<form:input id="address" name="address" type="pattern" placeholder="Address" class="form-control" path="address" /><br />
				        <!-- <input type="button" class="btn btn-warning" id="go" value="Validate Address" /> --> 
                </div></div>
				<!--
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="iconUrl">Latitude & Longitude</label>
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="text" id="latitude"  placeholder="Lattitude" value="${latitude}"  
                            class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" id="longitude" placeholder="Longitude" value="${longitude}" 
                            class="form-control">
                </div></div></div>
 				-->
		
				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Contact Name</label>
					<div class="col-sm-9">
						<form:input id="ContactName" name="contactName" type="pattern" class="form-control" placeholder="Contact Name"  path="contactName" />
				</div></div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Contact email</label>
					<div class="col-sm-9">
						<div class="input-group">
							<span class="input-group-addon"> <i class="fa fa-envelope"></i></span>
							<form:input id="ContactEmail" name="contactEmail" type="text" class="form-control" placeholder="eg.: email@email.com" 
							path="contactEmail" />
				</div></div></div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Contact phone
					</label>
					<div class="col-sm-9">
						<div class="input-group">
							<span class="input-group-addon"> <i class="fa fa-phone"></i></span>
							<form:input id="contactPhone" name="contactPhone" type="text" placeholder="(123) 123-1234"
								class="form-control" path="contactPhone" />
				</div></div></div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Organisation's Name</label>
					<div class="col-sm-9">
						<form:input id="Organisation" name="organisation" type="pattern" class="form-control" placeholder="Org Name" path="organisation" />
				</div></div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="iconUrl">Cost<span class="required">*</span></label>
					<div class="col-sm-9">
						<form:input id="Cost" name="cost" type="text" placeholder="Cost" class="form-control" path="cost" />
				</div></div>

				<div id="events">
					<div id="event[0]">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-3 control-label">Event Frequency/Scheduling
                                <abbr title="Here yo ucan add as many occurrences of the event as desired">
								 <i class="fa fa-question-circle"></i>
								</abbr>
							</label>
							<!--  
							<label id="dis" style="width:250px;"></label><br>
							<div class="col-sm-8">
								<div class="radio-custom radio-primary">
									<input type="hidden" id="radioSingle"  name="dateSelected" value="single">
									<label for="radioExample1">Single Date</label>
								</div>
								<div class="radio-custom radio-primary">
									<input type="hidden" id="radioPattern" name="dateSelected" value="pattern">
									<label for="radioExample2">Pattern</label>
							</div></div>
							 -->
							<input type="hidden" id="radioSingle"  name="dateSelected" value="single">
					</div></div>
					<br>
					
					<script>	
						$('#date').keyup(function(){ 
							var date =  new Date($('#date').val());
							var weekday = new Array(7);	weekday[0]=  "SU";	weekday[1] = "MO";	weekday[2] = "TU";
								weekday[3] = "WE";		weekday[4] = "TH";	weekday[5] = "FR";	weekday[6] = "SA";
							var n = weekday[date.getDay()];
							console.log ("Minutes:"+ minutesToAdd);
							console.log("Hours:" + hoursToAdd)
							$('#days').val(n);
						});
					</script>
					
					
					<div id="hideSingle" style="display: show" class="panel-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table id="addaccount" class="table table-bordered">
							          <thead><tr class="currECIS">
							               		<th >Start Date</th>
							               		<th >Start Time</th>
							              		<th >Minutes to add</th>
							              		<th >End Date</th>
							              		<th >End Time</th>
							              		<th >Options</th>
							            	</tr>
							          </thead>
							          <tbody>
							          	<c:forEach var="eci" items="${ecis}" varStatus="row">
											<tr class="currECIS">
												<td ><input id="eciId" type="hidden" name="eciId" value="${eci.id.activityId}">
													 <input id="eciHref" type="hidden" name="eciHref" value="${eci.id.calHref}">
													 <input id="datepicker" class="start_date" value="${eci.startDate}" name="sDate"/>
													 <input id="days" type="hidden" name="days" value="${days}"></td>
												<td >
													 <input id="stime" type="text" class="start_time" placeholder="Start Time" name="sTime" value="${eci.startTime}"/></td>
												<td >
						                			 <select name="duration" class="duration_Add">
								                    	<option value="0">Time to add</option>
								                    	<option value="30">30 min</option>
								                 		<option value="60">60 min</option>
								                 		<option value="90">90 min</option>
								                 		<option value="120">2hr</option>
								                 		<option value="180">3hr</option>
								                 		<option value="240">4hr</option>
								                 		<option value="300">5hr</option>
								                 		<option value="360">6hr</option>
								                 		<option value="420">7hr</option>
								                 		<option value="480">8hr</option>
								                 		<option value="540">9hr</option>
								                 	 </select></td>
												<td ><input id="datepicker" class="end_date" value="${eci.endDate}" name="eDate"/></td>
												<td ><input id="etime" type="text" class="end_Time" placeholder="End Time" name="eTime" value="${eci.endTime}"/>
													 <input id="dateType" type="hidden" name="dateType" value="single"/>
													 </td>
												<td ><a id="addnewitem"><i class="fa fa-plus-circle fa-lg text-success"></i></a>
							                	 	 <a class="removeItem"><i class="fa fa-minus-circle fa-lg text-danger"></i></a>
													 </td>
											</tr>
										</c:forEach>
										<tr><td ><input id="eciId" type="hidden" name="eciId" value="-1">
												 <input id="eciHref" type="hidden" name="eciHref" value="-1">
							                	 <input id="datepicker" class="start_date" name="sDate" value="" placeholder="yyyy/mm/dd">
							                	 <input id="days" type="hidden" name="days" value=""></td>
							                <td >
							                     <input id="stime" type="text" class="start_time" placeholder="Start Time" name="sTime" value=""></td>
							                <td >
							                	 <select name="duration" class="duration_Add">
							                    	<option value="0">Time to add</option>
							                    	<option value="30">30 min</option>
							                 		<option value="60">60 min</option>
							                 		<option value="90">90 min</option>
							                 		<option value="120">2hr</option>
							                 		<option value="180">3hr</option>
							                 		<option value="240">4hr</option>
							                 		<option value="300">5hr</option>
							                 		<option value="360">6hr</option>
							                 		<option value="420">7hr</option>
							                 		<option value="480">8hr</option>
							                 		<option value="540">19hr</option>
							                 	 </select></td>
							                <td ><input id="datepicker" class="end_date" name="eDate" value="" placeholder="yyyy/mm/dd"></td>
							                <td ><input id="etime" type="text" class="end_Time" placeholder="End Time" name="eTime" value="">
							                	 <input id="dateType" type="hidden" name="dateType" value="single"/>
							                	 </td>
							                <td ><a id="addnewitem"><i class="fa fa-plus-circle fa-lg text-success"></i></a>
							                	 <a class="removeItem"><i class="fa fa-minus-circle fa-lg text-danger"></i></a>
							                	 </td>
							            </tr>
							          </tbody>
							    	</table>
									<div class="panel-body">
											<label class="col-md-3 control-label"></label>
											<div class="input-group"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					<div id="hidePattern" style="display: none">
						<div class="panel-body">
							<div class="form-group">
								<label class="col-md-3 control-label">Date range</label>
								<div class="col-md-6">
									<div class="input-daterange input-group" data-plugin-datepicker>
										<span class="input-group-addon"> <i class="fa fa-calendar"></i></span> 
											<input type="text" class="form-control" name="sDate" value="${sDate}" >
										<span class="input-group-addon">to</span> 
											<input type="text" class="form-control" name="eDate" value="${eDate}" >
											<input id="dateType" type="hidden" name="dateType" value="pattern"/>
						</div></div></div></div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label">Periodicity</label>
	                            <div class="col-sm-8">
                                    <div class="radio-custom radio-primary">
										<input type="radio" value="WEEKLY" name="periodicity" id="weekly"><label>Weekly</label>
									</div>
									<div class="radio-custom radio-primary">
										<input type="radio" value="MONTHLY" name="periodicity" id="monthly"><label>Monthly</label>
						</div></div></div></div>
						<div id="hideWeekly" style="display: none">
							<div class="panel-body">
								<div id="days" class="control-group">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-6 control-label">Week Days of Event</label>
		                                        <div class="controls">
		                                            <c:choose>
		                                                <c:when test="${monday==true}">                       
		                                                	<input type="checkbox" name="days" value="MO" id="days" class="roles" checked="checked" /><label for="MO">Monday </label>
		                                                </c:when>
		                                                <c:otherwise>
		                                                    <input type="checkbox" name="days" class="roles" value="MO" id="days" /><label for="MO">Monday </label>
		                                                </c:otherwise>
		                                            </c:choose>
		                                            <c:choose>
		                                                <c:when test="${tuesday==true}">
		                                                     <input type="checkbox" name="days" class="roles" value="TU" id="days" checked="checked" /><label for="TU"> Tuesday</label>
		                                                </c:when>
		                                                <c:otherwise>
		                                                    <input type="checkbox" name="days" class="roles" value="TU" id="days" /><label for="TU"> Tuesday</label>
		                                                </c:otherwise>
		                                            </c:choose>
		                                            <c:choose>
		                                                <c:when test="${wednesday==true}">
		                                                    <input type="checkbox" name="days" value="WE"  class="roles" id="days" checked="checked" /><label for="WE"> Wednesday</label><br>
		                                                </c:when>
		                                                <c:otherwise>
		                                                     <input type="checkbox" name="days" value="WE" class="roles" id="days" /><label for="WE"> Wednesday</label><br>
		                                                </c:otherwise>
		                                            </c:choose>
		                                            <c:choose>
		                                                <c:when test="${thursday==true}">
		                                                    <input type="checkbox" name="days" value="TH" id="days" class="roles"  checked="checked" /><label for="TH"> Thursday</label>
		                                                </c:when>
		                                                <c:otherwise>
		                                                    <input type="checkbox" name="days" value="TH" class="roles" id="days" /><label for="TH"> Thursday</label>
		                                                </c:otherwise>
		                                            </c:choose>
		                                            <c:choose>
		                                                <c:when test="${friday==true}">
		                                                     <input type="checkbox" name="days" value="FR" class="roles" id="days" checked="checked" /><label for="FR"> Friday</label>
		                                                </c:when>
		                                                <c:otherwise>
		                                                    <input type="checkbox"
		                                                        name="days" value="FR" class="roles" id="days" /><label for="FR"> Friday</label>
		                                                </c:otherwise>
		                                            </c:choose>
		                                            <c:choose>
		                                                <c:when test="${saturday==true}">
		                                                     <input type="checkbox" name="days" class="roles" value="SA" id="days" checked="checked" /><label for="SA"> Saturday</label>
		                                                </c:when>
		                                                <c:otherwise>
		                                                    <input type="checkbox" name="days" class="roles" value="SA" id="days" /><label for="SA"> Saturday</label>
		                                                </c:otherwise>
		                                            </c:choose>
		                                            <c:choose>
		                                                <c:when test="${sunday==true}">
		                                                    <input type="checkbox" name="days" class="roles" value="SU" id="days" checked="checked" /><label for="SU"> Sunday</label>
		                                                </c:when>
		                                                <c:otherwise>
		                                                    <input type="checkbox" name="days" class="roles" value="SU" id="days" /><label for="SU"> Sunday</label>
		                                                </c:otherwise>
		                                            </c:choose>
		                	</div></div></div></div></div>
		                </div>
							<div class="panel-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Starts at</label>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"> <i class="fa fa-clock-o"></i></span> 
											<input id="sTime" type="text" data-plugin-timepicker class="time" data-show-meridian="false" name="sTime" value="${sTime}">
							</div></div></div></div>
							<div class="panel-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Duration</label>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"> <i class="fa fa-clock-o"></i></span> 
											<select name="duration" class="add">
							                    	<option value="0">Time to add</option>
							                    	<option value="30">30 min</option>
							                 		<option value="60">60 min</option>
							                 		<option value="90">90 min</option>
							                 		<option value="120">2hr</option>
							                 		<option value="180">3hr</option>
							                 		<option value="240">4hr</option>
							                 		<option value="300">5hr</option>
							                 		<option value="360">6hr</option>
							                 		<option value="420">7hr</option>
							                 		<option value="480">8hr</option>
							                 		<option value="540">9hr</option>
							                </select> 
							</div></div></div></div>
							<div class="panel-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Ends at</label>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"> <i class="fa fa-clock-o"></i></span>
											<input id="endTime" type="text" data-plugin-timepicker class="endTime" data-show-meridian="false" name="eTime" value="${eTime}">
							</div></div></div></div>
					</div>						
				</div><!--  End Event -->
		
			</div>
			<br>
			
			<!-- BANNER -- IMAGE UPLOAD -->
			<div id="dropzoneBanner" class="panel-body">
				<div class="control-group">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="imageUrl">Banner Image</label>	
						<div class="col-sm-9">
							<div class="input-group mb-md"> 
								<input id="bannerImage" name="imageUrl" type="text" class="form-control"/>
								<span class="input-group-btn">
									<a class="iframeBanner btn btn-success" href="<c:url value='/admin/medialibrary/imagesBanners'/>">
									<abbr title="Click the button if want a banner from the media library">Library</abbr></a></span></div></div></div>
					<!-- Upload files for Banner -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Banner </label>
						<div class="col-sm-9">
							<input id="fileuploadBanner" type="file" name="filesBanner[]" data-url="<c:url value="../../../file/uploadBanners?"/>">
							Drop file here
							<div id="progress" class="progress">
								<div class="progress-bar progress-bar-primary" role="progressbar"  style="width: 0%;">Complete</div></div>
							<div id="displayBanner"></div></div></div>
					<!-- BANNER -- FILE FILEUPLOAD TABLE-->
					<div class="form-group">
						<label class="col-sm-3 control-label">Banners</label>
						<div class="col-sm-9">				
							<div style="width:200px; padding: 20px">
								<table id="uploaded-files-banner"  class="table">
									<tr class="currB">
										<th>Banner Title</th>
										<th>Banner SubTitle</th>
										<th>Banner Image</th>
										<th>Option Remove</th>
									</tr>
									<c:forEach var="banner" items="${bannerList}" varStatus="row">
										<tr class="currB">
											<td><input value="${banner.title}" name="titleBanner[]"/></td>
											<td><input value="${banner.subtitle}" name="subtitleBanner[]"/></td>
											<td><input type="hidden" value="${banner.id}" name="bannerId[]"/>
												<input type="hidden" value="${banner.imageUrl}" name="bannerUrl[]"/>
												<input type="hidden" value="0" name="bannerRemove[]"/>
												<img src="<c:url value="${banner.imageUrl}"/>" alt="TestDisplay" width="30" height="20" name="urlBanner"/></td>
											<td><input type="button" class="remo btn-xs btn-danger" value="Remove"/></td>
										</tr>
									</c:forEach>
								</table>
							</div></div></div></div></div>				
				
			<!-- FILE FILE LOAD -->
			<div id="dropzoneFile" class="panel-body">
				<div class="control-group">
					<div class="form-group">
						<label class="col-sm-3 control-label">PDF File</label>
						<div class="col-sm-9">
							<div class="input-group mb-md"> 
								<input id="eventFile" type="text" name="eventFile" class="form-control" />
								<span class="input-group-btn">
									<a class="iframeFile btn btn-success" href="<c:url value='/admin/medialibrary/evFiles'/>">
										<abbr title="Click the button if want a PDF from the media library">Library</abbr></a></span>
					</div></div></div>
					<!-- Upload files -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload PDF File</label>
						<div class="col-sm-9">
							<input id="fileuploadFile" type="file" name="filesFile[]" data-url="<c:url value="../../../file/uploadFiles?"/>">
							Drop file here
							<div id="progressFile" class="progress">
								<div class="progress-bar progress-bar-primaryFile" role="progressbar"  style="width: 0%;">Complete</div></div></div></div>
					<!-- FILES -- FILE FILEUPLOAD -->
					<div class="form-group">
						<label class="col-sm-3 control-label">PDF Files</label>
						<div class="col-sm-9">				
							<div style="width:200px; padding: 20px">
								<table id="uploaded-files-eventFile" class="table">
									<tr class="currF">
										<th>File Name</th>
										<th>File Description</th>
										<th>File Image</th>
										<th>Option Remove</th>
									</tr>
									<c:forEach var="file" items="${filesList}" varStatus="row">
										<tr class="currF">
											<td><input value="${file.title}" name="titleFile[]"/></td>
											<td><input value="${file.description}" name="descriptionFile[]"/></td>
											<td><input type="hidden" value="${file.id}" name="fileId[]"/>
												<input type="hidden" value="${file.url}" name="fileUrl[]"/>
												<input type="hidden" value="0" name="fileRemove[]"/>
												<img src="<c:url value="${file.iconUrl}"/>" alt="TestDisplay" 
														 width="20" height="20" name="iconUrlFile"/></td>
											<td><input type="button" class="remo btn-xs btn-danger" value="Remove"/></td>
										</tr>
									</c:forEach>
								</table></div></div></div></div></div>
				
			<!-- VIDEO FILE LOAD -->
			<div id="dropzonevideo" class="panel-body">
				<div class="control-group">
					<div class="form-group">
						<label class="col-sm-3 control-label">Add Video</label>
						<div class="col-sm-9">
							<div class="input-group mb-md"> 
								<input id="eventvideo" type="text" name="eventVideo" class="form-control">
								<span class="input-group-btn">
									<a class="iframeVideo btn btn-success" href="<c:url value='/admin/medialibrary/evVideos'/>">
										<abbr title="Click the button if want a video from the media library">Library</abbr>
									</a>
								</span>
							</div>
						</div>
					</div>
					<!-- Upload files -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Upload Video</label>
						<div class="col-sm-9">
							<input id="fileuploadvideo" type=file name="filesVideo[]" data-url="<c:url value="../../../file/uploadVideos?"/>">
							Drop file here
							<div id="progressvideo" class="progress">
								<div class="progress-bar progress-bar-primaryVideo" role="progressbar"  style="width: 0%;">Complete</div>
							</div>
							<div id="displayVideo"></div>
					</div></div>
					<!-- VIDEO -- FILE FILEUPLOAD -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Videos</label>
						<div class="col-sm-9">				
							<div style="width:200px; padding: 20px">
								<table id="uploaded-files-eventvideo"  class="table">
									<tr class="currV">
										<th>Video Name</th>
										<th>Video Description</th>
										<th>Video Image</th>
										<th>Option Remove</th>
									</tr>
									<c:forEach var="video" items="${videosList}" varStatus="row">
										<tr class="currV">
											<td><input value="${video.title}" name="titleVideo[]"/></td>
											<td><input value="${video.description}" name="descriptionVideo[]"/></td>
											<td><input type="hidden" value="${video.id}" name="videoId[]"/>
												<input type="hidden" value="${video.url}" name="videoUrl[]"/>
												<input type="hidden" value="${video.iconUrl}" name="videoIconUrl[]"/>
												<input type="hidden" value="0" name="videoRemove[]"/>
												<img src="<c:url value="${video.iconUrl}"/>" alt="TestDisplay" width="32" height="25"/></td>
											<td><input type="button" class="remo btn-xs btn-danger" value="Remove"/></td>
										</tr>
									</c:forEach>
								</table>
			</div></div></div></div></div>
			
			<!-- WEB LINK ADD -->
			<div class="form-group">
				<label class="col-sm-3 control-label">Add Weblinks</label>
				<div class="col-sm-9">				
					<table id="weblinks"  class="table">
						<thead>
							<tr class="currWL">
								<th class="col-md-2">Weblink Name</th>
								<th class="col-md-3">Weblink Description</th>
								<th class="col-md-6">Weblink Address</th>
								<th class="col-md-1">Options</th>
							</tr>
						</thead>
						<tbody>
				          	<c:forEach var="wl" items="${weblinksList}" varStatus="row">
								<tr class="currWL">
									<td class="col-md-2"><input id="titleWL" class="titleWL col-md-12" value="${wl.title}" 		 name="titleWL[]"/>
														 <input type="hidden" value="${wl.id}" 									 name="idWL[]"/></td>
									<td class="col-md-3"><input id="descrWL" class="descrWL col-md-12" value="${wl.description}" name="descriptionWL[]"/></td>
									<td class="col-md-6"><input id="addrsWL" class="addrsWL col-md-12" value="${wl.url}" 		 name="urlWL[]"/></td>
									<td class="col-md-1"><a id="addWL"><i class="fa fa-plus-circle fa-lg text-success"></i></a>
				                	 					 <a class="removeItem"><i class="fa fa-minus-circle fa-lg text-danger"></i></a>
										 </td>
								</tr>
							</c:forEach>
							<tr>
								<td class="col-md-2"><input id="titleWL" class="titleWL  col-md-12" value="${wl.title}" 	   name="titleWL[]"/>
													 <input type="hidden" value="" 											   name="idWL[]"/></td>
								<td class="col-md-3"><input id="descrWL" class="descrWL  col-md-12" value="${wl.description}"  name="descriptionWL[]"/></td>
								<td class="col-md-6"><input id="addrsWL" class="addrsWL  col-md-12" value="${wl.url}" 	 	   name="urlWL[]"/></td>
								<td class="col-md-1"><a id="addWL"><i class="fa fa-plus-circle fa-lg text-success"></i></a>
			                	 	 				 <a class="removeWL"><i class="fa fa-minus-circle fa-lg text-danger"></i></a>
								</td>
				            </tr>
				    	</tbody>
					</table>
			</div></div>
					
			<!-- Button (Double) -->
			<footer class="panel-footer">
			<br>
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<a class="btn btn-default" href="<c:url value='/admin/events'/>">Cancel</a>
						<button type="submit" id="Save" name="submit" class="btn btn-success">save</button>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">	
						<input type="checkbox" name="another" value="true"/><label> Create another event?</label>
					</div>
				</div>
			</footer>
		</div>		
		</fieldset>
		</form:form>
		
		
		
			<!-- fileupload scripts -->
		<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
		<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadIconForEvent.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadImgsForBanners.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadFilesForEvents.js'/>"></script>
		<script src="<c:url value='/scripts/myUploadVideosForEvents.js'/>"></script>
	
		<!-- Specific Page Vendor -->
		<script src="<c:url value='/assets/vendor/jquery-maskedinput/jquery.maskedinput.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js'/>"></script>
	
		<script src="<c:url value='/assets/vendor/codemirror/mode/css/css.js'/>"></script>
		<script src="<c:url value='/assets/vendor/summernote/summernote.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js'/>"></script>
		<script src="<c:url value='/assets/vendor/ios7-switch/ios7-switch.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/assets/vendor/jquery-validation/jquery.validate.js'/>"></script>
		
	
		
		<script>(function() {
			'use strict';

		 	/* function isValidContentBody() {
				var s = $('#ContentBody').val();
  				s = s.replace(/[\u00B8]/g, ",");
			    s = s.replace(/[\u201E|\u201C|\u201D|\u00AB|\u00BB]/g, '"');
				s = s.replace(/[\u02C6|\u2018|\u2019|\u201A|\u00B4|\u00B7|\u02C8|\u02CA|\u02CB|\u02B9|\u02BB\u02C1|\u0300|\u0301]/g, "'");
				s = s.replace(/[\u030D|\u0312\u0315|\u031B|\u0340\u0343|\u0374|\u2032|\u2035|\u0091|\u0092|\u]/g, "'");
			    s = s.replace(/[\u2013|\u2014]/g, "-");
				s = s.replace(/[\u2026]/g, "...");
				s = s.replace(/[^A-Za-z-_0-9!@#$%?^&*.,+=;:\[\]\\\\\/|`~()><��\n\s]/g, 00000);
				s = s.replace(/[\u201C|\u201D|\u201E]/g, "\"");
				s = s.replace(/\u02C6/g, "^");
				s = s.replace(/\u203A/g, ">");
				s = s.replace(/[\u02DC|\u00A0]/g, " ");
			    console.log(s);
				return true;
			};		
			
			jQuery.validator.addMethod("patternBody", function(value, element) {
			    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;:\[\]\\\\/|`~()><\n\s]+)$/.test(value);
				return isValidContentBody();
				    }, "Your data contain invalid body characters");
		
			jQuery.validator.addMethod("patternTitle", function(value, element) {
			    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;:\[\]\\\\/|`~()><\n\s]+)$/.test(value);
				return isValidContentTitle();
				    }, "Your data contain invalid characters");
			
			function isValidContentTitle() {
					var s = $('#ContentTitle').val();
					s = s.replace(/[\u00B8]/g, ",");
				    s = s.replace(/[\u201E|\u201C|\u201D|\u00AB|\u00BB]/g, '"');
					s = s.replace(/[\u02C6|\u2018|\u2019|\u201A|\u00B4|\u00B7]/g, "'");
				    s = s.replace(/[\u2013|\u2014]/g, "-");
					s = s.replace(/[\u2026]/g, "...");
					s = s.replace(/[^A-Za-z-_0-9!@#$%?^&*.,+=;:\[\]\\\\\/|`~()><��\n\s]/g, "");
					s = s.replace(/[\u201C|\u201D|\u201E]/g, "\"");
					s = s.replace(/\u02C6/g, "^");
					s = s.replace(/\u203A/g, ">");
					s = s.replace(/[\u02DC|\u00A0]/g, " ");
				    console.log(s);
					      return true;
				};	 */	
			
			jQuery.validator.addMethod(
					"urlImage",
					function(value, element) {
						return this.optional(element)
								|| /^https?:\/\/[^\s]+(?=.(jpe?g|png|gif)).\1$/
										.test(value);
					}, "Invalid path to png, jpg or gif image");
			
			
			
			jQuery.validator
					.addMethod(
							"contact",
							function() {
								return $('#mobilephone').val() || $('#landline').val();}, 
					"Enter at least one contact number");
			

			jQuery.validator.addMethod("postcode",
					function(value, element) {
						return isValidPostCode();
					}, "Invalid postcode, enter a 4-digit number.");

			function isValidPostCode() {

				var pLength = $('#pc').val().length;
				var postcodeVal = $('#pc').val();
				console.log(postcodeVal);
				console.log(pLength);
				if (pLength > 3 && $.isNumeric(postcodeVal))
					return true;
			};
			
			$.validator.addMethod('username',
                     function(value, element) {
					return myValidator(value);}
                         , "Email address already in use. Please use other email.");
			
			function myValidator(value) {
				   var isSuccess = false;

				   $.ajax({ url: '${pageContext.request.contextPath}/checkUniqueMail?email='+ encodeURIComponent(value).replace(/%20/g,'+'), 
				            data: {}, 
				            async: false, 
				            success: 
				                function(msg) { isSuccess = msg === "true" ? true : false }
				          });
				    return isSuccess;
				}

			
			//$.validator.addMethod("checkbox", function(value, elem, param) {
			//    if($(".roles:checkbox:checked").length > 0){
			//       return true;
			//   }else {
			//       return false;
			//   }
			//},"Select at least one day of the week.</br>");
			

			$.validator.addMethod("submit", function(value, elem, param) {
			    if($(".agree:checkbox:checked").length > 0){
			       return true;
			   }else {
			       return false;
			   }
			},"Please agree with our Termes of Service");
			
			
			jQuery.validator.classRuleSettings.urlImage = { urlImage: true };
			
			//Basic
			$("#form")
					.validate({
				            highlight : function(label) {
									$(label).closest('.form-group')
											.addClass('has-error');
								},
								success : function(label) {
									console.log("validation starts");
									$(label).closest('.form-group')
											.removeClass('has-error');
									label.remove();
								},
								errorPlacement : function(error, element) {
									var placement = element
											.closest('.input');
									if (!placement.get(0)) {
										placement = element;
									}
									if (error.text() !== '') {
										console.log(element.input);
										placement.before(error);
									}
								}
							});

		}).apply(this, [ jQuery ]);
		</script>
		
		
		<script>
		$ (document).ready(function(){
			$('table[id="uploaded-files-banner"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.currB").remove();
			});
			
			$('table[id="uploaded-files-eventFile"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.currF").remove();
			});
			
			$('table[id="uploaded-files-eventVideo"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.currV").remove();
			});

			$('table[id="weblinks"]').on("click", 'a.removeItem',  function(event){
				$(event.target).closest("tr.currWL").remove();
			});
			
			// COLORBOX - IFRAME
			$(".iframeIcon").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var bg = $.colorbox.element().data('bg');
		            $('#cboxLoadedContent').css('backgroundColor', bg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myIcon;
					$('#theIcon').val(test);
					$("#displayIcon").empty();
		       	 	$("#displayIcon").append("<img id='theIcon' width='100' height='100' src='"+test+"'/>");
				}
			});
			$(".iframeBanner").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var otherbg = $.colorbox.element().data('otherbg');
		            $('#cboxLoadedContent').css('backgroundColor', otherbg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myBanner;
					$('#bannerImage').val(test);
					$("#displayBanner").empty();
		       	 	$("#displayBanner").append("<img id='bannerImage' src='"+test+"' width='700px' height='350px'/>");
		       	 	$("#uploaded-files-banner").append($('<tr class="newB"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="subtitleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="bannerUrl[]" value="'+test+'"/>'+
													'<img src="'+test+'" width="30px" height="20px"/>'))
	                		.append($('<td/>').html('<input type="button" class="remo btn-xs btn-danger" value="Remove"/>'))
	                		);
				}
			});
			$('table[id="uploaded-files-banner"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newB").remove();
			});
			
			$(".iframeFile").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var videobg = $.colorbox.element().data('videobg');
		            $('#cboxLoadedContent').css('backgroundColor', videobg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var fileId = localStorage.myFileId;
					var fileIconUrl = localStorage.myFileIconUrl;
					var fileUrl = localStorage.myFileUrl;
					
		       	 	$("#displayFile").append("<img id='fileFile' src='"+fileIconUrl+"'/>");
		       	 	$("#uploaded-files-eventFile").append(
	                		$('<tr class="newF"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleFile[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="descriptionFile[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="fileId[]"  value="'+fileId+'"/>'+
	    	                						'<input type="hidden" name="fileUrl[]" value="'+fileUrl+'"/>'+
	    	                						'<input type="hidden" name="fileIconUrl[]" value="'+fileIconUrl+'"/>'+
													'<img src="'+fileIconUrl+'" width="32" height="25"/>'))
	                		.append($('<td/>').html('<input type="button" class="remoFi btn-xs btn-danger" value="Remove"/>'))
	                		);
				}
			});
			$('table[id="uploaded-files-eventFile"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newF").remove();
			});
			
			$(".iframeVideo").colorbox({
				iframe:true, 
				width:"80%", 
				height:"80%",
		        onComplete: function(){
		            var videobg = $.colorbox.element().data('videobg');
		            $('#cboxLoadedContent').css('backgroundColor', videobg);
		            var href = this.href;
		        },
				onCleanup: function() {
					var test = localStorage.myVideo;
					$('#fileVideo').val(test);
					var vidId = localStorage.myVideoId;
					var vidUrl = localStorage.myVideoUrl;
					
					$("#displayVideo").empty();
		       	 	$("#displayVideo").append("<img id='fileVideo' src='"+test+"'/>");
		       	 	$("#uploaded-files-eventvideo").append(
	                		$('<tr class="newV"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleVideo[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="descriptionVideo[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="videoId[]" value="'+vidId+'"/>'+
	    	                						'<input type="hidden" name="videoUrl[]" value="'+vidUrl+'"/>'+
	    	                						'<input type="hidden" name="videoIconUrl[]" value="'+test+'"/>'+
													'<img src="'+test+'" width="32" height="25"/>'))
	                		.append($('<td/>').html('<input type="button" class="remoVi btn-xs btn-danger" value="Remove"/>'))
	                		);
				}
			});
			$('table[id="uploaded-files-eventvideo"]').on("click", 'input[value="Remove"]',  function(event){
				$(event.target).closest("tr.newV").remove();
			});
			
			
			$("#radioSingle").click(function(){
				$("#hideSingle").show();
				$("#hidePattern").hide();
			});
			$("#radioPattern").click(function(){
				$("#hideSingle").hide();
				$("#hidePattern").show();
			});
			$("#weekly").click(function(){
				$("#hideWeekly").show();
				$("#hideMOnthly").hide();
			});	
			$("#monthly").click(function(){
				$("#hideWeekly").hide();
				$("#hideMonthly").show();
			});	
			var clonedInput = $('#inputx1').clone();
		    $('#btnAdd').click(function() {
		           var num     = $('.clonedInput').length; 
		           var newNum  = new Number(num + 1);      
		           var newElem = clonedInput.clone().attr('id', 'inputx' + newNum);
		           newElem.children(':first').attr('id', 'name' + newNum).attr('name', 'name' + newNum);
		           $('#inputx' + num).after(newElem);
		           $('#btnDele').attr('disabled',false);
		           if (newNum == 10)
		             $('#btnAdd').attr('disabled',false);
		    });
		    $('#btnDele').click(function() {
		           var num = $('.clonedInput').length;
		           $('#inputx' + num).remove();    
		           $('#btnAdd').attr('disabled',false);
		           if (num-1 == 1)
		               $('#btnDele').attr('disabled',true);
		    });
		    $('#btnDele').attr('disabled',true);

		    $('#submit').click(function(){
		        var radioExample=$('#radioExample').val();
		        if ($("#radioExample:checked").length == 0){
		            $('#dis').slideDown().html('<span id="error">Please select a periodicity</span>');
		            return false;
		        }
		    });
		    
		    function addZero(i) { if (i < 10) {i = "0" + i;} return i;} 
		    $('select').change(function(){
		        var time =  $('#sTime').val();
		        var str = time.split(":");
		        var hour = str[0];
		        var minutes = str[1];
		        var duration = 0;
		        
		        $('select.add').each(function(){
		            duration += parseInt($(this).val()) ;
		        });
		        var hoursToAdd = parseInt((duration+ parseInt(minutes)) / 60) ; 
		        var minutesToAdd = (duration+ parseInt(minutes)) % 60;
		        
		        var finalHours = addZero(hoursToAdd + parseInt(hour));
		    	var finalMins = addZero(minutesToAdd);
		        $('#endTime').val(finalHours + ":" + finalMins);
		    });
		});
		</script>
		
		<script>
      	$(function () {
      		var id = 1;
          	$(document).on('click', '#addnewitem', function () {
      		var currentTable = $(this).closest('table').attr('id');
          	id++;
      		$('#'+ currentTable ).append(
      				'<tr>'+
      				'<td>'+
      					'<input id="eciId" type="hidden" name="eciId" value="-1">'+
						'<input id="eciHref" type="hidden" name="eciHref" value="-1">'+
	                	'<input id="datepicker" class="start_date" name="sDate" value="" placeholder="yyyy/mm/dd">'+
	                	'<input id="days" type="hidden" name="days" value=""></div></td>'+
	                '<td>'+
	                    '<input id="stime" type="text" class="start_time" placeholder="Start Time" name="sTime" value=""></div></td>'+
	                '<td>'+
	                	'<select name="duration" class="duration_Add">'+
	                    	'<option value="0">Time to add</option>'+
	                    	'<option value="30">30 min</option>'+
	                 		'<option value="60">60 min</option>'+
	                 		'<option value="90">90 min</option>'+
	                 		'<option value="120">2hr</option>'+
	                 		'<option value="180">3hr</option>'+
	                 		'<option value="240">4hr</option>'+
	                 		'<option value="300">5hr</option>'+
	                 		'<option value="360">6hr</option>'+
	                 		'<option value="420">7hr</option>'+
	                 		'<option value="480">8hr</option>'+
	                 		'<option value="540">9hr</option>'+
	                 	'</select></td>'+
                 	'<td>'+
                		'<input id="datepicker" class="end_date" name="eDate" value="" placeholder="yyyy/mm/dd"></td>'+
	                '<td>'+
	                	'<input id="etime" type="text" class="end_Time" placeholder="End Time" name="eTime" value="">'+
	                	'<input id="dateType" type="hidden" name="dateType" value="single"/></td>'+
	                '<td><a id="addnewitem"><i class="fa fa-plus-circle fa-lg text-success"></i></a>'+
	                	'<a class="removeItem"><i class="fa fa-minus-circle fa-lg text-danger"></i></a></td></tr>');
      		$('.start_date').datepicker({dateFormat: "yy/mm/dd"});
    	    $('.end_date').datepicker(  {dateFormat: "yy/mm/dd"});
    	    $('.start_time').timepicker({showMeridian: false, showSeconds: false });
    	    $('.end_time').timepicker({  showMeridian: false, showSeconds: false });
		    $('.duration_Add').on('change', onChangeCallback);
          	});
              
		    $(document).on('click', '.removeItem', function () {
		            var currentTable = $(this).closest('table').attr('id');
			    	$(this).closest('tr').remove();
		    });
		    function addZero(i) { if (i < 10) {i = "0" + i;} return i;}   
	        var onChangeCallback = function () { 
		    	        var currentTable = $(this).closest('table').attr('id');
		    	        var datetoAdd = new Date();
	                    var theTime = $(this).parents('tr').find('.start_time').val();
	                    var str = theTime.split(":");
					    var hour = str[0];
					    var minute = str[1];
	                    var durtoAdd = $(this).parents('tr').find('.duration_Add').val();
	                    var hoursAdd = parseInt( (parseInt(durtoAdd) + parseInt(minute)) / 60 );
	                    var minsAdd = (parseInt(durtoAdd) + parseInt(minute))%60;
	                    var finalHours = addZero(hoursAdd + parseInt(hour));
	                    var finalMins = addZero(minsAdd);
	                    $(this).parents('tr').find('.end_Time').val(finalHours+":"+ finalMins);
	                    $('#eTime').val(finalHours+":"+ finalMins);
		    	 	};
		    $('.duration_Add').on('change', onChangeCallback);
    	 	
      	});
		       
	    $('.start_date').datepicker({dateFormat: "yy/mm/dd"});
	    $('.end_date').datepicker(  {dateFormat: "yy/mm/dd"});
	    $('.start_time').timepicker({showMeridian: false, showSeconds: false });
	    $('.end_time').timepicker({  showMeridian: false, showSeconds: false });

	    $(function () {
      		var id = 1;
          	$(document).on('click', '#addWL', function () {
      		var currentTable = $(this).closest('table').attr('id');
          	id++;
      		$('#'+ currentTable ).append(
      				'<tr>'+
      				'<td class="col-md-2"><input id="titleWL" class="titleWL  col-md-12" value="${wl.title}" 	   name="titleWL[]"/>'+
      				'<input type="hidden" value="" name="idWL[]"/></td>'+
					'<td class="col-md-3"><input id="descrWL" class="descrWL  col-md-12" value="${wl.description}" name="descriptionWL[]"/></td>'+
					'<td class="col-md-6"><input id="addrsWL" class="addrsWL  col-md-12" value="${wl.url}" 	 	   name="urlWL[]"/></td>'+
					'<td class="col-md-1">'+
	                	'<a id="addWL"><i class="fa fa-plus-circle fa-lg text-success"></i></a>'+
	                	'<a class="removeWL"><i class="fa fa-minus-circle fa-lg text-danger"></i></a></td></tr>');
          	});
              
		    $(document).on('click', '.removeWL', function () {
		            var currentTable = $(this).closest('table').attr('id');
			    	$(this).closest('tr').remove();
		    });
      	});
        
      </script>
      
	  
	
	</section>
</body>
</html>