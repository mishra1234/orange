<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html>
<body>
<style type="text/css">
button
{
background-color:#f78d3e;
width:80px;
height:30px;
}
a
{
text-decoration:  none;
color:black;
}
a hover
{
 text-decoration:  none;
}
</style>
<div class="text-center">
      <button type="submit" ><a style="text-decoration:none; color:black;" class="view-more" href="<c:url value='/admin/event/editEvent/${ev.id}'/>">Edit Event</a> </button>
	  <button type="submit" ><a style="text-decoration:none; color:black;" class="view-more" href="<c:url value='/admin/event/addBanners/${ev.id}'/>">Add Banners</a></button>
	  <button type="submit" ><a style="text-decoration:none; color:black;" class="view-more" href="<c:url value='/admin/event/promote/${ev.id}'/>">Promote Event</a></button>
	  <button type="submit" ><a style="text-decoration:none; color:black;" class="view-more" href="<c:url value='/admin/booking/bookings/${ev.id}/${commId}'/>">Bookings</a></button>
	  <button type="submit" ><a style="text-decoration:none; color:black;" class="view-more" href="<c:url value='/admin/booking/schedules/${ev.id}/${commId}'/>">Schedules</a></button>
	  <button type="submit" ><a style="text-decoration:none; color:black;" class="view-more" href="<c:url value='/admin/recommended/${ev.id}/${commId}'/>">Recommended Events</a></button>
	  
	  <!-- <button type="submit" ><a style="text-decoration:none; color:black;" class="view-more" href="<c:url value='/admin/booking/${ev.id}/${commId}'/>">Book event</a></button> -->
 </div>
<fieldset>

<br>
<h3 style="margin-left:100px;"><strong>Activity: </strong> ${ev.contentTitle}</h3>
<br>
<div class="slider">
	<ul class="bxslider">
		<c:forEach var="bann" items="${listOfBanners}" varStatus="row">
	  			<li>
	  				<img src="${bann.imageUrl}" title="${bann.subtitle}">
	  				<div class="text-section">
                    	<p class="subtitle"><h6>${bann.title}</h6></p>
                    	<br><br>
                </div>
	  			</li>
	  	</c:forEach>
	</ul>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.bxslider').bxSlider({
		  mode: 'fade',
		  captions: true,
		  auto: true,
		  slideWidth: 700
		});
		(function($){
			'use strict';
			var initBasicWithMarkers = function() {
				var map = new GMaps({
					div: '#gmap-basic-marker',
					lat: ${avLat},
					lng: ${avLng},
					markers: [{
						lat: ${avLat},
						lng: ${avLng},
						infoWindow: {
							content: '<p>Basic</p>'
						}
					}]
				});
				map.addMarker({
					lat: ${avLat},
					lng: ${avLng},
					infoWindow: {
						content: '<p>${ev.contentTitle}</p>'
					}
				});
			};
			// auto initialize
			$(function() {
				initBasicWithMarkers();
			});
		}).apply(this, [ jQuery ]);
	});
</script>

<link rel="stylesheet" href="http://bxslider.com/lib/jquery.bxslider.css" type="text/css" />
<link rel="stylesheet" href="http://bxslider.com/css/github.css" type="text/css" />
<script src="http://bxslider.com/js/jquery.min.js"></script>
<script src="http://bxslider.com/lib/jquery.bxslider.js"></script>
<script src="http://bxslider.com/js/rainbow.min.js"></script>
<script src="http://bxslider.com/js/scripts.js"></script>
	
<style type="text/css">
.khush
{
 float:right;
    margin-top:55%;;
}
table{
   width: 100% ;
}
td
{
height: 40px;
font-size:15px;
}
</style>
<table id="eventTable">
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Event Details:</FONT></td>		<td>${ev.contentBody}</td>
	<tr><td></td>					<td></td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Contact Name:</FONT></td>		<td>${ev.event.contactName}</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Contact Email:</FONT></td>		<td>${ev.event.contactEmail}</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Organisation:</FONT></td>		<td>${ev.event.organisation}</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Cost:</FONT></td>				<td>${ev.event.cost}</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>DateTime:</FONT></td>			<td>${ev.startDate} (${ev.rangeTime})</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>bookable:</FONT></td>			<td>${ev.event.bookable}</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>contactPhone:</FONT></td>		<td>${ev.event.contactPhone}</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Recommended?:</FONT></td>		<td><c:choose>
																					  <c:when test="${ev.recommended}">Yes</c:when>
																					  <c:otherwise>No</c:otherwise>
																					</c:choose></td>	
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Icon:</FONT></td>				<td><img src="${ev.iconUrl}" width="100" height="100" 
						 					 										onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></td>
	<tr><td></td>																<td></td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Address:</FONT></td>			<td>${ev.event.address}</td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Files:</FONT></td>			<td>
																					<c:forEach var="file" items="${listOfFiles}" varStatus="row">
																						<a href="${file.url}"> ${file.title}</a>
																					</c:forEach></td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Videos:</FONT></td>			<td>
																					<c:forEach var="video" items="${listOfVideos}" varStatus="row">
																						<video src="${video.url}" controls style="width:240px;height:120px;"></video>
																					</c:forEach></td>
	<tr><td><FONT COLOR=black FACE="Arial" SIZE=3>Web-Links:</FONT></td>		<td>
																					<c:forEach var="wls" items="${listOfWeblinks}" varStatus="row"> 	
																						<a href="${wls.url}"> ${wls.title}</a>
																					</c:forEach>
																				</td>						 					 								
</table>
<br>

<div class="body">
		<label class="col-sm-1 control-label" style="font-size:15px; color:black" for="iconUrl">Map:  </label>
		<div class="col-md-9">
			<section class="panel">
				<div class="panel-body">
					<div id="gmap-basic-marker" style="height: 500px; width: 500px;"></div>
				</div>
			</section>
		</div>
</div>
			
	
<style type="text/css">
button
{
background-color:#f78d3e;
width:160px;
height:30px;
}
#longButton
{
background-color:#f78d3e;
width:120px;
height:30px;
}
a
{
text-decoration:  none;
color:black;
}
a hover
{
 text-decoration:  none;
}
</style>
</fieldset>
</body>

</html>