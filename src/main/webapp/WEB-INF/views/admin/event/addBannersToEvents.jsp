<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<body>
<c:url var="var" value='/admin/event/saveAbanners/'/>
<form:form modelAttribute="act" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<form:input type="hidden" id="id" name="act" path="id" value=""/>

<h4>Please be noticed that just five (or less) banners can be selected!!</h4> 
<legend>Add/Remove Banners to the Event: "${act.contentTitle}"</legend>

<!-- Select Multiple -->
<div class="control-group">
  <label  style="display:block; margin-bottom:10px; margin-left:35px"></label>
 	<ul class="checkbox-grid" style="display:block" id="world">
 	<c:forEach items="${listOfEvents}" var="parent">
 		<c:set var="x" value="false"/> 
    	<c:forEach items="${act.banners}" var="selected">
    		<c:choose>
	    		<c:when test="${selected.id == parent.id}">
	    			<c:set var="x" value="true"/> 
	    		</c:when>
    		</c:choose>
    	</c:forEach>
    	<c:choose>
	    	<c:when test="${x}">
    		  <li><input id="parent" type="checkbox" name="xevents" value="${parent.id}" checked="checked"/>
    			  <label for="text1"><img src="${parent.imageUrl}" width="420" height="220"> ${parent.title} </label></li>
	    	</c:when>
		    <c:otherwise>
			  <li><input type="checkbox" name="xevents" value="${parent.id}"/>
				  <label for="text1"><img src="${parent.imageUrl}" width="420" height="220"> ${parent.title} </label></li>
		    </c:otherwise>
		</c:choose>
    </c:forEach>
   
    </ul>
</div>
  

<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
    <a class="btn btn-medium btn-cancel" href="<c:url value='/admin/communities'/>">Cancel</a>
    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
  </div>
</div>


<script type="text/javascript">
jQuery(function(){
    var max = 5;
    var checkboxes = $('input[type="checkbox"]');
                       
    checkboxes.change(function(){
        var current = checkboxes.filter(':checked').length;
        checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
    });
});
</script>

<link rel="stylesheet" href="http://awesome-bootstrap-checkbox.okendoken.com/demo/bootstrap.css"/>
<link rel="stylesheet" href="http://awesome-bootstrap-checkbox.okendoken.com/bower_components/Font-Awesome/css/font-awesome.css"/>
<link rel="stylesheet" href="http://awesome-bootstrap-checkbox.okendoken.com/demo/build.css"/>


</fieldset>
</form:form>
	
</body>
</html>