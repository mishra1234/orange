<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<body>
	<script>
		(function( $ ) {
			'use strict';
	
			var datatableInit = function() {
				var $table = $('#eventsTable');
	
				$table.dataTable({
					sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
					oTableTools: {
						sSwfPath: $table.data('swf-path'),
						aButtons: [
							{
								sExtends: 'pdf',
								sButtonText: 'PDF'
							},
							{
								sExtends: 'csv',
								sButtonText: 'CSV'
							},
							{
								sExtends: 'xls',
								sButtonText: 'Excel'
							},
							{
								sExtends: 'print',
								sButtonText: 'Print',
								sInfo: 'Please press CTR+P to print or ESC to quit'
							}
						]
					}, 
					"paging":false,
					"info":false,
					"order":[["acc.id","desc"]], 
					"columnDefs":[{ "visible": false, "targets": 0 }],
				});
	
			};
	
			$(function() {
				datatableInit();
			});
		}).apply( this, [ jQuery ]);
	</script>
	
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">List of events on the community:  ${myComm.name}</h2>
		</header>
		<div class="panel-body">
		<div class="form-group">
		<div class="row">
		
			<h5>
				&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href ="<c:url value='/admin/community/addEvents/${myCommId}'/>">Add/Remove Events</a>
				<a class="btn btn-primary" href ="<c:url value='/admin/community/copyEvents/${myCommId}'/>">Copy Events to</a>
				<a class="btn btn-primary" href ="<c:url value='/admin/community/moveEvents/${myCommId}'/>">Move Events to</a>
			</h5>
			</div>
			</div>
			<table id="eventsTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
				<thead>
					<tr>
						<th>Icon</th>
						<th>Event Title</th>
						<th>Cost</th>
						<th>DateTime</th>
						<th>Recommended </th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ev" items="${myList}" varStatus="row">
							<tr>
								<td><a href= "<c:url value='/admin/event/${ev.id}'/>"><img src="${ev.iconUrl}" width="25" height="25" onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
								<td>${ev.contentTitle}</td>
								<td>${ev.event.cost}</td>
								<td>${ev.event.displayDatetime}</td>
								<td><c:choose>
										<c:when test="${ev.recommended}">Yes</c:when>
										<c:otherwise>No</c:otherwise>
									</c:choose></td>
					</c:forEach>
				</tbody>
			</table>
			
		</div>
		
	</section>
	
</body>

</html>
