<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>

<body>
<c:url var="var" value='/admin/event/sendPromote/${eventId}'/>
<form class="form-horizontal" id="form" method="POST" action="${var}">

<fieldset>

	<!-- Form Name -->
	<header class="panel-heading">
		<div class="panel-actions">
			<a href="#" class="fa fa-caret-down"></a> <a href="#"
				class="fa fa-times"></a>
		</div>

		<h2 class="panel-title">Promote Activity</h2>
		<p class="panel-subtitle">Please select a community to promote the activity</p>
	</header>
	

	<div class="panel-body">

	
		<div class="form-group">
			<label class="col-sm-3 control-label" for="layerId">Community to Send Mail (Users of)</label>
			<div class="col-md-9">
				<select id="layerId" name="layerId" class="form-control" multiple="multiple" size="4"> 
					<c:forEach var="layer" items="${mailLayers}"  varStatus="row">
						<option value="${layer.id}">${layer.name}"</option>
					</c:forEach>
				</select>
			</div>
		</div>
							
	</div>
				
	<br />

	<!-- Button (Double) -->
	<footer class="panel-footer">
		<div class="row">
			<div class="col-sm-9 col-sm-offset-3">
				<a class="btn btn-default"
					href="<c:url value='/admin/messages'/>">Cancel</a>
				<button type="submit" id="Save" class="btn btn-primary">Save</button>
			</div>
		</div>
	</footer>
					
</fieldset>
</form>
	
</body>
</html>