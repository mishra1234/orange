<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<body>
<div class="col-lg-12">
											<div class="chart-data-selector" id="salesSelectorWrapper">
												<h2>
													New Connections
													<strong>
														
														
													</strong>
												</h2>
												<div  id="salesSelectorItems" class="chart-data-selector-items mt-sm">
													<!-- Flot: Sales Porto Admin -->
													<div class="col-lg-12">
										
													<div id="curve_chart" data-sales-rel="Porto Drupal"></div></div>

													<!-- Flot: Sales Porto Wordpress -->
<!-- 													<div class="chart chart-sm" data-sales-rel="All" id="flotDashSales3" class="chart-hidden"></div> -->
												 <script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>
     
		
													<script>
													window.onload = function all(){
													        	var getID = JSON.stringify(${NewUsersJsonString});
													       
													        //var checkOutDate= $("#checkOutDate").val();
															console.log("Id is: " + getID);
															
														  window.onload =   google.setOnLoadCallback(drawChart(getID));
											        
        function drawChart(jsonData1) {
            console.log("Here");
           
            console.log("JSON data: " +jsonData1);
            var data = google.visualization.arrayToDataTable(JSON.parse(jsonData1));
           var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
           var options = {
                   legend: 'none',
                   colors: ['#f78d3e'],
                   pointSize: 7
                 };
               chart.draw(data, options);
              
             }
													}
													    	
       // window.onload =  google.setOnLoadCallback(drawChart(${newUserList}));
        </script>
        
												</div>
   					</div></div>
   <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					 <h1 class="page-header"> 
						 <ul class="list-inline">
							  <li><img src="${myComm.iconUrl}" class="img-thumbnail" width="50" height="50"></li>
							  <li style="color:#FFFFFF">${myComm.name}</li>
						 </ul>
					 </h1>    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
			    <div class="col-md-12 col-lg-6 col-xl-6">
					<section class="panel panel-featured-left  panel-featured-primary">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-primary">
										<i class="fa fa-users fa"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
										<h4 class="title">Connected Users</h4>
										<div class="info">
											<c:choose>
						    					<c:when test="${myComm.type == 'gated'}">
						    					  <div class="row">
														<div class="col-md-4">
															<div class="h4 text-bold mb-none mt-lg">${fn:length(GrantedUsers)}</div>
															<p class="text-xs text-muted mb-none">Granted</p>
														</div>
														<div class="col-md-4">
															<div class="h4 text-bold mb-none mt-lg">${fn:length(PendingUsers)}</div>
															<p class="text-xs text-muted mb-none">Requested</p>
														</div>
														<div class="col-md-4">
															<div class="h4 text-bold mb-none mt-lg">${fn:length(DeniedUsers)}</div>
															<p class="text-xs text-muted mb-none">Denied</p>
														</div>
												  </div>
						    					</c:when>
						    					<c:otherwise>
						    					 <strong class="amount">${fn:length(ConnectedUsers)}</strong>
						    					</c:otherwise>
										   </c:choose>
									   	</div>
									</div>
									<div class="summary-footer">
										 <a class="btn text-uppercase" href="<c:url value='/admin/accounts/${myCommId}/1'/>">(detailed view)</a>
										 <!-- 
										 <a class="text-muted text-uppercase" data-toggle="collapse" href="#usersConnected">(detailed view)</a>
										 <div id="usersConnected" class="collapse">
										 	<section class="panel">
												<div class="panel-body">
													<table class="table table-bordered table-striped table-condensed mb-none">
														<thead>
															<tr>
																<th class="text-center">#</th>
																<th class="text-center">Name</th>
																<th class="text-center">Email</th>
														<c:choose>
	   														<c:when test="${myComm.type == 'gated'}">
																<th class="text-center">Status</th>
														    </c:when>
														</c:choose>
														<th class="text-center">Revoke/Grant</th>
															</tr>
														</thead>
														<tbody>
															<c:choose>
		   														<c:when test="${myComm.type == 'gated'}">
		   														<c:set var="countU" value="0" scope="page" />
		   														<c:set var="SERVER" value="http://www.cc-me.com.au" scope="page" />
		   														    
		   														     <c:forEach var="dto" items="${grantedDTO}" varStatus="row">
											                               <c:set var="countU" value="${countU + 1}" scope="page"/>
											                             
											                                      <tr class="success">
																				<th scope="row">${countU}</th>
																				<td class="text-left">${dto.account.name}</td>
																				<td class="text-left"><em>${dto.account.email}</em></td>
																				<td class="text-left"><em>Granted</em></td>
																				<td class="text-center"><em><a style="color:#FFFFFF" target = "_blank" href='${SERVER}/token/${dto.code}/action/deny'/><i title="Revoke" class="fa fa-ban"></i></a></em>
																				</tr>
																				
																       </c:forEach>
											                      	   <c:forEach var="requestedDTO" items="${pendingDTO}" varStatus="row">
											                     	     
											                         	
											                            <c:set var="countU" value="${countU + 1}" scope="page"/>
											                   		          <tr class="active">
																				<th scope="row">${countU}</th>
																				<td class="text-left">${requestedDTO.account.name}</td>
																				<td class="text-left"><em>${requestedDTO.account.email}</em></td>
																				<td class="text-left"><em>Pending</em></td>
																				<td class="text-center"><em><a style="color:#FFFFFF" href='${SERVER}/token/${requestedDTO.grantCode}/action/grant'/><i title="Grant" class="fa fa-check-circle-o"></i></a>&nbsp;&nbsp;<a style="color:#FFFFFF" href='${SERVER}/token/${requestedDTO.denyCode}/action/deny' /><i  title="Revoke" class="fa fa-ban"></i></a></em>
																			
																				</tr>
																			
											                            </c:forEach>
											                            <c:forEach var="deny" items="${denyDTO}" varStatus="row">
											                            <c:set var="countU" value="${countU + 1}" scope="page"/>
											                         
														                        <tr class="info">
																				<th scope="row">${countU}</th>
																				<td class="text-left">${deny.account3.name}</td>
																				<td class="text-left"><em>${deny.account3.email}</em></td>
																				<td class="text-left"><em>Denied</em></td>
																				<td class="text-center"><em><a style="color:#FFFFFF" href='${SERVER}/token/${deny.code}/action/grant'><i title="Grant" class="fa fa-check-circle-o"></i></a></em>
																		
																				</tr>
											                            </c:forEach>																                            																                            
		   														</c:when>
		   														<c:otherwise>
											                            <c:forEach var="mName" items="${ConnectedUsers}" varStatus="row">
														                        <tr>
																				<th scope="row">${row.index+1}</th>
																				<td class="text-left">${mName.name}</td>
																				<td class="text-left"><em>${mName.email}</em></td>
																				</tr>
											                            </c:forEach>
											                	</c:otherwise>
					   										</c:choose>
														</tbody>
													</table>
												</div>
											</section>
											<div class="info">
												<a class="info text-uppercase invusers" href="#">
													Add Users to the Community
												</a>
											</div>
										</div>
										 -->
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>	
				<div class="col-md-12 col-lg-6 col-xl-6">
					<section class="panel panel-featured-left panel-featured-quartenary">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-quartenary">
										<i class="fa fa-calendar fa"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
										<h4 class="title">Current/Future Events</h4>
										<div class="info">
											<strong class="amount">${FutureEvents}</strong>
										</div>
									</div>
									<div class="summary-footer">
										 <a class="btn text-uppercase" href="<c:url value='/admin/events/1/${myCommId}/110010011'/>">(detailed view)</a>
										 <input type="hidden" name="commId" value="${myCommId}">
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div> <!-- end of row -->
              
            <div class="row">
			    <div class="col-md-12 col-lg-6 col-xl-6">
					<section class="panel panel-featured-left panel-featured-tertiary">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-tertiary">
										<i class="fa fa-calendar-o fa"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
										<h4 class="title">Past Events</h4>
										<div class="info">
											<strong class="amount">${PastEvents}</strong>
										</div>
									</div>
									<div class="summary-footer">
										<a class="btn text-uppercase" href="<c:url value='/admin/events/1/${myCommId}/110000011'/>">(detailed view)</a>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				
				<div class="col-md-12 col-lg-6 col-xl-6">
					<section class="panel panel-featured-left  panel-featured-tertiary">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-tertiary">
										<i class="fa fa-plus"></i>
										<i class="fa fa-calendar"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
											<h4 class="title">Create New Event</h4>
											<a class="info text-uppercase invusers" href="<c:url value='/admin/event/new/${myCommId}'/>">
												<p>Click here.</p>
											</a>
									</div>
								</div>
							</div>
						</div>
					</section>
 				</div>
			</div> <!-- end of row -->
            
            <div class="row">
			    <div class="col-md-12 col-lg-6 col-xl-6 col-centered">
					<section class="panel panel-featured-left panel-featured-tertiary">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-info">
										<i class="fa fa-info-circle fa"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
										<h4 class="title">Information Pages</h4>
										<div class="info">
											<strong class="amount">${fn:length(Informations)}</strong>
										</div>
									</div>
									<div class="summary-footer">
										<a class="btn text-uppercase" href="<c:url value='/admin/infos/${myCommId}/1'/>">(detailed view)</a>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<section class="panel panel-featured-left  panel-featured-tertiary">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-info">
										<i class="fa fa-plus"></i>
										<i class="fa fa-info-circle"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
											<h4 class="title">Create New Information Page</h4>
											<a class="info text-uppercase invusers" href="<c:url value='/admin/info/new/${myCommId}'/>">
												<p>Click here.</p>
											</a>
									</div>
								</div>
							</div>
						</div>
					</section>
 				</div>
 			</div> <!-- end of row -->
 			
 			<div class="row">
				<div class="col-md-12 col-lg-6 col-xl-6">
					<section class="panel panel-featured-left panel-featured-secondary">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-secondary">
										<i class="fa fa-support fa"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
										<h4 class="title">Surveys</h4>
										<div class="info">
											<strong class="amount">${fn:length(SurveyCount)}</strong>
										</div>
									</div>
									<div class="summary-footer">
										<a class="btn text-uppercase" href="<c:url value='/admin/surveys/${myCommId}/1'/>">(detailed view)</a>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<section class="panel panel-featured-left  panel-featured-secondary">
						<div class="panel-body">
							<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-secondary">
										<i class="fa fa-plus"></i>
										<i class="fa fa-support"></i>
									</div>
								</div>
								<div class="widget-summary-col">
									<div class="summary">
											<h4 class="title">Create New Survey</h4>
											<a class="info text-uppercase invusers" href="<c:url value='/admin/survey/new/${myCommId}'/>">
												<p>Click here.</p>
											</a>
									</div>
								</div>
							</div>
						</div>
					</section>
 				</div>
 			</div> <!-- end of row -->
				
			<script src="<c:url value='/scripts/lib/sweet-alert.min.js'/>"></script>
			<link rel="stylesheet" href="<c:url value='/scripts/lib/sweet-alert.css'/>" type="text/css" />
  			<script>
				$(document).ready(function(){
				    $(".invusers").click(function(){	
						console.log('Good Job');
						var x= $('#modalForm').modal('show');
						$(".select2-search-choice").remove();
						console.log('done invoking');
						console.log(x);
					});
				    
				    $(".inviteU").click(function(){	
						console.log('Great');
						var inviteUserDetails = $("#userMultiSelect").val();
						console.log('user invite'+inviteUserDetails);
						var communityID= $("#communityIDS").val();
						console.log('Comm id'+communityID);
						var x= $('#modalForm').modal('hide');
						var controllerurl= "<c:url value='/admin/invite/request'/>";
						var jqxhr = $.ajax({
							  type: "POST",
								url: controllerurl,
								data: JSON.stringify({ CommunityID: communityID, users: inviteUserDetails}),
								dataType: "json",
								beforeSend: function(xhr) {
									xhr.setRequestHeader("Accept", "application/json");
									xhr.setRequestHeader("Content-Type", "application/json");}
							    })
							  .always(function( response ) {
								    if ( response.responseText.indexOf("Success")>-1 ) {
								    	var z= response.responseText.split("--");
								    	swal("Connected!", z[1], "success");
								    	}
								    else{
								    	var z= response.responseText.split("--");
							    		swal("Cancelled", z[1], "error");
								    	}
							   }) ;
						console.log('done invoking');
					});

				    $(".addMgrs").click(function(){	
						var inviteUserDetails = $("#userMultiSelectMgr").val();
						var communityId= $("#communityID").val();
						var x= $('#myModal').modal('hide');
						var controllerurl= "<c:url value='/admin/addMgrs/request'/>";
						var jqxhr = $.ajax({
							  type: "POST",
								url: controllerurl,
								data: JSON.stringify({ CommunityID: communityId, users: inviteUserDetails}),
								dataType: "json",
								beforeSend: function(xhr) {
								xhr.setRequestHeader("Accept", "application/json");
								xhr.setRequestHeader("Content-Type", "application/json");
								}
							    })
							  .always(function( response ) {
								    if ( response.responseText.indexOf("Success")>-1 ) {
								    	var z= response.responseText.split("--");
								    	swal("New Account Added!", z[1], "success");
								    	}
								    else
								    	{
								    	var z= response.responseText.split("--");
							    		swal("Action Cancelled", z[1], "error");
								    	}
								  }) ;
					});
					
				    $(".delMgrs").click(function(){	
						var inviteUserDetails = $("#userMultiSelectDel").val();
						var communityId= $("#communityID").val();
						var x= $('#myModalDel').modal('hide');
						var controllerurl= "<c:url value='/admin/delMgrs/request'/>";
						var jqxhr = $.ajax({
							  type: "POST",
								url: controllerurl,
								data: JSON.stringify({ CommunityID: communityId, users: inviteUserDetails}),
								dataType: "json",
								beforeSend: function(xhr) {
								xhr.setRequestHeader("Accept", "application/json");
								xhr.setRequestHeader("Content-Type", "application/json");
								}
							    })
							  .always(function( response ) {
								    if ( response.responseText.indexOf("Success")>-1 ) {
								    	var z= response.responseText.split("--");
								    	swal("Account Removed!", z[1], "success");4
								    	}
								    else
								    	{
								    	var z= response.responseText.split("--");
							    		swal("Action Cancelled", z[1], "error");
								    	}
								  }) ;
					});
				});
			</script>		  
			
			
			<!-- Recommended Events Row --> 
			<div class="row">
				<div class="panel panel-default">
	                <div class="panel-heading">
	                    <i class="fa fa-check fa-fw"></i> Recommended Events
	                </div>
	                <!-- /.panel-heading -->
	                <div class="panel-body">
	                    <div class="list-group">
	                       <c:choose>
								<c:when test="${fn:length(recommendedActs) > 0}">
				                       <c:forEach var="mName" items="${recommendedActs}" varStatus="row">
				                           <a href="<c:url value='/admin/event/${mName.id}'/>"  class="list-group-item">
				                               <i class="fa fa-user fa-fw"></i> ${mName.contentTitle}
				                               <span class="pull-right text-muted small"><em>${mName.event.displayDatetime}, ${mName.event.cost}</em>
				                               </span>
				                           </a>
				                       </c:forEach>
							    </c:when>
								<c:otherwise>
								<p class="list-group-item"> There are no recommended activities here.</p>
								</c:otherwise>
				  		   </c:choose>
	                    </div>
	                    <!-- /.list-group -->
	                </div>
	                <!-- /.panel-body -->
                </div>
                
                <!-- Sub Communities Row --> 
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-sitemap fa-fw"></i> Sub Communities
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="list-group">
                        	<c:choose>
			   					<c:when test="${fn:length(subcommunities) > 0}">
			   							<c:forEach var="mName" items="${subcommunities}" varStatus="row">
			                                <a href="<c:url value='/admin/events/community/${mName.id}/${mName.name}'/>" class="list-group-item">
			                                    <i class="fa fa-th-large fa-fw"></i> ${mName.name}
			                                    <span class="pull-right text-muted small"><em>${mName.status}</em>
			                                    </span>
			                                </a>
			                            </c:forEach>
			   				    </c:when>
	   							<c:otherwise>
				   					<p class="list-group-item"> There are no sub-communities here.</p>
				   				</c:otherwise>
			   				</c:choose>
                        </div>
                        <!-- /.list-group -->
                    </div>
                    <!-- /.panel-body -->
                </div>
			</div>    
			
			<!-- Community Managers Row --> 
			<div class="row">
				<div class="panel panel-default">
	                <div class="panel-heading">
	                    <i class="fa fa-sitemap fa-fw"></i> Community Managers 
	                    <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal">
			        		<i class="fa fa-plus"></i>
							<i class="fa fa-users"></i>
							Add Users
				        </button>
				        <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#myModalDel">
				          	<i class="fa fa-minus"></i>
							<i class="fa fa-users"></i>
							Del Users
				        </button>
				        
	                </div>
	                <!-- /.panel-heading -->
	                <div class="panel-body">
	                    <div class="list-group">
		                    <c:forEach var="mName" items="${Managers}" varStatus="row">
		                        <a href="#" class="list-group-item">
		                            	<i class="fa fa-user fa-fw"></i> ${mName}
		                            	<span class="text-muted small">
		                            		<em>  (${myComm.name}) </em>
		                            	</span>
								</a>
		                    </c:forEach>
	                    </div>
	                    <!-- /.list-group -->
	                </div>
	                <!-- /.panel-body -->
	             </div>
			</div>  
			
	        <!-- Modal Form for Adding Users as Community Manager -->
		    <div class="modal fade" id="myModal" role="dialog">
			    <div class="modal-dialog">
			    	<div class="modal-content">
				        <div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal">&times;</button>
				        	<h4 class="modal-title">Add Users as Community Managers</h4>
				        </div>
				        <div class="modal-body">
					       <div class="form-group">
								<label class="col-md-3 control-label">Select Users</label>
								<input type="hidden" id="communityID" value="${myComm.id}">
								<div class="col-md-6">
									<select multiple data-plugin-selectTwo class="form-control populate" id="userMultiSelectMgr">
							            <optgroup label="Users">
											<c:forEach var="account" items="${addUserManagers}">
												<option value="${account.id}">${account.name}</option>
											</c:forEach>
										</optgroup>	
									</select>
								</div>
						  </div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary addMgrs">Save changes</button>
				      </div>
			        </div>
			    </div>
		    </div>	<!-- end of modal --> 
		    
		    <!-- Modal Form for Deleting Users as Community Manager -->
		    <div class="modal fade" id="myModalDel" role="dialog">
			    <div class="modal-dialog">
			    	<div class="modal-content">
				        <div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal">&times;</button>
				        	<h4 class="modal-title">Remove Users from Community Managers List</h4>
				        </div>
				        <div class="modal-body">
					       <div class="form-group">
								<label class="col-md-3 control-label">Select Users</label>
								<input type="hidden" id="communityID" value="${myComm.id}">
								<div class="col-md-6">
									<select multiple data-plugin-selectTwo class="form-control populate" id="userMultiSelectDel">
							            <optgroup label="Users">
											<c:forEach var="account" items="${managers}">
												<option value="${account.id}">${account.name}</option>
											</c:forEach>
										</optgroup>	
									</select>
								</div>
						  </div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary delMgrs">Save changes</button>
				      </div>
			        </div>
			    </div>
		    </div>	<!-- end of modal -->
			            
			               
			     
        </div>
        <!-- /#page-wrapper -->
    </div>
    
</body>

</html>
