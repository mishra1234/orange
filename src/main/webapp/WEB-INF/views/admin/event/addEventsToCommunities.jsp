<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<body>
<c:url var="var" value='/admin/community/saveEvents/'/>
<form:form modelAttribute="comm" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<form:input type="hidden" id="id" name="comm" path="id" value=""/>

<!-- Form Name -->
<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Add/Remove Events to the Community: "${comm.name}"</h2>
		</header>

<!-- Select Multiple -->
				<div class="panel-body">
					<div class="form-group">
						<b><h4>
								<label class="col-sm-3 control-label">List of Events to
									Add:</label>
							</h4></b>
						<div class="col-sm-9">
							<ul class="checkbox-grid" style="display: block">
								<c:forEach items="${listOfEvents}" var="parent">
									<c:set var="x" value="false" />
									<c:forEach items="${comm.activities}" var="selected">
										<c:choose>
											<c:when test="${selected.id == parent.id}">
												<c:set var="x" value="true" />
											</c:when>
										</c:choose>
									</c:forEach>
									<c:choose>
										<c:when test="${x}">
											<input id="parent" type="checkbox" name="xevents"
												value="${parent.id}" checked="checked" />
											<label for="text1">${parent.contentTitle}</label>
											<br>
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="xevents" value="${parent.id}" />
											<label for="text1">${parent.contentTitle}</label>
											<br>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
						</div>
					</div>



					<!-- Button (Double) -->
					<footer class="panel-footer">
						<div class="row">
							<div class="col-sm-9 col-sm-offset-3">
								<a class="btn btn-default"
									href="<c:url value='/admin/events/community/${comm.id}'/>">Cancel</a>
								<button type="submit" id="Save" class="btn btn-primary">save</button>
							</div>
						</div>
					</footer>
				</div>
			</section>
</fieldset>
</form:form>
	
</body>
</html>