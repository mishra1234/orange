<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<body>
 <script src="<c:url value='/scripts/lib/sweet-alert.min.js'/>"></script>
 <link rel="stylesheet" href="<c:url value='/scripts/lib/sweet-alert.css'/>" type="text/css" />

	<script>
		(function( $ ) {
			'use strict';
	
			var datatableInit = function() {
				var $table = $('#eventTable');
	
				$table.dataTable({
					sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
					oTableTools: {
						sSwfPath: $table.data('swf-path'),
						aButtons: [
							{
								sExtends: 'pdf',
								sButtonText: 'PDF'
							},
							{
								sExtends: 'csv',
								sButtonText: 'CSV'
							},
							{
								sExtends: 'xls',
								sButtonText: 'Excel'
							},
							{
								sExtends: 'print',
								sButtonText: 'Print',
								sInfo: 'Please press CTR+P to print or ESC to quit'
							}
						]
					}, 
					"paging":false,
					"info":false,
					"order":[[2,"asc"]], 
					"columnDefs":[{ "visible": false, "targets": 0 }],
				});
	
			};
	
			$(function() {
				datatableInit();
			});
		}).apply( this, [ jQuery ]);
	</script>
	
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Events at Community "${community.name}"</h2>
		</header>
		<div class="panel-body">
			<p>
				<a href="<c:url value='/admin/events/community/${commId}/${community.name}'/>" class="btn btn-primary">Back to Community Dashboard</a>  
			</p>
			<table id="eventTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
				<thead>
					<tr>
						<th>ID</th>
						<th>Icon</th>
						<th>Event Title</th>
						<th>Cost</th>
						<th>Date / Time</th>
						<th># Banners</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ev" items="${myActivitiesList}" varStatus="row">
						<tr>
							<td>${ev.id}</td>
							<td><a href="<c:url value='/admin/event/${ev.id}/${commId}'/>">
								<img src="${ev.iconUrl}" width="25" height="25" onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
							<td>${ev.contentTitle}</td>
							<td>${ev.event.cost}</td>
							<td>${ev.startDate} (${ev.rangeTime})</td>
							<td><a href="<c:url value='/admin/banners/event/${ev.id}'/>">${fn:length(ev.banners)}</a></td>
							<td>
							    <a href="${ev.id}"><i class="deleteCheck" title="Delete"><i class="fa fa-minus-circle"></i></i></a>
							    <input type="hidden" id="eventId" value="${ev.id}"/>		
								<a href="<c:url value='/admin/event/editEvent/${ev.id}/${commId}'/>"><i class="fa fa-edit" title="Edit"></i> </a> 
								<a href="<c:url value='/admin/event/${ev.id}/${commId}'/>"><i class="fa fa-tasks" title="View"></i></a>
								<a href="<c:url value='/admin/booking/bookings/${ev.id}/${commId}'/>"><i class="fa fa-book" title="Bookings"></i></a>
		  						<a href="<c:url value='/admin/booking/schedules/${ev.id}/${commId}'/>"><i class="fa fa-calendar-o" title="Schedules"></i></a>
	  						</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<h5>
				<c:set var="current" value="${curr}" />
				<c:choose>
					<c:when test="${current < 5}">
						<c:forEach var="num" items="${numbers}" varStatus="row" begin="0" end="9">
							<c:choose>
								<c:when test="${num==current}">
									<a class="btn btn-primary btn-lg" href="<c:url value='/admin/events/${num}/${commId}/${evValId}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/events/${num}/${commId}/${evValId}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:when>
					
					<c:when test="${(current>4) and (current<(hmany-5))}">
						<a class="btn" href="<c:url value='/admin/events/${prev}/${commId}/${evValId}'/>">Prev</a>
						<c:forEach var="num" items="${numbers}" varStatus="row" begin="${current-5}" end="${current+5}">
							<c:choose>
								<c:when test="${num==current}">
									<a class="btn btn-primary btn-lg" href="<c:url value='/admin/events/${num}/${commId}/${evValId}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/events/${num}/${commId}/${evValId}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<input id="curr" type="hidden" name="curr" value="${next}" />
						<a class="btn" href="<c:url value='/admin/events/${next}/${commId}/${evValId}'/>">Next</a>
					</c:when>
					
					<c:otherwise>
						<a class="btn" href="<c:url value='/admin/events/${prev}/${commId}/${evValId}'/>">Prev</a>
						<c:forEach var="num" items="${numbers}" varStatus="row" begin="${current-5}" end="${hmany+2}">
							<c:choose>
								<c:when test="${num==current}">
									<a class="btn btn-primary btn-lg" href="<c:url value='/admin/events/${num}/${commId}/${evValId}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/events/${num}/${commId}/${evValId}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</h5>
		</div>
	</section>
<script>
	jQuery(document).ready(function(){
		var eventID = $('#eventId').val();
		console.log("Inside gated checks");
		jQuery('.deleteCheck').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			var usercontrol=$( this );			
			swal({   
				title: "Wait!!",   
				text: "Are you sure you want to delete it?",   
				type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Yes",   
				cancelButtonText: "No",   
				closeOnConfirm: false,   
				closeOnCancel: false 
				}, 
				function(isConfirm){   
						if(isConfirm){     
							console.log("communityID is ="+eventID);
						 	var action = "delete";
						 	//var realUrl = 
							var controllerurl= "<c:url value='/admin/event/delete/"+eventID+"'/>";
							console.log("Controller URl is:"+ controllerurl);
				
							var jqxhr = $.ajax({
								  	type: "POST",
									url: controllerurl,
									//data: JSON.stringify({ id: communityID}),
									dataType: "json",
									beforeSend: function(xhr) {
									        xhr.setRequestHeader("Accept", "application/json");
									        xhr.setRequestHeader("Content-Type", "application/json");}
								    }).always(function(response){
										if(response.responseText.indexOf("Success")>-1 ){
										   	location.reload();
									    	console.log($( usercontrol ).val());
									    	console.log(response.responseText.indexOf("Success"));
									    	swal("Deleted!", "success");
									    }else{
										   // 		var z= response.responseText.split("--");
										    swal("Cancelled", "error");
										}
									});
						}else{    
							swal("Cancelled", "The operation was cancelled");   
						};
				}
			); 
			$(document).ajaxStart(function(){
				swal({
					  title: "Please wait...",
					  text:  "While your request is been processed",
					  allowOutsideClick:false,
					  allowEscapeKey:false,
					  showConfirmButton: false
					});
			});
		});
	});
</script>

</body>

</html>