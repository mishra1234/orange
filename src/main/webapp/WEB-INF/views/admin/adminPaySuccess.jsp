<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="<c:url value='/js/lib/jquery.payment.js'/>"></script>

	<!-- Page Title
		============================================= -->
<section id="page-title">
	<div class="container clearfix">
		<div class="row">
			<div class="col-md-3 center">
				<img src="${ev.iconUrl}" height="100" />
			</div>
			<div class="col-md-6">
				<h2>${ev.contentTitle}</h2>
				<span>Community Connect Me</span>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</section>


<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

			<div class="single-event">

				<div class="col_three_fourth">
					
					<c:if test="${paid}">
					<h3>Payment Approved<br><span>You will receive a confirmation email shortly confirming your payment.</span></h3>
					<div class="row">
						<img src = "<c:url value='/images/tick.png'/>">
					</div>
					<div class="row">Authorization: ${auth}</div>
				    <div class="row">${message}</div>
					</c:if>
					
					<c:if test="${!paid}">
					<h3>Transaction not Proccessed</h3>
					<div class="row">
						<img src = "<c:url value='/images/cancel2.png'/>">
					</div>
					<div class="row"><br><br><p class="danger">${errors}</p></div>
				   
					</c:if>
					
				</div>
				<div class="col_one_fourth col_last">
					<div class="panel panel-default events-meta">
						<div class="panel-heading">
							<h3 class="panel-title">Event Info:</h3>
						</div>
						<div class="panel-body">
							<ul class="iconlist nobottommargin">
								<li><i class="icon-calendar3"></i> ${ev.startDate}</li>
								<li><i class="icon-time"></i> ${ev.rangeTime}</li>
								<li><i class="icon-phone"></i> ${ev.event.contactPhone}</li>
								<li><i class="icon-dollar"></i> <strong>${ev.event.cost}
								</strong></li>
							</ul>
						</div>
					</div>
					<!-- <a href="<c:url value='/event/pay/${ev.id}'/>"
						class="btn btn-success btn-block btn-lg">Use Paypal</a> -->
				</div>

				<div class="clear"></div>

				<div class="col_three_fourth">

					<h3>Activity Details</h3>

					<p>${ev.contentBody}</p>

					<p>
						<strong>Contact:</strong> ${ev.event.contactName}
					</p>
					<p>
						<strong>Email:</strong> ${ev.event.contactEmail}
					</p>
					<p>
						<strong>Address:</strong> ${ev.event.address}
					</p>


					<!-- 
							<h4>Inclusions</h4>

							<div class="col_half nobottommargin">

								<ul class="iconlist nobottommargin">
									<li><i class="icon-ok"></i> Return Flight Tickets</li>
									<li><i class="icon-ok"></i> All Local/Airport Transfers</li>
									<li><i class="icon-ok"></i> Resort Accomodation</li>
									<li><i class="icon-ok"></i> All Meals Included</li>
									<li><i class="icon-ok"></i> Adventure Activities</li>
								</ul>

							</div>

							<div class="col_half nobottommargin col_last">

								<ul class="iconlist nobottommargin">
									<li><i class="icon-ok"></i> Games</li>
									<li><i class="icon-ok"></i> Local Guides</li>
									<li><i class="icon-ok"></i> Support Staff</li>
									<li><i class="icon-ok"></i> Personal Security</li>
									<li><i class="icon-ok"></i> VISA Fees &amp; Medical Insurance</li>
								</ul>

							</div>
 -->
				</div>



				<div class="col_one_fourth col_last">

					<h4>Location</h4>

					<section id="event-location" class="gmap" style="height: 300px;"></section>

					<script type="text/javascript"
						src="http://maps.google.com/maps/api/js?sensor=false"></script>
					<script type="text/javascript"
						src="<c:url value='/js/jquery.gmap.js'/>"></script>

					<script type="text/javascript">

								jQuery('#event-location').gMap({

									address: '${ev.event.address}',
									maptype: 'ROADMAP',
									zoom: 15,
									markers: [
										{
											address: "${ev.event.address}"
										}
									],
									doubleclickzoom: false,
									controls: {
										panControl: true,
										zoomControl: true,
										mapTypeControl: true,
										scaleControl: false,
										streetViewControl: false,
										overviewMapControl: false
									}

								});

							</script>

				</div>

				<div class="clear"></div>

			</div>

		</div>

			</div>
			
		</section><!-- #content end -->