<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>
	<br>
	<h1>--- HTTP Status 504 -  Error on the Web-Link. ---</h1>
	<br>
	<br>
	<h2>Ups!!!, it seems there was a problem on your event web-link address.</h2>
	<br>
	<br>
	<h2>Please go back and verify the web-link has at least a title and is written correctly.</h2>

</body>
</html>