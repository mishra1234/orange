<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
 
<h3>Edit Account</h3>
<c:url var="save" value="/admin/account/save/${account.id}" />
<form:form modelAttribute="account" method="POST" action="../save">
 <table>
 
  <tr>
   <td><form:label path="role.name">Role</form:label></td>
   <td><form:input path="role.name"/></td>
  </tr>
     
  <tr>
   <td><form:label path="name">Account Name</form:label></td>
   <td><form:input path="name"/></td>
  </tr>
 
 <tr>
   <td><form:label path="email">eMail</form:label></td>	
   <td><form:input path="email"/></td>
 </tr>
   
  <tr>
   <td><form:label path="password">Password:</form:label></td>
   <td><form:input path="password"/></td>
  </tr>
  
  <tr>
   <td><form:label path="salt">salt:</form:label></td>
   <td><form:input path="salt"/></td>
  </tr>
  
  <tr>
   <td><form:label path="timezone">timezone:</form:label></td>
   <td><form:input path="timezone"/></td>
  </tr>
 
  <tr>
   <td><form:label path="firstName">firstName:</form:label></td>
   <td><form:input path="firstName"/></td>
  </tr>
 
  <tr>
   <td><form:label path="lastName">lastName:</form:label></td>
   <td><form:input path="lastName"/></td>
  </tr>
  
  <tr>
   <td><form:label path="postcode">postcode:</form:label></td>
   <td><form:input path="postcode"/></td>
  </tr>
  
  <tr>
   <td><form:label path="status">status:</form:label></td>
   <td><form:input path="status"/></td>
  </tr>
  
  <tr>
   <td><form:label path="resetCode">resetCode:</form:label></td>
   <td><form:input path="resetCode"/></td>
  </tr>
  
 </table> 
 
 
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
	<button type="submit" value="Cancel" name="_cancel" class="btn btn-default">Cancel</button>
    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
  </div>
</div>

</form:form>
</body>
</html>