<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<body>
<c:url var="var" value='/admin/community/saveUsers/'/>
<form:form modelAttribute="comm" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<form:input type="hidden" id="id" name="comm" path="id" value=""/>

<!-- Form Name -->
<legend>Add/Remove Users to the Community ${comm.name}</legend>

<!-- Select Multiple -->
<div class="control-group">
  <label  style="display:block; margin-bottom:10px; margin-left:35px">List of Users to Add</label>
 	<ul class="checkbox-grid" style="display:block" >
 	<c:forEach items="${listOfUsers}" var="parent">
 		<c:set var="x" value="false"/> 
    		<c:forEach items="${comm.accounts_1}" var="selected">
    		<c:choose>
	    		<c:when test="${selected.id == parent.id}">
	    			<c:set var="x" value="true"/> 
	    		</c:when>
    		</c:choose>
    	 	</c:forEach>
    	 	<c:choose>
	    		<c:when test="${x}">
    	<li><input id="parent" type="checkbox" name="xusers" value="${parent.id}" checked="checked"/><label for="text1">${parent.name}</label></li>
	    	</c:when>
		    		<c:otherwise>
		<li><input type="checkbox" name="xusers" value="${parent.id}" /><label for="text1">${parent.name}</label></li>
		    		</c:otherwise>
		      </c:choose>
    </c:forEach>
    </ul>
</div>
  


<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="cancel"></label>
  <div class="controls">
    <a class="btn btn-medium btn-cancel" href="<c:url value='/admin/communities'/>">Cancel</a>
    <button type="submit" value="Save" name="save"   class="btn btn-primary">Save</button>
  </div>
</div>

</fieldset>
</form:form>
	
</body>
</html>