<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<style>
	    #sync1 .item{
	    	background: #0c83e7;
	        padding: 80px 0px;
	        margin: 5px;
	        color: #FFF;
	        -webkit-border-radius: 3px;
	        -moz-border-radius: 3px;
	        border-radius: 3px;
	        text-align: center;
	    }
	    #sync2 .item{
	        background: #C9C9C9;
	        padding: 2px 0px;
	        margin: 2px;
	        color: #FFF;
	        -webkit-border-radius: 2px;
	        -moz-border-radius: 2px;
	        border-radius: 2px;
	        text-align: center;
	        cursor: pointer;
	    }
	    #sync2 .item h1{
	      font-size: 18px;
	    }
	    #sync2 .synced .item{
	      background: #0c83e7;
	    }
    </style>     	
</head>

<body>

<c:url var="var" value='/admin/account/save'/>
	
<!-- Jquery Validation files -->
	<script src="assets/vendor/jquery/jquery.js"></script>
	<script
		src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
	<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script
		src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="assets/vendor/magnific-popup/magnific-popup.js"></script>
	<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

	<!-- Specific Page Vendor -->
	<script src="assets/vendor/jquery-validation/jquery.validate.js"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="assets/javascripts/theme.js"></script>

	<!-- Theme Custom -->
	<script src="assets/javascripts/theme.custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="assets/javascripts/theme.init.js"></script>


	<!-- Examples -->
	<script src="assets/javascripts/forms/examples.validation.js"></script>
<form:form modelAttribute="account" id="form" class="form-horizontal" method="POST" action="${var}">
<fieldset>

<!-- Form Name -->
<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Account Management</h2>
			<p class="panel-subtitle">Edit account</p>
		</header>

		<!-- Hidden ID -->
		<form:input type="hidden" id="id" 		   path="id" value=""/>
		<form:input type="hidden" id="password"    path="password" value=""/>
		<form:input type="hidden" id="salt" 	   path="salt"/>
		<form:input type="hidden" id="resetCode"   path="resetCode"/>
		<form:input type="hidden" id="enabled"     path="enabled" value="true"/>
		<form:input type="hidden" id="facebookUid" path="facebookUid" value="0"/>
		<form:input type="hidden" id="googleUid"   path="googleUid" value="0"/>
		<form:input type="hidden" id="role"		   path="role.id"/>
		<form:input type="hidden" id="status"      path="status"/>
		<form:input type="hidden" id="imageUrl"    path="imageUrl"/>
		<form:input type="hidden" id="timezone"    path="timezone"/>
		<form:input type="hidden" id="name"    	   path="name"/>
		<form:input type="hidden" id="platform"    path="platform"/>
		<form:input type="hidden" id="subscribe"   path="subscribe"/>
		<form:input type="hidden" id="address" 	   value="780 South Road, Glandore, Adelaide, South Australia" path="address"/>
		<form:input type="hidden" id="pc" 		   value="5037" path="postcode" /> 

		 <br>
		 
		 <div class="form-group">
                  	<div class="col-md-12">
                     	<label class="col-sm-3 control-label" for="email">First Name<span class="required">*</span></label>
                         <div class="col-sm-9">
                         	<form:input path="firstName"  class="form-control input-lg"  maxlength="45" type="pattern" required="required" autofocus="autofocus" placeholder="First Name" />
                     	</div>
                     </div>
                 </div>
            		<form:errors path="*" /> 
            		

                 <div class="form-group">
                     <div class="col-md-12">
                         <label class="col-sm-3 control-label" for="email">Surname*</label>
                         <div class="col-sm-9">
                         	<form:input path="lastName" required="required" type="pattern" maxlength="45" class="form-control input-lg" placeholder="Last Name" />
                     	</div>
                     </div>
                 </div>

		<!-- Text input-->
		<div class="form-group">
			<div class="col-md-12">
				<label class="col-sm-3 control-label" for="email">Email<span class="required">*</span></label>
				<div class="col-sm-9">
					<form:input id="email" name="email" type="email"
						placeholder="email@email.com" class="form-control" data-plugin-maxlength="data-plugin-maxlength" maxlength="255"
						required="required" path="email" />
				</div>
			</div>
		</div>
		
		<div class="form-group">
                  <div class="col-md-12">
                      <label class="col-sm-3 control-label" for="email">Phone Number</label>
                      <div class="col-sm-9">
                      	<form:input id="mobilephone" maxlength="40" type="contact" class="form-control input-lg" path="mobilephone" placeholder="ie 08XX XXX XXX"/>
                  	</div>
                  </div>
              </div>
		
<!--    		<div class="form-group"> -->
<!--         	<div class="col-md-12"> -->
<!--             	<label class="col-sm-3 control-label">Status*</label> -->
<!--                 <div class="col-sm-9"> -->
<!--                 	<select id="statusSelect" name="statusSelect" class="form-control"> -->
<%-- 						<c:forEach items="${statusComm}" var="parent"> --%>
<%-- 							<c:set var="x" value="false" /> --%>
<%-- 							<c:forEach items="${account.layers_1}" var="selected"> --%>
<%-- 								<c:choose> --%>
<%-- 									<c:when test="${selected.id == parent.id}"> --%>
<%-- 										<c:set var="x" value="true" /> --%>
<%-- 									</c:when> --%>
<%-- 								</c:choose> --%>
<%-- 							</c:forEach> --%>
<%-- 							<c:choose> --%>
<%-- 								<c:when test="${x}"> --%>
<%-- 										<option value="${parent.id}" selected="selected">${parent.name}</option> --%>
<%-- 								</c:when> --%>
<%-- 								<c:otherwise> --%>
<%-- 										<option value="${parent.id}">${parent.name}</option> --%>
<%-- 								</c:otherwise> --%>
<%-- 							</c:choose> --%>
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!--         </div>                                         -->
<!--         <br> -->
<!-- 		<div class="form-group"> -->
<!-- 			<div class="col-md-12"> -->
<!-- 				<label class="col-sm-3 control-label">Please select the local government area(s) in which you are involved in: (start typing)</label>  -->
<!-- 				<div class="col-sm-9"> -->
<!-- 					<select name="regionalMultiSelect" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px"> -->
<!-- 						<optgroup label="SA Regional Councils"> -->
<!-- 							<option value="allcommunities">-- Select All Govenment Areas --</option> -->
<%-- 							<c:forEach items="${councils}" var="parent"> --%>
<%-- 								<c:set var="x" value="false" /> --%>
<%-- 								<c:forEach items="${account.layers_1}" var="selected"> --%>
<%-- 									<c:choose> --%>
<%-- 										<c:when test="${selected.id == parent.id}"> --%>
<%-- 											<c:set var="x" value="true" /> --%>
<%-- 										</c:when> --%>
<%-- 									</c:choose> --%>
<%-- 								</c:forEach> --%>
<%-- 								<c:choose> --%>
<%-- 									<c:when test="${x}"> --%>
<%-- 											<option value="${parent.id}" selected="selected">${parent.name}</option> --%>
<%-- 									</c:when> --%>
<%-- 									<c:otherwise> --%>
<%-- 											<option value="${parent.id}">${parent.name}</option> --%>
<%-- 									</c:otherwise> --%>
<%-- 								</c:choose> --%>
<%-- 							</c:forEach> --%>
<!-- 						</optgroup> -->
<!-- 					</select> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
		<br>
		<!-- Button (Double) -->
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-9 col-sm-offset-3">
				<a class="btn btn-default" href="<c:url value='/admin/accounts'/>">Cancel</a>
				<button type="submit" id="Save" class="btn btn-primary">Save</button>
			</div>
		</div>
		</footer>
	</section>

	<!---- SCRIPTS ---->
	<script>
		(function() {
			'use strict';
			jQuery.validator.addMethod("pattern", function(value, element) {
			    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
			}, "Your data contain invalid characters");
			     
			   
			jQuery.validator.addMethod("postcode", function(value, element) {
				    return  isValidPostCode() && /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
					}, "Your data is not valid, must contain atleast 4 numeric digits");    
	
					function isValidPostCode(){
					var isAus = $("#postcode").val();
					    if (isAus.length >3 && !isNaN(isAus)) 
					        return true; 
					};
					
			jQuery.validator.addMethod("urlIcon", function(value, element) {
					    	return isValidIconSize();
						}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only.");
			
			 function isValidIconSize(){
				 var imgWidth = $("#theImg").width();
				 var imgHeight = $("#theImg").height();
				 var extension = $('#theImg').attr("src").split('.').pop().toLowerCase();;
				 console.log("URL: " + extension);
				 console.log("width:" + imgWidth);
				 console.log("height:" + imgHeight);
				 if (imgHeight == 200 && imgWidth == 200 && (extension == "jpg" || extension == "png"))
					return true;
					 };   
			// basic
			$("#form").validate({
				highlight: function( label ) {
					console.log("validation starts");
					$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				success: function( label ) {
					$(label).closest('.form-group').removeClass('has-error');
					label.remove();
				},
				errorPlacement: function( error, element ) {
					var placement = element.closest('.input-group');
					if (!placement.get(0)) {
						placement = element;
					}
					if (error.text() !== '') {
						placement.after(error);
					}
				}
			});
		}).apply( this, [ jQuery ]);
	</script>
	

</fieldset>
	
</form:form>
<!-- File Upload javascript files -->
	<link href="<c:url value='/styles/dropzone.css'/>" type="text/css" 	rel="stylesheet" />
	<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
	<script src="<c:url value='/scripts/iconUploadForNewAccount.js'/>"></script>

</body>
</html>