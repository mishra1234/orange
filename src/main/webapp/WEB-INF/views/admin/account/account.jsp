<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<html>
<body>
<br>
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h1 class="panel-title">Account Information</h1>
		</header>
		<div class="panel-body">
			<div class="form-group">

				<table class="form-group">
					<thead>
						<tr>
							<td><label class="col-sm-3 control-label">Avatar:</label></td>
							<td><img src="${acc.imageUrl}" width="50" height="50"
								onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';">
							</td>
						</tr>
						<tr>
							<td><label class="col-md-9">Role:</label></td>
							<td><label class="col-sm-3 control-label">${acc.role.name}</label></td>
						</tr>
						<tr>
							<td><label class="col-md-9">Name: </label></td>
							<td><label class="col-md-12">${acc.name}</label></td>
						</tr>
						<tr>
							<td><label class="col-md-12">First Name: </label></td>
							<td><label class="col-md-12">${acc.firstName}</label></td>
						</tr>
						<tr>
							<td><label class="col-md-12">Last Name:</label></td>
							<td><label class="col-md-12">${acc.lastName}</label></td>
						</tr>
						<tr>
							<td><label class="col-md-12">eMail: </label></td>
							<td><label class="col-md-12">${acc.email}</label></td>
						</tr>
						<tr>
							<td><label class="col-md-12">TimeZone:</label></td>
							<td><label class="col-md-12">${acc.timezone}</label></td>
						</tr>
						<tr>
							<td><label class="col-md-12">Post Code: </label></td>
							<td><label class="col-md-12">${acc.postcode}</label></td>
						</tr>
						<tr>
							<td><label class="col-md-12">Status: </label></td>
							<td><label class="col-md-12">${acc.status}</label></td>
						</tr>
					</thead>
				</table>


				<h5>

					<a class="btn btn-primary"
						href="<c:url value='/admin/account/editAccount/${acc.id}'/>">Edit
						Account</a> <a class="btn btn-primary"
						href="<c:url value='/admin/account/resetPassword/${acc.id}'/>">Reset
						Account Password</a>
				</h5>
			</div>
		</div>
	</section>
</body>

</html>