<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<script>
		(function( $ ) {
			'use strict';
	
			var datatableInit = function() {
				var $table = $('#accountsTable');
	
				$table.dataTable({
					sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
					oTableTools: {
						sSwfPath: $table.data('swf-path'),
						aButtons: [
							{
								sExtends: 'pdf',
								sButtonText: 'PDF'
							},
							{
								sExtends: 'csv',
								sButtonText: 'CSV'
							},
							{
								sExtends: 'xls',
								sButtonText: 'Excel'
							},
							{
								sExtends: 'print',
								sButtonText: 'Print',
								sInfo: 'Please press CTR+P to print or ESC to quit'
							}
						]
					}, 
					"paging":false,
					"info":false,
					"order":[[1,"asc"]], 
					"columnDefs":[{ "visible": false, "targets": 0 }],
				});
	
			};
	
			$(function() {
				datatableInit();
			});
		}).apply( this, [ jQuery ]);
	</script>

	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Accounts</h2>
		</header>
		<div class="panel-body">


			<!--  <h5>
				<a class="btn" href="<c:url value='/admin/account/new/'/>">Create
					New Account</a>
			</h5>-->
			<table id="accountsTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
				<thead>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Role</th>
						<th>eMail</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="acc" items="${myList}" varStatus="row">
						<tr>
							<td>${acc.id}</td>
							<td>${acc.name}</td>
							<td>${acc.role.name}</td>
							<td>${acc.email}</td>
							<td>
								<a href="<c:url value='/admin/account/editAccount/${acc.id}'/>"><i class="fa fa-edit"></i></a>
						</tr>
					</c:forEach>

				</tbody>
			</table>

			<h5>
				<c:set var="curr" value="${curr}" />
				<c:choose>
					<c:when test="${curr <= 5 }">
						<c:forEach var="num" items="${numbers}" varStatus="row" begin="0"
							end="10">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/accounts/${commId}/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/accounts/${commId}/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:when>
					<c:when test="${curr>10 and curr<(hmany-5)}">
						<a class="btn" href="<c:url value='/admin/accounts/${commId}/${prev}'/>">Prev</a>
						<c:forEach var="num" items="${numbers}" varStatus="row"
							begin="${curr-5}" end="${curr+5}">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/accounts/${commId}/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/accounts/${commId}/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<input id="curr" type="hidden" name="curr" value="${next}" />
						<a class="btn" href="<c:url value='/admin/accounts/${commId}/${next}'/>">Next</a>
					</c:when>
					<c:otherwise>
						<a class="btn" href="<c:url value='/admin/accounts/${commId}/${prev}'/>">Prev</a>
						<c:forEach var="num" items="${numbers}" varStatus="row"
							begin="${curr-5}" end="${hmany+1}">
							<c:choose>
								<c:when test="${num==curr}">
									<a class="btn btn-primary btn-lg"
										href="<c:url value='/admin/accounts/${commId}/${num}'/>">${num}</a>
								</c:when>
								<c:otherwise>
									<a class="btn" href="<c:url value='/admin/accounts/${commId}/${num}'/>">${num}</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</h5>

		</div>
	</section>

</body>

</html>