<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html>
<body>
<section class="level1  contentWidth clearBoth">
	<br>
	<br>
	<br>
	<h1 >We apologise, the account provided is already registered on Rural Connect.</h1>
	<nav class="left"></nav>
	
 	<br>
 	<c:url var="var" value='/register/'/>
  	<form:form class="form-horizontal" method="POST" action="${var}">
		 <div class="content">
			<h3>Hint on registering.</h3>
	        <p>Remember you cannot add a new member using the same email.</p>
	        
	        <p>If you want to login using your existing account please go to log on button.</p>
			<br>
			<div class="submitbutton">
				<a  class="btn btn-medium btn-cancel" href="/ccme">Rural Connect Home</a> 
			</div>
		</div>
		<br>
		<br>
		<br>
		<br>
    </form:form>
	
</section>
</body>
</html>