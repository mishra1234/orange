<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />


<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

</style>
</head>
<body>


	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div id='loading'>loading...</div>

				<div id='calendar'></div>

			</div>
		</div>
	</div>

	<script>
		$(document).ready(function() {

			$('#calendar').fullCalendar({
				theme : true,
				header : {
					left : 'prev,next today',
					center : 'title',
					right : 'month,agendaWeek,agendaDay'
				},

				editable : true,
				eventLimit : true, // allow "more" link when too many events
				events : {
					url : '<c:url value="/api/myEvents"/>'
				},
				loading : function(bool) {
					$('#loading').toggle(bool);
				}
			});

		});
	</script>

</body>
</html>
