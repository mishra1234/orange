<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- start: page -->
<html>

<body>

    <link rel="stylesheet" href="<c:url value='/styles/vendor/colorbox/colorbox.css'/>" type="text/css" />
	<script src="<c:url value='/scripts/vendor/colorbox/jquery.colorbox.js'/>"></script>
    

	<script src="<c:url value='/ruralAssets/js/newsRSS.js'/>"></script>
	<script src="<c:url value='/ruralAssets/myJS/custom/firealerts.js'/>"></script>
	<script src="<c:url value='/ruralAssets/myJS/custom/weather.js'/>"></script>
	<script src="<c:url value='/ruralAssets/myJS/custom/currencyexchange.js'/>"></script>

	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load("feeds", "1");
		google.setOnLoadCallback(newsRSS.init);
	</script>

	<div role="main" class="main">
		<div class="home-intro" id="home-intro">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						 <p>Welcome to Rural Connect  <em>Dashboard</em>
						 </br>
                         <strong>Today's reports</strong>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="<c:url value='/member/partners'/>" class="btn btn-lg btn-primary" onclick="_gaq.push(['_trackEvent', 'Commercial Partners', 'Click', 'Meet our commercial partners']);" target="_blank">Meet our commercial partners</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container" style="margin-top:-45px">
			<div class="row">
				<div class="col-md-12">
					<div class="tabs" id="tabs">
						<ul class="nav nav-tabs nav-justified">
							<li class="active">
								<a href="#dashboardTab" data-toggle="tab" class="text-center" onclick="_gaq.push(['_trackEvent', 'Member Dashboard Tab', 'Click', 'Member Dashboard']);"><i class="fa fa-star"></i> Dashboard</a></li>
							<li><a href="#newsTab"    data-toggle="tab" class="text-center" onclick="_gaq.push(['_trackEvent', 'Member News Tab', 'Click', 'Member News Tab']);">News</a></li>
							<li id="eventList"><a href="#eventsTab"  data-toggle="tab" class="text-center" onclick="_gaq.push(['_trackEvent', 'Member Event Tab', 'Click', 'Member Event Tab']);">Events</a></li>
							<li><a href="#surveysTab" data-toggle="tab" class="text-center" onclick="_gaq.push(['_trackEvent', 'Survey Tab', 'Click', 'Member Survey Tab']);">Surveys</a></li>
							<li><a href="#weatherTab" data-toggle="tab" class="text-center" onclick="_gaq.push(['_trackEvent', 'Weather Tab', 'Click', 'Member Weather Tab']);">Weather</a></li>
							<li><a href="#paymentsTab" data-toggle="tab" class="text-center" onclick="_gaq.push(['_trackEvent', 'Payments Tab', 'Click', 'PaymentTab']);">Forms</a></li>
							  <li><a href="#transportTab" data-toggle="tab" class="text-center">Emergencies</a></li>
								  <li><a href="#workplaceTab" data-toggle="tab" class="text-center">Workplace</a></li>
					
						</ul>
						
						<div class="tab-content">
							<div id="dashboardTab" class="tab-pane active">
								<div class="row">
									<div class="col-md-6">
										<div class="featured-box featured-box-tertiary">
											<div class="box-content">
										        <H4><i class="fa fa-th-large"></i> <strong>Pinboard</strong></H4>
                                                <br>
                                                <div class="toggle" data-plugin-toggle="">
                                                    <c:forEach var="act" items="${infos}" begin="0" end="0" varStatus="row">
                                                        <section class="toggle">
                                                            <label>
                                                                <div class="img-thumbnail">
                                                                    <img class="avatar" alt="" src="<c:url value='${act.iconUrl}'/>" width="45px">
                                                                </div>
                                                                ${act.contentTitle}
                                                            </label>
                                                            <div class="toggle-content" style="display: block;">
                                                                <div class="detail" style="display: block; ">
                                                                    <c:forEach var="banner" items="${act.banners}" begin="0" end="0" varStatus="counter">
                                                                        <p><img src="<c:url value='${banner.imageUrl}'/>" width="250px" height="150" class="img-responsive imgRight" />
                                                                        <p align="left" style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"> 
                                                                         ${act.contentBody}
                                                                        </p></br></br></br></br>
                                                                        <div class="post-meta"> 
                                                                            <a class="infosframe" href="<c:url value='/member/info/${act.id}'/>" class="btn btn-xs btn-primary pull-right">More info...</a>
                                                                        </div></br>
                                                                        </p>
										                            </c:forEach>
                                                                </div>
                                                                
                                                            </div>
                                                        </section>
                                                    </c:forEach>
                                                    <c:forEach var="act" items="${infos}" begin="1" end="5" varStatus="row">
                                                        <section class="toggle">
                                                            <label>
                                                                <div class="img-thumbnail">
                                                                    <img class="avatar" alt="" src="<c:url value='${act.iconUrl}'/>" width="45px">
                                                                </div>
                                                                ${act.contentTitle}
                                                            </label>
                                                            <div class="toggle-content" style="display: none;">
                                                                <div class="detail" style="display: block; ">
                                                                    <c:forEach var="banner" items="${act.banners}" begin="0" end="0" varStatus="counter">
                                                                        <p><img src="<c:url value='${banner.imageUrl}'/>" width="250px" height="150" class="imgRight" />
                                                                        <p align="left" style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"> 
                                                                         ${act.contentBody}
                                                                        </p></br></br></br></br>
                                                                        <div class="post-meta"> 
                                                                            <a class="infosframe" href="<c:url value='/member/info/${act.id}'/>" class="btn btn-xs btn-primary pull-right">More info...</a>
                                                                        </div></br>
                                                                        </p>
										                            </c:forEach>
                                                                </div>
                                                                
                                                            </div>
                                                        </section>
                                                    </c:forEach>
                                                    
                                                    
													<a href="<c:url value='/member/infos'/>" class="notification-icon warning" style="background-color:transparent;">
														<H4><i class="fa fa-level-down"></i> <strong>View News Archive</strong></H4>
													</a>
													
                                                    	
									</a>
                                                </div>
											</div>
										</div>
									</div>
									<!-- Stay up to date-->


									<!-- Box 1 -->
									<div class="col-md-3">
										<div class="featured-box featured-box-primary">
											<div class="box-content">
												<H4><i class="fa fa-cloud-download"></i> <strong>Alerts</strong></H4>
												
									<!-- Ivan Content-->
												<div class="row text-center">
													<div class="col-xs-6">
														<strong><span id="spnWeatherWidget"></strong><br>
														<small><i class="fa fa-cloud"></i> Weather Alerts</small>
													</div>
													<div class="col-xs-6">
														<strong><span id="spnFireAlertsWidget"></strong><br>
														<small><i class="fa fa-fire-extinguisher"></i>
															Fire Alerts</small>
														<div id="dvFireAlertModal" style="display: none"></div>
													</div>
												</div>
									<!--End  content  -->
												<br>
											</div>
										</div>
									</div>
									<!-- End Box 2 - WEATHER-->

									<div class="col-md-3">
										<div class="featured-box featured-box-tertiary">
											<div class="box-content">
												<H4><i class="fa fa-envelope"></i><strong> Inbox</strong></H4>
									<!-- Ivan Content-->
												<div class="row text-center">
													<div class="center middle"><a  href="<c:url value='/member/inbox'/>" onclick="_gaq.push(['_trackEvent', 'MemberInbox', 'Inbox messages', 'New Messages']);">(${newMessages}) New Messages</a></div>
													<br /> <br /> <br />
												</div>
									<!--End  content  -->
											</div>
										</div>
									</div>
								

									<div class="col-md-2">
						                <div class="featured-box featured-box-primary">
						                    <div class="box-content">
						                        <H4><i class="fa fa-film"></i> <strong>TV NEWS</strong></H4>
						
						                        <!-- Ivan Content-->
						
						                        <div class="row text-center">
						                            <a href="<c:url value='/member/newsVideo'/>" onclick="_gaq.push(['_trackEvent', 'Tv news', 'Play', 'Member TV News']);"><img  src="<c:url value='/ruralAssets/img/icons/youtube-play-button.png'/>" width="40%"/></a>
						                        </div> <!--End  content  -->
						                    </div>
						                </div>
						            </div>  <!-- End News Section-->
						         	<!-- Radio Secrion-->

									<div class="col-md-4">
										<div class="featured-box featured-box-quartenary">
											<div class="box-content">
												<H4><i class="fa fa-microphone"></i> <strong>Country Hour Podcast</strong></H4>
												<div class="row text-center">
													<div class="center middle">
														<div id="playingName" style="font-weight: Bold;">
														   
															<i class="fa fa-volume-up"></i>&nbsp;&nbsp;Today's ABC Country Hour
														</div>
													</div>
													<br />
													<audio controls="play" id="au1" src="http://mpegmedia.abc.net.au/news/audio/201503/rural-sach-podcast-3003.mp3" >
														<source src="http://mpegmedia.abc.net.au/news/audio/201503/rural-sach-podcast-3003.mp3" type="audio/mpeg" onclick="_gaq.push(['_trackEvent', 'Member Radio', 'Play', 'Member Radio']);">
                                						Your browser does not support the audio element.</source>
													</audio>
													<br />
													<div id="dvRadio"></div>
											</div>
										</div>
									</div>
									<!-- Stay up to daue-->
									</div>

								</div>
								<!-- ROW DASHBOARD -->
							</div>
							
							<div id="newsTab" class="tab-pane">
								<div class="row">
									

									<div class="col-md-6">
										<div class="featured-box featured-box-primary">
											<div class="box-content">
												<H4><i class="fa fa-newspaper-o"></i> Latest News</H4>

												<!-- Ivan Content News-->
												<div class="widget">
													<div class="widget-extra">
														<!-- Timeline Content -->
														<div id="dvNewsRSS" class="timeline">
															<ul class="timeline-list">
																<li class="active">
																	<div class="timeline-icon">
																		<i class="fa fa-bullseye"></i>
																	</div>
																	<div class="timeline-time">
																		<small>22-1-2015</small>
																	</div>
																	<div class="timeline-content">
																		<p class="push-bit" style="display: inline-block">
																		<div class="col-sm-6 col-md-4">
																			<a target="#"
																				href="http://www.abc.net.au/news/image/5954934-16x9-2150x1210.jpg"
																				data-toggle="lightbox-image"><img
																				src="http://www.abc.net.au/news/image/5954934-16x9-2150x1210.jpg"
																				alt="image"></a>
																		</div>
																		</p>
																		<p class="push-bit" style="display: inline-block">Queensland
																			cattle producers say the results of a review into
																			Australia's live export system prove the industry is
																			serious about ...</p>
																		<p class="push-bit">
																			<a target="#"
																				href="http://www.abc.net.au/news/2015-01-22/queensland-graziers-say-cattle-industry-reformed/6034844"
																				class="btn btn-xs btn-primary" onclick="_gaq.push(['_trackEvent', 'Latest News 1', 'Click', 'Latest News Link1 =']);"><i class="fa fa-file"></i> Read the article</a>
																		</p>
																		<div class="row push"></div>
																	</div>
																</li>
																<li class="active">
																	<div class="timeline-icon">
																		<i class="fa fa-file-text"></i>
																	</div>
																	<div class="timeline-time">
																		<small>22-1-2015</small>
																	</div>
																	<div class="timeline-content">
																		<p class="push-bit">
																			<a
																				href="Fletcher International invests in rail freight"><strong>Fletcher
																					International invests in rail freight</strong></a>
																		</p>

																		<p class="push-bit">Export company Fletcher
																			International had made an investment in rail freight
																			to use in western New South Wales.</p>

																		<p class="push-bit">
																			<a target="#"
																				href="http://www.abc.net.au/news/2015-01-22/nsw-fletchers-rolling-stock/6034526"
																				class="btn btn-xs btn-primary" onclick="_gaq.push(['_trackEvent', 'Latest news', 'Click', 'Latest News Links']);"><i
																				class="fa fa-file"></i> Read the article</a>
																		</p>

																		<div class="row push">
																			<div class="col-sm-6 col-md-4">
																				<a target="#"
																					href="http://www.abc.net.au/news/image/6034460-16x9-2150x1210.jpg"
																					data-toggle="lightbox-image"><img
																					src="http://www.abc.net.au/news/image/6034460-16x9-2150x1210.jpg"
																					alt="image"></a>
																			</div>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<!-- END Timeline Content -->
													</div>
												</div>
												
											</div>
										<div class="timeline-time">
										<small style="text-align:left;"><strong>Disclaimer:</strong> Please note that these newsfeeds are sourced from other organisations and may not represent the views or policy of PPSA and PPSA's member associations.</small>	
						</div>				</div>
									</div>
									<!--End News Col 6  -->
									<div class="col-md-6">
										<div class="featured-box featured-box-fifth">
											<div class="box-content">
												<H4><i class="fa fa-retweet"></i> Tweets</H4>
												<div>
													<div id="dvTwitter">
														<center>
															<a class="twitter-timeline" href="https://twitter.com/iviteri/lists/rural"
																data-widget-id="514625573075689472" onclick="_gaq.push(['_trackEvent', 'Tweets', 'Click', 'Tweets from Rural Connect']);">Tweets from Rural SA</a>
														<script>
																!function(d, s, id) {
																	var js, fjs = d.getElementsByTagName(s)[0], 
																		p = /^http:/.test(d.location) ? 'http':'https';
																	if (!d.getElementById(id)) {
																		js = d.createElement(s);
																		js.id = id;
																		js.src = p+ "://platform.twitter.com/widgets.js";
																		fjs.parentNode.insertBefore(js, fjs);
																	}
																}
																(document, "script", "twitter-wjs");
														</script>
														</center>
													</div>
												</div>
												<!--End Ivan content news -->
											</div>
										</div>
									</div>
									
									
					
								</div>
							</div>
							<div id="transportTab" class="tab-pane" >
								<div class="panel-group" id="accordion" >
									<div class="panel panel">
											
						<h2>
						Emergency	<strong> Management</strong>
						</h2>
						
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Emergency Management
										<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<h5 style="text-transform:capitalize;"><strong>Checklists</strong></h5>			
									
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG" target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG" target="_blank">Heatwaves Risk (PIRSA)</a></li>
<!-- 											<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf" target="_blank">Personal Medical Details Checklist (Emergency Alert) (For printing and completing)</a></li> -->
												
										</ul><h5 style="text-transform:capitalize;"><strong>Bushfire</strong></h5>			
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf" target="_blank">Disaster Planning - Bushfire Checklist (CFS)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf" target="_blank">Disaster Planning - Earthquake Action Plan (SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf" target="_blank">Disaster Planning - Emergency Contacts(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf" target="_blank">Disaster Planning - Extreme Heat Checklist(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf" target="_blank">Disaster Planning - Flood Checklist(SES)</a></li>
											<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf" target="_blank">Extreme heat Checklist (SA Health)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf" target="_blank">Extreme Heat Guide (SA Health)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/61003298-32e8-4452-8a41-d86071e89196-Bushfire_Prevention_and_Preparedness_S2.pdf' target="_blank">Bushfire Prevention and Preparedness (PIRSA) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/97289472-5763-4ec0-ae30-1fe16850a2a1-CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf' target="_blank">Bushfire - Care of Pets and Livestock (CFS) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/d3d9575b-97b1-4d1e-acd4-520b98904157-CFS FACT SHEET - AFTER THE FIRE.pdf' target="_blank">Fact Sheet - After the Fire (CFS)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/0fed75f1-34bf-437a-84a0-736cac01d87c-Emergency animal disease preparedness.pdf' target="_blank">Emergency animal disease preparedness (PIRSA)</a></li>
												<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST%20-%20HEATWAVES%20RISK.PNG' target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												
												</ul>
											
											
										
											</div>
							</div>
									</h4>
								</div></div>
														
					</div></div>
												
							</div></div>
							<!-- Workplace tab -->
												<div id="workplaceTab" class="tab-pane" >
								<div class="panel-group" id="accordion" >
									<div class="panel panel">
						<h2>
							 <strong> Workplace</strong>
						</h2>
						<p class="lead">Links to Mediation and Employment Relations Services (MERS) reports updates on superannuation, WorkCover and arrangements for different types of employment.</p>
						
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											&nbsp;General Employment and Information links <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">
												<ul>
														<li><a href='http://www.mers.com.au/farms/' target="_blank">Latest Awards and Entitlements (MERS)</a></li>
										
												<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/pig/Fair+Work+Information+Booklet+for+Agricultural+Producers.pdf' target="_blank">Fair Work Information Booklet for Agricultural Producers (NFF and Dept of Agriculture)</a></li>
											
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/mers/Member-Update-Superannuation-May-2014.pdf' target="_blank">Member Updates Superannuation May 2014 (MERS)</a></li>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/mers/Work-Experience.pdf' target="_blank">Work Experience (MERS)</a></li>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/Lucerne-Growers-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2014.pdf' target="_blank">Lucerne-Growers-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading (MERS)</a></li>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/mers/Grain-Producers-SA-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2013.doc' target="_blank">Minimum-Wage-Adjustment-SGC-and-Casual-Loading (MERS) (Downloadable link)</a></li>
										
											</ul>	
											</div>
										</div>
									</h4>
								</div>
							</div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;General
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/commmm/BioSecurity+and+on-farm+workers+(Dept+of+Agri).pdf' target="_blank">Biosecurity and on-farm workers (Dept of Agri)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/commmm/Guidelines+for+entering+farms+or+animal+facilities+(Dept+of+Agriculture)+(2).pdf' target="_blank">Guidelines for entering farms or animal facilities (Dept of Agriculture)</a></li>
									<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/commmm/Guidelines+for+visiting+farms+during+disease+outbreak+(Dept+of+Agriculture)+(1).pdf' target="_blank"> Guidelines for visiting farms during disease outbreak (Dept of Agriculture)</a></li>
							
									<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/general_whs/Farm+Safety+Fact+Sheet+(SafeWork+SA).pdf' target="_blank">Farm Safety Fact Sheet (SafeWork SA)</a></li>
									<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/general_whs/Children+On+Farms+Safety+Guide+(SafeWork+SA).pdf' target="_blank">Children on Farms Safety Guide (SafeWork SA)</a></li>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockbusiness/2014+Agricultural+Self+Assessment+Guide+PDF.pdf' target="_blank"> Agricutural Self Assessment Guide (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Employer obligations
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.mers.com.au/wp-content/uploads/2013/07/Member-Update-WHS-June-2013-What-is-reasonably-practicable-in-meeting-obligations-to-ensure-the-health-and-safety-of-workers.doc' target="_blank">Member Update WHS June 2013 (SafeWork SA) (Downloadable doc)</a></li>
									
									</ul>
								</div></div></h4></div></div>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Preparing a safe workplace
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=5892' target="_blank">Labelling of Workplace Hazardous Chemicals (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113699' target="_blank">Managing Noise and Preventing Hearing Loss at Work (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113700' target="_blank">Managing the Risks of Plant in the Workplace (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113701' target="_blank">Managing Risks of Hazardous Chemicals in the Workplace (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113702' target="_blank">Managing Electrical Risks in the Workplace (SafeWork SA)</a></li>
								
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113704' target="_blank">Managing the Work Environment and Facilities (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113705' target="_blank">Preparation of Safety Data Sheets for Hazardous Chemicals  (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113708' target="_blank">Work Health and Safety Consultation Cooperation and Coordination (SafeWork SA)</a></li>
									</ul>
								</div></div></h4></div></div>
								
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Harvesters Code of Practice 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice+CHECKLIST+(1).pdf' target="_blank">Grain Harvesting Code of Practice Checklist (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice_STICKER.pdf' target="_blank">Grain Harvesting Code of Practice Sticker (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Harvesting+Lentils+WARNING.pdf' target="_blank">Harvesting Lentils Warnings (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Harvesting hazard
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/Power+lines+warning+at+Harvest+time.pdf' target="_blank">Power Lines warning at Harvest (SafeWork SA)</a></li>
						
									</ul>
								</div></div></h4></div></div>
								
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										 &nbsp;SafeWork SA documents 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=5892' target="_blank">SafeWorkSA Codes of Practice (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/CoPHazardousManualTasks.pdf' target="_blank">SafeWork SA Hazardous Tasks Safety (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/How_to_Manage_Work_Health_and_Safety_Risks.pdf' target="_blank">Safework SA Work Safety Guide (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/youngWorkersGuide.pdf' target="_blank">SafeWork SA Young Workers Guide (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/CoPFirstAidWorkplace.pdf' target="_blank">SafeWork SA First Aid Workplace Guide (SafeWork SA)</a></li>
								
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/sevenStepsSmallBuiness.pdf' target="_blank">SafeWork SA Seven Step Safety Checklist (SafeWork SA)  </a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/notification.pdf' target="_blank">SafeWork SA Notifiable Incident Report Form (SafeWork SA) </a></li>
								
									</ul>
								</div></div></h4></div></div>
								
							
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Equipment and Vehicle Safety 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/Forklift+Safety+(Safework+SA).pdf' target="_blank">Forklift safety (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/All-terrain+Vehicles+Safety+(WorkSAfe+SA).pdf' target="_blank">All- terrain Vehicles Safety (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
							
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Worker and Personal Health  
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockworkerpersonalhealth/Zoonoses+Disease+Prevention+(BioSecurity+SA).pdf' target="_blank">Zoonoses Disease Prevention (Biosecurity SA) </a></li>
							<li><a href='http://www.beyondblue.org.au/' target="_blank">Beyond Blue (Web link)</a></li>
							<li><a href='http://www.mensshed.org/home/.aspx' target="_blank">Men and Sheds (Web link)</a></li>
							
							
									</ul>
								</div></div></h4></div></div>
								
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;BIOSECURITY<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<h5 style="text-transform:capitalize;"><strong>Safe Chemical Use</strong></h5>		
										<ul>
										<li><a href='http://www.pir.sa.gov.au/__data/assets/pdf_file/0008/235547/PIRSABIOSEC12_DL_COP_Summer_weed_spraying_November25LR.pdf ' target="_blank">Code of Practice - Summer Weed Spraying (PIRSA) pdf</a></li>
							
										</ul>
							
									<h5 style="text-transform:capitalize;"><strong>Animal Health</strong></h5>		
										<ul>
										<li><a href='http://www.pir.sa.gov.au/__data/assets/pdf_file/0013/24106/SA_Notifiable_Diseases_List_24042012.pdf' target="_blank">Livestock Act 1997. SA Notifiable Diseases List (PIRSA) pdf</a></li>
							
										</ul>
										
										<h5 style="text-transform:capitalize;"><strong>Spray Drift Management</strong></h5>		
										<ul>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/WorkingTogether+to+Mininise+Chemical+Spray+Drift+0913+(PIRSA).pdf' target="_blank">Working Together to Minimise Chemical Spray Drift (PIRSA)</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;Land Management web links<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
											
										<ul>
										<li><a href='http://www.pir.sa.gov.au/sarms-iiip' target="_blank">South Australian River Murray Sustainability Program (PIRSA)</a></li>
											<li><a href='http://www.pir.sa.gov.au/newhorizons' target="_blank">New Horizons (PIRSA)</a></li>
										<li><a href='http://www.daff.gov.au/about/budget/budget-2013-14/reforming-drought-programs-factsheet' target="_blank">National Drought Program Reform Factsheet (PIRSA)</a></li>
										<li><a href='http://www.daff.gov.au/agriculture-food/drought' target="_blank">Drought Programs and Rural Assistance webpage (Department of Agriculture, Fisheries and Forestry)</a></li>
										<li><a href='http://www.sardi.sa.gov.au/climate/information_for_farmers' target="_blank">South Australia Research and Development Institute's Climate Support program (PIRSA)</a></li>
									<li><a href='http://www.nrm.sa.gov.au/' target="_blank">Natural Resource Management Boards (PIRSA)</a></li>
										<li><a href='http://www.ruralsolutions.sa.gov.au/markets/community_indigenous_services' target="_blank">Property management planning (PIRSA)</a></li>
									<li><a href='http://www.legislation.sa.gov.au/listPolicies.aspx?key=E' target="_blank">Burn off regulations and dates (EPA)</a></li>
										<li><a href='http://www.cfs.sa.gov.au/site/home.jsp' target="_blank">Fire Bans and Risk Management (EPA)</a></li>
											<li><a href='http://www.environment.sa.gov.au/firemanagement/home' target="_blank">Fire Management weblink (DEWNR)</a></li>
							
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;Water Management web links<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
											
										<ul>
										<li><a href='http://www.pir.sa.gov.au/sarms-iiip' target="_blank">South Australian River Murray Sustainability Program (PIRSA)</a></li>
										<li><a href='http://www.ruralsolutions.sa.gov.au/markets/water_management' target="_blank">Water management (PIRSA)</a></li>
										<li><a href='http://www.legislation.sa.gov.au/index.aspx' target="_blank">Dam building regulations (Attorney General's Dept)</a></li>
											
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Harvesters Code of Practice 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice+CHECKLIST+(1).pdf' target="_blank">Grain Harvesting Code of Practice Checklist (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice_STICKER.pdf' target="_blank">Grain Harvesting Code of Practice Sticker (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Harvesting+Lentils+WARNING.pdf' target="_blank">Harvesting Lentils Warnings (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Licences and permits
										 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<p class="lead">These forms relate to natural resource management.  Whilst the Natural Resource Management Board endeavours to keep the links up to date, some links may occasionally change without notice. </p>
									<h4><strong>Water Management</strong></h4>	
									<p>Water licences - these are required to take water from a prescribed water resource for certain uses or above certain volumes, as specified in the relevant Water Allocation Plan (WAP).  Depending on the relevant WAP:
								<ul> <li> Taking water may include (but not be limited to) pumping, damming, gravity systems and commercial forestry. 
									</li><li> A water resource may include (but not be limited to) groundwater, creeks, rivers, wetlands, lakes and surface run-off.
									</li></ul>
									<p>Forms are specific for each prescribed area in each NRM region:</p>
								<ul>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/adelaide-and-mount-lofty-ranges-water-licences-and-permits' target="_blank">Adelaide and Mt Lofty Ranges </a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/eyre-peninsula' target="_blank">Eyre Peninsula</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/northern-and-yorke' target="_blank">Northern and Yorke</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/sa-arid-lands' target="_blank">SA Arid Lands</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/sa-murray-darling-basin' target="_blank">SA Murray Darling Basin</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/south-east' target="_blank">South East</a></li>
								
									</ul>
								
										<p>Water affecting activity (WAA) permits are required to undertake an activity that may affect a water resource or water dependent ecosystem.  Each NRM Board determines what WAAs require a permit and whether a permit is required for the WAA in all or part of the region.  Some regions have developed 'recommended practices' and 'best practice operating procedures' to reduce red tape.</p>
							<p>For constructing a well, draining/discharging into a well, using imported or effluent water, water meter testing and well drilling licences use the <a href="http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/statewide" target="_blank"> Statewide forms</a></p>
							<p>For other water affecting activities use the regional forms:</p>
							<ul>
								<li><a href='http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/water/managing-water/water-affecting-activities' target="_blank">Adelaide and Mt Lofty Ranges </a></li>
								
								<li><a href='http://www.naturalresources.sa.gov.au/alinytjara-wilurara/water/water-in-the-region/managing-water-resources' target="_blank">Alinytjara Wilurara </a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/eyrepeninsula/land-and-water/water-affecting-activities' target="_blank">Eyre Peninsula</a></li>
								<li><a href="http://www.naturalresources.sa.gov.au/kangarooisland/land-and-water/water-management/water-affecting-activities" target="_blank">Kangaroo Island</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/northernandyorke/water/water-affecting-activities' target="_blank">Northern and Yorke</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/aridlands/water/water-affecting-activities' target="_blank">SA Arid Lands</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/samurraydarlingbasin/water/managing-water-resources/water-affecting-activities' target="_blank">SA Murray Darling Basin</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/southeast/water/water-affecting-activities' target="_blank">South East</a></li>
								
									</ul>
									<p><ul><li><a href='http://www.environment.sa.gov.au/managing-natural-resources/water-use/water-planning' target="_blank">More information about water resource management </a></li></ul></p>
							
							<h4><strong>Land Management</strong></h4>	
							<h5><strong>Native Vegetation</strong></h5>	
							<ul>
								<li><a href='http://www.environment.sa.gov.au/files/05e1a003-b7db-456a-9712-9efa00f8c536/con-nv-form-clearanceapplication.pdf' target="_blank">Application to clear native vegetation </li>
								<li><a href='http://www.environment.sa.gov.au/files/2c78a1d2-74fa-4547-8383-9efa00f8ce00/con-nv-guideline-agriculture.pdf' target="_blank">Guide to native vegetation regulations for agriculture</a></li>
								<li><a href="http://www.environment.sa.gov.au/managing-natural-resources/Native_vegetation/Managing_native_vegetation" target="_blank">Other information about managing native vegetation in South Australia</a></li>
								<li>Nationally protected communities and species</li>
								</ul><p>
								Some ecological communities, heritage places and species of plants and animals are protected nationally under the<a href="http://www.environment.gov.au/epbc/about" target="_blank"> Environment, Protection and Biodiversity Conservation Act 1999</a> (EPBC Act). 
								There is an <a href="http://www.environment.gov.au/epbc/protected-matters-search-tool" target="_blank">interactive map-based search tool</a> on the Department of the Environment website to search for matters of national significance that may occur in your area of interest.  Non-marine ecological communities currently protected under the EPBC Act in South Australia (as of 23/02/15) include, but may not be limited to:
								</p>
								<ul>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=3' target="_blank">Buloke Woodlands of the Riverina and Murray-Darling Depression Bioregions </li>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=86&status=Endangered' target="_blank">Grey Box (Eucalyptus microcarpa) Grassy Woodlands and Derived Native Grasslands of South-eastern Australia</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=124&status=Endangered" target="_blank">Eyre Peninsula Blue Gum (Eucalyptus petiolaris) Woodland</a></li>
								<li>Iron-grass Natural Temperate Grassland of South Australia</li>
								<li>Kangaroo Island Narrow-leaved Mallee (Eucalyptus cneorifolia) Woodland</li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=36" target="_blank">Peppermint Box (Eucalyptus odorata) Grassy Woodland of South Australia</a></li>
								
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=118' target="_blank">Subtropical and Temperate Coastal Saltmarsh</li>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=31' target="_blank">Swamps of the Fleurieu Peninsula</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=26" target="_blank">The community of native species dependent on natural discharge of groundwater from the Great Artesian Basin </a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/wetlands/alphablist.pl" target="_blank">Wetlands of international significance</a></li>
								
								</ul>
								
								<h5><strong>Wildlife</strong></h5>	
						<ul>
								<li><a href='http://www.environment.sa.gov.au/files/37da2e46-09af-4ab3-acf3-9e6100c2916f/PermitApp_destroywildlife.pdf' target="_blank">Permit to destroy wildlife</li>
								<li><a href='http://www.environment.sa.gov.au/files/3d447eb6-8038-446f-acd7-a40a00e6247c/hunt-duck-permit-application-2015-form.pdf' target="_blank">Duck hunting permit</a></li>
								<li><a href="http://www.environment.sa.gov.au/Plants_and_Animals/Animal_welfare/Animal_welfare_legislation/Codes_of_practice_for_the_humane_destruction_of_wildlife_" target="_blank">Codes of practice for the humane destruction of wildlife </a></li>
								<li><a href="http://www.environment.sa.gov.au/licences-and-permits/Animals_in_the_wild_permits" target="_blank">Other wildlife-related permits and information</a></li>
								
						</ul>
						<p><ul><li><a href="http://www.environment.sa.gov.au/licences-and-permits" target="_blank">Other licences and permits administered by DEWNR</a></li></ul>
							</div></div></h4></div></div></div></div></div></div></div>
								</div></div></div></div></div></div></div></div></div></div></div></div>
							<div id="eventsTab" class="tab-pane">
								<div id="cal">
									<a href="javascript:showList()" class="btn btn-secondary" onclick="_gaq.push(['_trackEvent', 'Events View List', 'Click', 'View List']);"><h4>View List</h4></a>
									<div id='loading'><h4>loading...</h4></div>
									<div id='calendar'></div>
								</div>					
								<ul id='listOfEvents' class="history" style="display:none;">
									<a href="javascript:showList()" class="btn btn-secondary" onclick="_gaq.push(['_trackEvent', 'Events View Calendar', 'Click', 'View Calendar']);"><h4>View Calendar</h4></a>
									<table id="eventsTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
										<thead>
											<tr>
												<th>Icon</th>
												<th style="width:120px;"><span><i class="fa fa-calendar"></i> Date</span></th>
												<th style="width:180px;"><i class="fa fa-clock-o"></i> Time</span></th>
												<th>Title</th>
												<th>Address</th>
												<th>Cost</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="act" items="${activities}" varStatus="row">
												<tr>
													<td><a href="<c:url value='/member/event/${act.id}'/>"><img src="${act.iconUrl}" width="25" height="25" 
														onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
													<td>${act.startDate}</td>
													<td>${act.rangeTime}</td>
													<td><a class="infosframe" href="<c:url value='/member/event/${act.id}'/>" onclick="_gaq.push(['_trackEvent', 'Event Title', 'Click', 'Content Title']);">${act.contentTitle}</a></td>
													<td>${act.event.address}</td>
													<td>${act.event.cost}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</ul>
							</div>
			
							<div id="surveysTab" class="tab-pane">
								<!-- Transport -->
								<div class="widget">
									<div class="widget-extra themed-background-dark">
										<div class="widget-options">
											<div class="btn-group btn-group-xs">
												<a target="#"
													href="http://survey.digitalmarketsquare.com/lime/limesurvey/index.php/185635/lang-en"
													class="btn btn-default" data-toggle="tooltip" title=""
													data-original-title="Open in a new window"><i
													class="fa fa-share-square-o"></i></a>
											</div>
										</div>
										<h4 class="widget-content-light">Surveys</h4>
									</div>
									<div class="widget-extra-full">
									<p><p><p><p><p><p><p><p>
									 <h4>Rural Connect publishes surveys designed to support your farming business and way of life.</h4>
									<h4>Visit the Pinboard menu to see the type of surveys which have attracted interest from our sector.</h4>
									<!-- 
									  
										<div class="row rr-margin-5">
											<iframe src="http://survey.digitalmarketsquare.com/lime/limesurvey/index.php/724522/lang-en" onclick="_gaq.push(['_trackEvent', 'Surveys', 'Click', 'Surveys']);"
												width="100%" frameborder="0" scrolling="auto" height="600px">
											</iframe>
										</div>
										 -->
									</div>
								</div>
								<!-- END Transport -->
							</div>
							
							<div id="weatherTab" class="tab-pane" >
								
								<!--  <div class="row">
											 <div class="  col-med-8 col-md-3">
								                <div class="featured-box featured-box-secundary">
								                    <div class="box-content">
								                        <H4>SA WEATHER</H4>						                   
								                        <div class="row text-center">
								                            <div class="col-xs-5 col-sm-3">
								                                <span id="spnTempMax"></span><br>
								                                <small>Temp Max</small>
								                            </div>
								                            <div class="col-xs-5 col-sm-5">
								                                <strong><span id="spnHumidity"></span></strong><br>
								                                <small><i class="fa fa-cloud"></i>Humidity</small>
								                            </div>
								                            <div class="col-xs-5 col-sm-4">
								                                <strong><span id="spnWindSpeed"></span></strong><br>
								                                <small>Wind Speed</small>
								                                <div id="dvFireAlertModal" style="display:none"></div>
								                            </div>
								                        </div> 
								                    </div>
								                    <button class="btn btn-primary btn-small push-top push-bottom" data-toggle="modal" data-target="#myModal">
								                        View weather detail...
								                    </button>
								                </div>
								            </div> 
										</div>-->
								<div class="row" >
									<!-- SAM WEATHER -->
								
								</div>
									<div class="timeline-time">
									<small style="text-align:left;"><strong>Rural Connect weather display:</strong> Ongoing work is being done with the Bureau of Meteorology to improve access to relevant and useful weather information via Rural Connect.</small></h6>		
			</div>
			<hr>
								<div class="panel-group" id="accordion" >
									<div class="panel panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion"
													href="#collapseAl" onclick="_gaq.push(['_trackEvent', 'Radar', 'Click', 'Weather Forecast']);"><li class="fa fa-chevron-circle-right"></li> South Australia Forecast Area Map</a>
											</h4>
										</div>
										<div id="collapseAl" class="panel-collapse collapse" role="tabpanel">
											<div class="panel-body">
												<div class="embed-responsive embed-responsive-4by3">
													<iframe class="embed-responsive-item"
														src="http://www.bom.gov.au/sa/forecasts/map7day.shtml"></iframe>
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion"
													href="#collapse2" onclick="_gaq.push(['_trackEvent', 'Radar', 'Click', 'Weather Radar']);"><li class="fa fa-chevron-circle-right"></li> Live Radar display for SA</a>
											</h4>
										</div>
										<div id="collapse2" class="panel-collapse collapse" role="tabpanel">
											<div class="panel-body">
												<div class="embed-responsive embed-responsive-4by3">
													<iframe class="embed-responsive-item"
														src="http://www.bom.gov.au/products/IDR641.loop.shtml"></iframe>
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion"
													href="#collapse3" onclick="_gaq.push(['_trackEvent', 'Observations', 'Click', 'Weather observations']);"><li class="fa fa-chevron-circle-right"></li> Search SA Regional Observations</a>
											</h4>
										</div>
										<div id="collapse3" class="panel-collapse collapse" role="tabpanel">
										<section class="panel" style="overflow: visible;">
																					
												<div class="panel-body">  
											    
													<table class="table table-striped table-bordered table-hover table-condensed compact"  data-height="400" data-pagination="true" data-search="true" >
												        <thead>
												        <tr>
												         <th colspan="3">	<div class="input-group">  </th>
												    		<th  colspan="16"  class="info">	<input id="filter" type="text" class="form-control" placeholder="SEARCH: Type in your region ..."></th>
															</div>    
												        </tr>
												          <tr>
												            <th>Area</th>
												            <th>Date/Time</th>
												            <th>Temp &deg;C</th>
												            <th>App Temp &deg;C</th>
												            <th>Dew Point &deg;C</th>
												            <th>Rel Hum %</th>
												          <!--   <th>Delta T &deg;C</th> -->
												            <th colspan=5>Wind</th>												            
												           <!--  <th>Press hPa</th>  -->
												            <th>Rain since 9 am mm</th>
												            <th>Low Temp &deg;C time</th>
												            <th>High Temp &deg;C time</th>
												            <th colspan=3>High Wind Gust</th>
												          </tr>
												          <tr>
												            <th colspan=7></th>
												          	<th>Dir</th>
												            <th>Speed Kmph</th>
												            <th>Gust Kmph</th>
												            <th>Speed Kts</th>
												            <th>Gust Kts</th>
												            <th colspan=4></th>
												           <!--   <th>Dir</th> -->
												          <!--   <th>Kmph time</th>  -->
												          <!--   <th>Kts time</th>  -->
												          </tr>
												        </thead>
												        <tbody  class="searchable">
												        <c:forEach var="listValue" items="${weatherobs}">
												        <c:choose>
												        	<c:when test="${listValue.windDirection =='-1'}">
												        	 <tr colspan="13" class="info"> <td>${listValue.location}</td></tr>
												        	</c:when>
												        <c:otherwise>
															  <tr> <td>${listValue.location}</td>
															  <td>${listValue.updatedTime}</td>
															  <td>${listValue.temperature}</td>
															  <td>${listValue.appTemperature}</td>
															  <td>${listValue.dewPointTemp}</td>
															  <td>${listValue.relHumidity}</td>
															<!--   <td>${listValue.deltaTemp}</td>  -->
															  <td>${listValue.windDirection}</td>
															  <td>${listValue.speedKmph}</td>
															  <td>${listValue.gustKmph}</td>
															  <td>${listValue.speedKts}</td>
															  <td>${listValue.gustKts}</td>
															 <!-- <td>${listValue.pressure}</td>  -->
															  <td>${listValue.rain}</td>
															  <td>${listValue.lowTemp}</td>
															  <td>${listValue.highTemp}</td>
															  <td>${listValue.gustDirection}</td>
															<!--  <td>${listValue.gustSpeedKmph}</td> -->
															<!--  <td>${listValue.gustSpeedKts}</td></tr>  --> 
															</tr>
															</c:otherwise>
															</c:choose>
														</c:forEach>
												        </tbody>
													</table>
												</div>
										</section>
										
										</div>
									</div>
					
								<div class="panel panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#sevenDayForecast"><li class="fa fa-chevron-circle-right"></li> 7 Day Forecast  Adelaide Metropolitan Region</a>
											</h4>
										</div>
										<div id="sevenDayForecast" class="panel-collapse collapse" role="tabpanel">
											<div class="panel-body">
											    <div class="table-responsive">          
											    <table class="table">
											      <thead>
											        <tr>
											        <c:forEach var="listValue1" items="${weatherforecast}">
											          <th><p class="text-center"><fmt:formatDate value="${listValue1.weatherDate}" pattern="EEEE dd/MM/yyyy" /></p></th>
											        </c:forEach>
											        </tr>
											      </thead>
											      <tbody>
											        <tr>
											        <c:forEach var="listValue1" items="${weatherforecast}">
											          <td>
														<div class="panel panel">
														  <div class="panel-heading">
														    <p class="lead"><img src="${listValue1.imgUrl}" class="img-thumbnail" alt="Thumbnail Image">
															<strong> <kbd>${listValue1.maxTemperature} &degC </kbd></strong></br> 
															<p class="text-nowrap">${listValue1.weatherData}</br> <small>Low  ${listValue1.minTemperature} &degC</small></p> </p>
														  </div>
														</div>
													  </td>
											  		</c:forEach>
											        </tr>
											      </tbody>
											    </table>
											    <ul class="list-inline">
												  <li><strong> Current Wind Direction : </strong></li>
												  <li><small>${adelaideDirection}</small></li>
												  <li><strong> Wind Speed : </strong></li>
												  <li><small>${adelaideWind} kmph</small></li>
												  <li><strong> Gust Speed: </strong></li>
												  <li><small>${adelaideGust} kmph</small></li>
												</ul>
											    </div>
											</div>
										</div>
									</div>
									<div class="panel panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion"
													href="#collapse1" onclick="_gaq.push(['_trackEvent', 'Rainfall Forecast', 'Click', 'Weather Rainfall forcast']);"><li class="fa fa-chevron-circle-right"></li> Rainfall Observations</a>
											</h4>
										</div>
										<div id="collapse1" class="panel-collapse collapse" role="tabpanel">
											<div class="panel-body">
													<img src="http://www.bom.gov.au/fwo/IDS65101.gif" class="img-thumbnail" 
														alt="Thumbnail Image">
											</div>
										</div>
									</div>
									
												</div>
							</div>
							
							<!-- END SAM WEATHER - Payments -->
							
							<div id="paymentsTab" class="tab-pane" >
								<div class="panel-group" id="accordion" >
									<div class="panel panel">
<!-- 										<div class="panel-heading"> -->
<!-- 											<h4 class="panel-title"> -->
<!-- 												<a data-toggle="collapse" data-parent="#accordion" -->
<!-- 													href="#collapsePl" onclick="_gaq.push(['_trackEvent', 'Radar', 'Click', 'Weather Forecast']);"><li class="fa fa-chevron-circle-right"></li> Renew Motor Vehicle Registration</a> -->
<!-- 											</h4> -->
<!-- 										</div> -->
										
				
						<h2>
							<strong> Forms</strong>
						</h2>
								<style>
						.accordion_container { width: 1140px; } 
						.accordion_head {  color: #03ad5b; cursor: pointer; font-family:sans-serif; font-size: 16px; margin: 0 0 1px 0; padding: 10px 15px; font-weight: bold; display:block;} 
						.accordion_body {background-color:white;} 
						.accordion_body a {padding-right:0px;padding-top:0px;padding-left:0px;padding-bottom:0px;;font-family:sans-serif;font-size:14px; } 
						.accordion_body a:hover{text-decoration:underline;}
						.plusminus { float:left; } 
						</style>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Renew Motor Vehicle Registration
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									
										<ul>
											<li>		<a href="https://www.ecom.transport.sa.gov.au/et/renewARegistration.do"	target="_blank">https://www.ecom.transport.sa.gov.au/et/renewARegistration.do</a>		</li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Check Registration Expiry Date
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									
										<ul>
											<li> <a href="https://www.ecom.transport.sa.gov.au/et/checkRegistrationExpiryDate.do" target="_BLANK">https://www.ecom.transport.sa.gov.au/et/checkRegistrationExpiryDate.do</a> 
												
 									</li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;SafeWork SA Codes of Practice
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									
										<ul>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113693#.VLL9bCuUd8F	' target="_blank">First Aid in the Workplace (SafeWork SA)  </a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113694#.VLL92SuUd8F' target="_blank">Hazardous Manual Tasks (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113695#.VLL-GyuUd8F' target="_blank">How to Manage Work Health and Safety Risks (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113699#.VLMCiCuUd8F' target="_blank">Managing Noise and Preventing Hearing Loss at Work (SafeWork SA) </a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113700#.VLMC3CuUd8F' target="_blank">Managing the Risks of Plant in the Workplace (SafeWork SA) </a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113702#.VLMDFyuUd8F' target="_blank">Managing Electrical Risks in the Workplace (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113704#.VLMDMyuUd8F' target="_blank">Managing the Work Environment and Facilities (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113705#.VK94zSuUeSo' target="_blank">Preparation of Safety Data Sheets for Hazardous Chemicals (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113708#.VK944CuUeSo' target="_blank">Work Health and Safety Consultation Cooperation and Coordination (SafeWork SA)</a></li>
											
											</ul>
									</div>
							</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Injury Report Forms
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									
										<ul>
<!-- 											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+Capacity+information+form+(1)++FORMS.doc' target="_blank">Basic template forms for reporting injury to doctors and medical release forms</a></li> -->
												<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+WC+Letter+to+Doctor+(1)++FORMS.doc' target="_blank">Form for doctor </a></li> 
<!-- 											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+WC+Medical+release+authority+(1)+FORMS.doc' target="_blank">Medical Release form </a></li> -->
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Work Safety Guide
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									<ul>							
											<li><a href='http://www.safework.sa.gov.au/uploaded_files/20131028_seven_steps_small_business.pdf' target="_blank">Seven steps for small busines (SafeWork SA)</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Machinery and Tools
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
																	
											<ul>
												<li><a href="http://www.safework.sa.gov.au/uploaded_files/sgAll-terrainVehicles.pdf" target="_blank">Agricultural motorbikes (SafeWork SA) </a></li>
												<li><a href="http://www.safework.sa.gov.au/uploaded_files/sgAngleGrinders.pdf" target="_blank">Angle grinder safety (SafeWork SA)</a></li>
												<li><a href="https://drive.google.com/viewerng/viewer?url=http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf" target="_blank">Disc grinder in the shearing industry (SafeWork SA)</a></li>
												<li><a href="http://www.safework.sa.gov.au/uploaded_files/Take10at10Forkliftsafety.pdf" target="_blank">Forklift safety (SafeWork SA)</a></li>
												<li><a href="http://www.safework.sa.gov.au/uploaded_files/sgBowLadders.pdf" target="_blank">Ladder safety (SafeWork SA)</a></li>
											</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Chemicals
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
																
											<ul>
												<li><a href="http://www.safework.sa.gov.au/show_page.jsp?id=113698#.VLMCMiuUd8F" target="_blank">Labelling of Workplace Hazardous Chemicals (SafeWork SA)</a></li>
												<li><a href="http://www.safework.sa.gov.au/show_page.jsp?id=113701#.VLMC9SuUd8F" target="_blank">Managing Risks of Hazardous Chemicals in the Workplace (SafeWork SA)</a></li>
												<li><a href="http://www.safework.sa.gov.au/show_page.jsp?id=112871#.VQjUeWSUdK4" target="_blank">Pesticide (Chemical) safety (SafeWork SA)</a></li>
												</ul>
									</div>
							</div>
									</h4>
								</div></div>
								
<!-- 									<div class="panel-group"> -->
<!-- 							<div class="panel panel-default"> -->
<!-- 								<div class="panel-heading"> -->
<!-- 									<h4 class="panel-title"> -->
<!-- 										<div class="accordion_head"> -->
<!-- 												&nbsp;Spray drift -->
<!-- 											<span class="plusminus">+</span> -->
<!-- 										</div> -->
<!-- 											<div class="accordion_body" style="display: none;"> -->
<!-- 												<div class="panel-body"> -->
															
<!-- 											<ul> -->
<!-- 												<li><a href="http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockMers/RC+BIOSECURITY+Reducing_Spray_Drift_and_Damage_-_Checklist.pdf" target="_blank">Reducing Spray drift and damage Checklist (Biosecurity SA) </a></ul> -->
<!-- 									</div> -->
<!-- 							</div> -->
<!-- 									</h4> -->
<!-- 								</div></div> -->
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											&nbsp;BUSINESS <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">

												<ul>
														<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/forms/Plant+Export+Operations+Stakeholder+Registration+Form+(Dept+of+Agriculture).pdf' target="_blank">Plant Export Operations Stakeholder Registration Form (Dept of Agriculture)</a></li>
									
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+Guide+2015+pdf.pdf' target="_blank"> Farm Gross Margin Guide 2015 (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+and+Enterprise+Planning+Guide+2012.pdf' target="_blank"> Farm Gross Margin and Enterprise Planning Guide 2012 (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farming+the+Business+introductory+guide.pdf' target="_blank"> Farming the Business Introductory Guide (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/Key+financial+ratios+for+farm+sustainability+Grains+Research+and+Development+Corp.pdf' target="_blank">Key financial ratios for farm sustainability Grains Research and Development Corporation (GRDC)</a></li>
											<li><a href='http://www.pir.sa.gov.au/consultancy/major_programs/new_horizons' target="_blank"> New Horizons soil and productivity program (PIRSA)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockbusiness/Mineral+exploration+and+faming+booklet+pdf+(Dept+State+Development).pdf' target="_blank">Mineral exploration and faming booklet  (Dept State Development)</a></li>
													<li><a href=' http://www.pir.sa.gov.au/primary_industry/crops_and_pastures/crop_and_pasture_reports' target="_blank">Crop and Pastures Report (PIRSA live link)</a></li>
										
												</ul>
											</div>
										</div>
									</h4>
								</div>
							</div>
							
							
							</div>
								
								</div></div></div></div></div>
											</div></div>
										</div>
									</div>
<!-- 									<div class="panel panel"> -->
<!-- 										<div class="panel-heading"> -->
<!-- 											<h4 class="panel-title"> -->
<!-- 												<a data-toggle="collapse" data-parent="#accordion" -->
<!-- 													href="#collapseP2" onclick="_gaq.push(['_trackEvent', 'Radar', 'Click', 'Weather Radar']);"><li class="fa fa-chevron-circle-right"></li> Check Registration Expiry Date</a> -->
<!-- 											</h4> -->
<!-- 										</div> -->
<!-- 										<div id="collapseP2" class="panel-collapse collapse" role="tabpanel"> -->
<!-- 											<div class="panel-body"> -->
<!-- 												<a href="https://www.ecom.transport.sa.gov.au/et/checkRegistrationExpiryDate.do" target="_BLANK">https://www.ecom.transport.sa.gov.au/et/checkRegistrationExpiryDate.do</a> -->
<!-- 												<div class="embed-responsive embed-responsive-4by3">
												
													<iframe class="embed-responsive-item"
														src="https://www.ecom.transport.sa.gov.au/et/renewARegistration.do"></iframe>
												</div> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</div> -->
									
					
								<script>
						$(document).ready(function () {
						    //toggle the component with class accordion_body
						    $(".accordion_head").click(function () {
						        if ($('.accordion_body').is(':visible')) {
						            $(".accordion_body").slideUp(300);
						            $(".plusminus").text('+');
						        }
						        if ($(this).next(".accordion_body").is(':visible')) {
						            $(this).next(".accordion_body").slideUp(300);
						            $(this).children(".plusminus").text('+');
						        } else {
						            $(this).next(".accordion_body").slideDown(300);
						            $(this).children(".plusminus").text('-');
						        }
						    });
						});
						
</script>
								
									
												</div>
							</div>
										
								<!-- Forms Tab -->
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>

	<hr class="tall" />

	<div class="row center">
		<div class="col-md-12">
			<h2 class="short word-rotator-title">
			<!-- 	We're not the only ones <strong> <span
					class="word-rotate"
					data-plugin-options='{"delay": 3500, "animDelay": 400}'> <span
						class="word-rotate-items"> <span>excited</span> <span>happy</span>
					</span>
				</span>
				</strong> about Rural Connect... -->
			</h2>
			<!--  <h4 class="lead tall">more than 5,500 farmers use it. Meet our
				partners-->Rural Connect Partners</h2>
		</div>
	</div>
	<div class ="container">
	 <div class="row center"> 
	 <div class="col-md-12">
            <div class="owl-carousel" data-plugin-options='{"items": 4, "autoplay": false, "autoplayTimeout": 3000}'>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/primesuper_bi_2BW.jpg'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-2.png'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-3.png'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-4.png'/>" alt="">
                </div>

               </div>
            </div>
        </div>
        </div>
				

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
		style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title" id="myModalLabel">Start Connecting Now!</h4>
				</div>
				<div class="modal-body center middle">
					<img src="<c:url value='/ruralAssets/img/placements/signup_icon.png'/>" />
				</div>
				<div class="modal-footer">
					<a href="<c:url value='/register'/>"><button type="button" class="btn btn-primary">Register now for free</button></a>
				</div>
			</div>
		</div>
	</div>

	<div class="widget" style="display: none">
		<div id="dvCurrencyContainer"><i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading</div>
		<div align="center" style="margin-top: 30px;" id="dvCurrencySource">
			<div align="center" style="margin: 0px; padding: 0px; border: 2px solid rgb(136, 136, 136); width: 135px; 
				background-color: rgb(255, 255, 255);">
				<div align="center" style="width: 100%; border-bottom-width: 1px; border-bottom-style: solid; 
					border-bottom-color: rgb(136, 136, 136); margin: 0px; padding: 0px; text-align: center; color: rgb(0, 0, 0); 
					background-color: rgb(160, 192, 48);">
					<a href="http://fx-rate.net/AUD/" style="text-decoration: none; font-size: 14px; font-weight: bold; text-align: center; color: rgb(0, 0, 0);">
					<img src="http://fx-rate.net/images/countries/au.png" style="margin: 0px; padding: 0px; border: none;">
					Australian Dollar Exchange Rate</a>
				</div>
				<script type="text/javascript" 
					src="http://fx-rate.net/fx-rates2.php?label_name=Australian Dollar Exchange Rate&lab=1&width=135&currency=AUD&cp1_Hex=000000&cp2_Hex=FFFFFF&cp3_Hex=a0c030&hbg=0&flag_code=au&length=short&label_type=currency&cp=000000,FFFFFF,a0c030&cid=EUR,CNY,USD&lang=en">
				</script>
			</div>
		</div>
	</div>

	<div id='dvGMap'></div>
	
	

	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
	
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<c:url value='/ruralAssets/js/helpers/gmaps.min.js'/>"></script>
	<script src="<c:url value='/ruralAssets/myJS/custom/radio.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/pages/index.js'/>"></script>
	
	<script>
	$(document).ready(function () {

	    (function ($) {

	        $('#filter').keyup(function () {
				console.log("this keyedup");
	            var rex = new RegExp($(this).val(), 'i');
	            console.log(rex);
	            $('.searchable tr').hide();
	            $('.searchable tr').filter(function () {
	                return rex.test($(this).text());
	            }).show();

	        })

	    }(jQuery));

	});
	</script>

	<script>
	
	function list(){
			 
			$('#radioHide').toggle();
			$('#prevIcon').toggleClass("fa-chevron-down");
			$('#prevIcon').toggleClass("fa-chevron-up");
	}
		$(document).ready(function() {
			
			$('#HomeOption').addClass("active");
			
			$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
			$(".infosframe").colorbox({iframe:true, width:"70%", height:"80%"});
			
			Index.initHome();
		
			  $('#calendar').fullCalendar({
				theme : true,
				header : {
					left : 'prev,next today',
					center : 'title',
					right : 'month,agendaWeek,agendaDay'
				},

				editable : false,
				eventLimit : true, // allow "more" link when too many events
				events : {
					url : '<c:url value="/api/myEvents"/>'
				},
				loading : function(bool) {
					$('#loading').toggle(bool);
				},
				 eventClick: function(calEvent, jsEvent, view) {

				       /// alert('Event: ' + calEvent.title);
				      //  alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
				     //  alert('View: ' + view.name);
				     var url = "<c:url value='/member/event/'/>" + calEvent.id;
				      //  window.location.href = "<c:url value='/member/event/'/>" + calEvent.id;
				      //  $(this).css('border-color', 'red');
					 $.colorbox({iframe:true, width:"70%", height:"80%", href:url});
				    }
			});
			  
			  
			  $('#eventList').click(function () { 
				  console.log("hit");
					  setTimeout(function(){ $('#calendar').fullCalendar('render');},2000);
				});
			  
			  
			  $('.tabs').bind('selected', function (event) { 
				  if(event.args.item=="3") {
					 // console.log("hit");
				    jQuery('#calendar').fullCalendar('render');
				  }
				});
			  

		});
		
		function showList(){
			
			console.log("ShowList");
			$('#cal').toggle();
			$("#listOfEvents").toggle();
		}
		
	</script>

	<script src="<c:url value='/ruralAssets/myJS/vendor/bootbox.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/theme.js'/>"></script>



</body>
</html>
<!-- end: page -->
