<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- start: page -->
<html>

<body>

	<div role="main" class="main">
		<section class="page-top">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1>
							<img alt="Rural Connect" height="60"
								src="<c:url value='/ruralAssets/img/icons/porkIcon.png'/>">
							PORK SA
						</h1>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="/rural/member/partners" class="btn btn-lg btn-primary">Meet our commercial partners</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="container">
			<ul class="nav nav-pills">
				<!-- 				<li class="active pork" id="viewAllPorkTab"><a href="javascript:porkViewAll()">View All</a></li> -->
				<!-- 				<li class="pork" id="aboutPorkTab"><a href="javascript:aboutPork()">About Pork SA</a></li> -->
				<c:if test="${gatedPork != 'granted'}">
					<li class="pork" id="aboutPorkTab"><a
						href="javascript:aboutPork()">About Pork SA</a></li>
					<li class="pork" id="porkCommitteeTab"><a
						href="javascript:porkCommittee()">Pork SA Committee</a></li>
					<li class="pork" id="porkMembershipTab"><a
						href="javascript:porkMembership()">Charter</a></li>
						<li class="pork" id="grainWorkTab"><a href="javascript:grainWork()">Emergency Management </a></li> 
				</c:if>
							
				<c:if test="${gatedPork == 'granted'}">
					<li class="pork" id="porkCommTab"><a href="javascript:porkComm()">Pinboard</a></li>
					<li class="pork" id="porkEventsTab"><a href="javascript:porkEvents()">Events</a></li>
					<!-- <li class="pork" id="porkKeyTab"><a href="javascript:porkKeyContacts()">Key Contacts</a></li> -->
					<li class="pork" id="porkBusinesTab"><a
						href="javascript:porkBusiness()">Biosecurity</a></li>
					<li class="pork" id="porkBioSecurityTab"><a
						href="javascript:porkBioSecurity()">Business</a></li>
							<li class="pork" id="porkFormTab"><a
						href="javascript:porkForm()">Forms</a></li>
					<li class="pork" id="porkLegalTab">				<a href="javascript:porkLegal()">Transport</a></li>
					<li class="pork" id="porkSurveyTab">
					<li class="pork" id="porkOtherTab">				<a href="javascript:grainTraining()">Training & Education</a></li>
					<li class="pork" id="grainIndustrailTab"><a href="javascript:grainIndustrail()"> Workplace</a></li>
					
				</c:if>
			</ul>
			<hr/>

			<div id="porkContent">
			
				<div class="row" id="porkAbout">
					<div class="col-sm-7">
						<h2>
							Pork <strong>SA</strong>
						</h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<p class="tall" style="text-align: justify">In 2005 - 2006, the
							South Australian livestock industry contributed $2.7 billion or
							27% of South Australia's gross food revenue. The SA pig meat
							industry contributed $573 million or 21% of the total South
							Australia has about 550 pig farmers with the main pig producing
							regions the Murray lands, Barossa, Yorke, and Mid North and
							Northern. Membership of Pork SA is free to producers who pay
							levies into the Pig Industry Fund. Producer members are also able
							to nominate associate members, again with no joining fee, in the
							anticipation that farm managers and staff will join Pork SA and
							keep informed about Agribusinesses who supply and support SA pork
							producers, such as feedmillers, premix manufacturers and
							processors, can join as members upon payment of a nominal fee.

							Under the terms of incorporation, Pork SA is required to maintain
							its own register of members. Pork SA recognises that while
							profitability is critical, it is just as important to have the
							confidence of the community and consumers in how responsibly the
							industry operates in areas such as welfare, environmental
							sustainability and food safety. Pork SA will specifically address
							such matters and enable the industry to be ahead of the issues
							rather than reacting in an uncoordinated manner.</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a class="btn btn-primary"
							target="_blank" href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf">Pork
							SA Membership Form (live link)</a> </br> </br> <a class="btn btn-primary"
							target="_blank" href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf">Pork
							SA Charter (live link)</a> </br> </br> Membership forms are also available from
						Andy Pointon, Pork SA Executive Officer <a
							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
						or on 0418 848 845
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive"
							src="https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/rc_pigs.png">
					</div>
				</div>


				<div id="porkCommittee">
					<div class="row pork">
						<div class="col-sm-7">
							<h2>
								Committee<strong> 2014</strong>
							</h2>
							<p class="lead">
							<h3>Pork SA Committee for 2014</h3>
							<ul>
								<li>Chair: Matthew Starick</li>
								<li>Vice Chairs: Andrew Johnson and Mark McLean</li>
								<li>Secretary/Treasurer: Tony Richardson</li>
								<li>Executive Committee: Barry Lloyd and Peter Brechin</li>
								<li>Committee Members: Garry Tiss, Nick Lienert, David Reu,</li>
								<li>Butch Moses, Rod Hamann, Christine Sapwell.</li>
							</ul>
							</p>
							<p class="tall" style="text-align: justify"></p>
						</div>
						<div class="col-sm-4 col-sm-offset-1 push-top">
							<a
								href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf">Pork
								SA Membership Form (live link)</a> <a
								href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf">Pork
								SA Charter (live link)</a> Membership forms are also available from
							Andy Pointon, Pork SA Executive Officer <a
								href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
							or on 0418 848 845
						</div>
						<div class="col-sm-4 col-sm-offset-1 push-top">
							<img class="img-responsive"
								src="<c:url value='/ruralAssets/img/placements/committee.jpg'/>">
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>

							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Andrew<strong> Johnson: </strong>
							</h2>
							<h4>Vice Chair</h4>
							<p style="text-align: justify;">Contact M: 0427849467 email:
								asjohnson.mtb@bigpond.com Piggery/Business profile� Director of
								Mt. Boothby Pastoral a mixed family farming operation spread
								over multiple sites in SA, producing beef, lamb, wool, mixed
								cropping, lucerne seed and have a multi-site farrow to finish
								pork production operation.Andrew holds several positions in
								industry bodies both at the state and nationally. These include:
							</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Pork SA Vice Chairman</li>
								<li><i class="fa fa-check"></i> APL Director</li>
								<li><i class="fa fa-check"></i> PPSA Councillor</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> APL Animal Welfare and
									Quality Assurance Committee Chairman</li>
								<li><i class="fa fa-check"></i> Vice Chairman Nuffield
									Australia</li>
							</ul>

						</div>
					</div>
					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Mark<strong> McLean: </strong>
							</h2>
							<h4>Vice Chair</h4>
							<p style="text-align: justify;">Contact M: 0427 138 919
								Email: mclean_mark@bigpond.com Piggery/Business profile - Owner
								and General Manager of Riverhaven Enterprises, 700 sow farrow to
								finish pig farm combined with horticultural interests.
								Riverhaven Enterprises is a family farm which has been operating
								in the Riverland for over 50 years. The business operates using
								principles of sustainability and resource re-use to complement
								its various operations. It composts straw-based pig shelter
								bedding which is sold externally and used on its Citrus and
								Olive orchards to provide nutrient value and increase soil water
								holding capacity to reduce water requirements. The company
								constantly review procedures and production techniques to try
								and remain competitive and achieve good outcomes for our pork
								quality. Modern housing and innovation with genetics and
								husbandry are vital to ensure long-term pig farm profitability.
								The family has been co-founders of Top Multiplier, 1,000 sow
								farrow to finish pig unit at Bower. Mark has wide industry
								interests including:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Commitment to assist in
									helping achieve a sustainable future for all Australian pig
									farmers</li>
								<li><i class="fa fa-check"></i> Commitment to see
									Australian pork consumption grow and the national industry
									remain profitable</li>
								<li><i class="fa fa-check"></i> Improving the image of
									farming and encourage future generations to participate</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> Overseas travel to
									investigate different methods of farming and food marketing *
									APL Delegate since 2010</li>
								<li><i class="fa fa-check"></i> Committee Member of APL
									Specialist Group 1 â âMarketing, Supply Chain and Product

									Qualityâ.</li>
							</ul>

						</div>
					</div>
					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Tony <strong>Richardson</strong>
							</h2>
							<h4>Secretary/Treasurer: Tony</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Barry Lloyd <strong>and Peter Brechin</strong>
							</h2>
							<h4>Executive Committee</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">Garry Tiss, Nick Lienert, David Reu,
								Butch Moses, Hamann, Christine Sapwell</h2>
							<h4>Committee Members</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Garry <strong>Tiss</strong>
							</h2>
							<h4>Committee Members</h4>
							<p style="text-align: justify;">Contact M: 0418817703
								Email: gtiss@bigpond.net .au Piggery/Business profile - Has
								interest in two piggeries with more than 2000 sows. T & D Pig

								Marketers (agents for Elders Ltd) specialising in marketing live
								pigs and organising contracts with processors for past 36 years.
								Particular interests of Garry include trying to bring pork

								processors and growers closer together for a better outcome for
								both parties i.e. pig pricing structures and carcase gradings.

								Butch Moses: Contact M: 0428 64 2243 Email:
								butch.moses@internode.on.net Piggery/Business profile - Salt
								Lake Bacon. 550 sows farrow to finish (sow stall free). Butch
								has a wide interest in the industry and is involved as:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Commitment to assist in
									helping achieve a sustainable future for all Australian pig
									farmers</li>
								<li><i class="fa fa-check"></i> Top Pork Board Director</li>
								<li><i class="fa fa-check"></i> * ex SABOR Board Director</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> ex Chairman SAFF Pork
									Committee</li>
							</ul>
						</div>
					</div>


					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Rod<strong> Hamann</strong>
							</h2>
							<h4>Committee Members</h4>
							<p style="text-align: justify;">Piggery/Business profile: Rod
								is a South Australian who is a past graduate of Roseworthy

								Agricultural College. Through an initial career with Pig
								Improvement Company that took him to the UK and then a transfer
								to the USA, Rod has now lived and worked in the pig industries

								of three continents, plus had the opportunity to travel and
								consult in various capacities in South America, Europe and the
								USA. Prior to returning to Australia in 1999 to establish an Ag
								consulting business, he held senior executive positions within
								Murphy Family Farms (the largest pig producer in the world) and

								Heartland Pork Enterprises. In the latter role, as Chief
								Operating Officer he was directly responsible to the Chairman
								for all operational areas for this 60,000-sow Production and

								Genetics operation. He currently has various consulting
								positions, but has a primary role and responsibility as the

								Chief Executive Officer / Managing Director of Australian Pork
								Farms Group Limited, which includes a number of pig production
								businesses including Wasleys and Sheaoak Piggeries. The group
								has an investment interest in Big River Pork; plus they are a
								core foundation investor of the Pork CRC.Industry interests and
								positions include:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Director Auspork marketing
									group</li>
								<li><i class="fa fa-check"></i> Director Big River Pork
									Abattoir</li>
								<li><i class="fa fa-check"></i> Director Pork CRC and
									member of Pork CRC R & D Sub Committee</li>
								<li><i class="fa fa-check"></i> Director Porkscan</li>
								<li><i class="fa fa-check"></i> Member SA Pig Industry
									Advisory Group (PIAG)</li>
							</ul>
						</div>
					</div>


					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Christine <strong>Sapwell</strong>
							</h2>
							<h4>Pork SA Secretary/Treasurer:</h4>
							<p style="text-align: justify;">Contact M: 0408 800 011
								Email: Piggery/Business profile: Owner of Norsap which commenced
								in the pig breeding industry in1968. The piggery has recently
								come to an end of it'�s useful life with the last pigs leaving

								the property on 26th November2012. Christine still retains an
								interest in the Australian Pork Farms Group. Christine has been
								involved in Industry organizations from the early 1970âs with
								involvement in SA Farmers Federation, Australian Pork
								Corporation, Delegate to Pork Council of Australia, Australian
								Pork Limited, Pig Industry Advisory Group (current), & the Pig
								Industry Development Board. She has also escorted a number of
								Pig Study Tours both overseas and within Australia. Christine is
								keen to see pork producers well represented to Government and
								legislators and are assisted to cope with change to enable
								survival in the Other current industry positions held by
								Christine include:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Shareholder within the
									Australian Pork Farms Group which has piggery interests on the

									Adelaide Plains and Murray Bridge areas</li>
								<li><i class="fa fa-check"></i> Treasurer of the Ronald J
									Lienert Memorial Scholarship Fund whose aim is to provide a

									bursary to a student to encourage their involvement in a
									research project</li>

							</ul>
						</div>
					</div>



				</div>

<!-- PORK MEMBERSHIP -->

				<div class="row" id="porkMembership">
					<div class="col-sm-7">
						<h2>
							Pork<strong> Charter</strong>
						</h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<p class="tall" style="text-align: justify">
							<iframe
								src="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf"
							target="_blank"	style="width: 100%; height: 100%;" frameborder="0"></iframe>
						</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a target="_blank"
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf">Pork
							SA Membership Form (live link)</a> <a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf">Pork
							SA Charter (live link)</a> Membership forms are also available from
						Andy Pointon, Pork SA Executive Officer <a
							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
						or on 0418 848 845
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive" src="<c:url value='/ruralAssets/img/placements/china-pork.jpg'/>">
					</div>
				</div>


				<!-- PORK Communications -->

				<div class="row" id="porkComm"  style="display: none;">
					<div class="col-sm-12">
						<h2>Pork <strong> Pinboard</strong></h2>	
						<hr>
						<div class="toggle" data-plugin-toggle="">
						<c:forEach var="act" items="${infos}" begin="0" end="2" varStatus="row">
							<section class="toggle">
								<label>
									<div class="img-thumbnail">
										<img class="avatar" alt="" src="<c:url value='${act.iconUrl}'/>" width="45px">
									</div>
									${act.contentTitle}
								</label>
								<div class="toggle-content" style="display: none;">
									<div class="detail" style="display: block;">
										<c:forEach var="banner" items="${act.banners}" begin="0" end="0" varStatus="counter">
											<p><img src="<c:url value='${banner.imageUrl}'/>" width="250px" height="150" class="imgRight" />
											<p align="left" style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"> 
											 ${act.contentBody}
											</p></br></br></br></br>
											<div class="post-meta"> 
												<a href="<c:url value='/member/info/${act.id}'/>" class="btn btn-xs btn-primary pull-right">More info...</a>
											</div></br>
											</p>
										</c:forEach>
									</div>
								</div>
							</section>
						</c:forEach>
						</div>
					</div>
				</div>

<!-- 				PORK Events -->
				<div class="row" id="porkEvents" style="display: none;" >
					<h2>Pork<strong> Events</strong></h2>
					<div class="col-md-12">
						<table id="eventsTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
							<thead>
								<tr>
									<th>Icon</th>
									<th style="width:120px;"><span><i class="fa fa-calendar"></i> Date</span></th>
									<th style="width:180px;"><span><i class="fa fa-clock-o"></i> Time</span></th>
									<th>Title</th>
									<th>Address</th>
									<th>Cost</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="act" items="${activities}" varStatus="row">
									<tr>
										<td><a href="<c:url value='/member/event/${act.id}'/>"><img src="${act.iconUrl}" width="25" height="25"
											onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
										<td>${act.startDate}</td>
										<td>${act.rangeTime}</td>
										<td><a href="<c:url value='/member/event/${act.id}'/>">${act.contentTitle}</a></td>
										<td>${act.event.address}</td>
										<td>${act.event.cost}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>


<!-- 				PORK Hey Contacts -->

<!-- 				<div class="row" id="porkKeyContact"> -->
<!-- 					<div class="col-sm-7"> -->
<!-- 						<h2> -->
<!-- 							Pork<strong> Key Contact</strong> -->
<!-- 						</h2> -->
<!-- 						<p class="lead">Pork SA is the peak industry organisation -->
<!-- 							representing pork producers and agribusinesses associated with -->
<!-- 							the South Australian pork industry. Pork SA aims to provide -->
<!-- 							leadership, policy, advocacy and services to support the SA pork -->
<!-- 							industry.</p> -->
<!-- 						<p class="tall" style="text-align: justify">In 2005 – 06, the -->
<!-- 							South Australian livestock industry contributed $2.7 billion or -->
<!-- 							27% of South Australia's gross food revenue. The SA pig meat -->
<!-- 							industry contributed $573 million or 21% of the total South -->
<!-- 							Australia has about 550 pig farmers with the main pig producing -->
<!-- 							regions the Murray lands, Barossa, Yorke, and Mid North and -->
<!-- 							Northern. Membership of Pork SA is free to producers who pay -->
<!-- 							levies into the Pig Industry Fund. Producer members are also able -->
<!-- 							to nominate associate members, again with no joining fee, in the -->
<!-- 							anticipation that farm managers and staff will join Pork SA and -->
<!-- 							keep informed about Agribusinesses who supply and support SA pork -->
<!-- 							producers, such as feedmillers, premix manufacturers and -->
<!-- 							processors, can join as members upon payment of a nominal fee. -->

<!-- 							Under the terms of incorporation, Pork SA is required to maintain -->
<!-- 							its own register of members. Pork SA recognises that while -->
<!-- 							profitability is critical, it is just as important to have the -->
<!-- 							confidence of the community and consumers in how responsibly the -->
<!-- 							industry operates in areas such as welfare, environmental -->
<!-- 							sustainability and food safety. Pork SA will specifically address -->
<!-- 							such matters and enable the industry to be ahead of the issues -->
<!-- 							rather than reacting in an uncoordinated manner.</p> -->
<!-- 					</div> -->
<!-- 					<div class="col-sm-4 col-sm-offset-1 push-top"> -->
<!-- 						<a -->
<!-- 							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf" target="_blank">Pork -->
<!-- 							SA Membership Form (live link)</a> <a -->
<!-- 							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf" target="_blank">Pork -->
<!-- 							SA Charter (live link)</a> Membership forms are also available from -->
<!-- 						Andy Pointon, Pork SA Executive Officer <a -->
<!-- 							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a> -->
<!-- 						or on 0418 848 845 -->
<!-- 					</div> -->
<!-- 					<div class="col-sm-4 col-sm-offset-1 push-top"> -->
<!-- 						<img class="img-responsive" src="img/placements/china-pork.jpg"> -->
<!-- 					</div> -->
<!-- 				</div> -->


				<!-- PORK Business -->

				<div class="row" id="porkBusiness" style="display: none;">
				<h2>
							Pork<strong> BIOSECURITY</strong>
						</h2>
						<p class="lead">
						Guidelines, checklists and links for managing animal health issues and safe use of farm chemicals.
						</p>
				<style>
						.accordion_container { width: 1140px; } 
						.accordion_head {  color: #03ad5b; cursor: pointer; font-family:sans-serif; font-size: 16px; margin: 0 0 1px 0; padding: 10px 15px; font-weight: bold; display:block;} 
						.accordion_body {background-color:white;} 
						.accordion_body a {padding-right:0px;padding-top:0px;padding-left:0px;padding-bottom:0px;;font-family:sans-serif;font-size:14px; } 
						.accordion_body a:hover{text-decoration:underline;}
						.plusminus { float:left; } 
						</style>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
								
												&nbsp;BIOSECURITY 
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/pig/6a38923e-20dc-4c55-a866-4d8eec49c447-RC_BIOSECURITY_Pig_brands_(1).pdf' target="_blank">Pig Brands (BioSecuriity SA)</a></li>
												<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestock_forms/PIC+New+Registration+Application+Form+July+2013+(BioSecurity+SA).pdf' target="_blank">PIC New Registration Application Form July 2013 (Biosecurity SA)</a></li>
									<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestock_forms/PIC+Numbers+FAQ+(BioSecurity+SA).pdf' target="_blank">PIC Numbers FAQ (Biosecurity SA)</a></li>
									<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestock_forms/PIC+Registration+Application+Form+July+2013+(BioSecurity+SA).doc' target="_blank">PIC Registration Application Form July 2013 (Biosecurity SA) (Downloadable)</a></li>
								</ul>
								<h5 style="text-transform:capitalize;"><strong>Animal Health</strong></h5>					
								<ul>
											<li><a href='http://www.agriculture.gov.au/pests-diseases-weeds/animal' target="_blank">Pests and Diseases (Department of Agriculture)</a></li>
												<li><a href='http://www.agriculture.gov.au/animal/welfare/standards-guidelines' target="_blank">Animal Welfare Standards and Guidelines (Department of Agriculture)</a></li>
							
								</ul>
											</div>
									</h4>
								</div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Licences and permits
										 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<p class="lead">These forms and links relate to natural resource management.  Whilst the Natural Resource Management Board endeavours to keep the links up to date, some links may occasionally change without notice. </p>
									<h4><strong>Water Management</strong></h4>	
									<p>Water licences - these are required to take water from a prescribed water resource for certain uses or above certain volumes, as specified in the relevant Water Allocation Plan (WAP).  Depending on the relevant WAP:
								<ul> <li> Taking water may include (but not be limited to) pumping, damming, gravity systems and commercial forestry. 
									</li><li> A water resource may include (but not be limited to) groundwater, creeks, rivers, wetlands, lakes and surface run-off.
									</li></ul>
									<p>Forms are specific for each prescribed area in each NRM region:</p>
								<ul>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/adelaide-and-mount-lofty-ranges-water-licences-and-permits' target="_blank">Adelaide and Mt Lofty Ranges </a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/eyre-peninsula' target="_blank">Eyre Peninsula</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/northern-and-yorke' target="_blank">Northern and Yorke</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/sa-arid-lands' target="_blank">SA Arid Lands</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/sa-murray-darling-basin' target="_blank">SA Murray Darling Basin</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/south-east' target="_blank">South East</a></li>
								
									</ul>
								
										<p>Water affecting activity (WAA) permits are required to undertake an activity that may affect a water resource or water dependent ecosystem.  Each NRM Board determines what WAAs require a permit and whether a permit is required for the WAA in all or part of the region.  Some regions have developed 'recommended practices' and 'best practice operating procedures' to reduce red tape.</p>
							<p>For constructing a well, draining/discharging into a well, using imported or effluent water, water meter testing and well drilling licences use the <a href="http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/statewide" target="_blank"> Statewide forms</a></p>
							<p>For other water affecting activities use the regional forms:</p>
							<ul>
								<li><a href='http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/water/managing-water/water-affecting-activities' target="_blank">Adelaide and Mt Lofty Ranges </a></li>
								
								<li><a href='http://www.naturalresources.sa.gov.au/alinytjara-wilurara/water/water-in-the-region/managing-water-resources' target="_blank">Alinytjara Wilurara </a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/eyrepeninsula/land-and-water/water-affecting-activities' target="_blank">Eyre Peninsula</a></li>
								<li><a href="http://www.naturalresources.sa.gov.au/kangarooisland/land-and-water/water-management/water-affecting-activities" target="_blank">Kangaroo Island</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/northernandyorke/water/water-affecting-activities' target="_blank">Northern and Yorke</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/aridlands/water/water-affecting-activities' target="_blank">SA Arid Lands</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/samurraydarlingbasin/water/managing-water-resources/water-affecting-activities' target="_blank">SA Murray Darling Basin</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/southeast/water/water-affecting-activities' target="_blank">South East</a></li>
								
									</ul>
									<p><ul><li><a href='http://www.environment.sa.gov.au/managing-natural-resources/water-use/water-planning' target="_blank">More information about water resource management </a></li></ul></p>
							
							<h4><strong>Land Management</strong></h4>	
							<h5><strong>Native Vegetation</strong></h5>	
							<ul>
								<li><a href='http://www.environment.sa.gov.au/files/05e1a003-b7db-456a-9712-9efa00f8c536/con-nv-form-clearanceapplication.pdf' target="_blank">Application to clear native vegetation </li>
								<li><a href='http://www.environment.sa.gov.au/files/2c78a1d2-74fa-4547-8383-9efa00f8ce00/con-nv-guideline-agriculture.pdf' target="_blank">Guide to native vegetation regulations for agriculture</a></li>
								<li><a href="http://www.environment.sa.gov.au/managing-natural-resources/Native_vegetation/Managing_native_vegetation" target="_blank">Other information about managing native vegetation in South Australia</a></li>
								<li>Nationally protected communities and species</li>
								</ul><p>
								Some ecological communities, heritage places and species of plants and animals are protected nationally under the<a href="http://www.environment.gov.au/epbc/about" target="_blank"> Environment, Protection and Biodiversity Conservation Act 1999</a> (EPBC Act). 
								There is an <a href="http://www.environment.gov.au/epbc/protected-matters-search-tool" target="_blank">interactive map-based search tool</a> on the Department of the Environment website to search for matters of national significance that may occur in your area of interest.  Non-marine ecological communities currently protected under the EPBC Act in South Australia (as of 23/02/15) include, but may not be limited to:
								</p>
								<ul>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=3' target="_blank">Buloke Woodlands of the Riverina and Murray-Darling Depression Bioregions </li>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=86&status=Endangered' target="_blank">Grey Box (Eucalyptus microcarpa) Grassy Woodlands and Derived Native Grasslands of South-eastern Australia</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=124&status=Endangered" target="_blank">Eyre Peninsula Blue Gum (Eucalyptus petiolaris) Woodland</a></li>
								<li>Iron-grass Natural Temperate Grassland of South Australia</li>
								<li>Kangaroo Island Narrow-leaved Mallee (Eucalyptus cneorifolia) Woodland</li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=36" target="_blank">Peppermint Box (Eucalyptus odorata) Grassy Woodland of South Australia</a></li>
								
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=118' target="_blank">Subtropical and Temperate Coastal Saltmarsh</li>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=31' target="_blank">Swamps of the Fleurieu Peninsula</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=26" target="_blank">The community of native species dependent on natural discharge of groundwater from the Great Artesian Basin </a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/wetlands/alphablist.pl" target="_blank">Wetlands of international significance</a></li>
								
								</ul>
								
								<h5><strong>Wildlife</strong></h5>	
						<ul>
								<li><a href='http://www.environment.sa.gov.au/files/37da2e46-09af-4ab3-acf3-9e6100c2916f/PermitApp_destroywildlife.pdf' target="_blank">Permit to destroy wildlife</li>
								<li><a href='http://www.environment.sa.gov.au/files/3d447eb6-8038-446f-acd7-a40a00e6247c/hunt-duck-permit-application-2015-form.pdf' target="_blank">Duck hunting permit</a></li>
								<li><a href="http://www.environment.sa.gov.au/Plants_and_Animals/Animal_welfare/Animal_welfare_legislation/Codes_of_practice_for_the_humane_destruction_of_wildlife_" target="_blank">Codes of practice for the humane destruction of wildlife </a></li>
								<li><a href="http://www.environment.sa.gov.au/licences-and-permits/Animals_in_the_wild_permits" target="_blank">Other wildlife-related permits and information</a></li>
								
						</ul>
						<p><ul><li><a href="http://www.environment.sa.gov.au/licences-and-permits" target="_blank">Other licences and permits administered by DEWNR</a></li></ul>
							</div></div></h4></div></div>
								</div>
							
								
				<script>
						$(document).ready(function () {
						    //toggle the component with class accordion_body
						    $(".accordion_head").click(function () {
						        if ($('.accordion_body').is(':visible')) {
						            $(".accordion_body").slideUp(300);
						            $(".plusminus").text('+');
						        }
						        if ($(this).next(".accordion_body").is(':visible')) {
						            $(this).next(".accordion_body").slideUp(300);
						            $(this).children(".plusminus").text('+');
						        } else {
						            $(this).next(".accordion_body").slideDown(300);
						            $(this).children(".plusminus").text('-');
						        }
						    });
						});
						
</script></div></div>
					<!-- Business -->
					
					<div class="row" id="porkBioSecurity" style="display: none;">
					<div class="col-sm-12">
						<h2>
							Pork <strong> Business</strong>
						</h2>
						<p class="lead">Reports, strategies and checklists to support
							farm business sustainability and growth.</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											&nbsp;BUSINESS <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">

												<ul>
													<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/Farm_finance_strategy_2007.pdf' target="_blank">Farm Finance Strategy 2007 (SAFF)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+Guide+2015+pdf.pdf' target="_blank"> Farm Gross Margin Guide 2015 (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+and+Enterprise+Planning+Guide+2012.pdf' target="_blank"> Farm Gross Margin and Enterprise Planning Guide 2012 (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farming+the+Business+introductory+guide.pdf' target="_blank"> Farming the Business Introductory Guide (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/How_does_my_farm_business_compare+DEWNR+doc.pdf' target="_blank">How does my farm business compare (DEWNR)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/Key+financial+ratios+for+farm+sustainability+Grains+Research+and+Development+Corp.pdf' target="_blank">Key financial ratios for farm sustainability Grains Research and Development Corporation (GRDC)</a></li>
										
												</ul>

											</div>
										</div>
									</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
	
	
					
				<!-- PORK Legal -->

				
							
							<!-- Transport -->
				
				<div class="row" id="porkForm" style="display: none;">
					<div class="col-sm-12">
						<h2>
							Pork<strong> Forms</strong>
						</h2>
						<p class="lead">Guidelines and application forms for PIC numbers, livestock brands, earmarks and tattoos.</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											&nbsp;Forms <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">
										
									<ul>
											<li><a href='https://s3.amazonaws.com/ccmeresources/aa79e0d8-2a89-4879-995c-31363d60515a-APIQ_SOP3_-_Staff_Competency_Procedure_Version_1.2_1_2014 (1).doc' target="_blank">APIQ SOP3 Staff_Competency_Procedure_Version_1.2_1_2014 (1)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/f30374e7-e664-45cd-977e-58f632b47d28-APIQ_SOP10_-_Euthanasia_Version_1.2_1_2014.pdf' target="_blank">APIQ_SOP10 Euthanasia_Version_1.2_1_2014</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/27e8e905-2ef1-4d1d-9c02-953982b3ed76-PIC Numbers.docx' target="_blank">PIC Numbers</a></li>
											</ul>
									</div>
							</div>
									</h4>
								</div></div></div></div></div>
								<!-- Transport -->
				<div class="row" id="porkLegal" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Pork <strong> Transport</strong>
						</h2>
						<p class="lead">
						Information on animal loading, heavy vehicle regulations, vehicle escorting guidelines, driver work diaries and license application forms. Sourced from Government websites and including live links to websites for latest news and updates.
						
						</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;Animal Loading<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										
									<ul>
							<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/Is+It+Fit+To+Load+-+Guide+(Meat+and+Livestock+Australia).pdf' target="_blank">Is It Fit to Load Guide (Meat and Livestock Australia)</a></li>
														</ul>	
									</div>
							</div>
									</h4>
								</div></div>
			<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Regulations for heavy vehicles
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf
											' target="_blank">Regulations for Driving Oversize or Overmass Agricultural Vehicles (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf' target="_blank">Regulations for Transporting Agricultural Vehicles as Loads (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf' target="_blank">Required documents to be carried (NHVR)</a></li>
											<li><a target="_blank" href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting+Guidelines+for+oversize+and+overmass+vechicles+and+loads.pdf'>Escorting Guidelines for Drivers and Escorting Vehicles (NHVR)</a></li>
						
											</ul>
										
											</div>
							</div>
									</h4></div>
								</div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Licence Application Forms & Driver Work Diaries
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33+Upgrade+Truck+Licence.pdf
											' target="_blank">Restricted Licence Application Form MR 33 (DPTI) </a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf
											' target="_blank"> Driver Work Diary (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD+201405-0028-supplementary-work-diary-record.pdf
											' target="_blank">Supplementary Work Diary Record (NHVR) </a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;NHVR Live Weblinks 
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
				
											<li><a href='https://www.nhvr.gov.au/resources/faqs
											' target="_blank">https://www.nhvr.gov.au/resources/faqs
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/forms-and-services
' target="_blank">https://www.nhvr.gov.au/resources/forms-and-services
											</a></li>
											<li><a href='https://www.nhvr.gov.au/news-events/stakeholder-events
											' target="_blank">https://www.nhvr.gov.au/news-events/stakeholder-events
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/rss-feeds
											' target="_blank">https://www.nhvr.gov.au/resources/rss-feeds

											</a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div></div></div></div></div>			
						</div></div>
						
								<!-- Trainign & Education -->	
								
								
								<div class="row" id="grainTraining" style="display:none;">
				<div class="col-sm-12">
					<h2>Pork <strong> Training & Education</strong></h2>
					<p class="lead">
						Links to scholarships and courses for livestock industry.
						</p>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Links to further education scholarships
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.adelaide.edu.au/scholarships/undergrad/isolated/ ' target="_blank">Scholarships at Adelaide University</a></li>
									<li><a href='http://www.flinders.edu.au/medicine/sites/nt-clinical-school/students/scholarships.cfm' target="_blank">Scholarships at Flinders University</a></li>
									<li><a href='http://www.tafesa.edu.au/apply-enrol/before-starting/scholarships-grants' target="_blank">Scholarships at TAFE SA</a></li>
									<li><a href='https://www.sa.gov.au/topics/education-skills-and-learning/financial-help-scholarships-and-grants/scholarships' target="_blank">Selection of SA Government scholarships</a></li>
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Courses
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.tafesa.edu.au/courses/agriculture-science/agriculture' target="_blank">TAFE SA</a></li>
									<li><a href='http://www.adelaide.edu.au/course-outlines/104332/1/sem-2/' target="_blank">ADELAIDE UNIVERSITY (Roseworthy campus course)</a></li>
									
									</ul>
								</div></div></h4></div></div>
					</div>
					
					</div></div></div>
							
						<!-- Industrial Relations -->
							<div class="row" id="grainIndustrailContent" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Pork<strong> Workplace</strong>
						</h2>
						<p class="lead">
						Links to MERS reports and updates on superannuation, WorkCover and arrangements for different types of employment.
						</p>
						
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;INDUSTRIAL RELATIONS
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/pig/Fair+Work+Information+Booklet+for+Agricultural+Producers.pdf'>Fair Work Information Booklet for Agricultural Producers (NFF and Dept of Agriculture)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/c3f9a50d-ae38-4594-a71b-13fde29b7923-Industrial Relations - Member Update September  2014.pdf' target="_blank">Industrial Relations - Member Update September  2014</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/6936c3a8-bcf7-4693-9102-84ef095efa68-Workers Capacity information form.doc' target="_blank">Workers Capacity (Download and Print)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/f3b1c812-40ac-40fd-9e3b-e7dbe516f496-Workers Capacity Letter to Doctor TEMPLATE.doc' target="_blank">Workers Capacity Letter (Download and Print)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/2d5fae55-4d4c-4f9c-bbf3-0e34b19e1c40-Workers Capacity Medical release authority TEMPLATE.doc' target="_blank">Workers Capacity Medical Letter (Download and Print)</a></li>
											</ul>
										</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Employer obligations
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
											<li><a href='https://s3.amazonaws.com/ccmeresources/e81f5a9d-4692-4bef-bab5-fc57174ee135-2014 Agricultural Self Assessment Guide PDF.pdf' target="_blank">2014 Agricultural Self Assessment Guide PDF</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/7b2988a3-93e9-4b1a-a4b8-69011c9e7ca6-Pork SA Piggery WHS Guide  Final 2-4-14.pdf' target="_blank">Pork SA Piggery WHS Guide  Final 2-4-14</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/6d0dea04-0eb3-44a9-beed-04a1bf1c1cbb-Pork SA WHS Awareness Seminars March 2014_AP.pdf' target="_blank">Pork SA WHS Awareness Seminars March 2014_AP</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/91a306fa-dfe9-4e74-a470-49f4077247d2-WORKPLACE SAFETY APIQ_SOP3_-_Staff_Competency_Procedure_Version_1.2_1_2014.pdf' target="_blank">Workplace Safety APIQ_SOP3_-_Staff_Competency_Procedure_Version_1.2_1_2014</a></li>
											
											<li><a href='https://s3.amazonaws.com/ccmeresources/b7cd8837-b8b5-4039-b5e2-e20ac3e0d114-WHS Seven Steps for Small Buiness.pdf' target="_blank">WHS Seven Steps for Small Buiness</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/1a3187f0-7c2c-4250-a2ff-7cb7c97e5e78-WORKPLACE SAFETY Prosecutions 2014.pdf' target="_blank">Workplace Safety Prosecutions 2014</a></li>
											</ul>
								</div></div></h4></div></div></div>
							
								</div></div></div>
					<!-- Work Health Safety -->

				<div class="row" id="grainWorkContent" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Pork<strong> Emergency Management</strong>
						</h2>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Emergency Management
										<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<h5 style="text-transform:capitalize;"><strong>Checklists</strong></h5>			
									
										<ul>
											<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG" target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG" target="_blank">Heatwaves Risk (PIRSA)</a></li>
<!-- 													<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf" target="_blank">Personal Medical Details Checklist (Emergency Alert) (For printing and completing)</a></li> -->
												
										</ul><h5 style="text-transform:capitalize;"><strong>Fire, Flood and Earthquake </strong></h5>			
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf" target="_blank">Disaster Planning - Bushfire Checklist (CFS)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf" target="_blank">Disaster Planning - Earthquake Action Plan (SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf" target="_blank">Disaster Planning - Emergency Contacts(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf" target="_blank">Disaster Planning - Extreme Heat Checklist(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf" target="_blank">Disaster Planning - Flood Checklist(SES)</a></li>
											<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf" target="_blank">Extreme heat Checklist (SA Health)</a></li>
												<li><a href="ttp://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf" target="_blank">Extreme Heat Guide (SA Health)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/61003298-32e8-4452-8a41-d86071e89196-Bushfire_Prevention_and_Preparedness_S2.pdf' target="_blank">Bushfire Prevention and Preparedness (PIRSA) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/97289472-5763-4ec0-ae30-1fe16850a2a1-CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf' target="_blank">Bushfire - Care of Pets and Livestock (CFS) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/d3d9575b-97b1-4d1e-acd4-520b98904157-CFS FACT SHEET - AFTER THE FIRE.pdf' target="_blank">Fact Sheet - After the Fire (CFS)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/0fed75f1-34bf-437a-84a0-736cac01d87c-Emergency animal disease preparedness.pdf' target="_blank">Emergency animal disease preparedness (PIRSA)</a></li>
												<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST%20-%20HEATWAVES%20RISK.PNG' target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												
												</ul>
											
											
										
											</div>
							</div>
									</h4>
								</div></div>
					</div></div>
				<!-- PORK SURVEY -->

				<div class="row" id="porkSurvey" style="display: none;">
					<div class="col-sm-7">
						<h2>
							Pork<strong> Surveys</strong>
						</h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<p class="tall" style="text-align: justify">In 2005 – 06, the
							South Australian livestock industry contributed $2.7 billion or
							27% of South Australia's gross food revenue. The SA pig meat
							industry contributed $573 million or 21% of the total South
							Australia has about 550 pig farmers with the main pig producing
							regions the Murray lands, Barossa, Yorke, and Mid North and
							Northern. Membership of Pork SA is free to producers who pay
							levies into the Pig Industry Fund. Producer members are also able
							to nominate associate members, again with no joining fee, in the
							anticipation that farm managers and staff will join Pork SA and
							keep informed about Agribusinesses who supply and support SA pork
							producers, such as feedmillers, premix manufacturers and
							processors, can join as members upon payment of a nominal fee.

							Under the terms of incorporation, Pork SA is required to maintain
							its own register of members. Pork SA recognises that while
							profitability is critical, it is just as important to have the
							confidence of the community and consumers in how responsibly the
							industry operates in areas such as welfare, environmental
							sustainability and food safety. Pork SA will specifically address
							such matters and enable the industry to be ahead of the issues
							rather than reacting in an uncoordinated manner.</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a target="_blank"
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf" target="_blank">Pork
							SA Membership Form (live link)</a> <a
						 target="_blank"	href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf" target="_blank">Pork
							SA Charter (live link)</a> Membership forms are also available from
						Andy Pointon, Pork SA Executive Officer <a
							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
						or on 0418 848 845
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive" src="<c:url value='/ruralAssets/img/placements/china-pork.jpg'/>">
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<script src="<c:url value='/ruralAssets/js/logic.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/theme.js'/>"></script>
	
	<script src="<c:url value='/ruralAssets/js/newsRSS.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/pages/index.js'/>"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	
	<script>
		(function( $ ) {
			'use strict';
			var datatableInit = function() {
				var $table = $('#eventTable');
				$table.dataTable({
					sDom: "<'text-right mb-md'>" + $.fn.dataTable.defaults.sDom,
					"paging":false,
					"info":false,
					"order":[[1,"asc"]],
					"oLanguage": {
					    "sSearch": "Search: "}
				});
			};
			$(function() {
				datatableInit();
			});
		}).apply( this, [ jQuery ]);
	</script>
	
	<script type="text/javascript">
		google.load("feeds", "1");
		google.setOnLoadCallback(newsRSS.init);
	</script>

	<script>
		$(document).ready(function() {
			$('#porkOption').addClass("active");
			Index.initPork();
			
			$('#porkAbout').show();
			
			//if tabs clicked
			function porkCommittee(){
				$('.pork').removeClass("active"); 
				$('#porkCommitteeTab').addClass('active');
				$('.row').hide();
				$('.pork').show();
				$('#porkCommittee').show();
			}
			
			function porkMembership(){
				$('.pork').removeClass("active"); 
				$('#porkMembershipTab').addClass('active');
				$('.row').hide();
				$('#porkMembership').show();
			}
			
			function aboutPork(){
				$('.pork').removeClass("active"); 
				$('#aboutPorkTab').addClass('active');
				$('.row').hide();
				$('#porkAbout').show();
			}
			
		});
	</script>
</body>
</html>

