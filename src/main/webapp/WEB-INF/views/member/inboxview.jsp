<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<div class="home-intro" id="home-intro">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<p>
						Rural Connect <em>Messages</em> </br> <strong>Today's Snapshot</strong>
				</div>
				<div class="col-md-4">
					<div class="get-started">
						<a href="<c:url value='/member/partners'/>"
							class="btn btn-lg btn-warning"
							onclick="_gaq.push(['_trackEvent', 'Commercial Partners', 'Click', 'Meet our commercial partners']);"
							target="_blank">Meet our commercial partners</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row center">

					<section class="content-with-menu mailbox" id="testmodal">
						<div class="content-with-menu-container" data-mailbox data-mailbox-view="folder">
							
							<menu id="content-menu" class="inner-menu" role="menu">
								<div class="nano">
									<div class="nano-content">
							
										<div class="inner-menu-content">
											
											    <div class="panel panel-success">
												      <div class="panel-heading">
												        <h3 class="panel-title">From: ${message.accountByFromId.name} - ${message.formattedDate}</h3>
												      </div>
												      <div class="panel-body">
												      <ul class="list-inline">
												      	<c:if test="${not empty banner}">
														    <li><img class="img-responsive img-thumbnail" src="${banner}" /></li>
														</c:if>
														  <li class="vcenter"><h4>${messagesubject}</h4>
														  <p>${messagebody}</p>
														  <c:if test="${not empty activity}">
								  							<div>
																	<section class="panel panel-featured-left panel-featured-success">
																		<div class="panel-body">
																			<div class="widget-summary">
																				<div class="widget-summary-col widget-summary-col-icon">
																					<div class="summary-icon bg-success">
																						<img class="img-responsive img-thumbnail" src="${activity.iconUrl}" />
																					</div>
																				</div>
																				<div class="widget-summary-col">
																					<div class="summary">
																						<h4 class="title">${activity.contentTitle}s</h4>
																						<div class="info">
																							<strong class="amount">${activity.event.address}</strong>
																							<span class="text-primary">${activity.event.displayDatetime}</span>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</section>
																</div>
														  	</c:if>													  
														  </li>
													  </ul>
												      </div>
												      <a type="button" href="<c:url value='/member/inbox'/>" class="btn btn-success btn-lg" >Go Back</a>
												      <a id="deleteButton" type="button" class="btn btn-danger btn-lg" >Delete</a>
    											</div>
											<style>
											.vcenter {
													    display: inline-block;
													    vertical-align: top;
													    float: none;
													}
											</style>
											<form id="delete" action="<c:url value='/member/inbox/deleteCurrent'/>" method="post"/>
												<input type="hidden" value="${message.id}" name="id"/>
											</form>
										</div>
									</div>
								</div>
							</menu>
						</div>
						
					</section>
					</div>
					</div>
					
					<script>
				$( document ).ready(function() {
				
				$("#deleteButton").click(function(){
					
						$("#delete").submit();
					
				});
				
			});
			
			</script>