<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- start: page -->
<html>
<style>
.tile{
    width: 350px;
    height: 280px;
    margin-bottom: 20px;
    padding:1px;
    float: left;
    overflow: hidden;
  	margin-top:10px;
}
.tileLogo{
    width: 280px;
    height: 200px;
    margin-bottom: 20px;
    padding:1px;
    float: left;
    overflow: hidden;
  	margin-top:40px;
  	margin-left:10px;
}	
.tileText{
    width: 430px;
    height: 230px;
    margin-bottom: 20px;
    padding:1px;
    float: left;
    overflow: hidden;
  margin-top:25px;
}
</style>
	<body>

	
<div role="main" class="main">

    <section class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active" style="color:white">Our Sponsors</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Meet our commercial partners </h1>
                </div>
            </div>
        </div>
    </section>
	
	  <div  class="container">
            <h4>Primary Producers South Australia is pleased to introduce these commercial partners who have been invited to provide some highly relevant and useful offers to its members.</h4>
 
	  <!-- Momentum video section -->
	  <div style="width:1130px;height:250px;border:1px solid #000;">
	   <div class="tileLogo"><a href="http://www.momentumenergy.com.au/" target="#"><img style="width:250px;height:100px;"src="http://rural-resources.ruralconnect.com.au/momentum-logo.png">
			 </a> <a href="http://www.momentumenergy.com.au/" target="_blank"><h5>Click here to visit our website</h5></a> </div>
	 	   <div class="tileText"><h4>&nbsp;Welcome to Momentum Energy screen</h4>
			  <h6 style="margin-left:10px;margin-right:80px;">Momentum's goal is to offer you as much value as possible, not only via our PPSA Momentum Energy special, but also through our energy efficiency solutions.
			<a target="_blank" href="<c:url value='/member/momentum'/>"> Click here</a> to open our short video clip.</h6></div>
	 	   <div class="tile"><img src="http://rural-resources.ruralconnect.com.au/mqdefault.jpg"><br /><a target="_blank" href="<c:url value='/member/momentum'/>"> Click here</a> to open our short video clip.</img>
	 	   </div>
	 
	  </div>
	  </div>
	  
	  <!-- Prime Super page -->
    <div class="container">

 <br> <div style="width:1130px;height:250px;border:1px solid #000;">
	   <div class="tileLogo"><a href="http://www.primesuper.com.au/" target="#"><img style="width:250px;height:100px;"src="http://rural-resources.ruralconnect.com.au/videos/primesuper_bi_2.jpg">
			 </a><br /> <br /><a href="http://www.primesuper.com.au/" target="_blank"> <h5>Click here to visit our website</h5></a></div>
	 	   <div class="tileText"><h4>&nbsp;Welcome to Prime Super screen</h4>
			  <h6 style="margin-left:10px;margin-right:80px;"><a target="_blank" href="<c:url value='/member/primeSuper'/>"> Click here</a> to open our short video clips. They are an introduction to the ways we can help you.</h6></div>
	 	   <div class="tile"><img src="http://rural-resources.ruralconnect.com.au/RC-Prime.jpg"><br /><a target="_blank" href="<c:url value='/member/primeSuper'/>"> Click here</a> to open our short video clips.</img>
	 	   </div>
	 
	  </div></div>
     
        </div>
    
    </body>
    </html>