<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- start: page -->
<html>

	<body>


<div role="main" class="main">

    <section class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active" style="color:white">Our Sponsors</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Meet our commercial partners </h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">

            <div class="col-md-7">

                <h2 class="shorter">Prime <strong>Super</strong></h2>
                <h4>Michael Hawthorn</h4>
				<div class="row">
                <video width="100%"  controls id="dvVideoPlayer" src="http://rural-resources.ruralconnect.com.au/videos/PrimeSuperVideo2_compressed.mp4" />
                    <source src="" type="video/mp4" id="srcVideo">
                    </video>
</div>

                </div>
            <div class="col-md-5">

                <div class="text-center"><a href="http://www.primesuper.com.au/" target="#"><img src="http://rural-resources.ruralconnect.com.au/videos/primesuper_bi_2.jpg">

                </a></div>




                <div class="col-sm-3"><span class="arrow hlb" style="margin-top: 70px"></span>
                </div>
                <div class="col-sm-9">
                    <h2 class="shorter" style="margin-top: 50px">Click to Watch Video
                    </h2>

                    <div id="dvListContainer" style="font-size:small">
                    

                     <p> For more information on our tailor-made plans for you, please click here to our website</p>

                </div>
            </div>

        </div>

        <hr class="tall">

        <div class="row">
           
            	<div style="display: block; text-align: justify"  id="dvDisclaimer">
                                               
            	
            </div>
        </div>
        </div>
    </div>
    
    <!-- Theme Initialization Files-->
		<script src="<c:url value='/ruralAssets/myJS/custom/video.js'/>"></script>
		<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
		<script src="<c:url value='/ruralAssets/js/pages/index.js'/>"></script>   
		
		    <script>
            $(document).ready(function () {
                Index.initPartners();
            });

        </script>

        </script>
		
    </body>
    </html>