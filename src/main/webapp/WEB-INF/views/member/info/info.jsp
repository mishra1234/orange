<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>

<html>
<body>
		<div class="container">

			<div class="row">
				<div class="col-md-4">
					<div>
						<c:forEach var="bann" items="${listOfBanners}" begin="0" end="0" varStatus="row">
							<div>
								<div class="thumbnail">
									<img alt="" src="${bann.imageUrl}" title="${bann.subtitle}" style="width: 50%;max-height: 100%">
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
				
				
					<div class="col-md-8">

							<h4>${inf.contentTitle}</h4>
							<p class="taller">${inf.contentBody}</p>
							
							<ul class="portfolio-details">
								<c:if test="${fn:length(listOfFiles) gt 0}">
								
									<li>
										<p><strong>Files:</strong></p>
	
										<ul class="list list-skills icons list-unstyled list-inline">
											
												<c:forEach var="file" items="${listOfFiles}" varStatus="row">
													<li><i class="fa fa-check-circle"></i> 	<a href="${file.url}"> ${file.title}</a></li>
												</c:forEach>
										</ul>
									</li>
								</c:if>
								<c:if test="${fn:length(listOfVideos) gt 0}">
									<li>
										<p><strong>Videos:</strong></p>
	
										<ul class="list list-skills icons list-unstyled list-inline">
											
												<c:forEach var="video" items="${listOfVideos}" varStatus="row">
													<li><i class="fa fa-check-circle"></i> 	<a href="${video.url}"> ${video.title}</a></li>
												</c:forEach>
										</ul>
									</li>
								</c:if>
								
								<li>
									<p><strong>Contact details:</strong></p>
									<p>${inf.account.name}</p> <p>${inf.account.email}</p>
								</li>
							</ul>
						</div>
					</div>
			</div>

	
</body>
</html>
