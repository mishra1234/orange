<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<div class="main">

<div class="home-intro" id="home-intro">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<p>
					Rural Connect <em>News Archive</em> </br> <strong>Today's Snapshot</strong>
			</div>
			<div class="col-md-4">
				<div class="get-started">
					<a href="<c:url value='/member/partners'/>"
						class="btn btn-lg btn-primary"
						onclick="_gaq.push(['_trackEvent', 'Commercial Partners', 'Click', 'Meet our commercial partners']);"
						target="_blank">Meet our commercial partners</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container" style="margin-top: -45px">
	<div class="row">
		<div class="col-md-12">
			<div class="tabs" id="tabs">
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a href="#dashboardTab" data-toggle="tab"
						class="text-center"
						onclick="_gaq.push(['_trackEvent', 'Inbox', 'Click', 'Member Inbox']);"><i
							class="fa fa-newspaper-o"></i> News Archive</a></li>

				</ul>
					<form action="deleteMessages" method="post"/>
					<div class="tab-content">
						<div id="dashboardTab" class="tab-pane active">

							<div class="panel">
								<div class="panel-body">
									<ul class="list-group">
										<table id="infosTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
											<thead>
												<tr>
													<th>id</th>
													<th>Icon</th>
													<th style="width:320px;">Title</th>
													<th style="width:440px;">Content</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="act" items="${infos}" begin="0" end="20" varStatus="row">
												<tr>
													<td>${act.id}</td>
													<td><a class="infosframe" href="<c:url value='/member/info/${act.id}'/>">
														<img src="${act.iconUrl}" width="25" height="25" onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
													<td><a class="infosframe" href="<c:url value='/member/info/${act.id}'/>" class="btn btn-xs btn-primary pull-right">${act.contentTitle}</a>
														</td>
													<td><p align="left" style="display: block; display: -webkit-box; height: 27px;
												 			margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 2; -webkit-box-orient: vertical; 
												 			overflow: hidden; text-overflow: ellipsis;"> ${act.contentBody}</p></td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
									</ul>

								</div>
							</div>
							
						</div>
						
					</div>
					</form>
				</div>
			</div>
		</div>
</div>
				

						<!-- DataTables -->
					<script src="<c:url value='/assets/vendor/select2/select2.js"'/>"></script>
					<script src="<c:url value='/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"'/>"></script>
					<script src="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"'/>"></script>
					<script src="<c:url value='/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"'/>"></script>
					
					<script>
						(function( $ ) {
							var datatableInit = function() {
								var $table = $('#infosTable');
								$table.dataTable({
									oTableTools: {
										sSwfPath: $table.data('swf-path')
									}, 
									"paging":false,
									"info":false,
									"order":[[0,"desc"]], 
									"columnDefs":[{ "visible": false, "targets": 0 }],
									"oLanguage": {
									    "sSearch": "Search: "}
								});
					
							};
					
							$(function() {
								datatableInit();
							});
						}).apply( this, [ jQuery ]);
				</script>
			

</body>

		

</html>