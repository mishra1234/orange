<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- start: page -->
<html>

<body>

	<div role="main" class="main">

		<section class="page-top">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1>
							<img alt="Rural Connect" height="60" src="<c:url value='/ruralAssets/img/icons/grainproducers.jpg'/>">
							Grain Producers SA
						</h1><br />
					
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="<c:url value='/member/partners'/>" class="btn btn-lg btn-primary">Meet our commercial partners</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="container">

			<ul class="nav nav-pills center">
				
<!-- 					<li class="active pork" id="porkMembershipTab"><a -->
<!-- 					href="javascript:porkMembership()">About</a> -->
<!-- 					</li> -->

				<c:if test="${gatedGrain != 'granted'}">

					<li class="active pork" id="porkMembershipTab"><a
						href="javascript:porkMembership()">About</a></li>
				</c:if>
<!-- 				<li class="pork" id="aboutPorkTab"><a -->
<!-- 					href="javascript:aboutPork()">News</a></li> -->


<!-- 				<li class="pork" id="porkCommTab"><a -->
<!-- 					href="javascript:porkComm()">Pinboard</a></li> -->
<!-- 				<li class="pork" id="porkEventsTab"><a -->
<!-- 					href="javascript:porkEvents()">Events</a></li> -->
<!-- 					<li class="pork" id="grainWorkTab"><a -->
<!-- 							href="javascript:grainWork()">Emergency Management </a></li> -->
				<c:if test="${gatedGrain == 'granted'}">
<!-- 						<li class="pork" id="porkBusinesTab"><a -->
<!-- 							href="javascript:porkBusiness()">Biosecurity</a></li> -->
<!--  						<li class="pork" id="porkBioSecurityTab"><a  -->
<!-- 							href="javascript:porkBioSecurity()">Business</a></li> -->
<!-- 						<li class="pork" id="grainIndustrailTab"><a -->
<!-- 							href="javascript:grainIndustrail()">Workplace</a></li> -->
	
<!-- 						<li class="pork" id="porkFormTab"><a -->
<!-- 							href="javascript:porkForm()">Forms</a></li> -->
	
<!-- 						<li class="pork" id="porkLegalTab"><a -->
<!-- 							href="javascript:porkLegal()">Transport</a></li> -->
	
<!-- 						<li class="pork" id="porkOtherTab"><a -->
<!-- 							href="javascript:grainTraining()">Training & Education</a></li> -->
	
						
				</c:if>

			</ul>

			

			<div id="porkContent">
			
					<!-- About Grain -->
<div class="row" id="porkAbout" style="display:none;">
					<div class="col-sm-7">
						<h2>
							Latest <strong>News</strong>
						</h2>
						
						<div class="widget">
                                <div class="widget-extra themed-background-dark">
                                    
                                </div>
                                <div class="widget">

													<div class="widget-extra">
														<!-- Timeline Content -->
														<div id="dvNewsRSS" class="timeline">
															
														</div>
														<!-- END Timeline Content -->
													</div>
												</div>
                            </div>
						
						</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						
					
				</div>
</div>
			<!-- About Grain Producer SA -->
			<div class="row" id="porkMembership">
					<div class="col-sm-7">
						<h2>
							About<strong> Grain Producers SA</strong>
							
						</h2>
						Visit us at <a href="http://www.grainproducerssa.com.au/" target="_blank" >http://www.grainproducerssa.com.au/</a> 
					<br /><br/>
						<p class="lead">Grain Producers SA is a not-for-profit organisation which represents South Australia's 3000 grain growers.
					On average, the state's growers plant more than 4.2 million hectares of cereals, pulses, oilseeds and hay every year. Depending on the season, 6 to 10 million tonnes of grain is produced, plus hay. Grain growing is a significant industry for South Australia, generating about $2 billion per year for the State's economy as well as employing thousands of people in food-related industries.
</p>
						<p>
				GPSA develops and implements policies that promote the economic and environmental sustainability of the grains industry to benefit producers.<br /><br />
				For SA grain growers, GPSA is your opportunity to influence the policies and projects that impact you and the people who represent you beyond the farm gate.<br/><br />
				
				Visit our website for:<br />
				<a href="http://www.grainproducerssa.com.au/pages/news-information.php" target="_blank">News & Information</a><br/>
				<a href="http://www.grainproducerssa.com.au/pages/news-information/diary-dates.php" target="_blank">Events</a><br/>
				<a href="http://www.grainproducerssa.com.au/pages/membership.php" target="_blank">Membership</a><br/>
				<a href="http://www.grainproducerssa.com.au/pages/news-information.php" target="_blank">Other Important Grains Information</a><br/>
				
				
						</p>
					</div>
					
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive" src="<c:url value='/ruralAssets/img/placements/grain1.jpg'/>">
					</div>
					
				</div>			
	
				<!-- Grain Communications -->

				<div class="row" id="porkComm" style="display:none;">
					<div class="col-sm-12">
						<h2>Grain <strong> Pinboard</strong></h2>	
						<hr>
						<div class="toggle" data-plugin-toggle="">
						<c:forEach var="act" items="${infos}" begin="0" end="2" varStatus="row">
							<section class="toggle">
								<label>
									<div class="img-thumbnail">
										<img class="avatar" alt="" src="<c:url value='${act.iconUrl}'/>" width="45px">
									</div>
									${act.contentTitle}
								</label>
								<div class="toggle-content" style="display: none;">
									<div class="detail" style="display: block;">
										<c:forEach var="banner" items="${act.banners}" begin="0" end="0" varStatus="counter">
											<p><img src="<c:url value='${banner.imageUrl}'/>" width="250px" height="150" class="imgRight" />
											<p align="left" style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"> 
											 ${act.contentBody}
											</p></br></br></br></br>
											<div class="post-meta"> 
												<a href="<c:url value='/member/info/${act.id}'/>" class="btn btn-xs btn-primary pull-right">More info...</a>
											</div></br>
											</p>
										</c:forEach>
									</div>
								</div>
							</section>
						</c:forEach>
						</div>
					</div>
				</div>

				<!-- PORK Events -->
				<div class="row" id="porkEvents" style="display:none;">
					<h2>Grain SA <strong> Events</strong></h2>
					<div class="col-md-12">
						<table id="eventsTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
							<thead>
								<tr>
									<th>Icon</th>
									<th style="width:120px;"><span><i class="fa fa-calendar"></i> Date</span></th>
									<th style="width:180px;"><span><i class="fa fa-clock-o"></i> Time</span></th>
									<th>Title</th>
									<th>Address</th>
									<th>Cost</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="act" items="${activities}" varStatus="row">
									<tr>
										<td><a href="<c:url value='/member/event/${act.id}'/>"><img src="${act.iconUrl}" width="25" height="25"
											onerror="this.src='https://www.google.com.br/logos/2012/montessori-res.png';"></a></td>
										<td>${act.startDate}</td>
										<td>${act.rangeTime}</td>
										<td><a href="<c:url value='/member/event/${act.id}'/>">${act.contentTitle}</a></td>
										<td>${act.event.address}</td>
										<td>${act.event.cost}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>


				<!-- PORK Hey Contacts -->

				<div class="row" id="porkForm" style="display:none;">
				<div class="col-sm-12">
						<h2>
							Grain<strong> Forms</strong>
						</h2>
								<style>
						.accordion_container { width: 1140px; } 
						.accordion_head {  color: #03ad5b; cursor: pointer; font-family:sans-serif; font-size: 16px; margin: 0 0 1px 0; padding: 10px 15px; font-weight: bold; display:block;} 
						.accordion_body {background-color:white;} 
						.accordion_body a {padding-right:0px;padding-top:0px;padding-left:0px;padding-bottom:0px;;font-family:sans-serif;font-size:14px; } 
						.accordion_body a:hover{text-decoration:underline;}
						.plusminus { float:left; } 
						</style>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;SafeWork SA Codes of Practice
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									
										<ul>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113693#.VLL9bCuUd8F	' target="_blank">First Aid in the Workplace (SafeWork SA)  </a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113694#.VLL92SuUd8F' target="_blank">Hazardous Manual Tasks (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113695#.VLL-GyuUd8F' target="_blank">How to Manage Work Health and Safety Risks (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113699#.VLMCiCuUd8F' target="_blank">Managing Noise and Preventing Hearing Loss at Work (SafeWork SA) </a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113700#.VLMC3CuUd8F' target="_blank">Managing the Risks of Plant in the Workplace (SafeWork SA) </a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113702#.VLMDFyuUd8F' target="_blank">Managing Electrical Risks in the Workplace (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113704#.VLMDMyuUd8F' target="_blank">Managing the Work Environment and Facilities (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113705#.VK94zSuUeSo' target="_blank">Preparation of Safety Data Sheets for Hazardous Chemicals (SafeWork SA)</a></li>
											<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113708#.VK944CuUeSo' target="_blank">Work Health and Safety Consultation Cooperation and Coordination (SafeWork SA)</a></li>
											
											</ul>
									</div>
							</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Injury Report Forms
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									
										<ul>
<!-- 											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+Capacity+information+form+(1)++FORMS.doc' target="_blank">Basic template forms for reporting injury to doctors and medical release forms</a></li> -->
												<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+WC+Letter+to+Doctor+(1)++FORMS.doc' target="_blank">Form for doctor </a></li> 
<!-- 											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK+SA+WC+Medical+release+authority+(1)+FORMS.doc' target="_blank">Medical Release form </a></li> -->
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Work Safety Guide
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
									<ul>							
											<li><a href='http://www.safework.sa.gov.au/uploaded_files/20131028_seven_steps_small_business.pdf' target="_blank">Seven steps for small busines (SafeWork SA)</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Machinery and Tools
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
																	
											<ul>
												<li><a href="http://www.safework.sa.gov.au/uploaded_files/sgAll-terrainVehicles.pdf" target="_blank">Agricultural motorbikes (SafeWork SA) </a></li>
												<li><a href="http://www.safework.sa.gov.au/uploaded_files/sgAngleGrinders.pdf" target="_blank">Angle grinder safety (SafeWork SA)</a></li>
												<li><a href="https://drive.google.com/viewerng/viewer?url=http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf" target="_blank">Disc grinder in the shearing industry (SafeWork SA)</a></li>
												<li><a href="http://www.safework.sa.gov.au/uploaded_files/Take10at10Forkliftsafety.pdf" target="_blank">Forklift safety (SafeWork SA)</a></li>
												<li><a href="http://www.safework.sa.gov.au/uploaded_files/sgBowLadders.pdf" target="_blank">Ladder safety (SafeWork SA)</a></li>
											</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Chemicals
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
																
											<ul>
												<li><a href="http://www.safework.sa.gov.au/show_page.jsp?id=113698#.VLMCMiuUd8F" target="_blank">Labelling of Workplace Hazardous Chemicals (SafeWork SA)</a></li>
												<li><a href="http://www.safework.sa.gov.au/show_page.jsp?id=113701#.VLMC9SuUd8F" target="_blank">Managing Risks of Hazardous Chemicals in the Workplace (SafeWork SA)</a></li>
												<li><a href="http://www.safework.sa.gov.au/show_page.jsp?id=112871#.VQjUeWSUdK4" target="_blank">Pesticide (Chemical) safety (SafeWork SA)</a></li>
												</ul>
									</div>
							</div>
									</h4>
								</div></div>
								
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Spray drift
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
															
											<ul>
												<li><a href="http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockMers/RC+BIOSECURITY+Reducing_Spray_Drift_and_Damage_-_Checklist.pdf" target="_blank">Reducing Spray drift and damage Checklist (Biosecurity SA) </a></ul>
									</div>
							</div>
									</h4>
								</div></div>
<!-- 								<div class="panel-group"> -->
<!-- 							<div class="panel panel-default"> -->
<!-- 								<div class="panel-heading"> -->
<!-- 									<h4 class="panel-title"> -->
<!-- 										<div class="accordion_head"> -->
<!-- 											&nbsp;Crop and Pastures Reports <span class="plusminus">+</span> -->
<!-- 										</div> -->
<!-- 										<div class="accordion_body" style="display: none;"> -->
<!-- 											<div class="panel-body"> -->

<!-- 												<ul> -->
<!-- 												</ul> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</h4> -->
<!-- 								</div> -->
<!-- 							</div> -->

								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											&nbsp;BUSINESS <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">

												<ul>
														<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/forms/Plant+Export+Operations+Stakeholder+Registration+Form+(Dept+of+Agriculture).pdf' target="_blank">Plant Export Operations Stakeholder Registration Form (Dept of Agriculture)</a></li>
									
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+Guide+2015+pdf.pdf' target="_blank"> Farm Gross Margin Guide 2015 (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+and+Enterprise+Planning+Guide+2012.pdf' target="_blank"> Farm Gross Margin and Enterprise Planning Guide 2012 (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farming+the+Business+introductory+guide.pdf' target="_blank"> Farming the Business Introductory Guide (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/Key+financial+ratios+for+farm+sustainability+Grains+Research+and+Development+Corp.pdf' target="_blank">Key financial ratios for farm sustainability Grains Research and Development Corporation (GRDC)</a></li>
											<li><a href='http://www.pir.sa.gov.au/consultancy/major_programs/new_horizons' target="_blank"> New Horizons soil and productivity program (PIRSA)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockbusiness/Mineral+exploration+and+faming+booklet+pdf+(Dept+State+Development).pdf' target="_blank">Mineral exploration and faming booklet  (Dept State Development)</a></li>
													<li><a href='http://pir.sa.gov.au/primary_industry/crops_and_pastures/crop_and_pasture_reports' target="_blank">Link to PIRSA Crop and Pasture Report March 2015</a></li>
													<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/pastures/Crop_and_Pasture_Report_2014-15_Season_March_2015+(PIRSA).pdf' target="_blank">PIRSA Crop and Pasture Report March 2015</a></li>
										
												</ul>
											</div>
										</div>
									</h4>
								</div>
							</div>
							
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;TRANSPORT
											<span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										
											<h4><strong>Regulations for heavy vehicles</strong></h4>
												
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf
											' target="_blank">Regulations for Driving Oversize or Overmass Agricultural Vehicles (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf' target="_blank">Regulations for Transporting Agricultural Vehicles as Loads (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf' target="_blank">Required documents to be carried (NHVR)</a></li>
											<li><a target="_blank" href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting+Guidelines+for+oversize+and+overmass+vechicles+and+loads.pdf'>Escorting Guidelines for Drivers and Escorting Vehicles (NHVR)</a></li>
						
											</ul>
										<h4><strong>Licence Application Forms & Driver Work Diaries</strong></h4>
											<ul>
										
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33+Upgrade+Truck+Licence.pdf
											' target="_blank">Restricted Licence Application Form MR 33 (DPTI) </a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf
											' target="_blank"> Driver Work Diary (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD+201405-0028-supplementary-work-diary-record.pdf
											' target="_blank">Supplementary Work Diary Record (NHVR) </a></li>
											
											</ul>
											<h4><strong>NHVR Live Weblinks</strong></h4>
											
										<ul>
				
											<li><a href='https://www.nhvr.gov.au/resources/faqs
											' target="_blank">https://www.nhvr.gov.au/resources/faqs
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/forms-and-services
' target="_blank">https://www.nhvr.gov.au/resources/forms-and-services
											</a></li>
											<li><a href='https://www.nhvr.gov.au/news-events/stakeholder-events
											' target="_blank">https://www.nhvr.gov.au/news-events/stakeholder-events
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/rss-feeds
											' target="_blank">https://www.nhvr.gov.au/resources/rss-feeds

											</a></li>
											
											</ul>
										
											
											</div>
							</div>
									</h4></div>
								</div>
							
										
											
							</div>
						</div></div></div>
								<script>
						$(document).ready(function () {
						    //toggle the component with class accordion_body
						    $(".accordion_head").click(function () {
						        if ($('.accordion_body').is(':visible')) {
						            $(".accordion_body").slideUp(300);
						            $(".plusminus").text('+');
						        }
						        if ($(this).next(".accordion_body").is(':visible')) {
						            $(this).next(".accordion_body").slideUp(300);
						            $(this).children(".plusminus").text('+');
						        } else {
						            $(this).next(".accordion_body").slideDown(300);
						            $(this).children(".plusminus").text('-');
						        }
						    });
						});
						
</script>
							<!-- --------------------------------------------------------------------------- -->
				
				</div></div></div></div></div></div>

				<!-- PORK Business -->

				<div class="row" id="porkBusiness" style="display:none;">
				<h2>
							Grain<strong> Biosecurity</strong>
						</h2>
						<p class="lead">
						Guidelines, checklists and links for managing spray drift and safe use of farm chemicals.
						</p>
			<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;BIOSECURITY<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
<!-- 										<h4><strong>Safe Chemical Use</strong></h4>		 -->
<!-- 										<ul> -->
<!-- 										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/Storing+and+disposing+of+chemicals+(PIRSA)+.pdf' target="_blank">Chemical Misuse brochure (PIRSA)</a></li> -->
<!-- 										</ul> -->
										<h4><strong>Spray Drift Management</strong></h4>		
										<ul>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/WorkingTogether+to+Mininise+Chemical+Spray+Drift+0913+(PIRSA).pdf' target="_blank">Working Together to Minimise Chemical Spray Drift (PIRSA)</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;Land Management web links<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
											
										<ul>
										<li><a href='http://www.pir.sa.gov.au/sarms-iiip' target="_blank">South Australian River Murray Sustainability Program (PIRSA)</a></li>
											<li><a href='http://www.pir.sa.gov.au/newhorizons' target="_blank">New Horizons (PIRSA)</a></li>
										<li><a href='http://www.daff.gov.au/about/budget/budget-2013-14/reforming-drought-programs-factsheet' target="_blank">National Drought Program Reform Factsheet (PIRSA)</a></li>
										<li><a href='http://www.daff.gov.au/agriculture-food/drought' target="_blank">Drought Programs and Rural Assistance webpage (Department of Agriculture, Fisheries and Forestry)</a></li>
										<li><a href='http://www.sardi.sa.gov.au/climate/information_for_farmers' target="_blank">South Australia Research and Development Institute's Climate Support program (PIRSA)</a></li>
									<li><a href='http://www.nrm.sa.gov.au/' target="_blank">Natural Resource Management Boards (PIRSA)</a></li>
										<li><a href='http://www.ruralsolutions.sa.gov.au/markets/community_indigenous_services' target="_blank">Property management planning (PIRSA)</a></li>
									<li><a href='http://www.legislation.sa.gov.au/listPolicies.aspx?key=E' target="_blank">Burn off regulations and dates (EPA)</a></li>
										<li><a href='http://www.cfs.sa.gov.au/site/home.jsp' target="_blank">Fire Bans and Risk Management (EPA)</a></li>
											<li><a href='http://www.environment.sa.gov.au/firemanagement/home' target="_blank">Fire Management weblink (DEWNR)</a></li>
							
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;Water Management web links<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
											
										<ul>
										<li><a href='http://www.pir.sa.gov.au/sarms-iiip' target="_blank">South Australian River Murray Sustainability Program (PIRSA)</a></li>
										<li><a href='http://www.ruralsolutions.sa.gov.au/markets/water_management' target="_blank">Water management (PIRSA)</a></li>
										<li><a href='http://www.legislation.sa.gov.au/index.aspx' target="_blank">Dam building regulations (Attorney General's Dept)</a></li>
											
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Harvesters Code of Practice 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice+CHECKLIST+(1).pdf' target="_blank">Grain Harvesting Code of Practice Checklist (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice_STICKER.pdf' target="_blank">Grain Harvesting Code of Practice Sticker (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Harvesting+Lentils+WARNING.pdf' target="_blank">Harvesting Lentils Warnings (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Licences and permits
										 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<p class="lead">These forms relate to natural resource management.  Whilst the Natural Resource Management Board endeavours to keep the links up to date, some links may occasionally change without notice. </p>
									<h4><strong>Water Management</strong></h4>	
									<p>Water licences - these are required to take water from a prescribed water resource for certain uses or above certain volumes, as specified in the relevant Water Allocation Plan (WAP).  Depending on the relevant WAP:
								<ul> <li> Taking water may include (but not be limited to) pumping, damming, gravity systems and commercial forestry. 
									</li><li> A water resource may include (but not be limited to) groundwater, creeks, rivers, wetlands, lakes and surface run-off.
									</li></ul>
									<p>Forms are specific for each prescribed area in each NRM region:</p>
								<ul>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/adelaide-and-mount-lofty-ranges-water-licences-and-permits' target="_blank">Adelaide and Mt Lofty Ranges </a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/eyre-peninsula' target="_blank">Eyre Peninsula</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/northern-and-yorke' target="_blank">Northern and Yorke</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/sa-arid-lands' target="_blank">SA Arid Lands</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/sa-murray-darling-basin' target="_blank">SA Murray Darling Basin</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/south-east' target="_blank">South East</a></li>
								
									</ul>
								
										<p>Water affecting activity (WAA) permits are required to undertake an activity that may affect a water resource or water dependent ecosystem.  Each NRM Board determines what WAAs require a permit and whether a permit is required for the WAA in all or part of the region.  Some regions have developed 'recommended practices' and 'best practice operating procedures' to reduce red tape.</p>
							<p>For constructing a well, draining/discharging into a well, using imported or effluent water, water meter testing and well drilling licences use the <a href="http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/statewide" target="_blank"> Statewide forms</a></p>
							<p>For other water affecting activities use the regional forms:</p>
							<ul>
								<li><a href='http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/water/managing-water/water-affecting-activities' target="_blank">Adelaide and Mt Lofty Ranges </a></li>
								
								<li><a href='http://www.naturalresources.sa.gov.au/alinytjara-wilurara/water/water-in-the-region/managing-water-resources' target="_blank">Alinytjara Wilurara </a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/eyrepeninsula/land-and-water/water-affecting-activities' target="_blank">Eyre Peninsula</a></li>
								<li><a href="http://www.naturalresources.sa.gov.au/kangarooisland/land-and-water/water-management/water-affecting-activities" target="_blank">Kangaroo Island</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/northernandyorke/water/water-affecting-activities' target="_blank">Northern and Yorke</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/aridlands/water/water-affecting-activities' target="_blank">SA Arid Lands</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/samurraydarlingbasin/water/managing-water-resources/water-affecting-activities' target="_blank">SA Murray Darling Basin</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/southeast/water/water-affecting-activities' target="_blank">South East</a></li>
								
									</ul>
									<p><ul><li><a href='http://www.environment.sa.gov.au/managing-natural-resources/water-use/water-planning' target="_blank">More information about water resource management </a></li></ul></p>
							
							<h4><strong>Land Management</strong></h4>	
							<h5><strong>Native Vegetation</strong></h5>	
							<ul>
								<li><a href='http://www.environment.sa.gov.au/files/05e1a003-b7db-456a-9712-9efa00f8c536/con-nv-form-clearanceapplication.pdf' target="_blank">Application to clear native vegetation </li>
								<li><a href='http://www.environment.sa.gov.au/files/2c78a1d2-74fa-4547-8383-9efa00f8ce00/con-nv-guideline-agriculture.pdf' target="_blank">Guide to native vegetation regulations for agriculture</a></li>
								<li><a href="http://www.environment.sa.gov.au/managing-natural-resources/Native_vegetation/Managing_native_vegetation" target="_blank">Other information about managing native vegetation in South Australia</a></li>
								</ul><p>
								Some ecological communities, heritage places and species of plants and animals are protected nationally under the<a href="http://www.environment.gov.au/epbc/about" target="_blank"> Environment, Protection and Biodiversity Conservation Act 1999</a> (EPBC Act). 
								There is an <a href="http://www.environment.gov.au/epbc/protected-matters-search-tool" target="_blank">interactive map-based search tool</a> on the Department of the Environment website to search for matters of national significance that may occur in your area of interest.  Non-marine ecological communities currently protected under the EPBC Act in South Australia (as of 23/02/15) include, but may not be limited to:
								</p>
								<ul>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=3' target="_blank">Buloke Woodlands of the Riverina and Murray-Darling Depression Bioregions </li>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=86&status=Endangered' target="_blank">Grey Box (Eucalyptus microcarpa) Grassy Woodlands and Derived Native Grasslands of South-eastern Australia</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=124&status=Endangered" target="_blank">Eyre Peninsula Blue Gum (Eucalyptus petiolaris) Woodland</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=36" target="_blank">Peppermint Box (Eucalyptus odorata) Grassy Woodland of South Australia</a></li>
								
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=118' target="_blank">Subtropical and Temperate Coastal Saltmarsh</li>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=31' target="_blank">Swamps of the Fleurieu Peninsula</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=26" target="_blank">The community of native species dependent on natural discharge of groundwater from the Great Artesian Basin </a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/wetlands/alphablist.pl" target="_blank">Wetlands of international significance</a></li>
								
								</ul>
								
								<h5><strong>Wildlife</strong></h5>	
						<ul>
								<li><a href='http://www.environment.sa.gov.au/files/37da2e46-09af-4ab3-acf3-9e6100c2916f/PermitApp_destroywildlife.pdf' target="_blank">Permit to destroy wildlife</li>
								<li><a href='http://www.environment.sa.gov.au/files/3d447eb6-8038-446f-acd7-a40a00e6247c/hunt-duck-permit-application-2015-form.pdf' target="_blank">Duck hunting permit</a></li>
								<li><a href="http://www.environment.sa.gov.au/Plants_and_Animals/Animal_welfare/Animal_welfare_legislation/Codes_of_practice_for_the_humane_destruction_of_wildlife_" target="_blank">Codes of practice for the humane destruction of wildlife </a></li>
								<li><a href="http://www.environment.sa.gov.au/licences-and-permits/Animals_in_the_wild_permits" target="_blank">Other wildlife-related permits and information</a></li>
								
						</ul>
						<p><ul><li><a href="http://www.environment.sa.gov.au/licences-and-permits" target="_blank">Other licences and permits administered by DEWNR</a></li></ul>
							</div></div></h4></div></div>
								</div>
								</div>
								</div></div>	
							</div></div>
							<!-- Business -->
							
								<div class="row" id="porkBioSecurity" style="display: none;">
					<div class="col-sm-12">
						<h2>
							Grain <strong> Business</strong>
						</h2>
						<p class="lead">Reports, strategies and checklists to support farm business sustainability and growth.</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											&nbsp;BUSINESS <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">

												<ul>
														<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/forms/Plant+Export+Operations+Stakeholder+Registration+Form+(Dept+of+Agriculture).pdf' target="_blank">Plant Export Operations Stakeholder Registration Form (Dept of Agriculture)</a></li>
									
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+Guide+2015+pdf.pdf' target="_blank"> Farm Gross Margin Guide 2015 (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+and+Enterprise+Planning+Guide+2012.pdf' target="_blank"> Farm Gross Margin and Enterprise Planning Guide 2012 (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/GRDC+Farming+the+Business+introductory+guide.pdf' target="_blank"> Farming the Business Introductory Guide (GRDC)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/hort_business_pdf/Key+financial+ratios+for+farm+sustainability+Grains+Research+and+Development+Corp.pdf' target="_blank">Key financial ratios for farm sustainability Grains Research and Development Corporation (GRDC)</a></li>
											<li><a href='http://www.pir.sa.gov.au/consultancy/major_programs/new_horizons' target="_blank"> New Horizons soil and productivity program (PIRSA)</a></li>
											<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockbusiness/Mineral+exploration+and+faming+booklet+pdf+(Dept+State+Development).pdf' target="_blank">Mineral exploration and faming booklet  (Dept State Development)</a></li>
										
												</ul>
											</div>
										</div>
									</h4>
								</div>
							</div>
							<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											&nbsp;Crop and Pastures Reports <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">

												<ul>
													<li><a href='http://pir.sa.gov.au/primary_industry/crops_and_pastures/crop_and_pasture_reports' target="_blank">Link to PIRSA Crop and Pastures Report March 2015</a></li>
													<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/pastures/Crop_and_Pasture_Report_2014-15_Season_March_2015+(PIRSA).pdf' target="_blank">PIRSA Crop and Pastures Report March 2015</a></li>
												</ul>
											</div>
										</div>
									</h4>
								</div>
							</div></div>
						</div>
					</div>
				</div>
	
					<!-- Work Health Safety -->
							
				<div class="row" id="grainWorkContent" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Grain<strong> Emergency Management</strong>
						</h2>
						
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Emergency Management
										<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<h5 style="text-transform:capitalize;"><strong>Checklists</strong></h5>			
									
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG" target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG" target="_blank">Heatwaves Risk (PIRSA)</a></li>
<!-- 											<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf" target="_blank">Personal Medical Details Checklist (Emergency Alert) (For printing and completing)</a></li> -->
												
										</ul><h5 style="text-transform:capitalize;"><strong>Bushfire</strong></h5>			
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf" target="_blank">Disaster Planning - Bushfire Checklist (CFS)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf" target="_blank">Disaster Planning - Earthquake Action Plan (SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf" target="_blank">Disaster Planning - Emergency Contacts(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf" target="_blank">Disaster Planning - Extreme Heat Checklist(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf" target="_blank">Disaster Planning - Flood Checklist(SES)</a></li>
											<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf" target="_blank">Extreme heat Checklist (SA Health)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf" target="_blank">Extreme Heat Guide (SA Health)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/61003298-32e8-4452-8a41-d86071e89196-Bushfire_Prevention_and_Preparedness_S2.pdf' target="_blank">Bushfire Prevention and Preparedness (PIRSA) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/97289472-5763-4ec0-ae30-1fe16850a2a1-CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf' target="_blank">Bushfire - Care of Pets and Livestock (CFS) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/d3d9575b-97b1-4d1e-acd4-520b98904157-CFS FACT SHEET - AFTER THE FIRE.pdf' target="_blank">Fact Sheet - After the Fire (CFS)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/0fed75f1-34bf-437a-84a0-736cac01d87c-Emergency animal disease preparedness.pdf' target="_blank">Emergency animal disease preparedness (PIRSA)</a></li>
												<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST%20-%20HEATWAVES%20RISK.PNG' target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												
												</ul>
											
											
										
											</div>
							</div>
									</h4>
								</div></div>
														</div>
					</div></div>
				<!-- PORK Legal -->

				<div class="row" id="porkLegal" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Grain <strong> Transport</strong>
						</h2>
						<p class="lead">
						Information on heavy vehicle regulations, vehicle escorting guidelines, driver work diaries and license application forms. Sourced from Government websites and including live links to websites for latest news and updates.
						
						</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Regulations for heavy vehicles
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf
											' target="_blank">Regulations for Driving Oversize or Overmass Agricultural Vehicles (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf' target="_blank">Regulations for Transporting Agricultural Vehicles as Loads (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf' target="_blank">Required documents to be carried (NHVR)</a></li>
											<li><a target="_blank" href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting+Guidelines+for+oversize+and+overmass+vechicles+and+loads.pdf'>Escorting Guidelines for Drivers and Escorting Vehicles (NHVR)</a></li>
						
											</ul>
										
											</div>
							</div>
									</h4></div>
								</div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;Licence Application Forms & Driver Work Diaries
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
										
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33+Upgrade+Truck+Licence.pdf
											' target="_blank">Restricted Licence Application Form MR 33 (DPTI) </a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf
											' target="_blank"> Driver Work Diary (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD+201405-0028-supplementary-work-diary-record.pdf
											' target="_blank">Supplementary Work Diary Record (NHVR) </a></li>
											
											</ul>
											</div>
							</div>
									</h4>
								</div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												&nbsp;NHVR Live Weblinks 
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
				
											<li><a href='https://www.nhvr.gov.au/resources/faqs
											' target="_blank">https://www.nhvr.gov.au/resources/faqs
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/forms-and-services
' target="_blank">https://www.nhvr.gov.au/resources/forms-and-services
											</a></li>
											<li><a href='https://www.nhvr.gov.au/news-events/stakeholder-events
											' target="_blank">https://www.nhvr.gov.au/news-events/stakeholder-events
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/rss-feeds
											' target="_blank">https://www.nhvr.gov.au/resources/rss-feeds

											</a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div></div></div></div>
								</div></div>
								
								


				<!-- PORK SURVEY -->

				<div class="row" id="porkSurvey" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Grain<strong> Surveys</strong>
						</h2>
						<p class="lead">No survey available at this time</p>
						
					</div>
					
					
				</div>
				
				
				<!-- Industrial Relations -->
				<div class="row" id="grainIndustrailContent" style="display: none;">
					<div class="col-sm-12">
						<h2>
							Grain<strong> Workplace</strong>
						</h2>
						<p class="lead">Links to MERS reports updates on superannuation, WorkCover and arrangements for different types of employment.</p>
						
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											&nbsp;General Employment and Information links <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">
												<ul>
														<li><a href='http://www.mers.com.au/farms/' target="_blank">Latest Awards and Entitlements (MERS)</a></li>
										
												<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/pig/Fair+Work+Information+Booklet+for+Agricultural+Producers.pdf' target="_blank">Fair Work Information Booklet for Agricultural Producers (NFF and Dept of Agriculture)</a></li>
											
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/mers/Member-Update-Superannuation-May-2014.pdf' target="_blank">Member Updates Superannuation May 2014 (MERS)</a></li>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/mers/Work-Experience.pdf' target="_blank">Work Experience (MERS)</a></li>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/Lucerne-Growers-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2014.pdf' target="_blank">Lucerne-Growers-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading (MERS)</a></li>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/mers/Grain-Producers-SA-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2013.doc' target="_blank">Minimum-Wage-Adjustment-SGC-and-Casual-Loading (MERS) (Downloadable link)</a></li>
										
											</ul>	
											</div>
										</div>
									</h4>
								</div>
							</div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;General
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/commmm/BioSecurity+and+on-farm+workers+(Dept+of+Agri).pdf' target="_blank">Biosecurity and on-farm workers (Dept of Agri)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/commmm/Guidelines+for+entering+farms+or+animal+facilities+(Dept+of+Agriculture)+(2).pdf' target="_blank">Guidelines for entering farms or animal facilities (Dept of Agriculture)</a></li>
									<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/commmm/Guidelines+for+visiting+farms+during+disease+outbreak+(Dept+of+Agriculture)+(1).pdf' target="_blank"> Guidelines for visiting farms during disease outbreak (Dept of Agriculture)</a></li>
							
									<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/general_whs/Farm+Safety+Fact+Sheet+(SafeWork+SA).pdf' target="_blank">Farm Safety Fact Sheet (SafeWork SA)</a></li>
									<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/general_whs/Children+On+Farms+Safety+Guide+(SafeWork+SA).pdf' target="_blank">Children on Farms Safety Guide (SafeWork SA)</a></li>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockbusiness/2014+Agricultural+Self+Assessment+Guide+PDF.pdf' target="_blank"> Agricutural Self Assessment Guide (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Employer obligations
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.mers.com.au/wp-content/uploads/2013/07/Member-Update-WHS-June-2013-What-is-reasonably-practicable-in-meeting-obligations-to-ensure-the-health-and-safety-of-workers.doc' target="_blank">Member Update WHS June 2013 (SafeWork SA) (Downloadable doc)</a></li>
									
									</ul>
								</div></div></h4></div></div>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Preparing a safe workplace
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=5892' target="_blank">Labelling of Workplace Hazardous Chemicals (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113699' target="_blank">Managing Noise and Preventing Hearing Loss at Work (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113700' target="_blank">Managing the Risks of Plant in the Workplace (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113701' target="_blank">Managing Risks of Hazardous Chemicals in the Workplace (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113702' target="_blank">Managing Electrical Risks in the Workplace (SafeWork SA)</a></li>
								
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113704' target="_blank">Managing the Work Environment and Facilities (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113705' target="_blank">Preparation of Safety Data Sheets for Hazardous Chemicals  (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113708' target="_blank">Work Health and Safety Consultation Cooperation and Coordination (SafeWork SA)</a></li>
									</ul>
								</div></div></h4></div></div>
								
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Harvesters Code of Practice 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice+CHECKLIST+(1).pdf' target="_blank">Grain Harvesting Code of Practice Checklist (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice_STICKER.pdf' target="_blank">Grain Harvesting Code of Practice Sticker (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Harvesting+Lentils+WARNING.pdf' target="_blank">Harvesting Lentils Warnings (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Harvesting hazard
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/Power+lines+warning+at+Harvest+time.pdf' target="_blank">Power Lines warning at Harvest (SafeWork SA)</a></li>
						
									</ul>
								</div></div></h4></div></div>
								
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										 &nbsp;SafeWork SA documents 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=5892' target="_blank">SafeWorkSA Codes of Practice (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/CoPHazardousManualTasks.pdf' target="_blank">SafeWork SA Hazardous Tasks Safety (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/How_to_Manage_Work_Health_and_Safety_Risks.pdf' target="_blank">Safework SA Work Safety Guide (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/youngWorkersGuide.pdf' target="_blank">SafeWork SA Young Workers Guide (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/CoPFirstAidWorkplace.pdf' target="_blank">SafeWork SA First Aid Workplace Guide (SafeWork SA)</a></li>
								
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/sevenStepsSmallBuiness.pdf' target="_blank">SafeWork SA Seven Step Safety Checklist (SafeWork SA)  </a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/notification.pdf' target="_blank">SafeWork SA Notifiable Incident Report Form (SafeWork SA) </a></li>
								
									</ul>
								</div></div></h4></div></div>
								
							
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Equipment and Vehicle Safety 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/Forklift+Safety+(Safework+SA).pdf' target="_blank">Forklift safety (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/All-terrain+Vehicles+Safety+(WorkSAfe+SA).pdf' target="_blank">All- terrain Vehicles Safety (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
							
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Worker and Personal Health  
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/livestockworkerpersonalhealth/Zoonoses+Disease+Prevention+(BioSecurity+SA).pdf' target="_blank">Zoonoses Disease Prevention (Biosecurity SA) </a></li>
							<li><a href='http://www.beyondblue.org.au/' target="_blank">Beyond Blue (Web link)</a></li>
							<li><a href='http://www.mensshed.org/home/.aspx' target="_blank">Men and Sheds (Web link)</a></li>
							
							
									</ul>
								</div></div></h4></div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;BIOSECURITY<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
<!-- 										<h4><strong>Safe Chemical Use</strong></h4>		 -->
<!-- 										<ul> -->
<!-- 										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/Storing+and+disposing+of+chemicals+(PIRSA)+.pdf' target="_blank">Chemical Misuse brochure (PIRSA)</a></li> -->
<!-- 										</ul> -->
										<h5 style="text-transform:capitalize;"><strong>Spray Drift Management</strong></h5>		
										<ul>
										<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/safeguard/WorkingTogether+to+Mininise+Chemical+Spray+Drift+0913+(PIRSA).pdf' target="_blank">Working Together to Minimise Chemical Spray Drift (PIRSA)</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;Land Management web links<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
											
										<ul>
										<li><a href='http://www.pir.sa.gov.au/sarms-iiip' target="_blank">South Australian River Murray Sustainability Program (PIRSA)</a></li>
											<li><a href='http://www.pir.sa.gov.au/newhorizons' target="_blank">New Horizons (PIRSA)</a></li>
										<li><a href='http://www.daff.gov.au/about/budget/budget-2013-14/reforming-drought-programs-factsheet' target="_blank">National Drought Program Reform Factsheet (PIRSA)</a></li>
										<li><a href='http://www.daff.gov.au/agriculture-food/drought' target="_blank">Drought Programs and Rural Assistance webpage (Department of Agriculture, Fisheries and Forestry)</a></li>
										<li><a href='http://www.sardi.sa.gov.au/climate/information_for_farmers' target="_blank">South Australia Research and Development Institute's Climate Support program (PIRSA)</a></li>
									<li><a href='http://www.nrm.sa.gov.au/' target="_blank">Natural Resource Management Boards (PIRSA)</a></li>
										<li><a href='http://www.ruralsolutions.sa.gov.au/markets/community_indigenous_services' target="_blank">Property management planning (PIRSA)</a></li>
									<li><a href='http://www.legislation.sa.gov.au/listPolicies.aspx?key=E' target="_blank">Burn off regulations and dates (EPA)</a></li>
										<li><a href='http://www.cfs.sa.gov.au/site/home.jsp' target="_blank">Fire Bans and Risk Management (EPA)</a></li>
											<li><a href='http://www.environment.sa.gov.au/firemanagement/home' target="_blank">Fire Management weblink (DEWNR)</a></li>
							
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> &nbsp;Water Management web links<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
											
										<ul>
										<li><a href='http://www.pir.sa.gov.au/sarms-iiip' target="_blank">South Australian River Murray Sustainability Program (PIRSA)</a></li>
										<li><a href='http://www.ruralsolutions.sa.gov.au/markets/water_management' target="_blank">Water management (PIRSA)</a></li>
										<li><a href='http://www.legislation.sa.gov.au/index.aspx' target="_blank">Dam building regulations (Attorney General's Dept)</a></li>
											
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Harvesters Code of Practice 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice+CHECKLIST+(1).pdf' target="_blank">Grain Harvesting Code of Practice Checklist (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Grain_harvesting_code_of_practice_STICKER.pdf' target="_blank">Grain Harvesting Code of Practice Sticker (SafeWork SA)</a></li>
								<li><a href='http://rural-resources.ruralconnect.com.au/000RuralPDF/harvesti/Harvesting+Lentils+WARNING.pdf' target="_blank">Harvesting Lentils Warnings (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Licences and permits
										 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
													<p class="lead">These forms and links relate to natural resource management.  Whilst the Natural Resource Management Board endeavours to keep the links up to date, some links may occasionally change without notice. </p>
									<h4><strong>Water Management</strong></h4>	
									<p>Water licences - these are required to take water from a prescribed water resource for certain uses or above certain volumes, as specified in the relevant Water Allocation Plan (WAP).  Depending on the relevant WAP:
								<ul> <li> Taking water may include (but not be limited to) pumping, damming, gravity systems and commercial forestry. 
									</li><li> A water resource may include (but not be limited to) groundwater, creeks, rivers, wetlands, lakes and surface run-off.
									</li></ul>
									<p>Forms are specific for each prescribed area in each NRM region:</p>
								<ul>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/adelaide-and-mount-lofty-ranges-water-licences-and-permits' target="_blank">Adelaide and Mt Lofty Ranges </a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/eyre-peninsula' target="_blank">Eyre Peninsula</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/northern-and-yorke' target="_blank">Northern and Yorke</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/sa-arid-lands' target="_blank">SA Arid Lands</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/sa-murray-darling-basin' target="_blank">SA Murray Darling Basin</a></li>
								<li><a href='http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/south-east' target="_blank">South East</a></li>
								
									</ul>
								
										<p>Water affecting activity (WAA) permits are required to undertake an activity that may affect a water resource or water dependent ecosystem.  Each NRM Board determines what WAAs require a permit and whether a permit is required for the WAA in all or part of the region.  Some regions have developed 'recommended practices' and 'best practice operating procedures' to reduce red tape.</p>
							<p>For constructing a well, draining/discharging into a well, using imported or effluent water, water meter testing and well drilling licences use the <a href="http://www.environment.sa.gov.au/licences-and-permits/water-licence-and-permit-forms/statewide" target="_blank"> Statewide forms</a></p>
							<p>For other water affecting activities use the regional forms:</p>
							<ul>
								<li><a href='http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/water/managing-water/water-affecting-activities' target="_blank">Adelaide and Mt Lofty Ranges </a></li>
								
								<li><a href='http://www.naturalresources.sa.gov.au/alinytjara-wilurara/water/water-in-the-region/managing-water-resources' target="_blank">Alinytjara Wilurara </a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/eyrepeninsula/land-and-water/water-affecting-activities' target="_blank">Eyre Peninsula</a></li>
								<li><a href="http://www.naturalresources.sa.gov.au/kangarooisland/land-and-water/water-management/water-affecting-activities" target="_blank">Kangaroo Island</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/northernandyorke/water/water-affecting-activities' target="_blank">Northern and Yorke</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/aridlands/water/water-affecting-activities' target="_blank">SA Arid Lands</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/samurraydarlingbasin/water/managing-water-resources/water-affecting-activities' target="_blank">SA Murray Darling Basin</a></li>
								<li><a href='http://www.naturalresources.sa.gov.au/southeast/water/water-affecting-activities' target="_blank">South East</a></li>
								
									</ul>
									<p><ul><li><a href='http://www.environment.sa.gov.au/managing-natural-resources/water-use/water-planning' target="_blank">More information about water resource management </a></li></ul></p>
							
							<h4><strong>Land Management</strong></h4>	
							<h5><strong>Native Vegetation</strong></h5>	
							<ul>
								<li><a href='http://www.environment.sa.gov.au/files/05e1a003-b7db-456a-9712-9efa00f8c536/con-nv-form-clearanceapplication.pdf' target="_blank">Application to clear native vegetation </li>
								<li><a href='http://www.environment.sa.gov.au/files/2c78a1d2-74fa-4547-8383-9efa00f8ce00/con-nv-guideline-agriculture.pdf' target="_blank">Guide to native vegetation regulations for agriculture</a></li>
								<li><a href="http://www.environment.sa.gov.au/managing-natural-resources/Native_vegetation/Managing_native_vegetation" target="_blank">Other information about managing native vegetation in South Australia</a></li>
								<li>Nationally protected communities and species</li>
								</ul><p>
								Some ecological communities, heritage places and species of plants and animals are protected nationally under the<a href="http://www.environment.gov.au/epbc/about" target="_blank"> Environment, Protection and Biodiversity Conservation Act 1999</a> (EPBC Act). 
								There is an <a href="http://www.environment.gov.au/epbc/protected-matters-search-tool" target="_blank">interactive map-based search tool</a> on the Department of the Environment website to search for matters of national significance that may occur in your area of interest.  Non-marine ecological communities currently protected under the EPBC Act in South Australia (as of 23/02/15) include, but may not be limited to:
								</p>
								<ul>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=3' target="_blank">Buloke Woodlands of the Riverina and Murray-Darling Depression Bioregions </li>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=86&status=Endangered' target="_blank">Grey Box (Eucalyptus microcarpa) Grassy Woodlands and Derived Native Grasslands of South-eastern Australia</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=124&status=Endangered" target="_blank">Eyre Peninsula Blue Gum (Eucalyptus petiolaris) Woodland</a></li>
								<li>Iron-grass Natural Temperate Grassland of South Australia</li>
								<li>Kangaroo Island Narrow-leaved Mallee (Eucalyptus cneorifolia) Woodland</li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=36" target="_blank">Peppermint Box (Eucalyptus odorata) Grassy Woodland of South Australia</a></li>
								
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=118' target="_blank">Subtropical and Temperate Coastal Saltmarsh</li>
								<li><a href='http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=31' target="_blank">Swamps of the Fleurieu Peninsula</a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/sprat/public/publicshowcommunity.pl?id=26" target="_blank">The community of native species dependent on natural discharge of groundwater from the Great Artesian Basin </a></li>
								<li><a href="http://www.environment.gov.au/cgi-bin/wetlands/alphablist.pl" target="_blank">Wetlands of international significance</a></li>
								
								</ul>
								
								<h5><strong>Wildlife</strong></h5>	
						<ul>
								<li><a href='http://www.environment.sa.gov.au/files/37da2e46-09af-4ab3-acf3-9e6100c2916f/PermitApp_destroywildlife.pdf' target="_blank">Permit to destroy wildlife</li>
								<li><a href='http://www.environment.sa.gov.au/files/3d447eb6-8038-446f-acd7-a40a00e6247c/hunt-duck-permit-application-2015-form.pdf' target="_blank">Duck hunting permit</a></li>
								<li><a href="http://www.environment.sa.gov.au/Plants_and_Animals/Animal_welfare/Animal_welfare_legislation/Codes_of_practice_for_the_humane_destruction_of_wildlife_" target="_blank">Codes of practice for the humane destruction of wildlife </a></li>
								<li><a href="http://www.environment.sa.gov.au/licences-and-permits/Animals_in_the_wild_permits" target="_blank">Other wildlife-related permits and information</a></li>
								
						</ul>
						<p><ul><li><a href="http://www.environment.sa.gov.au/licences-and-permits" target="_blank">Other licences and permits administered by DEWNR</a></li></ul>
							</div></div></h4></div></div>
								</div>
								</div>
								</div></div>	
							</div></div>
							</div>
						
					</div>
					
					
				</div></div></div></div></div></div>
						</div>


					</div>
				
				
				<!-- Transport -->
				<div class="row" id="porkLegal" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Grain <strong> Transport</strong>
						</h2>
						<p class="lead">
						Information on heavy vehicle regulations, vehicle escorting guidelines, driver work diaries and license application forms. Sourced from Government websites and including live links to websites for latest news and updates.
						
						</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Regulations for heavy vehicles
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf
											' target="_blank">Regulations for Driving Oversize or Overmass Agricultural Vehicles (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf' target="_blank">Regulations for Transporting Agricultural Vehicles as Loads (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf' target="_blank">Required documents to be carried (NHVR)</a></li>
											<li><a target="_blank" href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting+Guidelines+for+oversize+and+overmass+vechicles+and+loads.pdf'>Escorting Guidelines for Drivers and Escorting Vehicles (NHVR)</a></li>
						
											</ul>
										
											</div>
							</div>
									</h4></div>
								</div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Licence Application Forms & Driver Work Diaries
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33+Upgrade+Truck+Licence.pdf
											' target="_blank">Restricted Licence Application Form MR 33 </a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf
											' target="_blank">NHVR Driver Work Diary</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD+201405-0028-supplementary-work-diary-record.pdf
											' target="_blank">Supplementary Work Diary Record </a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												NHVR Live Weblinks 
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
				
											<li><a href='https://www.nhvr.gov.au/resources/faqs
											' target="_blank">https://www.nhvr.gov.au/resources/faqs
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/forms-and-services
' target="_blank">https://www.nhvr.gov.au/resources/forms-and-services
											</a></li>
											<li><a href='https://www.nhvr.gov.au/news-events/stakeholder-events
											' target="_blank">https://www.nhvr.gov.au/news-events/stakeholder-events
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/rss-feeds
											' target="_blank">https://www.nhvr.gov.au/resources/rss-feeds

											</a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div></div></div></div>
								</div></div>
								
								
				
				<!-- Training And Education -->
					<div class="row" id="grainTraining" style="display:none;">
				<div class="col-sm-12">
					<h2>Grain <strong> Training & Education</strong></h2>
					<p class="lead">
						Links to scholarships and courses for grain industry.
						</p>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Links to further education scholarships
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.adelaide.edu.au/scholarships/undergrad/isolated/ ' target="_blank">Scholarships at Adelaide University</a></li>
									<li><a href='http://www.flinders.edu.au/medicine/sites/nt-clinical-school/students/scholarships.cfm' target="_blank">Scholarships at Flinders University</a></li>
									<li><a href='http://www.tafesa.edu.au/apply-enrol/before-starting/scholarships-grants' target="_blank">Scholarships at TAFE SA</a></li>
									<li><a href='https://www.sa.gov.au/topics/education-skills-and-learning/financial-help-scholarships-and-grants/scholarships' target="_blank">Selection of SA Government scholarships</a></li>
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										&nbsp;Courses
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.adelaide.edu.au/course-outlines/106101/1/sem-2/' target="_blank">Adelaide University courses</a></li>
									<li><a href='http://www.careerharvest.com.au/' target="_blank">Career Harvest</a></li>
									
									
									</ul>
								</div></div></h4></div></div>
					</div>
					
					</div></div></div>

	</div></div></div>
	<script src="<c:url value='/ruralAssets/js/logic.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/theme.js'/>"></script>
	
	<script src="<c:url value='/ruralAssets/js/newsRSS.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/pages/index.js'/>"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	
	<script>
		(function( $ ) {
			'use strict';
			var datatableInit = function() {
				var $table = $('#eventsTable');
				$table.dataTable({
					sDom: "<'text-right mb-md'>" + $.fn.dataTable.defaults.sDom,
					"paging":false,
					"info":false,
					"order":[[1,"asc"]],
					"oLanguage": {
					    "sSearch": "Search: "}
				});
			};
			$(function() {
				datatableInit();
			});
		}).apply( this, [ jQuery ]);
	</script>
	
	<script type="text/javascript">
		google.load("feeds", "1");
		google.setOnLoadCallback(newsRSS.init);
	</script>

	<script>
		$(document).ready(function() {
			$('#grainOption').addClass("active");
			Index.initGrain();
		});
	</script>
</body>
</html>

