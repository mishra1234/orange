<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>
<sec:authorize access="hasRole('3')">
<html>
	<head>
		<meta charset="utf-8">
		<title>Profile</title>
		
		
	</head>
	<body>
		<div class="content">
		<h2>Bill Smith</h2>

        <div>
        	<div class="image" style="width: 200px; float: left;">
            	<img src="../images/farmer-dude.jpg" class="farmer" /><br />
	            <input value="Update Image" type="button" class="button " style="width: 152px; text-align: center;" />
            </div>

            	<a class="button" href="update" style="margin-right: 20px;" />Update My Details</a> <a href="#"   class="button ">Add Another Member</a>
			<div class="contactDetails" >


            	<h3>Contact Details</h3>
            	<div class="row" >
	            	<label>Address</label><div class="details">24 Happy Street<br />Adelaide Hills<br />SA 5123</div>
					<div class="clear"></div>
				</div>
            	<div  class="row clear" >
	            	<label>Phone</label><div class="details">P (08) 8123 4567<br />M 0412 345 678
                    </div>
					<div class="clear"></div>
				</div>		
            	<div  class="row clear">
	            	<label>Email</label><div class="details">bill@smith.com.au</div>
					<div class="clear"></div>
				</div>					            	
            	<div  class="row clear">
	            	<label>Password</label><div class="details"><a href="#">Change my password</a></div>
					<div class="clear"></div>
				</div>					            	

            	<h3>Personal Details</h3>
            	<div  class="row clear">
	            	<label>PIC</label><div class="details">XX123456789-1</div>
					<div class="clear"></div>
				</div>					            	

            	<div  class="row clear">
	                Which industries are you apart of?
                    	<ul>
                        	<li>Grain Grower</li>
						</ul>                    
				</div>					            	

            	<div  class="row clear">
	                Which Associations do you belong to?
                    	<ul>
                        	<li>GPSA</li>
						</ul>                    
				</div>		

			            	

        </div>
        
 
    </div>
	</body>
</html>
</sec:authorize>