<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<style>
	    #sync1 .item{
	    	background: #0c83e7;
	        padding: 80px 0px;
	        margin: 5px;
	        color: #FFF;
	        -webkit-border-radius: 3px;
	        -moz-border-radius: 3px;
	        border-radius: 3px;
	        text-align: center;
	    }
	    #sync2 .item{
	        background: #C9C9C9;
	        padding: 2px 0px;
	        margin: 2px;
	        color: #FFF;
	        -webkit-border-radius: 2px;
	        -moz-border-radius: 2px;
	        border-radius: 2px;
	        text-align: center;
	        cursor: pointer;
	    }
	    #sync2 .item h1{
	      font-size: 18px;
	    }
	    #sync2 .synced .item{
	      background: #0c83e7;
	    }
    </style>     	
</head>

<body>

<c:url var="var" value='/member/account/save'/>
	
	<!-- Jquery Validation files -->
	<script src="assets/vendor/jquery/jquery.js"></script>
	<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
	<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="assets/vendor/magnific-popup/magnific-popup.js"></script>
	<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
	<!-- Specific Page Vendor -->
	<script src="assets/vendor/jquery-validation/jquery.validate.js"></script>
	<!-- Theme Base, Components and Settings -->
	<script src="assets/javascripts/theme.js"></script>
	<!-- Theme Custom -->
	<script src="assets/javascripts/theme.custom.js"></script>
	<!-- Theme Initialization Files -->
	<script src="assets/javascripts/theme.init.js"></script>

	<!-- Examples -->
	<script src="assets/javascripts/forms/examples.validation.js"></script>
	<form:form modelAttribute="account" id="form" class="form-horizontal" method="POST" action="${var}">
	<fieldset>

	<!-- Form Name -->
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a> <a href="#"
					class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">My Account</h2>
			<p class="panel-subtitle">edit account</p>
		</header>

	<!-- Hidden ID -->
	<form:input type="hidden" id="id" 		   path="id" value=""/>
	<form:input type="hidden" id="password"    path="password" value=""/>
	<form:input type="hidden" id="salt" 	   name="salt" placeholder="salt" class="input-xlarge" path="salt"/>
	<form:input type="hidden" id="resetCode"   name="resetCode" placeholder="resetCode" class="input-xlarge" required="" path="resetCode"/>
	<form:input type="hidden" id="enabled"     name="resetCode" placeholder="resetCode" class="input-xlarge" required="" path="enabled" value="true"/>
	<form:input type="hidden" id="facebookUid" name="facebookUid" placeholder="facebookUid" class="input-xlarge" path="facebookUid" value="0"/>
	<form:input type="hidden" id="googleUid"   name="googleUid" placeholder="googleUid" class="input-xlarge" path="googleUid" value="0"/>
	<!--
	<form:input type="hidden" id="imageUrl"    name="imageUrl" placeholder="imageUrl" value="" path="imageUrl"/>
	-->

	<!-- Select from list -->
				<div id="dropzone" class="panel-body">
					<div class="panel-body">

						<div class="form-group">
							<label class="col-sm-3 control-label">Icon<span
								class="required">*</span></label>
							<div class="col-sm-9">
								<form:input id="accountIcon" name="iconUrl" type="urlIcon" class="form-control" 
									placeholder="http://" path="imageUrl" required="required"/>
							</div>
						</div>

						<!-- Upload Icon -->
						<div class="form-group">
							<label class="col-sm-3 control-label">Upload Icon</label>
							<div class="col-sm-9">
								<input id="newAccountIconUpload" type="file" name="files[]"
									class="dropzone dz-square"
									data-url="../../../file/upload?${_csrf.parameterName}=${_csrf.token}">
								<div class="panel-body">Drop files here</div>
								<div id="progress" class="progress">
									<div class="progress-bar progress-bar-primary"
										role="progressbar" style="width: 0%;">Complete</div>
								</div>
								<div id="displayAccountIcon"></div>
							</div>

						</div>
					</div>
					
					<!-- Select Multiple -->
					<div class="form-group">
						<label class="col-sm-3 control-label" for="role">Role<span
							class="required">*</span></label>
						<div class="col-sm-9">
							<form:select id="role" name="role" class="form-control"
								 path="role.id" required="required">
								<form:option value="" label="--Select one Role--" />
								<form:options items="${listOfRoles}" itemValue="id"
									itemLabel="name"></form:options>
							</form:select>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-3 control-label" for="subject">Name<span
							class="required">*</span></label>
						<div class="col-sm-9">
							<form:input id="name" name="name" type="pattern" data-plugin-maxlength="data-plugin-maxlength" maxlength="128"
								placeholder="name" class="form-control" required="required"
								path="name" />
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-3 control-label" for="email">Email<span
							class="required">*</span></label>
						<div class="col-sm-9">
							<form:input id="email" name="email" type="email"
								placeholder="email@email.com" class="form-control" data-plugin-maxlength="data-plugin-maxlength" maxlength="255"
								required="required" path="email" />
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-3 control-label" for="timezone">Timezone</label>
						<div class="col-sm-9">
							<form:input id="timezone" name="timezone" type="pattern"
								placeholder="eg.:+9:30" class="form-control" path="timezone" />
							<!-- Todo: Get a list of timezones -->
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-3 control-label" for="firstName">Firstname</label>
						<div class="col-sm-9">
							<form:input id="firstName" name="firstName" type="pattern"
								placeholder="Firstname" class="form-control" path="firstName" />
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-3 control-label" for="lastName">Lastname</label>
						<div class="col-sm-9">
							<form:input id="lastName" name="lastName" type="text"
								placeholder="lastName" class="form-control" path="lastName" />
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-3 control-label" for="postcode">postcode<span
							class="required">*</span></label>
						<div class="col-sm-9">
							<form:input id="postcode" name="postcode" type="postcode"
								placeholder="eg.:5000" class="form-control" required="required"
								path="postcode" />
						</div>
					</div>

					<!-- Text input-->

					<form:input id="status" name="status" type="hidden"
						placeholder="status" class="input-xlarge" required=""
						path="status" />
					<!--  todo: list -->
				</div>

				<!-- Button (Double) -->
						<footer class="panel-footer">
							<div class="row">
								<div class="col-sm-9 col-sm-offset-3">
								<a class="btn btn-default" 
									href="<c:url value='/member/account/${account.id}'/>">Cancel</a>
								<button type="submit" id="Save" class="btn btn-primary">save</button>
							</div>
						</div>
						</footer>
						</section>

					<!---- SCRIPTS ---->
						<script>
	(function() {
		'use strict';
		jQuery.validator.addMethod("pattern", function(value, element) {
		    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
		}, "Your data contain invalid characters");
		     
		   
		jQuery.validator.addMethod("postcode", function(value, element) {
			    return  isValidPostCode() && /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
			}, "Your data is not valid, must contain atleast 4 numeric digits");    
		
				function isValidPostCode()
				{
				var isAus = $("#postcode").val();
		
				    if (isAus.length >3 && !isNaN(isAus)) 
				        return true;
				   
				};
				
	jQuery.validator.addMethod("urlIcon", function(value, element) {
			    	return isValidIconSize();
				}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only.");
		
		 function isValidIconSize(){

			 var imgWidth = $("#theImg").width();
			 var imgHeight = $("#theImg").height();
			 var extension = $('#theImg').attr("src").split('.').pop().toLowerCase();;
			 console.log("URL: " + extension);
			 console.log("width:" + imgWidth);
			 console.log("height:" + imgHeight);
			 if (imgHeight == 200 && imgWidth == 200 && (extension == "jpg" || extension == "png"))
				return true;
			
				 };   
		// basic
		$("#form").validate({
			highlight: function( label ) {
				console.log("validation starts");
				$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function( label ) {
				$(label).closest('.form-group').removeClass('has-error');
				label.remove();
			},
			errorPlacement: function( error, element ) {
				var placement = element.closest('.input-group');
				if (!placement.get(0)) {
					placement = element;
				}
				if (error.text() !== '') {
					placement.after(error);
				}
			}
		});

	}).apply( this, [ jQuery ]);
	</script>
					<script>
    $(document).ready(function() {
        var sync1  = $("#sync1");
        var status = $("#owlStatus");
        var sync2  = $("#sync2");
        
        sync1.owlCarousel({
          singleItem:true,
          items:1,
          margin:10,
          autoHeight:true,
          itemsMobile : true,
          slideSpeed : 100,
          navigation: true,
          pagination:false,
          afterAction : syncPosition,
          responsiveRefreshRate : 200,
        });
        sync2.owlCarousel({
          items : 8,
          itemsDesktop      : [1000,8],
          itemsDesktopSmall : [979,6],
          itemsTablet       : [768,4],
          itemsMobile       : [479,2],
          pagination:false,
          responsiveRefreshRate : 100,
          afterInit : function(el){
            el.find(".owl-item").eq(0).addClass("synced");
          }
        });
        function updateResult(pos,value){
            status.find(pos).find(".result").text(value);
            document.getElementById('inputId').value =(value);
            document.getElementById('formId').checked = true;
          }
        function afterAction(){
            updateResult(".currentItem", this.sync1.currentItem);
          }
        function syncPosition(el){
          var current = this.currentItem;
          $("#sync2")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")
          if($("#sync2").data("owlCarousel") !== undefined){
            center(current)
          }
        }
        $("#sync2").on("click", ".owl-item", function(e){
          e.preventDefault();
          var number = $(this).data("owlItem");
          sync1.trigger("owl.goTo",number);
          updateResult(".currentItem", number);
        });
        function center(number){
          var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
          var num = number;
          var found = false;
          for(var i in sync2visible){
            if(num === sync2visible[i]){
              	var found = true;
            }
          }
          if(found===false){
            	if(num>sync2visible[sync2visible.length-1]){
              		sync2.trigger("owl.goTo", num - sync2visible.length+10)
            	}else{
              		if(num - 1 === -1){
                		num = 0;
              		}
              	sync2.trigger("owl.goTo", num);
            	}
          }else if(num === sync2visible[sync2visible.length-10]){
            	sync2.trigger("owl.goTo", sync2visible[10])
          }else if(num === sync2visible[0]){
            	sync2.trigger("owl.goTo", num-1)
          }
        }
      });
    </script>

					<script
						src="http://owlgraphic.com/owlcarousel/assets/js/jquery-1.9.1.min.js"></script>
					<script
						src="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.js"></script>
					<link
						href="http://owlgraphic.com/owlcarousel/assets/css/custom.css"
						rel="stylesheet">
					<link
						href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.css"
						rel="stylesheet">
					<link
						href="http://owlgraphic.com/owlcarousel/owl-carousel/owl.theme.css"
						rel="stylesheet">

					<link rel="apple-touch-icon-precomposed" sizes="144x144"
						href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-144-precomposed.png">
					<link rel="apple-touch-icon-precomposed" sizes="114x114"
						href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-114-precomposed.png">
					<link rel="apple-touch-icon-precomposed" sizes="72x72"
						href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-72-precomposed.png">
					<link rel="apple-touch-icon-precomposed"
						href="http://owlgraphic.com/owlcarousel/assets/ico/apple-touch-icon-57-precomposed.png">
					<link rel="shortcut icon"
						href="http://owlgraphic.com/owlcarousel/assets/ico/favicon.png">

					<script
						src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-collapse.js"></script>
					<script
						src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-transition.js"></script>
					<script
						src="http://owlgraphic.com/owlcarousel/assets/js/bootstrap-tab.js"></script>
					<script
						src="http://owlgraphic.com/owlcarousel/assets/js/google-code-prettify/prettify.js"></script>
					<script
						src="http://owlgraphic.com/owlcarousel/assets/js/application.js"></script></fieldset>
	
</form:form>
<!-- File Upload javascript files -->
	<link href="<c:url value='/styles/dropzone.css'/>" type="text/css" 	rel="stylesheet" />
	<script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.fileupload.js'/>"></script>
	<script src="<c:url value='/scripts/iconUploadForNewAccount.js'/>"></script>

</body>
</html>