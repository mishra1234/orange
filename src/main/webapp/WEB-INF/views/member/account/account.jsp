<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!-- start: page -->
<html>

<body>
<c:url var="var" value='/member/account/save'/>


 <script src="<c:url value='/scripts/lib/sweet-alert.min.js'/>"></script>
 <link rel="stylesheet" href="<c:url value='/scripts/lib/sweet-alert.css'/>" type="text/css" />


<div class="modal fade bs-example-modal-lg" id="modalAnim" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
			<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Are you sure?</h2>
			</header>
			<div class="panel-body">
				<div class="modal-wrapper">
					<div class="modal-icon">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="modal-text">
						<p>Are you sure that you want to delete this image?</p>
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-md-12 text-right">
						<button class="btn btn-primary modal-confirm">Confirm</button>
						<button class="btn btn-default modal-dismiss">Cancel</button>
					</div>
				</div>
			</footer>
		</section>
    </div>
  </div>
</div>

	<div role="main" class="main">
		<div class="home-intro" id="home-intro">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						 <p>Rural Connect  <em>Dashboard</em>
						 </br>
                         <strong>Today's Snapshot</strong>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="<c:url value='/member/partners'/>" class="btn btn-lg btn-primary">Meet our commercial partners</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="tabs">
						<ul class="nav nav-tabs nav-justified">
							<li class="active"><a href="#newsTab" data-toggle="tab" class="text-center">Industries / Commodity Associations </a></li>
							<li ><a href="#dashboardTab" data-toggle="tab"
								class="text-center"><i class="fa fa-star"></i> My Settings</a></li>
							
						</ul>

						<div class="tab-content">
						<div id="newsTab" class="tab-pane active">
								<h3>My Industries of interest</h3>
								 <div class="row">
                                 
                                    	<br>
                                    	<div class="checkbox-custom checkbox-success" style="margin-top:20px">
                                    	
                                    	<table width="100%">
                                    		<tr>
                                    		<td>
                                    		<c:choose>
                                    		  
						    					<c:when test="${ungatedPork == true}">
                                    			<input type="checkbox" name="checkboxG1" id="checkboxPork" class="ungatedChecks" value="70515" checked/>
                                    			</c:when>
                                    		<c:otherwise>
                                    			<input type="checkbox" name="checkboxG1" id="checkboxPork" class="ungatedChecks" value="70515"/>
                                    		</c:otherwise>
                                    		</c:choose>
                                    			<label for="checkboxPork" >PORK</label>
												</td>
                                    		
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${ungatedGrain}">
                                    			<input type="checkbox" name="checkboxG1" id="checkboxGrain" class="ungatedChecks" value="70510" checked/>
                                    			</c:when>
                                    		<c:otherwise>
                                    		     <input type="checkbox" name="checkboxG1" id="checkboxGrain" class="ungatedChecks" value="70510"/>
                                    		</c:otherwise>
                                    		</c:choose>
                                    			<label for="checkboxGrain" class="">GRAIN</label>
                                    		</td>
                                    		
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${ungatedLivestock}">
                                    			<input type="checkbox" name="checkboxG1" id="checkboxLivestock" class="ungatedChecks" value="70511" checked/>
                                    			</c:when>
                                    		<c:otherwise>
                                    		     <input type="checkbox" name="checkboxG1" id="checkboxLivestock" class="ungatedChecks" value="70511"/>
                                    		</c:otherwise>
                                    		</c:choose>
                                    			<label for="checkboxLivestock" class="">LIVESTOCK</label>	
                                    		</td>
                                    		</tr>
                                    		
                                    		
                                    		<tr>                                   		
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${ungatedHorticulture}">
                                    			<input type="checkbox" name="checkboxG1" id="checkboxH" class="ungatedChecks" value="70512" checked="checked"/>
                                    			</c:when>
                                    		<c:otherwise>
                                    		     <input type="checkbox" name="checkboxG1" id="checkboxH" class="ungatedChecks" value="70512"/>
                                    		</c:otherwise>
                                    		</c:choose>
                                    			<label for="checkboxH" class="">HORTICULTURE</label>
                                    		</td>
                                    		
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${ungatedDairy}">
                                    			<input type="checkbox" name="checkboxG1" id="checkboxD" class="ungatedChecks" value="70514" checked/>
                                    			</c:when>
                                    		<c:otherwise>
                                    		     <input type="checkbox" name="checkboxG1" id="checkboxD" class="ungatedChecks" value="70514"/>
                                    		</c:otherwise>
                                    		</c:choose>
                                    			<label for="checkboxD" class="">DAIRY</label>
                                    		</td>
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${ungatedWine}">
                                    			<input type="checkbox" name="checkboxG1" id="checkboxWinegrapes" class="ungatedChecks" value="70513" checked="checked"/>
                                    			</c:when>
                                    		<c:otherwise>
                                    		     <input type="checkbox" name="checkboxG1" id="checkboxWinegrapes" class="ungatedChecks" value="70513"/>
                                    		</c:otherwise>
                                    		</c:choose>
                                    			<label for="checkboxWinegrapes" class="">WINEGRAPES</label>
                                    		</td>
                                    		
                                    		</tr>
										</table>	
								
										</div>	
                                </div>
                                
                               <p>
                               <p>
                               <p>
                               <p>
                               
								 <div class="row" style="margin-top:50px">
                                  <h3>Gated Access to Commodity Associations</h3>
                                    	<label>I am a member of:</label>
                                    	<div class="checkbox-custom checkbox-success" style="margin-top:20px">
                                    	
                                    	<table width="100%">
                                    		<tr>
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${gatedPork == 'granted'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxPork" class="gatedChecks" value="70521" checked="checked" />
                                    			<label for="checkboxPork" >PORK SA</label>	
                                    			</c:when>
                                    			<c:when test="${gatedPork == 'requested'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxPork" class="gatedChecks" value="70521" checked="checked" disabled="disabled" />
                                    			<label for="checkboxPork" >PORK SA (requested)</label>	
                                    			</c:when>
                                    		<c:otherwise>
                                    		     <input type="checkbox" name="checkboxG1" id="gatedcheckboxPork" class="gatedChecks" value="70521"/>
                                    		     <label for="checkboxPork" >PORK SA</label>	
                                    		</c:otherwise>
                                    		</c:choose>
                                    			
												</td>
                                    		
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${gatedGrain == 'granted'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxGrain" class="gatedChecks" value="70516" checked="checked"/>
                                    			<label for="checkboxGrain" class="">GRAIN PRODUCERS SA</label>
                                    			</c:when>
                                    			<c:when test="${gatedGrain == 'requested'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxGrain" class="gatedChecks" value="70516" checked="checked" disabled="disabled" />
                                    			<label for="checkboxGrain" class="">GRAIN PRODUCERS SA (requested)</label>	
                                    			</c:when>
                                    		<c:otherwise>
                                    		    <input type="checkbox" name="checkboxG1" id="gatedcheckboxGrain" class="gatedChecks" value="70516"/>
                                    		    <label for="checkboxGrain" class="">GRAIN PRODUCERS SA</label>
                                    		</c:otherwise>
                                    		</c:choose>
                                    		</td>
                                    		
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${gatedLivestock == 'granted'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxLivestock" class="gatedChecks" value="70517" checked/>
                                    			<label for="checkboxLivestock" class="">LIVESTOCK SA</label>	
                                    			</c:when>
                                    			<c:when test="${gatedLivestock == 'requested'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxLivestock" class="gatedChecks" value="70517" disabled="disabled" checked/>
                                    			<label for="checkboxLivestock" class="">LIVESTOCK SA(requested)</label>	
                                    			</c:when>
                                    		<c:otherwise>
                                    		    <input type="checkbox" name="checkboxG1" id="gatedcheckboxLivestock" class="gatedChecks" value="70517"/>
                                    		    <label for="checkboxLivestock" class="">LIVESTOCK SA</label>	
                                    		</c:otherwise>
                                    		</c:choose>
                                    			
                                    		</td>
                                    		</tr>
                                    		
                                    		
                                    		<tr>                                   		
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${gatedHorticulture == 'granted'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxH" class="gatedChecks" value="70518" checked="checked"/>
                                    			<label for="checkboxH" class="">HORTICULTURE COALITION OF SA</label>
                                    			</c:when>
                                    			
                                    			<c:when test="${gatedHorticulture == 'requested'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxH" class="gatedChecks" value="70518" disabled="disabled" checked="checked"/>
                                    			<label for="checkboxH" class="">HORTICULTURE COALITION OF SA(requested)</label>
                                    			</c:when>
                                    		<c:otherwise>
                                    		    <input type="checkbox" name="checkboxG1" id="gatedcheckboxH" class="gatedChecks" value="70518"/>
                                    		    <label for="checkboxH" class="">HORTICULTURE COALITION OF SA</label>
                                    		</c:otherwise>
                                    		</c:choose>
                                    			
                                    		</td>
                                    		
                                    		<td>
                                    		<c:choose>
						    					<c:when test="${gatedDairy == 'granted'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxD" class="gatedChecks" value="70520" checked/>
                                    			<label for="checkboxD" class="">SA DAIRYFARMERS ASSOCIATION</label>
                                    			</c:when>
						    					<c:when test="${gatedDairy == 'requested'}">
                                    			<input type="checkbox" name="checkboxG1" id="gatedcheckboxD" class="gatedChecks" value="70520" disabled="disabled" checked/>
                                    			<label for="checkboxD" class="">SA DAIRYFARMERS ASSOCIATION(requested)</label>
                                    			</c:when>
                                    		<c:otherwise>
                                    		    <input type="checkbox" name="checkboxG1" id="gatedcheckboxD" class="gatedChecks" value="70520"/>
                                    		    <label for="checkboxD" class="">SA DAIRYFARMERS ASSOCIATION</label>
                                    		</c:otherwise>
                                    		</c:choose>
                                    			
                                    		</td>
                                    		
                                    		</tr>
										</table>	
								
										</div>	
                                </div>
							</div>
							<div id="dashboardTab" class="tab-pane">
								
                            	<form:form modelAttribute="acc" id="reg" class="form-horizontal form-bordered form-bordered" method="POST" action="${var}">
							    
							    <form:input type="hidden" id="id" 		   path="id"/>
								<form:input type="hidden" id="role"		   path="role.id"/>
								<form:input type="hidden" id="salt" 	   path="salt"/>
								<form:input type="hidden" id="resetCode"   path="resetCode"/>
								<form:input type="hidden" id="enabled"     path="enabled"/>
								<form:input type="hidden" id="facebookUid" path="facebookUid"/>
								<form:input type="hidden" id="googleUid"   path="googleUid"/>
								<form:input type="hidden" id="status"      path="status"/>
								<form:input type="hidden" id="imageUrl"    path="imageUrl"/>
								<form:input type="hidden" id="timezone"    path="timezone"/>
								<form:input type="hidden" id="name"    	   path="name"/>
								<form:input type="hidden" id="platform"    path="platform"/>
								<form:input type="hidden" id="subscribe"   path="subscribe"/>
								<form:input type="hidden" id="password"    path="password" />
								<form:input type="hidden" id="address" value="780 South Road, Glandore, Adelaide, South Australia" path="address"/>
							    <form:input type="hidden" id="pc" value="5037" path="postcode" />         
                            
                             <div class="row">
                                  <div class="form-group">
                                        <div class="col-md-12">
                                            <label>First Name<span
 									class="required">*</span></label>
                                            <form:input path="firstName"  class="form-control input-lg"  maxlength="45" type="pattern" required="required" autofocus="autofocus" placeholder="First Name" />
                                        </div>
                                    </div>
                                </div>
                               
                               
                                <form:errors path="*" /> 
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Surname*</label>
                                             <form:input path="lastName" required="required" type="pattern" maxlength="45" class="form-control input-lg" placeholder="Last Name" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>E-mail Address*</label>
                                            <form:input id="email" path="email" maxlength="128" type = "username" class="form-control input-lg" placeholder="email@email.com" required="required" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>Phone Number</label>
                                            <form:input id="mobilephone" maxlength="40" type="contact" class="form-control input-lg" path="mobilephone" placeholder="ie 08XX XXX XXX"/>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Status*</label>
                                            <select id="statusSelect" name="statusSelect" class="form-control">
												<c:forEach items="${statusComm}" var="parent">
													<c:set var="x" value="false" />
													<c:forEach items="${acc.layers_1}" var="selected">
														<c:choose>
															<c:when test="${selected.id == parent.id}">
																<c:set var="x" value="true" />
															</c:when>
														</c:choose>
													</c:forEach>
													<c:choose>
														<c:when test="${x}">
																<option value="${parent.id}" selected="selected">${parent.name}</option>
														</c:when>
														<c:otherwise>
																<option value="${parent.id}">${parent.name}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</select>
                                        </div>
                                    </div>
                                </div>                               
                                
                               <br>
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
												<label>Please select the local government area(s) in which you are involved in: (start typing)</label> 
													<select name="regionalMultiSelect" multiple data-plugin-selectTwo class="form-control populate" style="padding: 0px 0px">
														<optgroup label="SA Regional Councils">
															<option value="allcommunities">-- Select All Govenment Areas --</option>
															<c:forEach items="${councils}" var="parent">
																<c:set var="x" value="false" />
																<c:forEach items="${acc.layers_1}" var="selected">
																	<c:choose>
																		<c:when test="${selected.id == parent.id}">
																			<c:set var="x" value="true" />
																		</c:when>
																	</c:choose>
																</c:forEach>
																<c:choose>
																	<c:when test="${x}">
																			<option value="${parent.id}" selected="selected">${parent.name}</option>
																	</c:when>
																	<c:otherwise>
																			<option value="${parent.id}">${parent.name}</option>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</optgroup>
													</select>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-12">
											<div class="form-group" >
												<br> <br> <br> <input type="submit"
													value="Save"
													class="btn btn-primary pull-right push-bottom"
													data-loading-text="Saving..." style="margin-top:50px">
											</div>
										</div>
									</div>
										
								</form:form>

								</div>
								
							
							
							
						</div>
					</div>
					
				</div>	
			</div>
		</div>
	</div>

	<hr class="tall" />

	<div class="row center">
		<div class="col-md-12">
			<h2 class="short word-rotator-title">
			<!-- 	We're not the only ones <strong> <span
					class="word-rotate"
					data-plugin-options='{"delay": 3500, "animDelay": 400}'> <span
						class="word-rotate-items"> <span>excited</span> <span>happy</span>
					</span>
				</span>
				</strong> about Rural Connect... -->
			</h2>
			<!--  <h4 class="lead tall">more than 5,500 farmers use it. Meet our
				partners-->Rural Connect Partners</h2>
		</div>
	</div>
	<div class ="container">
	 <div class="row center"> 
	 <div class="col-md-12">
            <div class="owl-carousel" data-plugin-options='{"items": 4, "autoplay": false, "autoplayTimeout": 3000}'>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/primesuper_bi_2BW.jpg'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-2.png'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-3.png'/>" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="<c:url value='/ruralAssets/img/logos/logo-4.png'/>" alt="">
                </div>

               </div>
            </div>
        </div>
        </div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
		style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title" id="myModalLabel">Start Connecting Now!</h4>
				</div>
				<div class="modal-body center middle">
					<img src="<c:url value='/ruralAssets/img/placements/signup_icon.png'/>" />
				</div>
				<div class="modal-footer">
					<a href="<c:url value='/register'/>"><button type="button" class="btn btn-primary">Register now for free</button></a>
				</div>
			</div>
		</div>
	</div>

	<div class="widget" style="display: none">
		<div id="dvCurrencyContainer"><i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading</div>
		<div align="center" style="margin-top: 30px;" id="dvCurrencySource">
			<div align="center" style="margin: 0px; padding: 0px; border: 2px solid rgb(136, 136, 136); width: 135px; 
				background-color: rgb(255, 255, 255);">
				<div align="center" style="width: 100%; border-bottom-width: 1px; border-bottom-style: solid; 
					border-bottom-color: rgb(136, 136, 136); margin: 0px; padding: 0px; text-align: center; color: rgb(0, 0, 0); 
					background-color: rgb(160, 192, 48);">
					<a href="http://fx-rate.net/AUD/" style="text-decoration: none; font-size: 14px; font-weight: bold; text-align: center; color: rgb(0, 0, 0);">
					<img src="http://fx-rate.net/images/countries/au.png" style="margin: 0px; padding: 0px; border: none;">
					Australian Dollar Exchange Rate</a>
				</div>
				<script type="text/javascript" 
					src="http://fx-rate.net/fx-rates2.php?label_name=Australian Dollar Exchange Rate&lab=1&width=135&currency=AUD&cp1_Hex=000000&cp2_Hex=FFFFFF&cp3_Hex=a0c030&hbg=0&flag_code=au&length=short&label_type=currency&cp=000000,FFFFFF,a0c030&cid=EUR,CNY,USD&lang=en">
				</script>
			</div>
		</div>
	</div>


	
	<script src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
	
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<c:url value='/ruralAssets/js/helpers/gmaps.min.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/pages/index.js'/>"></script>
	
	<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('.gatedChecks').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		var usercontrol=$( this );
		  if(this.checked) {
			    	swal({
			    		  title: "Gated Access Request",
			    		  text: "Do you want to request access to this community?",
			    		  type: "info",
			    		  showCancelButton: true,
			    		  confirmButtonColor: "#DD6B55",
			    		  confirmButtonText: "Yes",
			    		  cancelButtonText: "No",
			    		  closeOnConfirm: false,
			    		  closeOnCancel: false
			    		},
			    		function(isConfirm){
			    		  if (isConfirm) {
							 	var communityID = $( usercontrol ).val();
							 	console.log("communityID is ="+communityID);
							 	var action = "connect";
								var controllerurl= "<c:url value='/member/community/request'/>";

								var jqxhr = $.ajax({
									  type: "POST",
										url: controllerurl,
										data: JSON.stringify({ CommunityID: communityID, message: action}),
										dataType: "json",
										beforeSend: function(xhr) {
										xhr.setRequestHeader("Accept", "application/json");
										xhr.setRequestHeader("Content-Type", "application/json");
										}
									    })
									  .always(function( response ) {
										    if ( response.responseText.indexOf("Success")>-1 ) {
										    		console.log($( usercontrol ).val());
										    	    $( usercontrol ).prop('checked',true);
										    	    $( usercontrol ).prop('disabled', true);
										    	    $( usercontrol ).next().text( $( usercontrol ).next().text()+"(requested)" );
											    	var z= response.responseText.split("--");
											    	swal("Connected!", z[1], "success");
										    	}
										    else
										    	{
										    		var z= response.responseText.split("--");
										    		swal("Cancelled", z[1], "error");
										    	}
										  }) ;
			    		    
			    		  } else {
			    			    swal("Cancelled", "Your request is aborted", "error");
			    		  }
			    		});
		    }
		  else
			  {
			  
			  swal({
	    		  title: "Please confirm",
	    		  text: "Do you want to disconnect from this community?",
	    		  type: "info",
	    		  showCancelButton: true,
	    		  confirmButtonColor: "#DD6B55",
	    		  confirmButtonText: "Yes",
	    		  cancelButtonText: "No",
	    		  closeOnConfirm: false,
	    		  closeOnCancel: false
	    		},
	    		function(isConfirm){
	    		  if (isConfirm) {
	    			  
					 	var communityID = $( usercontrol ).val();
					 	console.log("communityID is ="+communityID);
					 	var action = "disconnect";
						var controllerurl= "<c:url value='/member/community/request'/>";

						var jqxhr = $.ajax({
							  type: "POST",
								url: controllerurl,
								data: JSON.stringify({ CommunityID: communityID, message: action}),
								dataType: "json",
								beforeSend: function(xhr) {
								xhr.setRequestHeader("Accept", "application/json");
								xhr.setRequestHeader("Content-Type", "application/json");
								}
							    })
							  .always(function( response ) {
								    if ( response.responseText.indexOf("Success")>-1 ) {
								    		console.log($( usercontrol ).val());
								    	    $( usercontrol ).prop('checked',false);
								    	    $( usercontrol ).prop('disabled', false);
									    	var z= response.responseText.split("--");
									    	swal("Successfully disconnected!", z[1], "success");
								    	}
								    else
								    	{
								    		var z= response.responseText.split("--");
								    		swal("Cancelled", z[1], "error");
								    	}
								  }) ;
	    		    
	    		  } else {
	    			    swal("Cancelled", "Your request is aborted", "error");
	    		  }
	    		});
			  }
	});
	
	jQuery('.ungatedChecks').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		 var usercontrol=$( this );
		  if(this.checked) {
		    	swal({
		    		  title: "Please confirm",
		    		  text: "Do you want to connect to this community?",
		    		  type: "info",
		    		  showCancelButton: true,
		    		  confirmButtonColor: "#DD6B55",
		    		  confirmButtonText: "Yes",
		    		  cancelButtonText: "No",
		    		  closeOnConfirm: false,
		    		  closeOnCancel: false
		    		},
		    		function(isConfirm){
		    		  if (isConfirm) {
		    			  	var communityID = $( usercontrol ).val();
		    			  	console.log("communityID is ="+communityID);
						 	var action = "connect";
							var controllerurl= "<c:url value='/member/community/request'/>";

							var jqxhr = $.ajax({
								  type: "POST",
									url: controllerurl,
									data: JSON.stringify({ CommunityID: communityID, message: action}),
									dataType: "json",
									beforeSend: function(xhr) {
									xhr.setRequestHeader("Accept", "application/json");
									xhr.setRequestHeader("Content-Type", "application/json");
									}
								    })
								  .always(function( response ) {
									    if ( response.responseText.indexOf("Success")>-1 ) {
									    		console.log($( usercontrol ).val());
									    	    $( usercontrol ).prop('checked',true);
										    	var z= response.responseText.split("--");
										    	swal("Connected!", z[1], "success");
									    	}
									    else
									    	{
									    		var z= response.responseText.split("--");
									    		swal("Cancelled", z[1], "error");
									    	}
									  }) ;
		    		    
		    		  } else {
		    			    swal("Cancelled", "Your request is aborted", "error");
		    		  }
		    		});
		    }
		  else
			  {
		    	swal({
		    		  title: "Please confirm",
		    		  text: "Do you want to disconnect from this community?",
		    		  type: "info",
		    		  showCancelButton: true,
		    		  confirmButtonColor: "#DD6B55",
		    		  confirmButtonText: "Yes",
		    		  cancelButtonText: "No",
		    		  closeOnConfirm: false,
		    		  closeOnCancel: false
		    		},
		    		function(isConfirm){
		    		  if (isConfirm) {
		    			  var communityID = $( usercontrol ).val();
		    			  console.log("communityID is ="+communityID);
						 	var action = "disconnect";
							var controllerurl= "<c:url value='/member/community/request'/>";
							var jqxhr = $.ajax({
								  type: "POST",
									url: controllerurl,
									data: JSON.stringify({ CommunityID: communityID, message: action}),
									dataType: "json",
									beforeSend: function(xhr) {
									xhr.setRequestHeader("Accept", "application/json");
									xhr.setRequestHeader("Content-Type", "application/json");
									}
								    })
								  .always(function( response ) {
									    if ( response.responseText.indexOf("Success")>-1 ) {
									    		console.log($( usercontrol ).val());
									    		console.log(response.responseText.indexOf("Success"));
									    	    $( usercontrol ).prop('checked',false);
										    	var z= response.responseText.split("--");
										    	swal("Disconnected!", z[1], "success");
									    	}
									    else
									    	{
									    		var z= response.responseText.split("--");
									    		swal("Cancelled", z[1], "error");
									    	}
									  }) ;
		    		    
		    		  } else {
		    			    swal("Cancelled", "Your request is aborted", "error");
		    		  }
		    		});
			  }

	});
	
	
    $(document).ajaxStart(function(){
    	swal({
    		  title: "Please wait...",
    		  text:  "While your request is been processed",
    		  allowOutsideClick:false,
    		  allowEscapeKey:false,
    		  showConfirmButton: false
    		});
    });
});
	</script>



	

</body>
</html>
<!-- end: page -->
