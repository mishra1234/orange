<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>

<html>
<body>
		<div class="container">

			<div class="row">
				<div class="col-md-4">
					<div>
						<c:forEach var="bann" items="${ev.banners}" begin="0" end="0" varStatus="counter">
							<div>
								<div class="thumbnail">
									<img alt="" class="img-responsive" src="${bann.imageUrl}"
										title="${bann.subtitle}">
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
				
				
					<div class="col-md-8">

							<div class="portfolio-info">
								<div class="row">
									<div class="col-md-12 center">
										<ul>
											<li>
												<i class="fa fa-calendar"></i>${ev.startDate} ${ev.rangeTime}
											</li>
											<li>
												<i class="fa fa-money"></i>${ev.event.cost}
											</li>
										</ul>
									</div>
								</div>
							</div>

							<h4>${ev.contentTitle}</h4>
							<p class="taller">${ev.contentBody}</p>
								<a href="http://www.google.com/calendar/event?action=TEMPLATE&text=${ev.contentTitle}&dates=${ev.startDT}/${ev.endDT}&details=${ev.contentBody}&location=${ev.event.address}&trp=true&sprop=&sprop=name:${ev.contentTitle}" target="_blank">
                                  <img src="<c:url value='/images/add_to_google.png'/>" width="13%"></a>
                                <a download="calendar.ics" href="data:text/html,${ev.calData}">
                                  <img src="<c:url value='/images/add_to_outlook.png'/>" width="13%"></a>
								<span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>add to your Calendar
								
							<ul class="portfolio-details">
								<c:if test="${fn:length(listOfFiles) gt 0}">
								
									<li>
										<p><strong>Files:</strong></p>
	
										<ul class="list list-skills icons list-unstyled list-inline">
											
												<c:forEach var="file" items="${listOfFiles}" varStatus="row">
													<li>&nbsp;&nbsp;<i class="fa fa-check-circle"></i> 	<a href="${file.url}"> ${file.title}</a></li>
												</c:forEach>
										</ul>
									</li>
								</c:if>
								<c:if test="${fn:length(listOfVideos) gt 0}">
									<li>
										<p><strong>Videos:</strong></p>
	
										<ul class="list list-skills icons list-unstyled list-inline">
											
												<c:forEach var="video" items="${listOfVideos}" varStatus="row">
													<li>&nbsp;&nbsp;<i class="fa fa-check-circle"></i> 	<a href="${video.url}"> ${video.title}</a></li>
												</c:forEach>
										</ul>
									</li>
								</c:if>
								<c:if test="${fn:length(listOfWeblinks) gt 0}">
									<li>
										<p><strong>Web-links:</strong></p>
	
										<ul class="list list-skills icons list-unstyled list-inline">
											
												<c:forEach var="wls" items="${listOfWeblinks}" varStatus="row">
													<li>&nbsp;&nbsp;<i class="fa fa-check-circle"></i> 	<a href="${wls.url}"> ${wls.title}</a></li>
												</c:forEach>
										</ul>
									</li>
								</c:if>
								<li>
									<p><strong>Location:</strong></p>
									<p>${ev.event.address}</p>
								</li>
								<li>
									<p><strong>Contact details:</strong></p>
									<p>${ev.event.contactName}  ${ev.event.contactEmail}  ${ev.event.organisation}  ${ev.event.contactPhone}</p>
								</li>
							</ul>
						</div>
					</div>
			</div>

	
</body>
</html>