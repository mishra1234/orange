<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8">
		<title>Member Page</title>
		
	</head>
	<body>
	
 <div class="content">
		<h2>Events</h2>
        <p>These days are designed to exhibit and promote the latest equipment, technology and information in the agricultural industry.  Demonstrations and exhibits of modern homewares, crafts and information for a better rural lifestyle are also featured. </p>
        <p style="text-align: right;"><a href="#" class="button calendar">Calendar View</a></p>

	
	
		<div class="expandableRegion">
            <div  class="expandableButton hasDate">
                 <h4>Australian Controlled Traffic </h4>
                 <div class="icon" style="background-image:url(../images/icons/afgl.gif)"></div>
                 <p>Farming Conference</p>
                 <div class="dueDateBox">
                    <div class="dueDate">26 – 29 Aug</div>
                    <div class="dueBy">7:00am - 5:00pm</div>
                 </div>                 
                  <div class="hasMore">&nbsp;</div>
            </div>
            <div class="detail" style="display: none;">
                <p><img src="../images/placements/wheat-field-golden-grain.jpg" width="250px" class="imgRight"/><strong>When:</strong> Wednesday 4th December 2013 at 7:00-9:00

            	<p><strong>Where:</strong> Mildura, Victoria, Australia
				<p><a href="#" class="button calendar">Add to My Calendar</a>
            	
    			<p>A conference for farmers, their suppliers and advisers, organised by the Australian Controlled Traffic
    			<p> Farming Association, in partnership with the Victorian No-Till Farmers Association.
    			<p>This will be the 9th in a series of conferences that have demonstrated the profitability
    			<p> , practicality and sustainability of Controlled Traffic Farming systems.
            </div>
		</div>   		
    

<div class="expandableRegion">
            <div  class="expandableButton hasDate">
                 <h4>2014 CheeseFest</h4>
                 <div class="icon" style="background-image:url(../images/icons/gta.gif)"></div>
                 <p>Rymill Park / Murlawirrapurka Adelaide</p>
                 <div class="dueDateBox">
                    <div class="dueDate">Saturday 25 Oct</div>
                    <div class="dueBy">7:00am - 5:00pm</div>
                 </div>                 
                  <div class="hasMore">&nbsp;</div>
            </div>
            <div class="detail" style="display: none;">
                <p><img src="../images/cheese.png" width="250px" class="imgRight"/><strong>When:</strong> Wednesday 4th December 2013 at 7:00-9:00

            	<p><strong>Where:</strong> Rymill Park / Murlawirrapurka Adelaide
				<p><a href="#" class="button calendar">Add to My Calendar</a>
            	
    			<p>The 9th annual CheeseFest, recognised as Australias biggest Cheese Festival this year proudly 
    			<p>sponsored by Primary Industries and Regions SA – Premium Food and Wine from our Clean Environment.
                <p>Providing an opportunity for cheesemakers to promote and raise the awareness of the art of specialty cheesemaking.
                <p>This year there are 60 stalls featuring cheese makers from across Australia, winemakers, brewers, local chefs, media, and foodies over our 2 day program.  New events include the Premium Pavilion Degustation Lunch, Foodland Funky Fondue Lounge, The Indian Pacific Cheese Train and Talk of the Town – cheese maker talks.

            </div>
		</div>   	


<c:forEach var="event" items="${events}" varStatus="row">
			<div class="expandableRegion">
            <div  class="expandableButton hasDate">
                 <h4>${event.title}</h4>
                 <div class="icon" style="background-image:url(../images/icons/grdc.gif)"></div>
                 <p>${event.cost}</p>
                 <div class="dueDateBox">
                    <div class="dueDate">${event.time}</div>
                    <div class="dueBy">${event.time}</div>
                 </div>                 
                  <div class="hasMore">&nbsp;</div>
            </div>
            <div class="detail" style="display: none;">
                <p><img src="../images/placements/wheat-field-golden-grain.jpg" width="250px" class="imgRight"/><strong>When:</strong> Wednesday 4th December 2013 at 7:00-9:00

            	<p><strong>Where:</strong> ${event.location}
				<p><a href="#" class="button calendar">Add to My Calendar</a>
            	
    			<p>${event.description}</p>

				<p>${event.description} </p>
            </div>
		</div>   	

</c:forEach>



  </div>
	</body>
</html>