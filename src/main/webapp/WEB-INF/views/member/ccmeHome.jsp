<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<section id="page-title">
	<div class="container clearfix">
	<div class="row">
		<div class="col-md-2 center">
			<img src="<c:url value='/assets/images/logo.png'/>" height="85" />
		</div>
		<div class="col-md-5">
				<h2>Community Connect Me</h2>
				<span style="margin-top:-30px">Your community platform in one place.</span>
		</div>
		<div class="col-md-5">
		<img src="http://www.digitalmarketsquare.com.au/wp-content/uploads/CC-Me-on-Google-Play-270-px.jpg" alt="Click to download CC Me from Google Play" width="40%">
		<img src="http://www.digitalmarketsquare.com.au/wp-content/uploads/CC-Me-on-App-Store-270-px.jpg" alt="Click to download CC Me from App Store" width="40%">
		</div>
	</div>
</div>
</section>



<section id="content">
            <div class="content-wrap">
                <div class="container clearfix">

                    <div class="postcontent nobottommargin">

				<c:if test="${fn:length(recommended) gt 0}">
					<div class="center-block">
						<div class="fslider center" data-easing="easeInQuad">
							<div class="flexslider">
								<div class="slider-wrap">
									<c:forEach var="rec" items="${recommended}">
										<div class="slide"
											data-thumb="images/slider/boxed/thumbs/2.jpg">
											<c:forEach var="banner" items="${rec.banners}"
												varStatus="loop">
												<c:if test="${(loop.index) == 0}">
													<a href="<c:url value='/event/${rec.id}'/>"> <img src="${banner.imageUrl}"
														alt="Slide 2" height="100px">
														<div class="flex-caption slider-caption-bg ">${rec.contentTitle}: ${banner.title}</div>
													</a>
												</c:if>
											</c:forEach>
										</div>
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
				</c:if>

				<div id="posts" class="events small-thumbs">
                       
                       <!--  -->

					<div class="fancy-title title-center title-dotted-border topmargin">
						<h3>Upcoming Events</h3> 
					</div>

					<div id="oc-events" class="owl-carousel">
						<c:forEach var="event" items="${activities}" varStatus="loop">
							<c:if test="${(loop.index mod 2) == 0}">
								<div class="oc-item">
									<div class="ievent clearfix">
										<div class="entry-image">
											<a href="<c:url value='/event/${activities[loop.index].id}'/>"> <img src="${activities[loop.index].iconUrl}"
												alt="">
												<div class="entry-date">
													${activities[loop.index].day}<span>${activities[loop.index].month}</span>
												</div>
											</a>
										</div>
										<div class="entry-c">
											<div class="entry-title truncate">
												<h2>
													<a href="<c:url value='/event/${activities[loop.index].id}'/>">${activities[loop.index].contentTitle}</a>
												</h2>
											</div>
											<ul class="entry-meta clearfix">
												
												<li><a href="#"><i class="icon-time"></i>${activities[loop.index].rangeTime}</a></li>
												<li><a href="#"><i class="icon-map-dollar"></i>
														${activities[loop.index].event.cost}</a></li>
											</ul>
											<div class="entry-content">
												<a href="<c:url value='/event/${activities[loop.index].id}'/>" class="btn btn-default">Read More</a>
											</div>
										</div>
									</div>

									<c:if test="${fn:length(activities) gt loop.count}">

										<div class="ievent clearfix">
											<div class="entry-image">
												<a href="<c:url value='/event/${activities[loop.index+1].id}'/>"> <img
													src="${activities[loop.index+1].iconUrl}"
													alt="${activities[loop.index+1].contentTitle}">
													
													<div class="entry-date">
													${activities[loop.index].day}<span>${activities[loop.index].month}</span>
												</div>
												</a>
											</div>
											<div class="entry-c">
												<div class="entry-title">
													<h2>
														<a href="<c:url value='/event/${activities[loop.index+1].id}'/>">${activities[loop.index+1].contentTitle}</a>
													</h2>
												</div>
												<ul class="entry-meta clearfix">
													<li><span class="label label-warning">Private</span></li>
													<li><a href="#"><i class="icon-time"></i>${activities[loop.index+1].rangeTime}</a></li>
												<li><a href="#"><i class="icon-map-dollar"></i>
														${activities[loop.index+1].event.cost}</a></li>
												</ul>
												<div class="entry-content">
													 <a href="<c:url value='/event/${activities[loop.index+1].id}'/>" class="btn btn-default">Read More</a>
												</div>
											</div>
										</div>
									</c:if>


								</div>
							</c:if>
						</c:forEach>
					</div>
					
					<!-- <!-- 

					<div class="fancy-title title-center title-dotted-border topmargin">
						<h3>Next Week</h3>
					</div>

					<div id="oc-events2" class=" ">

						<c:forEach var="event" items="${activities}" varStatus="loop">
							
							<c:if test="${(loop.index mod 2) == 0}">
								<div class="oc-item">
									<div class="ievent clearfix">
										<div class="entry-image">
											<a href="<c:url value='/event/${activities[loop.index].id}'/>"> <img src="${activities[loop.index].iconUrl}"
												alt="Inventore voluptates velit totam ipsa">
												<!-- <div class="entry-date">
													10<span>Apr</span>
												</div>
											</a>
										</div>
										<div class="entry-c">
											<div class="entry-title truncate">
												<h2>
													<a href="<c:url value='/event/${activities[loop.index].id}'/>">${activities[loop.index].contentTitle}</a>
												</h2>
											</div>
											<ul class="entry-meta clearfix">
												
												<li><a href="#"><i class="icon-time"></i>${activities[loop.index].rangeTime}</a></li>
												<li><a href="#"><i class="icon-map-dollar"></i>
														${activities[loop.index].event.cost}</a></li>
											</ul>
											<div class="entry-content">
												<a href="<c:url value='/event/${activities[loop.index].id}'/>" class="btn btn-default">Read More</a>
											</div>
										</div>
									</div>

									<c:if test="${fn:length(activities) gt loop.count}">

										<div class="ievent clearfix">
											<div class="entry-image">
												<a href="<c:url value='/event/${activities[loop.index+1].id}'/>"> <img
													src="${activities[loop.index+1].iconUrl}"
													alt="Inventore voluptates velit totam ipsa">
													<!-- 
													<div class="entry-date">
														10<span>Apr</span>
													</div>
												</a>
											</div>
											<div class="entry-c">
												<div class="entry-title">
													<h2>
														<a href="<c:url value='/event/${activities[loop.index+1].id}'/>">${activities[loop.index+1].contentTitle}</a>
													</h2>
												</div>
												<ul class="entry-meta clearfix">
													<li><span class="label label-warning">Private</span></li>
													<li><a href="#"><i class="icon-time"></i>${activities[loop.index+1].rangeTime}</a></li>
												<li><a href="#"><i class="icon-map-dollar"></i>
														${activities[loop.index+1].event.cost}</a></li>
												</ul>
												<div class="entry-content">
													 <a href="<c:url value='/event/${activities[loop.index+1].id}'/>" class="btn btn-default">Read More</a>
												</div>
											</div>
										</div>
									</c:if>


								</div>
							</c:if>
						</c:forEach>

					</div>
					--> -->


					

					<script type="text/javascript">

						jQuery(document).ready(function($) {

							var today = $("#oc-events");
							//var nextWeek = $("#oc-events2");

							today.owlCarousel({
								margin: 20,
								nav: true,
								navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
								autoplay: true,
								autoplayHoverPause: true,
								dots: true,
								responsive:{
									0:{ items:1 },
									1000:{ items:2 }
								}
							});
							
// 							nextWeek.owlCarousel({
// 								margin: 20,
// 								nav: true,
// 								navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
// 								autoplay: false,
// 								autoplayHoverPause: true,
// 								dots: false,
// 								responsive:{
// 									0:{ items:1 },
// 									1000:{ items:2 }
// 								}
// 							});

						});

					</script>
                       
                       
                       <!--  -->

                        </div>

                        <!-- Pagination
                        ============================================= 
                        <ul class="pager nomargin">

                            <li class="next"><a href="#">View More</a></li>
                        </ul><!-- .pager end -->

                    </div>

                    <div class="sidebar nobottommargin col_last clearfix">
                        <div class="sidebar-widgets-wrap">

                            <div class="widget clearfix">

                                
                                <div id="post-list-footer">
                                
									<h4>Recent Posts</h4>
								<c:forEach var="info" items="${infos}" varStatus="loop">
                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href="#" class="nobg"><img src="${info.iconUrl}" alt=""></a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">${info.contentTitle}</a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li>10th July 2014</li>
                                            </ul>
                                        </div>
                                    </div>
								</c:forEach>
                                </div>
                            </div>

                            <!-- 

                            <div class="widget clearfix">

                                <h4>Recent Event in Video</h4>
                                <iframe src="//player.vimeo.com/video/103927232" width="500" height="250" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                            </div>

 -->

                        </div>
                    </div>

                </div>

            </div>

        </section><!-- #content end -->