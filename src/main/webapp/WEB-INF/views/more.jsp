<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- start: page -->
<html>

<body>

<div role="main" class="main">

    <div class="home-intro" id="home-intro">
        <div class="container">

            <div class="row">
                <div class="col-md-8">
                    <p>
                        Handle all your farming requirements right here at <em>Rural Connect</em></br>
                        <strong>Enjoy the time savings and efficiency. </strong>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="get-started">
                        <a href="javascript:startConnecting()" class="btn btn-lg btn-primary">Start Connecting Now!</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

<div class="container">

<div class="row center">
    <div class="col-md-12">
        <h1 class="short word-rotator-title">
           Rural Connect is
            <strong class="inverted">
									<span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
										<span class="word-rotate-items">
											<span>incredibly</span>
											<span>especially</span>
											<span>extremely</span>
										</span>
									</span>
            </strong>
            useful and fully loaded.
        </h1>
        <p class="featured lead">
 	We have designed this toolkit to provide much of the essential information needed by primary producers to run their business and working lives on the land - in one space. 
  	      </p>
    </div>
</div>

</div>

<div class="home-concept">
    <div class="container">
        <div class="row center">
            <span class="sun"></span>
            <span class="cloud"></span>
            <div class="col-md-2 col-md-offset-1">
                <div class="process-image" data-appear-animation="bounceIn">
                    <img src="<c:url value='/ruralAssets/img/placements/farm-1.jpg'/>" height="150" alt="" />
                    <strong>Strategy</strong>
                </div>
            </div>
            <div class="col-md-2">
                <div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="200">
                    <img src="<c:url value='/ruralAssets/img/placements/farm-5.jpg'/>"  height="150px" alt="" />
                    <strong>Planning</strong>
                </div>
            </div>
            <div class="col-md-2">
                <div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="400">
                    <img src="<c:url value='/ruralAssets/img/placements/wingrape.jpg'/>"  height="150px" alt="" />
                    <strong>Build</strong>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <div class="project-image">
                    <div id="fcSlideshow" class="fc-slideshow">
                        <ul class="fc-slides">
                            <li> <img src="<c:url value='/ruralAssets/img/placements/slide1.jpg'/>"/></li>
                            <li> <img src="<c:url value='/ruralAssets/img/placements/slide2.png'/>"/></li>
                           
                        </ul>
                    </div>
                    <strong class="our-work">Our Services</strong>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container">

    <div class="row">
        <hr class="tall" />
    </div>

</div>

<div class="container" style="text-align:left">

    <div class="row">
        <div class="col-md-12">
            <h2>You will <strong>find</strong> in Rural Connect:</h2>
            <div class="row">
                <div class="col-sm-6">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-group"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Local Government information</h4>
                            <p class="tall">Find forms and guidelines</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Surveys</h4>
                            <p class="tall">Will share our finding as well</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-google-plus"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">News and Updates</h4>
                            <p class="tall">Latest information within tweets and newspapers</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-adjust"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Alerts</h4>
                            <p class="tall">Emergency Alerts</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-film"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Radio and Video News Podcasts</h4>
                            <p class="tall">Media for Rural Communities</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Your Weather</h4>
                            <p class="tall">Weather forecast direclty from your closest station</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-bars"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Messages</h4>
                            <p class="tall">Receive newsletters from your connectedgroups</p>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <i class="fa fa-desktop"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">Network</h4>
                            <p class="tall">Engage with your community</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <!-- Theme Base, Components and Settings -->
    <script src="js/theme.js"></script>

    <!-- Specific Page Vendor and Views -->
    <script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
    <script src="js/views/view.home.js"></script>

    <!-- Theme Custom -->
    <script src="js/custom.js"></script>


    <!-- Theme Initialization Files-->
    <script src="js/theme.init.js"></script>


</div>
	
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script
		src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>

	
</body>
</html>

