<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<body>

<c:url var="var" value='/start'/>
<form:form modelAttribute="acc" class="form-horizontal" method="POST" action="${var}">
	<fieldset>
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<section class="panel">
						<header class="panel-heading">
							<br>
							<br>
							<h1>Account Unsubscribed.</h1>
						</header>
						<div class="panel-body">
							<div class="form-group">
								<h5><label class="col-md-12">The account related to "${acc.email}", has been unsubscribed successfully from receiving weekly emails from Community Connect Me.</label></h5>
								<br>
								<h5><label label class="col-md-12">Press OK to continue</label></h5>
								<br /><br />
								<div class="col-md-6">
								    <button type="submit" name="Save" value="save" class="btn btn-primary">Ok</button>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</fieldset>
</form:form>

</body>
</html>