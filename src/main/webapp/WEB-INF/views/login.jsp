<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html class="fixed">
<head>
<style type="text/css"> </style>
	<!-- Vendor CSS -->
	
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/pnotify/pnotify.custom.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		
		
</head>

<body>
<div class="content-wrap nopadding">
	<div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; 
		 background: url('images/parallax/home/Slider-A.jpg') center center no-repeat; background-size: cover;">
	</div>
	<div class="section nobg full-screen nopadding nomargin">
	    <div class="container vertical-middle divcenter clearfix">
	        <div class="row center">
	            <a href="index.html"><img src="images/logo-dark.png" alt="CC Me"></a>
	        </div>
	        <br><br><br><br><br><br>
	    	<div class="panel panel-default divcenter noradius noborder" style="max-width: 900px; background-color: rgba(255,255,255,0.93);">
               <div class="panel-body" style="padding: 20px;">
					<c:url var="var" value='/account/save'/>
					<!-- 
					<section class="page-top">
					    <div class="container">
					        <div class="row">
					            <div class="col-md-12">
					                <ul class="breadcrumb">
					                    <li><a href="<c:url value='/start'/>">Home</a></li>
					                  
					                </ul>
					            </div>
					        </div>
					        <div class="row">
					            <div class="col-md-12">
					                <h1>Sign Up</h1>
					            </div>
					        </div>
					    </div>
					</section>
 					-->
					<c:choose>
						<c:when test="${param.log == '520ce4e03480fcb95b6b6bbcf7120271'}">
						<div class ="center middle">
				     		 <div class="alert alert-danger" role="alert"><strong>Oh snap!</strong> Username / password combination error. </div>
				        </div>
				        </c:when>
					</c:choose>

					<div class="container">
					    <div class="row center middle">
					        <div class="col-xs-12 col-md-8">
					            <div class="row featured-boxes login">
					            	  <div class="col-sm-6">
					                    <div class="featured-box featured-box-quartenary">
					                        <div class="box-content">
					                           <h4>Please sign in</h4>
													<form id="form" action="<c:url value='/j_spring_security_check'/>" method="post">
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label style="text-align:left">Username or E-mail Address</label>
																	<input type="text" value="" name="username" class="form-control input-lg">
																</div>
															</div>
														</div>
														<br>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label style="text-align:left">Password</label>
																	<input type="password" name="password" value="" class="form-control input-lg">
																</div>
															</div>
														</div>
														<br>
														<div class="row">
															<input type="submit" value="Login" class="btn btn-primary">
														</div>
													</form>
					                         </div>       
					                     </div>
					                 </div>   
					                 <div class="col-sm-6">
					                    <div class="featured-box featured-box-tertiary">
					                        <div class="box-content">
					                           <h4>Forgotten Password?</h4>
					                       		<div id="button">
													<a href="javascript:reset()"><button class="tertiary" id="buttonReset">Reset Password</button></a>
													<form action="<c:url value='/action/reset'/>" style="display:none" method="post" id="resetForm">
														<input type="text" name="email" placeholder="email@domain.com" size="45" required/>
														</br>
														<button class="btn btn-primary">Reset</button>
													</form>
												</div>
												<br>
												<br>
								 				<h4>New to Community Connect Me?</h4>
												<a href="<c:url value='/register'/>"><button>Signup here</button></a>
												<br>
												<br>
												<br>
												<br>							
					                         </div>       
					                    </div>
					                </div>   
					            </div>
					        </div>
					    </div>
					</div>
				</div>
			</div>
			<div class="line line-sm"></div>
		    <div class="row center dark" style="color: #171616;">
		    	<small>Copyrights &copy; All Rights Reserved by Digital Market Square</small>
		    </div>
    	</div>
    </div>
</div>
	


	<!-- Theme Base, Components and Settings -->
		
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
    <script src="<c:url value='/scripts/vendor/jquery.ui.widget.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.iframe-transport.js'/>"></script>
	
	
	<!-- Specific Page Vendor -->
		<script src="<c:url value='/assets/vendor/jquery-validation/jquery.validate.js'/>"></script>
		<script src="<c:url value='/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js'/>"></script>
		<script src="<c:url value='/assets/vendor/pnotify/pnotify.custom.js'/>"></script>
	
	<script type="text/javascript">
	
	$(document).ready(function(){
		
		$('#buttonReset').click(function(){
			
			$('#buttonReset').hide();
			$('#resetForm').show();
			
		});
		
	});
	
	</script>
	
	
	
</body>