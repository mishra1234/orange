<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- start: page -->
<html>
<head>
<style>
.collapsibleList li{
  list-style-image:url('button.png');
  cursor:auto;
}

li.collapsibleListOpen{
  list-style-image:url('button-open.png');
  cursor:pointer;
}

li.collapsibleListClosed{
  list-style-image:url('button-closed.png');
  cursor:pointer;
}
</style>
</head>
<body>

	<div role="main" class="main">

		<section class="page-top">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1>
							<img alt="Rural Connect" height="60"
								src="<c:url value='/ruralAssets/img/placements/livestock.jpg'/>">
							LIVESTOCK SA
						</h1>
					</div>
					<div class="col-md-4">
						<div class="get-started">
							<a href="javascript:startConnecting()"
								class="btn btn-lg btn-primary">Gated Access Unlocked</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="container">

			<ul class="nav nav-pills">
				
				<li class="pork" id="aboutPorkTab"><a
					href="javascript:aboutPork()">News</a></li>
				
				<li class="pork" id="porkCommTab"><a
					href="javascript:porkComm()">Communications</a></li>
				<li class="pork" id="porkEventsTab"><a
					href="javascript:porkEvents()">Events</a></li>
				<!-- <li class="pork" id="porkKeyTab"><a
					href="javascript:porkKeyContacts()">Key Contacts</a></li> -->
				<li class="pork" id="porkBusinesTab">
				<!--  <a
					href="javascript:porkBusiness()">Biosecurity</a></li>
				<li class="pork" id="porkBioSecurityTab"><a
					href="javascript:porkBioSecurity()">Business</a></li>
					<li class="pork" id="grainIndustrailTab"><a href="javascript:grainIndustrail()">Industrial Relations</a></li>
			
					<li class="pork" id="porkFormTab"><a
					href="javascript:porkForm()">Forms</a></li>
				
				<li class="pork" id="porkLegalTab">				<a href="javascript:porkLegal()">Transport</a></li>
				
				<li class="pork" id="porkOtherTab">				<a href="javascript:grainTraining()">Training & Education</a></li>
				
					<li class="pork" id="grainWorkTab"><a href="javascript:grainWork()">Work Health Safety </a></li> 
				-->
				
			</ul>

			<hr />

			<div id="porkContent">
				<div class="row" id="porkAbout">
					<div class="col-sm-7">
						<h2>
							Latest <strong>News</strong>
						</h2>
						
					<div class="widget">
							<div class="widget-extra themed-background-dark"></div>
							<div class="widget">

								<div class="widget-extra">
									<!-- Timeline Content -->
									<div id="dvNewsRSS" class="timeline"></div>
									<!-- END Timeline Content -->
								</div>
							</div>
						</div>
						
						</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a class="btn btn-primary"
							href="https://s3.amazonaws.com/ccmeresources/000RuralPDF/EAD-Risk-Management-Manual.pdf" target="_blank">EAD Risk Management Manual</a> 
							</br> </br>
							<a class="btn btn-primary"
							href="https://s3.amazonaws.com/ccmeresources/000RuralPDF/Emergency+animal+disease+preparedness.pdf" target="_blank">Emergency animal disease preparedness</a> </br> </br> 
						
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive"
							src="https://s3.amazonaws.com/ccmeresources/000RuralPDF/pig/RC+COMMS+LIVESTOCK+SA+BANNER+(1).PNG">
					</div>
				</div>

				<!-- PORK COMMITTEE -->

				<div id="porkCommittee" style="display:none;">
					<div class="row pork">
						<div class="col-sm-7">
							<h2>
								Committee<strong> 2014</strong>
							</h2>
							<p class="lead">
							<h3>Pork SA Committee for 2014</h3>
							<ul>
								<li>Chair: Matthew Starick</li>
								<li>Vice Chairs: Andrew Johnson and Mark McLean</li>
								<li>Secretary/Treasurer: Tony Richardson</li>
								<li>Executive Committee: Barry Lloyd and Peter Brechin</li>
								<li>Committee Members: Garry Tiss, Nick Lienert, David Reu,</li>
								<li>Butch Moses, Rod Hamann, Christine Sapwell.</li>
							</ul>
							</p>
							<p class="tall" style="text-align: justify"></p>
						</div>
						<div class="col-sm-4 col-sm-offset-1 push-top">
							<a
								href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf" target="_blank">Pork
								SA Membership Form (live link)</a> <a
								href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf" target="_blank">Pork
								SA Charter (live link)</a> Membership forms are also available from
							Andy Pointon, Pork SA Executive Officer <a
								href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
							or on 0418 848 845
						</div>
						<div class="col-sm-4 col-sm-offset-1 push-top">
							<img class="img-responsive"
								src="<c:url value='/ruralAssets/img/placements/committee.jpg'/>">
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>

							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Andrew<strong> Johnson: </strong>
							</h2>
							<h4>Vice Chair</h4>
							<p style="text-align: justify;">Contact M: 0427849467 email:
								asjohnson.mtb@bigpond.com Piggery/Business profile – Director of
								Mt. Boothby Pastoral a mixed family farming operation spread
								over multiple sites in SA, producing beef, lamb, wool, mixed
								cropping, lucerne seed and have a multi-site farrow to finish
								pork production operation.Andrew holds several positions in
								industry bodies both at the state and nationally. These include:
							</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Pork SA Vice Chairman</li>
								<li><i class="fa fa-check"></i> APL Director</li>
								<li><i class="fa fa-check"></i> PPSA Councillor</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> APL Animal Welfare and
									Quality Assurance Committee Chairman</li>
								<li><i class="fa fa-check"></i> Vice Chairman Nuffield
									Australia</li>
							</ul>

						</div>
					</div>
					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Mark<strong> McLean: </strong>
							</h2>
							<h4>Vice Chair</h4>
							<p style="text-align: justify;">Contact– M: 0427 138 919
								Email: mclean_mark@bigpond.com Piggery/Business profile - Owner
								and General Manager of Riverhaven Enterprises, 700 sow farrow to
								finish pig farm combined with horticultural interests.
								Riverhaven Enterprises is a family farm which has been operating
								in the Riverland for over 50 years. The business operates using
								principles of sustainability and resource re-use to complement
								its various operations. It composts straw-based pig shelter
								bedding which is sold externally and used on its Citrus and
								Olive orchards to provide nutrient value and increase soil water
								holding capacity to reduce water requirements. The company
								constantly review procedures and production techniques to try
								and remain competitive and achieve good outcomes for our pork
								quality. Modern housing and innovation with genetics and
								husbandry are vital to ensure long-term pig farm profitability.
								The family has been co-founders of Top Multiplier, 1,000 sow
								farrow to finish pig unit at Bower. Mark has wide industry
								interests including:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Commitment to assist in
									helping achieve a sustainable future for all Australian pig
									farmers</li>
								<li><i class="fa fa-check"></i> Commitment to see
									Australian pork consumption grow and the national industry
									remain profitable</li>
								<li><i class="fa fa-check"></i> Improving the image of
									farming and encourage future generations to participate</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> Overseas travel to
									investigate different methods of farming and food marketing *
									APL Delegate since 2010</li>
								<li><i class="fa fa-check"></i> Committee Member of APL
									Specialist Group 1 – ‘Marketing, Supply Chain and Product

									Quality’.</li>
							</ul>

						</div>
					</div>
					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Tony <strong>Richardson</strong>
							</h2>
							<h4>Secretary/Treasurer: Tony</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Barry Lloyd <strong>and Peter Brechin</strong>
							</h2>
							<h4>Executive Committee</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">Garry Tiss, Nick Lienert, David Reu,
								Butch Moses, Hamann, Christine Sapwell</h2>
							<h4>Committee Members</h4>
						</div>
					</div>

					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Garry <strong>Tiss</strong>
							</h2>
							<h4>Committee Members</h4>
							<p style="text-align: justify;">Tiss: Contact – M: 0418817703
								Email: gtiss@bigpond.net .au Piggery/Business profile - Has
								interest in two piggeries with more than 2000 sows. T & D Pig

								Marketers (agents for Elders Ltd) specialising in marketing live
								pigs and organising contracts with processors for past 36 years.
								Particular interests of Garry include trying to bring pork

								processors and growers closer together for a better outcome for
								both parties i.e. pig pricing structures and carcase gradings.

								Butch Moses: Contact – M: 0428 64 2243 Email:
								butch.moses@internode.on.net Piggery/Business profile - Salt
								Lake Bacon. 550 sows farrow to finish (sow stall free). Butch
								has a wide interest in the industry and is involved as:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Commitment to assist in
									helping achieve a sustainable future for all Australian pig
									farmers</li>
								<li><i class="fa fa-check"></i> Top Pork Board Director</li>
								<li><i class="fa fa-check"></i> * ex SABOR Board Director</li>
								<li><i class="fa fa-check"></i> Pork NLIS Chairman</li>
								<li><i class="fa fa-check"></i> ex Chairman SAFF Pork
									Committee</li>
							</ul>
						</div>
					</div>


					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Rod<strong> Hamann</strong>
							</h2>
							<h4>Committee Members</h4>
							<p style="text-align: justify;">Piggery/Business profile: Rod
								is a South Australian who is a past graduate of Roseworthy

								Agricultural College. Through an initial career with Pig
								Improvement Company that took him to the UK and then a transfer
								to the USA, Rod has now lived and worked in the pig industries

								of three continents, plus had the opportunity to travel and
								consult in various capacities in South America, Europe and the
								USA. Prior to returning to Australia in 1999 to establish an Ag
								consulting business, he held senior executive positions within
								Murphy Family Farms (the largest pig producer in the world) and

								Heartland Pork Enterprises. In the latter role, as Chief
								Operating Officer he was directly responsible to the Chairman
								for all operational areas for this 60,000-sow Production and

								Genetics operation. He currently has various consulting
								positions, but has a primary role and responsibility as the

								Chief Executive Officer / Managing Director of Australian Pork
								Farms Group Limited, which includes a number of pig production
								businesses including Wasleys and Sheaoak Piggeries. The group
								has an investment interest in Big River Pork; plus they are a
								core foundation investor of the Pork CRC.Industry interests and
								positions include:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Director Auspork marketing
									group</li>
								<li><i class="fa fa-check"></i> Director Big River Pork
									Abattoir</li>
								<li><i class="fa fa-check"></i> Director Pork CRC and
									member of Pork CRC R & D Sub Committee</li>
								<li><i class="fa fa-check"></i> Director Porkscan</li>
								<li><i class="fa fa-check"></i> Member SA Pig Industry
									Advisory Group (PIAG)</li>
							</ul>
						</div>
					</div>


					<div class="row pork">
						<div class="col-md-2">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" height="300" class="img-responsive"
											src="<c:url value='/ruralAssets/img/placements/sil.png'/>">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-10">

							<h2 class="shorter">
								Christine <strong>Sapwell</strong>
							</h2>
							<h4>Pork SA Secretary/Treasurer:</h4>
							<p style="text-align: justify;">PContact M: 0408 800 011
								Email: Piggery/Business profile: Owner of Norsap which commenced
								in the pig breeding industry in1968. The piggery has recently
								come to an end of it’s useful life with the last pigs leaving

								the property on 26th November2012. Christine still retains an
								interest in the Australian Pork Farms Group. Christine has been
								involved in Industry organizations from the early 1970’s with
								involvement in SA Farmers Federation, Australian Pork
								Corporation, Delegate to Pork Council of Australia, Australian
								Pork Limited, Pig Industry Advisory Group (current), & the Pig
								Industry Development Board. She has also escorted a number of
								Pig Study Tours both overseas and within Australia. Christine is
								keen to see pork producers well represented to Government and
								legislators and are assisted to cope with change to enable
								survival in the Other current industry positions held by
								Christine include:</p>

							<ul class="list icons list-unstyled">
								<li><i class="fa fa-check"></i> Shareholder within the
									Australian Pork Farms Group which has piggery interests on the

									Adelaide Plains and Murray Bridge areas</li>
								<li><i class="fa fa-check"></i> Treasurer of the Ronald J
									Lienert Memorial Scholarship Fund whose aim is to provide a

									bursary to a student to encourage their involvement in a
									research project</li>

							</ul>
						</div>
					</div>



				</div>

				<!-- PORK MEMBERSHIP -->

				<div class="row" id="porkMembership" style="display:none;">
					<div class="col-sm-7" style="display:none;">
						<h2>
							Pork<strong> Charter</strong>
						</h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<p class="tall" style="text-align: justify">
							<iframe
								src="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf"
								style="width: 100%; height: 100%;" frameborder="0"></iframe>
						</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf" target="_blank">Pork
							SA Membership Form (live link)</a> <a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf" target="_blank">Pork
							SA Charter (live link)</a> Membership forms are also available from
						Andy Pointon, Pork SA Executive Officer <a
							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
						or on 0418 848 845
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive" src="img/placements/china-pork.jpg">
					</div>
				</div>


				<!-- PORK Communications -->

				<div class="row" id="porkComm" style="display:none;">
					<div class="col-sm-12">
						<h2>Livestock <strong> Communications</strong></h2>	
						<hr>
						<div class="toggle" data-plugin-toggle="">
						<c:forEach var="act" items="${infos}" begin="0" end="2" varStatus="row">
							<section class="toggle">
								<label>
									<div class="img-thumbnail">
										<img class="avatar" alt="" src="<c:url value='${act.iconUrl}'/>" width="45px">
									</div>
									${act.contentTitle}
								</label>
								<div class="toggle-content" style="display: none;">
									<div class="detail" style="display: block;">
										<c:forEach var="banner" items="${act.banners}" begin="0" end="0" varStatus="counter">
											<p><img src="<c:url value='${banner.imageUrl}'/>" width="250px" height="150" class="imgRight" />
											<p align="left" style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"> 
											 ${act.contentBody}
											</p></br></br></br></br>
											<div class="post-meta"> 
												<a href="<c:url value='/member/info/${act.id}'/>" class="btn btn-xs btn-primary pull-right">More info...</a>
											</div></br>
											</p>
										</c:forEach>
									</div>
								</div>
							</section>
						</c:forEach>
						</div>
					</div>
				</div>

				<!-- PORK Events -->

				<div class="row" id="porkEvents" style="display:none;">
					<h2>
						LIVESTOCK <strong> Events</strong>
					</h2>
					<div class="col-md-12">
						<ul class="history">
							<c:forEach var="act" items="${activities}" varStatus="row">
								<li data-appear-animation="fadeInUp">
									<div class="thumb"><img src="${act.iconUrl}" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<div class="row">
												<div class="col-md-7">
													<div class="post-content">
														<h4>${act.contentTitle}</h4><br><br>
														<p align="left" style="display: block; display: -webkit-box; max-width: 100%; height: 43px; margin: 0 auto; font-size: 14px; line-height: 1; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"> 
														 ${act.contentBody}
														</p>
													</div>
												</div>
												<div class="col-md-5">
													<c:forEach var="banner" items="${act.banners}" begin="0" end="0" varStatus="counter">
														<div class="post-image">
															<img src="<c:url value='${banner.imageUrl}'/>" width="250px" class="imgRight" />
														</div>
													</c:forEach>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="post-meta">
														<span><i class="fa fa-calendar"></i> ${act.startDate} at ${act.rangeTime}</span> 
														<a target="_blank" href="<c:url value='/member/event/${act.id}'/>" class="btn btn-xs btn-primary pull-right">More info...</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>


				<!-- PORK Hey Contacts -->

				<div class="row" id="porkKeyContact" style="display:none;">
					<div class="col-sm-7">
						<h2>
							Pork<strong> Key Contact</strong>
						</h2>
						<p class="lead">Pork SA is the peak industry organisation
							representing pork producers and agribusinesses associated with
							the South Australian pork industry. Pork SA aims to provide
							leadership, policy, advocacy and services to support the SA pork
							industry.</p>
						<p class="tall" style="text-align: justify">In 2005 – 06, the
							South Australian livestock industry contributed $2.7 billion or
							27% of South Australia's gross food revenue. The SA pig meat
							industry contributed $573 million or 21% of the total South
							Australia has about 550 pig farmers with the main pig producing
							regions the Murray lands, Barossa, Yorke, and Mid North and
							Northern. Membership of Pork SA is free to producers who pay
							levies into the Pig Industry Fund. Producer members are also able
							to nominate associate members, again with no joining fee, in the
							anticipation that farm managers and staff will join Pork SA and
							keep informed about Agribusinesses who supply and support SA pork
							producers, such as feedmillers, premix manufacturers and
							processors, can join as members upon payment of a nominal fee.

							Under the terms of incorporation, Pork SA is required to maintain
							its own register of members. Pork SA recognises that while
							profitability is critical, it is just as important to have the
							confidence of the community and consumers in how responsibly the
							industry operates in areas such as welfare, environmental
							sustainability and food safety. Pork SA will specifically address
							such matters and enable the industry to be ahead of the issues
							rather than reacting in an uncoordinated manner.</p>
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20SA%20Membership%20rego%20form.pdf" target="_blank">Pork
							SA Membership Form (live link)</a> <a
							href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/comitteemembers/Pork%20South%20Australia_Charter.pdf" target="_blank">Pork
							SA Charter (live link)</a> Membership forms are also available from
						Andy Pointon, Pork SA Executive Officer <a
							href="mailto:andypointon.food@iinet.com.au">andypointon.food@iinet.com.au</a>
						or on 0418 848 845
					</div>
					<div class="col-sm-4 col-sm-offset-1 push-top">
						<img class="img-responsive" src="img/placements/china-pork.jpg">
					</div>
				</div>


				<!-- PORK Business -->

				<div class="row" id="porkBusiness" style="display:none;">
				<h2>
							Livestock<strong> BIOSECURITY</strong>
						</h2>
						<p class="lead">
						Guidelines, checklists and links for managing animal health issues and safe use of farm chemicals.
						</p>
						<style>
						.accordion_container { width: 1140px; } 
						.accordion_head {  color: #03ad5b; cursor: pointer; font-family:sans-serif; font-size: 16px; margin: 0 0 1px 0; padding: 10px 15px; font-weight: bold; display:block;} 
						.accordion_body {background-color:white;} 
						.accordion_body a {padding-right:0px;padding-top:0px;padding-left:0px;padding-bottom:0px;;font-family:sans-serif;font-size:14px; } 
						.accordion_body a:hover{text-decoration:underline;}
						.plusminus { float:left; } 
						</style>
						
						
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> BIOSECURITY<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
												<h4><strong>Animal Health</strong></h4>		
										<ul>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Is+It+Fit+To+Load+-+Guide+(Meat+and+Livestock+Australia).pdf' target="_blank">Is It Fit to Load Guide (Meat and Livestock Australia)</a></li>
											<li><a href='http://www.agriculture.gov.au/' target="_blank">Australian Department of Agriculture  </a></li>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockMers/Preparing-your-business-to-survive-an-emergency-animal-disease-outbreak.pdf' target="_blank">Preparing your business to survive an emergency animal disease outbreak </a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Bovine+tuberculosis+freedom+(Dept+of+Agriculture).pdf' target="_blank">Bovine tuberculosis freedom (Dept of Agriculture) </a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Brucella-abortus+eridication+(Dept+of+Agriculture)+(2).pdf' target="_blank">Brucella-abortus eridication (Dept of Agriculture) </a></li>
									
										</ul>
										<h4><strong>Safe Chemical Use</strong></h4>		
										<ul>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Storing+and+disposing+of+chemicals+(PIRSA)+.pdf' target="_blank">Storing and disposing of Chemicals (PIRSA)</a></li>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockMers/RC+BIOSECURITY+Reducing+harm+to+Honey+Bees+from+Pesticides.pdf' target="_blank">Reducing Harm to Honeybees from Pesticides (PIRSA) (Web link)</a>
										
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Emergency Management
										<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<h4><strong>Checklists</strong></h4>			
									
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG" target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG" target="_blank">Heatwaves Risk (PIRSA)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf" target="_blank">Personal Medical Details Checklist (Emergency Alert)</a></li>
												
										</ul><h4><strong>Bushfire</strong></h4>			
										<ul>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf" target="_blank">Disaster Planning - Bushfire Checklist (CFS)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf" target="_blank">Disaster Planning - Earthquake Action Plan (SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf" target="_blank">Disaster Planning - Emergency Contacts(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf" target="_blank">Disaster Planning - Extreme Heat Checklist(SES)</a></li>
												<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf" target="_blank">Disaster Planning - Flood Checklist(SES)</a></li>
											<li><a href="http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf" target="_blank">Extreme heat Checklist (SA Health)</a></li>
												<li><a href="ttp://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf" target="_blank">Extreme Heat Guide (SA Health)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/61003298-32e8-4452-8a41-d86071e89196-Bushfire_Prevention_and_Preparedness_S2.pdf' target="_blank">Bushfire Prevention and Preparedness (PIRSA) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/97289472-5763-4ec0-ae30-1fe16850a2a1-CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf' target="_blank">Bushfire - Care of Pets and Livestock (CFS) </a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/d3d9575b-97b1-4d1e-acd4-520b98904157-CFS FACT SHEET - AFTER THE FIRE.pdf' target="_blank">Fact Sheet - After the Fire (CFS)</a></li>
												<li><a href='https://s3.amazonaws.com/ccmeresources/0fed75f1-34bf-437a-84a0-736cac01d87c-Emergency animal disease preparedness.pdf' target="_blank">Emergency animal disease preparedness (PIRSA)</a></li>
												<li><a href='http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST%20-%20HEATWAVES%20RISK.PNG' target="_blank">Biosecurity Emergency hotlines (PIRSA)</a></li>
												
												</ul>
											
											
										
											</div>
							</div>
									</h4>
								</div></div>
										
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> Land Management web links<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
											
										<ul>
										<li><a href='http://www.pir.sa.gov.au/sarms-iiip' target="_blank">South Australian River Murray Sustainability Program (PIRSA)</a></li>
											<li><a href='http://www.pir.sa.gov.au/newhorizons' target="_blank">New Horizons (PIRSA)</a></li>
										<li><a href='http://www.daff.gov.au/about/budget/budget-2013-14/reforming-drought-programs-factsheet' target="_blank">National Drought Program Reform Factsheet (PIRSA)</a></li>
										<li><a href='http://www.daff.gov.au/agriculture-food/drought' target="_blank">Drought Programs and Rural Assistance webpage (Department of Agriculture, Fisheries and Forestry)</a></li>
										<li><a href='http://www.sardi.sa.gov.au/climate/information_for_farmers' target="_blank">South Australia Research and Development Institute's Climate Support program (PIRSA)</a></li>
									<li><a href='http://www.nrm.sa.gov.au/' target="_blank">Natural Resource Management Boards (PIRSA)</a></li>
										<li><a href='http://www.ruralsolutions.sa.gov.au/markets/community_indigenous_services' target="_blank">Property management planning (PIRSA)</a></li>
									<li><a href='http://www.legislation.sa.gov.au/listPolicies.aspx?key=E' target="_blank">Burn off regulations and dates (EPA)</a></li>
										<li><a href='http://www.cfs.sa.gov.au/site/home.jsp' target="_blank">Fire Bans and Risk Management (EPA)</a></li>
											<li><a href='http://www.environment.sa.gov.au/firemanagement/home' target="_blank">Controlled burns on Crown (the Department of Environment, Water Natural Resources)</a></li>
							
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> Water Management web links<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
											
										<ul>
										<li><a href='http://www.pir.sa.gov.au/sarms-iiip' target="_blank">South Australian River Murray Sustainability Program (PIRSA)</a></li>
										<li><a href='http://www.ruralsolutions.sa.gov.au/markets/water_management' target="_blank">Water management (PIRSA)</a></li>
										<li><a href='http://www.legislation.sa.gov.au/index.aspx' target="_blank">Dam building regulations (PIRSA)</a></li>
											
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
								
						</div></div>
						
				
	</div></div></div>
	<!-- Business Section -->
				<div class="row" id="porkBioSecurity" style="display: none;">
					<div class="col-sm-12">
						<h2>
							Livestock <strong> Business</strong>
						</h2>
						<p class="lead">Reports, strategies and checklists to support
							farm business sustainability and growth.</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											BUSINESS <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">

												<ul>
														<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Plant+Export+Operations+Stakeholder+Registration+Form+(Dept+of+Agriculture).pdf' target="_blank">Plant Export Operations Stakeholder Registration Form (Dept of Agriculture)</a></li>
									
													<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/Farm_finance_strategy_2007.pdf' target="_blank">Farm Finance Strategy 2007 (SAFF)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+Guide+2015+pdf.pdf' target="_blank"> Farm Gross Margin Guide 2015 (GRDC)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farm+Gross+Margin+and+Enterprise+Planning+Guide+2012.pdf' target="_blank"> Farm Gross Margin and Enterprise Planning Guide 2012 (GRDC)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/GRDC+Farming+the+Business+introductory+guide.pdf' target="_blank"> Farming the Business Introductory Guide (GRDC)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/How_does_my_farm_business_compare+DEWNR+doc.pdf' target="_blank">How does my farm business compare (DEWNR)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/hort_business_pdf/Key+financial+ratios+for+farm+sustainability+Grains+Research+and+Development+Corp.pdf' target="_blank">Key financial ratios for farm sustainability Grains Research and Development Corporation (GRDC)</a></li>
											<li><a href='http://www.pir.sa.gov.au/consultancy/major_programs/new_horizons' target="_blank"> New Horizons soil and productivity program (PIRSA)</a></li>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockbusiness/Improving+lamb+weaning+percentages+(SARDI).pdf' target="_blank">Lambs - Improving Weaning percentages (SARDI)</a></li>
											<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockbusiness/Mineral+exploration+and+faming+booklet+pdf+(Dept+State+Development).pdf' target="_blank">Mineral exploration and faming booklet  (Dept State Development)</a></li>
										
												</ul>

											</div>
										</div>
									</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
	
	
				<!-- Industrial Relations -->
				<div class="row" id="grainIndustrailContent" style="display: none;">
					<div class="col-sm-12">
						<h2>
							Livestock<strong> Industrial Relations</strong>
						</h2>
						<p class="lead">Links to Pastoral reports and Shearers awards, plus updates on superannuation, WorkCover and arrangements for different types of employment.</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											 Awards <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">
												<ul>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Pastoral+Award+2010+as+at+1+July+2013+(FairWork+Australia).pdf' target="_blank">Pastoral Award 2010 as at 1 July 2013 (FairWork Australia)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Shearers+Pastoral+Award+2014+(Shearing+Contracting+Assoc.+of+Australia).pdf' target="_blank">Shearers Pastoral Award 2014 (Shearing Contracting Assoc. of Australia)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockMers/Livestock-SA-July-2014-Minimum-Wage-Adjustment-SGC-and-Casual-Loading-with-effect-from-first-pay-period-commencing-on-or-after-1st-July-2013.doc' target="_blank">Minimum Wage Adjustment SGC and Casual Loading (MERS) </a></li>
											
											</ul>	
											</div>
										</div>
									</h4>
								</div>
							</div>
							<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
											General Employment and Information links <span class="plusminus">+</span>
										</div>
										<div class="accordion_body" style="display: none;">
											<div class="panel-body">
												<ul>
												<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/pig/Fair+Work+Information+Booklet+for+Agricultural+Producers.pdf'>Fair Work Information Booklet for Agricultural Producers (NFF and Dept of Agriculture)</a></li>
											
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Member-Update-Superannuation-May-2014.pdf' target="_blank">Member Updates Superannuation May 2014 (MERS)</a></li>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Work-Experience.pdf' target="_blank">Work Experience (MERS)</a></li>
										
											</ul>	
											</div>
										</div>
									</h4>
								</div>
							</div></div>
						</div>


					</div>
				</div>

				<!-- Forms -->
					<div class="row" id="porkForm" style="display: none;">
					<div class="col-sm-12">
						<h2>
							Livestock<strong> Forms</strong>
						</h2>
						<p class="lead">Guidelines and application forms for PIC numbers, livestock brands, earmarks and tattoos.</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> General<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										
									<ul>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_forms/Cancellation+of+Application+Form+for+Brands+(BioSecurity+SA).pdf' target="_blank">Cancellation of Application Form for Brands (BioSecurity SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_forms/Livestock+Brands+summary+info+(BioSecurity+SA).pdf' target="_blank">Livestock Brands summary info (BioSecurity SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_forms/PIC+New+Registration+Application+Form+July+2013+(BioSecurity+SA).pdf' target="_blank">PIC New Registration Application Form July 2013 (BioSecurity SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_forms/PIC+Numbers+FAQ+(BioSecurity+SA).pdf' target="_blank">PIC Numbers FAQ (BioSecurity SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_forms/PIC+Registration+Application+Form+July+2013+(BioSecurity+SA).doc' target="_blank">PIC Registration Application Form July 2013 (BioSecurity SA) (Downloadable)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_forms/Stud+Stock+Brand+Form+(BioSecurity+SA).doc' target="_blank">Stud Stock Brand Form(BioSecurity SA) (Downloadable)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_forms/Stud+Stock+Brand+Form+(BioSecurity+SA).pdf' target="_blank">Stud Stock Brand Form (BioSecurity SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_forms/Transfer+of+a+Brand+or+Mark+Application+(BioSecurity+SA).pdf' target="_blank">Transfer of a Brand or Mark Application (BioSecurity SA)</a></li>
											
											</ul>	
									</div>
							</div>
									</h4>
								</div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> Cattle, Horses and Camels<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										
									<ul>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockcattels/Horse%2C+Cattle+and+Camel+Brands+and+Earmarks++(BioSecurity+SA).pdf' target="_blank">Horse, Cattle and Camel Brands and Earmarks</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockcattels/Cattle+Earmark+Form+(BioSecurity).doc' target="_blank">Cattle Earmark Form (Downloadable form)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockcattels/Cattle+Earmark+Form+(BioSecurity).doc' target="_blank">Horse, Cattle Brand Form (Downloadable form)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockcattels/Horses+and+PIC+-+Frequently+Asked+Questions+(BioSecurity+SA).pdf' target="_blank">Horses and PIC - FAQ</a></li>
											
											</ul>	
									</div>
							</div>
									</h4>
								</div></div>
				<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> Sheep<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										
									<ul>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_sheep/Sheep+Brand+Form+(BioSecurity+SA).doc' target="_blank">Sheep Brand Form (BioSecurity SA) (Downloadable Form)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_sheep/Sheep+Brands+and+Earmarks+(BioSecurity+SA).pdf' target="_blank">Sheep Brands and Earmarks (BioSecurity SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_sheep/Sheep+Earmark+Form+(BioSecurity+SA).doc' target="_blank">Sheep Earmark Form (BioSecurity SA) (Downloadable Form)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_sheep/Sheep+Earmark+Key+to+the+System+(BioSecurity+SA).pdf' target="_blank">Sheep Earmark Key to the System (BioSecurity SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestock_sheep/Sheep+Earmark+Key+to+the+System+Form+(BioSecurity+SA).doc' target="_blank">Sheep Earmark Key to the System (BioSecurity SA) (Downloadable Form)</a></li>
											
											</ul>	
									</div>
							</div>
									</h4>
								</div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> Imports & Exports<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										
									<ul>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Export+Documentation+EXDOC+Registration+Form+(Dept+of+Agriculture).pdf' target="_blank">Export Documentation EXDOC Registration Form (Dept of Agriculture)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Export+Transfer+Notice+for+Fresh+Fruit+and+Vegetables+(Dept+of+Agriculture).docx' target="_blank">Export Transfer Notice for Fresh Fruit and Vegetables (Dept of Agriculture)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Export+documentation+EXDOC+and+EDI+User+Form+(Dept+of+Agriculture).pdf' target="_blank">Export documentation EXDOC and EDI User Form (Dept of Agriculture)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Exporter+Clearance+Form+EX222+(Dept+of+Agriculture).docx' target="_blank">Exporter Clearance Form EX222 (Dept of Agriculture)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Exporter+Registration+for+Integrated+Cargo+System+(Australian+Customs+Service).pdf' target="_blank">Exporter Registration for Integrated Cargo System (Australian Customs Service)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Importer+Credit+Card+Payment+Form+(Dept+of+Agriculture).doc' target="_blank">Importer Credit Card Payment Form (Dept of Agriculture)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Importing+e+Permits+User+Guide+(Dept+of+Agriculture+AQIS).pdf' target="_blank">Importing e Permits User Guide (Dept of Agriculture AQIS)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/LIve+Animal+Exports+Credit+Card+Payment+Form+(Dept+of+Agriculture).pdf' target="_blank">LIve Animal Exports Credit Card Payment Form (Dept of Agriculture)</a></li>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Livestock+Export+License+Form+variation.pdf' target="_blank">Livestock Export License Form variation</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/forms/Livestock+Export+License+application+form+Dept+of+Agriculture).pdf' target="_blank">Livestock Export License application form (Dept of Agriculture)</a></li>
										
											</ul>	
									</div>
							</div>
									</h4>
								</div></div>
								
						</div>

</div>
					</div>
				</div></div></div>
				<!-- Transport -->
				<div class="row" id="porkLegal" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Livestock <strong> Transport</strong>
						</h2>
						<p class="lead">
						Information on animal loading, heavy vehicle regulations, vehicle escorting guidelines, driver work diaries and license application forms. Sourced from Government websites and including live links to websites for latest news and updates.
						
						</p>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> Animal Loading<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										
									<ul>
							<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Is+It+Fit+To+Load+-+Guide+(Meat+and+Livestock+Australia).pdf' target="_blank">Is It Fit to Load Guide (Meat and Livestock Australia)</a></li>
														</ul>	
									</div>
							</div>
									</h4>
								</div></div>
		
				<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Regulations for heavy vehicles
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf
											' target="_blank">Regulations for Driving Oversize or Overmass Agricultural Vehicles (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf' target="_blank">Regulations for Transporting Agricultural Vehicles as Loads (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf' target="_blank">Required documents to be carried (NHVR)</a></li>
											<li><a target="_blank" href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting+Guidelines+for+oversize+and+overmass+vechicles+and+loads.pdf'>Escorting Guidelines for Drivers and Escorting Vehicles (NHVR)</a></li>
						
											</ul>
										
											</div>
							</div>
									</h4></div>
								</div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												Licence Application Forms & Driver Work Diaries
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33+Upgrade+Truck+Licence.pdf
											' target="_blank">Restricted Licence Application Form MR 33 (DPTI) </a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf
											' target="_blank"> Driver Work Diary (NHVR)</a></li>
											<li><a href='https://drive.google.com/viewerng/viewer?url=http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD+201405-0028-supplementary-work-diary-record.pdf
											' target="_blank">Supplementary Work Diary Record (NHVR) </a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
												NHVR Live Weblinks 
											<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
				
											<li><a href='https://www.nhvr.gov.au/resources/faqs
											' target="_blank">https://www.nhvr.gov.au/resources/faqs
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/forms-and-services
' target="_blank">https://www.nhvr.gov.au/resources/forms-and-services
											</a></li>
											<li><a href='https://www.nhvr.gov.au/news-events/stakeholder-events
											' target="_blank">https://www.nhvr.gov.au/news-events/stakeholder-events
											</a></li>
											<li><a href='https://www.nhvr.gov.au/resources/rss-feeds
											' target="_blank">https://www.nhvr.gov.au/resources/rss-feeds

											</a></li>
											
											</ul>
										
											</div>
							</div>
									</h4>
								</div></div></div></div></div></div>			
						</div></div>
						
						
								<!-- Trainign & Education -->	
								
								
								<div class="row" id="grainTraining" style="display:none;">
				<div class="col-sm-12">
					<h2>Livestock <strong> Training & Education</strong></h2>
					<p class="lead">
						Links to scholarships and courses for livestock industry.
						</p>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Links to agricultural further education courses
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.adelaide.edu.au/scholarships/undergrad/isolated/ ' target="_blank">Scholarships at Adelaide University</a></li>
									<li><a href='http://www.flinders.edu.au/medicine/sites/nt-clinical-school/students/scholarships.cfm' target="_blank">Scholarships at Flinders University</a></li>
									<li><a href='http://www.tafesa.edu.au/apply-enrol/before-starting/scholarships-grants' target="_blank">Scholarships at TAFE SA</a></li>
									<li><a href='https://www.sa.gov.au/topics/education-skills-and-learning/financial-help-scholarships-and-grants/scholarships' target="_blank">Selection of SA Government scholarships</a></li>
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Courses
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='http://www.tafesa.edu.au/courses/agriculture-science/agriculture' target="_blank">TAFE SA</a></li>
									<li><a href='http://www.adelaide.edu.au/course-outlines/104332/1/sem-2/' target="_blank">ADELAIDE UNIVERSITY (Roseworthy campus course)</a></li>
									
									</ul>
								</div></div></h4></div></div>
					</div>
					
					</div></div></div>
							
							<!-- Work Health Safety -->
							
				<div class="row" id="grainWorkContent" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Livestock<strong> Work Health Safety</strong>
						</h2>
						<p class="lead">
						SafeWork SA workplace guidelines, procedures, checklists and links to web pages and files for download.
						</p>
					<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Employer obligations
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/mers/Member-Update-WHS-June-2013-What-is-reasonably-practicable-in-meeting-obligations-to-ensure-the-health-and-safety-of-workers.pdf' target="_blank">Member-Update-WHS-June-2013 (SafeWork SA)</a></li>
									
									</ul>
								</div></div></h4></div></div>
					
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										General Information
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/BioSecurity+and+on-farm+workers+(Dept+of+Agri).pdf' target="_blank">BioSecurity and on-farm workers (Dept of Agri)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/Guidelines+for+entering+farms+or+animal+facilities+(Dept+of+Agriculture)+(2).pdf' target="_blank">Guidelines for entering farms or animal facilities (Dept of Agriculture)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/commmm/Guidelines+for+visiting+farms+during+disease+outbreak+(Dept+of+Agriculture)+(1).pdf' target="_blank"> Guidelines for visiting farms during disease outbreak (Dept of Agriculture)</a></li>
							
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/general_whs/Farm+Safety+Fact+Sheet+(SafeWork+SA).pdf' target="_blank">Farm Safety Fact Sheet (SafeWork SA)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Children+On+Farms+Safety+Guide+(SafeWork+SA).pdf' target="_blank">Children on Farms Safety Guide (SafeWork SA)</a></li>
									<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockbusiness/2014+Agricultural+Self+Assessment+Guide+PDF.pdf' target="_blank"> Agricutural Self Assessment Guide (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Workplace Safety guidelines and procedures
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
										<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Power+lines+warning+at+Harvest+time.pdf' target="_blank">Power Lines warning at Harvest (SafeWork SA)</a></li>
						
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=5892' target="_blank"> Codes of Practice (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/CoPHazardousManualTasks.pdf' target="_blank"> Hazardous Tasks Safety (SafeWork SA) </a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/How_to_Manage_Work_Health_and_Safety_Risks.pdf' target="_blank"> Work Safety Guide (Safework SA) </a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/youngWorkersGuide.pdf' target="_blank"> Young Workers Guide (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/CoPFirstAidWorkplace.pdf' target="_blank"> First Aid Workplace Guide (SafeWork SA) </a></li>
								
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/sevenStepsSmallBuiness.pdf' target="_blank"> Seven Step Safety Checklist (SafeWork SA)  </a></li>
								<li><a href='http://www.safework.sa.gov.au/uploaded_files/notification.pdf' target="_blank"> Notifiable Incident Report Form (SafeWork SA) </a></li>
									<li><a href='http://www.safework.sa.gov.au/uploaded_files/farming_community.pdf' target="_blank">Familiar Work Health and Safety principles for Farmers  (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/CoPFirstAidWorkplace.pdf' target="_blank">First Aid in the Workplace Code of Practice (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/FSCoPFirstAidWorkplace.pdf' target="_blank">First Aid in the Workplace Fact Sheet (SafeWork SA)</a></li>
							<li><a href='http://www.epa.sa.gov.au/xstd_files/Water/Other/pesticide_commercial.pdf' target="_blank">Safe and Effective Pesticide Use (EPA SA)</a></li>
						<li><a href='http://www.safework.sa.gov.au/uploaded_files/youngWorkersGuide.pdf' target="_blank">Young Worker Safety (SafeWork SA)</a></li>
								
								
									</ul>
								</div></div></h4></div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Safety preparation in the workplace 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=5892' target="_blank">Labelling of Workplace Hazardous Chemicals (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113699' target="_blank">Managing Noise and Preventing Hearing Loss at Work (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113700' target="_blank">Managing the Risks of Plant in the Workplace (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113701' target="_blank">Managing Risks of Hazardous Chemicals in the Workplace (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113702' target="_blank">Managing Electrical Risks in the Workplace (SafeWork SA)</a></li>
								
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113704' target="_blank">Managing the Work Environment and Facilities (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113705' target="_blank">Preparation of Safety Data Sheets for Hazardous Chemicals (SafeWork SA)</a></li>
								<li><a href='http://www.safework.sa.gov.au/show_page.jsp?id=113708' target="_blank">Work Health and Safety Consultation Cooperation and Coordination (SafeWork SA)</a></li>
									</ul>
								</div></div></h4></div></div>
									<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Equipment and Vehicle Safety 
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/general_whs/Forklift+Safety+(Safework+SA).pdf' target="_blank">Forklift safety (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/sgAngleGrinders.pdf' target="_blank">Angle grinder safety (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/sgEarthmovingEquipmentLoads.pdf' target="_blank">Earth Moving Equipment to lift load Safeguard (SafeWork SA)</a></li>
								<li><a href='https://www.safework.sa.gov.au/uploaded_files/sgConveyors.pdf' target="_blank">Plant and Machinery Conveyor Safeguard (SafeWork SA)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/Bow+Ladders+Safety+Guide+for+Fruit+Picking+(SafeWork+SA).pdf' target="_blank">Bow Ladders Safety Guide for Fruit Picking (SafeWork SA)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/safeguard/All-terrain+Vehicles+Safety+(WorkSAfe+SA).pdf' target="_blank">All-terrain Vehicles Safety (SafeWork SA)</a></li>
								
									</ul>
								</div></div></h4></div></div>
								
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">
										Worker and Personal Health  
								<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
								<ul>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Mental+Health+Digital+Telehealth+Network+(SA+Health).pdf' target="_blank">Mental Health Digital Telehealth Network (SA Health)</a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Farm+Health+%26+Safety+Toolkit+for+Rural+GPs+(Australian+Centre+for+Agri+Health+%26+Safety%2C+Uni+of+Sydney).pdf' target="_blank">Farm Health & Safety Toolkit for Rural GPs (Australian Centre for Agri Health & Safety, Uni of Sydney) </a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Country+mental+health+services+(SA+Health).pdf' target="_blank">Country mental health services (SA Health) </a></li>
								<li><a href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/livestockworkerpersonalhealth/Being+Active+Fact+Sheet+Nov2012+(SA+Health).pdf' target="_blank">Being Active Fact Sheet Nov2012 (SA Health) </a></li>
								
									</ul>
								</div></div></h4></div></div>
								
							</div></div>
						
					</div>
					
					
				</div></div></div></div></div>
				
							
				<!-- PORK Legal -->

				<div class="row" id="porkLegal" style="display:none;">
					<div class="col-sm-12">
						<h2>
							Animal<strong> Health</strong>
						</h2>
						
						
						<!--Starting my Test From here  -->
						<style>
						.accordion_container { width: 1140px; } 
						.accordion_head {  color: #03ad5b; cursor: pointer; font-family:sans-serif; font-size: 16px; margin: 0 0 1px 0; padding: 10px 15px; font-weight: bold; display:block;} 
						.accordion_body {background-color:white;} 
						.accordion_body a {padding-right:0px;padding-top:0px;padding-left:0px;padding-bottom:0px;;font-family:sans-serif;font-size:14px; } 
						.accordion_body a:hover{text-decoration:underline;}
						.plusminus { float:left; } 
						</style>
						
						
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> FORMS<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/EAD-Risk-Management-Manual.pdf'
												target="_blank" >EAD-Risk-Management-Manual</a></li>
											<li><a 
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/Emergency+animal+disease+preparedness.pdf'
												target="_blank">Emergency animal disease preparedness</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/MLA+GUIDE+Is+It+Fit+To+Load.pdf'
												target="_blank">Preparing-your-business-to-survive-an-emergency-animal-disease-outbreak</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/Preparing-your-business-to-survive-an-emergency-animal-disease-outbreak.pdf'
												target="_blank">RC BIOSECURITY Stud stock brands and
													tattoos</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/RC+BIOSECURITY+Stud+stock+brands+and+tattoos.pdf'
												target="_blank">RRC BIOSECURITY Stud_Stock_Brand_FORM</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/RC+BIOSECURITY+Stud_Stock_Brand_FORM.doc'
												target="_blank">RC BIOSECURITY Stud_Stock_Brand_FORM</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/RC+LIVESTOCK+BIOSECURITY+WEB+LINKS.docx'
												target="_blank">RC LIVESTOCK BIOSECURITY WEB LINKS</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
						
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> Disaster Planning<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/CFA+BUSHFIRE+-+PROTECTION+OF+FODDER+RESERVES.pdf'
												target="_blank">CFA BUSHFIRE - PROTECTION OF FODDER
													RESERVES</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/CFS+BUSHFIRE+FACT+SHEET+303_EMERGENCY+KITS.pdf'
												target="_blank">CFS BUSHFIRE FACT SHEET 303_EMERGENCY
													KITS</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/CFS+BUSHFIRES+-+CARE+OF+PETS+AND+LIVESTOCK.pdf'
												target="_blank">CFS BUSHFIRES - CARE OF PETS AND
													LIVESTOCK</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/CHECKLIST+-+Farming+Guidelines+for+Reducing+Bushfires.PNG'
												target="_blank">CHECKLIST - Farming Guidelines for
													Reducing Bushfires</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/Farming_Guidelines_for_the_Reduction_of_Bushfire_Risk.pdf'
												target="_blank">Farming_Guidelines_for_the_Reduction_of_Bushfire_Risk</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/GUIDE+to_bushfire_safety_2014.pdf'
												target="_blank">GUIDE to_bushfire_safety_2014</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/Livestock_Safety_in_Bushfires_Sept2012_version.pdf'
												target="_blank">Livestock_Safety_in_Bushfires_Sept2012_version</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/RC+BIOSECURITY+-+EMERGENCY+MANAGMENT++LIVE+LINK.docx'
												target="_blank">RC BIOSECURITY - EMERGENCY MANAGMENT
													LIVE LINK</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/Recovering+after+bushfires.docx'
												target="_blank">Recovering after bushfires</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/disasterPlanning/STOCK+OWNERS+Risk-management-in-times-of-fire-and-flood.pdf'
												target="_blank">Recovering after bushfires</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
						
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head"> PIC
											INFORMATION<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/BIOSECURITY+Livestock+property+registration.docx'
												target="_blank">BIOSECURITY Livestock property
													registration</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC++BIOSECURITY+Stud+stock+brands+and+tattoos.pdf'
												target="_blank">RC BIOSECURITY Stud stock brands and
													tattoos</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+CANCELLATION_application+FORM.doc'
												target="_blank">RC BIOSECURITY CANCELLATION_application
													FORM</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Cattle_Earmark_FORM.doc'
												target="_blank">RC BIOSECURITY Cattle_Earmark_FORM</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Frequently+Asked+Questions+-+HORSES+AND+PIC.pdf'
												target="_blank">RC BIOSECURITY Frequently Asked
													Questions - HORSES AND PIC </a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Horse_Cattle_brand_FORM.doc'
												target="_blank">RC BIOSECURITY Horse_Cattle_brand_FORM</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Livestock+brand+Information.pdf'
												target="_blank">RC BIOSECURITY Livestock brand
													Information</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Livestock+brands.docx'
												target="_blank">RC BIOSECURITY Livestock brands</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+PIC+New_Registration_Application_Form_July_2013+WORD.doc'
												target="_blank">RC BIOSECURITY PIC
													New_Registration_Application_Form_July_2013 WORD</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+PIC+Transfer+Brand+FORM.doc'
												target="_blank">RC BIOSECURITY PIC Transfer Brand FORM</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Sheep+brands+and+earmarks.pdf'
												target="_blank">RC BIOSECURITY Sheep brands and earmarks</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Sheep_Brand_FORM.doc'
												target="_blank">RC BIOSECURITY Sheep_Brand_FORM</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Sheep_Earmark_FORM.doc'
												target="_blank">RC BIOSECURITY Sheep_Earmark_FORM</a></li>
											<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/RC+BIOSECURITY+Sheep_earmark_Key_to_the_SystemFORM.doc'
												target="_blank">RC BIOSECURITY
													Sheep_earmark_Key_to_the_SystemFORM</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div>
						</div>
								<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="accordion_head">  USEFUL
											INFORMATION<span class="plusminus">+</span>
										</div>
											<div class="accordion_body" style="display: none;">
												<div class="panel-body">
										<ul>
								<li><a
												href='https://s3.amazonaws.com/ccmeresources/000RuralPDF/PIC/SARDI+Lamb+doc+1.pdf'
												target="_blank">SARDI Lamb</a></li>
										</ul>
									</div>
							</div>
									</h4>
								</div></div>
						<script>
						$(document).ready(function () {
						    //toggle the component with class accordion_body
						    $(".accordion_head").click(function () {
						        if ($('.accordion_body').is(':visible')) {
						            $(".accordion_body").slideUp(300);
						            $(".plusminus").text('+');
						        }
						        if ($(this).next(".accordion_body").is(':visible')) {
						            $(this).next(".accordion_body").slideUp(300);
						            $(this).children(".plusminus").text('+');
						        } else {
						            $(this).next(".accordion_body").slideDown(300);
						            $(this).children(".plusminus").text('-');
						        }
						    });
						});
						
</script>
						<!-- I will finish my test here!! -->



						</div></div></div></div></div></div></div></div></div></div></div>
					
				<!-- PORK SURVEY -->

				<div class="row" id="porkSurvey" style="display:none;">
					<div class="col-sm-7">
						<h2>
							Livestock<strong> Surveys</strong>
						</h2>
						<p class="lead">No survey available at this time</p>	
					</div>	
				</div>
				
				
			</div>
		</div>
	</div></div>
	
	<script src="<c:url value='/ruralAssets/js/logic.js'/>"></script>
	<script src="<c:url value='/ruralAssets/vendor/jquery/jquery.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/newsRSS.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/pages/index.js'/>"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script src="<c:url value='/ruralAssets/vendor/bootstrap/bootstrap.js'/>"></script>
	<script src="<c:url value='/ruralAssets/js/theme.js'/>"></script>
	
	<script type="text/javascript">
		google.load("feeds", "1");
		google.setOnLoadCallback(newsRSS.init);
	</script>

	<script>
		$(document).ready(function() {
			$('#livestockOption').addClass("active");
			Index.initLivestock();
		});
	</script>
</body>
</html>

