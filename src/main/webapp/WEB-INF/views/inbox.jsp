<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<div class="main">

	<div class="home-intro" id="home-intro">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<p>
						Rural Connect <em>Messages</em> </br> <strong>Today's Snapshot</strong>
				</div>
				<div class="col-md-4">
					<div class="get-started">
						<a href="<c:url value='/member/partners'/>"
							class="btn btn-lg btn-primary"
							onclick="_gaq.push(['_trackEvent', 'Commercial Partners', 'Click', 'Meet our commercial partners']);"
							target="_blank">Meet our commercial partners</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container" style="margin-top: -45px">
		<div class="row">
			<div class="col-md-12">
				<div class="tabs" id="tabs">
					<ul class="nav nav-tabs nav-justified">
						<li class="active"><a href="#dashboardTab" data-toggle="tab"
							class="text-center"
							onclick="_gaq.push(['_trackEvent', 'Inbox', 'Click', 'Member Inbox']);"><i
								class="fa fa-envelope"></i> Messages</a></li>

					</ul>

					<form action="<c:url value='/member/inbox/delete'/>" method="post"/>
					<div class="tab-content">
						<div id="dashboardTab" class="tab-pane active">

							<div class="panel">
								<div class="panel-heading">
									<button class="btn btn-success right">Delete selected</button><br>
								</div>
								
									<table id="messagesTable" class="table table-bordered table-striped mb-none" data-swf-path="<c:url value='/assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf'/>">
										<thead>
											<tr>
												<th></th>
												<th style="width:120px;"><span><i class="fa fa-calendar"></i>From</span></th>
												<th style="width:180px;"><i class="fa fa-clock-o"></i>Subject</span></th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
										<c:forEach var="message" items="${messages}" varStatus="row">
											<tr>
												<td> <input type="checkbox" name="ids"></td>
												<td> <a href="<c:url value='/member/inbox/view/${message.id}'/>">
													<span class="name" style="min-width: 120px; display: inline-block;">${message.accountByFromId.name}</span></a></td>
												<td> <span class=""><a href="<c:url value='/member/inbox/view/${message.id}'/>">${message.subject}</a></span> </td>
												<td><a href="<c:url value='/member/inbox/view/${message.id}'/>">${message.formattedDate}</a></td>
											</tr>
										

										</c:forEach>
									</tbody>
									</table>

								
							</div>
							
						</div>
						
					</div>
					</form>
				</div>
			</div>
		</div>
</div>