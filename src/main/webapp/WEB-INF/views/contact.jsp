<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html>
<html>
<head>
    <title><decorator:title default="Rural Connect"/></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

	<link rel="stylesheet" href="<c:url value='/styles/normalize.css'/>">
	<link rel="stylesheet" href="<c:url value='/styles/core.css'/>">
	<link rel="stylesheet" href="<c:url value='/styles/internal3.css'/>">
	<link rel="stylesheet" href="<c:url value='/styles/farmer.css'/>">
	<script src="<c:url value='/scripts/jquery-1.10.2.min.js'/>"></script>
	<script src="<c:url value='/scripts/jquery.marquee.min.js'/>"></script>
	<script src="<c:url value='/scripts/generic_bits.js'/>"></script>
	
	<script src="http://cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.0/css/jquery.dataTables.css">


</head>
</head>
<body>

<nav class="containerWidth yellowSaw">
        <div class="contentWidth" style="position: relative;">
     
        </div>
</nav>
<section class="level1  contentWidth clearBoth">
	<h1>Contact us</h1>

	
    
    <div id="content">
     <h3>  Primary Producers SA </h3></p>
 <p> Unit 4, 780 South Road
 <p> Glandore SA 5037
 <p> Telephone: (08) 8297 0899
 <p> Independent Chair, Rob Kerin: robkerin@ymail.com
Policy & Executive Support, Amy Williams
Commodity Group Contacts:
 <p><h3> Grain Producers SA</h3>
 <p> Telephone: 1300 734 884
 <p> Email: info@grainproducerssa.com.au
<p>Website: www.grainproducerssa.com.au
<p><h3>Livestock SA</h3>
<p>Telephone: (08) 8297 2299
<p>Email: dcrabb@livestocksa.org.au
<p>Horticulture Coalition of SA
<p>Telephone: 0417 809 172
<p>Email: sahort@bigpond.com
<p><h3>Wine Grape Council of SA</h3>
<p>Telephone: (08) 8351 4378
<p>Email: admin@wgcsa.com.au
<p>Website: www.wgcsa.com.au
<p><h3>South Australian Dairy Farmers Association</h3>
<p>Telephone: (08) 8293 2399
<p>Website: www.sadafresh.com.au
<p><h3>Pork SA</h3>
<p>Telephone: 0418 848 845
<p>Email: andypointon.food@iinet.com.au


    </div>
 
   </section>
  
</body>
</html>


