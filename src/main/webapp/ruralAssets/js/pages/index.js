/*
 *  Document   : index.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Dashboard page
 */

var Index = {

    feeds : [],

    initHome : function(){

        /* start: Custom JSs */

        // start:radio

       radioController.Init();

        // end:radio

        // start:currency

    	 currencyExchange.Init();

        // end:currency
        
        // start:newsRSS
        //Index.feeds.push("http://www.abc.net.au/news/feed/4535882/rss.xml");
        Index.feeds.push("https://news.google.com/news?pz=1&cf=all&ned=au&hl=en&q=primary+producers+SA+or+Grain+SA+or+Livestock+SA+or+Dairy+farmers+or+PPSA+or+Horiticulture&output=rss");

        var news = new newsRSS({ feeds: Index.feeds }).Init();

        // end:newsRSS

        // start:fire alerts

          firealertsWidget.Init();

        // end:fire alerts

        // start:weather alerts

        weatherWidget.Init();

        // end:weather alerts

        /*

        var logos = [
            { href: "grain.html", src: "img/logos_gm/grainproducers.jpg", label: "GRAIN PRODUCERS SA" },
            { href: "pork_micro.html", src: "img/logos_gm/pork.jpg", label: "PORK SA" },
            { href: "dairyfarmers.html", src: "img/logos_gm/dairyfarmers.jpg", label: "SOUTH AUSTRALIAN DAIRYFARMERS ASSOCIATION" },
            { href: "winegrapes.html", src: "img/logos_gm/winegrape.jpg", label: "WINE GRAPE COUNCIL OF SOUTH AUSTRALIA" },
            { href: "livestock.html", src: "img/logos_gm/livestock.jpg", label: "LIVESTOCK SA" },
            { href: "horticulture.html", src: "img/logos_gm/horticulture.jpg", label: "HORTICULTURE COALITION OF SA" }
        ]
        gatedMemberWidget.Init({logos:logos});

        // end:gated member

        /* end: Custom JSs */


            /*
             * With Gmaps.js, Check out examples and documentation at http://hpneo.github.io/gmaps/examples.html
             */

            // Set default height to Google Maps container
            $('.gmap').css('height', '220px');

            // Initialize Timeline map
            	/*
            new GMaps({
                div: '#gmap-timeline',
                lat: -33.863,
                lng: 151.202,
                zoom: 15,
                disableDefaultUI: true,
                scrollwheel: false
            }).addMarkers([
                {
                    lat: -33.863,
                    lng: 151.202,
                    animation: google.maps.Animation.DROP,
                    infoWindow: {content: '<strong>Cafe-Bar: Example Address</strong>'}
                }
            ]);*/
            

        //Transport
       /*     var ET = new fileWidget({
                IDdvContainer: "#_dvTs",
                iconWidth: "40px",
                iconHeight: "40px",
                Docs: [
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Escorting Guidelines for oversize and overmass vechicles and loads.pdf", title: "Request for Restricted Licence to operate Agricultural Machinery", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Fatigue Exemption for Grain Cartage (with Edit).pdf", title: "RESTRICTED LICENCE APPLICATION", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Grain Carriers Code of Practice  FINAL  20101119 (1).pdf", title: "CODE OF PRACTICE FOR OVERSIZE OR OVERMASS AGRICULTURAL VEHICLES", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Grain Carriers Code of Practice  FINAL  20101119.pdf", title: "CODE OF PRACTICE FOR THE TRANSPORT OF AGRICULTURAL VEHICLES AS LOADS", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HV_Carriage_of_Documents_Bulletin_14-06-2011.pdf", title: "Heavy Vehicle Transport Off-Farm Grain Carriers' Code of Practice", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/Licence Restriction Request - Special Purpose Vehicles - Version 5 7 13.pdf", title: "ESCORTING GUIDELINES", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR33 Upgrade Truck Licence.pdf", title: "Road Traffic (Heavy Vehicle Driver Fatigue) (Transport of Fruit, Vegetables and Grain) Notice 2011", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR802-Code-of-practice-for-oversize.pdf", title: "ESCORTING GUIDELINES", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/MR803-Code-of-practice-for-the.pdf", title: "Road Traffic (Heavy Vehicle Driver Fatigue) (Transport of Fruit, Vegetables and Grain) Notice 2011", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HeavyVehFMNR Regs1.pdf", title: "Heavy Vehicle (Fatigue) National Regulation", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HeavyVehGenNR Regs2.pdf", title: "Heavy Vehicle (General) National Regulation", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HeavyVehMDLNR Regs 3.pdf", title: "Heavy Vehicle (Mass,Dimension and Loading) National Regulation", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/HeavyVehVSNR Regs 4.pdf", title: "Heavy Vehicle (Vehicle Standards) National Regulation", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/NDWD 201405-0028-supplementary-work-diary-record.pdf", title: "DRIVER WORK DIARY DAILY SHEET", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/nhvr-national-driver-work-diary-08-2013.pdf", title: "Nationsl Driver Work Diary", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/home/transport/RC USEFUL TRANSPORT.docx", title: "Useful Transport", ext: "doc", page: 1 },

                ],
            }).Init();*/

    },
    initPork: function () {

        // start:currency

       // currencyExchange.Init();

        // end:currency

        // start:newsRSS

        Index.feeds.push("https://news.google.com/news/feeds?pz=1&cf=all&ned=au&hl=en&q=pork+farming+australia&output=rss");
        
        var news = new newsRSS({ feeds: Index.feeds }).Init();

        // end:newsRSS

        // start:fire alerts

        // firealertsWidget.Init();

        // end:fire alerts

        // start:weather alerts

        // weatherWidget.Init();

        // end:weather alerts

        // start:file

        //Education and Training
        /*
        var ET = new fileWidget({
            IDdvContainer: "#_dvET",
            iconWidth:"40px", 
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news%20archive/Pork%20SA%20Snapshots%20August%202014.pdf", title: "Farm business Skills", ext:"pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/education%20and%20training/Stepping%20Into%20Leadership-2.pdf", title: "Women in Leadership", ext:"pdf", page: 1 }
            ],
        }).Init();

        //Industrial Relations
        var IR = new fileWidget({
            IDdvContainer: "#_dvIR",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Industrial Relations - Member Update September  2014.pdf", title: "MERS Members Update", ext:"pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity information form.doc", title: "Workers Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity Letter to Doctor TEMPLATE.doc", title: "Workers Capacity Letter to Doctor Template", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity Medical release authority TEMPLATE.doc", title: "Workers Capacity Medical release authority Template", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/RC 3 GRAINS SAGIT REPORT.pdf", title: "SAGIT Report", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/RC Wages awards July 2014.pdf", title: "Wages newsJuly 2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/WorkCover Levy Rates 2013-14.pdf", title: "Workcover Industry premium rates", ext: "pdf", page: 1 }
            ],
        }).Init();

        //News Archive
        var NA = new fileWidget({
            IDdvContainer: "#_dvNA",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/Pork SA Committee for 2014.docx", title: "MERS Members Update", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/Pork SA Snapshots August 2014.pdf", title: "Pork SA Snapshots August 2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/Pork SA Snapshots June 2014.pdf", title: "Pork SA Snapshots June 2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/Pork SA Snapshots March 2014.pdf", title: "Pork SA Snapshots March 2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/THE PORK PAGE September 2014.pdf", title: "The Pork Page September 2014", ext: "pdf", page: 1 }
            ],
        }).Init();
        
        //Workcover
        var WC = new fileWidget({
            IDdvContainer: "#_dvWC",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/workcover/WorkCover Levy Rates 2013-14.pdf", title: "WorkCover Levy Rate", ext: "pdf", page: 1 }
            ],
        }).Init();

     //Health & Safety
        var HS = new fileWidget({
            IDdvContainer: "#_dvHS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping:true,
            Groups : [
                {
                    name:"Safe Work Practice",
                    Docs: [
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs602i.pdf", title: "Basic accident investigation", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs38i.pdf", title: "Confined spaces (working in)", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs661i.pdf", title: "Computer workstations design", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/GS52-3.pdf", title: "Noise control in the workplace", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/remote_safe_staff.pdf", title: "Remote location staff safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/sy1i.pdf", title: "Stockyard safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs34i.pdf", title: "Young worker safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs51i.pdf", title: "Zoonosis", ext: "pdf", page: 1 }
                    ]
                },
				                {
                    name:"Chemicals",
                    Docs: [
                            { path: "http://www.safework.sa.gov.au/uploaded_files/rs8i.pdf", title: "Pesticide safety", ext: "pdf", page: 1 },
                    ]
                },
								                {
                    name:"Machinery and Tools",
                    Docs: [
							{ path: "http://www.safework.sa.gov.au/uploaded_files/pmc1i.pdf", title: "Agricultural motorbikes", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/angle_grinder_safety.pdf", title: "Angle grinder safety", ext: "pdf", page: 1 },
							{ path: "http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf", title: "Disc grinder safety in the shearing industry", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs4i.pdf", title: "Forklift safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs57i.pdf", title: "Ladder safety", ext: "pdf", page: 1 },
                            
                    ]
                },
            ]           
        }).Init();
        //Biosecurity
        var BS = new fileWidget({
            IDdvContainer: "#_dvBS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups : [
                {
                    name:"Animal Health",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/EAD-Risk-Management-Manual.pdf", title: "Risk management planning for an emergency animal disease outbreak", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/Emergency animal disease preparedness.docx", title: "Emergency animal disease preparedness", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/Emergency animal disease preparedness.pdf", title: "Emergency animal disease preparedness", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/Livestock and pet owners urged to prepare for heat.docx", title: "Livestock and pet owners urged to prepare for heat", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/Preparing-your-business-to-survive-an-emergency-animal-disease-outbreak.pdf", title: "Preparing your business", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/RC BIOSECURITY - EMERGENCY MANAGMENT  LIVE LINK.docx", title: "RC BIOSECURITY � EMERGENCY MANAGEMENT", ext: "doc", page: 1 }
                    ]
                },
                {
                    name:"Biosecurity Chemicals",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY ChemMisuse_brochure_updatedOct29FINAL.pdf", title: "Guidelines for Reporting Agricultural and Veterinary Chemical Misuse", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY PIC New_Registration_Application_Form_July_2013.pdf", title: "REGISTRATION / PIC APPLICATION FORM ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY Reducing harm to Honey Bees from Pesticides.pdf", title: "Reducing Harm to Honey Bees from Pesticides ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC Biosecurity Rural chemicals.docx", title: "Rural chemicals", ext: "doc", page: 1 }

                    ]
                },
                {
                    name:"Bushfire Planning",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producer1.docx", title: "Bushfire information for primary producers", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producers.docx", title: "Bushfire information for primary produce", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire_Prevention_and_Preparedness_S2.pdf", title: "Bushfire Prevention and Preparedness", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFA BUSHFIRE - PROTECTION OF FODDER RESERVES.pdf", title: "Protection of Fodder Reserves", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRE FACT SHEET 303_EMERGENCY KITS.pdf", title: "Emergency Kits", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf", title: "Care of Pets and Livestock", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES AND HORSES.pdf", title: "Horses and Bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS FACT SHEET - AFTER THE FIRE.pdf", title: "After the Fire", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CHECKLIST - Farming Guidelines for Reducing Bushfires.PNG", title: "Farming Guidelines for Reducing Bushfires", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming guidelines for reduction of bushfire - SUMMARY.docx", title: "Farming guidelines for reduction of bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming_Guidelines_for_the_Reduction_of_Bushfire_Risk.pdf", title: "Farming Guidelines for the Reduction of Bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/GUIDE to_bushfire_safety_2014.pdf", title: "Your guide to bushfire safety", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Livestock_Safety_in_Bushfires_Sept2012_version.pdf", title: "LIVESTOCK SAFETY DURING BUSHFIRES", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Recovering after bushfires.docx", title: "Recovering after bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/STOCK OWNERS Risk-management-in-times-of-fire-and-flood.pdf", title: "Risk management for stock owners in times of fire and flood", ext: "pdf", page: 1 },

                    ]
                },
                {
                    name: "Emergency Checklists",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG", title: "Biosecurity Emergency hotlines", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG", title: "HEATWAVES RISK", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC BIOSECURITY - EMERGENCY MANAGMENT  LIVE LINK.docx", title: "EMERGENCY MANAGMENT  LIVE LINK", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf", title: "Disaster Planning - Bushfire Checklis", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf", title: "Disaster Planning - Earthquake Action Plan", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf", title: "Disaster Planning - Emergency Contacts", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf", title: "Disaster Planning - Extreme Heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf", title: "Disaster Planning - Flood Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Home Fire Safety Checklist.pdf", title: "Disaster Planning - Home Fire Safety Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf", title: "Personal Medical Details", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf", title: "Extreme heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf", title: "Extreme Heat Guide", ext: "pdf", page: 1 },

                    ]
                }                 
            ]            
        }).Init();

        //Natural Resource Management
        var BS = new fileWidget({
            IDdvContainer: "#_dvNRM",
            iconWidth: "40px",
            iconHeight: "40px",
                     Docs: [
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC LAND MANAGEMENT.docx", title: "LAND MANAGEMENT", ext: "doc", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC WATER MANAGEMENT.docx", title: "WATER MANAGEMENT", ext: "doc", page: 1 },
                     ]
        }).Init();

        //Forms and Payments
        var BS = new fileWidget({
            IDdvContainer: "#_dvFP",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/RDIF_Application_form_amended_16_July.docx", title: "Regional Development and Innovation Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA Capacity information form (1)  FORMS.doc", title: "Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA WC Medical release authority (1) FORMS.doc", title: "Medical release authority", ext: "doc", page: 1 },
            ],
        }).Init();


        //Business and Finance
        var BS = new fileWidget({
            IDdvContainer: "#_dvBF",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                {
                    name: "Goverment programs",
                    Docs: [
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RDIF_Information_Sessions_-_July_2014.pdf", title: "RDIF_Information_Sessions_-_July_2014", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES.pdf", title: "SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SARMS intro.docx", title: "SARMS intro", ext: "doc", page: 1 },
                    ],
                },
                {
                    name: "Goverment funds",
                    Docs: [
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Enterprise Zone Fund.docx", title: "Enterprise Zone Fund", ext: "doc", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Guidelines for the Country Cabinet Program.docx", title: "Guidelines for the Country Cabinet Program", ext: "doc", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA Regional Development Fund.docx", title: "PIRSA Regional Development Fund", ext: "doc", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA-managed Grants.docx", title: "PIRSA-managed Grants", ext: "doc", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Development Australia Funding.docx", title: "Regional Development Australia Funding", ext: "doc", page: 1 },
                    //{ path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Food Initiatives Program Recipients 2014.PNG", title: "Regional Food Initiatives Program Recipients 2014", ext: "png", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Riverland Sustainable Futures Fund.docx", title: "Riverland Sustainable Futures Fund", ext: "doc", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE.pdf", title: "SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE", ext: "pdf", page: 1 },
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/National Stronger Regions Fund.docx", title: "National Stronger Regions Fund", ext: "doc", page: 1 }
                    ],
                },
                {
                    name: "Farm Support and Advice ",
                    Docs: [
                    { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RC Farm support and advice.docx", title: "Farm support and advice", ext: "doc", page: 1 },
                    ],
                },
            ]

        }).Init();

        //Quality Assurance
        var BS = new fileWidget({
            IDdvContainer: "#_dvQA",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/qualityassurance/APIQ QA Standards.pdf", title: "APIQ QA Standards", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/qualityassurance/FACT-SHEET-APIQ.pdf", title: "FACT-SHEET-APIQ", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/qualityassurance/Pig Identification.docx", title: "Pig Identification", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/qualityassurance/Pig Pass.docx", title: "Pig Pass", ext: "doc", page: 1 }
            ],
        }).Init();

        //Forms
        var BS = new fileWidget({
            IDdvContainer: "#_dvF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA Capacity information form (1)  FORMS.doc", title: "PORK SA Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "PORK SA WC Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Medical release authority (1) FORMS.doc", title: "PORK SA WC Medical release authority", ext: "doc", page: 1 }
            ],
        }).Init();

        //Useful Info
        var BS = new fileWidget({
            IDdvContainer: "#_dvUS",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0013/214330/RB201300017.pdf", title: "Understanding dryland farming", ext: "pdf", page: 1 },
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0014/214331/RB201300018.pdf", title: "Understanding mineral exploration", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/health and safety/Pork SA Animal Welfare and Farm Raids.pdf", title: "Animal Welfare and Farm Raids", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/health and safety/RC PORK GATED - animal welfare pigs.docx", title: "Animal Welfare and Farm Raids", ext: "doc", page: 1 },
            ],
        }).Init();

        // end:file

        //Committee Members

        var CM = [
            { name: "Andrew Johnson", title: null, contactm: "0427849467", email: "asjohnson.mtb@bigpond.com", profile: "<strong>Piggery/Business profile</strong> - Director of Mt. Boothby Pastoral a mixed family farming operation spread over multiple sites in SA, producing beef, lamb, wool, mixed cropping, lucerne seed and have a multi-site farrow to finish pork production operation.Andrew holds several positions in industry bodies both at the state and nationally. These include:<br><ul><li>Pork SA Vice Chairman</li><li>APL Director</li><li>PPSA Councillor</li><li>Pork NLIS Chairman</li><li>APL Animal Welfare and Quality Assurance Committee Chairman</li><li>Vice Chairman Nuffield Australia</li></ul>" },
            { name: "Mark McLean", title: null, contactm: "0427 138 919", email: "mclean_mark@bigpond.com", profile: "<strong>Piggery/Business profile</strong> - Owner and General Manager of Riverhaven Enterprises, 700 sow farrow to finish pig farm combined with horticultural interests. Riverhaven Enterprises is a family farm which has been operating in the Riverland for over 50 years. The business operates using principles of sustainability and resource re-use to complement its various operations. It composts straw-based pig shelter bedding which is sold externally and used on its Citrus and Olive orchards to provide nutrient value and increase soil water holding capacity to reduce water requirements.<br>The company constantly review procedures and production techniques to try and remain competitive and achieve good outcomes for our pork quality. Modern housing and innovation with genetics and husbandry are vital to ensure long-term pig farm profitability. The family has been co-founders of Top Multiplier, 1,000 sow farrow to finish pig unit at Bower.<br>Mark has wide industry interests including:<br><ul><li>commitment to assist in helping achieve a sustainable future for all Australian pig farmers</li><li>commitment to see Australian pork consumption grow and the national industry remain profitable</li><li>improving the image of farming and encourage future generations to participate</li><li>overseas travel to investigate different methods of farming and food marketing</li><li>APL Delegate since 2010</li><li>Committee Member of APL Specialist Group 1 - 'Marketing, Supply Chain and Product Quality'.</li></ul>" },
            { name: "Garry Tiss", title: null, contactm: "0418817703", email: "gtiss@bigpond.net.au", profile: "<strong>Piggery/Business profile</strong> - Has interest in two piggeries with more than 2000 sows. T & D Pig Marketers (agents for Elders Ltd) specialising in marketing live pigs and organising contracts with processors for past 36 years. Particular interests of Garry include trying to bring pork processors and growers closer together for a better outcome for both parties i.e. pig pricing structures and carcase gradings." },
            { name: "Butch Moses", title: null, contactm: "0428 64 2243", email: "butch.moses@internode.on.net", profile: "<strong>Piggery/Business profile</strong> - Salt Lake Bacon. 550 sows farrow to finish (sow stall free). Butch has a wide interest in the industry and is involved as:<br><ul><li>Top Pork Board Director</li><li>ex SABOR Board Director</li><li>ex Chairman SAFF Pork Committee</li></ul>" },
            { name: "Rod Hamann", title: null, contactm: "0421 072 779", email: "rod@austporkfarms.com.au", profile: "<strong>Piggery/Business profile</strong> Rod is a South Australian who is a past graduate of Roseworthy Agricultural College. Through an initial career with Pig Improvement Company that took him to the UK and then a transfer to the USA, Rod has now lived and worked in the pig industries of three continents, plus had the opportunity to travel and consult in various capacities in South America, Europe and the USA.<br>Prior to returning to Australia in 1999 to establish an Ag consulting business, he held senior executive positions within Murphy Family Farms (the largest pig producer in the world) and Heartland Pork Enterprises. In the latter role, as Chief Operating Officer he was directly responsible to the Chairman for all operational areas for this 60,000-sow Production and Genetics operation.<br>He currently has various consulting positions, but has a primary role and responsibility as the Chief Executive Officer / Managing Director of Australian Pork Farms Group Limited, which includes a number of pig production businesses including Wasleys and Sheaoak Piggeries. The group has an investment interest in Big River Pork; plus they are a core foundation investor of the Pork CRC.<br>Industry interests and positions include:<br><ul><li>Director Auspork marketing group</li><li>Director Big River Pork Abattoir</li><li>Director Pork CRC and member of Pork CRC R & D Sub Committee</li><li>Director Porkscan</li><li>Delegate APL</li><li>Member SA Pig Industry Advisory Group (PIAG)</li></ul>" },
            { name: "Christine Sapwell", title: "Pork SA Secretary/Treasurer", contactm: "0408 800 011", email: "c.sapwell@bigpond.com", profile: "<strong>Piggery/Business profile</strong> Owner of Norsap which commenced in the pig breeding industry in1968. The piggery has recently come to an end of it�s useful life with the last pigs leaving the property on 26th November2012. Christine still retains an interest in the Australian Pork Farms Group. Christine has been involved in Industry organizations from the early 1970�s with involvement in SA Farmers Federation, Australian Pork Corporation, Delegate to Pork Council of Australia, Australian Pork Limited, Pig Industry Advisory Group (current), & the Pig Industry Development Board. She has also escorted a number of Pig Study Tours both overseas and within Australia. Christine is keen to see pork producers well represented to Government and legislators and are assisted to cope with change to enable survival in the industry.<Br>Other current industry positions held by Christine include:<Br><ul><li>Shareholder within the Australian Pork Farms Group which has piggery interests on the Adelaide Plains and Murray Bridge areas</li><li>Treasurer of the Ronald J Lienert Memorial Scholarship Fund whose aim is to provide a bursary to a student to encourage their involvement in a research project</li></ul>" },
            { name: "Nick Lienert", title: null, contactm: null, email: null },
            { name: "David Reu", title: null, contactm: null, email: null }
        ];
        committeeWidget.Init({
            list: CM,
            dvlist: "#dvCommitteMembersList",
        });

        // end Committee Members

        /* end: Custom JSs */
    },
    initGrain: function () {

        // start:currency

        //$("#sidebar, #sidebar-alt, .sidebar-header").hide();

       // currencyExchange.Init();

        // end:currency

        // start:newsRSS

        Index.feeds.push("http://news.google.com/news?pz=1&cf=all&ned=au&hl=en&q=grain+australia&output=rss");

        var news = new newsRSS({ feeds: Index.feeds }).Init();

        // end:newsRSS

        // start:fire alerts

       // firealertsWidget.Init();

        // end:fire alerts

        // start:weather alerts

       // weatherWidget.Init();

        // end:weather alerts

        // start:file

        //Education and Training
        /*
        var ET = new fileWidget({
            IDdvContainer: "#_dvET",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news%20archive/Pork%20SA%20Snapshots%20August%202014.pdf", title: "Farm business Skills", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/education%20and%20training/Stepping%20Into%20Leadership-2.pdf", title: "Women in Leadership", ext: "pdf", page: 1 }
            ],
        }).Init();

        //Industrial Relations
        var IR = new fileWidget({
            IDdvContainer: "#_dvIR",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity information form.doc", title: "Workers Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity Letter to Doctor TEMPLATE.doc", title: "Workers Capacity Letter to Doctor Template", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity Medical release authority TEMPLATE.doc", title: "Workers Capacity Medical release authority Template", ext: "doc", page: 1 }
            ],
        }).Init();

        //News Archive
        var NA = new fileWidget({
            IDdvContainer: "#_dvNA",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [                
            ],
        }).Init();

        //Workcover
        var WC = new fileWidget({
            IDdvContainer: "#_dvWC",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/workcover/WorkCover Levy Rates 2013-14.pdf", title: "WorkCover Levy Rate", ext: "pdf", page: 1 }
            ],
        }).Init();

        //Health & Safety
        var HS = new fileWidget({
            IDdvContainer: "#_dvHS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [               
                {
                    name: "Safe Work Practice",
                    Docs: [
                            { path: "http://www.safework.sa.gov.au/uploaded_files/pmc1i.pdf", title: "Agricultural motorbikes", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/angle_grinder_safety.pdf", title: "Angle grinder safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs602i.pdf", title: "Basic accident investigation", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs38i.pdf", title: "Confined spaces (working in)", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs661i.pdf", title: "Computer workstations design", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf", title: "Disc grinder safety in the shearing industry", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs4i.pdf", title: "Forklift safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs57i.pdf", title: "Ladder safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/GS52-3.pdf", title: "Noise control in the workplace", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/rs8i.pdf", title: "Pesticide safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/remote_safe_staff.pdf", title: "Remote location staff safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/sy1i.pdf", title: "Stockyard safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs34i.pdf", title: "Young worker safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs51i.pdf", title: "Zoonosis", ext: "pdf", page: 1 }
                    ]
                },
            ]
        }).Init();
        
        //Biosecurity
        var BS = new fileWidget({
            IDdvContainer: "#_dvBS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                {
                    name: "Biosecurity Chemicals",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY ChemMisuse_brochure_updatedOct29FINAL.pdf", title: "Guidelines for Reporting Agricultural and Veterinary Chemical Misuse", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY PIC New_Registration_Application_Form_July_2013.pdf", title: "REGISTRATION / PIC APPLICATION FORM ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY Reducing harm to Honey Bees from Pesticides.pdf", title: "Reducing Harm to Honey Bees from Pesticides ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC Biosecurity Rural chemicals.docx", title: "Rural chemicals", ext: "doc", page: 1 }

                    ]
                },
                {
                    name: "Bushfire Planning",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producer1.docx", title: "Bushfire information for primary producers", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producers.docx", title: "Bushfire information for primary produce", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire_Prevention_and_Preparedness_S2.pdf", title: "Bushfire Prevention and Preparedness", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFA BUSHFIRE - PROTECTION OF FODDER RESERVES.pdf", title: "Protection of Fodder Reserves", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRE FACT SHEET 303_EMERGENCY KITS.pdf", title: "Emergency Kits", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf", title: "Care of Pets and Livestock", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES AND HORSES.pdf", title: "Horses and Bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS FACT SHEET - AFTER THE FIRE.pdf", title: "After the Fire", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CHECKLIST - Farming Guidelines for Reducing Bushfires.PNG", title: "Farming Guidelines for Reducing Bushfires", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming guidelines for reduction of bushfire - SUMMARY.docx", title: "Farming guidelines for reduction of bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming_Guidelines_for_the_Reduction_of_Bushfire_Risk.pdf", title: "Farming Guidelines for the Reduction of Bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/GUIDE to_bushfire_safety_2014.pdf", title: "Your guide to bushfire safety", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Livestock_Safety_in_Bushfires_Sept2012_version.pdf", title: "LIVESTOCK SAFETY DURING BUSHFIRES", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Recovering after bushfires.docx", title: "Recovering after bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/STOCK OWNERS Risk-management-in-times-of-fire-and-flood.pdf", title: "Risk management for stock owners in times of fire and flood", ext: "pdf", page: 1 },

                    ]
                },
                {
                    name: "Emergency Checklists",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG", title: "Biosecurity Emergency hotlines", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG", title: "HEATWAVES RISK", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC BIOSECURITY - EMERGENCY MANAGMENT  LIVE LINK.docx", title: "EMERGENCY MANAGMENT  LIVE LINK", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf", title: "Disaster Planning - Bushfire Checklis", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf", title: "Disaster Planning - Earthquake Action Plan", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf", title: "Disaster Planning - Emergency Contacts", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf", title: "Disaster Planning - Extreme Heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf", title: "Disaster Planning - Flood Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Home Fire Safety Checklist.pdf", title: "Disaster Planning - Home Fire Safety Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf", title: "Personal Medical Details", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf", title: "Extreme heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf", title: "Extreme Heat Guide", ext: "pdf", page: 1 },

                    ]
                },
                 {
                     name: "Natural Resources Management",
                     Docs: [
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC LAND MANAGEMENT.docx", title: "LAND MANAGEMENT", ext: "doc", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC WATER MANAGEMENT.docx", title: "WATER MANAGEMENT", ext: "doc", page: 1 },
                     ]
                 }
            ]
        }).Init();

        //Forms and Payments
        var BS = new fileWidget({
            IDdvContainer: "#_dvFP",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/RDIF_Application_form_amended_16_July.docx", title: "Regional Development and Innovation Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA Capacity information form (1)  FORMS.doc", title: "Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA WC Medical release authority (1) FORMS.doc", title: "Medical release authority", ext: "doc", page: 1 },
            ],
        }).Init();

        //Business and Finance
        var BS = new fileWidget({
            IDdvContainer: "#_dvBF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Enterprise Zone Fund.docx", title: "Enterprise Zone Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Guidelines for the Country Cabinet Program.docx", title: "Guidelines for the Country Cabinet Program", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA Regional Development Fund.docx", title: "PIRSA Regional Development Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA-managed Grants.docx", title: "PIRSA-managed Grants", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RDIF_Information_Sessions_-_July_2014.pdf", title: "RDIF_Information_Sessions_-_July_2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Development Australia Funding.docx", title: "Regional Development Australia Funding", ext: "doc", page: 1 },
                //{ path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Food Initiatives Program Recipients 2014.PNG", title: "Regional Food Initiatives Program Recipients 2014", ext: "png", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Riverland Sustainable Futures Fund.docx", title: "Riverland Sustainable Futures Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES.pdf", title: "SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE.pdf", title: "SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SARMS intro.docx", title: "SARMS intro", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RC Farm support and advice.docx", title: "Farm support and advice", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/National Stronger Regions Fund.docx", title: "National Stronger Regions Fund", ext: "doc", page: 1 }
            ],
        }).Init();       

        //Forms
        var BS = new fileWidget({
            IDdvContainer: "#_dvF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA Capacity information form (1)  FORMS.doc", title: "PORK SA Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "PORK SA WC Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Medical release authority (1) FORMS.doc", title: "PORK SA WC Medical release authority", ext: "doc", page: 1 }
            ],
        }).Init();

        //Useful Info
        var BS = new fileWidget({
            IDdvContainer: "#_dvUS",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0013/214330/RB201300017.pdf", title: "Understanding dryland farming", ext: "pdf", page: 1 },
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0014/214331/RB201300018.pdf", title: "Understanding mineral exploration", ext: "pdf", page: 1 },
            ],
        }).Init();

        // end:file

        /* end: Custom JSs */
    },
    initDairyfarmers: function () {

        // start:currency

        //currencyExchange.Init();

        // end:currency

        // start:newsRSS

        Index.feeds.push("http://news.google.com/news?pz=1&cf=all&ned=au&hl=en&q=dairy+south+australia&output=rss");

        var news = new newsRSS({ feeds: Index.feeds }).Init();

        // end:newsRSS

        // start:fire alerts

        //firealertsWidget.Init();

        // end:fire alerts

        // start:weather alerts

        //weatherWidget.Init();

        // end:weather alerts

        // start:file

        //Education and Training
        /*
        var ET = new fileWidget({
            IDdvContainer: "#_dvET",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/dairy/education/TAFE DAIRY SHORT COURSES.docx", title: "TAFE Dairy short courses", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/dairy/education/RC EDUCATION AND TRAINING - TAFE Dairy diplomas.docx", title: "TAFE Dairy diplomas", ext: "doc", page: 1 }
            ],
        }).Init();

        //Industrial Relations
        var IR = new fileWidget({
            IDdvContainer: "#_dvIR",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                //{ path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Industrial Relations - Member Update September  2014.pdf", title: "MERS Members Update", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity information form.doc", title: "Workers Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity Letter to Doctor TEMPLATE.doc", title: "Workers Capacity Letter to Doctor Template", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/industrial relations/Workers Capacity Medical release authority TEMPLATE.doc", title: "Workers Capacity Medical release authority Template", ext: "doc", page: 1 }
            ],
        }).Init();

        //News Archive
        var NA = new fileWidget({
            IDdvContainer: "#_dvNA",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                
            ],
        }).Init();

        //Workcover
        var WC = new fileWidget({
            IDdvContainer: "#_dvWC",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/workcover/WorkCover Levy Rates 2013-14.pdf", title: "WorkCover Levy Rate", ext: "pdf", page: 1 }
            ],
        }).Init();

        //Health & Safety
        var HS = new fileWidget({
            IDdvContainer: "#_dvHS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                {
                    name: "Safe Work Practice",
                    Docs: [
                            { path: "http://www.safework.sa.gov.au/uploaded_files/pmc1i.pdf", title: "Agricultural motorbikes", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/angle_grinder_safety.pdf", title: "Angle grinder safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs602i.pdf", title: "Basic accident investigation", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs38i.pdf", title: "Confined spaces (working in)", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs661i.pdf", title: "Computer workstations design", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf", title: "Disc grinder safety in the shearing industry", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs4i.pdf", title: "Forklift safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs57i.pdf", title: "Ladder safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/GS52-3.pdf", title: "Noise control in the workplace", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/rs8i.pdf", title: "Pesticide safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/remote_safe_staff.pdf", title: "Remote location staff safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/sy1i.pdf", title: "Stockyard safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs34i.pdf", title: "Young worker safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs51i.pdf", title: "Zoonosis", ext: "pdf", page: 1 }
                    ]
                },
            ]
        }).Init();

        //Biosecurity
        var BS = new fileWidget({
            IDdvContainer: "#_dvBS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                {
                    name: "Animal Health",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/EAD-Risk-Management-Manual.pdf", title: "Risk management planning for an emergency animal disease outbreak", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/Emergency animal disease preparedness.docx", title: "Emergency animal disease preparedness", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/Emergency animal disease preparedness.pdf", title: "Emergency animal disease preparedness", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/Livestock and pet owners urged to prepare for heat.docx", title: "Livestock and pet owners urged to prepare for heat", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/Preparing-your-business-to-survive-an-emergency-animal-disease-outbreak.pdf", title: "Preparing your business", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/RC BIOSECURITY - EMERGENCY MANAGMENT  LIVE LINK.docx", title: "RC BIOSECURITY � EMERGENCY MANAGEMENT", ext: "doc", page: 1 }
                    ]
                },
                {
                    name: "Biosecurity Chemicals",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY ChemMisuse_brochure_updatedOct29FINAL.pdf", title: "Guidelines for Reporting Agricultural and Veterinary Chemical Misuse", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY PIC New_Registration_Application_Form_July_2013.pdf", title: "REGISTRATION / PIC APPLICATION FORM ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY Reducing harm to Honey Bees from Pesticides.pdf", title: "Reducing Harm to Honey Bees from Pesticides ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC Biosecurity Rural chemicals.docx", title: "Rural chemicals", ext: "doc", page: 1 }

                    ]
                },
                {
                    name: "Bushfire Planning",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producer1.docx", title: "Bushfire information for primary producers", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producers.docx", title: "Bushfire information for primary produce", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire_Prevention_and_Preparedness_S2.pdf", title: "Bushfire Prevention and Preparedness", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFA BUSHFIRE - PROTECTION OF FODDER RESERVES.pdf", title: "Protection of Fodder Reserves", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRE FACT SHEET 303_EMERGENCY KITS.pdf", title: "Emergency Kits", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf", title: "Care of Pets and Livestock", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES AND HORSES.pdf", title: "Horses and Bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS FACT SHEET - AFTER THE FIRE.pdf", title: "After the Fire", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CHECKLIST - Farming Guidelines for Reducing Bushfires.PNG", title: "Farming Guidelines for Reducing Bushfires", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming guidelines for reduction of bushfire - SUMMARY.docx", title: "Farming guidelines for reduction of bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming_Guidelines_for_the_Reduction_of_Bushfire_Risk.pdf", title: "Farming Guidelines for the Reduction of Bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/GUIDE to_bushfire_safety_2014.pdf", title: "Your guide to bushfire safety", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Livestock_Safety_in_Bushfires_Sept2012_version.pdf", title: "LIVESTOCK SAFETY DURING BUSHFIRES", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Recovering after bushfires.docx", title: "Recovering after bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/STOCK OWNERS Risk-management-in-times-of-fire-and-flood.pdf", title: "Risk management for stock owners in times of fire and flood", ext: "pdf", page: 1 },

                    ]
                },
                {
                    name: "Emergency Checklists",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG", title: "Biosecurity Emergency hotlines", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG", title: "HEATWAVES RISK", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC BIOSECURITY - EMERGENCY MANAGMENT  LIVE LINK.docx", title: "EMERGENCY MANAGMENT  LIVE LINK", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf", title: "Disaster Planning - Bushfire Checklis", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf", title: "Disaster Planning - Earthquake Action Plan", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf", title: "Disaster Planning - Emergency Contacts", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf", title: "Disaster Planning - Extreme Heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf", title: "Disaster Planning - Flood Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Home Fire Safety Checklist.pdf", title: "Disaster Planning - Home Fire Safety Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf", title: "Personal Medical Details", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf", title: "Extreme heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf", title: "Extreme Heat Guide", ext: "pdf", page: 1 },

                    ]
                }                 
            ]
        }).Init();

        //Natural Resource Management
        var BS = new fileWidget({
            IDdvContainer: "#_dvNRM",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                 {
                     name: "Water and Land",
                     Docs: [
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC LAND MANAGEMENT.docx", title: "LAND MANAGEMENT", ext: "doc", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC WATER MANAGEMENT.docx", title: "WATER MANAGEMENT", ext: "doc", page: 1 },
                     ]
                 }
            ]
        }).Init();

        //Forms and Payments
        var BS = new fileWidget({
            IDdvContainer: "#_dvFP",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/RDIF_Application_form_amended_16_July.docx", title: "Regional Development and Innovation Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA Capacity information form (1)  FORMS.doc", title: "Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA WC Medical release authority (1) FORMS.doc", title: "Medical release authority", ext: "doc", page: 1 },
            ],
        }).Init();

        //Business and Finance
        var BS = new fileWidget({
            IDdvContainer: "#_dvBF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Enterprise Zone Fund.docx", title: "Enterprise Zone Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Guidelines for the Country Cabinet Program.docx", title: "Guidelines for the Country Cabinet Program", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA Regional Development Fund.docx", title: "PIRSA Regional Development Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA-managed Grants.docx", title: "PIRSA-managed Grants", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RDIF_Information_Sessions_-_July_2014.pdf", title: "RDIF_Information_Sessions_-_July_2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Development Australia Funding.docx", title: "Regional Development Australia Funding", ext: "doc", page: 1 },
                //{ path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Food Initiatives Program Recipients 2014.PNG", title: "Regional Food Initiatives Program Recipients 2014", ext: "png", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Riverland Sustainable Futures Fund.docx", title: "Riverland Sustainable Futures Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES.pdf", title: "SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE.pdf", title: "SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SARMS intro.docx", title: "SARMS intro", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RC Farm support and advice.docx", title: "Farm support and advice", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/National Stronger Regions Fund.docx", title: "National Stronger Regions Fund", ext: "doc", page: 1 }
            ],
        }).Init();

        //Forms
        var BS = new fileWidget({
            IDdvContainer: "#_dvF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA Capacity information form (1)  FORMS.doc", title: "PORK SA Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "PORK SA WC Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Medical release authority (1) FORMS.doc", title: "PORK SA WC Medical release authority", ext: "doc", page: 1 }
            ],
        }).Init();

        //Useful Info
        var BS = new fileWidget({
            IDdvContainer: "#_dvUS",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0013/214330/RB201300017.pdf", title: "Understanding dryland farming", ext: "pdf", page: 1 },
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0014/214331/RB201300018.pdf", title: "Understanding mineral exploration", ext: "pdf", page: 1 },
            ],
        }).Init();

        // end:file

        /* end: Custom JSs */
    },
    initWinegrape: function () {

        // start:currency

        currencyExchange.Init();

        // end:currency

        // start:newsRSS

        HotNews = [];

        HotNews.push("http://winebiz.com.au/rss/");
        HotNews.push("http://wineindustryinsight.com/?feed=rss2");

        Index.feeds.push("http://news.google.com/news?pz=1&cf=all&ned=au&hl=en&q=wine+grape+%22south+australia%22&output=rss");

        var news = new newsRSS({ feeds: Index.feeds }).Init();

        var news = new newsRSS({ feeds: HotNews, IDdvContainer: "#dvHotNews" }).Init();

        // end:newsRSS

        // start:fire alerts

        firealertsWidget.Init();

        // end:fire alerts

        // start:weather alerts

        weatherWidget.Init();

        // end:weather alerts

        // Charts

        // Get the elements where we will attach the charts
        var dashWidgetChart = $('#dash-widget-chart');

        // Random data for the chart
        var dataEarnings = [[1, 1560], [2, 1650], [3, 1320], [4, 1950], [5, 1800], [6, 2400], [7, 2100], [8, 2550], [9, 3300], [10, 3900], [11, 4200], [12, 4500]];
        var dataSales = [[1, 500], [2, 420], [3, 480], [4, 350], [5, 600], [6, 850], [7, 1100], [8, 950], [9, 1220], [10, 1300], [11, 1500], [12, 1700]];

        // Array with month labels used in chart
        var chartMonths = [[1, 'January'], [2, 'February'], [3, 'March'], [4, 'April'], [5, 'May'], [6, 'June'], [7, 'July'], [8, 'August'], [9, 'September'], [10, 'October'], [11, 'November'], [12, 'December']];

        // Initialize Dash Widget Chart
        $.plot(dashWidgetChart,
            [
                {
                    data: dataEarnings,
                    lines: { show: true, fill: false },
                    points: { show: true, radius: 6, fillColor: '#cccccc' }
                },
                {
                    data: dataSales,
                    lines: { show: true, fill: false },
                    points: { show: true, radius: 6, fillColor: '#ffffff' }
                }
            ],
            {
                colors: ['#ffffff', '#353535'],
                legend: { show: false },
                grid: { borderWidth: 0, hoverable: true, clickable: true },
                yaxis: { show: false },
                xaxis: { show: false, ticks: chartMonths }
            }
        );

        // Creating and attaching a tooltip to the widget
        var previousPoint = null, ttlabel = null;
        dashWidgetChart.bind('plothover', function (event, pos, item) {

            if (item) {
                if (previousPoint !== item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $('#chart-tooltip').remove();
                    var x = item.datapoint[0], y = item.datapoint[1];

                    // Get xaxis label
                    var monthLabel = item.series.xaxis.options.ticks[item.dataIndex][1];

                    if (item.seriesIndex === 1) {
                        ttlabel = '<strong>' + y + '</strong> sales in <strong>' + monthLabel + '</strong>';
                    } else {
                        ttlabel = '$ <strong>' + y + '</strong> in <strong>' + monthLabel + '</strong>';
                    }

                    $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                        .css({ top: item.pageY - 50, left: item.pageX - 50 }).appendTo("body").show();
                }
            }
            else {
                $('#chart-tooltip').remove();
                previousPoint = null;
            }
        });

        // end: Charts

        // start:file

        //Education and Training
        var ET = new fileWidget({
            IDdvContainer: "#_dvET",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC Education and training TAFE VITICULTURE.docx", title: "TAFE Viticulture", ext: "doc", page: 1 }],
        }).Init();

        //Industrial Relations
        var IR = new fileWidget({
            IDdvContainer: "#_dvIR",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                
            ],
        }).Init();

        //News Archive
        var NA = new fileWidget({
            IDdvContainer: "#_dvNA",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/Pork SA Committee for 2014.docx", title: "MERS Members Update", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/Pork SA Snapshots August 2014.pdf", title: "Pork SA Snapshots August 2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/Pork SA Snapshots June 2014.pdf", title: "Pork SA Snapshots June 2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/Pork SA Snapshots March 2014.pdf", title: "Pork SA Snapshots March 2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news archive/THE PORK PAGE September 2014.pdf", title: "The Pork Page September 2014", ext: "pdf", page: 1 }
            ],
        }).Init();

        //Workcover
        var WC = new fileWidget({
            IDdvContainer: "#_dvWC",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/workcover/WorkCover Levy Rates 2013-14.pdf", title: "WorkCover Levy Rate", ext: "pdf", page: 1 }
            ],
        }).Init();

        //Health & Safety
        var HS = new fileWidget({
            IDdvContainer: "#_dvHS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                {
                    name: "Safe Work Practice",
                    Docs: [
                            { path: "http://www.safework.sa.gov.au/uploaded_files/pmc1i.pdf", title: "Agricultural motorbikes", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/angle_grinder_safety.pdf", title: "Angle grinder safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs602i.pdf", title: "Basic accident investigation", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs38i.pdf", title: "Confined spaces (working in)", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs661i.pdf", title: "Computer workstations design", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf", title: "Disc grinder safety in the shearing industry", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs4i.pdf", title: "Forklift safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs57i.pdf", title: "Ladder safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/GS52-3.pdf", title: "Noise control in the workplace", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/rs8i.pdf", title: "Pesticide safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/remote_safe_staff.pdf", title: "Remote location staff safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/sy1i.pdf", title: "Stockyard safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs34i.pdf", title: "Young worker safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs51i.pdf", title: "Zoonosis", ext: "pdf", page: 1 }
                    ]
                },
            ]
        }).Init();

        //Biosecurity
        var BS = new fileWidget({
            IDdvContainer: "#_dvBS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [                
                {
                    name: "Biosecurity Chemicals",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY ChemMisuse_brochure_updatedOct29FINAL.pdf", title: "Guidelines for Reporting Agricultural and Veterinary Chemical Misuse", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY PIC New_Registration_Application_Form_July_2013.pdf", title: "REGISTRATION / PIC APPLICATION FORM ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY Reducing harm to Honey Bees from Pesticides.pdf", title: "Reducing Harm to Honey Bees from Pesticides ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC Biosecurity Rural chemicals.docx", title: "Rural chemicals", ext: "doc", page: 1 }

                    ]
                },
                {
                    name: "Bushfire Planning",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producer1.docx", title: "Bushfire information for primary producers", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producers.docx", title: "Bushfire information for primary produce", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire_Prevention_and_Preparedness_S2.pdf", title: "Bushfire Prevention and Preparedness", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFA BUSHFIRE - PROTECTION OF FODDER RESERVES.pdf", title: "Protection of Fodder Reserves", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRE FACT SHEET 303_EMERGENCY KITS.pdf", title: "Emergency Kits", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf", title: "Care of Pets and Livestock", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES AND HORSES.pdf", title: "Horses and Bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS FACT SHEET - AFTER THE FIRE.pdf", title: "After the Fire", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CHECKLIST - Farming Guidelines for Reducing Bushfires.PNG", title: "Farming Guidelines for Reducing Bushfires", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming guidelines for reduction of bushfire - SUMMARY.docx", title: "Farming guidelines for reduction of bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming_Guidelines_for_the_Reduction_of_Bushfire_Risk.pdf", title: "Farming Guidelines for the Reduction of Bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/GUIDE to_bushfire_safety_2014.pdf", title: "Your guide to bushfire safety", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Livestock_Safety_in_Bushfires_Sept2012_version.pdf", title: "LIVESTOCK SAFETY DURING BUSHFIRES", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Recovering after bushfires.docx", title: "Recovering after bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/STOCK OWNERS Risk-management-in-times-of-fire-and-flood.pdf", title: "Risk management for stock owners in times of fire and flood", ext: "pdf", page: 1 },

                    ]
                },
                {
                    name: "Emergency Checklists",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG", title: "Biosecurity Emergency hotlines", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG", title: "HEATWAVES RISK", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC BIOSECURITY - EMERGENCY MANAGMENT  LIVE LINK.docx", title: "EMERGENCY MANAGMENT  LIVE LINK", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf", title: "Disaster Planning - Bushfire Checklis", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf", title: "Disaster Planning - Earthquake Action Plan", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf", title: "Disaster Planning - Emergency Contacts", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf", title: "Disaster Planning - Extreme Heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf", title: "Disaster Planning - Flood Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Home Fire Safety Checklist.pdf", title: "Disaster Planning - Home Fire Safety Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf", title: "Personal Medical Details", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf", title: "Extreme heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf", title: "Extreme Heat Guide", ext: "pdf", page: 1 },

                    ]
                },                 
                 {
                     name: "Fruit Fly",
                     Docs: [
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/fruitfly/Fruit Fly Fact_Sheet.pdf", title: "Fruit Fly Fact Sheet", ext: "pdf", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/fruitfly/Fruit Fly intro - prevention, detection, eradication.docx", title: "Fruit Fly intro - prevention, detection, eradication ", ext: "pdf", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/fruitfly/Fruit_fly_detection.pdf", title: "Fruit Fly detection ", ext: "pdf", page: 1 }
                     ]
                 },
            ]
        }).Init();

        //Natural Resource Management
        var BS = new fileWidget({
            IDdvContainer: "#_dvNRM",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                 {
                     name: "Water and Land",
                     Docs: [
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC LAND MANAGEMENT.docx", title: "LAND MANAGEMENT", ext: "doc", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC WATER MANAGEMENT.docx", title: "WATER MANAGEMENT", ext: "doc", page: 1 },
                     ]
                 }
            ]
        }).Init();

        //Forms and Payments
        var BS = new fileWidget({
            IDdvContainer: "#_dvFP",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES 2014_Adelaide_Hills_Contributions.pdf", title: "2014 Adelaide Hills Contributions", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES 2014_Barossa_Contributions.pdf", title: "2014 Barossa Contributions", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES 2014_Clare_Valley_Contributions.pdf", title: "2014 Clare Valley Contributions", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES 2014_Langhorne_Creek_Contributions.pdf", title: "2014 Langhorne Creek Contributions", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES 2014_McLaren_Vale_Contributions.pdf", title: "2014 McLaren Vale Contributions", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES 2014_Riverland_Contributions.pdf", title: "2014 Riverland Contributions", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES 2014_SA_Grape_Growers_Contributions.pdf", title: "2014 SA Grape Growers Contributions", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES Adelaide_Hills_Refund_Request_Form.pdf", title: "Adelaide Hills Refund Request Form", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES Barossa_Refund_Request_Form.pdf", title: "Barossa Refund Request Form", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES Clare_Refund_Request_Form.pdf", title: "Clare Refund Request Form", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES Langhorne_Creek_Refund_Form.pdf", title: "Langhorne Creek Refund Form", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES McL_Refund_Request_Form.pdf", title: "McL Refund Request Form", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES Riverland_Refund_Request_Form.pdf", title: "Riverland Refund Request Form", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC GRAPES SA_Grape_Growers_Refund_Request_Form.pdf", title: "SA Grape Growers Refund Request Form", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/wine/wineforms/RC USEFUL INFO GRAPES Wine Industry Funds.pdf", title: "Wine Industry Funds", ext: "pdf", page: 1 }                
            ],
        }).Init();

        //Business and Finance
        var BS = new fileWidget({
            IDdvContainer: "#_dvBF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Enterprise Zone Fund.docx", title: "Enterprise Zone Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Guidelines for the Country Cabinet Program.docx", title: "Guidelines for the Country Cabinet Program", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA Regional Development Fund.docx", title: "PIRSA Regional Development Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA-managed Grants.docx", title: "PIRSA-managed Grants", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RDIF_Information_Sessions_-_July_2014.pdf", title: "RDIF_Information_Sessions_-_July_2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Development Australia Funding.docx", title: "Regional Development Australia Funding", ext: "doc", page: 1 },
                //{ path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Food Initiatives Program Recipients 2014.PNG", title: "Regional Food Initiatives Program Recipients 2014", ext: "png", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Riverland Sustainable Futures Fund.docx", title: "Riverland Sustainable Futures Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES.pdf", title: "SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE.pdf", title: "SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SARMS intro.docx", title: "SARMS intro", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RC Farm support and advice.docx", title: "Farm support and advice", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/National Stronger Regions Fund.docx", title: "National Stronger Regions Fund", ext: "doc", page: 1 }
            ],
        }).Init();

        //Forms
        var BS = new fileWidget({
            IDdvContainer: "#_dvF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA Capacity information form (1)  FORMS.doc", title: "PORK SA Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "PORK SA WC Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Medical release authority (1) FORMS.doc", title: "PORK SA WC Medical release authority", ext: "doc", page: 1 }
            ],
        }).Init();

        //Useful Info
        var BS = new fileWidget({
            IDdvContainer: "#_dvUS",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0013/214330/RB201300017.pdf", title: "Understanding dryland farming", ext: "pdf", page: 1 },
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0014/214331/RB201300018.pdf", title: "Understanding mineral exploration", ext: "pdf", page: 1 },
            ],
        }).Init();

        // end:file

        /* end: Custom JSs */
    },
    initLivestock: function () {

        // start:currency

        //currencyExchange.Init();

        // end:currency

        // start:newsRSS

        Index.feeds.push("https://news.google.com/news/feeds?pz=1&cf=all&ned=au&hl=en&q=livestock+%22south+australia%22&output=rss");

        var news = new newsRSS({ feeds: Index.feeds }).Init();

        // end:newsRSS

        // start:fire alerts

       // firealertsWidget.Init();

        // end:fire alerts

        // start:weather alerts

       // weatherWidget.Init();

        // end:weather alerts

        // start:file

        //Education and Training
        /*
        var ET = new fileWidget({
            IDdvContainer: "#_dvET",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news%20archive/Pork%20SA%20Snapshots%20August%202014.pdf", title: "Farm business Skills", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/education%20and%20training/Stepping%20Into%20Leadership-2.pdf", title: "Women in Leadership", ext: "pdf", page: 1 }
            ],
        }).Init();

        //Industrial Relations
        var IR = new fileWidget({
            IDdvContainer: "#_dvIR",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                
            ],
        }).Init();

        //News Archive
        var NA = new fileWidget({
            IDdvContainer: "#_dvNA",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                
            ],
        }).Init();

        //Workcover
        var WC = new fileWidget({
            IDdvContainer: "#_dvWC",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/workcover/WorkCover Levy Rates 2013-14.pdf", title: "WorkCover Levy Rate", ext: "pdf", page: 1 }
            ],
        }).Init();

        //Health & Safety
        var HS = new fileWidget({
            IDdvContainer: "#_dvHS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                {
                    name: "Safe Work Practice",
                    Docs: [
                            { path: "http://www.safework.sa.gov.au/uploaded_files/pmc1i.pdf", title: "Agricultural motorbikes", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/angle_grinder_safety.pdf", title: "Angle grinder safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs602i.pdf", title: "Basic accident investigation", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs38i.pdf", title: "Confined spaces (working in)", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs661i.pdf", title: "Computer workstations design", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf", title: "Disc grinder safety in the shearing industry", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs4i.pdf", title: "Forklift safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs57i.pdf", title: "Ladder safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/GS52-3.pdf", title: "Noise control in the workplace", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/rs8i.pdf", title: "Pesticide safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/remote_safe_staff.pdf", title: "Remote location staff safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/sy1i.pdf", title: "Stockyard safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs34i.pdf", title: "Young worker safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs51i.pdf", title: "Zoonosis", ext: "pdf", page: 1 }
                    ]
                },
            ]
        }).Init();

        //Biosecurity
        var BS = new fileWidget({
            IDdvContainer: "#_dvBS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                {
                    name: "Animal Health",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/MLA GUIDE Is It Fit To Load.pdf", title: "Guide Is It Fit To Load", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Stud stock brands and tattoos.pdf", title: "Stud stock brands and tattoos", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Stud_Stock_Brand_FORM.doc", title: "Stud Stock Brand Form", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC LIVESTOCK BIOSECURITY WEB LINKS.docx", title: "Web links", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/BIOSECURITY Livestock property registration.docx", title: "Livestock property registration", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY CANCELLATION_application FORM.doc", title: "Cancellation application Form", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Cattle_Earmark_FORM.doc", title: "Cattle Earmark Form", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Frequently Asked Questions - HORSES AND PIC.pdf", title: "Frequently Asked Questions", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Frequently Asked Questions on PIC.pdf", title: "Frequently Asked Questions on PIC", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Horse_Cattle_brand_FORM.doc", title: "Horse Cattle Brand Form", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Livestock brand Information.pdf", title: "Livestock brand Information", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Livestock brands.docx", title: "Livestock brands", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY PIC New_Registration_Application_Form_July_2013 WORD.doc", title: "PIC Registration Application Form", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY PIC Transfer Brand FORM.doc", title: "PIC Transfer Brand Form", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Sheep brands and earmarks.pdf", title: "Sheep brands and earmarks", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Sheep_Brand_FORM.doc", title: "Sheep Brand Form", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Sheep_Earmark_FORM.doc", title: "Sheep Earmark Form.doc", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/animalhealth/livestock/RC BIOSECURITY Sheep_earmark_Key_to_the_SystemFORM.doc", title: "Sheep Earmark Form", ext: "doc", page: 1 }
                    ]
                },
                {
                    name: "Biosecurity Chemicals",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY ChemMisuse_brochure_updatedOct29FINAL.pdf", title: "Guidelines for Reporting Agricultural and Veterinary Chemical Misuse", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY PIC New_Registration_Application_Form_July_2013.pdf", title: "REGISTRATION / PIC APPLICATION FORM ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY Reducing harm to Honey Bees from Pesticides.pdf", title: "Reducing Harm to Honey Bees from Pesticides ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC Biosecurity Rural chemicals.docx", title: "Rural chemicals", ext: "doc", page: 1 }

                    ]
                },
                {
                    name: "Bushfire Planning",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producer1.docx", title: "Bushfire information for primary producers", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producers.docx", title: "Bushfire information for primary produce", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire_Prevention_and_Preparedness_S2.pdf", title: "Bushfire Prevention and Preparedness", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFA BUSHFIRE - PROTECTION OF FODDER RESERVES.pdf", title: "Protection of Fodder Reserves", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRE FACT SHEET 303_EMERGENCY KITS.pdf", title: "Emergency Kits", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf", title: "Care of Pets and Livestock", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES AND HORSES.pdf", title: "Horses and Bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS FACT SHEET - AFTER THE FIRE.pdf", title: "After the Fire", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CHECKLIST - Farming Guidelines for Reducing Bushfires.PNG", title: "Farming Guidelines for Reducing Bushfires", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming guidelines for reduction of bushfire - SUMMARY.docx", title: "Farming guidelines for reduction of bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming_Guidelines_for_the_Reduction_of_Bushfire_Risk.pdf", title: "Farming Guidelines for the Reduction of Bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/GUIDE to_bushfire_safety_2014.pdf", title: "Your guide to bushfire safety", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Livestock_Safety_in_Bushfires_Sept2012_version.pdf", title: "LIVESTOCK SAFETY DURING BUSHFIRES", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Recovering after bushfires.docx", title: "Recovering after bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/STOCK OWNERS Risk-management-in-times-of-fire-and-flood.pdf", title: "Risk management for stock owners in times of fire and flood", ext: "pdf", page: 1 },

                    ]
                },
                {
                    name: "Emergency Checklists",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG", title: "Biosecurity Emergency hotlines", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG", title: "HEATWAVES RISK", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC BIOSECURITY - EMERGENCY MANAGMENT  LIVE LINK.docx", title: "EMERGENCY MANAGMENT  LIVE LINK", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf", title: "Disaster Planning - Bushfire Checklis", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf", title: "Disaster Planning - Earthquake Action Plan", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf", title: "Disaster Planning - Emergency Contacts", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf", title: "Disaster Planning - Extreme Heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf", title: "Disaster Planning - Flood Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Home Fire Safety Checklist.pdf", title: "Disaster Planning - Home Fire Safety Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf", title: "Personal Medical Details", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf", title: "Extreme heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf", title: "Extreme Heat Guide", ext: "pdf", page: 1 },

                    ]
                }                
            ]
        }).Init();

        //Natural Resource Management
        var BS = new fileWidget({
            IDdvContainer: "#_dvNRM",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                 {
                     name: "Water and Land",
                     Docs: [
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC LAND MANAGEMENT.docx", title: "LAND MANAGEMENT", ext: "doc", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC WATER MANAGEMENT.docx", title: "WATER MANAGEMENT", ext: "doc", page: 1 },
                     ]
                 }
            ]
        }).Init();

        //Forms and Payments
        var BS = new fileWidget({
            IDdvContainer: "#_dvFP",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/livestock/forms/RC BIOSECURITY Cattle_Earmark_FORM.doc", title: "Cattle Earmark Form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/livestock/forms/RC BIOSECURITY PIC Transfer Brand FORM.doc", title: "PIC Transfer Brand Form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/livestock/forms/RC BIOSECURITY Sheep_Brand_FORM.doc", title: "Sheep Brand Form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/livestock/forms/RC BIOSECURITY Sheep_Earmark_FORM.doc", title: "Sheep Earmark Form", ext: "doc", page: 1 }
            ],
        }).Init();

        //Business and Finance
        var BS = new fileWidget({
            IDdvContainer: "#_dvBF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Enterprise Zone Fund.docx", title: "Enterprise Zone Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Guidelines for the Country Cabinet Program.docx", title: "Guidelines for the Country Cabinet Program", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA Regional Development Fund.docx", title: "PIRSA Regional Development Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA-managed Grants.docx", title: "PIRSA-managed Grants", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RDIF_Information_Sessions_-_July_2014.pdf", title: "RDIF_Information_Sessions_-_July_2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Development Australia Funding.docx", title: "Regional Development Australia Funding", ext: "doc", page: 1 },
                //{ path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Food Initiatives Program Recipients 2014.PNG", title: "Regional Food Initiatives Program Recipients 2014", ext: "png", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Riverland Sustainable Futures Fund.docx", title: "Riverland Sustainable Futures Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES.pdf", title: "SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE.pdf", title: "SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SARMS intro.docx", title: "SARMS intro", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RC Farm support and advice.docx", title: "Farm support and advice", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/National Stronger Regions Fund.docx", title: "National Stronger Regions Fund", ext: "doc", page: 1 }
            ],
        }).Init();        

        //Forms
        var BS = new fileWidget({
            IDdvContainer: "#_dvF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA Capacity information form (1)  FORMS.doc", title: "PORK SA Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "PORK SA WC Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Medical release authority (1) FORMS.doc", title: "PORK SA WC Medical release authority", ext: "doc", page: 1 }
            ],
        }).Init();

        //Useful Info
        var BS = new fileWidget({
            IDdvContainer: "#_dvUS",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0013/214330/RB201300017.pdf", title: "Understanding dryland farming", ext: "pdf", page: 1 },
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0014/214331/RB201300018.pdf", title: "Understanding mineral exploration", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/livestock/useful/SARDI Lamb doc 1.pdf", title: "Management strategies to improve lamb weaning percentages", ext: "pdf", page: 1 },
            ],
        }).Init();

        // end:file

        /* end: Custom JSs */
    },
    initHorticulture: function () {

        // start:currency

        //currencyExchange.Init();

        // end:currency

        // start:newsRSS

        Index.feeds.push("https://news.google.com/news/feeds?pz=1&cf=all&ned=au&hl=en&q=Horticulture+%22south+australia%22&output=rss");

        var news = new newsRSS({ feeds: Index.feeds }).Init();

        // end:newsRSS

        // start:fire alerts

        //firealertsWidget.Init();

        // end:fire alerts

        // start:weather alerts

        //weatherWidget.Init();

        // end:weather alerts

        // start:file

        //Education and Training
        /*var ET = new fileWidget({
            IDdvContainer: "#_dvET",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/news%20archive/Pork%20SA%20Snapshots%20August%202014.pdf", title: "Farm business Skills", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/education%20and%20training/Stepping%20Into%20Leadership-2.pdf", title: "Women in Leadership", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/education%20and%20training/hort EDUCATION AND TRAINING.docx", title: "Education and training", ext: "doc", page: 1 }
            ],
        }).Init();

        //Industrial Relations
        var IR = new fileWidget({
            IDdvContainer: "#_dvIR",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                
            ],
        }).Init();

        //News Archive
        var NA = new fileWidget({
            IDdvContainer: "#_dvNA",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                
            ],
        }).Init();

        //Workcover
        var WC = new fileWidget({
            IDdvContainer: "#_dvWC",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/workcover/WorkCover Levy Rates 2013-14.pdf", title: "WorkCover Levy Rate", ext: "pdf", page: 1 }
            ],
        }).Init();

        //Health & Safety
        var HS = new fileWidget({
            IDdvContainer: "#_dvHS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping:true,
            Groups: [                
                {
                    name: "Safe Work Practice",
                    Docs: [
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs602i.pdf", title: "Basic accident investigation", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs38i.pdf", title: "Confined spaces (working in)", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs661i.pdf", title: "Computer workstations design", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/GS52-3.pdf", title: "Noise control in the workplace", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/remote_safe_staff.pdf", title: "Remote location staff safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/sy1i.pdf", title: "Stockyard safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs34i.pdf", title: "Young worker safety", ext: "pdf", page: 1 },
                            { path: "http://www.safework.sa.gov.au/uploaded_files/gs51i.pdf", title: "Zoonosis", ext: "pdf", page: 1 }
                    ]
                },
                 {
                     name: "Chemicals",
                     Docs: [
                          { path: "http://www.safework.sa.gov.au/uploaded_files/rs8i.pdf", title: "Pesticide safety", ext: "pdf", page: 1 }
                     ]
                 },
                  {
                      name: "Machinery and Tools",
                      Docs: [
                          { path: "http://www.safework.sa.gov.au/uploaded_files/pmc1i.pdf", title: "Agricultural motorbikes", ext: "pdf", page: 1 },
                          { path: "http://www.safework.sa.gov.au/uploaded_files/angle_grinder_safety.pdf", title: "Angle grinder safety", ext: "pdf", page: 1 },
                          { path: "http://www.safework.sa.gov.au/uploaded_files/disc_grinder_safety.pdf", title: "Disc grinder safety in the shearing industry", ext: "pdf", page: 1 },
                          { path: "http://www.safework.sa.gov.au/uploaded_files/gs4i.pdf", title: "Forklift safety", ext: "pdf", page: 1 },
                          { path: "http://www.safework.sa.gov.au/uploaded_files/gs57i.pdf", title: "Ladder safety", ext: "pdf", page: 1 }
                      ]
                  },
            ]
        }).Init();

       //Biosecurity
        var BS = new fileWidget({
            IDdvContainer: "#_dvBS",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                {
                    name: "Plant Health - Pests",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Australian_Plague_Locust.pdf", title: "The Australian Plague Locust ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Glassy_winged_sharpshooter_-_FS.pdf", title: "Glassy-winged sharpshooter", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Myrtle_Rust_-_awarness_sheet.pdf", title: "Exotic Plant Pest Alert", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest Alert - European House Borer fact_sheet_August_2011.pdf", title: "European House Borer", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest Alert - GreenSnail-Cantareusapertus.pdf", title: "Pest Alert - GreenSnail-Cantareusapertus", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest Alert Fire Ant PaDIL-Redimportedfireant-Solenopsisinvicta.pdf", title: "Pest Alert Fire Ant PaDIL-Redimportedfireant-Solenopsisinvicta", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Citrus_Canker.pdf", title: "Pest Alert-Citrus Canker", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Citrus_Greening.pdf", title: "Pest Alert-Citrus_Greening", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Citrus_variegated_chlorosis_CVC.pdf", title: "Pest Alert-Citrus variegated chlorosis CVC", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Melon_necrotic_spot_virus.pdf", title: "Pest Alert-Melon necrotic spot virus", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Pepper_Chat_Fruit_Viroid_PCFVd.pdf", title: "Pest Alert-Pepper Chat Fruit Viroid PCFVd", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Potato_Spindle_Tuber_Viroid.pdf", title: "Pest Alert-Potato Spindle Tuber Viroid", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_The_Tomato_Potato_Psyllid_TPP.pdf", title: "Pest Alert-TheTomato Potato Psyllid TPP", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Tomato_Red_Spider_Mite.pdf", title: "Pest Alert -Tomato Red Spider Mite", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Pest_Alert_-_Zebra_Chip.pdf", title: "Pest Alert-Zebra Chip", ext: "pdf", page: 1 }                            
                    ]
                },
                {
                    name: "Plant Health - Documents",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/CA-01-_IVCA_Operational_Procedure_Version_5_2-fixed.pdf", title: "IMPORT VERIFICATION COMPLIANCE ARRANGEMENT ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Guidelines for completing Plant Health Certificates.pdf", title: "GUIDELINES FOR THE COMPLETION OF PLANT HEALTH ASSURANCE CERTIFICATES ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/ICA_LEAFLET.pdf", title: "ICA LEAFLET", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/INFO_to_ICA_APPLICANTS.pdf", title: "INTERSTATE CERTIFICATION ASSURANCE (ICA) ARRANGEMENTS INFORMATION FOR APPLICANTS ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Myrtle Rust Management Plan 2012 Final V2.pdf", title: "Australian  Nursery Industry Myrtle Rust - Management Plan 2012 ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/PIC Numbers FAQ.pdf", title: "PIC Numbers FAQ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Plant Health Assurance Certificate with_CAA_06.pdf", title: "Plant Health Assurance Certificate with CAA 06", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Plant Health Certif Request_Form.pdf", title: "Plant Health Certif Request Form", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/RC BIOSECURITY Plant Health Export links.docx", title: "Export links", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/planthealth/Weed Control On_Property_Management_of_Branched_Broomrape_v2.pdf", title: "Weed Control On Property Management of Branched Broomrape v2", ext: "pdf", page: 1 },
                    ]
                },
                {
                    name: "Biosecurity Chemicals",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY ChemMisuse_brochure_updatedOct29FINAL.pdf", title: "Guidelines for Reporting Agricultural and Veterinary Chemical Misuse", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY PIC New_Registration_Application_Form_July_2013.pdf", title: "REGISTRATION / PIC APPLICATION FORM ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC BIOSECURITY Reducing harm to Honey Bees from Pesticides.pdf", title: "Reducing Harm to Honey Bees from Pesticides ", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/chemicals/RC Biosecurity Rural chemicals.docx", title: "Rural chemicals", ext: "doc", page: 1 }

                    ]
                },
                {
                    name: "Bushfire Planning",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producer1.docx", title: "Bushfire information for primary producers", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire information for primary producers.docx", title: "Bushfire information for primary produce", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Bushfire_Prevention_and_Preparedness_S2.pdf", title: "Bushfire Prevention and Preparedness", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFA BUSHFIRE - PROTECTION OF FODDER RESERVES.pdf", title: "Protection of Fodder Reserves", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRE FACT SHEET 303_EMERGENCY KITS.pdf", title: "Emergency Kits", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES - CARE OF PETS AND LIVESTOCK.pdf", title: "Care of Pets and Livestock", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS BUSHFIRES AND HORSES.pdf", title: "Horses and Bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CFS FACT SHEET - AFTER THE FIRE.pdf", title: "After the Fire", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/CHECKLIST - Farming Guidelines for Reducing Bushfires.PNG", title: "Farming Guidelines for Reducing Bushfires", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming guidelines for reduction of bushfire - SUMMARY.docx", title: "Farming guidelines for reduction of bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Farming_Guidelines_for_the_Reduction_of_Bushfire_Risk.pdf", title: "Farming Guidelines for the Reduction of Bushfire", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/GUIDE to_bushfire_safety_2014.pdf", title: "Your guide to bushfire safety", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Livestock_Safety_in_Bushfires_Sept2012_version.pdf", title: "LIVESTOCK SAFETY DURING BUSHFIRES", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/Recovering after bushfires.docx", title: "Recovering after bushfires", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/bushfire/STOCK OWNERS Risk-management-in-times-of-fire-and-flood.pdf", title: "Risk management for stock owners in times of fire and flood", ext: "pdf", page: 1 },

                    ]
                },
                {
                    name: "Emergency Checklists",
                    Docs: [
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CHECKLIST - HEATWAVES RISK.PNG", title: "Biosecurity Emergency hotlines", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/CONTACTS - Biosecurity Emergency hotlines.PNG", title: "HEATWAVES RISK", ext: "png", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC BIOSECURITY - EMERGENCY MANAGMENT  LIVE LINK.docx", title: "EMERGENCY MANAGMENT  LIVE LINK", ext: "doc", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Bushfire Checklist.pdf", title: "Disaster Planning - Bushfire Checklis", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Earthquake Action Plan.pdf", title: "Disaster Planning - Earthquake Action Plan", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Emergency Contacts.pdf", title: "Disaster Planning - Emergency Contacts", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Extreme Heat checklist.pdf", title: "Disaster Planning - Extreme Heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Flood Checklist.pdf", title: "Disaster Planning - Flood Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Home Fire Safety Checklist.pdf", title: "Disaster Planning - Home Fire Safety Checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Disaster Planning - Personal Medical Details.pdf", title: "Personal Medical Details", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme heat checklist.pdf", title: "Extreme heat checklist", ext: "pdf", page: 1 },
                            { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/emergency/RC Useful - Extreme Heat Guide.pdf", title: "Extreme Heat Guide", ext: "pdf", page: 1 },

                    ]
                },                 
                 {
                     name: "Fruit Fly",
                     Docs: [
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/fruitfly/Fruit Fly Fact_Sheet.pdf", title: "Fruit Fly Fact Sheet", ext: "pdf", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/fruitfly/Fruit Fly intro - prevention, detection, eradication.docx", title: "Fruit Fly intro - prevention, detection, eradication ", ext: "pdf", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/fruitfly/Fruit_fly_detection.pdf", title: "Fruit Fly detection ", ext: "pdf", page: 1 }
                     ]
                 }
            ]
        }).Init();

        //Natural Resource Management
        var BS = new fileWidget({
            IDdvContainer: "#_dvNRM",
            iconWidth: "40px",
            iconHeight: "40px",
            grouping: true,
            Groups: [
                 {
                     name: "Water and Land",
                     Docs: [
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC LAND MANAGEMENT.docx", title: "LAND MANAGEMENT", ext: "doc", page: 1 },
                             { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/common/biosecurity/waterandland/RC WATER MANAGEMENT.docx", title: "WATER MANAGEMENT", ext: "doc", page: 1 },
                     ]
                 }
            ]
        }).Init();

        //Forms and Payments
        var BS = new fileWidget({
            IDdvContainer: "#_dvFP",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/RDIF_Application_form_amended_16_July.docx", title: "Regional Development and Innovation Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA Capacity information form (1)  FORMS.doc", title: "Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/formsandpayments/PORK SA WC Medical release authority (1) FORMS.doc", title: "Medical release authority", ext: "doc", page: 1 },
            ],
        }).Init();

        //Business and Finance
        var BS = new fileWidget({
            IDdvContainer: "#_dvBF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Enterprise Zone Fund.docx", title: "Enterprise Zone Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Guidelines for the Country Cabinet Program.docx", title: "Guidelines for the Country Cabinet Program", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA Regional Development Fund.docx", title: "PIRSA Regional Development Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/PIRSA-managed Grants.docx", title: "PIRSA-managed Grants", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RDIF_Information_Sessions_-_July_2014.pdf", title: "RDIF_Information_Sessions_-_July_2014", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Development Australia Funding.docx", title: "Regional Development Australia Funding", ext: "doc", page: 1 },
                //{ path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Regional Food Initiatives Program Recipients 2014.PNG", title: "Regional Food Initiatives Program Recipients 2014", ext: "png", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/Riverland Sustainable Futures Fund.docx", title: "Riverland Sustainable Futures Fund", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES.pdf", title: "SA River Murray Sustainability Reg Dev and Innovation Fund GRANT FUNDING GUIDELINES", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE.pdf", title: "SA River Murray Sustainabioity Regional Dev and Innovation Fund GUIDE", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/SARMS intro.docx", title: "SARMS intro", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/RC Farm support and advice.docx", title: "Farm support and advice", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/businessandfinance/National Stronger Regions Fund.docx", title: "National Stronger Regions Fund", ext: "doc", page: 1 }
            ],
        }).Init();

        //Forms
        var BS = new fileWidget({
            IDdvContainer: "#_dvF",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA Capacity information form (1)  FORMS.doc", title: "PORK SA Capacity information form", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Letter to Doctor (1)  FORMS.doc", title: "PORK SA WC Letter to Doctor", ext: "doc", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/pork/forms/PORK SA WC Medical release authority (1) FORMS.doc", title: "PORK SA WC Medical release authority", ext: "doc", page: 1 }
            ],
        }).Init();

        //Useful Info
        var BS = new fileWidget({
            IDdvContainer: "#_dvUS",
            iconWidth: "40px",
            iconHeight: "40px",
            Docs: [
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0013/214330/RB201300017.pdf", title: "Understanding dryland farming", ext: "pdf", page: 1 },
                { path: "http://www.pir.sa.gov.au/__data/assets/pdf_file/0014/214331/RB201300018.pdf", title: "Understanding mineral exploration", ext: "pdf", page: 1 },
                { path: "http://sigrif.com/demo/rop/dmsq/rule/rule/docs/grain/RC USEFUL INFO - GRAINS REPORTS PIRSA_Crop_and_Pasture_Report_Final_Sept_2014.pdf", title: "Crop_and_Pasture_Report_Final_Sept_2014", ext: "pdf", page: 1 },
            ],
        }).Init();

        // end:file
		
		// iframeLinks
        
        //Caoallition
        var links = [
            { title:"Adelaide Produce Market Ltd", href:"#" },
            { title:"Almond Board of Australia Ltd", href:"#" },
            { title:"Apple and Pear Growers Association Inc", href:"#" },
            { title:"AUSVEG SA", href:"#" },
            { title:"Citrus Australia (SA Region)", href:"#" },
            { title:"Hortex Alliance Inc", href:"#" },
            { title:"Australian Mushroom Growers Association", href:"#" },
            { title:"Nursery and Garden Industry�of SA Inc", href:"#" },
            { title:"Olives South Australia Inc", href:"#" },
            { title:"Onions Australia", href:"#" },
            { title:"Pistachio Growers' Association Inc", href:"#" },
            { title:"South Australian Chamber of Fruit and Vegetable Industries", href:"#" },
            { title:"Women in Horticulture", href:"#" },
        ];
        var iL = new iframePopup({
            IDdvContainer: "#_dvLiveLinks",
            title: "<h3>Live website links and contact details:</h3>",
            links : links
        }).init();

        //Education and Training
        var links = [
			{ title: "University of South Australia", href:"http://www.unisa.edu.au/" },
            { title: "Adelaide University", href:"http://www.adelaide.edu.au/scholarships/undergrad/isolated/" },
            { title: "Flinders University", href:"http://www.flinders.edu.au/medicine/sites/nt-clinical-school/students/scholarships.cfm" },            
            { title: "TAFE SA2", href:"http://www.tafesa.edu.au/apply-enrol/before-starting/scholarships-grants" },            
            { title: "Country Health", href:"http://www.countryhealthsa.sa.gov.au/Scholarships.aspx" },            
            { title: "SA Government", href:"https://www.sa.gov.au/topics/education-skills-and-learning/financial-help-scholarships-and-grants/scholarships" }
        ];
        var iL = new iframePopup({
            IDdvContainer: "#_dvLinksET",
            title: "<h4>LINKS TO AGRICULTURAL FURTHER EDUCATION COURSES</h4>",
            links: links
        }).init();

        // end: iframeLinks

        /* end: Custom JSs */
    },
    initPartners: function () {

        // start:currency

        //currencyExchange.Init();

        var videos = [
                { source: "https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/videos/PrimeSuperVideo1_compressed.mp4", title: "<h3> Super Thoughts - Age 50 Retirement Planning</h3>", disclaimer: "The information contained in this presentation is of a general nature only and has been prepared by Prime Super Pty Ltd (ABN 81 067 241 016 AFSL no. 219723) without taking into <br>account your personal financial situation or needs. Prime Super Pty Ltd (Trustee) is not licensed to provide personal financial product advice to you and we recommend you seek <br>financial advice tailored to your personal circumstances and read our Short-Form PDS in full before deciding whether an investment in the Fund is appropriate for you.<br>Prime Super (ABN 60 562 335 823) is a regulated superannuation fund issued by Prime Super Pty Ltd. <br>A Short-Form Product Disclosure Statement (PDS) may be obtained by calling 1800 675 839 or online at www.primesuper.com.au and should be considered before making any <br>decision. In delivering this presentation to you, Prime Super is not required to provide you with a Financial Services Guide (FSG) based on Regulation 7.1.33H of the Corporations <br>Regulations 2001 (Cth). However, if you would like a copy of  the Joint FSG with our administrator, Link Super, a copy may be obtained from me, or by calling 1800 675 839 or <br>online at www.primesuper.com.au. <br>The information in this presentation is correct as at 22 September 2014. Please refer to the ATO website at www.ato.gov.au for further taxation information." },
                { source: "https://s3.amazonaws.com/rural-resources.ruralconnect.com.au/videos/PrimeSuperVideo2_compressed+(1).mp4", title: "<h3> Super Thoughts - Age 55 Boost my Super </h3>", disclaimer: "The information contained in this presentation is of a general nature only and has been prepared by Prime Super Pty Ltd (ABN 81 067 241 016 AFSL no. 219723) without taking into <br>account your personal financial situation or needs. Prime Super Pty Ltd (Trustee) is not licensed to provide personal financial product advice to you and we recommend you seek <br>financial advice tailored to your personal circumstances and read our Short-Form PDS in full before deciding whether an investment in the Fund is appropriate for you.<br>Prime Super (ABN 60 562 335 823) is a regulated superannuation fund issued by Prime Super Pty Ltd. <br>A Short-Form Product Disclosure Statement (PDS) may be obtained by calling 1800 675 839 or online at www.primesuper.com.au and should be considered before making any <br>decision. In delivering this presentation to you, Prime Super is not required to provide you with a Financial Services Guide (FSG) based on Regulation 7.1.33H of the Corporations <br>Regulations 2001 (Cth). However, if you would like a copy of  the Joint FSG with our administrator, Link Super, a copy may be obtained from me, or by calling 1800 675 839 or <br>online at www.primesuper.com.au. <br>The information in this presentation is correct as at 22 September 2014. Please refer to the ATO website at www.ato.gov.au for further taxation information." },
                { source: "http://rural-resources.ruralconnect.com.au/videos/PrimeSuperVideo3_compressed.mp4", title: "<h3> Super Thoughts - Age 60 10% Tax Free </h3>", disclaimer: "The information contained in this presentation is of a general nature only and has been prepared by Prime Super Pty Ltd (ABN 81 067 241 016 AFSL no. 219723) without taking into <br>account your personal financial situation or needs. Prime Super Pty Ltd (Trustee) is not licensed to provide personal financial product advice to you and we recommend you seek <br>financial advice tailored to your personal circumstances and read our Short-Form PDS in full before deciding whether an investment in the Fund is appropriate for you.<br>Prime Super (ABN 60 562 335 823) is a regulated superannuation fund issued by Prime Super Pty Ltd. <br>A Short-Form Product Disclosure Statement (PDS) may be obtained by calling 1800 675 839 or online at www.primesuper.com.au and should be considered before making any <br>decision. In delivering this presentation to you, Prime Super is not required to provide you with a Financial Services Guide (FSG) based on Regulation 7.1.33H of the Corporations <br>Regulations 2001 (Cth). However, if you would like a copy of  the Joint FSG with our administrator, Link Super, a copy may be obtained from me, or by calling 1800 675 839 or <br>online at www.primesuper.com.au. <br>The information in this presentation is correct as at 22 September 2014. Please refer to the ATO website at www.ato.gov.au for further taxation information." }
        ];

        var video = new videoWidget({ videos: videos, listcontainer: "#dvListContainer", dvplayer: "#dvVideoPlayer", disclaimercontainer: "#dvDisclaimer", autoplay:true }).Init();
        
        /* end: Custom JSs */
    },
    initMomentumPartners: function () {

        // start:currency

        //currencyExchange.Init();

        var videos = [
                { source: "http://rural-resources.ruralconnect.com.au/Momentum+Energy+-+energising+the+Adelaide+Crows.mp4", title: "<h3> Proud to partner with the crows. Supporting South Australians.</h3>", disclaimer: "Momentum's goal is to offer you as much value as possible, not only via our PPSA Momentum Energy special, but also through our energy efficiency solutions. To achieve this, Momentum will be spending the next two years working with each of the PPSA member organisations. Looking after our country is a priority here at Momentum. After all, our parent company, Hydro Tasmania has been generating clean energy for over 100 years."},
      	       
                ];
        var video = new videoWidget({ videos: videos, listcontainer: "#dvListContainer", dvplayer: "#dvVideoPlayer", disclaimercontainer: "#dvDisclaimer", autoplay:true }).Init();
    }
              
}