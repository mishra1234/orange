

        // Logic when it loads




        // Helpers

        function pork(){
            $("#porkOption").addClass("active");
            $("#mainContent").load("pork.html");

        }

        function myAccount(){
           // $("#porkOption").addClass("active");
            $("#mainContent").load("member.html");

        }

        function partner(){
            // $("#porkOption").addClass("active");
            $("#mainContent").load("partner.html");

        }

        function startConnecting(){
            $("#mainContent").load("login.html");

        }

        function learnMore(){
            $("#mainContent").load("learnMore.html");

        }

        // PORK

        function porkViewAll(){

            $('.pork').removeClass("active");           // remove all active tabs
            $('#viewAllPorkTab').addClass("active");    // Add Active to this tab only

            $("#porkContent .row").show();              // Show all rows under porkContent DIV id
        }

       function porkCommittee()
       {
           $('.pork').removeClass("active");
           $('#porkCommitteeTab').addClass("active");
           $("#porkContent .row").hide();
           $("#porkContent .row .pork").show();
           $("#porkCommittee .row").show();

       }

        function aboutPork()
        {
            $('.pork').removeClass("active");
            $('#aboutPorkTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkAbout").show();

        }
        function porkForm()
        {
            $('.pork').removeClass("active");
            $('#porkFormTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkForm").show();

        }

        function porkMembership()
        {
            $('.pork').removeClass("active");
            $('#porkMembershipTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkMembership").show();

        }
        
        
        function porkBioSecurity()
        {
            $('.pork').removeClass("active");
            $('#porkBioSecurityTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkBioSecurity").show();

        }
        

        function porkComm()
        {
        	console.log("communication");
            $('.pork').removeClass("active");
            $('#porkCommTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkComm").show();

        }

        function porkEvents()
        {
            $('.pork').removeClass("active");
            $('#porkEventsTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkEvents").show();
            $("#porkEvents .row").show();

        }

        function porkBusiness()
        {
            $('.pork').removeClass("active");
            $('#porkBusinesTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkBusiness").show();

        }

        function porkSurvey()
        {
            $('.pork').removeClass("active");
            $('#porkSurveyTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkSurvey").show();

        }

        function porkKeyContacts()
        {
            $('.pork').removeClass("active");
            $('#porkKeyTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkKeyContact").show();

        }

        function porkLegal()
        {
            $('.pork').removeClass("active");
            $('#porkLegalTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkLegal").show();

        }
        
        function grainTraining()
        {
            $('.pork').removeClass("active");
            $('#porkOtherTab').addClass("active");
            $("#porkContent .row").hide();
            $("#grainTraining").show();

        }
        
        function grainIndustrail()
        {
            $('.pork').removeClass("active");
            $('#grainIndustrailTab').addClass("active");
            $("#porkContent .row").hide();
            $("#grainIndustrailContent").show();

        }
        
        function grainWork()
        {
            $('.pork').removeClass("active");
            $('#grainWorkTab').addClass("active");
            $("#porkContent .row").hide();
            $("#grainWorkContent").show();

        }
        
        function porkInfos()
        {
            $('.pork').removeClass("active");
            $('#porkInfosTab').addClass("active");
            $("#porkContent .row").hide();
            $("#porkInfos").show();
            $("#porkInfos .row").show();

        }

