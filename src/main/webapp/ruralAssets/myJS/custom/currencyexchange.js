var currencyExchange = {

    IDdvSource: "#dvCurrencySource",
    IDdvContainer: "#dvCurrencyContainer",
    IDspnDollarExchange: "#spnExchangeDollar",

    DollarExchange:null,

    Init: function () {
        currencyExchange.ReadAdvanced();
        //currencyExchange.ReadSmall({withIcon:false});
    },
    ReadAdvanced: function () {

        var $html = $(currencyExchange.IDdvSource);
        var $table = $html.find("table[class='WXNtopmenu2']");
        var $tbody = $table.find("tbody");
        var $trs = $table.find("tr");
        var $cabecera = $($trs[0]);
        var $fecha = $($trs[$trs.length - 1]);
        var Html = "";
        var img, exch, curr;
        var $aux;
        var $tds;

        Html += "<div class='widget-advanced'>";
        
        Html += "<div class='widget-header text-center themed-background-dark'>";
        //Html += "<div class='widget-icon pull-left themed-background animation-fadeIn'><i class='fa fa-user'></i></div>";
        Html += "<h3 class='widget-content-light clearfix'><small>Australian Dollar Exchange Rate</small><br>" + $($fecha).html() + "</h3>";
        Html += "</div>";

        Html += "<div class='widget-main'>";

        Html += "<a href='http://fx-rate.net/AUD/' class='widget-image-container'>";
        Html += "<span class='widget-icon themed-background'>";
        Html += "&nbsp;<i class='fa fa-money'></i>&nbsp;";
        Html += "</span>";
        Html += "</a>";

        Html += "<div class='row text-center'>"
        Html += "<div class='col-xs-12 col-lg-12 text-center'>"
        Html += "<h3><small><strong>" + $($trs[0]).html() + "</strong></small></h3>";
        Html += "</div>"
        Html += "<div class='row text-center'>"
        /*Html += "<div class='col-xs-6 col-lg-3'>";
        Html += "<i class='fa fa-user fa-lg'></i>&nbsp;";
        Html += "</div>"*/

        for (var i = 1; i < $trs.length - 1; i++) {
            $aux = $($trs[i]);
            img = $aux.find("img")[0];
            $tds = $aux.find("td");
            exch = $($tds[0]).text();
            curr = $($tds[1]).text();
            currencyExchange.DollarExchange = exch == "   USD" ? curr : "";
            Html += "<div class='col-xs-10 col-lg-4 text-center'>";
            Html += "<h3><center><img width='20px' src='" + $(img).attr("src") + "'></center><sup><small>" + exch + "</small></sup><small><strong>" + curr + "</strong></small></h3>";
            Html += "</div>"

        }
        
        Html += "</div>";
        Html += "</div>";

        var $test = $($trs[1]);

        $(currencyExchange.IDdvContainer).html(Html);
        $(currencyExchange.IDspnDollarExchange).text(currencyExchange.DollarExchange);
        console.log("USD" + currencyExchange.DollarExchange);
    },
    ReadSmall: function (args) {

        var $html = $(currencyExchange.IDdvSource);
        var $table = $html.find("table[class='WXNtopmenu2']");
        var $tbody = $table.find("tbody");
        var $trs = $table.find("tr");
        var $cabecera = $($trs[0]);
        var $fecha = $($trs[$trs.length - 1]);
        var Html = "";
        var img, exch, curr;
        var $aux;
        var $tds;
        var withIcon = args.withIcon == undefined ? false : args.withIcon;

        if (withIcon) {
            Html += "<div class='widget-icon pull-left themed-background animation-fadeIn'><i class='fa fa-money'></i></div>";
            Html += "<div class='row text-center'>"
            Html += "<br><strong>Australian Dollar <br>Exchange Rate</strong><br>" + $($fecha).html() + "<br><strong>" + $($cabecera).html() + "</strong>";
        } else {
            Html += "<div class='row text-center'>"
            Html += "<br><strong>Australian Dollar Exchange Rate</strong><br>" + $($fecha).html() + "<br><strong>" + $($cabecera).html() + "</strong>";
        }        
        Html += "</div>";

        Html += "<div class='row text-center'>"
        
        for (var i = 1; i < $trs.length - 1; i++) {
            $aux = $($trs[i]);
            img = $aux.find("img")[0];
            $tds = $aux.find("td");
            exch = $($tds[0]).text().replace(/\s/g, "");
            curr = $($tds[1]).text();
            currencyExchange.DollarExchange = exch == "USD" ? curr : "";
            Html += "<div class='col-xs-10 col-lg-4 text-center'>";
            Html += "<h3><center><img src='" + $(img).attr("src") + "'></center><sup><small>" + exch + "</small></sup><small><strong>" + curr + "</strong></small></h3>";
            Html += "</div>"

        }

        Html += "</div>";

        var $test = $($trs[1]);

        $(currencyExchange.IDdvContainer).html(Html);
        $(currencyExchange.IDspnDollarExchange).text(currencyExchange.DollarExchange);
    }    

}