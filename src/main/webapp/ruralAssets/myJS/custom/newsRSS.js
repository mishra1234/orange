

function newsRSS(args){

    this.IDdvContainer = args.IDdvContainer==undefined?"#dvNewsRSS":args.IDdvContainer;

    this.xmls = [];
    this.feeds = args.feeds;

    this.Init = function () {

        var that = this;
        var feeds = that.feeds;

        if (feeds == undefined) {
            bootbox.dialog({
                title: "Feeds were not found",
                message: "The feeds source was not defined.",
                buttons: {
                    "Close": function () { }
                }
            });
            return false;
        }

        for (var i = 0; i < feeds.length; i++) {
            
            feed = new google.feeds.Feed(feeds[i]);

            feed.setNumEntries(2)

            feed.load(function (result) {

                if (!result.error) {
                    //newsRSS.refreshFeeds(result.feed.entries);

                    that.combineandShow(result.feed.entries);

                    //container.innerHTML = html;
                }
            });

        }

        
    },

    this.combineandShow = function(xml){

        var combined;
        var that = this;

        combined = that.xmls.concat(xml);

        that.xmls = combined;

        that.refreshFeeds(combined);
    };

    this.refreshFeeds = function (entries) {

        var that = this;
        var entry, date2, date3, date4;
        var foto = "";
        var html = "<ul class='timeline-list'>";

        for (var i = 0; i < entries.length; i++) {

            entry = entries[i];
            date2 = entry.publishedDate;
            date3 = new Date(date2);
            date4 = date3.getDate() + "-" + (date3.getMonth()+1) + "-" + date3.getFullYear();

            foto = "";

            try { foto = entry.mediaGroups[0].contents[0].url; } catch (e) { }

            html += "<li class='active'>";
            html += "<div class='timeline-icon'>";
            html += "<i class='fa fa-file-text'></i>";
            html += "</div>";
            html += '<div class="timeline-time"><small>' + date4 + '</small></div>';
            html += '<div class="timeline-content">';
            html += '<p class="push-bit"><a href="' + entry.link + '" target="_blank"><strong>' + entry.title + '</strong></a></p>';
            html += '<p class="push-bit">' + entry.contentSnippet + '</p>';
            html += '<p class="push-bit"><a target="#" href="' + entry.link + '" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-file"></i> Read the article</a></p>';
            if (foto != "") {
                html += '<div class="row push">';
                html += '<div class="col-sm-6 col-md-4">';
                html += '<a target="#" href="' + foto + '" data-toggle="lightbox-image">';
                html += '<img src="' + foto + '" alt="image">';
                html += '</a>';
                html += '</div>';
                html += '</div>';
            }
            html += '</div>';

            html += "</li>";            
            
        }

        html += '<li class="text-center">';
        html += '<a href="javascript:void(0)" class="btn btn-xs btn-default">View more..</a>';
        html += '</li>';

        html = html + "</ul>"
        $(that.IDdvContainer).html(html);

    }


}



