var gatedMemberWidget = {

    logos: [],
    container: "#dvGatedMember",

    Init: function (args) {

        gatedMemberWidget.logos = gatedMemberWidget.suffleArray(args.logos);

        gatedMemberWidget.render();
    },
    suffleArray:function(array){
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    },
    render: function () {

        var html = "";
        var l;

        for (var i = 0; i < gatedMemberWidget.logos.length; i++) {
            l = gatedMemberWidget.logos[i];
            html += "<div class='col-xs-2 cols-sm-2'><center><a href='" + l.href + "'><img src='" + l.src + "'><br><small>" + l.label + "</center></small></a></div>";
        }

        $(gatedMemberWidget.container).html(html);
    }

}