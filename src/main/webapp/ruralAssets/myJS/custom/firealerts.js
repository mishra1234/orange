var firealertsWidget = {
    
    IDdvContainer: "#dvFireAlerts",
    IDdvFireAlertsRender: "#dvFireAlertsRender",
    IDlnkFireAlerts: "#lnkFireAlerts",
    IDdvWidget: "#spnFireAlertsWidget",
    IDdvModal: "#dvFireAlertModal",

    Init: function () {

        var feeds = [];

        feeds.push("http://www.cfs.sa.gov.au/custom/criimson/CFS_Fire_Warnings.xml");
        
        for (
            var i = 0; i < feeds.length; i++) {

            feed = new google.feeds.Feed(feeds[i]);

            feed.setNumEntries(2)

            feed.load(function (result) {

                if (!result.error) {
                    firealertsWidget.ViewWidget(result.feed);                    
                }
            });

        }

    },
    FilterEntries: function (feed) {
        var entries = [];
        for (var i = 0; i < feed.entries.length; i++) {
            if (feed.entries[i].title != "No Current Fire Warnings") {
                entries.push(feed.entries[i]);
            }
        }
        return entries;
    },
    ViewWidget: function (feed) {

    	console.log(feed);
        var id = 'spnEntrieFA';
        var entries = firealertsWidget.FilterEntries(feed);
        //console.log("Number Fire warnings:" +  entries.length);
        var html = "<span id='" + id + "'>" + entries.length + "</span>";

        $(firealertsWidget.IDdvWidget).html(html);

        if (entries.length > 0) {
            $("#" + id)
                .css("cursor", "pointer")
                .bind("click", function (e) {
                e.preventDefault();
                bootbox.dialog({
                    title: "Fire Alerts",
                    message: firealertsWidget.EntriesToStr(feed, entries),
                    buttons: {
                        "Close": function () { }
                    }
                });
            });
        }
        
    },
    RenderEntries : function(feed, entries){

        var E, title, Title, link, description;
        var html = "<ul class='podcast-list'>";

        content = "";
        snippet = "";
        Title = feed.title;
        link = feed.link;
        description = feed.description;

        for (var i = 0; i < entries.length; i++) {

            E = entries[i];
            link = E.link;
            publishedDate = E.publishedDate;
            content = E.content;
            snippet = E.contentSnippet;
            title = E.title;

            html += "<li><a href='" + link + "'>";
            html += "<h3><strong>" + Title + "</strong></a><small>" + title + "</small><br><sub>" + (publishedDate==""?"":publishedDate) + "</sub></h3>";
            html += "</li>";

        }

        html += "</ul>";

        $(firealertsWidget.IDdvFireAlertsRender).html(html);

        $(firealertsWidget.IDlnkFireAlerts).attr("href", link);

    },
    EntriesToStr: function (feed, entries) {

        var E, title, Title, link, description;
        var html = "";

        content = "";
        snippet = "";
        Title = feed.title;
        link = feed.link;
        description = feed.description;

        for (var i = 0; i < entries.length; i++) {

            E = entries[i];
            link = E.link;
            publishedDate = E.publishedDate;
            content = E.content;
            snippet = E.contentSnippet;
            title = E.title;

            html += "<p><a href=" + link + ">";
            html += "<h5>" + Title + "</a><br><small>" + title + "</small><br><sub>" + (publishedDate == "" ? "" : publishedDate) + "</sub></h5>";
            html += "</p><br>";

        }
        console.log(html);
        return html;

    }

}