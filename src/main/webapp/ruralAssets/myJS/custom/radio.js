var radioController = {
    
    IDbtnSave: "#btnSave",
    IDbtnMore: "#btnMore",
    IDdvRadio: "#dvRadio",
    IDdvPlaying : "#playingName",

    Xml: null,

    Init: function () {

        radioController.getXml();
    },

    binds: function () {

        $(IDbtnSave).bind("click", function (e) {
            e.preventDefault();
            pg_form.save();
        });
        
    },

    getXmlLOCAL: function (){
            $.ajax({
                type: "GET",
                url: 'xml/abcrural.xml',
                dataType: "xml",
                success: function (xml) { radioController.refreshList(xml); }
            });     

    },


    getXml: function(all){
    
        var feed = new google.feeds.Feed("http://www.abc.net.au/local/podcasts/sacountryhour.xml");
        
        feed.setNumEntries(20)

        feed.load(function (result) {

            radioController.refreshList(result)
        });
        
},


    refreshList: function (xml, all) {        

        var html = "<a href='javascript:list();' id='togglePrevRadio' style='color:#efa740'><p style='color:#efa740'><li class='fa fa-chevron-down' id='prevIcon'></li>Previous Episodes</p></a><div id='radioHide' style='display:none;'><ul class='podcast-list' style='list-style-type: none'>";
        var max;

        radioController.Xml = xml;

        if (all) { max = 15; } else max = 5;
        
        var items = xml.feed.entries;

        if (items.length < max) { max = items.length; }

        for (var I = 0 ; I < max ; I++) {

            var item = items[I];
            //var name = item.getElementsByTagName("title")[0].firstChild.nodeValue;
            name2 = item.title;
            name = name2.replace("SA Country Hour", "");

            var link = item.mediaGroups[1].contents[0].url;

            //var link1 = item.getElementsByTagName("link")[0].firstChild.nodeValue;

            //var link = item.getElementsByTagName("media:content")[0].attributes.url.nodeValue;
             

            
            
            html = html + "<li data-role='button' class='mp3' data-link='" + link + "' style='text-align:left;'><a href='#' style='color:#efa740'><i class='fa fa-volume-off'></i>" + name + "</a></li>";            

        }

        html = html + "</ul></div>";


        //if (! all) {
        //    html = html + "<button id='btnMore2'>Show more</button>";
        //}
       

        $(radioController.IDdvRadio).html(html);

        $(".mp3").button().bind("click", function (e) {
            e.preventDefault();
            radioController.changeMp3($(this).data('link'), $(this).text());
        });

        $(radioController.IDbtnMore).button().bind("click", function (e) {
            e.preventDefault();
            radioController.refreshList(xml,1) ;
        });


        //defaults to the last:
        var itemD = items[0];        

        var nameD2 = itemD.title;
        nameD = nameD2.replace("SA Country Hour", "");
        var linkD = itemD.mediaGroups[1].contents[0].url;
        radioController.changeMp3(linkD, nameD,1);

    },

    changeMp3: function (link, name, noplay) {
        //var link = $(this).data-link;
        au1.src = link;
        if (! noplay) {
            au1.play();
        } 
        $(radioController.IDdvPlaying).html("<i class='fa fa-volume-up'></i>&nbsp;" + name);

}


}