var weatherWidget = {

    IDdvContainer: "#dvFireAlerts",
    IDdvFireAlertsRender: "#dvFireAlertsRender",
    IDlnkFireAlerts: "#lnkFireAlerts",

    IDdvWidget: "#spnWeatherWidget",
    IDdvGMap: "#dvGMap",
    CLdvGMap: ".dvGMap",

    IDspnTempMax: "#spnTempMax",
    IDspnTempMin: "#spnTempMin",
    IDspnHumidity: "#spnHumidity",
    IDspnWindSpeed: "#spnWindSpeed",
    IDspnLocationName: "#locationName",

    IDbtnChangeLocation: "#btnChangeLocation",

    Map: null,
    DefLat: -34.931155,
    DefLng: 138.605576,

    Init: function () {

        var feeds = [];        

        feeds.push("http://www.bom.gov.au/fwo/IDZ00057.warnings_sa.xml");

        for (var i = 0; i < feeds.length; i++) {

            feed = new google.feeds.Feed(feeds[i]);

            feed.setNumEntries(2)

            feed.load(function (result) {

                if (!result.error) {
                    weatherWidget.ViewWidget(result.feed);
                }
            });

        }
        
        weatherWidget.GetWeatherByLocation(-34.931155, 138.605576);
        
        weatherWidget.Binds();

        try{

            var coords = weatherWidget.GetCoords();
            var lat = coords.lat==null?weatherWidget.DefLat:coords.lat;
            var lng = coords.lng == null ? weatherWidget.DefLng : coords.lng;

            var map = GMaps({
                div: '#dvGMap',
                lat: lat,
                lng: lng,
                zoom: 6,
                disableDefaultUI: false,
                scrollwheel: false
            });

            weatherWidget.GetJSONPData(lat, lng);

            $(weatherWidget.CLdvGMap).toggle();

            try {
                weatherWidget.GetWeatherByLocation(coords.lat, coords.lng);
                
                map.setContextMenu({
                    control: 'map',
                    options: [{
                        title: 'Add marker',
                        name: 'add_marker',
                        action: function (e) {
                            this.addMarker({
                                lat: coords.lat,
                                lng: coords.lng,
                                title: 'Select location'
                            });
                        }
                    }]
                });

                map.addMarker({
                    lat: coords.lat,
                    lng: coords.lng
                });
                
            } catch (ex) {  }

            GMaps.on('marker_added', map, function (marker) {

                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();

                map.setCenter(lat, lng);

            });

            GMaps.on('click', map.map, function (event) {
                var index = map.markers.length;
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
                weatherWidget.GetWeatherByLocation(lat, lng);
                map.removeMarkers();
                map.addMarker({
                    lat: lat,
                    lng: lng
                });
                $(weatherWidget.CLdvGMap).toggle();
                map.refresh();
                weatherWidget.SetCoords(lat, lng);
            });            

            weatherWidget.Map = map;

        }catch(ex){ }

    },
    GetWeatherByLocation: function (lt, lg) {

        var url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lt + "&lon=" + lg + "&units=metric";

        $.get(url, function (response) {

            var country = response.sys.country;
            var location = response.name;
            var tmax = response.main.temp_max;
            var tmin = response.main.temp_min;
            var hum = response.main.humidity;
            var wspeed = response.wind.speed;

            $(weatherWidget.IDspnLocationName).html(location + ", " + country);
            $(weatherWidget.IDspnTempMax).html("<strong>" + tmax + "</strong> <small>C</small>");
            $(weatherWidget.IDspnTempMin).html("<strong>" + tmin + "</strong> <small>C</small>");
            $(weatherWidget.IDspnHumidity).html("<strong>" + hum + "</strong> <small>%</small>");
            $(weatherWidget.IDspnWindSpeed).html("<strong>" + wspeed + "</strong> <small>Km/h</small>");

        });
    },
    SetCoords:function(lat, lng){
        $.jStorage.set("wlt", lat);
        $.jStorage.set("wlg", lng);
    },
    GetCoords:function(){

        var lat = $.jStorage.get("wlt", null);
        var lng = $.jStorage.get("wlg", null);

        return { lat: lat, lng: lng };
        
    },
    Binds:function(){
        $(weatherWidget.IDbtnChangeLocation).bind("click", function (e) {
            e.preventDefault();
            weatherWidget.GetMap();
            weatherWidget.Map.refresh();
        });
    },
    GetJSONPData: function (lat, lng) {
        $.ajax({
            //url: 'http://api.openweathermap.org/data/2.5/find?q=adelaide&units=metric',
            url: "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&units=metric",
            dataType: 'jsonp',
            success: function (data) {

                //alert(data.list[0].weather[0].main);

                if (!(data.list == undefined)) {
                    $(weatherWidget.IDspnTempMax).html("<strong>" + data.list[0].main.temp_max + "</strong> <small>C</small>");
                    $(weatherWidget.IDspnTempMin).html("<strong>" + data.list[0].main.temp_min + "</strong> <small>C</small>");
                    $(weatherWidget.IDspnHumidity).html("<strong>" + data.list[0].main.humidity + "</strong> <small>%</small>");
                    $(weatherWidget.IDspnWindSpeed).html("<strong>" + data.list[0].wind.speed + "</strong> <small>Km/h</small>");
                } else {
                    if (!(data == undefined)) {
                        $(weatherWidget.IDspnTempMax).html("<strong>" + data.list.main.temp_max + "</strong> <small>C</small>");
                        $(weatherWidget.IDspnTempMin).html("<strong>" + data.list.main.temp_min + "</strong> <small>C</small>");
                        $(weatherWidget.IDspnHumidity).html("<strong>" + data.list.main.humidity + "</strong> <small>%</small>");
                        $(weatherWidget.IDspnWindSpeed).html("<strong>" + data.list.wind.speed + "</strong> <small>Km/h</small>");
                    } else {
                        $(weatherWidget.IDspnTempMax).html("<strong>-</strong> <small>C</small>");
                        $(weatherWidget.IDspnTempMin).html("<strong>-</strong> <small>C</small>");
                        $(weatherWidget.IDspnHumidity).html("<strong>-</strong> <small>%</small>");
                        $(weatherWidget.IDspnWindSpeed).html("<strong>-</strong> <small>Km/h</small>");
                    }                                  
                }

            }
        });
    },
    ViewWidget: function (feed) {

    	console.log(feed);
        var id = 'spnEntrieWA';
        var entries = feed.entries;
        console.log("Weather warnings: " + entries.length);
        var html = "<span id='" + id + "'>" + entries.length + "</span>";

        $(weatherWidget.IDdvWidget).html(html);

        if (entries.length > 0) {
            $("#" + id)
                .css("cursor", "pointer")
                .bind("click", function (e) {
                e.preventDefault();
                bootbox.dialog({
                    title: "Weather Alerts",
                    message:weatherWidget.EntriesToStr(feed, entries),
                    buttons: {
                        "Close": function () {}
                    }
                });
            });
        }

    },
    ViewVertical: function (feed) {

        var E, title, Title, link, description;
        var entries = feed.entries;
        var html = "<ul class='podcast-list'>";

        content = "";
        snippet = "";
        Title = feed.title;
        link = feed.link;
        description = feed.description;

        html += "<li><a href='" + link + "'><strong>" + Title + "</strong></a>";
        html += "</li>";

        for (var i = 0; i < entries.length; i++) {

            E = entries[i];
            link = E.link;
            publishedDate = E.publishedDate;
            content = E.content;
            snippet = E.contentSnippet;
            title = E.title;

            html += "<li>";
            html += "<h3>" + title + "<br><small>" + (publishedDate == "" ? "" : publishedDate) + "</small></h3>";
            html += "</li>";

        }

        html += "</ul>";

        $(weatherWidget.IDdvFireAlertsRender).html(html);

        $(weatherWidget.IDlnkFireAlerts).attr("href", link);

    },
    ViewHorizontal: function (feed) {

        var E, title, Title, link, description;
        var entries = feed.entries;
        var html = "";

        content = "";
        snippet = "";
        Title = feed.title;
        link = feed.link;
        description = feed.description;

        html += "<div class='widget-advanced'>";

        html += "<div class='widget-header text-center themed-background-dark'>";
        html += "<h3 class='widget-content-light clearfix'><small>" + Title + "</small></h3>";
        html += "</div>";

        html += "<div class='widget-main'>";

        html += "<a href='" + link + "' class='widget-image-container'>";
        html += "<span class='widget-icon themed-background'>";
        html += "&nbsp;<i class='fa fa-fire-extinguisher'></i>&nbsp;";
        html += "</span>";
        html += "</a>";

        html += "<div class='row text-center'>";

        html += "<ul class='podcast-list'>";
        
        for (var i = 0; i < entries.length; i++) {

            E = entries[i];
            link = E.link;
            publishedDate = E.publishedDate;
            content = E.content;
            snippet = E.contentSnippet;
            title = E.title;

            html += "<li>";
            html += "<h3>" + title + "<br><small>" + (publishedDate == "" ? "" : publishedDate) + "</small></h3>";
            html += "</li>";

        }

        html += "</ul>";
        html += "</div>";
        html += "</div>";

        $(weatherWidget.IDdvFireAlertsRender).html(html);

        $(weatherWidget.IDlnkFireAlerts).attr("href", link);

    },
    RenderEntries: function (feed, entries) {

        var E, title, Title, link, description;
        var html = "<ul class='podcast-list'>";

        content = "";
        snippet = "";
        Title = feed.title;
        link = feed.link;
        description = feed.description;

        for (var i = 0; i < entries.length; i++) {

            E = entries[i];
            link = E.link;
            publishedDate = E.publishedDate;
            content = E.content;
            snippet = E.contentSnippet;
            title = E.title;

            html += "<li><a href='" + link + "'>";
            html += "<h3><strong>" + Title + "</strong></a><small>" + title + "</small><br><sub>" + (publishedDate == "" ? "" : publishedDate) + "</sub></h3>";
            html += "</li>";

        }

        html += "</ul>";

        $(weatherWidget.IDdvContainer).html(html);        

    },
    EntriesToStr: function (feed, entries) {

        var E, title, Title, link, description;
        var html = "";

        content = "";
        snippet = "";
        Title = feed.title;
        link = feed.link;
        description = feed.description;

        for (var i = 0; i < entries.length; i++) {

            E = entries[i];
            link = E.link;
            publishedDate = E.publishedDate;
            content = E.content;
            snippet = E.contentSnippet;
            title = E.title;

            html += "<p><a href=" + link + ">";
            html += "<h5>" + Title + "</a><br><small>" + title + "</small><br><sub>" + (publishedDate == "" ? "" : publishedDate) + "</sub></h5>";
            html += "</p><br>";

        }

        return html;

    },
    GetMap: function () {

        $(weatherWidget.CLdvGMap).toggle();       
    }
}