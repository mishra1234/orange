function videoWidget(args){

    this.videos = args.videos;

    this.IDdvContainer = args.container == undefined ? "#dvVideo" : args.container;
    this.IDdvList = args.listcontainer;
    this.Source = args.source==undefined?"media/video/Prime.mp4":args.source;
    this.width = args.width == undefined ? "100%" : args.width;
    this.IDdvVideoPlayer = args.dvplayer;
    this.IDdvSource = args.srcVideo;
    this.IDdvDisclaimer = args.disclaimercontainer;
    this.AutoPlay = args.autoplay;

    this.Init = function () {

        var that = this;
        that.RenderList();

    };
    this.RenderList = function () {

        var that = this;
        var html = "<ul class='podcast-list'>";
        var v;

        for (var i = 0; i < that.videos.length; i++) {

            v = that.videos[i];

            html += "<li data-src='" + v.source + "'><h5><a data-index='" + i + "' class='dvsources' href='#'><i class='fa fa-video-camera fa-lg'></i>&nbsp;" + v.title + "</a> <i class='spnVideoCheck fa'></i> </h5></li>";

        }

        html += "</ul>";

        $(that.IDdvList).html(html);

        $.each($(".dvsources"), function (i, li) {
            $(li).bind("click", function (e) {
                e.preventDefault();
                that.ShowVideo($(li).attr("data-index"));
            });
        });
        if (that.AutoPlay) {
            that.ShowVideo(0);
        }

    };
    this.ShowVideo = function (index) {
        var that = this;
        $(that.IDdvVideoPlayer).attr({
            "src": that.videos[index].source,
            "autoplay": "autoplay"
        });
        $(that.IDdvDisclaimer).fadeOut("fast", function () {
            $(this).fadeIn("fast").html("<small><strong>" + that.videos[index].title + "</strong> " + that.videos[index].disclaimer + "</small>");
        });
        $(".spnVideoCheck").removeClass("fa-eye");
        $($(".spnVideoCheck")[index]).addClass("fa-eye");
    }

}