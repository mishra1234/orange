function fileWidget(args) {

    this.IDdvContainer = args.IDdvContainer;
    this.Docs = args.Docs; // {path:xx, ext:zz, page:yy }
    this.Groups = args.Groups == undefined ? null : args.Groups;
    this.ZoomLevel = args.ZoomLevel == undefined ? 100 : args.ZoomLevel;
    this.BaseURL = "https://docs.google.com/viewer?url=^&a=bi&pagenumber=|&w=" + this.ZoomLevel;
    this.iconWidth = args.iconWidth==undefined?"":args.iconWidth;
    this.iconHeight = args.iconHeight == undefined ? "" : args.iconHeight;
    this.grouping = args.grouping==undefined?false:args.grouping;

    this.Init = function () {

        this.RenderGridIcons();
        return this;

    };
    this.GetIcoExtension = function (extension) {

        var ico = "";

        switch (extension.toUpperCase()) {
            case "PDF":
                ico = "fa fa-file-pdf-o";
                break;
            case "DOC":
                ico = "fa fa-file-word-o";
                break;
            case "PPT":
                ico = "fa fa-file-powerpoint-o";
                break;
            case "XLS":
                ico = "fa fa-file-xls-o";
                break;
            case "PNG": case "JPG": case "GIF":
                ico = "fa fa-image";
                break;
            default:
                ico = "fa fa-file-pdf-o";
        }

        return ico;

    },
    this.RenderIcons = function () {

        var that = this;
        var rnd = Math.floor((Math.random() * 10000) + 1);
        var html = "<div id='dv" + rnd + "' class='owl-carousel'>";
        var ext = "";
        var html;
        var Rnd = Math.floor((Math.random() * 10000) + 1);

        if (that.grouping == true) {

            html = "<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";
            html += "<div class='panel panel-default'>";

            for (var g = 0; g < that.Groups.length; g++) {

                rnd = Math.floor((Math.random() * 10000) + 1);

                html += "<div class='panel-heading' role='tab' id='hGrp" + rnd + "'>";
                html += "<h4 class='panel-title'>";
                html += "<a data-toggle='collapse' data-parent='#accordion' href='#cGrp" + rnd + "' aria-expanded='false' aria-controls='cGrp" + rnd + "'>";
                html += that.Groups[g].name + " <strong>[" + that.Groups[g].Docs.length + "]</strong>";
                html += "</a></h4>";
                html += "</h4>";
                html += "</div>";

                html += "<div id='cGrp" + rnd + "' class='panel-collapse collapse " + (g == 0 ? "in" : "") + "' role='tabpanel' aria-labelledby='hGrp" + rnd + "'>";
                html += "<div class='panel-body'>";
                html += "<div id='dv" + rnd + "' class='owl-carousel carrousel'>";

                for (var i = 0; i < that.Groups[g].Docs.length; i++) {
                    html += "<div class='item'><center>";
                    html += "<img src='img/file_ext/" + that.Groups[g].Docs[i].ext + ".png' width='" + that.iconWidth + "' height='" + that.iconHeight + "'>";
                    html += "<br><small><i class='" + ext + " fa-lg'></i> ";
                    html += "<a class='dvclass" + Rnd + "' target='#' href='#' data-ico='" + ext + "' data-title='" + that.Groups[g].Docs[i].title + "' data-href='" + that.Groups[g].Docs[i].path + "'>" + that.Groups[g].Docs[i].title + "</a></small>";
                    html += "</center></div>";
                }

                html += "</div>";
                html += "</div>";
                html += "</div>";

            }

            html += "</div>";
            html += "</div>";

        } else {

            html = "<div id='dv" + rnd + "' class='owl-carousel carrousel'>";

            for (var i = 0; i < that.Docs.length; i++) {

                html += "<div class='item'><center>";
                html += "<img src='img/file_ext/" + that.Docs[i].ext + ".png' width='" + that.iconWidth + "' height='" + that.iconHeight + "'>";
                html += "<br><small><i class='" + ext + " fa-lg'></i> ";
                html += "<a class='dvclass" + Rnd + "' target='#' href='#' data-ico='" + ext + "' data-title='" + that.Docs[i].title + "' data-href='" + that.Docs[i].path + "'>" + that.Docs[i].title + "</a></small>";
                html += "</center></div>";

            }

            html += "</tr>";
            html += "</div>";
        }

        $(that.IDdvContainer).html(html);

        $.each($(".carrousel"), function (i, el) {
            $(el).owlCarousel({
                navigation: true,
                autoPlay: 3000
            });
        });
        $.each($(".dvclass" + Rnd), function (i, el) {
            $(el).bind("click", function (ev) {
                ev.preventDefault();
                var lnk = "http://docs.google.com/viewer?url=" + (encodeURIComponent($(el).attr("data-href"))) + "&embedded=true";
                bootbox.dialog({
                    title: "File Preview",
                    message: "<h3><i class='" + $(el).attr("data-ico") + "'></i> " + $(el).attr("data-title") + "</h3><br><iframe src='" + lnk + "' width='98%' height='400' style='border: none;'></iframe>",
                    buttons: {
                        "Close": function () { }
                    }
                });
            });
        });

    },
    this.RenderGridIcons = function(){
        
        var that = this;
        var rnd = Math.floor((Math.random() * 10000) + 1);
        var html = "<div id='dv" + rnd + "'>";
        var ext = "";
        var html;
        var Rnd = Math.floor((Math.random() * 10000) + 1);
        
        if (that.grouping == true) {
            
            html = "<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";
            html += "<div class='panel panel-default'>";

            for (var g = 0; g < that.Groups.length; g++) {

                rnd = Math.floor((Math.random() * 10000) + 1);

                html += "<div class='panel-heading' role='tab' id='hGrp" + rnd + "'>";
                html += "<h4 class='panel-title'>";
                html += "<a data-toggle='collapse' data-parent='#accordion' href='#cGrp" + rnd + "' aria-expanded='false' aria-controls='cGrp" + rnd + "'>";
                html += that.Groups[g].name + " <strong>[" + that.Groups[g].Docs.length + "]</strong>";
                html += "</a></h4>";
                html += "</h4>";
                html += "</div>";

                html += "<div id='cGrp" + rnd + "' class='panel-collapse collapse " + (g == 0 ? "in" : "") + "' role='tabpanel' aria-labelledby='hGrp" + rnd + "'>";
                html += "<div class='panel-body'>";
                html += "<div id='dv" + rnd + "' >";

                html += "<ul>";
                
                //aquí
                for (var i = 0; i < that.Groups[g].Docs.length; i++) {
                    html += "<div class='item'>";
                    html += "<img src='img/file_ext/" + that.Groups[g].Docs[i].ext + ".png' width='"
                        + "20" + "' height='" + "20" + "'>";
                    html += "<small><i class='" + ext + " fa-lg'></i> ";
                    html += "<a class='dvclass" + Rnd + "' target='#' href='#' data-ico='"
                        + ext + "' data-title='" + that.Groups[g].Docs[i].title + "' data-href='"
                        + that.Groups[g].Docs[i].path + "'>" + that.Groups[g].Docs[i].title + "</a></small>";
                    html += "</div><br>";
                }

                html += "</div>";

                html += "</ul>";

                html += "</tr>";


                html += "</div>";
                html += "</div>";
                
            }

            html += "</div>";
            html += "</div>";

        } else {
            
            html = "<div id='dv" + rnd + "'>";
            html += "<ul>";
            //y aquí
            for (var i = 0; i < that.Docs.length; i++) {

                
                html += "<div class='item'>";
                html += "<img src='img/file_ext/" + that.Docs[i].ext + ".png' width='" + "20"
                    + "' height='" + "20" + "'>";
                html += "<small><i class='" + ext + " fa-lg'></i> ";
                html += "<a class='dvclass" + Rnd + "' target='#' href='#' data-ico='" + ext
                    + "' data-title='" + that.Docs[i].title + "' data-href='" + that.Docs[i].path + "'>" + that.Docs[i].title + "</a></small>";
                html += "</div><br>";
               
            }
            html += "</ul>";

            html += "</tr>";
            html += "</div>";
        }
        
        $(that.IDdvContainer).html(html);

        $.each($(".dvclass" + Rnd), function (i, el) {
            $(el).bind("click", function (ev) {
                ev.preventDefault();
                var lnk = "http://docs.google.com/viewer?url=" + (encodeURIComponent($(el).attr("data-href"))) + "&embedded=true";
                bootbox.dialog({
                    title: "File Preview",
                    message: "<h3><i class='" + $(el).attr("data-ico") + "'></i> " + $(el).attr("data-title") + "</h3><br><iframe src='" + lnk + "' width='98%' height='400' style='border: none;'></iframe>",
                    buttons: {
                        "Close": function () { }
                    }
                });
            });
        });

    },
    this.RenderThumbs = function () {

        var that = this;
        var rnd = Math.floor((Math.random() * 10000) + 1);
        var html = "<div id='dv" + rnd + "' class='owl-carousel'>";
        var ext = "";
               
        for (var i = 0; i < that.Docs.length; i++) {
            
            ext = that.GetIcoExtension(that.Docs[i].ext);

            html += "<div class='item'><center>";
            html += that.GetThumbnail(that.Docs[i]);
            html += "<br><small><i class='" + ext + " fa-lg'></i> ";
            html += "<a class='dvclass" + rnd + "' target='#' href='#' data-ico='" + ext + "' data-title='" + that.Docs[i].title + "' data-href='" + that.Docs[i].path + "'>" + that.Docs[i].title + "</a></small>";
            html += "</center></div>";            
                                   
        }

        html += "</tr>";
        html += "</div>";

        $(that.IDdvContainer).html(html);
        $("#dv" + rnd).owlCarousel({
            navigation: true,
            autoPlay:3000
        });
        $.each($(".dvclass" + rnd), function (i, el) {
            $(el).bind("click", function (ev) {
                ev.preventDefault();
                var lnk = "http://docs.google.com/viewer?url=" + (encodeURIComponent($(el).attr("data-href"))) + "&embedded=true";
                bootbox.dialog({
                    title: "File Preview",
                    message: "<h3><i class='" + $(el).attr("data-ico") + "'></i> " + $(el).attr("data-title") + "</h3><br><iframe src='" + lnk + "' width='98%' height='400' style='border: none;'></iframe>",
                    buttons: {
                        "Close": function () { }
                    }
                });
            });
        });

    },
    this.GetThumbnail= function (doc) {

        var that = this;
        var html = "<iframe src='" + (that.BaseURL.replace("^", doc.path).replace("|", doc.page)) + "' style='border:none;text-align:center' scrolling='no'></iframe>";
        return html;

    },
    this.GetThumbnailIcon = function (doc) {

        var that = this;
        var html = "<iframe src='" + (that.BaseURL.replace("^", doc.path).replace("|", doc.page)) + "' style='border:none;text-align:center' scrolling='no'></iframe>";
        return html;

    }

}