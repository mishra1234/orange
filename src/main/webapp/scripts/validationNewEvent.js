document.observe('dom:loaded', function() {
	RowSelector.bindEvents();
	
	<script>
	(function() {
	
		'use strict';
		jQuery.validator.addMethod("pattern", function(value, element) {
		    return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
		}, "Your data contain invalid characters");
		   
		jQuery.validator.addMethod("urlIcon", function(value, element) {
			    	return isValidIconSize();
				}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only.");
		
	 
		jQuery.validator.addMethod("pattern", function(value, element) {
		return this.optional(element) || /^([A-Za-z-_0-9!@#$?^&*.,+=;':\[\]\}\{\\\/|`~()>< \n\s]+)$/.test(value);
		}, "Your data contain invalid characters");

		jQuery.validator.addMethod("urlIcon", function(value, element) {
			return isValidIconSize();
		}, "Invalid Icon, use 200px X 200px size and use .jpg or .png images only.");

		function isValidIconSize(){
		
		var imgWidth = $("#theIcon").width();
		var imgHeight = $("#theIcon").height();
		var extension = $('#theIcon').attr("src").split('.').pop().toLowerCase();;
		console.log("URL: " + extension);
		console.log("width:" + imgWidth);
		console.log("height:" + imgHeight);
		if (imgHeight == 200 && imgWidth == 200 && (extension == "jpg" || extension == "png"))
		return true;
		 };   
	
		//	 jQuery.validator.addMethod("urlVideo", function(value, element) {
		//	    	return isValidFileType();
		//		}, "Invalid Video type, use mp4 videos only.");
		
		//function isValidFileType(){
		
		//	 var extension1 = $('#theVideoImage').attr("src").split('.').pop().toLowerCase();;
		//	 console.log("Video URL: " + extension);
		//	 if (extension == "mp4")
		//		return true;
		//		 };   
		 
	
		jQuery.validator.addMethod("urlBanner", function(value, element) {
		    	return isValidBannerSize();
			}, "Invalid Banner, use 640px X 320px size and use .jpg or .png images only.");
	
		function isValidBannerSize(){
		
		 var imgWidth = $("#theBannImage").width();
		 var imgHeight = $("#theBannImage").height();
		 var extension = $('#theBannImage').attr("src").split('.').pop().toLowerCase();;
		 console.log("Banner URL: " + extension);
		 console.log("Banner width:" + imgWidth);
		 console.log("Banner height:" + imgHeight);
		 if (imgHeight == 320 && imgWidth == 640 && (extension == "jpg" || extension == "png"))
			return true;
			 };   
		
		//Wizard validation
		var $w3finish = $('#form').find('ul.pager li.finish'),
		$w3validator = $("#form").validate({
			highlight: function(element) {
				
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
				$(element).remove();
			},
			errorPlacement: function( error, element ) {
				element.parent().append( error );
			}
		});
		
		// Submit the from when click finish
		$w3finish.on('click', function( ev ) {
			//ev.preventDefault();
			var validated = $('#form').valid();
			if ( validated ) {
				$("#Save").click(function() {
					  $("#form").submit();
					});
			}
		});
	
		$('#form').bootstrapWizard({
			tabClass: 'wizard-steps',
			nextSelector: 'ul.pager li.next',
			previousSelector: 'ul.pager li.previous',
			firstSelector: null,
			lastSelector: null,
			onNext: function( tab, navigation, index, newindex ) {
				console.log("Wizard validation started!!");
				var validated = $('#form').valid();
				if( !validated ) {
					$w3validator.focusInvalid();
					return false;
				}
			},
			onTabClick: function( tab, navigation, index, newindex ) {
				if ( newindex == index + 1 ) {
					return this.onNext( tab, navigation, index, newindex);
				} else if ( newindex > index + 1 ) {
					return false;
				} else {
					return true;
				}
			},
			onTabChange: function( tab, navigation, index, newindex ) {
				var $total = navigation.find('li').size() - 1;
				$w3finish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
				$('#form').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
			},
			onTabShow: function( tab, navigation, index ) {
				var $total = navigation.find('li').length - 1;
				var $current = index;
				var $percent = Math.floor(( $current / $total ) * 100);
				$('#form').find('.progress-indicator').css({ 'width': $percent + '%' });
				tab.prevAll().addClass('completed');
				tab.nextAll().removeClass('completed');
			}
		});
			// basic
			$("#form").validate({
				highlight: function( label ) {
					console.log("validation starts");
					$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				success: function( label ) {
					$(label).closest('.form-group').removeClass('has-error');
					label.remove();
				},
				errorPlacement: function( error, element ) {
					var placement = element.closest('.input-group');
					if (!placement.get(0)) {
						placement = element;
					}
					if (error.text() !== '') {
						placement.after(error);
					}
				}
			});

	}).apply( this, [ jQuery ]);

</script>
});
