$(function () {
	
    $('#fileuploadFile').fileupload({
        dataType: 'json',
        autoUpload: true,
        done: function (e, data) {
        	//$('tr.newF td').remove();
            $.each(data.result, function (index, file) {
            	$("#eventFile").val(file.url);
            	$("#uploaded-files-eventFile").append(
                		$('<tr class="newF"/>')
                		.append($('<td/>').html("<input type='text'   name='titleFile[]'/>"))
                		.append($('<td/>').html("<input type='text'   name='descriptionFile[]'/>"))
                		.append($('<td/>').html("<input type='hidden' name='fileId[]'/>"+
                								"<input type='hidden' name='fileUrl[]' value='"+file.url+"'/>"+
                								"<input type='hidden' name='fileRemove[]'/>"+
												"<img src='https://s3.amazonaws.com/ccmeresources/30d2b739-3137-4a3b-b07f-48cdfad5c8cf-pdf.png' width='20px' height='20px'/>"))
                		.append($('<td/>').html('<input type="button" class="remo btn-xs btn-danger" value="Remove"/>'))
                		);
            }); 
        },
         
        progressall: function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progressFile .progress-bar-primaryFile').css(
	            'width',
	            progress + '%'
	        );
   		},
   		
		dropZone: $('#dropzoneFile')
    });
});