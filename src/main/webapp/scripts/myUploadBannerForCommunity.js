$(function () {
	
	 $('#fileuploadBanner').fileupload({
	        dataType: 'json',
	        autoUpload:true,
	        done: function (e, data) {
	        	//$("tr.newB").remove();
	            $.each(data.result, function (index, file) {
	            	$("#bannerImage").val(file.url);
	            	$("#displayBanner").empty();
	           	 	$("#displayBanner").append("<img id='theInfoBanner' src='"+file.url+"'/>");
	           	 	
	            	$("#uploaded-files-banner").append(
	                		$('<tr class="newB"/>')
	                		.append($('<td/>').html('<input type="text"   name="titleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="text"   name="subtitleBanner[]"/>'))
	                		.append($('<td/>').html('<input type="hidden" name="bannerId[]"/>'+
	                								'<input type="hidden" name="bannerUrl[]" value="'+file.url+'"/>'+
	                								'<input type="hidden" name="bannerRemove[]"/>'+
													'<img src="'+file.url+'" width="30px" height="20px"/>'))
	                		.append($('<td/>').html('<input type="button" class="remo btn-xs btn-danger" value="Remove"/>'))
	                		);
	            }); 
	        },
	         
	        progressall: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#progress .progress-bar-primary').css(
		            'width',
		            progress + '%'
		        );
	   		},
	   		
			dropZone: $('#dropzoneBanner')
	    });
	});
