$(function () {
	
    $('#newAccountIconUpload').fileupload({
        dataType: 'json',
        done: function (e, data) {
        	$("tr:has(td)").remove();
        	   $.each(data.result, function (index, file) {
            	 $("#accountIcon").val(file.url);
            	$("#displayAccountIcon").empty();
                 $("#displayAccountIcon").append("<img id='theImg' src='"+file.url+"'/>");
                 $("#uploaded-files").empty();
                   $("#uploaded-files").append(
                		$('<tr/>')
                		.append($('<td/>').text(file.fileName))
                		.append($('<td/>').text(file.fileSize))
                		.append($('<td/>').text(file.fileType))
                		.append($('<td/>').text(index))
                		.append($('<td/>').html("<a href='"+file.url+"'>Click</a>"))
                		)//end $("#uploaded-files").append()
            }); 
        },
         
        progressall: function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progress .progress-bar-primary').css(
	            'width',
	            progress + '%'
	        );
   		},
   		
		dropZone: $('#dropzone')
    });
});





