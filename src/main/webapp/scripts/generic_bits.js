// JavaScript Document

$(document).ready(function() {
	// show/hide ticket details on click of expandable yellow box
	$(".expandableRegion .expandableButton").click(function() {
		//var currID = $(this).attr("name");
		if($(this).next(".detail").is(':visible')){
			$(this).removeClass("active")
			$(this).next(".detail").slideUp(500);
		}else{
			$(this).addClass("active")
			$(this).next(".detail").slideDown(500);
		}
	})
	
	$('.marquee').marquee({
			speed: 15000,
			//gap in pixels between the tickers
			gap: 500,
			//gap in pixels between the tickers
			delayBeforeStart: 0	
	});
})