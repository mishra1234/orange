$(function () {
    $('#fileuploadvideo').fileupload({
        dataType: 'json',
        autoUpload: true,
        
        done: function (e, data) {
        	//$('tr.newV td').remove();
            $.each(data.result, function (index, file) {
            	$("#eventvideo").val(file.url);
            	var fileZ = data.files[0];
            	$("#uploaded-files-eventvideo").append($("<tr class='newV'/>")
            			.append($("<td/>").html("<input type='text'   name='titleVideo[]'/>"))
                		.append($("<td/>").html("<input type='text'   name='descriptionVideo[]'/>"))
                		.append($("<td/>").html("<input type='hidden' name='videoId[]'/>"+
                								"<input type='hidden' name='videoUrl[]' value='"+file.url+"'/>"+
                								"<input type='hidden' name='videoRemove[]'/>"+
                								"<input type='hidden' name='videoIconUrl[]' value='"+"https://s3.amazonaws.com/ccmeresources/ba059896-324e-43f5-ac29-e67136d9fa57-video_icon.png"+"'/>"+
                								"<img src='"+"https://s3.amazonaws.com/ccmeresources/ba059896-324e-43f5-ac29-e67136d9fa57-video_icon.png"+"' width='30px' height='20px'/>"))
                		.append($("<td/>").html("<input type='button' class='remo btn-xs btn-danger' value='Remove'/>"))
                		);                
            }); 
        },
         
        progressall: function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progressvideo .progress-bar-primaryVideo').css(
	            'width',
	            progress + '%'
	        );
   		},
   		
		dropZone: $('#dropzonevideo')
    });
});