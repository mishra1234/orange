$(function () {
	
    $('#iconUploadComm').fileupload({
        dataType: 'json',
        autoUpload: true,
        done: function (e, data) {
            $.each(data.result, function (index, file) {
            	 $("#theIconImage").val(file.url);
            	 $("#displayIconDiv").empty();
            	 $("#displayIconDiv").append("<img id='theIconImage' src='"+file.url+"'/>");
            }); 
        },
         
        progressall: function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progressCommIcon .progress-bar-primary').css(
	            'width',
	            progress + '%'
	        );
   		},
   		
		dropZone: $('#dropzone')
    });
});