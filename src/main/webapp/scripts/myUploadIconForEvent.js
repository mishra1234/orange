$(function () {
	
    $('#fileuploadEvent').fileupload({
        dataType: 'json',
        autoUpload: true,
        done: function (e, data) {
            $.each(data.result, function (index, file) {
            	 $("#theIcon").val(file.url);
            	 $("#displayIcon").empty();
            	 $("#displayIcon").append("<img id='theIcon' src='"+file.url+"'/>");
            }); 
           
        },
        progressall: function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progressEventIcon .progress-bar-primaryIcon').css(
	            'width',
	            progress + '%'
	        );
   		},
   		
		dropZone: $('#dropzoneIcon')
    });
});