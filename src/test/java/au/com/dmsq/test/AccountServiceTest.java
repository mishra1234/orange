package au.com.dmsq.test;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Role;
import au.com.dmsq.service.AccountService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:testContext.xml"})
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@TransactionConfiguration(transactionManager = "transactionManager")
public class AccountServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	AccountService accountService;
	
	

/*	@Test
	public void  save() 
	{
		Role adminx = new Role();
		adminx.setId(1);
		
		Account account = new Account();
		account.setName("Tanuja Mishra"); 
		account.setEmail("tanuja@mail.com");
		account.setPassword("PASSWORD");
		account.setSalt("SALT CODE");
		account.setTimezone("+9:30");
		account.setRole(adminx);
		account.setFirstName("John");
		account.setLastName("Doe");
		account.setPostcode(5000);
		account.setEnabled(true);
		account.setStatus("active");
		account.setResetCode("12345");
		
		Assert.assertEquals(accountService.save(account), true);
	}*/

//	@Test

//	@Test		// -- This test is working --
//	public void  save() {
//		Role adminx = new Role();
//		adminx.setId(1);
//		
//		Account account = new Account();
//		account.setName("John Doe 20 AUG 2014"); 
//		account.setEmail("email@mail.com");
//		account.setPassword("PASSWORD");
//		account.setSalt("SALT CODE");
//		account.setTimezone("+9:30");
//		account.setRole(adminx);
//		account.setFirstName("John");
//		account.setLastName("Doe");
//		account.setPostcode(5000);
//		account.setEnabled(true);
//		account.setStatus("active");
//		account.setResetCode("12345");
//		
//		Assert.assertEquals(accountService.save(account), true);
//		
//		account = accountService.get(account.getId());
//		Assert.assertNotNull(account);
//
//	}
	
	/*@Test
	public void findAll(){
		List<Account> accountList = accountService.findAll();
		
		Assert.assertTrue(accountList.size() > 0);
	}*/
//	@Test		// -- This test is working --
//	public void findAll(){
//		List<Account> accountList = accountService.findAll();
//		
//		Assert.assertTrue(accountList.size() > 0);
//	}
	
//	@Test		// -- This test is working --
//	@Rollback(false)
//	public void removeById() {
//		boolean account = false;
//		account = accountService.deleteById(104);
//		Assert.assertEquals(account, true);
//	}
//	
//	@Test		// -- This test is working --
//	public void get() {
//		Account account = accountService.get(113);
//		Assert.assertNotNull(account.getId());
//	}
	
	@Test		// -- This test is working --
	public void checkNonDeleted() {
//		List<Account> nonDeletedAccounts = accountService.getNonDeleted();
//		for (Account account : nonDeletedAccounts) {
//			Boolean enabled = account.getEnabled();
//			Assert.assertEquals(enabled, true);
//		}
	}
}
