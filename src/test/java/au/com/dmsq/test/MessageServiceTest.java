package au.com.dmsq.test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Message;
import au.com.dmsq.model.Account;
import au.com.dmsq.service.MessageService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:testContext.xml"})
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@TransactionConfiguration(transactionManager = "transactionManager")
public class MessageServiceTest extends AbstractJUnit4SpringContextTests{

	@Autowired
	MessageService messageService;
	
	
	@Test		// -- This test is working --
	public void  save() {
//		Account accountFrom = new Account();
//		Account accountTo= new Account();
//		accountFrom.setId(4);
//		accountTo.setId(30);
//		
//		Message message = new Message();
//		message.setSubject("Message Subject 22 AUG 2014"); 
//		message.setMessage("This is just a test");
//		message.setDateSent((long) (1407991883));
//		message.setRead(false);
//		message.setDeleted(false);
//		message.setLayerId(798);
//		message.setActivityId("activity id X");
//		message.setAccountByFromId(accountFrom);
//		message.setAccountByToId(accountTo);
//		
//		Assert.assertEquals(messageService.save(message), true);
//		
//		message = messageService.get(message.getId());
//		Assert.assertNotNull(message);
//	
//	}
//	
//	@Test		// -- This test is working --
//	public void findAll(){
//		List<Message> messageList = messageService.findAll();
//		
//		Assert.assertTrue(messageList.size() > 0);
//	}
//	
//	@Test		// -- This test is working --
//	@Rollback(false)
//	public void removeById() {
//		boolean message = false;
//		message = messageService.deleteById(61);
//		Assert.assertEquals(message, true);
//	}
//	
//	@Test		// -- This test is working --
//	public void get() {
//		Message message = messageService.get(65);
//		Assert.assertNotNull(message.getId());
//	}
	
//	@Test		// -- This test is working --
//	public void checkNonDeleted() {
//		List<Message> nonDeletedMessages = messageService.getNonDeleted();
//		for (Message message : nonDeletedMessages) {
//			Boolean deleted = message.getDeleted();
//			Assert.assertEquals(deleted, true);
//		}
	}
	
}
