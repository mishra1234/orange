package au.com.dmsq.test;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Role;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.EventService;
import au.com.dmsq.service.LayerService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:testContext.xml"})
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@TransactionConfiguration(transactionManager = "transactionManager")
public class LayerServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	LayerService layerService;
	
//	//This test is working
	@Test
	public void save() 
	{
//		Account account = new Account();
//		account.setId(2);  //Parent ID
//		Layer layer = new Layer();
//		layer.setName("Anoop Test Service Layer");
//		layer.setAccount(account);
//		layer.setRecommended(true);
//		layer.setIconUrl("http://s3:amazon-web-services/icons/anoop.jpg");
//		layer.setDeleted(false);
//		layer.setConnectable(true);
//		Assert.assertEquals(layerService.save(layer), true);
	}

//	@Test		// -- This test is working --
//	public void get() {
//		Layer layer = layerService.get(1033);
//		Assert.assertNotNull(layer.getId());
//	}
	
	
	/*@Test
	public void findAll(){
		List<Layer> layerList = layerService.findAll();
		
		Assert.assertTrue(layerList.size() > 0);
	}*/
	

//	@Test
//	@Rollback(false)
//	public void removeById() 
//	{
//		Layer layer = layerService.get(926);
//		boolean delete = layerService.remove(layer);
//		Assert.assertEquals(delete, true);
//	}

	/*@Test
	@Rollback(false)
	public void removeById() 
	{
		Layer layer = layerService.get(926);
		boolean delete = layerService.remove(layer);
		Assert.assertEquals(delete, true);
	}
	*/
	
	
//	@Test
//	public void checkNonDeleted() {
//		List<Layer> nonDeletedLayers = layerService.getNonDeleted();
//		for (Layer layer : nonDeletedLayers) {
//			Boolean deleted = layer.getDeleted();
//			Assert.assertEquals(deleted, false);
//		}
//	}
}


