package au.com.dmsq.test;

import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Auth;
import au.com.dmsq.service.AuthService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@TransactionConfiguration(transactionManager = "transactionManager")
public class AuthServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	AuthService authService;
	
	@Test		// -- This test is working --
	public void  save() {
//		
//		Account accountx = new Account();
//		accountx.setId(112);
//		Auth auth = new Auth();
//		auth.setToken("a8e270d2f9b0764e6d07cec142b0f57ede4699f3");
//		auth.setExpiry((long) 1408427403);
//		auth.setDevice("IOX Testing");
//		auth.setAccount(accountx);
//		
//		Assert.assertEquals(authService.save(auth), true);
//		
//		auth = authService.get(auth.getId());
//		Assert.assertNotNull(auth);
//
	}
	
//	@Test		// -- This test is working --
//	public void findAll(){
//		List<Auth> authList = authService.findAll();
//		
//		Assert.assertTrue(authList.size() > 0);
//	}
	
//	@Test		// -- This test is working --
//	public void get() {
//		Auth auth = authService.get(126);
//		Assert.assertNotNull(auth.getId());
//	}
	
//	@Test		// -- This test is working --
//	@Rollback(false)
//	public void deleteById() {
//		boolean auth = false;
//		auth = authService.deleteById(125);
//		Assert.assertEquals(auth, true);
//	}
	
}
