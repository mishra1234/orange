package au.com.dmsq.test;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.model.Event;
import au.com.dmsq.model.Layer;
import au.com.dmsq.model.Role;
import au.com.dmsq.service.AccountService;
import au.com.dmsq.service.EventService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:testContext.xml"})
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@TransactionConfiguration(transactionManager = "transactionManager")
public class EventServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	EventService eventService;
	
	
	@Test
	public void save() 
	{
//		Activity activity = new Activity();
//		activity.setId(6);
//		Event event = new Event();
//		event.setActivity(activity);
//		event.setAddress("Punjab");
//		event.setContactEmail("anoop1000@gmail.com");
//		event.setContactName("Anoop Graduation Ceremony");
//		event.setOrganisation("Carnegie Mellon University Australia");
//		event.setCost("FREE");
//		event.setDisplayDatetime("2654564456456");
//		Assert.assertEquals(eventService.save(event), true);
	}

//	@Test		// -- This test is working --
//	public void get() {
//		Event event = eventService.get(1036);
//		Assert.assertNotNull(event.getActivityId());
//	}
	
//	@Test
//	public void findAll(){
//		List<Event> eventList = eventService.findAll();
//		
//		Assert.assertTrue(eventList.size() > 0);
//	}
	
	/*@Test
	@Rollback(false)
	public void removeById() 
	{
		Event event = eventService.get(908);
		boolean delete = eventService.remove(event);
		Assert.assertEquals(delete, true);
	}*/
	
	
	

}


