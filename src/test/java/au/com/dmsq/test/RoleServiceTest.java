package au.com.dmsq.test;

import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import au.com.dmsq.model.Role;
import au.com.dmsq.service.RoleService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@TransactionConfiguration(transactionManager = "transactionManager")
public class RoleServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	RoleService roleService;
	
	@Test		// -- This test is working --
	public void  save() {
//		
//		Role role = new Role();
//		role.setName("SuperPowers");
//		
//		Assert.assertEquals(roleService.save(role), true);
//		
//		role = roleService.get(role.getId());
//		Assert.assertNotNull(role);
//
	}
	
//	@Test		// -- This test is working --
//	public void findAll(){
//		List<Role> roleList = roleService.findAll();
//		
//		Assert.assertTrue(roleList.size() > 0);
//	}
	
//	@Test		// -- This test is working --
//	public void get() {
//		Role role = roleService.get(5);
//		Assert.assertNotNull(role.getId());
//	}
	
//	@Test		// -- This test is working --
//	@Rollback(false)
//	public void removeById() {
//		boolean role = false;
//		role = roleService.deleteById(4);
//		Assert.assertEquals(role, true);
//	}
	
}
