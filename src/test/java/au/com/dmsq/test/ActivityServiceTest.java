package au.com.dmsq.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import au.com.dmsq.model.Account;
import au.com.dmsq.model.Activity;
import au.com.dmsq.service.ActivityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@TransactionConfiguration(transactionManager = "transactionManager")
public class ActivityServiceTest extends AbstractJUnit4SpringContextTests {
	
	@Autowired
	ActivityService activityService;

/*	@Test
	public void  save() {
		Account accountx = new Account();
		accountx.setId(13);
				
		Activity activity = new Activity(); 
		activity.setContentTitle("Content Title Test 20/08/14");
		activity.setContentBody("Content Body Text Test");
		activity.setActivityType("Event");
		activity.setActionUrl("http://actionurl.test");
		activity.setActionTitle("Movie");
		activity.setRecommended(true);
		activity.setIconUrl("http://www.iconurl.com");
		activity.setDeleted(false);
		activity.setAccount(accountx);
		
		Assert.assertEquals(activityService.save(activity), true);
		
		//activity = activityService.get(activity.getId());
		//Assert.assertNotNull(activity);

	}*/

	@Test		// -- This test is working --
	public void  save() { 
//		Account accountx = new Account();
//		accountx.setId(13);
//				
//		Activity activity = new Activity(); 
//		activity.setContentTitle("Content Title Test 20/08/14");
//		activity.setContentBody("Content Body Text Test");
//		activity.setActivityType("Event");
//		activity.setActionUrl("http://actionurl.test");
//		activity.setActionTitle("Movie");
//		activity.setRecommended(true);
//		activity.setIconUrl("http://www.iconurl.com");
//		activity.setDeleted(false);
//		activity.setAccount(accountx);
//
//		Assert.assertEquals(activityService.save(activity), true);
//		
//		activity = activityService.get(activity.getId());
//		Assert.assertNull(activity);
	}
	
//	@Test		// -- This test is working --
//	public void findAll(){
//		List<Activity> activityList = activityService.findAll();
//		
//		Assert.assertTrue(activityList.size() > 0);
//	}
	
//	@Test		// -- This test is working --
//	@Rollback(false)
//	public void removeById() {
//		boolean activityX = false;
//		activityX = activityService.deleteById(27);
//		Assert.assertEquals(activityX, true);
//	}
//	
//	@Test		// -- This test is working --
//	public void get() {
//		Activity activity = activityService.get(3);
//		Assert.assertNotNull(activity.getId());
//	}

//	@Test		// -- This test is working --
//	public void checkNonDeleted() {
//		List<Activity> nonDeletedActivities = activityService.getNonDeleted();
//		for (Activity activities : nonDeletedActivities) {
//			Boolean deleted = activities.getDeleted();
//			Assert.assertEquals(deleted, false);
//		}
//	}

 
}
