package au.com.dmsq.test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import au.com.dmsq.model.Activity;
import au.com.dmsq.model.ActivityCalendar;
import au.com.dmsq.service.ActivityCalendarService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:testContext.xml"})
@TestExecutionListeners({TransactionalTestExecutionListener.class})
@TransactionConfiguration(transactionManager = "transactionManager")
public class ActivityCalendarServiceTest extends AbstractJUnit4SpringContextTests{

	@Autowired
	ActivityCalendarService activityCalendarService;
		
	@Test		// -- This test is working --
	public void  save() {
//		Activity activityId = new Activity();
//		activityId.setId(37);
//		Account accountId = new Account();
//		accountId.setId(2);
//		
//		ActivityCalendar actCal = new ActivityCalendar();
//		actCal.setTimestamp((long) (2015)); 
//		actCal.setActivity(activityId);
//		actCal.setAccount(accountId);
//		
//		Assert.assertEquals(activityCalendarService.save(actCal), true);
//		
//		actCal = activityCalendarService.get(actCal.getId());
//		Assert.assertNotNull(actCal);
//	
	}
//	
//	@Test		// -- This test is working --
//	public void findAll(){
//		List<ActivityCalendar> actCalList = activityCalendarService.findAll();
//		
//		Assert.assertTrue(actCalList.size() > 0);
//	}
//	
//	@Test		// -- This test is working --
//	@Rollback(false)
//	public void removeById() {
//		boolean actCal = false;
//		actCal = activityCalendarService.deleteById(10);
//		Assert.assertEquals(actCal, true);
//	}
//	
//	@Test		// -- This test is working --
//	public void get() {
//		ActivityCalendar actCal = activityCalendarService.get(8);
//		Assert.assertNotNull(actCal.getId());
//	}
	
//	@Test		// -- This test is working --
//	public void getCurrent() {
//		List<ActivityCalendar> currentActivityCalendars = activityCalendarService.getCurrent();
//		for (ActivityCalendar activityCalendar : currentActivityCalendars) {
//			Long current = activityCalendar.getTimestamp();
//			Long currentY = (long) 2014;
//			Assert.assertEquals(current, currentY);
//		}
//	}
	
}
