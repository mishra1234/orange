CREATE TABLE `invoice` (
  
`id` 		 int(11) 	NOT NULL AUTO_INCREMENT,
  
`reference` 	 varchar(64) 	NULL,
  
`inicial_date` 	 DATETIME 	NULL DEFAULT '2015-01-01 09:21:39' COMMENT 'Date in which the invoice starts',
  
`end_date` 	 DATETIME 	NULL DEFAULT '2015-01-01 09:21:39' COMMENT 'Date in which the invoice ends',
  
`creation_date`  DATETIME 	NULL DEFAULT '2015-01-01 09:21:39' COMMENT '',
  
`amount` 	 int(11) 	NULL,
  
`is_paid` 	 tinyint(1) 	NOT NULL DEFAULT '0' COMMENT 'Not Paid = 0, Paid = 1',
  
`discount` 	 int(11) 	NULL,
  
`promocode` 	 int(11) 	NULL,
  
`client` 	 varchar(48) 	NULL,
`client_address` varchar(255) 	NULL,
  
`client_phone` 	 varchar(48) 	NULL,
  
`client_email` 	 varchar(48) 	NULL,
  
`client_abn` 	 varchar(48) 	NULL,
  
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;