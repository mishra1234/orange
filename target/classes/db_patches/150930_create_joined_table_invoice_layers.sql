CREATE TABLE `invoice_layers` (
  
`invoice_id` 	int(11) NOT NULL,
  
`layer_id` 	int(11) NOT NULL,
  
PRIMARY KEY (`invoice_id`,`layer_id`),
  
KEY `fk_layer9_idx` (`layer_id`),
  
KEY `fk_invoice4_idx` (`invoice_id`),
  
CONSTRAINT `fk_invoice4` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  
CONSTRAINT `fk_layer9`   FOREIGN KEY (`layer_id`)   REFERENCES `layer` (`id`)   ON DELETE NO ACTION ON UPDATE NO ACTION
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1;