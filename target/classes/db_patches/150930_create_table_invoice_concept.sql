CREATE TABLE `invoice_concept` (
  `invoice_id` 	int(11) 	NOT NULL,
  `concept` 	varchar(255) 	NULL,
  `amount` 	int(11)		NULL,
  `unit_cost` 	int(11)		NULL,
  `total` 	int(11)		NULL,
  PRIMARY KEY (`invoice_id`),
  CONSTRAINT `invoice_id3` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;